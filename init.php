<?php
/*
* ---------------------------------------------------------------
* APPLICATION ENVIRONMENT
* ---------------------------------------------------------------
*
*/
$_ENVIRONMENT = "production";

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['ENVIRONMENT'])) {
    $_SESSION['ENVIRONMENT'] = $_ENVIRONMENT;
}

if (isset($_REQUEST['env'])) {
    if ($_REQUEST['env'] == "dev") {
        $_SESSION['ENVIRONMENT'] = 'development';
    }
    if ($_REQUEST['env'] == "prod") {
        $_SESSION['ENVIRONMENT'] = 'production';
    }
    if ($_REQUEST['env'] == "default") {
        $_SESSION['ENVIRONMENT'] = $_ENVIRONMENT;
    }
}

//echo $_REQUEST['env'];

define('ENVIRONMENT', $_SESSION['ENVIRONMENT']);

?>

