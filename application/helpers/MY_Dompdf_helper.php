<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function send_email_button($div_to_convert) {

    $ci = & get_instance();

    $output = "";
    $config = $ci->load->config('wkhtmltopdf', true);
    ob_start();
    ?>
    <a href="#" id="emailTemplateModelTrigger" data-toggle="modal" data-target="#emailTemplateModel" class="pdf-btn btn btn-sm btn-primary pull-right"><i style="padding-right: 5px;" class="fa fa-envelope" aria-hidden="true"></i> Send Email </a>

    <a href="#"  id="pdfDownload" class="m-l btn btn-sm btn-primary pdf-btn" ><i style="padding-right: 5px;" class="i i-file-pdf" aria-hidden="true"></i>PDF Download</a>
    <div id="emailTemplateModel" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i style="padding-right: 5px;" class="fa fa-envelope" aria-hidden="true"></i> Send Email</h4>
                </div>
                <div class="modal-body"> 
                    <form id="printAndEmailFormm" data-parsley-validate data-parsley-errors-messages-disabled method="POST" action="">
                        <input name="_sendEmail" value="1" type="hidden">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">To:</label>
                                <div class="col-sm-10">
                                    <select style="width:400px"  multiple class="selecta form-control input-sm input-s" name="email[]" required>
                                        <option></option>
                                        <?php
                                        $users = $ci->db->query("SELECT * FROM intg_users WHERE deleted = '0' ")->result();
                                        foreach ($users as $user):
                                            ?>
                                            <option value="<?= $user->email ?>"><?= $user->email ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Subject:</label>
                                <div class="col-sm-10">
                                    <input value="" style="width:400px" class='input-sm input-s  form-control' type='text' name='subject' />

                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Attachment:</label>
                                <div class="col-sm-10">
                                    <a id="pdfButton" class="btn btn-default disabled" href=""><i class="i i-file-pdf"></i> .pdf</a>
                                    <i id="loader"></i>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Body</label>
                                <div class="col-sm-10">
                                    <textarea name="body" style="width:400px; height:200px;">Please find the attachment.</textarea>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="pdfName" id="pdfName">
                        <div class="modal-footer">
                            <div class="form-actions">
                                <input type="submit" name="submit" class="btn btn-primary" value="Send"  />
                                &nbsp;&nbsp;
                                <a href="#" class="btn btn-warning" data-dismiss="modal">Close</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            response = 0;
            pdfAlreadyGenratedName = null;
            ispdfAlreadyGenrated = 0;
            page_html  = $("#<?=$div_to_convert;?>").html();
            
            var css_list = [];
            $.each($("html").find("link"), function( index, value ) {
                if(value.type==="text/css"){
                    css_list.push(value.href);
                }
            });
            page_css_list = JSON.stringify(css_list);;

            $("#printAndEmailFormm").submit(function () {
                
                 var data = $('#printAndEmailFormm').serializeArray();
                    data.push({name: '_html', value: page_html});
                    data.push({name: '_css_list', value: page_css_list});

                if (response)
                {
                    waitingDialog.show('...Please Wait ');

                    $.ajax({
                        type: 'post',
                        url: '',
                        data:data,
                        success: function (data)
                        {
                            alert("Notice: Email sent successfully.");
                            $("#emailTemplateModel").modal('hide');
                            waitingDialog.hide();
                        }
                    });

                } else
                {
                    alert("Notice: File attachment is in progress.");
                }

                return false;
            });




            $('#pdfDownload').on('click', function (e) {
              
                
                waitingDialog.show('...Pdf Conversion in Progress ');
                $.ajax({
                    type: 'post',
                    url: '',
                    data: {
                        _downloadPDF: "1",
                        _html: page_html,
                        _css_list: page_css_list
                    },
                    success: function (data)
                    {
                        console.log(data);
                        if (data.file_name) {
                            pdfAlreadyGenratedName = data.file_name;
                            ispdfAlreadyGenrated = 1;
                            response = 1;
                            $('#pdfName').val(pdfAlreadyGenratedName);
                            waitingDialog.hide();
                            console.log('<?php echo base_url() . $config['base_path']; ?>' + data.file_name + "");
                            window.open('<?php echo base_url() . $config['base_path']; ?>' + data.file_name + "", '_blank');
                        } else
                        {
                            alert("Error in Exporting: Please contact to administrator.");
                            waitingDialog.hide();
                        }
                    }
                });
            });
            $('#emailTemplateModel').on('show.bs.modal', function () {
                if (ispdfAlreadyGenrated)
                {
                    $('#pdfButton').removeClass('disabled');
                    $('#pdfButton').attr("href", '<?php echo base_url() . $config['base_path']; ?>' + pdfAlreadyGenratedName + "");
                    $('#pdfButton').attr("target", "_blank");
                } else {
                    var preloaderdiv = '<div class="thumbs_preloader">Loading...</div>';
                    $('#loader').html(preloaderdiv);
                    $('#pdfButton').addClass('disabled');

                    $.ajax({
                        type: 'post',
                        url: '',
                        data: {
                            _generatePDF: "1",
                            _html: page_html,
                            _css_list: page_css_list
                        },
                        success: function (data)
                        {
                            if (data.file_name)
                            {
                                response = 1;
                                $('#loader').html("");
                                $('#pdfButton').removeClass('disabled');
                                $('#pdfButton').attr("href", '<?php echo base_url() . $config['base_path']; ?>' + data.file_name + "");
                                $('#pdfButton').attr("target", "_blank");

                                $('#pdfName').val("" + data.file_name + "");

                            } else
                            {
                                alert("Error in Exporting: Please contact to administrator.");
                                waitingDialog.hide();
                            }
                        }
                    });
                }


            });

        });
    </script>

    <?php
    $output = ob_get_clean();
    return $output;
}

function convertToPdfAndSendEmailxxxx() {

 $ci = & get_instance();
    if (isset($_POST['_sendEmail'])) {
       
        //  $module_name = $ci->db->query("select nickname from intg_modules where id = '" . $module_id . "'")->row()->nickname;
        $emails = $ci->input->post('email');
        $subject = $ci->input->post('subject');
        $body = $ci->input->post('body');
        $pdfName = $ci->input->post('pdfName');
        $pdfFileName = $pdfName;

        $ci->load->library('emailer/emailer');

        $mail = $ci->emailer->getEmailer();
        foreach ($emails as $row) {
            $mail->addAddress($row, $row);
        }
        $mail->Subject = $subject;
        $mail->Body = $body;

        $config = $ci->load->config('wkhtmltopdf', true);
        $file = $config['path'] . '/' . $pdfFileName;

        //  attach
        $mail->addAttachment($file, ".pdf");
        $mail->Send();
        die('1');
    }
    if (isset($_POST['_generatePDF'])) {

        ob_clean();
        ob_start();

        header('Content-Type: application/json');

        $output = array();

        $output['file_name'] = md5(time() . rand(1, 2000));

        Template::pdf_view($output['file_name'], 3);

        $output['file_name'] = $output['file_name'] . ".pdf";
        echo json_encode($output);
        die;
    }


    if (isset($_POST['_downloadPDF'])) {

        ob_clean();
        ob_start();

        header('Content-Type: application/json');

        $output = array();

        $output['file_name'] = md5(time() . rand(1, 2000));

        Template::pdf_view($output['file_name'], 3);

        $output['file_name'] = $output['file_name'] . ".pdf";
        echo json_encode($output);
        die;
    }
}

function convertToPdfAndSendEmail() {
    $CI = & get_instance();
    $_html = $CI->input->post('_html');
    $_css_list = json_decode(stripslashes($CI->input->post('_css_list')));

    if (isset($_POST['_sendEmail'])) {
        
    }
    
    if (isset($_POST['_generatePDF'])) {
        
    }

    if (isset($_POST['_downloadPDF'])) {
        
        $pdfFileName = "TEST_DOMPDF_" . time() . ".pdf";
        $CI->load->library('MY_Dompdf');
        $config = $CI->load->config('dompdf_config', true);
        $filenamepath = $config['path'] . '/' . $pdfFileName;
        
        $CI->load->helper('simple_html_dom');
        $strhtml = str_get_html($_html);
        foreach ($strhtml->find('a[class=pdf-btn]') as $element) {
            $_html = str_replace($element, "", $_html);
        }
        
        foreach ($strhtml->find('script') as $element) {
            $_html = str_replace($element, "", $_html);
        }
        

//        $_html_css = "<html><head>";
//        foreach ($_css_list as $key=>$val) {
//            $_html_css .=  '<link rel="stylesheet" type="text/css" href="'.$val.'" />';
//        }
//         $_html_css .= "</head>";
         
//         $_html =  $_html_css.'<body>'.$_html."<body></html>";
         
        ob_start();
        
        ?>
        <link rel="stylesheet" type="text/css" href="<?=Template::theme_url("css/bootstrap.css")?>" />
        <link rel="stylesheet" type="text/css" href="<?=Template::theme_url("css/font-awesome.min.css")?>" />
        <link rel="stylesheet" type="text/css" href="<?=Template::theme_url("css/font.css")?>" />
        <link rel="stylesheet" type="text/css" href="<?=Template::theme_url("css/app.css")?>" />
        <?php
        echo $_html;
        $html = ob_get_contents();
        ob_end_clean();
        

        $dompdf = new MY_Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
         $dompdf->set_base_path(base_url());
//        foreach ($_css_list as $key=>$val) {
//             $dompdf->set_base_path($val);
//        }
       
        $dompdf->render();

        $pdf_gen = $dompdf->output();
        // Output the generated PDF to Browser
        //$dompdf->stream();
        
        header('Content-Type: application/json');
        
        $output = array();

        if (!file_put_contents($filenamepath, $pdf_gen)) {
            $output['file_name'] = false;
        } else {
            $output['file_name'] = $pdfFileName;
        }
        echo json_encode($output);
        die();
    }
}
