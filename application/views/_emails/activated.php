<p>Congratulations. Your account on the <?php e($title); ?> site is now registered and active!</p>

<?php if(isset($username) && isset($password)):?>
<p>Username: <strong><?php e($username);?></strong></p>

<p>Password: <strong><?php e($password);?></strong></p>
<?php endif;?>

<p><a href="<?php echo $link ?>"><?php echo $link ?></a></p>