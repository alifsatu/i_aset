<div marginwidth="0" marginheight="0">



<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
<td width="100%" valign="top" bgcolor="#f8f8f8">

<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1;margin-top:50px">
  <tbody><tr>
    <td width="460">

      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td height="30">
          </td>
        </tr>
      </tbody></table>

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="40"></td>
          <td width="512" style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">
            
          </td>
          <td width="40"></td>
        </tr>
        <tr>
          <td width="40"></td>
           <td width="460" style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">
            <span style="text-decoration:none;color:#2f2f36;font-weight:bold;font-size:20px;line-height:32px">Hi <?php e($finance_name);?>,<br>You got a new request to unlock <?php e($item_type);?>.</span><br>
          </td>
          <td width="40"></td>
        </tr>
        </tbody></table>

    </td>
  </tr>
</tbody></table>



<table width="460" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody><tr>
    <td width="209">

      <table width="209" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="209" height="5" bgcolor="#ffffff"></td>
        </tr>
        <tr>
          <td width="209" height="1" bgcolor="#e1e1e1"></td>
        </tr>
        <tr>
          <td width="209" height="10" bgcolor="#ffffff"></td>
        </tr>
      </tbody></table>

    </td>
    <td width="42">

      <table width="42" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td style="text-align:center">
            <img src="https://ci5.googleusercontent.com/proxy/KveaD9HdYnB8s3-Zc6C809y_abyUkEw-32xLF1nlhYyRGb1NPYZZqpFAxjIo-ZmMIZKHkmySA1YaOAQxqkefobd2oPnN4M_-HC-mAvCOO2GRBed-FXB5MaqwWTyKl6sLOREe=s0-d-e1-ft#https://wave-vero-assets.s3.amazonaws.com/images/checkmark-reminder-email.jpg" alt="" border="0">
          </td>
        </tr>
      </tbody></table>

    </td>
    <td width="209">

      <table width="209" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="209" height="5" bgcolor="#ffffff"></td>
        </tr>
        <tr>
          <td width="209" height="1" bgcolor="#e1e1e1"></td>
        </tr>
        <tr>
          <td width="209" height="10" bgcolor="#ffffff"></td>
        </tr>
      </tbody></table>

    </td>
  </tr>
</tbody></table>



<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody>
    <tr>
      <td width="460">

        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
               <td style="font-size:16px;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" height="20"> Below are the <?php e($item_type);?> details:<br><br>
            <table border="0" cellpadding="0" cellspacing="0" align="center" style="text-align:left">
			<tbody>
				<tr><td>Period</td><td>: <strong><?php e($period);?></strong></td></tr>
				
			</tbody>
			</table>
              </td>
            </tr>
          </tbody>
        </table>

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
              <td width="40"></td>
              <td width="510" style="font-size:14px;color:#444;font-weight:normal;text-align:left;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top"><br><br>
                
               
                 <p style="font-size:18px;border-top:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;padding:10px 0;background:#fefefe;text-align:center;margin:5px 0">
                  
                   Created by :
                  
                  <span style="white-space:nowrap;font-weight:bold;font-size:18px"><?php e($creator_name);?></span>
                </p>
                <br>
              </td>
              <td width="40"></td>
            </tr>
          </tbody>
        </table>

      </td>
    </tr>
  </tbody>
</table>


<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody>
    <tr>
      <td width="460">

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
              <td width="40"></td>
              <td width="510" style="font-size:14px;color:#a0a0a5;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">

                <div>
                  
                    
                    <a href="<?php e(site_url('login'));?>" style="background-color:rgb(76, 192, 193); border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px" target="_blank">
                     Click here to Login
                    </a>
                  

                  

                </div>
                
              </td>
              <td width="40"></td>
            </tr>
            <tr>
              <td width="40"></td>
              <td width="512" height="10"></td>
              <td width="40"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>






<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody><tr>
    <td width="460">

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="460" height="10">
          </td>
        </tr>
      </tbody></table>
      

    </td>
  </tr>
</tbody></table>


      



<table width="462" bgcolor="#f0f0f0" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1">
  <tbody><tr>
    <td width="462" height="10">
    </td>
  </tr>
</tbody></table>



  
  <table width="460" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
    <tbody><tr>
      <td width="460" bgcolor="#f0f0f0">

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody><tr>
            <td width="460">

              
              <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
                <tbody><tr>
                  <td width="30"></td>
                  <td width="530">

                    
                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                      <tbody><tr>
                        <td height="0" style="font-size:14px;font-weight:normal;font-family:Helvetica,Arial,sans-serif;line-height:24px">
                        </td>
                      </tr>
                    </tbody></table>

                    
                    <table width="140" border="0" cellpadding="0" cellspacing="0" align="left">
                      <tbody><tr>
                        <td width="140">
                        </td>
                      </tr>
                    </tbody></table>
                    

                  </td>
                  <td width="30"></td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>
  </tbody></table>
  



<table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
  <tbody><tr>
    <td width="460" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;border-radius:0 0 10px 10px;background:#f0f0f0">

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td height="14">
          </td>
        </tr>
      </tbody></table>
      

    </td>
  </tr>
</tbody></table>




  
  

<img src="https://ci5.googleusercontent.com/proxy/OhkjNm72TKOZl_1_s8UJboFGRdGO8wiwZ4EdX-fLvCRup6LyZOCNW4T4ueT0IU9m3hWVc7OR470OOa2lsB63x9ZsAmcKKODXux0-OFAQWwMVQUKkFufk8V6i9oqmfpaND6PBm0CbejMglkBBKw=s0-d-e1-ft#http://click.waveapps.com/track/open.php?u=11278363&amp;id=78d6045fd07248fca3cb9ac9f4b1e746" height="1" width="1"></div>