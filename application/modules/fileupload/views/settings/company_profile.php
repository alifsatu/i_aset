<link rel="stylesheet" href="<?=Template::theme_url('js/fileupload/css/jquery.fileupload.css')?>">


<div class="col-lg-12 animated fadeInDownBig">
<section class="panel panel-default">
 <div class="panel-body"><button type="button" class="close closefile" data-dismiss="modal"><i class="fa fa-times"></i></button>
   
   <br /><br />
   
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
      
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" id="mulfile"  multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
              
                <input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-condensed"><tbody class="files"></tbody></table>
        <input type="hidden"  id="rowid" />
        <input type="hidden"  id="invaction" />
    </form>
    <br>
 </div></section></div>

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                
                
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}

		
	
	
</script>



<script src="<?=Template::theme_url('js/fileupload/js/tmpl.min.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/load-image.min.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/jquery.fileupload.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/jquery.fileupload-process.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/jquery.fileupload-validate.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/jquery.fileupload-ui.js')?>"></script>
<script src="<?=Template::theme_url('js/fileupload/js/main.js')?>"></script>
<script>
	

$('.loader').remove();
$('#ajaxModal').on('hidden.bs.modal', function () {
	
	
	
	// var origin = "<?=$this->uri->segment(5)?>";
	
	//var a = $("#rowid").val();
	
	
	var b = $("#filename").val().split(",");
	//$("#img"+a).attr("src","<?=base_url()?>files/CID<?=$this->auth->company_id()?>/"+b[0]);
	
	if ( b !="") {
$('#baba').html('<img src="<?=base_url()?>files/CID<?=$this->auth->company_id()?>/thumbnail/'+b[0]+'" alt="" data-item="media">');
	

$.post("<?php echo site_url(SITE_AREA .'/settings/fileupload/inventory_update?cmp=company') ?>", {filenames:$("#filename").val()}, function(data){} )
	}

	
	
 // alert(origin)
  if ( origin =="files") {
	  if ( $("#filename").val() !="" ) {
  ajaxfileinsert();
	  }
  $("#filename").val("");
  }
})
</script>
