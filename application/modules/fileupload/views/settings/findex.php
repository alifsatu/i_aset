<?php

$num_columns	= 5;
$can_delete	= $this->auth->has_permission('Fileupload.Settings.Delete');
$can_edit		= $this->auth->has_permission('Fileupload.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="admin-box">
	<h3>Fileupload <div style="float:right">
    <?php 
    
echo form_open($this->uri->uri_string(),'class="form-search"'); ?>
	<label style="color:#fff">Search by fields</label>
<select name="select_field" id="select_field" onchange="setfname()">
<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>

<option>All Fields</option>		
<option value="personal_particulars_name">Name</option>
<option value="personal_particulars_nric">NRIC</option>
<option value="personal_particulars_htel">Handphone</option>
<option value="personal_particulars_email">Email</option>
<option value="personal_particulars_citizenship">Citizenship</option>
</select>
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>



<input type="text" name="field_value" class="search-query input-xlarge" value="<?=$this->session->userdata('capfvalue')?>">
<button class="btn" type="submit" name="submit" value="Search" title="Search" ><i class="icon-search icon-black"></i></button>
<button type="submit" name="submit" class="btn" value="Reset" title="Reset"><i class="icon-refresh icon-black"></i></button>

  

    
    </h3>
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					<th><?php echo lang("fileupload_column_deleted"); ?></th>
					<th><?php echo lang("fileupload_column_created"); ?></th>
					<th><?php echo lang("fileupload_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('fileupload_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
					<td><?php echo $record->deleted > 0 ? lang('fileupload_true') : lang('fileupload_false')?></td>
					<td><?php e($record->created_on) ?></td>
					<td><?php e($record->modified_on) ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
         <?php  echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
</div>