<?php
$num_columns	= 19;
$can_delete	= $this->auth->has_permission('Ideas.Ideabank.Delete');
$can_edit		= $this->auth->has_permission('Ideas.Ideabank.Edit');
$has_records	= isset($query) && is_array($query) && count($query);
$validation_errors = validation_errors();



?>

 <script src="<?php echo Template::theme_url('js/jquery.min.js'); ?>"></script> 
 <script src="<?php echo Template::theme_url('js/jquery-ias.min.js'); ?>"></script> 

<script type="text/javascript">

        $(document).ready(function() {
			
		
        	// Infinite Ajax Scroll configuration
            jQuery.ias({
                container : '.parent-comments', // main container where data goes to append
                item: '.itema', // single items
                pagination: '.nava', // page navigation
                next: '.nava a', // next page selector
                //loader: '<img src="<?php echo Template::theme_url('js/ajax-loader.gif'); ?>"/>', // loading gif
               // triggerPageThreshold: 3 // show load more if scroll more than this
            }).on('rendered', function(items) {
    var $items = $(items);
console.log('onrendered');
    $items.each(function() {
		console.log(this,$('[rel=popover]'));
        $('[rel=popover]', this).popover();
    });
}).extension(new IASSpinnerExtension({
    src: '<?php echo Template::theme_url('js/ajax-loader.gif'); ?>', // optionally
	
})
)

			
        });
		
		
		 $(document).ready(function() {
        	// Infinite Ajax Scroll configuration
            jQuery('.scrollable').ias({
                container : '.parent-comments', // main container where data goes to append
                item: '.itema', // single items
                pagination: '.nava', // page navigation
                next: '.nava a', // next page selector
                //loader: '<img src="<?php echo Template::theme_url('js/ajax-loader.gif'); ?>"/>', // loading gif
               // triggerPageThreshold: 3 // show load more if scroll more than this
            }).on('rendered',function(items) {
    var $items = $(items);
console.log('boom');
    $items.each(function() {
        $('[rel=popover]', this).popover({html:true});
		 $('.titced').blur('click', function() {
        var siblings = $(this).html();			
	  
  if ( siblings.length ==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
	 
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {title:siblings,rowid:$(this).attr("rowid")}, function(){});
	 }
	});
  
   $('.desced').blur('click', function() {
        var siblings = $(this).html();			  
		 
		  if ( siblings.length > 50 || siblings.length==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {desc:siblings,rowid:$(this).attr("rowid"),table:$(this).attr("type")}, function(){}); 
 
		 }
  
 });
		
    });
}).extension(new IASSpinnerExtension({
    src: '<?php echo Template::theme_url('js/ajax-loader.gif'); ?>', // optionally
}));
			
			
        });
		
	function reload_wall(val)
	{
		
		window.location='<?=base_url()?>index.php/admin/ideabank/ideas/wall/group/'+val;
	}
		
		
    </script>
<div class="row parent-comments">

<div id="shift-target" class="visiblehide"></div>


  <div class="col-sm-10 ">

    <section class="panel panel-default"> <? echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
      <div class="panel-body">
        <div class="col-md-3"><h4>  <?  if ( mysql_num_rows($query) < 1 ) { //echo "No Records Found"; 
		} ?>
<?php echo lang('ideas_walls'); ?></h4></div>
        <div class="col-md-3">
       
        <select   name="group_id" id="group_id" class="form-control m-b selecta"  placeholder="Select Group" onchange="reload_wall($(this).val())"><option></option>
           
			<?php 
			$grps = $this->db->query('select * from intg_ideas_group where gcid = '.$current_user->company_id.' and find_in_set('.$this->auth->user_id().',gusers)')->result();
			
			
			foreach($grps as $grp)
			{
				if ( $this->uri->segment(6) == $grp->gno ) { 
				?>					
			<option selected="selected" value="<?=$grp->gno?>"><?=ucfirst($grp->gname)?></option>
			<?php } else { ?>		
            <option  value="<?=$grp->gno?>"><?=ucfirst($grp->gname)?></option><? }} echo $this->db->last_query(); ?>			
			</select>	
            </div>
        <div class="col-md-3">
          <select name="select_field" id="select_field" class="form-control m-b selecta"  onchange="setfname()">
            <option><?php echo lang('ideas_allfields'); ?></option>
            <option value="title"><?php echo lang('ideas_title'); ?></option>
            <option value="idea_description"><?php echo lang('ideas_Description'); ?></option>
            <option value="posted_by"><?php echo lang('ideas_posted_by'); ?></option>            
          </select>
        </div>
        <div class="col-md-3">
          <div class="input-group">
            <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('sapfvalue')?>">
            <input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('sapfname')?>"/>
            <span class="input-group-btn">
            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
            <button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
            </span> </div>
        </div>
      </div>
      <?php echo form_close(); ?><hr />
      <section class="panel panel-default " style="border:none">
        <form id="ideapost">
          <section class="chat-list panel-body">
            <textarea placeholder="Title" name="ideas_title" id="ideas_title" class="expand control-group error" style="border:none"></textarea>
            <textarea placeholder="Description"   name="ideas_description" id="ideas_description" class="expand" style="border:none" cols="40"></textarea>
            <span id="context" class="context pull-right  bg-light" style="padding:4px; font-size:12px"></span>
            <div id="files" class="files"></div>
          </section>
          <header class="panel-heading wallpost" style="display:none">
           <div id="quotefile"><ul id="ul-sample"></ul></div>
          <label for="file_upload" style="float:left"> <a href="<? echo base_url();?>index.php/admin/settings/fileupload/index/top" id="btn_quote_to_invoice" data-quote-id="<?=$this->uri->segment(5)?>"   data-toggle="ajaxModal"><i class="fa fa-camera  icon-muted  fa-lg m-r-sm"></i></a>
         
          <input type="hidden" name="filename" id="filename" />
           <input name="filerename" id="filerename" type="hidden" />
           
            </label>
            <span  style="float:left"  ><a href="<? echo base_url();?>index.php/admin/settings/fileupload" id="btn_quote_to_invoice" data-quote-id="<?=$this->uri->segment(5)?>"  data-toggle="ajaxModal"  style="float:left"><i class="fa fa-upload  icon-muted  fa-lg m-r-sm"></i></a> <a href="#" onclick="$('#video').toggle()"   style="float:left"><i class="fa fa-youtube icon-muted fa-lg m-r-sm"></i></a><input type="text" name="ideas_video_links" id="video" class="form-control input-sm input-s" placeholder="You Tube link only" style="display:none" /></span>  <span class="col-sm-5">
            <label> <? echo lang('ideas_category')?> :
            <select name="category" id="idea_category_id" class="form-control m-b selecta" style=" width:200px;height:30px" >
              <?php // Change the values in this array to populate your dropdown as required

				$category = $this->db->get_where('intg_idea_categories', array('category_deleted'=>'0','company_id' => $this->auth->company_id()))->result();
		
		foreach($category as $h)
		{ if ($h->category_id=="Default") {?>
              <option selected="selected" value="<?=$h->id?>"><?=$h->category_id?></option>
              <? } else {
		?> <option  value="<?=$h->id?>"><?=$h->category_id?></option><? } } ?>
            </select> </label>
            </span>
            <input type="hidden" name="dnbid" id="dnbid" value="  <?=$this->auth->display_name_by_id($this->auth->user_id())?>"/>
            <input type="hidden" name="fileup" id="fileup" />
            <span class="pull-right"><a href="javascript:onclick=insidea('<?=$this->auth->user_id()?>')" class="btn btn-xs btn-facebook"><? echo lang('ideas_post')?></a></span>
            <div class="clearfix"></div>
          </header>
        </form>
        <header class="panel-heading" id="mainidea"><?php //echo lang('ideas'); ?></header>
        <?   
while ($record = mysql_fetch_object($query)): 
//foreach ($records as $record) { 
?>
        <section class="chat-list panel-body itema"  id="itema-<?php echo $record->idea_id?>">
          <section class="comment-list block">
            <article id="comment-id-1" class="comment-item"> <a class="pull-left thumb-sm avatar"> 
            
             <?    
            
	if ( @getimagesize(Template::theme_url('images/userimg/'.$record->profileimage.''))) { ?>
    <img src="<?php echo Template::theme_url('images/userimg/'.$record->profileimage.'') ?>" >
    <? } else { ?> <img src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"><? } ?>
            
            </a> <span class="arrow left"></span>
              <section class="comment-body panel panel-default">
                <div class="panel-heading bg-white col-sm-12" >
               
                  <div class="row" >
                  <div class="col-sm-6 col-md-4 padder-v b-r" >
                   
                    <a href="#"><?=$this->auth->display_name_by_id($record->user_id)?></a>
                  </div>
                  <div class="col-sm-6 col-md-4 padder-v b-r ">
                    
                     <span id="title<?=$record->idea_id?>" contenteditable="false"  rowid="<?=$record->idea_id?>" class="titced bg-info" style="font-size:100%; color:#FFF; padding:4px"><? echo $record->title; ?></span>
                    
                  </div>
                  <div class="col-sm-6 col-md-4 padder-v b" >                     
                    
                  <i class="fa fa-clock-o"></i>&nbsp;<abbr class="timeago" title="<?=$record->ideacdate?>"></abbr>
                  </div>
                  
                </div>
                  
                  </div>
                <div class="panel-body">
                  <div>
                    <p id="main_desc<?=$record->idea_id?>" rowid="<?=$record->idea_id?>" contenteditable="false" class="desced" type="ideas"><? echo $record->idea_description;?></p>
                     <div id="att<?=$record->idea_id?>"></div>
                    
 <? if ( $record->video_links !="" ) { ?>  
               <iframe width="204" height="160" src="//www.youtube.com/embed/<?=$record->video_links?>" frameborder="0" allowfullscreen></iframe><? } ?>          
                    <?php 
			  $img = explode(",",$record->attachment_name);
			  $ext = array("pdf","txt","doc","docx","xls","xlsx");
			
			  foreach ( $img as $imgval ) {  // echo image_thumb("files/".$imgval, 500,500)."<p>&nbsp;</p>"; 
			  
			  $extension = explode(".",$imgval);
			  
                    if ( $imgval!="" && !in_array($extension[1],$ext)) {   $n = $n + 1;?><a href="#" id="A<?=$record->idea_id.$n?>"  data-toggle="modal"   data-target="#myModal<?=$n?>" ><img src="<?=base_url()?>files/CID<?=$this->auth->company_id()?>/thumbnail/<?=$imgval?>"   /></a>
                    <p>&nbsp;</p>
                    <div id="myModal<?=$n?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <img src="<?=base_url()?>files/CID<?=$this->auth->company_id()?>/<?=$imgval?>" class="img-responsive">
        </div>
    </div>
  </div>
</div><? }  else { ?><p><a href="<?=base_url()?>files/CID<?=$this->auth->company_id()?>/<?=$imgval?>" id="A<?=$record->idea_id.$n?>" target="_blank"><?=$imgval?></a></p><? }   $n++; }  

// query like dislike table
$likecnt = $this->db->query("select count(*) as countlike from intg_like_dislike where socialize_id = '".$record->idea_id."' and type = 'socialize' and action = 'L'")->row();
$unlikecnt = $this->db->query("select count(*) as countunlike from intg_like_dislike where socialize_id = '".$record->idea_id."' and type = 'socialize' and action = 'UL'")->row();
$userlike = $this->db->query("select id from intg_like_dislike where socialize_id = '".$record->idea_id."' and type = 'socialize' and user_id ='".$this->auth->user_id()."' and action = 'L'");
$userunlike = $this->db->query("select id from intg_like_dislike where socialize_id = '".$record->idea_id."' and type = 'socialize' and user_id ='".$this->auth->user_id()."' and action = 'UL'");

$userfollow = $this->db->query("select id from intg_like_dislike where socialize_id = '".$record->idea_id."' and type = 'socialize' and user_id ='".$this->auth->user_id()."' and action = 'F'");


 ?>
                    
                  </div>
                  <div class="comment-action m-t-sm"  style="margin-bottom:10px"> 
                  <a href="#" onclick="follow('<?=$record->idea_id?>','socialize','soc_follow','<?=$userfollow->row()->id?>')" data-toggle="class" class="btn btn-default btn-xs <? if ( $userfollow->num_rows > 0 )  {?>   <? } else { echo 'active"'; } ?>"> <i class="fa fa-star-o text-muted text-active"></i> <i class="fa fa-star text-danger text"></i> <?php echo lang('ideas_follow'); ?> </a> 
                  
                  
                  <a href="#" data-toggle="class" class="like btn btn-default btn-xs <? if ( $userlike->num_rows() > 0 )  { echo "";  } else { echo 'active'; } ?>" onclick="likee('<?=$record->idea_id?>','socialize','<?=$likecnt->countlike?>','like')"><i class="fa fa-thumbs-up text-muted text-active"></i> <i class="fa fa-thumbs-up text-success text"></i> <span id="like<?=$record->idea_id?>">
                    <?=$likecnt->countlike?>
                    </span> </a>                
                    
                    
                    
                    <a href="#" data-toggle="class" class="btn btn-default btn-xs <? if ( $userunlike->num_rows > 0 ) { echo ""; } else { echo 'active'; }?>" onclick="dislikee('<?=$record->idea_id?>','socialize','<?=$unlikecnt->countunlike?>','unlike')"> <i class="fa fa-thumbs-down text-muted text-active"></i> <i class="fa fa-thumbs-down text-danger text"></i> <span id="dislike<?=$record->idea_id?>">
                    <?=$unlikecnt->countunlike?>
                    </span> </a> <a href="javascript:onclick=$('#c<?=$record->idea_id?>').focus()" id="#r<?=$record->idea_id?>" class="btn btn-default btn-xs"> <i class="fa fa-mail-reply text-muted"></i><?php echo lang('ideas_reply'); ?> </a>
                      
                    
                     <? if ($record->user_id == $this->auth->user_id() ) { ?>
                     
                      <a href="#" onclick="editpost('<?=$record->idea_id?>','main')"  class="btn btn-default btn-xs"><i class="fa fa-pencil text-muted" ></i></a>
                          <a href="#" rel="popover"  data-trigger="click"   data-content="<div><button type='button' onclick=rem('<?=$record->idea_id?>','MAIN','<?=$record->user_id?>')>YES</button></div>"  data-original-title="Are you sure to delete?" class="btn btn-default btn-xs"><i class="fa fa-trash-o text-muted" ></i></a>
                          
                           
                          <? } ?>
                    
                    <? /*$tag = explode(",",$record->tag_words); foreach ($tag as $val ) { ?>
                    <a href="#" class="btn btn-xs btn-default btn-rounded"><? echo $val; ?></a>
                    <? }*/ ?>
                  </div>
                 
                  <? $com = $this->db->query("select idc.id as cmid,idc.created_on,idc.comment,idc.post_by,iu.id as iud,iu.profileimage from intg_idea_comments idc,intg_users iu where idc.idea_id = '".$record->idea_id." ' AND idc.post_by=iu.id order by idc.id desc limit 2");
				  $com2 = $this->db->query("select * from intg_idea_comments where idea_id = '".$record->idea_id."' order by id desc limit 10");
				  
				  //echo $com2->num_rows();
				  
				  if ( $com2->num_rows() > 2 ) { ?> <header class="panel-heading" id="vm<?=$record->idea_id?>"><a href="javascript:onclick=viewmore('<?=$record->idea_id?>')"> <i class="fa fa-comments-o"></i> <? echo lang('ideas_view_more_comments')?></a></header>
                  
				
                <? } else { ?><header class="panel-heading">Comments</header><? } 
						  foreach ( $com->result() as $cval ) {  
						  
						  
						  $cmlikecnt = $this->db->query("select count(*) as countlike from intg_like_dislike where socialize_id = '".$cval->cmid."' and type = 'comments' and action = 'L'")->row();
$cmunlikecnt = $this->db->query("select count(*) as countunlike from intg_like_dislike where socialize_id = '".$cval->cmid."' and type = 'comments' and action = 'UL'")->row();
$cmuserlike = $this->db->query("select id from intg_like_dislike where socialize_id = '".$cval->cmid."' and type = 'socialize' and user_id ='".$this->auth->user_id()."' and action = 'L'");
$cmuserunlike = $this->db->query("select id from intg_like_dislike where socialize_id = '".$cval->cmid."' and type = 'socialize' and user_id ='".$this->auth->user_id()."' and action = 'UL'");
$cmuserfollow = $this->db->query("select id from intg_like_dislike where socialize_id = '".$cval->cmid."' and type = 'comments' and user_id ='".$this->auth->user_id()."' and action = 'F'");				
						// echo $cmuserfollow->row()->id;
						  ?>
                  <article id="comment-id-<?=$record->idea_id.$cval->cmid?>" class="comment-item"> <a class="pull-left thumb-sm avatar">
                  <?    
            
	if ( @getimagesize(Template::theme_url('images/userimg/'.$cval->profileimage.''))) { ?>
    <img src="<?php echo Template::theme_url('images/userimg/'.$cval->profileimage.'') ?>"  class="img-circle">
    <? } else { ?> <img src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"  class="img-circle"><? } ?>
     
                  
                  
                 </a> <span class="arrow left"></span>
                    <section class="comment-body panel panel-default">
                    
                    <div class="panel-heading bg-white col-sm-12" >
               
                  <div class="row" >
                  <div class="col-sm-8 col-md-8 padder-v b" >
                   
                   <a href="#"><? echo $this->auth->display_name_by_id($cval->post_by); ?></a> 
                  </div>
                 
                  <div class="col-sm-4 col-md-2 padder-v b" >                     
                    
                  <i class="fa fa-clock-o"></i>&nbsp;<abbr class="timeago testLongTerm" title="<?=$cval->created_on?>"></abbr>
                  </div>
                  
                </div>
                  
                  </div>
                    
                     
                      <div class="panel-body">
                     
                          <div id="comment_desc<?=$cval->cmid?>" rowid="<?=$cval->cmid?>" contenteditable="false" class="desced" type="comment"><?=$cval->comment?></div>
                        <div class="comment-action m-t-sm">
                       
                          
                          <a href="#" data-toggle="class" class="btn btn-default btn-xs <? if ( $cmuserlike->num_rows > 0 )  { echo "";  } else { echo 'active'; } ?>" onclick="cmlikee('<?=$record->idea_id?>','<?=$cval->cmid?>','comments','<?=$cmlikecnt->countlike?>','cmlike')"> <i class="fa fa-thumbs-up text-muted text-active"></i> <i class="fa fa-thumbs-up text-success text"></i> <span id="cmlike<?=$record->idea_id.$cval->cmid?>">
                    <?=$cmlikecnt->countlike?>
                    </span> </a>
                          
                    
                     <a href="#" data-toggle="class" class="btn btn-default btn-xs <? if ( $cmuserunlike->num_rows > 0 ) { echo ""; } else { echo 'active'; }?>" onclick="cmdislikee('<?=$record->idea_id?>','<?=$cval->cmid?>','comments','<?=$cmunlikecnt->countunlike?>','cmunlike')"> <i class="fa fa-thumbs-down text-muted text-active"></i> <i class="fa fa-thumbs-down text-danger text"></i> <span id="cmdislike<?=$record->idea_id.$cval->cmid?>">
                    <?=$cmunlikecnt->countunlike?>
                    </span> </a>     
                           
                          
                          <a href="javascript:onclick=$('#c<?=$record->idea_id?>').focus()" id="#r<?=$record->idea_id?>" class="btn btn-default btn-xs"> <i class="fa fa-mail-reply text-muted"></i> <? echo lang('ideas_reply')?> </a>
                            <? if ($cval->post_by == $this->auth->user_id() ) { ?>
                            
                          dec
                             
                          <a href="#" rel="popover"    data-trigger="click" data-content="<div><button type='button' onclick=rem('<?=$record->idea_id?>','<?=$cval->cmid?>','<?=$cval->post_by?>')>YES</button></div>" data-original-title="Are you sure to delete?" class="btn btn-default btn-xs"><i class="fa fa-trash-o text-muted" ></i></a>
                          
                           
                          <? } ?>
                          
                         
                          
                          
                           </div>
                      </div> 
                    </section>
                  </article>
                  <? } ?>
                  <div id="rc<?=$record->idea_id?>"></div>
                </div>
              </section>
            </article>
            <div id="orc<?=$record->idea_id?>"></div>
          </section>
          <footer class="panel-footer"> 
            <!-- chat form -->
            <article class="chat-item" id="chat-form"> <a class="pull-left thumb-sm avatar">
        <?    
            
	if ( @getimagesize(Template::theme_url('images/userimg/'.$current_user->profileimage.''))) { ?>
    <img src="<?php echo Template::theme_url('images/userimg/'.$current_user->profileimage.'') ?>">
    <? } else { ?> <img src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"><? } ?>
            </a>
              <section class="chat-body">
                <div class="m-b-none">
                  <div class="input-group">
                    <input type="text" class="form-control" id="c<?=$record->idea_id?>" placeholder="Say something">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="inscom('<?=$record->idea_id?>','<?=$record->idea_id?>')"><? echo lang('ideas_send')?></button>
                    </span> </div>
                </div>
              </section>
            </article>
          </footer>
        </section>
        <? // } 
		endwhile?>
        
        
      </section>
    </section>
  
  </div>
  
  
  <div class="col-sm-2 hidden-xs  shift" data-toggle="shift:appendTo" data-target="#shift-target">
  <section class="panel panel-default">
     <div class="text-center wrapper bg-light lt"><strong><? echo lang('ideas_pop_members')?></strong> </div>
    
    <div class="panel-body text-center">
    
  
    
    <? foreach ( $popname as $val=>$key ) { ?>
				
<div class="avatar large  pull-left" title="<?=$popname[$val]?>">
	<?
    if ( @getimagesize(Template::theme_url('images/userimg/'.$profileimage[$val].''))) { ?>
    <img src="<?php echo Template::theme_url('images/userimg/'.$profileimage[$val].'') ?>"   class="thumb-sm avatar">
    <? } else { ?> <img src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"   class="thumb-sm avatar"><? } ?>
		
	
</div>
<? } ?>

</div>
</section>
    <section class="panel panel-default">
       <div class="text-center wrapper bg-light lt"><strong><? echo lang('ideas_top_ideas')?></strong> </div>
      <div class="panel-body text-center">
        <div class="sparkline inline"   data-type="pie" data-height="75" data-slice-colors="['#77c587','#41586e','#f2f2f2']" align="center"><?  echo implode(",",$ideacount);?></div>
        
            
        
      </div>
      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;<?=$ideacount[0]?></span> <span class="label bg-primary">1</span>&nbsp;<? echo $ideatitle[0]; ?></li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;<?=$ideacount[1]?></span> <span class="label bg-dark">2</span>&nbsp;<? echo $ideatitle[1]; ?></li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;<?=$ideacount[2]?></span> <span class="label bg-light">3</span>&nbsp;<? echo $ideatitle[2]; ?></li>
      </ul>
    </section>
    
    <section class="panel panel-default">
       <div class="text-center wrapper bg-light lt"><strong><? echo lang('ideas_top_contributors')?></strong> </div>
      <div class="panel-body text-center">
        <div class="sparkline inline" data-type="pie" data-height="75" data-slice-colors="['#77c587','#41586e','#f2f2f2']"><?  echo implode(",",$disccount);?></div>
      </div>
      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right">&nbsp;<?=$disccount[0]?></span> <span class="label bg-primary">1</span>&nbsp;<? echo $disctitle[0]; ?></li>
        <li class="list-group-item"> <span class="pull-right">&nbsp;<?=$disccount[1]?></span> <span class="label bg-dark">2</span>&nbsp;<? echo $disctitle[1]; ?></li>
        <li class="list-group-item"> <span class="pull-right">&nbsp;<?=$disccount[2]?></span> <span class="label bg-light">3</span>&nbsp;<? echo $disctitle[2]; ?></li>
      </ul>
    </section>
    
    <section class="panel panel-default">
      <div class="text-center wrapper bg-light lt"><strong><? echo lang('ideas_top_themes')?></strong> </div>
      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right"><? echo $catgcount[0]; ?></span> <span class="label bg-primary">1</span> <? echo $catgname[0]; ?> </li>
        <li class="list-group-item"><span class="pull-right"><? echo $catgcount[1]; ?></span>  <span class="label bg-dark">2</span> <?  echo $catgname[1];?> </li>
        <li class="list-group-item"> <span class="pull-right"><?  echo $catgcount[2];?></span> <span class="label bg-light">3</span> <? echo $catgname[2]; ?> </li>
      </ul>
    </section>
     
  </div>
   <?php if (isset($next)): ?>
	<div class="nava">
    <script>
	$(document).ready(function() {
	
	})
				</script>
   
		<a href='<?=base_url()?>index.php/admin/ideabank/ideas/wall?p=<?php echo $next?>'>Next</a>
	</div>
	<?php endif?>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog pulse animated">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Start Group Conversation</h4>
      </div>
      <div class="modal-body">
      <div class="m-b-sm">
		                <div class="btn-group" data-toggle="buttons">
		                  <label class="btn btn-sm btn-info" id="optiona">
		                    <input type="radio" name="options" id="option1"><i class="fa fa-check text-active"></i> Invite Users
		                  </label>
		                  <label class="btn btn-sm btn-danger" id="optionb">
		                    <input type="radio" name="options" id="option2"><i class="fa fa-check text-active"></i> Invite User Group
		                  </label>
                           <label class="btn btn-sm btn-primary">
		                    <input type="radio" name="options" id="option3"><i class="fa fa-check text-active"></i> Discuss Project
		                  </label>
		                 
		                </div>
                        <div id="addusers" ></div>
                        
		              </div>
                      
                     
      </div>
      <div class="modal-footer"  >
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="startgroup"  disabled="disabled"  data-loading-text="Please Wait..." autocomplete="off">Start</button>
      </div>
    </div>
  </div>
</div>

<script>

$(".btn-group > #optiona").click(function(){
	
$('#addusers').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/group_conference') ?>", {users:1}, function(data){$('#addusers').html(data); } )

	
});

$(".btn-group > #optionb").click(function(){
	
$('#addusers').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/group_conference') ?>", {group:1}, function(data){$('#addusers').html(data); } )

	
});

$("#startgroup").click(function() { 

$.ajax({
  type: "POST",
  url: "<?php echo site_url(SITE_AREA .'/ideabank/ideas/group_conference') ?>",
  data:{insert:1,gname:$("#groupname").val(),userid:$("#user_id").val(),projectid:0},
  dataType: 'json',
  success: function(data){
	  
	  reload_wall(data.insertid);

    $('#group_id').append("<option value='"+data.insertid+"'>"+data.grpname+"</option>"); 
	$("#startgroup").text("start");
	$("#startgroup").removeClass("disabled");
  }
  
  
});

$('#myModal').modal("hide");


/*
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/group_conference') ?>", {insert:1,gname:$("#groupname").val(),userid:$("#user_id").val()}, function(data){$('#group_id').append("<option value='"+data.insertid+"'>"+data.grpname+"</option>"); }) .done(function( data ) {
  $('#myModal').modal("hide");
  });*/



});

$('.img-modal').click(function(event) {

var username = $(this).attr('data-filename')

$("#img-in-modal").attr("src","big-avatar-" + username + ".jpg")


$('#myModal').modal('show')
})

function insidea(val)
{	

if ( $("#ideas_description").val()=="" ) {   $("#ideas_description").addClass("pulse animated infinite"); }
if ( $("#ideas_title").val()=="" ) {   $("#ideas_title").addClass("pulse animated infinite"); }

if ( $("#ideas_title").val() !="" && $("#ideas_description").val()!=""   ) {
	
	
	var imagesstr = $("#filerename").val();
	var images = imagesstr.split(',');	
	var imgstr ="";	
	for ( var i = 0;i < images.length; i++ ) {		
		if (  $("#filerename").val() !="" ) {
			
		var ext = images[i].split('.').pop().toLowerCase();
	
	if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
imgstr += "<p><a href='<?=base_url()?>files/CID<?=$this->auth->company_id()?>/"+images[i]+"' target='_blank'>"+images[i]+"</a></p>";		
	} else {
imgstr += "<img src='<?=base_url()?>files/CID<?=$this->auth->company_id()?>/thumbnail/"+images[i]+"'><br>";				
	}	
	
	
		}		
	}
	
	if ( $("#video").val() !="" ) {		
		
		var myString = $("#video").val();
var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
var match = myString.match(regExp);		
		
		imgstr +='<iframe width="254" height="210" src="//www.youtube.com/embed/'+match[2]+'" frameborder="0" allowfullscreen></iframe>';	
		
	}
	
	var usrimg;
	
	$.get('<?=base_url()?>themes/notebook/images/userimg/<?=$current_user->profileimage?>')
    .done(function() { 
	
	  usrimg = "<img src='<?=base_url()?>themes/notebook/images/userimg/<?=$current_user->profileimage?>'>";     

    }).fail(function() { 
   	usrimg = "<img src='<?=base_url()?>themes/notebook/images/userimg/default.jpg'>";
    })
	
// add  image html above
	$.post("<?=base_url()?>index.php/admin/ideabank/ideas/ajax_create",  $('form[id$=ideapost]').serialize()+'&groupid=<?=$this->uri->segment(6)?>&save=', function(data){	
	
	
		
		$('<div style="padding:15px 15px 0px 15px" id="itema-'+data+'"><section class="comment-list block"><article id="comment-id-1" class="comment-item"><a class="pull-left thumb-sm avatar">'+usrimg+'</a> <span class="arrow left"></span><section class="comment-body panel panel-default"><header class="panel-heading bg-white">'+$("#dnbid").val()+'&nbsp;&nbsp;<span id="title'+data+'" contenteditable="false" rowid="'+data+'" class="titced  bg-info"  style="font-size:100%; color:#FFF">'+$("#ideas_title").val()+'</span><span class="text-muted m-l-sm pull-right"><i class="fa fa-clock-o"></i>  Just Now</span> </header><div class="panel-body"><div><p id="main_desc'+data+'" rowid="'+data+'" contenteditable="false" class="desced" type="ideas">'+$("#ideas_description").val()+'</p><div id="att'+data+'"></div><p id ="A'+data+'">'+imgstr+'</p></div><div class="comment-action m-t-sm"  style="margin-bottom:10px"><a href="#" data-toggle="class" onClick=follow(\"'+data+'"\,"socialize","soc_follow") class="btn btn-default btn-xs active"><i class="fa fa-star-o text-muted text-active"></i><i class="fa fa-star text-danger text"></i>Follow</a>&nbsp;<a href="#" data-toggle="class" class="btn btn-default btn-xs active" onclick="likee(\''+data+'\',\'socialize\',\'0\',\'like\')"><i class="fa fa-thumbs-up text-muted text-active"></i><i class="fa fa-thumbs-up text-success text"></i><span id="like'+data+'">0</span></a>&nbsp;<a href="#" data-toggle="class" class="btn btn-default btn-xs active" onclick="dislikee(\''+data+'\',\'socialize\',\'0\',\'unlike\')"><i class="fa fa-thumbs-down text-muted text-active"></i><i class="fa fa-thumbs-down text-danger text"></i><span id="dislike'+data+'">0</span></a>&nbsp;<a href="javascript:onclick=$("#c'+data+'").focus()" id="#r'+data+'" class="btn btn-default btn-xs"><i class="fa fa-mail-reply text-muted"></i>Reply</a><a href="#" onclick="editpost(\''+data+'\',\'main\')"  class="btn btn-default btn-xs"><i class="fa fa-pencil text-muted" ></i></a><a href="#" id="popover"  data-trigger="click"     data-original-title="Are you sure to delete?" class="btn btn-default btn-xs"><i class="fa fa-trash-o text-muted" ></i></a></div><div id="rc'+data+'"></div></div></section></article><footer class="panel-footer"><article class="chat-item" id="chat-form"> <a class="pull-left thumb-sm avatar">'+usrimg+'</a><section class="chat-body"><form action="" class="m-b-none"><div class="input-group"><input type="text" class="form-control" id="c'+data+'" placeholder="Say something"><span class="input-group-btn"><button class="btn btn-default" type="button" onclick=inscom("'+data+'","'+data+'")>SEND</button></span> </div></form></section></article></footer></section></div>').insertAfter('#mainidea');
		
		//data-content="<div><button type="button" onclick=rem(\''+data+'\',\'MAIN\',\'<?=$this->auth->user_id()?>\')>YES</button></div>"
		
		 $("#popover").popover({
		
		placement:'right',
		 html : "true",
		content : "<div><button type='button' onclick=rem('"+data+"',\'MAIN\',\'<?=$this->auth->user_id()?>\')>YES</button></div>",
		 trigger :"click",
		  title : "Are you sure to delete?"
		
		});
		
		
 $('.titced').on('blur', function() {
        var siblings = $(this).html();	
	 if ( siblings.length ==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
	 
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {title:siblings,rowid:$(this).attr("rowid")}, function(){});
	 }
	});
	
	 $('.desced').on('blur', function() {
        var siblings = $(this).html();		
		 if ( siblings.length > 50 || siblings.length==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {desc:siblings,rowid:$(this).attr("rowid"),table:$(this).attr("type")}, function(){}); 
 
		 }
 });
		
		 });
		 

		
		
		$(this).data("timeout", setTimeout(function () {
        $('#ideapost').trigger("reset");
		$('#files').html("");
		arr = [];
		$("#quotefile").remove();
		$("#filename,#filerename").val("");
		
    }, 1000));
		
}



}


$(document).ready(function() {
   // put all your jQuery goodness in here.
   
   //$(".like").on('click',function() { $(this).removeAttr("data-toggle"); });
   
    var characters = 50;
    $("#context").append($("#ideas_description").val().length+' / '+'5K');
    $("#ideas_description").keyup(function(){
        if($(this).val().length > characters){
        $(this).val($(this).val().substr(0, characters));
        }
    var remaining = characters -  $(this).val().length;
    $("#context").html($(this).val().length+' / '+ '5K');
    if(remaining <= 10)
    {
        $("#context").css("color","red");
    }
    else
    {
        $("#context").css("color","black");
    }
    });
   
   
   
   $("#ideas_description").mouseover( function () { $("#ideas_description").removeClass("pulse animated infinite"); });
   $("#ideas_title").mouseover( function () { $("#ideas_title").removeClass("pulse animated infinite"); });

   $("[rel=popover]").popover({
		
		placement:'right',
		 html : "true",
	/*	content : "<div><button type='button' onclick='rem('"+val2+"','<?=$id?>')'>YES</button></div>",*/
		 trigger :"click",
		  title : "Are you sure to delete?"
		
		});

   jQuery("abbr.timeago").timeago();
 });
 

function inscom(val,val2)
{
	var d = new Date();
var createdon = d.getFullYear()+"-"+d.getMonth()+1+"-"+d.getDay();
//$('#rfsddiv').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {desc :$('#c'+val).val(),ideaid:val2,createdate:createdon}, function(data){
	
	$(data).insertBefore('#rc'+val); 
	$("#c"+val).val("");
	
	}).done(function(){
    $("[rel=popover]").popover({
		
		placement:'right',
		 html : "true",
	
		
		});
  });
	
}




function rem(val,val2,val3)		{
$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid :val,commentid:val2,remove:val2,postby:val3}, function(data)
{
	if ( val2 =="MAIN" ) {
	$("#itema-"+val).hide('slow',function() 
	{ 
	$("#itema-"+val).remove(); 
	}); 
	
	
	} else {
	$("#comment-id-"+val+val2).hide('slow',function() 
	{ 
	$("#comment-id-"+val+val2).remove(); 
	}); 
	}
})
	
}

function likee(val,val2,val3,val4)
{


if ( 	$('#like'+val).attr("liked") != 1 ) {
	$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid:val,type:val2,total:val3,status:val4}, function(data){
	
	$('#like'+val).html(data); 
	$('#like'+val).attr("liked","1");
	
	} );
}
}

function cmlikee(val5,val,val2,val3,val4)
{
	if ( 	$('#cmlike'+val5+val).attr("cmliked") != 1 ) {
	$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid:val5,type:val2,total:val3,status:val4,cmid:val}, function(data){
	
	$('#cmlike'+val5+val).html(data); 
	$('#cmlike'+val5+val).attr("cmliked","1");
	
	} )
	}
}


function dislikee(val,val2,val3,val4)
{
if ( 	$('#dislike'+val).attr("disliked") != 1 ) {
	
$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid:val,type:val2,total:val3,status:val4}, function(data){
	
	$('#dislike'+val).html(data); 
	$('#dislike'+val).attr("disliked","1");
	} )
}
}


function cmdislikee(val5,val,val2,val3,val4)
{
	
	if ( 	$('#cmdislike'+val5+val).attr("cmdisliked") != 1 ) {

	$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid:val5,type:val2,total:val3,status:val4,cmid:val}, function(data){
	
	$('#cmdislike'+val5+val).html(data); 
	$('#cmdislike'+val5+val).attr("cmdisliked","1");
	
	} )
	}
}




function follow(val,val2,val3,val4)
{
	
$.post("<?=base_url()?>index.php/admin/ideabank/ideas/comments", {ideaid:val,type:val2,status:val3,rowid:val4}, function(data){
	
	//$('#cmlike'+val5+val).html(data); 
	
	} )	
}


function viewmore(val)
{
	
	$.post("<?=base_url()?>index.php/admin/ideabank/ideas/viewmore", {id:val}, function(data){

	$(data).insertAfter('#vm'+val).animate( {bottom:'-10px'},"Very Slow");
	
	$("#vm"+val).remove(); 
	
	//$(data).parent().insertBefore($('#vm'+val).parent().next()).animate();
	
	} ).done(function(){
    $("[rel=popover]").popover({
		
		placement:'right',
		 html : "true",
	/*	content : "<div><button type='button' onclick='rem('"+val2+"','<?=$id?>')'>YES</button></div>",*/
		 trigger :"click",
		  title : "Are you sure to delete?"
		
		});
  });
}

function editpost(val,val2)
{
	if ( val2=="main") {
$("#title"+val).prop("contenteditable","true").css("padding","10px").css("border","gray solid 1px"); 

if ( $("#cam"+val).length ) { } else {
$("<p id='cam"+val+"'><a href='<? echo base_url();?>index.php/admin/settings/fileupload/index/content/"+val+"'   data-toggle='ajaxModal'><i class='fa fa-camera fa-lg'></i></a></p>").insertAfter("#main_desc"+val);
}

/*if ( $(".trash").length ) {} else {
$('a[id^="A'+val+'"]').removeAttr("data-toggle").addClass("cross").append("&nbsp;&nbsp; <span class='trash' title='DELETE'> <span class='lid'></span> <span class='can'></span> </span>");
}*/

//$('a[id^="A'+val+'"]').toggle();
	}
	
$("#"+val2+"_desc"+val).prop("contenteditable","true").css("padding","10px").css("border","rgb(238, 238, 238) solid 1px");

}

$(document).on('click', ".cross", function() {
    $(this).hide('slow',function() 
	{ 
	$(this).remove(); 
	}); 
    
});

$(document).ready(function() {
   // put all your jQuery goodness in here.
    $('.titced').blur('click', function() {
        var siblings = $(this).html();	
	 if ( siblings.length ==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
	 
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {title:siblings,rowid:$(this).attr("rowid")}, function(){});
	 }
	});
  
   $('.desced').blur('click', function() {
        var siblings = $(this).html();		
		 if ( siblings.length > 50 || siblings.length==0 ) {$(this).css("border","solid 1px red") } else { $(this).css("border","solid 1px rgb(2348,238,238)")
$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/upd_title_desc') ?>", {desc:siblings,rowid:$(this).attr("rowid"),table:$(this).attr("type")}, function(){}); 
 
		 }
 });
 });

</script>
