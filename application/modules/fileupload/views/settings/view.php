<style type='text/css'>
.expand {
	height: 1em;
	padding: 3px;
	width:100%;
}
.hcur {
	float:left;
	font-weight:bold
}
</style>
<script>function updateApprovalstatus(val,val2,val3,val4,val5,val6,val8,val7){	var a = $('#ass').val();var b = $('#asr').val();$('#load').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");$.post("<?php echo site_url(SITE_AREA .'/ideabank/ideas/updateApprovalstatus') ?>", {status:a,remarks:b,rowid:val2,rolename:val3,docid:val,mrowid:val4,act:val5,userid:val6,finalapprover:val8,initiator:val7}, function(data){$("#load").html(data);	} )}function remrow(val)	{	 		  	   $.post("<? echo base_url();?>index.php/admin/ideabank/ideas/add_upd_rows", { method:"delete",rowid:val },function(data) { $("#rtfs").html(data); } );	        	  	  $("#row"+val).hide('fast', function(){  $("#row"+val).remove(); });	 setTimeout(function() {      calcamount()}, 500);		}					</script>
<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
  <h4 class="alert-heading">Please fix the following errors:</h4>
  <?php echo $validation_errors; ?> </div>
<?php
endif;

if (isset($ideas))
{
	$ideas = (array) $ideas;
}
$id = isset($ideas['id']) ? $ideas['id'] : '';

?>
<div class="row">
<h3>Ideas</h3>
<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
<fieldset>
  <div class="form-group">
    <label class="col-sm-1 control-label">Theme</label>
    <div class="col-sm-3">
      <?	   $query = $this->db->query("SELECT description FROM intg_idea_themes WHERE id=".$ideas['theme_id']."")->result();				           foreach($query as $h)		{				?>
      <div class="checkbox">
        <?			echo $h->description;		}		?>
        <!-- <label><?= $h->description;?></label>          <input type="checkbox" id="ideas_theme_id" name="ideas_theme_id" value="<?= $h->id?>" <?php echo (isset($ideas['theme_id']) && $ideas['theme_id'] == 1) ? 'checked="checked"' : set_checkbox('ideas_theme_id', 1); ?>>--></div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-1 control-label">Title</label>
    <div class="col-sm-3">
      <div class="checkbox"> <?php echo $ideas['title']; ?> <!-- <input id='ideas_title' class="form-control" type='text' name='ideas_title' maxlength="255" value="<?php echo set_value('ideas_title', isset($ideas['title']) ? $ideas['title'] : ''); ?>" />--> </div>
    </div>
    <label class="col-sm-2 control-label">Category</label>
    <div class="col-sm-5"> <!-- <select name="category" id="category" class="form-control m-b selecta">-->
      <?php // Change the values in this array to populate your dropdown as required												$category = $this->db->query("SELECT category_id FROM intg_idea_categories WHERE id=".$ideas['idea_category_id']."")->result();				        				foreach($category as $g)		{ 			$cat = $g->category_id;			}			?>
      <div class="checkbox">
        <?			//echo $cat;				?>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-1 control-label">Description</label>
    <div class="col-sm-10">
      <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor"> <!-- <div class="btn-group">                            <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>                              <ul class="dropdown-menu">                              <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>                          </div>                          <div class="btn-group">                            <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>                              <ul class="dropdown-menu">                              <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>                              <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>                              <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>                              </ul>                          </div>                          <!--<div class="btn-group">                            <a class="btn btn-default btn-sm" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>                            <a class="btn btn-default btn-sm" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>                            <a class="btn btn-default btn-sm" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="fa fa-strikethrough"></i></a>                            <a class="btn btn-default btn-sm" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>                          </div>                          <div class="btn-group">                            <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="fa fa-list-ul"></i></a>                            <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="fa fa-list-ol"></i></a>                            <a class="btn btn-default btn-sm" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>                            <a class="btn btn-default btn-sm" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="fa fa-indent"></i></a>                          </div>                          <div class="btn-group">                            <a class="btn btn-default btn-sm btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>                            <a class="btn btn-default btn-sm" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>                            <a class="btn btn-default btn-sm" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>                            <a class="btn btn-default btn-sm" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>                          </div>                          <div class="btn-group">                          <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="fa fa-link"></i></a>                            <div class="dropdown-menu">                              <div class="input-group m-l-xs m-r-xs">                                <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink">                                <div class="input-group-btn">                                  <button class="btn btn-default btn-sm" type="button">Add</button>                                </div>                              </div>                            </div>                            <a class="btn btn-default btn-sm" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="fa fa-cut"></i></a>                          </div>                                                    <div class="btn-group">                            <a class="btn btn-default btn-sm" title="" id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="fa fa-picture-o"></i></a>                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 35px; height: 30px;">                          </div>                          <div class="btn-group">                            <a class="btn btn-default btn-sm" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>                            <a class="btn btn-default btn-sm" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>                          </div>--> <!--<input type="text" class="form-control-trans pull-left" data-edit="inserttext" id="voiceBtn" x-webkit-speech="" style="width:25px;height:28px;">--> </div>
      <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px" contenteditable="true"> <?php echo $ideas['idea_description'];  ?></div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-1 control-label">Video Links</label>
    <div class="col-sm-3">
      <div class="checkbox"> <?php echo $ideas['video_links']; ?> <!--<input id='ideas_video_links' class="form-control" type='text' name='ideas_video_links' maxlength="255" value="<?php echo set_value('ideas_video_links', isset($ideas['video_links']) ? $ideas['video_links'] : ''); ?>" />--> </div>
    </div>
    <label class="col-sm-2 control-label">Tag Words</label>
    <div class="col-sm-5"> <!--  <input id='ideas_tag_words' type='text'class="form-control" name='ideas_tag_words' maxlength="255" value="<?php //echo set_value('ideas_tag_words', isset($ideas['tag_words']) ? $ideas['tag_words'] : ''); ?>" /> -->
      <div id="MyPillbox" class="pillbox clearfix">
        <ul>
          <li class="label bg-info">Idea</li>
          <input type="text" placeholder="add a pill">
        </ul>
      </div>
    </div>
  </div>
  <div class="form-group"> <!-- <label class="col-sm-1 control-label">Tag People</label>                          <div class="col-sm-3">                            <select name="ideas_tag_people" id="ideas_tag_people" class="form-control m-b selecta">                          <?php // Change the values in this array to populate your dropdown as required							$user = $this->db->get_where('intg_users', array('deleted'=>'0'))->result();				foreach($user as $h)		{ ?>			<option value="<?=$h->id?>"><?=$h->display_name;?></option>		<? //}			?>                                                   </select>   </div>--> <!--<label class="col-sm-1 control-label">Attachment</label>                      <div class="col-sm-5">      <input id='attachment' type='file' name='attachment' maxlength="255" value="<?php echo set_value('attachment', isset($ideas['attachment']) ? $ideas['attachment'] : ''); ?>" />                                             </div>--> </div>
  </div>
</fieldset>
<?php echo form_close(); ?>
</div>
