<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Fileupload.Settings.View');
		$this->load->model('fileupload_model', null, true);
		$this->lang->load('fileupload');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		
		
		
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function jindex($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		
		$user_id =  $this->auth->user_id();

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->fileupload_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('fileupload_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('fileupload_delete_failure') . $this->fileupload_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}

if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->fileupload_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->fileupload_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

		//$records = $this->fileupload_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;



if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->fileupload_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->fileupload_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Fileupload');
		Template::render();
	}

	//--------------------------------------------------------------------


	public function server() { 
	//$this->load->view('server/php/');
	
	Template::render(); 
	
	}
	
	public function inventory_update() { 
	//$this->load->view('server/php/');
	
	if ( $this->input->get('cmp')=="company" ) {
	$this->db->query("update intg_companies set cmpimage = '".$_POST['filenames']."' where id = '".$this->auth->company_id()."'");
//echo $this->db->last_query();
}

else if ($this->input->get('cmp')=="wall" ) {
	$this->db->query("update intg_ideas set attachment_name = '".$_POST['filerenames']."' , attachment = '".$_POST['filenames']."' where id = '".$_POST['rowid']."'");
//echo $this->db->last_query();
}
 

else {
$this->db->query("update intg_inventory set attach_image='".$_POST['filenames']."' where id = '".$_POST['rowid']."'");

}

	
	}
	
	
	public function index() { 
	

	Template::render(); 

	}
	
	public function inventory_fileupload() { 
	
	Template::render(); 
	
	}
	public function company_profile() { 
	
	Template::render(); 
	
	}
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_fileupload SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('fileupload_success'), 'success');
				}
				else
				{
					Template::set_message(lang('fileupload_restored_error'). $this->fileupload_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_fileupload where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('fileupload_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}


if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->fileupload_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->fileupload_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

//$records = $this-fileupload_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;


if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->fileupload_model
->where('deleted','1')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->fileupload_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('fileupload', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Fileupload');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Fileupload object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Fileupload.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_fileupload())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fileupload_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'fileupload');

				Template::set_message(lang('fileupload_create_success'), 'success');
				redirect(SITE_AREA .'/settings/fileupload');
			}
			else
			{
				Template::set_message(lang('fileupload_create_failure') . $this->fileupload_model->error, 'error');
			}
		}
		Assets::add_module_js('fileupload', 'fileupload.js');

		Template::set('toolbar_title', lang('fileupload_create') . ' Fileupload');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Fileupload data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('fileupload_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/fileupload');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Fileupload.Settings.Edit');

			if ($this->save_fileupload('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fileupload_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'fileupload');

				Template::set_message(lang('fileupload_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('fileupload_edit_failure') . $this->fileupload_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Fileupload.Settings.Delete');

			if ($this->fileupload_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fileupload_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'fileupload');

				Template::set_message(lang('fileupload_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/fileupload');
			}
			else
			{
				Template::set_message(lang('fileupload_delete_failure') . $this->fileupload_model->error, 'error');
			}
		}
		Template::set('fileupload', $this->fileupload_model->find($id));
		Template::set('toolbar_title', lang('fileupload_edit') .' Fileupload');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_fileupload SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('fileupload_success'), 'success');
											redirect(SITE_AREA .'/settings/fileupload/deleted');
				}
				else
				{
					
					Template::set_message(lang('fileupload_error') . $this->fileupload_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_fileupload where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('fileupload_purged'), 'success');
					redirect(SITE_AREA .'/settings/fileupload/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('fileupload_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/fileupload');
		}

		
		
		
		Template::set('fileupload', $this->fileupload_model->find($id));
		
		Assets::add_module_js('fileupload', 'fileupload.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Fileupload');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_fileupload($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();

		if ($type == 'insert')
		{
			$id = $this->fileupload_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->fileupload_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}