<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_status_permohonan_sokongan_ccc_manage']			= 'Manage';
$lang['daftar_status_permohonan_sokongan_ccc_edit']				= 'Edit';
$lang['daftar_status_permohonan_sokongan_ccc_true']				= 'True';
$lang['daftar_status_permohonan_sokongan_ccc_false']				= 'False';
$lang['daftar_status_permohonan_sokongan_ccc_create']			= 'Save';
$lang['daftar_status_permohonan_sokongan_ccc_list']				= 'List';
$lang['daftar_status_permohonan_sokongan_ccc_new']				= 'New';
$lang['daftar_status_permohonan_sokongan_ccc_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_status_permohonan_sokongan_ccc_no_records']		= 'There aren\'t any daftar_status_permohonan_sokongan_ccc in the system.';
$lang['daftar_status_permohonan_sokongan_ccc_create_new']		= 'Create a new Daftar Status Permohonan Sokongan CCC.';
$lang['daftar_status_permohonan_sokongan_ccc_create_success']	= 'Daftar Status Permohonan Sokongan CCC successfully created.';
$lang['daftar_status_permohonan_sokongan_ccc_create_failure']	= 'There was a problem creating the daftar_status_permohonan_sokongan_ccc: ';
$lang['daftar_status_permohonan_sokongan_ccc_create_new_button']	= 'Create New Daftar Status Permohonan Sokongan CCC';
$lang['daftar_status_permohonan_sokongan_ccc_invalid_id']		= 'Invalid Daftar Status Permohonan Sokongan CCC ID.';
$lang['daftar_status_permohonan_sokongan_ccc_edit_success']		= 'Daftar Status Permohonan Sokongan CCC successfully saved.';
$lang['daftar_status_permohonan_sokongan_ccc_edit_failure']		= 'There was a problem saving the daftar_status_permohonan_sokongan_ccc: ';
$lang['daftar_status_permohonan_sokongan_ccc_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_status_permohonan_sokongan_ccc_purged']	= 'record(s) successfully purged.';
$lang['daftar_status_permohonan_sokongan_ccc_success']	= 'record(s) successfully restored.';


$lang['daftar_status_permohonan_sokongan_ccc_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_status_permohonan_sokongan_ccc_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_status_permohonan_sokongan_ccc_actions']			= 'Actions';
$lang['daftar_status_permohonan_sokongan_ccc_cancel']			= 'Cancel';
$lang['daftar_status_permohonan_sokongan_ccc_delete_record']		= 'Delete';
$lang['daftar_status_permohonan_sokongan_ccc_delete_confirm']	= 'Are you sure you want to delete this daftar_status_permohonan_sokongan_ccc?';
$lang['daftar_status_permohonan_sokongan_ccc_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_status_permohonan_sokongan_ccc_action_edit']		= 'Save';
$lang['daftar_status_permohonan_sokongan_ccc_action_create']		= 'Create';

// Activities
$lang['daftar_status_permohonan_sokongan_ccc_act_create_record']	= 'Created record with ID';
$lang['daftar_status_permohonan_sokongan_ccc_act_edit_record']	= 'Updated record with ID';
$lang['daftar_status_permohonan_sokongan_ccc_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_status_permohonan_sokongan_ccc_column_created']	= 'Created';
$lang['daftar_status_permohonan_sokongan_ccc_column_deleted']	= 'Deleted';
$lang['daftar_status_permohonan_sokongan_ccc_column_modified']	= 'Modified';
