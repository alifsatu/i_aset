<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_classification))
{
	$daftar_classification = (array) $daftar_classification;
}
$id = isset($daftar_classification['id']) ? $daftar_classification['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Classification</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_classification_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_classification_name' class='input-sm input-s  form-control' type='text' name='daftar_classification_name' maxlength="255" value="<?php echo set_value('daftar_classification_name', isset($daftar_classification['name']) ? $daftar_classification['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_classification_classification_group_id', $options, set_value('daftar_classification_classification_group_id', isset($daftar_classification['classification_group_id']) ? $daftar_classification['classification_group_id'] : ''), 'Classification Group'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'daftar_classification_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_classification_description' class='input-sm input-s  form-control' type='text' name='daftar_classification_description' maxlength="255" value="<?php echo set_value('daftar_classification_description', isset($daftar_classification['description']) ? $daftar_classification['description'] : '');?>" />
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_classification_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/daftar_classification', lang('daftar_classification_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>