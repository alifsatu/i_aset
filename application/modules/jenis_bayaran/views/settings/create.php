<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($jenis_bayaran)) {
    $jenis_bayaran = (array) $jenis_bayaran;
}
$id = isset($jenis_bayaran['id']) ? $jenis_bayaran['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Jenis Bayaran</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Bayaran' . lang('bf_form_label_required'), 'jenis_bayaran_nama', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='jenis_bayaran_nama' class='input-sm input-s  form-control' type='text' name='jenis_bayaran_nama' maxlength="255" value="<?php echo set_value('jenis_bayaran_nama', isset($jenis_bayaran['nama']) ? $jenis_bayaran['nama'] : ''); ?>" required="true" />
                            <span class='help-inline'><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('sorting_number') ? 'error' : ''; ?>">
                        <?php echo form_label('Nombor Urutan' . lang('bf_form_label_required'), 'jenis_bayaran_sorting_number', array('class' => 'control-label')); ?><em>Untuk tujuan susunan jenis bayaran</em>
                        <div class='controls'>
                            <input id='jenis_bayaran_sorting_number' class='input-sm input-s  form-control' type='text' name='jenis_bayaran_sorting_number' maxlength="255" value="<?php echo set_value('jenis_bayaran_sorting_number', isset($jenis_bayaran['sorting_number']) ? $jenis_bayaran['sorting_number'] : ''); ?>" required="true" />
                            <span class='help-inline'><?php echo form_error('sorting_number'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('formula') ? 'error' : ''; ?>">
                        <?php echo form_label('Formula/Peratusan', 'jenis_bayaran_formula', array('class' => 'control-label')); ?><em>Untuk tujuan pengiraan bayaran sumbangan modal.(Jika berkenaan)</em>
                        <div class='controls'>
                            <input id='jenis_bayaran_formula' class='input-sm input-s  form-control' type='text' name='jenis_bayaran_formula' maxlength="255" value="<?php echo set_value('jenis_bayaran_formula', isset($jenis_bayaran['formula']) ? $jenis_bayaran['formula'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('formula'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
                        <?php echo form_label('Status' . lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'jenis_bayaran_status_label')); ?>
                        <div class='controls' aria-labelled-by='jenis_bayaran_status_label'>
                            <label class='radio' for='jenis_bayaran_status_option1'>
                                <input id='jenis_bayaran_status_option1' name='jenis_bayaran_status' type='radio'  value='1' <?php echo set_radio('jenis_bayaran_status', '1', isset($jenis_bayaran['status']) && $jenis_bayaran['status'] == 1 ? TRUE : FALSE); ?> />
                                Aktif
                            </label>
                            <label class='radio' for='jenis_bayaran_status_option2'>
                                <input id='jenis_bayaran_status_option2' name='jenis_bayaran_status' type='radio' value='2' <?php echo set_radio('jenis_bayaran_status', '2', isset($jenis_bayaran['status']) && $jenis_bayaran['status'] == 2 ? TRUE : FALSE); ?> />
                                Tak Aktif
                            </label>
                            <span class='help-inline'><?php echo form_error('status'); ?></span>
                        </div>
                    </div>

                    <?php if ($this->uri->segment(4) == 'create') { ?>
                        <div class="form-actions">

                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('jenis_bayaran_action_create'); ?>"  />
                            &nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/settings/jenis_bayaran', lang('jenis_bayaran_cancel'), 'class="btn btn-warning"'); ?>

                        </div>
                    <?php } else { ?>
                        <div class="form-actions">
                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('jenis_bayaran_action_edit'); ?>"  />
                            &nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/settings/jenis_bayaran', lang('jenis_bayaran_cancel'), 'class="btn btn-warning"'); ?>

                            <?php if ($this->auth->has_permission('Jenis_Bayaran.Settings.Delete')) : ?>
                                &nbsp;&nbsp;
                                <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('jenis_bayaran_delete_confirm'))); ?>');">
                                    <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('jenis_bayaran_delete_record'); ?>
                                </button>
                            <?php endif; ?>
                        </div>
                    <?php }
                    ?>

                </fieldset>
                <?php echo form_close(); ?>
            </div>