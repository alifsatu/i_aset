<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($jenis_bayaran))
{
	$jenis_bayaran = (array) $jenis_bayaran;
}
$id = isset($jenis_bayaran['id']) ? $jenis_bayaran['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Jenis Bayaran</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Bayaran'. lang('bf_form_label_required'), 'jenis_bayaran_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='jenis_bayaran_nama' class='input-sm input-s  form-control' type='text' name='jenis_bayaran_nama' maxlength="255" value="<?php echo set_value('jenis_bayaran_nama', isset($jenis_bayaran['nama']) ? $jenis_bayaran['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'jenis_bayaran_status_label') ); ?>
				<div class='controls' aria-labelled-by='jenis_bayaran_status_label'>
					<label class='radio' for='jenis_bayaran_status_option1'>
						<input id='jenis_bayaran_status_option1' name='jenis_bayaran_status' type='radio'  value='option1' <?php echo set_radio('jenis_bayaran_status', 'option1', TRUE); ?> />
						Radio option 1
					</label>
					<label class='radio' for='jenis_bayaran_status_option2'>
						<input id='jenis_bayaran_status_option2' name='jenis_bayaran_status' type='radio' value='option2' <?php echo set_radio('jenis_bayaran_status', 'option2'); ?> />
						Radio option 2
					</label>
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/jenis_bayaran', lang('jenis_bayaran_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Jenis_Bayaran.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('jenis_bayaran_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>