<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['jenis_bayaran_manage']			= 'Manage';
$lang['jenis_bayaran_edit']				= 'Edit';
$lang['jenis_bayaran_true']				= 'True';
$lang['jenis_bayaran_false']				= 'False';
$lang['jenis_bayaran_create']			= 'Save';
$lang['jenis_bayaran_list']				= 'List';
$lang['jenis_bayaran_new']				= 'New';
$lang['jenis_bayaran_edit_text']			= 'Edit this to suit your needs';
$lang['jenis_bayaran_no_records']		= 'There aren\'t any jenis_bayaran in the system.';
$lang['jenis_bayaran_create_new']		= 'Create a new Jenis Bayaran.';
$lang['jenis_bayaran_create_success']	= 'Jenis Bayaran successfully created.';
$lang['jenis_bayaran_create_failure']	= 'There was a problem creating the jenis_bayaran: ';
$lang['jenis_bayaran_create_new_button']	= 'Create New Jenis Bayaran';
$lang['jenis_bayaran_invalid_id']		= 'Invalid Jenis Bayaran ID.';
$lang['jenis_bayaran_edit_success']		= 'Jenis Bayaran successfully saved.';
$lang['jenis_bayaran_edit_failure']		= 'There was a problem saving the jenis_bayaran: ';
$lang['jenis_bayaran_delete_success']	= 'record(s) successfully deleted.';

$lang['jenis_bayaran_purged']	= 'record(s) successfully purged.';
$lang['jenis_bayaran_success']	= 'record(s) successfully restored.';


$lang['jenis_bayaran_delete_failure']	= 'We could not delete the record: ';
$lang['jenis_bayaran_delete_error']		= 'You have not selected any records to delete.';
$lang['jenis_bayaran_actions']			= 'Actions';
$lang['jenis_bayaran_cancel']			= 'Cancel';
$lang['jenis_bayaran_delete_record']		= 'Delete';
$lang['jenis_bayaran_delete_confirm']	= 'Are you sure you want to delete this jenis_bayaran?';
$lang['jenis_bayaran_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['jenis_bayaran_action_edit']		= 'Save';
$lang['jenis_bayaran_action_create']		= 'Create';

// Activities
$lang['jenis_bayaran_act_create_record']	= 'Created record with ID';
$lang['jenis_bayaran_act_edit_record']	= 'Updated record with ID';
$lang['jenis_bayaran_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['jenis_bayaran_column_created']	= 'Created';
$lang['jenis_bayaran_column_deleted']	= 'Deleted';
$lang['jenis_bayaran_column_modified']	= 'Modified';
