<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Jenis_Bayaran.Settings.View');
        $this->load->model('jenis_bayaran_model', null, true);
        $this->lang->load('jenis_bayaran');

        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('jenis_bayaran', 'jenis_bayaran.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->jenis_bayaran_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('jenis_bayaran_delete_success'), 'success');
                } else {
                    Template::set_message(lang('jenis_bayaran_delete_failure') . $this->jenis_bayaran_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('jenis_bayaranfield', $this->input->post('select_field'));
                $this->session->set_userdata('jenis_bayaranfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('jenis_bayaranfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('jenis_bayaranfield');
                $this->session->unset_userdata('jenis_bayaranfvalue');
                $this->session->unset_userdata('jenis_bayaranfname');
                break;
        }

        if ($this->session->userdata('jenis_bayaranfield') != '') {
            $field = $this->session->userdata('jenis_bayaranfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('jenis_bayaranfield') == 'All Fields') {

            $total = $this->jenis_bayaran_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->jenis_bayaran_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->jenis_bayaran_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('jenis_bayaranfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->jenis_bayaran_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->jenis_bayaran_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('jenis_bayaran', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Jenis Bayaran');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_jenis_bayaran SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('jenis_bayaran_success'), 'success');
                } else {
                    Template::set_message(lang('jenis_bayaran_restored_error') . $this->jenis_bayaran_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_jenis_bayaran where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('jenis_bayaran_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('jenis_bayaranfield', $this->input->post('select_field'));
                $this->session->set_userdata('jenis_bayaranfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('jenis_bayaranfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('jenis_bayaranfield');
                $this->session->unset_userdata('jenis_bayaranfvalue');
                $this->session->unset_userdata('jenis_bayaranfname');
                break;
        }


        if ($this->session->userdata('jenis_bayaranfield') != '') {
            $field = $this->session->userdata('jenis_bayaranfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('jenis_bayaranfield') == 'All Fields') {

            $total = $this->jenis_bayaran_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->jenis_bayaran_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-jenis_bayaran_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('jenis_bayaranfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->jenis_bayaran_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('jenis_bayaranfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->jenis_bayaran_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('jenis_bayaran', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Jenis Bayaran');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Jenis Bayaran object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Jenis_Bayaran.Settings.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_jenis_bayaran()) {
                // Log the activity
                log_activity($this->current_user->id, lang('jenis_bayaran_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'jenis_bayaran');

                Template::set_message(lang('jenis_bayaran_create_success'), 'success');
                redirect(SITE_AREA . '/settings/jenis_bayaran');
            } else {
                Template::set_message(lang('jenis_bayaran_create_failure') . $this->jenis_bayaran_model->error, 'error');
            }
        }
        Assets::add_module_js('jenis_bayaran', 'jenis_bayaran.js');

        Template::set('toolbar_title', lang('jenis_bayaran_create') . ' Jenis Bayaran');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Jenis Bayaran data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('jenis_bayaran_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/jenis_bayaran');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Jenis_Bayaran.Settings.Edit');

            if ($this->save_jenis_bayaran('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('jenis_bayaran_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'jenis_bayaran');

                Template::set_message(lang('jenis_bayaran_edit_success'), 'success');
            } else {
                Template::set_message(lang('jenis_bayaran_edit_failure') . $this->jenis_bayaran_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Jenis_Bayaran.Settings.Delete');

            if ($this->jenis_bayaran_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('jenis_bayaran_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'jenis_bayaran');

                Template::set_message(lang('jenis_bayaran_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/jenis_bayaran');
            } else {
                Template::set_message(lang('jenis_bayaran_delete_failure') . $this->jenis_bayaran_model->error, 'error');
            }
        }
        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find($id));
        Template::set('toolbar_title', lang('jenis_bayaran_edit') . ' Jenis Bayaran');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_jenis_bayaran SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('jenis_bayaran_success'), 'success');
                redirect(SITE_AREA . '/settings/jenis_bayaran/deleted');
            } else {

                Template::set_message(lang('jenis_bayaran_error') . $this->jenis_bayaran_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_jenis_bayaran where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('jenis_bayaran_purged'), 'success');
                redirect(SITE_AREA . '/settings/jenis_bayaran/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('jenis_bayaran_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/jenis_bayaran');
        }




        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find($id));

        Assets::add_module_js('jenis_bayaran', 'jenis_bayaran.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Jenis_bayaran');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_jenis_bayaran($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['nama'] = $this->input->post('jenis_bayaran_nama');
        $data['status'] = $this->input->post('jenis_bayaran_status');
        $data['sorting_number'] = $this->input->post('jenis_bayaran_sorting_number');
        $data['formula'] = $this->input->post('jenis_bayaran_formula');

        if ($type == 'insert') {
            $id = $this->jenis_bayaran_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->jenis_bayaran_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
