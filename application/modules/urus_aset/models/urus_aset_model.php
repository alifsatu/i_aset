<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Urus_aset_model extends BF_Model {

	protected $table_name	= "urus_aset";
	protected $key			= "id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "urus_aset_asset_name",
			"label"		=> "Asset Name",
			"rules"		=> "required|max_length[255]"
		),
		array(
			"field"		=> "urus_aset_asset_description",
			"label"		=> "Asset Description",
			"rules"		=> "max_length[5000]"
		),
		array(
			"field"		=> "urus_aset_district_id",
			"label"		=> "District",
			"rules"		=> "required|max_length[11]"
		),
		array(
			"field"		=> "urus_aset_location_id",
			"label"		=> "Location",
			"rules"		=> "required|max_length[11]"
		),
		array(
			"field"		=> "urus_aset_classification_group_id",
			"label"		=> "Asset Classification Group",
			"rules"		=> "required|max_length[11]"
		),
		array(
			"field"		=> "urus_aset_classification_id",
			"label"		=> "Asset Classification",
			"rules"		=> "required|max_length[11]"
		),
		array(
			"field"		=> "urus_aset_serial_no",
			"label"		=> "Serial No.",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "urus_aset_size",
			"label"		=> "Size H.p etc.",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "urus_aset_unit_id",
			"label"		=> "Unit",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "urus_aset_quantity",
			"label"		=> "Quantity",
			"rules"		=> "integer|max_length[30]"
		),
		array(
			"field"		=> "urus_aset_cost_rate",
			"label"		=> "Cost Rate",
			"rules"		=> "is_decimal|max_length[30]"
		),
		array(
			"field"		=> "urus_aset_cost_amount",
			"label"		=> "Cost Amount",
			"rules"		=> "is_decimal|max_length[30]"
		),
		array(
			"field"		=> "urus_aset_year_of_installation",
			"label"		=> "Year Of Installation",
			"rules"		=> "integer|max_length[4]"
		),
		array(
			"field"		=> "urus_aset_remarks",
			"label"		=> "Remarks",
			"rules"		=> "max_length[5000]"
		),
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

}
