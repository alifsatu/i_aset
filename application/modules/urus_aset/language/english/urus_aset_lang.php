<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['urus_aset_manage']			= 'Manage';
$lang['urus_aset_edit']				= 'Edit';
$lang['urus_aset_true']				= 'True';
$lang['urus_aset_false']				= 'False';
$lang['urus_aset_create']			= 'Save';
$lang['urus_aset_list']				= 'List';
$lang['urus_aset_new']				= 'New';
$lang['urus_aset_edit_text']			= 'Edit this to suit your needs';
$lang['urus_aset_no_records']		= 'There aren\'t any urus_aset in the system.';
$lang['urus_aset_create_new']		= 'Create a new Urus Aset.';
$lang['urus_aset_create_success']	= 'Urus Aset successfully created.';
$lang['urus_aset_create_failure']	= 'There was a problem creating the urus_aset: ';
$lang['urus_aset_create_new_button']	= 'Create New Urus Aset';
$lang['urus_aset_invalid_id']		= 'Invalid Urus Aset ID.';
$lang['urus_aset_edit_success']		= 'Urus Aset successfully saved.';
$lang['urus_aset_edit_failure']		= 'There was a problem saving the urus_aset: ';
$lang['urus_aset_delete_success']	= 'record(s) successfully deleted.';

$lang['urus_aset_purged']	= 'record(s) successfully purged.';
$lang['urus_aset_success']	= 'record(s) successfully restored.';


$lang['urus_aset_delete_failure']	= 'We could not delete the record: ';
$lang['urus_aset_delete_error']		= 'You have not selected any records to delete.';
$lang['urus_aset_actions']			= 'Actions';
$lang['urus_aset_cancel']			= 'Cancel';
$lang['urus_aset_delete_record']		= 'Delete';
$lang['urus_aset_delete_confirm']	= 'Are you sure you want to delete this urus_aset?';
$lang['urus_aset_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['urus_aset_action_edit']		= 'Save';
$lang['urus_aset_action_create']		= 'Create';

// Activities
$lang['urus_aset_act_create_record']	= 'Created record with ID';
$lang['urus_aset_act_edit_record']	= 'Updated record with ID';
$lang['urus_aset_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['urus_aset_column_created']	= 'Created';
$lang['urus_aset_column_deleted']	= 'Deleted';
$lang['urus_aset_column_modified']	= 'Modified';
