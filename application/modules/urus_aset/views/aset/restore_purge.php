<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($urus_aset))
{
	$urus_aset = (array) $urus_aset;
}
$id = isset($urus_aset['id']) ? $urus_aset['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Urus Aset</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('asset_name') ? 'error' : ''; ?>">
				<?php echo form_label('Asset Name'. lang('bf_form_label_required'), 'urus_aset_asset_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_asset_name' class='input-sm input-s  form-control' type='text' name='urus_aset_asset_name' maxlength="255" value="<?php echo set_value('urus_aset_asset_name', isset($urus_aset['asset_name']) ? $urus_aset['asset_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('asset_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('asset_description') ? 'error' : ''; ?>">
				<?php echo form_label('Asset Description', 'urus_aset_asset_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'urus_aset_asset_description', 'id' => 'urus_aset_asset_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('urus_aset_asset_description', isset($urus_aset['asset_description']) ? $urus_aset['asset_description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('asset_description'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('urus_aset_district_id', $options, set_value('urus_aset_district_id', isset($urus_aset['district_id']) ? $urus_aset['district_id'] : ''), 'District'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('urus_aset_location_id', $options, set_value('urus_aset_location_id', isset($urus_aset['location_id']) ? $urus_aset['location_id'] : ''), 'Location'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('urus_aset_classification_group_id', $options, set_value('urus_aset_classification_group_id', isset($urus_aset['classification_group_id']) ? $urus_aset['classification_group_id'] : ''), 'Asset Classification Group'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('urus_aset_classification_id', $options, set_value('urus_aset_classification_id', isset($urus_aset['classification_id']) ? $urus_aset['classification_id'] : ''), 'Asset Classification'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('serial_no') ? 'error' : ''; ?>">
				<?php echo form_label('Serial No.', 'urus_aset_serial_no', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_serial_no' class='input-sm input-s  form-control' type='text' name='urus_aset_serial_no' maxlength="255" value="<?php echo set_value('urus_aset_serial_no', isset($urus_aset['serial_no']) ? $urus_aset['serial_no'] : '');?>" />
					<span class='help-inline'><?php echo form_error('serial_no'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('size') ? 'error' : ''; ?>">
				<?php echo form_label('Size H.p etc.', 'urus_aset_size', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_size' class='input-sm input-s  form-control' type='text' name='urus_aset_size' maxlength="255" value="<?php echo set_value('urus_aset_size', isset($urus_aset['size']) ? $urus_aset['size'] : '');?>" />
					<span class='help-inline'><?php echo form_error('size'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('unit_id') ? 'error' : ''; ?>">
				<?php echo form_label('Unit'. lang('bf_form_label_required'), 'urus_aset_unit_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_unit_id' class='input-sm input-s  form-control' type='text' name='urus_aset_unit_id' maxlength="11" value="<?php echo set_value('urus_aset_unit_id', isset($urus_aset['unit_id']) ? $urus_aset['unit_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('unit_id'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('quantity') ? 'error' : ''; ?>">
				<?php echo form_label('Quantity', 'urus_aset_quantity', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_quantity' class='input-sm input-s  form-control' type='text' name='urus_aset_quantity' maxlength="30" value="<?php echo set_value('urus_aset_quantity', isset($urus_aset['quantity']) ? $urus_aset['quantity'] : '');?>" />
					<span class='help-inline'><?php echo form_error('quantity'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('cost_rate') ? 'error' : ''; ?>">
				<?php echo form_label('Cost Rate', 'urus_aset_cost_rate', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_cost_rate' class='input-sm input-s  form-control' type='text' name='urus_aset_cost_rate' maxlength="30" value="<?php echo set_value('urus_aset_cost_rate', isset($urus_aset['cost_rate']) ? $urus_aset['cost_rate'] : '');?>" />
					<span class='help-inline'><?php echo form_error('cost_rate'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('cost_amount') ? 'error' : ''; ?>">
				<?php echo form_label('Cost Amount', 'urus_aset_cost_amount', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_cost_amount' class='input-sm input-s  form-control' type='text' name='urus_aset_cost_amount' maxlength="30" value="<?php echo set_value('urus_aset_cost_amount', isset($urus_aset['cost_amount']) ? $urus_aset['cost_amount'] : '');?>" />
					<span class='help-inline'><?php echo form_error('cost_amount'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('year_of_installation') ? 'error' : ''; ?>">
				<?php echo form_label('Year Of Installation', 'urus_aset_year_of_installation', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='urus_aset_year_of_installation' class='input-sm input-s  form-control' type='text' name='urus_aset_year_of_installation' maxlength="4" value="<?php echo set_value('urus_aset_year_of_installation', isset($urus_aset['year_of_installation']) ? $urus_aset['year_of_installation'] : '');?>" />
					<span class='help-inline'><?php echo form_error('year_of_installation'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('remarks') ? 'error' : ''; ?>">
				<?php echo form_label('Remarks', 'urus_aset_remarks', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'urus_aset_remarks', 'id' => 'urus_aset_remarks', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('urus_aset_remarks', isset($urus_aset['remarks']) ? $urus_aset['remarks'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('remarks'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/aset/urus_aset', lang('urus_aset_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Urus_Aset.Aset.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('urus_aset_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>