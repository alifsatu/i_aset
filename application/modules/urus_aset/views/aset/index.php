<?php

$num_columns	= 17;
$can_delete	= $this->auth->has_permission('Urus_Aset.Aset.Delete');
$can_edit		= $this->auth->has_permission('Urus_Aset.Aset.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

$user_list = get_user_name_list();
$daerah_list = get_daerah_name_list();
$location_list = get_location_name_list();
$asset_classification_group_list = get_asset_classification_group_list();
$asset_classification_list = get_asset_classification_list();
$aset_unit_id_list = get_aset_unit_id_list();

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>Urus Aset</h4></div>
  <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">


<? if ( $this->session->userdata('urus_asetfield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('urus_asetfield')?>"><?=$this->session->userdata('urus_asetfield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_urus_aset')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('urus_asetfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('urus_asetfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('urus_asetfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_asset_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=asset_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'asset_name') ? 'desc' : 'asc'); ?>'>
                    Asset Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_asset_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=asset_description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'asset_description') ? 'desc' : 'asc'); ?>'>
                    Asset Description</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_district_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=district_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'district_id') ? 'desc' : 'asc'); ?>'>
                    District</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_location_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=location_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'location_id') ? 'desc' : 'asc'); ?>'>
                    Location</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_classification_group_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=classification_group_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'classification_group_id') ? 'desc' : 'asc'); ?>'>
                    Asset Classification Group</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_classification_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=classification_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'classification_id') ? 'desc' : 'asc'); ?>'>
                    Asset Classification</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_serial_no') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=serial_no&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'serial_no') ? 'desc' : 'asc'); ?>'>
                    Serial No.</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_size') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=size&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'size') ? 'desc' : 'asc'); ?>'>
                    Size H.p etc.</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_unit_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=unit_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'unit_id') ? 'desc' : 'asc'); ?>'>
                    Unit</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_quantity') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=quantity&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'quantity') ? 'desc' : 'asc'); ?>'>
                    Quantity</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_cost_rate') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=cost_rate&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'cost_rate') ? 'desc' : 'asc'); ?>'>
                    Cost Rate (RM)</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_cost_amount') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=cost_amount&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'cost_amount') ? 'desc' : 'asc'); ?>'>
                    Cost Amount (RM)</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_year_of_installation') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=year_of_installation&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'year_of_installation') ? 'desc' : 'asc'); ?>'>
                    Year Of Installation</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_remarks') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/urus_aset?sort_by=remarks&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'remarks') ? 'desc' : 'asc'); ?>'>
                    Remarks</a></th>
					<th><?php echo lang("urus_aset_column_created"); ?></th>
					<th><?php echo lang("urus_aset_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('urus_aset_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/aset/urus_aset/edit/' . $record->id, $record->asset_name ); ?>&nbsp;<i class="fa fa-pencil"></i> </td>
				<?php else : ?>
					<td><?php e($record->asset_name); ?></td>
				<?php endif; ?>
                                        

					<td><?php e($record->asset_description) ?></td>
					<td><?php e($daerah_list[$record->district_id]) ?></td>
					<td><?php e($location_list[$record->location_id]) ?></td>
					<td><?php e($asset_classification_group_list[$record->classification_group_id]) ?></td>
					<td><?php e($asset_classification_list[$record->classification_id]['name']) ?></td>
					<td><?php e($record->serial_no) ?></td>
					<td><?php e($record->size) ?></td>
					<td><?php e($aset_unit_id_list[$record->unit_id]) ?></td>
					<td><?php e($record->quantity) ?></td>
					<td><?php e($record->cost_rate) ?></td>
					<td><?php e($record->cost_amount) ?></td>
					<td><?php e($record->year_of_installation) ?></td>
					<td><?php e($record->remarks) ?></td>
					<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
					<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
