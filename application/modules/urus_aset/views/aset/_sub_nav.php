
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/aset/urus_aset') ?>"style="border-radius:0"  id="list"><?php echo lang('urus_aset_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Urus_Aset.Aset.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/aset/urus_aset/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('urus_aset_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Urus_Aset.Aset.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/aset/urus_aset/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
