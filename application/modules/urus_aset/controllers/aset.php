<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * aset controller
 */
class aset extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Urus_Aset.Aset.View');
        $this->load->model('urus_aset_model', null, true);
        $this->load->model('daftar_unit/daftar_unit_model', null, true);
        $this->load->model('daftar_lokasi/daftar_lokasi_model', null, true);
        $this->load->model('daftar_classification_group/daftar_classification_group_model', null, true);
        $this->load->model('daftar_classification/daftar_classification_model', null, true);
        $this->load->model('daftar_daerah/daftar_daerah_model', null, true);
        $this->lang->load('urus_aset');


        Template::set_block('sub_nav', 'aset/_sub_nav');

        Assets::add_module_js('urus_aset', 'urus_aset.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->urus_aset_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('urus_aset_delete_success'), 'success');
                } else {
                    Template::set_message(lang('urus_aset_delete_failure') . $this->urus_aset_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('urus_asetfield', $this->input->post('select_field'));
                $this->session->set_userdata('urus_asetfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('urus_asetfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('urus_asetfield');
                $this->session->unset_userdata('urus_asetfvalue');
                $this->session->unset_userdata('urus_asetfname');
                break;
        }

        if ($this->session->userdata('urus_asetfield') != '') {
            $field = $this->session->userdata('urus_asetfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('urus_asetfield') == 'All Fields') {

            $total = $this->urus_aset_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->urus_aset_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('urus_asetfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->urus_aset_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('urus_asetfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->urus_aset_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('urus_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->urus_aset_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);

        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('urus_aset', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Urus Aset');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_urus_aset SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('urus_aset_success'), 'success');
                } else {
                    Template::set_message(lang('urus_aset_restored_error') . $this->urus_aset_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_urus_aset where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('urus_aset_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('urus_asetfield', $this->input->post('select_field'));
                $this->session->set_userdata('urus_asetfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('urus_asetfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('urus_asetfield');
                $this->session->unset_userdata('urus_asetfvalue');
                $this->session->unset_userdata('urus_asetfname');
                break;
        }


        if ($this->session->userdata('urus_asetfield') != '') {
            $field = $this->session->userdata('urus_asetfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('urus_asetfield') == 'All Fields') {

            $total = $this->urus_aset_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->urus_aset_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('urus_asetfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-urus_aset_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('urus_asetfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('urus_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->urus_aset_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('urus_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->urus_aset_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('urus_aset', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Urus Aset');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Urus Aset object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Urus_Aset.Aset.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_urus_aset()) {
                // Log the activity
                log_activity($this->current_user->id, lang('urus_aset_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'urus_aset');

                Template::set_message(lang('urus_aset_create_success'), 'success');
                redirect(SITE_AREA . '/aset/urus_aset');
            } else {
                Template::set_message(lang('urus_aset_create_failure') . $this->urus_aset_model->error, 'error');
            }
        }
        Assets::add_module_js('urus_aset', 'urus_aset.js');


        $this->load->model('daftar_unit/daftar_unit_model', null, true);
        Template::set('aset_unit_id_list', $this->daftar_unit_model->find_all_by(array('deleted' => '0')));



        Template::set('toolbar_title', lang('urus_aset_create') . ' Urus Aset');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Urus Aset data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('urus_aset_invalid_id'), 'error');
            redirect(SITE_AREA . '/aset/urus_aset');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Urus_Aset.Aset.Edit');

            if ($this->save_urus_aset('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('urus_aset_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'urus_aset');

                Template::set_message(lang('urus_aset_edit_success'), 'success');
            } else {
                Template::set_message(lang('urus_aset_edit_failure') . $this->urus_aset_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Urus_Aset.Aset.Delete');

            if ($this->urus_aset_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('urus_aset_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'urus_aset');

                Template::set_message(lang('urus_aset_delete_success'), 'success');

                redirect(SITE_AREA . '/aset/urus_aset');
            } else {
                Template::set_message(lang('urus_aset_delete_failure') . $this->urus_aset_model->error, 'error');
            }
        }


        $this->load->model('daftar_unit/daftar_unit_model', null, true);
        Template::set('aset_unit_id_list', $this->daftar_unit_model->find_all_by(array('deleted' => '0')));
        Template::set('urus_aset', $this->urus_aset_model->find($id));
        Template::set('toolbar_title', lang('urus_aset_edit') . ' Urus Aset');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_urus_aset SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('urus_aset_success'), 'success');
                redirect(SITE_AREA . '/aset/urus_aset/deleted');
            } else {

                Template::set_message(lang('urus_aset_error') . $this->urus_aset_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_urus_aset where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('urus_aset_purged'), 'success');
                redirect(SITE_AREA . '/aset/urus_aset/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('urus_aset_invalid_id'), 'error');
            redirect(SITE_AREA . '/aset/urus_aset');
        }




        Template::set('urus_aset', $this->urus_aset_model->find($id));

        Assets::add_module_js('urus_aset', 'urus_aset.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Urus_aset');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_urus_aset($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['asset_name'] = $this->input->post('urus_aset_asset_name');
        $data['asset_description'] = $this->input->post('urus_aset_asset_description');
        $data['district_id'] = $this->input->post('urus_aset_district_id');
        $data['location_id'] = $this->input->post('urus_aset_location_id');
        $data['classification_group_id'] = $this->input->post('urus_aset_classification_group_id');
        $data['classification_id'] = $this->input->post('urus_aset_classification_id');
        $data['serial_no'] = $this->input->post('urus_aset_serial_no');
        $data['size'] = $this->input->post('urus_aset_size');
        $data['unit_id'] = $this->input->post('urus_aset_unit_id');
        $data['quantity'] = $this->input->post('urus_aset_quantity');
        $data['cost_rate'] = $this->input->post('urus_aset_cost_rate');
        $data['cost_amount'] = $this->input->post('urus_aset_cost_amount');
        $data['year_of_installation'] = $this->input->post('urus_aset_year_of_installation');
        $data['remarks'] = $this->input->post('urus_aset_remarks');

        if ($type == 'insert') {
            $id = $this->urus_aset_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->urus_aset_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
