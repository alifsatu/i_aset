<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['work_flow_manage']			= 'Manage';
$lang['work_flow_edit']				= 'Edit';
$lang['work_flow_true']				= 'True';
$lang['work_flow_false']				= 'False';
$lang['work_flow_create']			= 'Save';
$lang['work_flow_list']				= 'List';
$lang['work_flow_new']				= 'New';
$lang['work_flow_edit_text']			= 'Edit this to suit your needs';
$lang['work_flow_no_records']		= 'There aren\'t any work_flow in the system.';
$lang['work_flow_create_new']		= 'Create a new Work Flow.';
$lang['work_flow_create_success']	= 'Work Flow successfully created.';
$lang['work_flow_create_failure']	= 'There was a problem creating the work_flow: ';
$lang['work_flow_create_new_button']	= 'Create New Work Flow';
$lang['work_flow_invalid_id']		= 'Invalid Work Flow ID.';
$lang['work_flow_edit_success']		= 'Work Flow successfully saved.';
$lang['work_flow_edit_failure']		= 'There was a problem saving the work_flow: ';
$lang['work_flow_delete_success']	= 'record(s) successfully deleted.';

$lang['work_flow_purged']	= 'record(s) successfully purged.';
$lang['work_flow_success']	= 'record(s) successfully restored.';


$lang['work_flow_delete_failure']	= 'We could not delete the record: ';
$lang['work_flow_delete_error']		= 'You have not selected any records to delete.'; 
$lang['work_flow_actions']			= 'Actions';
$lang['work_flow_cancel']			= 'Cancel';
$lang['work_flow_delete_record']		= 'Delete';
$lang['work_flow_delete_confirm']	= 'Are you sure you want to delete this work flow?';
$lang['work_flow_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['work_flow_action_edit']		= 'Save';
$lang['work_flow_action_create']		= 'Create';
$lang['workflow_create_error']	= 'Please fix the following errors:';
$lang['workflow_by_role']	= 'By Role';
$lang['workflow_by_user']	= 'By User';
$lang['workflow_drag']	= "Drag & Reposition Approver's Hierarchy";

// Activities
$lang['work_flow_act_create_record']	= 'Created record with ID';
$lang['work_flow_act_edit_record']	= 'Updated record with ID';
$lang['work_flow_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['work_flow_column_created']	= 'Created';
$lang['work_flow_column_deleted']	= 'Deleted';
$lang['work_flow_column_modified']	= 'Modified';


//index 
$lang['work_flow']	= 'Work Flow';
$lang['module_name']	= 'Module Name';
$lang['Initiator']	= 'Initiator';
$lang['Escalation']	= 'Escalation';
$lang['final_approver']	= 'Final Approver';
$lang['Approvers']	= 'Approvers';
$lang['role_name']	= 'Role Name';
$lang['Escalation_hrs']	= 'Escalation (hrs)';
$lang['work_flow_records']	= 'No records found that match your selection.';
$lang['work_flow_showing']	= 'Showing';
$lang['work_flow_of']	= 'of';
