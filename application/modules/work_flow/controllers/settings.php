<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {

    //--------------------------------------------------------------------
    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->auth->restrict('Work_Flow.Settings.View');
        $this->load->model('work_flow_model', null, true);
        $this->lang->load('work_flow');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Template::set_block('sub_nav', 'settings/_sub_nav');
        Assets::add_module_js('work_flow', 'work_flow.js');
    }

    //--------------------------------------------------------------------
    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());
        $user_id = $this->auth->user_id();
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->work_flow_model->delete($pid);
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('work_flow_delete_success'), 'success');
                } else {
                    Template::set_message(lang('work_flow_delete_failure') . $this->work_flow_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sapfield', $this->input->post('select_field'));
                $this->session->set_userdata('sapfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sapfname', $this->input->post('field_name'));
                break;
            case 'Reset':
                $this->session->unset_userdata('sapfield');
                $this->session->unset_userdata('sapfvalue');
                $this->session->unset_userdata('sapfname');
                break;
        }
        if ($this->session->userdata('sapfield') != '') {
            $field = $this->session->userdata('sapfield');
        } else {
            $field = 'id';
        }
        if ($this->session->userdata('sapfield') == 'All Fields') {

            $this->db->select("FOUND_ROWS() as num_rows");
            $this->db->from("intg_work_flow");
            $this->db->where("deleted", "0");
            $this->db->where('company_id', $this->current_user->company_id);
            $this->db->where("(
      `work_flow_module_id`   like ('%" . $this->session->userdata('sapfvalue') . "%') OR 
   `work_flow_doc_generator` like ('%" . $this->session->userdata('sapfvalue') . "%') 
   )", NULL, FALSE);
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $total = $this->db->get();
        } else {

            $this->db->select("FOUND_ROWS() as num_rows");
            $this->db->from("intg_work_flow");
            $this->db->where("deleted", "0");
            $this->db->where('company_id', $this->auth->company_id());
            $this->db->like($field, $this->session->userdata('sapfvalue'), 'both');
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $total = $this->db->get();
        }
        //$records = $this->work_flow_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total->num_rows;
        $this->pager['per_page'] = 15;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;



        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('sapfield') == 'All Fields') {
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $records = $this->work_flow_model
                    ->where('deleted', '0')
                    ->where('company_id', $this->auth->company_id())
                    ->where("(
       `work_flow_module_id`   like ('%" . $this->session->userdata('sapfvalue') . "%') OR 
   `work_flow_doc_generator` like ('%" . $this->session->userdata('sapfvalue') . "%') 
   )", NULL, FALSE)
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");
            $records = $this->work_flow_model
                    ->where('deleted', '0')
                    ->where('company_id', $this->auth->company_id())
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

//echo $this->db->last_query();
//$records = $this->work_flow_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);

        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('work_flow', 'style.css');




        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Work Flow');
        Template::render();
    }

    //--------------------------------------------------------------------

    public function showroleusers() {
        Template::render();
    }

    public function role_user_dropdown() {
        Template::render();
    }

    public function checkrole_user() {
        Template::render();
    }

    public function concept() {
        Assets::add_module_js('work_flow', 'flare.json');

        Assets::add_js(Template::theme_url('js/d3/d3.v3.min.js'));
        Assets::add_js(Template::theme_url('js/d3/dndTree.js'));



        Template::render();
    }

    public function flare() {
        $this->load->view("flare.json");
    }

    public function populatelistbox() {
        Template::render();
    }

    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();

        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_work_flow SET deleted = 0 where id = ' . $pid . '');
                }


                if ($result) {
                    Template::set_message(lang('work_flow_success'), 'success');
                } else {
                    Template::set_message(lang('work_flow_restored_error') . $this->work_flow_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_work_flow where id = ' . $pid . '');
                }
                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('work_flow_purged'), 'success');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sapfield', $this->input->post('select_field'));
                $this->session->set_userdata('sapfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sapfname', $this->input->post('field_name'));
                break;
            case 'Reset':
                $this->session->unset_userdata('sapfield');
                $this->session->unset_userdata('sapfvalue');
                $this->session->unset_userdata('sapfname');
                break;
        }
        if ($this->session->userdata('sapfield') != '') {
            $field = $this->session->userdata('sapfield');
        } else {
            $field = 'id';
        }
        if ($this->session->userdata('sapfield') == 'All Fields') {

            $this->db->select("FOUND_ROWS() as num_rows");
            $this->db->from("intg_work_flow");
            $this->db->where("deleted", "1");
            $this->db->where('company_id', $this->auth->company_id());
            $this->db->where("(
      `work_flow_module_id`   like ('%" . $this->session->userdata('sapfvalue') . "%') OR 
   `work_flow_doc_generator` like ('%" . $this->session->userdata('sapfvalue') . "%') 
   )", NULL, FALSE);
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $total = $this->db->get();
        } else {

            $this->db->select("FOUND_ROWS() as num_rows");
            $this->db->from("intg_work_flow");
            $this->db->where("deleted", "1");
            $this->db->where('company_id', $this->auth->company_id());
            $this->db->like($field, $this->session->userdata('sapfvalue'), 'both');
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $total = $this->db->get();
        }
//$records = $this-work_flow_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total->num_rows;
        $this->pager['per_page'] = 5;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;


        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('sapfield') == 'All Fields') {
            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");

            $records = $this->work_flow_model
                    ->where('deleted', '1')
                    ->where('company_id', $current_user->company_id)
                    ->where("(
       `work_flow_module_id`   like ('%" . $this->session->userdata('sapfvalue') . "%') OR 
   `work_flow_doc_generator` like ('%" . $this->session->userdata('sapfvalue') . "%') 
   )", NULL, FALSE)
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $this->db->group_by("work_flow_module_id,work_flow_doc_generator");
            $records = $this->work_flow_model
                    ->where('deleted', '1')
                    ->where('company_id', $this->current_user->company_id)
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
        //$records = $this->work_flow_model->find_all();
        //echo $this->db->last_query();
        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('work_flow', 'style.css');
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Work Flow');
        Template::render();
    }

    //--------------------------------------------------------------------
    /**
     * Creates a Work Flow object.
     *
     * @return void
     */
    public function create() {
   
        $this->auth->restrict('Work_Flow.Settings.Create');
        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_work_flow()) {
                // Log the activity
                log_activity($this->current_user->id, lang('work_flow_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'work_flow');
                Template::set_message(lang('work_flow_create_success'), 'success');
                redirect(SITE_AREA . '/settings/work_flow');
            } else {
                Template::set_message(lang('work_flow_create_failure') . $this->work_flow_model->error, 'error');
            }
        }
        Assets::add_module_js('work_flow', 'work_flow.js');
        Template::set('toolbar_title', lang('work_flow_create') . ' Work Flow');
        Template::render();
    }

    //--------------------------------------------------------------------
    /**
     * Allows editing of Work Flow data.
     *
     * @return void
     */
    public function edit() {
//		print_r($_POST);exit();

        $id = $this->uri->segment(5);
        if (empty($id)) {
            Template::set_message(lang('work_flow_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/work_flow');
        }
        if (isset($_POST['save'])) {
            $this->auth->restrict('Work_Flow.Settings.Edit');
            if ($this->save_work_flow('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('work_flow_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'work_flow');
                Template::set_message(lang('work_flow_edit_success'), 'success');
                redirect(SITE_AREA . '/settings/work_flow');
            } else {
                Template::set_message(lang('work_flow_edit_failure') . $this->work_flow_model->error, 'error');
            }
        }
        Template::set('work_flow', $this->work_flow_model->find($id));

        Template::set('toolbar_title', lang('work_flow_edit') . ' Work Flow');
        Template::render();
    }

    //--------------------------------------------------------------------
    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_work_flow SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('work_flow_success'), 'success');
                //redirect(SITE_AREA .'/settings/work_flow/deleted');
            } else {

                Template::set_message(lang('work_flow_error') . $this->work_flow_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_work_flow where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('work_flow_purged'), 'success');
                //redirect(SITE_AREA .'/settings/work_flow/deleted');
            }
        }

        if (empty($id)) {
            Template::set_message(lang('work_flow_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/work_flow');
        }



        Template::set('work_flow', $this->work_flow_model->find($id));

        Assets::add_module_js('work_flow', 'work_flow.js');
        Template::set('toolbar_title', 'Restore / Purge' . ' Work_flow');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------
    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_work_flow($type = 'insert', $id = 0) {

        if ($type == 'update') {
            $_POST['id'] = $id;
            $this->db->delete('intg_work_flow', array('work_flow_module_id' => $this->input->post('work_flow_module_id'), 'work_flow_doc_generator' => $this->input->post('work_flow_doc_generator')));
        }

        $this->form_validation->set_rules('work_flow_module_id', 'Module Name', 'required|max_length[20]');
        $this->form_validation->set_rules('work_flow_doc_generator', 'Initiator', 'required|max_length[20]');
        $this->form_validation->set_rules('chk', 'Approvers', 'required');


        if ($this->form_validation->run() === FALSE) {
            return FALSE;
        }
        // make sure we only pass in the fields we want

        foreach ($this->input->post('chk') as $chknum) {
            $chkval = explode(":", $chknum);
            $data = array();
            $arr = implode(",", $this->input->post('work_flow' . $chkval[1]));

            $data['company_id'] = $this->auth->company_id();

            $data['flow_type'] = $this->input->post('byroleuser');
            $data['work_flow_module_id'] = $this->input->post('work_flow_module_id');
            $data['work_flow_doc_generator'] = $this->input->post('work_flow_doc_generator');
            //$data['work_flow_fappr']        = $row->role_name;
            //$data['rolename']        = $row->role_name;
            $data['rolename'] = $chkval[0];
            $data['work_flow_rule'] = $this->input->post('work_flow_rule');
            $data['work_flow_approvers'] = $arr;
            $data['escalation'] = $this->input->post('escalation' . $chkval[1]);
            
//            echo "<pre>";
//            print_r($data);
//            echo "</pre>";
//            exit;
            if ($type == 'insert') {
                $id = $this->work_flow_model->insert($data);
                if (is_numeric($id)) {
                    $return = $id;
                    $this->db->query("UPDATE intg_work_flow SET work_flow_fappr = '" . $chkval[0] . "' WHERE  work_flow_module_id = '" . $this->input->post('work_flow_module_id') . "' AND work_flow_doc_generator = '" . $this->input->post('work_flow_doc_generator') . "'");
                    //echo $this->db->last_query();
                } else {
                    $return = FALSE;
                }
            } else if ($type == 'update') {


                //$this->db->query("UPDATE bf_work_flow SET work_flow_fappr = '".$chkval[0]."' WHERE  work_flow_module_id = '19' AND work_flow_doc_generator = '".$this->input->post('work_flow_doc_generator')."'");
                //$return = $this->work_flow_model->update($id, $data);

                $return = $this->work_flow_model->insert($data);
                $this->db->query("UPDATE intg_work_flow SET work_flow_fappr = '" . $chkval[0] . "' WHERE  work_flow_module_id = '" . $this->input->post('work_flow_module_id') . "' AND work_flow_doc_generator = '" . $this->input->post('work_flow_doc_generator') . "'");
                //$return = $id;
			//echo $this->db->last_query()."<br>";
            }
        }
        return $return;
    }

    //--------------------------------------------------------------------
}
