<script>
function showrows(val)
{
	$("#row"+val).slideToggle();
}
$(document).ready(function() {  
 
	 
	/* $('.input-mini').live('change',function(){	
	 if(!$.isNumeric($(this).val())){ alert ("only numbers are allowed") }	 else {             
        var colors= 'rgb(' + (Math.floor(Math.random() * 128)) + ',' + (Math.floor(Math.random() * 128)) + ',' + (Math.floor(Math.random() * 128)) + ')';  
		$(this).css("border",colors +" solid 4px");
		var sm = $(this).attr("rowid");
		var lvl = $(this).val();	
			
		$.post("<?php echo site_url(SITE_AREA .'/settings/work_flow/updateescalate') ?>", {rowid:sm,valuea:lvl}, function(data){} )
	 }
})*/
})
</script>
<script>


$(document).ready(function() {
	
	
  $("#select_field").change(function(){
    $.getJSON("<?php echo site_url(SITE_AREA .'/settings/work_flow/populatelistbox') ?>",{deptid: $(this).val(), ajax: 'true'}, function(j){
      var options = '';
	  
	  if ( j[0].nickname ) {
      for (var i = 0; i < j.length; i++) {
		  
        options += '<option value="' + j[i].id + '">' + j[i].nickname + ' ( Active:' + j[i].modules_is_active + ')</option>';
      }
	  }
	  
	    if ( j[0].username ) {
      for (var i = 0; i < j.length; i++) {
		  
        options += '<option value="' + j[i].id + '">' + j[i].display_name + ' (' + j[i].role_name + ')</option>';
      }
	  }
	    $("#field_value").html(options);
		
		
	  $('#field_value').attr('size', function() {
   for (var i = 0; i < j.length; i++) {
        options += '<option value="' + j[i].id + '">' + j[i].nickname + ' (' + j[i].modules_is_active + ')</option>';
      }
	  });
  
	  
    })
  })
})
</script>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 


echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">
 <div class="col-md-5">  <h4><? echo lang('work_flow')?></h4></div>
    <div class="col-md-3">

<select name="select_field" id="select_field"  class="form-control m-b selecta pull-right" placeholder="select first">
<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option></select>
	

</div><div class="col-md-4"><div class="input-group"> <select  name="field_value" id="field_value" class="form-control selecta" placeholder="select next" style="width:250px"><option value="<?=$this->session->userdata('capfvalue')?>"></option></select>
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">

<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
   </span>
                        </div></div>
      </div>
	  <?php echo form_close(); ?>    

    
     <div class="table-responsive"> 
	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Work_Flow.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
				
					 <th<?php if ($this->input->get('sort_by') == '_work_flow_module_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_module_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow_module_id') ? 'desc' : 'asc'); ?>'>
					<th <?php if ($this->input->get('sort_by') == 'work_flow'.'_work_flow_doc_generator') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
					<th <?php if ($this->input->get('sort_by') == 'work_flow'.'_initiator') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_initiator&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow'.'_initiator') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('Initiator')?></a></th>
					<th></th>
					<th <?php if ($this->input->get('sort_by') == 'work_flow'.'_escalation') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_escalation&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow'.'_escalation') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('Escalation')?></a></th>
					
					<!--<th <?php if ($this->input->get('sort_by') == 'work_flow'.'_created_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_created_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow'.'_created_on') ? 'desc' : 'asc'); ?>'>
                    Created On</a></th>
					<th <?php if ($this->input->get('sort_by') == 'work_flow'.'_modified_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_modified_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow'.'_modified_on') ? 'desc' : 'asc'); ?>'>
                    Modified On</a></th>-->
					<th<?php if ($this->input->get('sort_by') == '_work_flow_fappr') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/work_flow?sort_by=work_flow_fappr&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow_fappr') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('final_approver')?></a></th>
                   
					
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Work_Flow.Settings.Restore_purge')) : ?>
				<tr>
					<td colspan="14">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Work_Flow.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
			
			
				<td><?php 
				 $query = $this->db->query('SELECT modules_module_name,nickname FROM intg_modules WHERE id = '.$record->work_flow_module_id .'');

$row = $query->row(); echo $row->nickname; ?>
				</td>
				<td><?php echo $this->auth->display_name_by_id($record->work_flow_doc_generator)?></td>
				
				 <td><a href="javascript:onclick=showrows('<?=$record->id?>')"><i class="fa fa-arrow-circle-o-right"></i></a></td>
                <td ><div id="row<?=$record->id?>" style="display:none"><table class="table table-bordered"><tr><td><strong><? echo lang('Approvers')?></strong></td><td><strong><? echo lang('role_name')?></strong></td><td><strong><? echo lang('Escalation_hrs')?></strong></td></tr><?php 
				$querymain = $this->db->query('SELECT id,work_flow_approvers,escalation,rolename FROM intg_work_flow WHERE work_flow_doc_generator ="'.$record->work_flow_doc_generator.'" AND work_flow_module_id	="'.$record->work_flow_module_id.'" order by id asc');
				//echo $this->db->last_query();
				
				foreach ( $querymain->result() as $rowmain )
				{
				echo "<tr><td>";
				$query = $this->db->query('SELECT display_name FROM intg_users WHERE id IN ( '.$rowmain->work_flow_approvers.')');	
				//echo $this->db->last_query();
		foreach ($query->result_array() as $row)
{
   echo $row['display_name'];   
}	echo "</td>";echo "<td>";			
				echo $rowmain->rolename; echo "</td><td>"; if ( $record->work_flow_fappr!=$rowmain->rolename ) { ?>
                
                <input type="text" class="input-mini row<?=$record->id?>" rowid="<?=$rowmain->id?>"  value="<?=$rowmain->escalation?>" /><? echo "</td></tr>"; } } ?></table></div></td>
				
				<td><?php e($record->work_flow_fappr) ?></td>	
				
				
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="14"><? echo lang('work_flow_records')?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         <?php echo form_close(); ?> 
  
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm"><? echo lang('work_flow_showing')?> <?=$offset+1?> - <? echo $rowcount+$offset?> <? echo lang('work_flow_of')?> <?  echo $total->num_rows(); ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div> </section>
