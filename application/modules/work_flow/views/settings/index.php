<script>
    function showrows(val)
    {
        $("#row" + val).slideToggle();
    }

</script>
<script>


    $(document).ready(function () {
        $("#select_field").change(function () {
            $.getJSON("<?php echo site_url(SITE_AREA . '/settings/work_flow/populatelistbox') ?>", {deptid: $(this).val(), ajax: 'true'}, function (j) {
                var options = '';

                if (j[0].nickname) {
                    for (var i = 0; i < j.length; i++) {

                        options += '<option value="' + j[i].id + '">' + j[i].nickname + ' ( Active:' + j[i].modules_is_active + ')</option>';
                    }
                }

                if (j[0].username) {
                    for (var i = 0; i < j.length; i++) {

                        options += '<option value="' + j[i].id + '">' + j[i].display_name + ' (' + j[i].role_name + ')</option>';
                    }
                }
                $("#field_value").html(options);


                $('#field_value').attr('size', function () {
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].id + '">' + j[i].nickname + ' (' + j[i].modules_is_active + ')</option>';
                    }
                });
            });
        });
    });
</script>
<?php
$num_columns = 11;
$can_delete = $this->auth->has_permission('Work_Flow.Settings.Delete');
$can_edit = $this->auth->has_permission('Work_Flow.Settings.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">
            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>

            <div class="row">
                <div class="col-md-5">  <h4><?php echo lang('work_flow') ?></h4></div>

                <div class="col-md-3">     
                    <select name="select_field" id="select_field" class="form-control m-b selecta"  placeholder="select first">
                        <option value="<?= $this->session->userdata('capfield') ?>"><?= $this->session->userdata('capfname') ?></option>
                        <option value="work_flow_module_id"><?php echo lang('module_name') ?></option>
                        <option value="work_flow_doc_generator"><?php echo lang('Initiator') ?></option>

                    </select>

                </div><div class="col-md-4"><div class="input-group">

                        <select  name="field_value" id="field_value" class="form-control selecta" placeholder="select next" style="width:250px"><option value="<?= $this->session->userdata('capfvalue') ?>"></option></select>
                        <input type="hidden" name="field_name" id="field_name" value="<?= $this->session->userdata('capfname') ?>"/>

                        <span class="input-group-btn">
                            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
                            <button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
                        </span>
                    </div></div>
            </div>
            <?php echo form_close(); ?>    


            <div class="table-responsive"> 

                <?php echo form_open($this->uri->uri_string()); ?>
                <table class="table table-striped datagrid m-b-sm">

                    <thead>
                        <tr>
                            <?php if ($can_delete && $has_records) : ?>
                                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                            <?php endif; ?>
                            <th><?php echo lang('module_name') ?></th>

                            <th<?php
                            if ($this->input->get('sort_by') == '_work_flow_module_id') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/settings/work_flow?sort_by=work_flow_module_id&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow_module_id') ? 'desc' : 'asc'); ?>'>
                                    <?php echo lang('Initiator') ?></a></th>
                            <th></th>
                            <th<?php
                            if ($this->input->get('sort_by') == '_work_flow_doc_generator') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/settings/work_flow?sort_by=work_flow_doc_generator&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow_doc_generator') ? 'desc' : 'asc'); ?>'>
                                    <?php echo lang('Escalation') ?></a></th>
                            <th></th>
                            <th></th>



                            <th<?php
                            if ($this->input->get('sort_by') == '_work_flow_fappr') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/settings/work_flow?sort_by=work_flow_fappr&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'work_flow_fappr') ? 'desc' : 'asc'); ?>'>
                                    <?php echo lang('final_approver') ?></a></th>
                        </tr>
                    </thead>
                    <?php if ($has_records) : ?>
                        <tfoot>
                            <?php if ($can_delete) : ?>
                                <tr>
                                    <td colspan="<?php echo $num_columns; ?>">
                                        <?php echo lang('bf_with_selected'); ?>
                                        <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('work_flow_delete_confirm'))); ?>')" />
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tfoot>
                    <?php endif; ?>
                    <tbody>
                        <?php
                        if ($has_records) : $no = $this->input->get('per_page') + 1;
                            foreach ($records as $record) :
                                ?>
                                <tr>
                                    <?php if ($can_delete) : ?>
                                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
                                    <?php endif; ?>

                                    <?php if ($can_edit) : ?>
                                        <td><?php
                                            $query = $this->db->query('SELECT modules_module_name,nickname FROM intg_modules WHERE id = ' . $record->work_flow_module_id . '');

                                            $row = $query->row();
                                            echo anchor(SITE_AREA . '/settings/work_flow/edit/' . $record->id, $row->nickname . '<i class="m-l fa fa-pencil"></i>')
                                            ?></td>
                                    <?php else : ?>
                                        <td><?php
                                            echo $row->nickname;
                                            ?></td>
                                    <?php endif; ?>

                                    <td><?php
                                        if ($record->flow_type == "user") {
                                            $query3 = $this->db->query('SELECT display_name FROM intg_users WHERE id = ' . $record->work_flow_doc_generator . '');
                                            $row3 = $query3->row();
                                            echo $row3->display_name;
                                        }
                                        if ($record->flow_type == "role") {
                                            echo $this->auth->role_name_by_id($role_id = $record->work_flow_doc_generator);
                                        }

//echo $this->db->last_query();
                                        ?></td>
                                    <td><a href="javascript:onclick=showrows('<?= $record->id ?>')"><i class="fa fa-arrow-circle-o-right"></i></a></td>
                                    <td colspan="3"><div id="row<?= $record->id ?>" style="display:none"><table class="table table-bordered"><tr><td><strong><?php echo lang('Approvers') ?></strong></td><td><strong><?php echo lang('role_name') ?></strong></td><td><strong><?php echo lang('Escalation_hrs') ?></strong></td></tr><?php
                                                $querymain = $this->db->query('SELECT id,work_flow_approvers,escalation,rolename FROM intg_work_flow WHERE work_flow_doc_generator ="' . $record->work_flow_doc_generator . '" AND work_flow_module_id	="' . $record->work_flow_module_id . '" order by id asc');
                                                //echo $this->db->last_query();

                                                foreach ($querymain->result() as $rowmain) {
                                                    echo "<tr><td>";
                                                    $query = $this->db->query('SELECT display_name FROM intg_users WHERE id IN ( ' . $rowmain->work_flow_approvers . ')');
                                                    //echo $this->db->last_query();
                                                    foreach ($query->result_array() as $row) {
                                                        echo $row['display_name'];
                                                    } echo "</td>";
                                                    echo "<td>";
                                                    echo $rowmain->rolename;
                                                    echo "</td><td>";
                                                    if ($record->work_flow_fappr != $rowmain->rolename) {
                                                        ?>

                                                        <input type="text" class="input-mini row<?= $record->id ?>" rowid="<?= $rowmain->id ?>"  value="<?= $rowmain->escalation ?>" /><?php
                                                        echo "</td></tr>";
                                                    }
                                                }
                                                ?></table></div></td>
                                    <td><?php e($record->work_flow_fappr) ?></td>					
                                </tr>
                                <?php
                                $no++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="<?php echo $num_columns; ?>"><?php echo lang('work_flow_records') ?></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>

                <?php echo form_close(); ?> 

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">


                        </div>
                        <div class="col-sm-4 text-center">
                            <small class="text-muted inline m-t-sm m-b-sm"><?php echo lang('work_flow_showing') ?> <?= $offset + 1 ?> - <?php echo $rowcount + $offset ?> <?php echo lang('work_flow_of') ?>  <?php echo $total->num_rows(); ?></small>
                        </div>
                        <div class="col-sm-4 text-right text-center-xs">                

                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </footer>

            </div>


        </div> </section>
