<script>
 function showroleusers(val,val1)
	{			
	
	$('#ru'+val).toggle();
	$('#ru'+val).html("<img src='<? echo Template::theme_url('images/loadingnew.gif') ?>' />");		
	$.post("<?php echo site_url(SITE_AREA .'/settings/work_flow/showroleusers') ?>", {divno:val,role:val1}, function(data){$('#ru'+val).html(data);
	
	  $('#ru'+val+' select').select2({placeholder:'Please select',dropdownAutoWidth:true,width:'150px'});
	
	 } )
	 
	
	}
	
	function selroleid(val)
	{
		
		
	$('#showroleuser_dd,#showroleuser_cb,#showroleuser_approver').html("<img src='<? echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?php echo site_url(SITE_AREA.'/settings/work_flow/role_user_dropdown') ?>", {type:val,module:$('#work_flow_module_id').val()}, function(resp){
	

		

	$('#showroleuser_dd').html( $('#inner1' , resp).html() );
	$('#showroleuser_cb').html($('#inner2', resp).html());	
	$('#showroleuser_approver').html($('#inner3', resp).html());	
	 $("#sortable").sortable();
	 
$("#work_flow_doc_generator").select2(		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:false}	);
	
	
	})
	}
	function check_roleuser()
	{
		
	var a = $("#work_flow_doc_generator").val();	
    var b = $("#work_flow_module_id").val();
	
	
	 $.ajax({
            type: 'POST',
            url:"<?php echo site_url(SITE_AREA .'/settings/work_flow/checkrole_user') ?>",
            data: {id: a,module:b},  
            dataType: 'html',
            success: function (html) 
            { 
                $('#alert').html(html);

                if($('#alert').text() == 'error')
                { 
                   $('#alert').html('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert">&times;</a><h5 class="alert-heading">You cannot add role or user id already defined in the work flow</h5></div>');
				   
				   $("#butang").attr("disabled", true);
                }
                 
                else{ //when success refresh the div
                    $('#alert').html(html);
					 $("#butang").attr("disabled", false);
					
					
					}
					
            }
        });
		
	}
	
</script>
    <style>
.sortable {
	/*width: 1500px;*/
}
.sortable li {
	list-style: none;
	/*border: 1px solid #CCC;*/
	background: #F6F6F6;
	font-family: "Tahoma";
	color: #1C94C4;
	margin: 20px;
	padding: 10px;
	height:auto;
	float:left;
	cursor:move
}
#sortable li{
    
    float:left; margin:10px; cursor: move; list-style:none
}
</style>


 <script src="<?php echo Template::theme_url('js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>

    <script>
  $(document).ready(function() {
    $("#sortable").sortable();	
  });
 $( 'body' ).on( 'click', '#fooBtnGroup .btn', function(event) {
  
   $(this).button('toggle');
 event.stopPropagation(); // prevent default bootstrap behavior
            // do some stuff
           // alert('click after toggle');
	
  //  alert( $(this).hasClass( 'active' ) ); // button state AFTER the click
  
});

  </script>
    <?php  if (validation_errors()) :  ?>
<div class="alert alert-block alert-error fade in "> <a class="close" data-dismiss="alert">&times;</a>
  <h4 class="alert-heading"><? echo lang('workflow_create_error')?></h4>
  <?php echo validation_errors(); ?> </div>
<?php endif; ?>
<?php // Change the css classes to suit your needs

if( isset($work_flow) ) {
    $work_flow = (array)$work_flow;
}
$id = isset($work_flow['id']) ? $work_flow['id'] : '';
?>
<div class="row">
 <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
        <fieldset>
    <section class="panel panel-default">
      <header class="panel-heading">
        <h3 class="m-b-none"><? echo lang('work_flow')?></h3>
      </header>
       
      <div class="panel-body">
      <div class="table-responsive">
     
        
      <table class="table table-condensed b-t b-light text-sm">
        <tr>
          <td width="100">  <?php // Change the values in this array to populate your dropdown as required 
		$modname = $this->db->get_where('intg_modules', array('deleted'=>'0'))->result();
		$options[''] = "- - -Please Select- - -";
		
		?>
            
                <label><? echo lang('module_name')?></label>
            
               </td>
    <td width="700"> 
        <select name="work_flow_module_id" id="work_flow_module_id" class="form-control product selecta" style="width:auto" onchange="selroleid('user')">
                  <option></option>
                  <?php
		foreach($modname as $h)
		{?>
                  <option value="<?=$h->id?>"><?=$h->nickname;?></option>
                  <?php
		}
			
		?>
                </select></td>
          </tr>
        <tr>
          <td>  <label><?php echo lang('Initiator')?></label></td>
          <td> 
                 <!--  <div class="btn-group"  data-toggle="buttons" style="float:left; margin-right:30px; width:50%" id="fooBtnGroup1"><ul></ul>
<!--                    <label class="btn btn-sm btn-info" value="role">
                      <input type="radio" name="byroleuser" id="byrole"    value="role" onclick="selroleid('role')" >
                      <i class="fa fa-check text-active"></i><? echo lang('workflow_by_role')?></label>
                    <label class="btn btn-sm btn-danger" value="user">
                      <input type="radio" name="byroleuser" id="byuser"   value="user" onclick="selroleid('user')" >
                      <i class="fa fa-check text-active"></i><? echo lang('workflow_by_user')?> </label>
                      
                  </div>-->
                <input type="hidden" name="byroleuser" id="byuser" value="user" >
                  
                  <div id="showroleuser_dd"></div> 
                
                 </td>
          </tr>
        <tr>
          <td><div id="showroleuser_approver"></div></td>
          <td>  <div class="control-group <?php echo form_error('work_flow_approvers') ? 'error' : ''; ?>" id="showroleuser_cb"></div></td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          </tr>
      </table>

       </div>
                
              
                
     
       <div class="form-actions"> <br/>
              <input type="submit" name="save" class="btn btn-primary" value="Create" id="butang" />
              &nbsp;&nbsp;&nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/work_flow', lang('work_flow_cancel'), 'class="btn btn-warning"'); ?> </div>
          
                </section>
              
            </div> 
           
            
           
           
         
      </fieldset>          
       
        <?php echo form_close(); ?>
        </div>
        
         <script>

$(".btn-group label").click(function() {
	
	selroleid($(this).attr("value"));
	

   
 });
 </script>
