<script>
    function showroleusers(val, val1)
    {
        if ($('#ru' + val).is(":visible")) {
            $('#ru' + val).hide();
        } else {
            $('#ru' + val).show();
        }
        ;

        $('#ru' + val).html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
        $.post("<?php echo site_url(SITE_AREA . '/settings/work_flow/showroleusers') ?>", {divno: val, role: val1}, function (data) {
            $('#ru' + val).html(data);

        })
    }

    function selroleid(val)
    {


        $('#showroleuser_dd,#showroleuser_cb,#showroleuser_approver').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
        $.post("<?php echo site_url(SITE_AREA . '/settings/work_flow/role_user_dropdown') ?>", {type: val, module: $('#work_flow_module_id').val()}, function (resp) {




            $('#showroleuser_dd').html($('#inner1', resp).html());
            $('#showroleuser_cb').html($('#inner2', resp).html());
            $('#showroleuser_approver').html($('#inner3', resp).html());
            $("#sortable").sortable();

            $("#work_flow_doc_generator").select2(
                    {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: false});


        })
    }
    function check_roleuser()
    {

        var a = $("#work_flow_doc_generator").val();
        var b = $("#work_flow_module_id").val();


        $.ajax({
            type: 'POST',
            url: "<?php echo site_url(SITE_AREA . '/settings/work_flow/checkrole_user') ?>",
            data: {id: a, module: b},
            dataType: 'html',
            success: function (html)
            {
                $('#alert').html(html);

                if ($('#alert').text() == 'error')
                {
                    $('#alert').html('<div class="alert alert-block alert-error fade in"><a class="close" data-dismiss="alert">&times;</a><h5 class="alert-heading">You cannot add role or user id already defined in the work flow</h5></div>');

                    $("#butang").attr("disabled", true);
                } else { //when success refresh the div
                    $('#alert').html(html);
                    $("#butang").attr("disabled", false);


                }

            }
        });

    }

</script>
<style>
    .sortable {
        width: 1000px;
    }
    .sortable li {
        list-style: none;
        border: 1px solid #CCC;
        background: #F6F6F6;
        font-family: "Tahoma";
        color: #1C94C4;
        margin: 20px;
        padding: 10px;
        height:auto;
        float:left;
        cursor:move
    }
    li.sortable-placeholder {
        border: 1px dashed #CCC;
        background: none;
        width:100px;
    }
</style>
<script src="<?php echo Template::theme_url('js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>
<script>
    $(document).ready(function () {
        $(".sortable").sortable();

        $("#work_flow_doc_generator").select2(
                {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true});

    });
</script>
<?php if (validation_errors()) : ?>
    <div class="alert alert-block alert-error fade in "> <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('workflow_create_error') ?></h4>
        <?php echo validation_errors(); ?> </div>
<?php endif; ?>
<?php
// Change the css classes to suit your needs

if (isset($work_flow)) {
    $work_flow = (array) $work_flow;
}
$id = isset($work_flow['id']) ? $work_flow['id'] : '';
?>
<div class="row">
    <div class="col-sm-10">
        <section class="panel panel-default">
            <header class="panel-heading">
                <h3 class="m-b-none"><?php echo lang('work_flow') ?></h3>
            </header>
            <div class="panel-body">
                <div class="col-sm-10">
                    <?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
                    <fieldset>
                        <?php
                        // Change the values in this array to populate your dropdown as required 
                        $modname = $this->db->get_where('intg_modules', array('deleted' => '0'))->result();
                        ?>
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-2">
                                <label><?php echo lang('module_name') ?></label>
                            </div>
                            <div class="col-sm-6">
                                <select name="work_flow_module_id_fake" id="work_flow_module_id" class="form-control product selecta" style="width:auto" disabled="">
                                    <?php
                                    foreach ($modname as $h) {
                                        if ($h->id == $work_flow['work_flow_module_id']) {
                                            ?>
                                            <option selected="selected" value="<?= $h->id ?>">
                                                <?= $h->nickname; ?>
                                            </option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?= $h->id ?>">
                                                <?= $h->nickname; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <input type="hidden" name="work_flow_module_id" value="<?= $work_flow['work_flow_module_id'] ?>">

                            </div>
                        </div>
                        <div class="form-group pull-in clearfix">
                            <div class="control-group">
                                <div class="col-sm-2">
                                    <label><?php echo lang('Initiator') ?></label>
                                </div>
                                <div class="col-sm-6">
                                    <div class="btn-group" data-toggle="buttons" style="float:left; margin-right:30px" id="fooBtnGroup">
                                        <!--                <label class="btn btn-sm btn-info <?php
                                        if ($work_flow['flow_type'] == "role") {
                                            echo 'active';
                                        }
                                        ?>" value="role">
                                                          <input name="byroleuser" type="radio"  value="role"  onclick="selroleid('role')" />
                                                          <i class="fa fa-check text-active"></i> <?php echo lang('workflow_by_role') ?> </label>-->
                                        <label class=" disabled btn btn-sm btn-danger <?php
                                        if ($work_flow['flow_type'] == "user") {
                                            echo 'active';
                                        }
                                        ?> " value="user">
                                            <input name="byroleuser_fake" type="radio" value="user" onclick="selroleid('user')" disabled=""/>
                                            <input type="hidden" name="byroleuser" value="user">
                                            <i class="fa fa-check text-active"></i><?php echo lang('workflow_by_user') ?> </label>
                                    </div>
                                    <div id="showroleuser_dd">
                                        <div class="span3">
                                            <select id="work_flow_doc_generator"  name="work_flow_doc_generator_fake" onchange="check_roleuser()" class="form-control product selecta" style="width:auto" disabled="">
                                                <option></option>
                                                <?php
                                                if ($work_flow['flow_type'] == "user") {
                                                    $users1 = $this->db->get_where('intg_users', array('deleted' => '0'))->result();

                                                    foreach ($users1 as $h2) {
                                                        if ($h2->username != "admin") {
                                                            if ($h2->id == $work_flow['work_flow_doc_generator']) {
                                                                ?>
                                                                <option selected="selected" value="<?php echo $h2->id; ?>" ><?php echo $h2->display_name; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $h2->id; ?>" ><?php echo $h2->display_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }

                                                if ($work_flow['flow_type'] == "role") {

                                                    $users1 = $this->db->get_where('intg_roles', array('deleted' => '0'))->result();

                                                    foreach ($users1 as $h2) {
                                                        if ($h2->role_name != "Administrator") {
                                                            ?>
                                                            <option value="<?php echo $h2->role_id; ?>" ><?php echo $h2->role_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <input type="hidden" name="work_flow_doc_generator" value="<?= $work_flow['work_flow_doc_generator'] ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <label><?php echo lang('Approvers') ?> </label>
                            </div>
                            <div class='controls m-l' id="roletable" style=" width:1500px;overflow:auto; ">
                                <h4><?php echo lang('workflow_drag') ?></h4>
                                <ul class="sortable">
                                    <?php
                                    $querymain = $this->db->query('SELECT rolename,work_flow_approvers FROM intg_work_flow WHERE work_flow_module_id = "' . $work_flow['work_flow_module_id'] . '" AND work_flow_doc_generator = "' . $work_flow['work_flow_doc_generator'] . '" order by id asc');
                                    $no = 1;
                                    foreach ($querymain->result() as $rowmain) {

                                        $existingrole[] = $rowmain->rolename;
                                        $queryrole = $this->db->query('SELECT role_id,role_name FROM intg_roles WHERE role_name  = "' . $rowmain->rolename . '"');
                                        ?>
                                        <?php
                                        foreach ($queryrole->result() as $rowrole) {
                                            ?>
                                            <li>
                                                <input type="checkbox"  name="chk[]" id="<?= $rowrole->role_name ?>" value="<?= $rowrole->role_name ?>:<?= $rowrole->role_id ?>" onclick = "showroleusers('<?= $no ?>', '<?= $rowrole->role_id ?>', '<?= $rowrole->role_name ?>')" checked="checked" class="checkbox inline"/>
                                                <?php echo $rowrole->role_name; ?>
                                                </td>
                                                <?php
                                                $no2 = 1;
                                                ?>
                                                <div id="ru<?php echo $no; ?>" style="display:block">
                                                    <?php $xy = explode(",", $rowmain->work_flow_approvers); ?>
                                                    <select name="work_flow<?= $rowrole->role_id ?>[]" multiple="multiple" style="width:auto">
                                                        <?php
                                                        $query = $this->db->query('SELECT * FROM intg_users WHERE role_id = ' . $rowrole->role_id . '');
                                                        foreach ($query->result() as $row) {
                                                            if (in_array($row->id, $xy)) {
                                                                ?>
                                                                <option value="<?php echo $row->id; ?>" selected="selected"><?php echo $row->display_name; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $row->id; ?>"><?php echo $row->display_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <?php
                                                $no++;
                                            }
                                        }
                                        ?>
                                    </li>
                                    <?php
                                    $removerole = implode(",", $existingrole);
                                    $query_newrole = $this->db->query('SELECT role_id,role_name FROM intg_roles WHERE NOT FIND_IN_SET (role_name, "' . $removerole . ',Administrator,USER")');
                                    //echo $this->db->last_query();
                                    $noo = $no;
                                    foreach ($query_newrole->result() as $newrowrole) {
                                        ?>
                                        <li>
                                            <input type="checkbox"  name="chk[]" id="<?= $newrowrole->role_name ?>" value="<?= $newrowrole->role_name ?>:<?= $newrowrole->role_id ?>" onclick = "showroleusers('<?= $noo ?>', '<?= $newrowrole->role_id ?>', '<?= $newrowrole->role_name ?>')"  class="checkbox inline"/>
                                            <?php echo $newrowrole->role_name; ?>
                                            </td>
                                            <?php ?>
                                            <div id="ru<?= $noo ?>" style="display:none">
                                                <select name="work_flow<?= $newrowrole->role_id ?>[]" multiple="multiple" style="width:auto">
                                                    <?php
                                                    $queryuser = $this->db->query('SELECT * FROM intg_users WHERE role_id = ' . $newrowrole->role_id . '');
                                                    foreach ($queryuser->result() as $rowuser) {
                                                        ?>
                                                        <option value="<?php echo $rowuser->id; ?>"><?php echo $rowuser->display_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php
                                            $noo++;
                                        }
                                        ?>
                                    </li>
                                </ul>
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="clear:both"></div>
                        <div class="form-actions"> <br/>
                            <input type="submit" name="save" class="btn btn-primary" value="Edit" id="butang" />
                            &nbsp;&nbsp;&nbsp;&nbsp; <?php echo anchor(SITE_AREA . '/settings/work_flow', lang('work_flow_cancel'), 'class="btn btn-warning"'); ?> </div>
                    </fieldset>
                    <?php echo form_close(); ?> </div>
            </div>
        </section>
    </div>
</div>
<script>

    $(".btn-group label").click(function () {

        selroleid($(this).attr("value"));



    });
</script>