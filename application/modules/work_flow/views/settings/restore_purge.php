<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($work_flow))
{
	$work_flow = (array) $work_flow;
}
$id = isset($work_flow['id']) ? $work_flow['id'] : '';

?>
<div class="row">
	<h3>Work Flow</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('work_flow_module_id') ? 'error' : ''; ?>">
				<?php echo form_label('Id', 'work_flow_work_flow_module_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_work_flow_module_id' type='text' name='work_flow_work_flow_module_id' maxlength="20" value="<?php echo set_value('work_flow_work_flow_module_id', isset($work_flow['work_flow_module_id']) ? $work_flow['work_flow_module_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('work_flow_module_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('work_flow_doc_generator') ? 'error' : ''; ?>">
				<?php echo form_label('Work Flow Doc Generator', 'work_flow_work_flow_doc_generator', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_work_flow_doc_generator' type='text' name='work_flow_work_flow_doc_generator' maxlength="11" value="<?php echo set_value('work_flow_work_flow_doc_generator', isset($work_flow['work_flow_doc_generator']) ? $work_flow['work_flow_doc_generator'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('work_flow_doc_generator'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('work_flow_fappr') ? 'error' : ''; ?>">
				<?php echo form_label('Work Flow Fappr', 'work_flow_work_flow_fappr', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_work_flow_fappr' type='text' name='work_flow_work_flow_fappr' maxlength="30" value="<?php echo set_value('work_flow_work_flow_fappr', isset($work_flow['work_flow_fappr']) ? $work_flow['work_flow_fappr'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('work_flow_fappr'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('rolename') ? 'error' : ''; ?>">
				<?php echo form_label('Rolename', 'work_flow_rolename', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_rolename' type='text' name='work_flow_rolename' maxlength="20" value="<?php echo set_value('work_flow_rolename', isset($work_flow['rolename']) ? $work_flow['rolename'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('rolename'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('work_flow_rule') ? 'error' : ''; ?>">
				<?php echo form_label('Work Flow Rule', 'work_flow_work_flow_rule', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_work_flow_rule' type='text' name='work_flow_work_flow_rule' maxlength="20" value="<?php echo set_value('work_flow_work_flow_rule', isset($work_flow['work_flow_rule']) ? $work_flow['work_flow_rule'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('work_flow_rule'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('work_flow_approvers') ? 'error' : ''; ?>">
				<?php echo form_label('Work Flow Approvers', 'work_flow_work_flow_approvers', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_work_flow_approvers' type='text' name='work_flow_work_flow_approvers' maxlength="100" value="<?php echo set_value('work_flow_work_flow_approvers', isset($work_flow['work_flow_approvers']) ? $work_flow['work_flow_approvers'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('work_flow_approvers'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('escalation') ? 'error' : ''; ?>">
				<?php echo form_label('Escalation', 'work_flow_escalation', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_escalation' type='text' name='work_flow_escalation' maxlength="20" value="<?php echo set_value('work_flow_escalation', isset($work_flow['escalation']) ? $work_flow['escalation'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('escalation'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('deleted') ? 'error' : ''; ?>">
				<?php echo form_label('Deleted', 'work_flow_deleted', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='work_flow_deleted'>
						<input type='checkbox' id='work_flow_deleted' name='work_flow_deleted' value='1' <?php echo (isset($work_flow['deleted']) && $work_flow['deleted'] == 1) ? 'checked="checked"' : set_checkbox('work_flow_deleted', 1); ?>>
						<span class='help-inline'><?php echo form_error('deleted'); ?></span>
					</label>
				</div>
			</div>

			<div class="control-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'work_flow_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_created_on' type='text' name='work_flow_created_on' maxlength="1" value="<?php echo set_value('work_flow_created_on', isset($work_flow['created_on']) ? $work_flow['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'work_flow_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='work_flow_modified_on' type='text' name='work_flow_modified_on' maxlength="1" value="<?php echo set_value('work_flow_modified_on', isset($work_flow['modified_on']) ? $work_flow['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/work_flow', lang('work_flow_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Work_Flow.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('work_flow_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>