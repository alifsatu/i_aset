
<script>
    $(document).ready(function () {
        $("#sortable").sortable();
    });
</script>


<div>
    <div id="inner1"><div class="span3">

            <select id="work_flow_doc_generator"  name="work_flow_doc_generator" class="form-control product " style="width:auto" onchange="check_roleuser()"> 
                <option></option>
                <?php
                if ($_POST['type'] == "user") {
                    $users1 = $this->db->get_where('intg_users', array('deleted' => '0','company_id' => $this->auth->company_id()))->result();
                    $moduleids = $_POST['module'];

                    foreach ($users1 as $h2) {
                        $q = $this->db->query("select work_flow_doc_generator from intg_work_flow where work_flow_doc_generator = '" . $h2->id . "' and  FIND_IN_SET ($moduleids,work_flow_module_id) ");

                        if ($h2->username != "admin") {
                            if ($q->num_rows() > 0) {
                                ?>

                                <option value="<?php echo $h2->id; ?>" disabled="disabled"><?php echo $h2->display_name; ?></option>
                                
                            <?php } else 
                                { ?> <option value="<?php echo $h2->id; ?>"  ><?php echo $h2->display_name; ?></option>
                                    <?php
                            }
                        }
                    }
                }

                if ($_POST['type'] == "role") {

                    $users1 = $this->db->get_where('intg_roles', array('deleted' => '0'))->result();
                    $moduleids = implode(",", $_POST['module']);

                    foreach ($users1 as $h2) {
                        $q = $this->db->query("select work_flow_doc_generator from intg_work_flow where work_flow_doc_generator = '" . $h2->role_id . "' and  FIND_IN_SET ($moduleids,work_flow_module_id)");

                        if ($h2->role_name != "Administrator") {
                            if ($q->num_rows() > 0) {
                                ?>

                                <option value="<?php echo $h2->role_id; ?>"  disabled="disabled"><?php echo $h2->role_name; ?></option>
                            <?php } else { ?><option value="<?php echo $h2->role_id; ?>"  ><?php echo $h2->role_name; ?></option><?php
                            }
                        } //end of foreach
                    }
                }
                ?>


            </select></div>
        <span class="span5"><div id="alert"></div></span>
    </div>

    <div id="inner2">



        <div  id="roletable">
            <h4><?php echo lang('workflow_drag') ?></h4>     
            <?php
            $queryrole = $this->db->query('SELECT role_id,role_name FROM intg_roles WHERE role_name not like ("admin%") order by role_id asc');
            $no = 1;
            ?>


            <ul id="sortable">


                <?php
                foreach ($queryrole->result() as $rowrole) {
                    ?>

                    <li><input type="checkbox"  name="chk[]"  value="<?= $rowrole->role_name ?>:<?= $rowrole->role_id ?>" onclick = "showroleusers('<?= $no ?>', '<?= $rowrole->role_id ?>')" class="checkbox inline"/><?php echo $rowrole->role_name; ?><?php
                        foreach ($queryrole->result() as $rowrole) {
                            ?>

                            <div id="ru<?= $no ?>" style="display:none;"><?= $no ?></div><?php
                        }

                        $no++;
                    }
                    ?>
                </li>


            </ul>
            <div style="clear:both"></div>
            <span class="help-inline"><?php echo form_error('work_flow_approvers'); ?></span>

        </div>






    </div>

    <div id="inner3"> <label><?php echo lang('Approvers') ?></label></div> </div>
