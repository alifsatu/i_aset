<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/work_flow') ?>"style="border-radius:0"  id="list"><?php echo lang('work_flow_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Work_Flow.Settings.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/work_flow/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('work_flow_new'); ?></a>
	</li>
	<?php endif; ?>
     <?php if ($this->auth->has_permission('Work_Flow.Settings.Deleted')) : ?>
    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/work_flow/deleted') ?>" style="border-radius:0"  id="deleted"><? echo lang('work_flow_column_deleted')?></a>
	</li>
    <?php endif; ?>
   
</ul>