<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/tags') ?>" style="border-radius:0"   id="list"><?php echo lang('tags_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Tags.Settings.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/tags/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('tags_new'); ?></a>
	</li>
	<?php endif; ?>
     <?php if ($this->auth->has_permission('Tags.Settings.Deleted')) : ?>
    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/tags/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>
	</li>
    <?php endif; ?>
</ul>