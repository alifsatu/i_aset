<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($tags))
{
	$tags = (array) $tags;
}
$id = isset($tags['id']) ? $tags['id'] : '';

?>
 <div class="row">
                <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading clearfix">Clients</header>
                    <div class="panel-body">
                          

	<?php echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>
    	<fieldset>
        
	

			<div class="form-group <?php echo form_error('tag_name') ? 'error' : ''; ?>">
				<?php echo form_label('Tag Name'. lang('bf_form_label_required'), 'tags_tag_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tags_tag_name' type='text' class="input-sm input-s  form-control" name='tags_tag_name' maxlength="255" value="<?php echo set_value('tags_tag_name', isset($tags['tag_name']) ? $tags['tag_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('tag_name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$clients = $this->db->get_where('intg_clients', array('deleted'=>'0'))->result();
		$clients_options[''] = "- - -Please Select- - -";
		foreach($clients as $h)
		{
			$clients_options[$h->id] = $h->client_name;
		}

				echo form_dropdown('tags_client_id', $clients_options, set_value('tags_client_id', isset($tags['client_id']) ? $tags['client_id'] : ''), 'Client Name'. lang('bf_form_label_required'));
			?>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('tags_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/tags', lang('tags_cancel'), 'class="btn btn-warning"'); ?>
				
			 </div>
					
					</fieldset>
					<?php echo form_close(); ?>
                    
                    
                    </div>
                    </section>
			</div><!--end of col6-->
		
    
</div> <!--end of row-->
