<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($tags))
{
	$tags = (array) $tags;
}
$id = isset($tags['id']) ? $tags['id'] : '';

?>
<div class="row">
	<h3>Tags</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('tag_name') ? 'error' : ''; ?>">
				<?php echo form_label('Tag Name'. lang('bf_form_label_required'), 'tags_tag_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tags_tag_name' type='text' name='tags_tag_name' maxlength="255" value="<?php echo set_value('tags_tag_name', isset($tags['tag_name']) ? $tags['tag_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('tag_name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('tags_client_id', $options, set_value('tags_client_id', isset($tags['client_id']) ? $tags['client_id'] : ''), 'Client Name'. lang('bf_form_label_required'));
			?>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/tags', lang('tags_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Tags.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('tags_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>