<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['tags_manage']			= 'Manage';
$lang['tags_edit']				= 'Edit';
$lang['tags_true']				= 'True';
$lang['tags_false']				= 'False';
$lang['tags_create']			= 'Save';
$lang['tags_list']				= 'List';
$lang['tags_new']				= 'New';
$lang['tags_edit_text']			= 'Edit this to suit your needs';
$lang['tags_no_records']		= 'There aren\'t any tags in the system.';
$lang['tags_create_new']		= 'Create a new Tags.';
$lang['tags_create_success']	= 'Tags successfully created.';
$lang['tags_create_failure']	= 'There was a problem creating the tags: ';
$lang['tags_create_new_button']	= 'Create New Tags';
$lang['tags_invalid_id']		= 'Invalid Tags ID.';
$lang['tags_edit_success']		= 'Tags successfully saved.';
$lang['tags_edit_failure']		= 'There was a problem saving the tags: ';
$lang['tags_delete_success']	= 'record(s) successfully deleted.';

$lang['tags_purged']	= 'record(s) successfully purged.';
$lang['tags_success']	= 'record(s) successfully restored.';


$lang['tags_delete_failure']	= 'We could not delete the record: ';
$lang['tags_delete_error']		= 'You have not selected any records to delete.';
$lang['tags_actions']			= 'Actions';
$lang['tags_cancel']			= 'Cancel';
$lang['tags_delete_record']		= 'Delete';
$lang['tags_delete_confirm']	= 'Are you sure you want to delete this tags?';
$lang['tags_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['tags_action_edit']		= 'Save';
$lang['tags_action_create']		= 'Create';

// Activities
$lang['tags_act_create_record']	= 'Created record with ID';
$lang['tags_act_edit_record']	= 'Updated record with ID';
$lang['tags_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['tags_column_created']	= 'Created';
$lang['tags_column_deleted']	= 'Deleted';
$lang['tags_column_modified']	= 'Modified';
