<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'List of notifications',
	'name'		=> 'Notifications',
	'version'		=> '0.0.1',
	'author'		=> 'admin'
);