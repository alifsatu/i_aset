<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_model extends BF_Model {

	protected $table_name	= "notifications";
	protected $key			= "id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array('notify');
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "user_id",
			"label"		=> "User ID",
			"rules"		=> "required|max_length[11]"
		),
		array(
			"field"		=> "title",
			"label"		=> "Title",
			"rules"		=> "required|max_length[100]"
		),
		array(
			"field"		=> "content",
			"label"		=> "Content",
			"rules"		=> "required|max_length[255]"
		),
		
		array(
			"field"		=> "link",
			"label"		=> "Link",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "class",
			"label"		=> "Class",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "module",
			"label"		=> "Module",
			"rules"		=> "max_length[100]"
		),
		array(
			"field"		=> "type",
			"label"		=> "Type",
			"rules"		=> "max_length[25]"
		),
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

	public function notify($id)
	{
		$notification = $this->find($id);
		
		$this->load->library('pusher/MY_Pusher');
		
		$this->my_pusher->notify($notification->user_id, array(
		
			'title' => $notification->title, 
			
			'message' => $notification->content,
		
			'link' => $notification->link ? (strpos($notification->link, '?') === false ? $notification->link .'?' : '&') . 'notification_id='.$id : $notification->link,
			
			'class' => $notification->class,
			
			'type' => $notification->type,
			
			'created_on' => date('c', strtotime($notification->created_on)),
		));
		
		$this->load->model('users/user_model',null,true);
		$user = $this->user_model->find((int) $notification->user_id);
		
		if($user && $user->notiapp_access_token) {
		
			$this->load->library('notiapp');
			
			$this->notiapp->send_notification(array(
				
				'user' => $user->notiapp_access_token,
				
				'title' => $notification->title,
				
				'text' => $notification->content,
				
				'url' => $notification->link ? (strpos($notification->link, '?') === false ? $notification->link .'?' : '&') . 'notification_id='.$id : $notification->link,
				
				'sound' => null,
				
				'image' => Template::theme_url('images/logo.png')
			));
		}
	}
}
