<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Notifications.Settings.View');
		$this->load->model('notifications_model', null, true);
		$this->lang->load('notifications');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('notifications', 'notifications.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	*/
	public function index()
	{
		if($this->input->get('notification_id'))
			$this->notifications_model->delete($this->input->get('notification_id'));
	
		Template::set('notifications', $this->notifications_model->order_by('created_on', 'desc')->find_all_by(array('user_id' => $this->auth->user_id())));
		
		Template::set('unread_notifications', $this->notifications_model->find_all_by(array('user_id' => $this->auth->user_id(), 'deleted' => 0)));
		
		Template::render();
	}

	//--------------------------------------------------------------------

	public function demo()
	{
		$users = $this->user_model->find_all_by(array('id' => $this->auth->user_id()));
		
		$this->load->model('notifications/notifications_model',null,true);
		$type = array('error','info','success','warning');
		foreach($users as $user)
		{
			for($i = 0;$i < 4;$i++)
			{
				var_dump($this->notifications_model->insert(array(
					'user_id' => $user->id,
					'title' => 'Woot woot',
					'content' => 'suprise madafaka',
					'link' => site_url('admin/settings/notifications'),
					'class' => 'fa-meh-o',
					'type' => $type[$i],
					'module' => 'Notification',
				)));
			}
		}
	}
	
	public function desktop($is_authed = 0)
	{
		$this->load->library('notiapp');
		
		$notiapp = $this->notiapp;
		$session = $this->session;
		$req_perm = function() use ($notiapp, $session) {
		
		$request_permission = $notiapp->get_request_token(site_url('admin/settings/notifications/desktop/1'));
		
		if(isset($request_permission['error'])) {
		
			echo $request_permission['error'];
			
		} else {
			$session->set_userdata('notiapp_request_token', $request_permission['request_token']);
			Template::redirect($request_permission['redirect_url']);
		}
		
		};
		
		if($is_authed) {
			
			if($request_token = $this->session->userdata('notiapp_request_token')) {
		
				$get_access_token = $this->notiapp->get_access_token($request_token);
				
				if(isset($get_access_token['error']))
					
					$req_perm();
				
				else {
				
					$this->user_model->update($this->auth->user_id(), array('notiapp_access_token' => $get_access_token['access_token']));
					
					$this->notifications_model->insert(array(
						'user_id' => $this->auth->user_id(),
						'title' => 'Notiapp has been authorized',
						'content' => 'You have authorized this sistem to send desktop notification using Notiapp. Click here to download Notiapp and install in your desktop.',
						'link' => 'https://www.notiapp.com/download',
						'class' => 'fa-desktop',
					));
					
					Template::redirect(site_url('admin/dashboard/dashboards'));
				}
			} else {
			
				$req_perm();
			}
		
		} else {
		
			$req_perm();
		}
		
	}
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_notifications SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('notifications_success'), 'success');
				}
				else
				{
					Template::set_message(lang('notifications_restored_error'). $this->notifications_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_notifications where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('notifications_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}


if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->notifications_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->notifications_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

//$records = $this-notifications_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;


if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->notifications_model
->where('deleted','1')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->notifications_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('notifications', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Notifications');
		Template::render();
	}

	//--------------------------------------------------------------------


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_notifications($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['user_id']        = $this->input->post('notifications_user_id');
		$data['title']        = $this->input->post('notifications_title');
		$data['content']        = $this->input->post('notifications_content');

		if ($type == 'insert')
		{
			$id = $this->notifications_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->notifications_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}