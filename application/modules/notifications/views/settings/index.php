<div class="row"><div class="col-sm-12 portlet">
<section class="panel panel-default portlet-item">
                <header class="panel-heading">
                  <ul class="nav nav-pills pull-right">
                    <li>
                      <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                    </li>
                  </ul>
                  Total <span class="badge bg-success"><?php echo $notifications ? count($notifications) : '0';?></span> Unread <span class="badge bg-info"><?php echo $unread_notifications ? count($unread_notifications) : '0';?></span>               
                </header>
                <section class="panel-body">
					<?php if($notifications):?>
					<?php foreach($notifications as $notification):?>
						<article class="media">
							<div class="pull-left">
								<span class="fa-stack fa-lg">
									<i class="fa fa-circle fa-stack-2x <?php echo !$notification->deleted ? 'text-info' : 'icon-muted';?>"></i>
									<i class="fa <?php echo $notification->class;?> fa-stack-1x text-white"></i>
								</span>
							</div>
							<div class="media-body">                        
								<a href="<?php echo $notification->link ? (strpos($notification->link, '?') === false ? $notification->link .'?' : '&') . 'notification_id='.$notification->id : 'javascript:false';?>" class="h4"><?php echo $notification->title;?></a>
								<small class="block m-t-xs"><?php echo $notification->content;?></small>
								<em class="text-xs"><span class="text-danger"><abbr class="timeago" title="<?php echo date('c', strtotime($notification->created_on));?>"><?php echo date('d/m/Y H:i:s', strtotime($notification->created_on));?></abbr></span></em>
							</div>
						</article>
						<div class="line pull-in"></div>
					<?php endforeach;?>
					<?php else:?>
						<article class="media">
							<div class="media-body">                        
								<a href="javascript:false" class="h4">You have no notification</a>
							</div>
						</article>
					<?php endif;?>
				<!--
                  <article class="media">
                    <div class="pull-left">
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-bold fa-stack-1x text-white"></i>
                      </span>
                    </div>
                    <div class="media-body">                        
                      <a href="#" class="h4">Bootstrap 3: What you need to know</a>
                      <small class="block m-t-xs">Sleek, intuitive, and powerful mobile-first front-end framework for faster and easier web development.</small>
                      <em class="text-xs">Posted on <span class="text-danger">Feb 23, 2013</span></em>
                    </div>
                  </article>
                  <div class="line pull-in"></div>
                  <article class="media">
                    <div class="pull-left">
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x text-info"></i>
                        <i class="fa fa-file-o fa-stack-1x text-white"></i>
                      </span>
                    </div>
                    <div class="media-body">
                      <a href="#" class="h4">Bootstrap documents</a>
                      <small class="block m-t-xs">There are a few easy ways to quickly get started with Bootstrap, each one appealing to a different skill level and use case. Read through to see what suits your particular needs.</small>
                      <em class="text-xs">Posted on <span class="text-danger">Feb 12, 2013</span></em>
                    </div>
                  </article>
                  <div class="line pull-in"></div>
                  <article class="media">
                    <div class="pull-left">
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x icon-muted"></i>
                        <i class="fa fa-mobile fa-stack-1x text-white"></i>
                      </span>
                    </div>
                    <div class="media-body">
                      <a href="#" class="h4 text-success">Mobile first html/css framework</a>
                      <small class="block m-t-xs">Bootstrap, Ratchet</small>
                      <em class="text-xs">Posted on <span class="text-danger">Feb 05, 2013</span></em>
                    </div>
                  </article>
				-->
                </section>
              </section>
			  </div></div>