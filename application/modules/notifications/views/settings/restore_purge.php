<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($notifications))
{
	$notifications = (array) $notifications;
}
$id = isset($notifications['id']) ? $notifications['id'] : '';

?>
<div class="admin-box">
	<h3>Notifications</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('user_id') ? 'error' : ''; ?>">
				<?php echo form_label('User ID'. lang('bf_form_label_required'), 'notifications_user_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='notifications_user_id' type='text' name='notifications_user_id' maxlength="11" value="<?php echo set_value('notifications_user_id', isset($notifications['user_id']) ? $notifications['user_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('user_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('title') ? 'error' : ''; ?>">
				<?php echo form_label('Title'. lang('bf_form_label_required'), 'notifications_title', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='notifications_title' type='text' name='notifications_title' maxlength="100" value="<?php echo set_value('notifications_title', isset($notifications['title']) ? $notifications['title'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('title'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('content') ? 'error' : ''; ?>">
				<?php echo form_label('Content'. lang('bf_form_label_required'), 'notifications_content', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'notifications_content', 'id' => 'notifications_content', 'rows' => '5', 'cols' => '80', 'value' => set_value('notifications_content', isset($notifications['content']) ? $notifications['content'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('content'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/notifications', lang('notifications_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Notifications.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('notifications_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>