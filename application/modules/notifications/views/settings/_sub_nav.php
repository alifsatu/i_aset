<!--
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/notifications') ?>"style="border-radius:0"  id="list"><?php echo lang('notifications_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Notifications.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/notifications/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('notifications_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Notifications.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/notifications/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
-->
<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><i class="fa fa-home"></i> Home</li>
                <li> Settings</li>
				<li class="active"> Notifications</li>
              </ul>