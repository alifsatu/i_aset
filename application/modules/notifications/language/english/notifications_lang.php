<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['notifications_manage']			= 'Manage';
$lang['notifications_edit']				= 'Edit';
$lang['notifications_true']				= 'True';
$lang['notifications_false']				= 'False';
$lang['notifications_create']			= 'Save';
$lang['notifications_list']				= 'List';
$lang['notifications_new']				= 'New';
$lang['notifications_edit_text']			= 'Edit this to suit your needs';
$lang['notifications_no_records']		= 'There aren\'t any notifications in the system.';
$lang['notifications_create_new']		= 'Create a new Notifications.';
$lang['notifications_create_success']	= 'Notifications successfully created.';
$lang['notifications_create_failure']	= 'There was a problem creating the notifications: ';
$lang['notifications_create_new_button']	= 'Create New Notifications';
$lang['notifications_invalid_id']		= 'Invalid Notifications ID.';
$lang['notifications_edit_success']		= 'Notifications successfully saved.';
$lang['notifications_edit_failure']		= 'There was a problem saving the notifications: ';
$lang['notifications_delete_success']	= 'record(s) successfully deleted.';

$lang['notifications_purged']	= 'record(s) successfully purged.';
$lang['notifications_success']	= 'record(s) successfully restored.';


$lang['notifications_delete_failure']	= 'We could not delete the record: ';
$lang['notifications_delete_error']		= 'You have not selected any records to delete.';
$lang['notifications_actions']			= 'Actions';
$lang['notifications_cancel']			= 'Cancel';
$lang['notifications_delete_record']		= 'Delete';
$lang['notifications_delete_confirm']	= 'Are you sure you want to delete this notifications?';
$lang['notifications_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['notifications_action_edit']		= 'Save';
$lang['notifications_action_create']		= 'Create';

// Activities
$lang['notifications_act_create_record']	= 'Created record with ID';
$lang['notifications_act_edit_record']	= 'Updated record with ID';
$lang['notifications_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['notifications_column_created']	= 'Created';
$lang['notifications_column_deleted']	= 'Deleted';
$lang['notifications_column_modified']	= 'Modified';
