<?php

$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
<? echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
				<div class="row">
				 <div class="col-md-3">  <h4>Project Report</h4></div><div class="col-md-4"><div class="input-group"><select name="projects" class="selecta form-control"><option></option><? 
				 $proj = $this->db->query("select * from intg_projects")->result();
				 foreach ( $proj as $p ) {
				 
				 ?><option value="<?=$p->id?>"><?=$p->project_name?></option><? } ?></select><span class="input-group-btn">                                    
                        <button type="submit" name="projsubmit" value="projsubmit" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
						<button type="submit" name="projsubmit" class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
						</span>
							</div></div>
                  <div class="col-md-2">
				<input type="text" class="form-control datepicker-input" placeholder="From Date" name="fromdate" id="fromdate" value="<?=$this->session->userdata('reportafromfield') != "" ? date("d-m-Y",strtotime($this->session->userdata('reportafromfield'))) : ""?>" data-date-format="dd-mm-yyyy">
						</div>
						<div class="col-md-3"><div class="input-group">
					<input type="text" class="form-control datepicker-input" placeholder="To Date" name="todate" id="todate" value="<?=$this->session->userdata('reportatofield') != "" ? date("d-m-Y",strtotime($this->session->userdata('reportatofield'))): ""?>" data-date-format="dd-mm-yyyy">
						<span class="input-group-btn">                                    
                        <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
						<button type="submit" name="submit" class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
						</span>
							</div>
						</div>
					</div>
                  <?php echo form_close(); ?> 
     <div class="table-responsive m-t">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					
					<th>#</th>
                   
                    <th<?php if ($this->input->get('sort_by') == '_display_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=display_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'display_name') ? 'desc' : 'asc'); ?>'>
                    Individual</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_project_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=project_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_name') ? 'desc' : 'asc'); ?>'>
                   Staff Level</a></th>
                   <th<?php if ($this->input->get('sort_by') == '_email') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=email&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'email') ? 'desc' : 'asc'); ?>'>
                   Designation</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_sbu_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=sbu_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sbu_name') ? 'desc' : 'asc'); ?>'>
                   Hours</a></th>
                     <th<?php if ($this->input->get('sort_by') == '_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                   RM</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_total_days') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=total_days&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'total_days') ? 'desc' : 'asc'); ?>'>
                   Remarks</a></th>
					 <th<?php if ($this->input->get('sort_by') == '_project_cost') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/project_report?sort_by=project_cost&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_cost') ? 'desc' : 'asc'); ?>'>
                   Task</a></th>
				</tr>
			</thead>
			
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					
					<td><?=$no?></td>
                    <td><?=$record->display_name?></td>
                    <td><?=$record->user_level?></td>
                    <td><?=$this->auth->role_name_by_id( $record->role_id )?></td>
                    <td><?
                    echo $this->db->query("select sum(tsd_hours) as hrs from intg_timesheet_details where pid = '".$record->project_id."'")->row()->hrs;
					
					?></td>
                    <td><?=$record->project_cost?></td>
                    <td>&nbsp;</td>
                    <td><?
                    
					 $ts = $this->db->query("select distinct(tp.task_name) from intg_task_pool tp,intg_timesheet_details itd  where itd.ptskid = tp.id and itd.pid = '".$record->project_id."'")->result();
					 foreach ( $ts as $t ) { echo $t->task_name."<br>"; }
					?></td>
					
			  </tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="7">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
<script>

 $(document).ready(function() {
  $('#fromdate').datepicker().on('changeDate', function(){
          $('#fromdate').datepicker('hide');
		  
        });
		 $('#todate').datepicker().on('changeDate', function(){
          $('#todate').datepicker('hide');
		  
        });
 });
	
	
</script>