
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/reports/project_report') ?>"style="border-radius:0"  id="list"><?php echo lang('project_report_list'); ?></a>

	</li>
<li <?php echo $this->uri->segment(4) == 'export' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/reports/project_report/export') ?>"style="border-radius:0"  id="list">Export to excel</a>

	</li>
	
</ul>
