<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['project_report_manage']			= 'Manage';
$lang['project_report_edit']				= 'Edit';
$lang['project_report_true']				= 'True';
$lang['project_report_false']				= 'False';
$lang['project_report_create']			= 'Save';
$lang['project_report_list']				= 'List';
$lang['project_report_new']				= 'New';
$lang['project_report_edit_text']			= 'Edit this to suit your needs';
$lang['project_report_no_records']		= 'There aren\'t any project_report in the system.';
$lang['project_report_create_new']		= 'Create a new Project Report.';
$lang['project_report_create_success']	= 'Project Report successfully created.';
$lang['project_report_create_failure']	= 'There was a problem creating the project_report: ';
$lang['project_report_create_new_button']	= 'Create New Project Report';
$lang['project_report_invalid_id']		= 'Invalid Project Report ID.';
$lang['project_report_edit_success']		= 'Project Report successfully saved.';
$lang['project_report_edit_failure']		= 'There was a problem saving the project_report: ';
$lang['project_report_delete_success']	= 'record(s) successfully deleted.';

$lang['project_report_purged']	= 'record(s) successfully purged.';
$lang['project_report_success']	= 'record(s) successfully restored.';


$lang['project_report_delete_failure']	= 'We could not delete the record: ';
$lang['project_report_delete_error']		= 'You have not selected any records to delete.';
$lang['project_report_actions']			= 'Actions';
$lang['project_report_cancel']			= 'Cancel';
$lang['project_report_delete_record']		= 'Delete';
$lang['project_report_delete_confirm']	= 'Are you sure you want to delete this project_report?';
$lang['project_report_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['project_report_action_edit']		= 'Save';
$lang['project_report_action_create']		= 'Create';

// Activities
$lang['project_report_act_create_record']	= 'Created record with ID';
$lang['project_report_act_edit_record']	= 'Updated record with ID';
$lang['project_report_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['project_report_column_created']	= 'Created';
$lang['project_report_column_deleted']	= 'Deleted';
$lang['project_report_column_modified']	= 'Modified';
