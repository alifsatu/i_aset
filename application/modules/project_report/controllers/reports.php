<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Project_Report.Reports.View');
		$this->load->model('projects/projects_model', null, true);
		$this->lang->load('project_report');
		
		Template::set_block('sub_nav', 'reports/_sub_nav');

		Assets::add_module_js('project_report', 'project_report.js');
		
		
		Assets::add_css('js/datepicker/datepicker.css');
		Assets::add_js("js/datepicker/bootstrap-datepicker.js");
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('reportafromfield',date("Y-m-d",strtotime($this->input->post('fromdate'))));
$this->session->set_userdata('reportatofield',date("Y-m-d",strtotime($this->input->post('todate'))));
$this->session->unset_userdata('reportaprojectfield');

break;
case 'Reset':
$this->session->unset_userdata('reportafromfield');
$this->session->unset_userdata('reportatofield');
break;
}

				
switch ( $this->input->post('projsubmit') ) 
{
case 'projsubmit':
$this->session->unset_userdata('reportafromfield');
$this->session->unset_userdata('reportatofield');
$this->session->set_userdata('reportaprojectfield',$this->input->post('projects'));
break;
case 'Reset':
$this->session->unset_userdata('reportaprojectfield');
break;
}

$fromdate = $this->session->userdata('reportafromfield')=="" ? "1970-01-01" : $this->session->userdata('reportafromfield');
$todate = $this->session->userdata('reportatofield')=="" ? "2050-01-01" : $this->session->userdata('reportatofield');
$id = $this->session->userdata('reportaprojectfield')=="" ? 'intg_projects.id' : $this->session->userdata('reportaprojectfield');
	
	


$total = $this->db->query("SELECT count(*) as numrows FROM (intg_projects) JOIN intg_users ON intg_users.id=intg_projects.initiator WHERE `intg_projects`.`project_start_date` >= '{$fromdate}' AND `intg_projects`.`project_end_date` <= '{$todate}' AND intg_projects.id = {$id} AND final_status = 'Yes' and `row_status` = 'final'  AND ( `initiator` = ".$this->auth->user_id()." OR FIND_IN_SET( '".$this->auth->user_id()."',coworker) OR  FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR  FIND_IN_SET( '".$this->auth->user_id()."',other_approvers))")->row()->numrows;

 	

		//$records = $this->reporta_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page')=="" ? 0 : $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


	 

	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = 	$this->input->get('sort_by')." ". $this->input->get('sort_order').",intg_projects.id desc";
		
	}
	else { $ord1 =		"intg_projects.id desc";
		}
	 	 	


$records = $this->db->query("SELECT *, intg_projects.id as project_id FROM (intg_projects) JOIN intg_users ON intg_users.id=intg_projects.initiator JOIN intg_user_level ON intg_users.user_level_id=intg_user_level.id WHERE `intg_projects`.`project_start_date` >= '{$fromdate}' AND `intg_projects`.`project_end_date` <= '{$todate}' AND intg_projects.id = {$id} AND final_status = 'Yes' and `row_status` = 'final'  AND ( `initiator` = ".$this->auth->user_id()." OR FIND_IN_SET( '".$this->auth->user_id()."',coworker) OR  FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR  FIND_IN_SET( '".$this->auth->user_id()."',other_approvers))order by {$ord1}  LIMIT {$offset} ,".$this->pager['per_page']."")->result();	


 



//echo $this->db->last_query();
//$records = $this->reporta_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('reporta', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage reporta');
		Template::render();
	}

	
public function export()
{
	$this->load->library('session');
	$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
//name the worksheet
$this->excel->getActiveSheet()->setTitle('project');
$this->excel->getActiveSheet()->setCellValue('A1', 'Individual');
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B1', 'Staff Level');
$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('C1', 'Designation');
$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('D1', 'Hours');
$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('E1', 'RM');
$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('F1', 'Remarks');
$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('G1', 'Task');
$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);




$fromdate = $this->session->userdata('reportafromfield')=="" ? "1970-01-01" : $this->session->userdata('reportafromfield');
$todate = $this->session->userdata('reportatofield')=="" ? "2050-01-01" : $this->session->userdata('reportatofield');
$id = $this->session->userdata('reportaprojectfield')=="" ? 'intg_projects.id' : $this->session->userdata('reportaprojectfield');



	 	 	
$this->load->model('projects/projects_model', null, true);
				 

$records = $this->db->query("SELECT *, intg_projects.id as project_id FROM (intg_projects) JOIN intg_users ON intg_users.id=intg_projects.initiator JOIN intg_user_level ON intg_users.user_level_id=intg_user_level.id WHERE `intg_projects`.`project_start_date` >= '{$fromdate}' AND `intg_projects`.`project_end_date` <= '{$todate}' AND intg_projects.id = {$id} order by intg_projects.id desc ")->result();	

 $no=2;
 

 foreach($records as $record) 
 {
 
$this->excel->getActiveSheet()->setCellValue("A".$no, $record->display_name);
$this->excel->getActiveSheet()->setCellValue('B'.$no, $record->user_level);
$this->excel->getActiveSheet()->setCellValue('C'.$no, $record->position);

$this->excel->getActiveSheet()->setCellValue("D".$no,  $this->db->query("select sum(tsd_hours) as hrs from intg_timesheet_details where pid = '".$record->project_id."'")->row()->hrs);	 
$this->excel->getActiveSheet()->setCellValue("E".$no,number_format($record->project_cost));						
$this->excel->getActiveSheet()->setCellValue("F".$no, $record->remarks); 
 $ts = $this->db->query("select distinct(tp.task_name) from intg_task_pool tp,intg_timesheet_details itd  where itd.ptskid = tp.id and itd.pid = '".$record->project_id."'")->result();
 foreach ( $ts as $t ) {$tn[] = $t->task_name; }
$this->excel->getActiveSheet()->setCellValue("G".$no, implode(",",$tn)); 



                  
                   
                   
                  

 $no++;
 }


$filename='project.XLSX'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');

	
		
		//Template::render();
	
}


}