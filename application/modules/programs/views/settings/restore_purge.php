<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($programs))
{
	$programs = (array) $programs;
}
$id = isset($programs['id']) ? $programs['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Programs</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('program_name') ? 'error' : ''; ?>">
				<?php echo form_label('Program Name'. lang('bf_form_label_required'), 'programs_program_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='programs_program_name' class='input-sm input-s  form-control' type='text' name='programs_program_name' maxlength="255" value="<?php echo set_value('programs_program_name', isset($programs['program_name']) ? $programs['program_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('program_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'programs_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'programs_description', 'id' => 'programs_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('programs_description', isset($programs['description']) ? $programs['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created By', 'programs_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='programs_created_by' class='input-sm input-s  form-control' type='text' name='programs_created_by' maxlength="255" value="<?php echo set_value('programs_created_by', isset($programs['created_by']) ? $programs['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name', 'programs_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='programs_company_id' class='input-sm input-s  form-control' type='text' name='programs_company_id' maxlength="255" value="<?php echo set_value('programs_company_id', isset($programs['company_id']) ? $programs['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/programs', lang('programs_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Programs.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('programs_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>