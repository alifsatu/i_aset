
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/programs') ?>"style="border-radius:0"  id="list"><?php echo lang('programs_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Programs.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/programs/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('programs_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Programs.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/programs/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
