<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['programs_manage']			= 'Manage';
$lang['programs_edit']				= 'Edit';
$lang['programs_true']				= 'True';
$lang['programs_false']				= 'False';
$lang['programs_create']			= 'Save';
$lang['programs_list']				= 'List';
$lang['programs_new']				= 'New';
$lang['programs_edit_text']			= 'Edit this to suit your needs';
$lang['programs_no_records']		= 'There aren\'t any programs in the system.';
$lang['programs_create_new']		= 'Create a new Programs.';
$lang['programs_create_success']	= 'Programs successfully created.';
$lang['programs_create_failure']	= 'There was a problem creating the programs: ';
$lang['programs_create_new_button']	= 'Create New Programs';
$lang['programs_invalid_id']		= 'Invalid Programs ID.';
$lang['programs_edit_success']		= 'Programs successfully saved.';
$lang['programs_edit_failure']		= 'There was a problem saving the programs: ';
$lang['programs_delete_success']	= 'record(s) successfully deleted.';

$lang['programs_purged']	= 'record(s) successfully purged.';
$lang['programs_success']	= 'record(s) successfully restored.';


$lang['programs_delete_failure']	= 'We could not delete the record: ';
$lang['programs_delete_error']		= 'You have not selected any records to delete.';
$lang['programs_actions']			= 'Actions';
$lang['programs_cancel']			= 'Cancel';
$lang['programs_delete_record']		= 'Delete';
$lang['programs_delete_confirm']	= 'Are you sure you want to delete this programs?';
$lang['programs_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['programs_action_edit']		= 'Save';
$lang['programs_action_create']		= 'Create';

// Activities
$lang['programs_act_create_record']	= 'Created record with ID';
$lang['programs_act_edit_record']	= 'Updated record with ID';
$lang['programs_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['programs_column_created']	= 'Created';
$lang['programs_column_deleted']	= 'Deleted';
$lang['programs_column_modified']	= 'Modified';
