<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_projects_permissions extends Migration
{

	/**
	 * Permissions to Migrate
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name' => 'Projects.Quotes.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Quotes.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Quotes.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Quotes.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Quotes.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Quotes.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Invoices.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Client.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Content.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Reports.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Settings.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Projects.Developer.Delete',
			'description' => '',
			'status' => 'active',
		),
	);

	/**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $table_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->table_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->table_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->table_name, array('name' => $permission_value['name']));
		}
	}

	//--------------------------------------------------------------------

}