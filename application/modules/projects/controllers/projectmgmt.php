<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * projects controller
 */
class projectmgmt extends Admin_Controller {

    //--------------------------------------------------------------------

    public $dota = array();

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Projects.Projectmgmt.View');
        $this->load->model('projects_model', null, true);
        $this->load->model('users/user_model', null, true);
        $this->lang->load('projects');
        $this->load->library('pagination');

        Assets::add_js(Template::theme_url("js/jQueryHtmlToWord/FileSaver.js"));
        Assets::add_js(Template::theme_url("js/jQueryHtmlToWord/jquery.wordexport.js"));
        Assets::add_js(Template::theme_url("js/html2pdf.bundle.min.js"));

        //Assets::add_js("js/parsley/parsley.min.js");
        //$this->load->library('dateformat');
        Template::set_block('sub_nav', 'projectmgmt/_sub_nav');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {
        $rolename = $this->auth->role_name_by_id($this->auth->role_id());
        $records = array();
        $filter_string = array();
        if ($this->input->get('from')) {
            array_push($filter_string, 'DATE(project_start_date) >= "' . date('Y-m-d', strtotime($this->input->get('from'))) . '"');
        }
        if ($this->input->get('to')) {
            array_push($filter_string, 'DATE(project_end_date) <= "' . date('Y-m-d', strtotime($this->input->get('to'))) . '"');
        }
        if ($this->input->get('search')) {
            array_push($filter_string, 'project_name like "%' . $this->input->get('search') . '%"');
        }
        if ($filter_string) {
            $records = $this->db->query("SELECT * FROM (`intg_projects`) WHERE final_status = 'Yes' and `row_status` = 'final'  AND " . implode(' AND ', $filter_string) . " AND ( `initiator` = " . $this->auth->user_id() . " OR FIND_IN_SET( '" . $this->auth->user_id() . "',coworker) OR  FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR  FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers) OR FIND_IN_SET('$rolename','CEO,Finance Team'))")->result();
        } else {
            //$records = $this->projects_model->get_projects('0','0','final','Yes');

            $records = $this->projects_model
                    ->where('final_status', 'Yes')
                    ->where('row_status', 'final')
                    ->where('deleted', '0')
                    //->where('FIND_IN_SET( subsidiary_id,"'.$this->current_user->subsidiary_id.'")')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR 
   FIND_IN_SET( '" . $this->auth->user_id() . "',coworker) OR
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers) 
	OR FIND_IN_SET('$rolename','CEO,Finance Team')   
   )")
                    ->find_all();
        }

        echo $this->db->last_query();

        Template::set('records', $records);
        Assets::add_js(Template::theme_url('js/jquery.mixitup.min.js'));

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));

        Assets::add_js(Template::theme_url('js/HC/highcharts.js'));
        Assets::add_js(Template::theme_url('js/HC//modules/exporting.js'));

        Assets::add_module_js('projects', 'projects.js');
        Template::set_view('projectmgmt/dashboard');
        Template::render();
    }

    public function approval() {

        $this->load->helper('email_template_modal');
        convertToPdfAndSendEmail();

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sapfield', $this->input->post('select_field'));
                $this->session->set_userdata('sapfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sapfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('sapfield');
                $this->session->unset_userdata('sapfvalue');
                $this->session->unset_userdata('sapfname');
                break;
        }

        if ($this->session->userdata('sapfield') != '') {
            $field = $this->session->userdata('sapfield');
        } else {
            $field = 'intg_projects.id';
        }

        if ($this->session->userdata('sapfield') == 'All Fields') {


            $total = $this->projects_model
                    ->where('row_status', 'final')
                    // ->where('FIND_IN_SET( "'.$this->current_user->subsidiary_id.'",subsidiary_id)')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR    
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers)    
   )")
                    ->where("(
     `client_name`   like ('%" . $this->session->userdata('sapfvalue') . "%') OR 
   `display_name` like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `quote_number` like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `quote_notes` like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `Quote_due_date` like ('%" . $this->session->userdata('sapfvalue') . "%')  
   )", NULL, FALSE)
                    ->count_all();
        } else {

            $total = $this->projects_model
                    ->where('row_status', 'final')
                    //->where('FIND_IN_SET( "'.$this->current_user->subsidiary_id.'",subsidiary_id)')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR 
   
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers)    
   )")
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->count_all();
        }


        //$records = $this->quote_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');

        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 15;
        $this->pager['num_links'] = 5;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }





        if ($this->session->userdata('sapfield') == 'All Fields') {
            $records = $this->projects_model
                    ->where('row_status', 'final')
                    // ->where('FIND_IN_SET( "'.$this->current_user->subsidiary_id.'",subsidiary_id)')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR 
   
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers)    
   )")
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'intg_projects.id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'intg_projects.version_no' => 'desc',
                    'intg_projects.id' => 'desc'
                );
            }

            $records = $this->projects_model
                    ->where('row_status', 'final')
                    // ->where('FIND_IN_SET( "'.$this->current_user->subsidiary_id.'",subsidiary_id)')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR 
  
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers)    
   )")
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }


        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());

        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Template::set('records', $records);



        //Template::set_view('projectmgmt/dashboard');
        Template::render();
    }

    function quickapprove() {
        Template::render();
    }

    function updateApprovalstatus() {
//        var_dump($_POST);exit('booo');
        Template::render();
    }

    public function version_history() {
        //$records = $this->db->query("select * from intg_files where id = '".$this->uri->segment(5)."' or ( version = '".$this->uri->segment(5)."') order by id desc")->result();
        //echo $this->db->last_query();
//        Template::set("records", $records);
        Template::render();
    }

    public function view_status() {
        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);
        $this->load->model('assigned_to/assigned_to_model', null, true);

        $uid = $this->auth->user_id();

        if ($this->uri->segment(5) == "insert_mu") {
            $id = $this->milestone_users_model->insert(array("milestones_id" => $this->input->post("milestone_id")));
            echo $id;
            exit;
        }

        if ($this->uri->segment(5) == "markcomplete") {
            $this->milestone_model->update($this->input->post("rowid"), array("completed_status" => "completed", "remarks" => $this->input->post("remarks")));
            exit;
        }
        if (isset($_POST['m_approval'])) {
            $this->load->library('workflow1');
            list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow1->checkflow1();
            $data = array();
            //$data['parent_Rid'] = $this->input->post('parent_Rid');		
            //$typeworkflow=="none" ? $data['final_status'] = "none" : "";
            $data['final_status'] = "No";
            //$data['final_approvers'] = $work_flow_approvers;
            $data['initiator'] = $dataroleuser;
            $result_milestone = $this->db->query("INSERT INTO `intg_milestone_approval`(`final_status`, `initiator`, `final_approvers`, `project_code`, `created_by`, `project_id`) VALUES ('No'," . $dataroleuser . ",'" . $work_flow_approvers . "'," . $this->uri->segment(5) . "," . $this->auth->user_id() . "," . $this->uri->segment(5) . ")");
            //$last_id = mysql_insert_id();
            $last_id = $this->db->insert_id();
            $this->workflow1->create_approvers($last_id);
            $this->sendmail($this->auth->user_id(), $this->uri->segment(5), $url);

            if (isset($result_milestone)) {

                Template::set_message('Milestone successfully sent for approval', 'success');
                redirect(SITE_AREA . '/projectmgmt/projects/milestone_approval');
            } else {
                echo "not success";
                exit;
            }
        }
        if (isset($_POST['save'])) {
            $sbu = $this->db->query("select sbu_id,prefix_io_number,cost_centre_id,status,remarks  from intg_projects where id = " . $this->uri->segment(5) . "")->row();
            if ($this->input->post('projects_state') == "Initiated" || $this->input->post('projects_state') == "Active") {

                if ($sbu->status == $this->input->post('projects_state')) {
                    Template::set_message('Project Already Initiated', 'error');
                    redirect(SITE_AREA . '/projectmgmt/projects/view_status' . "/" . $this->uri->segment(5));
                }
                if ($this->input->post('projects_prefix') == null || $this->input->post('projects_prefix') == null) {
                    Template::set_message('Please provide Project Prefix', 'error');
                    redirect(SITE_AREA . '/projectmgmt/projects/view_status' . "/" . $this->uri->segment(5));
                }
            }
            $pre = $this->db->query("select ip.entry_name from intg_pool_entry ip,intg_cost_centre cc where cc.pool_entry_id = ip.id and cc.id = " . $sbu->cost_centre_id . " and ip.deleted = 0 ")->row();

//			
//			
//			
//			$row = $this->db->query("SELECT COUNT(*) as numrows FROM intg_projects where final_status='Yes'")->row();
//			$i=$row->numrows;
//			$pro_prefix = "60BT-".$pre->prefix."".$i."A"; // Auto generated, OLD
            //ALIF ... CHANGE REQUEST ON 9 SEPT...System will never go to this if statement.always goes to else statement.
            if ($this->input->post('projects_prefix') == null || $this->input->post('projects_prefix') == "") {

                $mprefix = $this->db->query("select max(prefix_io_number) as mprefix from intg_projects")->row()->mprefix;
                $nofix = substr($mprefix, -4) + 1;
                //echo $mprefix."<br>";
                //echo $nofix; exit;
                switch (strlen($nofix)) {
                    case 1 : $prefix = "000" . $nofix;
                        break;
                    case 2 : $prefix = "00" . $nofix;
                        break;
                    case 3 : $prefix = "0" . $nofix;
                        break;
                    case 4 : $prefix = $nofix;
                        break;
                }
            } else {

                if ($this->input->post('projects_suffix') == null || $this->input->post('projects_suffix') == "") {
                    $projsuffix = $this->db->query("SELECT MAX(suffix_number) AS suffix FROM intg_projects WHERE prefix_number ='" . $this->input->post('projects_prefix') . "' ")->row()->suffix;

                    if ($projsuffix == "" || $projsuffix == NULL) {
                        $proj_suffix = '001';
                    } else {
                        $projsuffix = $projsuffix + 1;
                    }
                } else {
                    $projsuffix = $this->input->post('projects_suffix');
                }

                switch (strlen($projsuffix)) {
                    case 1 : $proj_suffix = "00" . $projsuffix;
                        break;
                    case 2 : $proj_suffix = "0" . $projsuffix;
                        break;
                    case 3 : $proj_suffix = $projsuffix;
                        break;
                }


                $prefix = $this->input->post('projects_prefix') . $proj_suffix;
            }

            //POPULATE WHOLE PROJECT CODE
            $pro_prefix = "60BT-" . $pre->entry_name . $prefix; // Manually key in, changed 2 March 2016, Alif
//                        $iscomplete = false;
//                        if (preg_match("/\b60BT-\b/i", $sbu->prefix_io_number)){
//                            $iscomplete = true; //code already created
//                        }
            //If user add a remarks
            if ($this->input->post('project_remarks')) {
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $t = time();
                $today = date("Y-m-d H:i:s", $t);

                $obj = json_decode($sbu->remarks, true);
                $data_push = array(
                    'datetime' => $today,
                    'text' => $this->input->post('project_remarks'),
                    'status' => $this->input->post('project_remarks'),
                    'status' => $this->input->post('projects_state'),
                    'user_id' => $this->auth->user_id()
                );


                array_push($obj, $data_push);

                $remarks_update = $this->db->query("UPDATE intg_projects SET remarks='" . json_encode($obj) . "'WHERE id = " . $this->uri->segment(5) . "");
            }

            if ($this->input->post('projects_state') == "Initiated" || $this->input->post('projects_state') == "Active") {

                $isexist = get_from_any_table("intg_projects", "prefix_io_number", "prefix_io_number", '"' . $pro_prefix . '"');
                if ($isexist == NULL || $isexist == '') {
                    $pre_update = $this->db->query("UPDATE intg_projects SET prefix_io_number='" . $pro_prefix . "'WHERE id = " . $this->uri->segment(5) . "");
                } else {
                    Template::set_message('Project Code ' . $pro_prefix . ' already exists, please enter new project code or contact System Administrator for further help. ', 'error');
                    redirect(SITE_AREA . '/projectmgmt/projects/view_status' . "/" . $this->uri->segment(5));
                }


                $pre_update = $this->db->query("UPDATE intg_projects SET status='" . $this->input->post('projects_state') . "',prefix_number = '" . $this->input->post('projects_prefix') . "', suffix_number = '" . $proj_suffix . "' WHERE id = " . $this->uri->segment(5) . "");

                //send email to all coworker
                //echo "select coworker from intg_projects where id = ".$this->uri->segment(5)."";
                $user_ids = $this->db->query("select coworker from intg_projects where id = " . $this->uri->segment(5) . "")->row()->coworker;
                $p_data = $this->db->query("select * from intg_projects where id = " . $this->uri->segment(5) . "")->row();
                //echo "<br>user_ids : ".$user_ids;

                if (!$user_ids)
                    $user_ids = array($this->auth->user_id());
                //echo "<br>user_ids : ".$user_ids."<br>";
                $this->assigned_to_model->assign_user($this->uri->segment(5), 'project', explode(",", $user_ids));
                $this->load->library('emailer/emailer');
                //                $users = $this->user_model->where_in('id', $user_ids)->find_all();
                $u = $this->db->query("select email,display_name from intg_users where id in (" . $user_ids . ")")->result();

                $creator = $this->user_model->find($this->auth->user_id());
                //                var_dump($users);
                //                exit();

                foreach ($u as $us) {

                    $this->emailer->addAddress($us->email, $us->display_name);
                    $dis = $us->display_name;
                    $this->emailer->Subject = '[Timesheet ACGT] A project is assigned to you';
                    $this->emailer->Body = $this->load->view('_emails/project_and_task', array('item_type' => 'project', 'item_name' => $p_data->project_name, 'assignee_name' => $us->display_name, 'start_date' => $p_data->project_start_date, 'end_date' => $p_data->project_end_date, 'creator_name' => $creator->display_name), TRUE);
                    //$this->emailer->Body = $this->load->view('_emails/project_and_task', array('item_type' => 'project', 'item_name' => $p_data->project_name,'assignee_name' => $user->email,'start_date' => $p_data->project_start_date, 'end_date' => $p_data->project_end_date, 'creator_name'=> $creator->display_name ), TRUE);
                    //Send notification here
                }
                $this->emailer->mail();
                //  echo '[Timesheet ACGT] A project is assigned to you'.$dis;
                // exit;
                //echo '[Timesheet ACGT] A project is assigned to you'.$user->display_name;
                //exit;
                // CSO TIMESHEET AND TIMESHEET DETAILS POPULATION
                if (!empty($this->config->item('csoulevelid'))) {
                    $this->load->model('timesheet_details/timesheet_details_model', null, true);
                    $this->timesheet_details_model->cso_timesheet_from_project($this->uri->segment(5));
                }
            } else {
                $this->load->library('workflow');

                list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();
                // make sure we only pass in the fields we want
                $data = array();
                //$data['parent_Rid'] = $this->input->post('parent_Rid');		
                //$typeworkflow=="none" ? $data['final_status'] = "none" : "";
                $data['final_status'] = "No";
                //$data['final_approvers'] = $work_flow_approvers;
                $data['initiator'] = $dataroleuser;
                //$data['prefix_io_number'] = $pro_prefix; 
                $data['status'] = $this->input->post('projects_state');

                $pre_update = $this->db->query("UPDATE intg_projects SET status='" . $this->input->post('projects_state') . "',final_status='No' WHERE id = " . $this->uri->segment(5) . "");


                $this->workflow->create_approvers($this->uri->segment(5));
                $this->sendmail($data['initiator'], $this->uri->segment(5), $url);

                Template::set_message('Changing project state to ' . $this->input->post('projects_state') . ' has been sent for approval', 'success');
                redirect(SITE_AREA . '/projectmgmt/projects/approval');
            }


            if (isset($pre_update)) {

                Template::set_message('Project Successfully ' . $this->input->post('projects_state') . '', 'success');
                redirect(SITE_AREA . '/projectmgmt/projects');
            } else {
                echo $pro_prefix;
                exit;
            }
        }
        // PDF Conversion AND SEND EMAIL
//        $this->load->helper('MY_Dompdf');
        $this->load->helper('email_template_modal');
        convertToPdfAndSendEmail();
        ///////////////////////////////////////////


        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));
        Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css'));

        Template::set('pro_status', array(
            'Initiated',
            /* 'Active', */
            'Suspended',
            'Completed',
            'Terminated'
        ));
        $data = array();
        $segments = $this->uri->segment_array();
        if (in_array('pdf_layout', $segments)) {
            $pdfFileName = "PDF_Project_" . time() . ".pdf";
            $this->load->library('MY_Dompdf');
            $this->load->config('dompdf_config', true);
            $html = $this->load->view('projectmgmt/project_to_pdf', $data, true);

            $dompdf = new MY_Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->set_base_path(base_url());
            $dompdf->render();
            $dompdf->stream($pdfFileName);
        } else {
//            Template::set_view('project_to_pdf');
            Template::render();
        }
    }

    public function ms_gantt() {
        Template::render();
    }

    public function milestone_chart() {


        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));
        Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css'));

        //Assets::add_css(Template::theme_url('js/dhtmlxgantt/dhtmlxgantt.css'));
        Assets::add_js('//export.dhtmlx.com/gantt/api.js');





        Template::render();
    }

    public function milestone_approval() {
//                    print_r($_POST);
        $this->load->model('milestone_approval_model', null, true);

        //fixing *******'s shitt
        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sapfield_ma', $this->input->post('select_field'));
                $this->session->set_userdata('sapfvalue_ma', $this->input->post('field_value'));
                $this->session->set_userdata('sapfname_ma', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('sapfield_ma');
                $this->session->unset_userdata('sapfvalue_ma');
                $this->session->unset_userdata('sapfname_ma');
                break;
        }

        if ($this->session->userdata('sapfield_ma') != '') {
            $field = $this->session->userdata('sapfield_ma');
        } else {
            $field = 'project_code';
        }

        if ($this->session->userdata('sapfield_ma') == 'All Fields') {

            $this->db->select('ima.*,ima.id as ima_id, ima.initiator as imainitiator, ip.project_name, im.milestone_name as milestone_name, iu.display_name,im.milestone_id AS milestone_id');
            $this->db->from('intg_milestone_approval ima');
            $this->db->join('intg_projects ip', 'ip.id = project_id ');
            $this->db->join('intg_milestones im', 'im.ms_approval_id =  ima.id ');
            $this->db->join('intg_users iu', 'iu.id = ima.initiator ');
            $this->db->where("(
                        ima.`initiator` = " . $this->auth->user_id() . " OR    
                        FIND_IN_SET( '" . $this->auth->user_id() . "',ima.final_approvers) OR   
                         FIND_IN_SET( '" . $this->auth->user_id() . "',ima.msother_approvers)    
                        )");
            $this->db->where("(
                     $this->db->       ip.`project_name`   like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR 
                           `project_code` like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                           milestone_name like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                           display_name like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                           ima.`created_on` like ('%" . $this->session->userdata('sapfvalue_ma') . "%')
                           )", NULL, FALSE);

            $this->db->group_by('im.ms_approval_id');
            $totalx = $this->db->get()->result();
            $total = count($totalx);
        } else {

            $this->db->select('ima.*,ima.id as ima_id, ima.initiator as imainitiator, ip.project_name, im.milestone_name as milestone_name, iu.display_name,im.milestone_id AS milestone_id');
            $this->db->from('intg_milestone_approval ima');
            $this->db->join('intg_projects ip', 'ip.id = project_id ');
            $this->db->join('intg_milestones im', 'im.ms_approval_id =  ima.id ');
            $this->db->join('intg_users iu', 'iu.id = ima.initiator ');
            $this->db->where("(
                        ima.`initiator` = " . $this->auth->user_id() . " OR    
                        FIND_IN_SET( '" . $this->auth->user_id() . "',ima.final_approvers) OR   
                         FIND_IN_SET( '" . $this->auth->user_id() . "',ima.msother_approvers)    
                        )");
            $this->db->like($field, $this->session->userdata('sapfvalue_ma'), 'both');
            $this->db->group_by('im.ms_approval_id');
            $totalx = $this->db->get()->result();
            $total = count($totalx);
        }

        //$records = $this->quote_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');

        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 15;
        $this->pager['num_links'] = 5;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }





        if ($this->session->userdata('sapfield_ma') == 'All Fields') {
            $records = $this->db
                            ->select('ima.*,ima.id as imaid, ima.initiator as imainitiator, ip.project_name, im.milestone_name as milestone_name')
                            ->from('intg_milestone_approval ima')
                            ->join('intg_projects ip', 'ip.id = project_id ')
                            ->join('intg_milestones im', 'im.ms_approval_id = ima.id ')
                            ->join('intg_users iu', 'iu.id = ima.initiator ')
                            ->where("(
                        ima.`initiator` = " . $this->auth->user_id() . " OR    
                        FIND_IN_SET( '" . $this->auth->user_id() . "',ima.final_approvers) OR   
                         FIND_IN_SET( '" . $this->auth->user_id() . "',ima.msother_approvers)    
                        )")
                            ->where("(
                            ip.`project_name`   like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR 
                           `project_code` like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                            milestone_name like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                            display_name like ('%" . $this->session->userdata('sapfvalue_ma') . "%') OR
                           ima.`created_on` like ('%" . $this->session->userdata('sapfvalue_ma') . "%')
                           )", NULL, FALSE)->group_by('im.ms_approval_id')
                            ->get()->result();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = $this->input->get('sort_by') . " " . $this->input->get('sort_order') . ",ima.id desc";
            } else {
                $ord1 = 'ima.id desc';
            }

            $records = $this->db
                            ->select('ima.*,ima.id as imaid, ima.initiator as imainitiator, ip.project_name, im.milestone_name as milestone_name')
                            ->from('intg_milestone_approval ima')
                            ->join('intg_projects ip', 'ip.id = project_id ')
                            ->join('intg_milestones im', 'im.ms_approval_id = ima.id ')
                            ->join('intg_users iu', 'iu.id = ima.initiator ')
                            ->where("(
                        ima.`initiator` = " . $this->auth->user_id() . " OR    
                        FIND_IN_SET( '" . $this->auth->user_id() . "',ima.final_approvers) OR   
                         FIND_IN_SET( '" . $this->auth->user_id() . "',ima.msother_approvers)    
                        )")
                            ->like($field, $this->session->userdata('sapfvalue_ma'), 'both')
                            ->limit($this->pager['per_page'], $offset)
                            ->group_by('im.ms_approval_id')
                            ->order_by($ord1)
                            ->get()->result();
        }

//        echo $this->db->last_query();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Template::set('records', $records);
//        Template::set('project_list', $this->db->query("select ima.project_id,ip.project_name from intg_milestone_approval ima JOIN intg_projects ip ON ima.project_id = ip.id where ima.initiator = '".$this->auth->user_id()."' or  FIND_IN_SET( '".$this->auth->user_id()."',ima.final_approvers) OR  FIND_IN_SET( '".$this->auth->user_id()."',ima.msother_approvers) ORDER BY ima.id DESC")->result()); 
        // end of shitttttttttttttttttttttt
        //$offset = $this->input->get('per_page');
        // $total = $this->db->query("select * from intg_milestone_approval where initiator = '".$this->auth->user_id()."' or  FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR  FIND_IN_SET( '".$this->auth->user_id()."',msother_approvers) ORDER BY id DESC")->num_rows();
        //$this->pagination->initialize($this->pager);
        //Template::set('current_url', current_url());
        //Template::set('total',$total);
        //Template::set('offset',$offset);
        //Template::set('rowcount',$this->pager['per_page']);
        //Template::set('records', $this->db->query("select * from intg_milestone_approval where initiator = '".$this->auth->user_id()."' or  FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR  FIND_IN_SET( '".$this->auth->user_id()."',msother_approvers) ORDER BY id DESC")->result()); 

        Template::render();
    }

    function milestone_quickapprove() {
        Template::render();
    }

    function milestone_updateApprovalstatus() {
        Template::render();
    }

    public function milestone() {
        // echo var_dump($_POST); exit;

        Assets::add_css(Template::theme_url('js/datepicker2/bootstrap-datepicker.css'));
        Assets::add_js(Template::theme_url("js/datepicker2/bootstrap-datepicker.js"));

        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);

        $uid = $this->auth->user_id();

        if ($this->uri->segment(5) == "insert_mu") {

            $id = $this->milestone_users_model->insert(array("milestones_id" => $this->input->post("milestone_id")));
            echo $id;
            exit;
        }

        if ($this->uri->segment(5) == "markcomplete") {
            $this->milestone_model->update($this->input->post("rowid"), array("completed_status" => "completed", "remarks" => $this->input->post("remarks")));
            exit;
        }



        if (isset($_POST['save']) || isset($_POST['create_version'])) {
            $this->load->model('timesheet/timesheet_model', null, true);
            $this->load->model('timesheet_details/timesheet_details_model', null, true);

            $id = $this->uri->segment(5);  // project id	
            $data_m = array();
            $data_m['project_id'] = $this->input->post('project_id');
            $data_m['initiator'] = $this->auth->user_id();
            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
            $data_m['start_date'] = date_format(date_create($this->input->post('projects_milestone_start_date')), "Y-m-d");
            $data_m['end_date'] = date_format(date_create($this->input->post('projects_milestone_end_date')), "Y-m-d");
            $data_m['created_date'] = date("Y-m-d");
            $data_m['created_by'] = $uid;
            $data_m['material_cost'] = $this->input->post('material_cost');
            $data_m['mcost_desc'] = $this->input->post('mcost_desc');
            $data_m['new_capex'] = $this->input->post('new_capex');
            $data_m['ncapex_desc'] = $this->input->post('ncapex_desc');
            $data_m['outsourcing'] = $this->input->post('outsourcing');
            $data_m['outsourcing_desc'] = $this->input->post('outsourcing_desc');
            $data_m['others'] = $this->input->post('others');
            $data_m['others_desc'] = $this->input->post('others_desc');

            $qmax = $this->db->query("select max(version_no) as rmax from intg_milestones where  parent_Rid='" . $this->input->post('milestone_id') . "'");
            $max = $qmax->row();
            if (isset($_POST['create_version'])) {
                $max_insert = $max->rmax + 1;
                $data_m['parent_Rid'] = $this->input->post('milestone_id');
                $data_m['version_no'] = $max_insert;
            } else {
                $max_insert = NULL;
                $data_m['parent_Rid'] = 0;
                $data_m['version_no'] = NULL;
            }

            $mid = $this->milestone_model->insert($data_m);

            $Level = $this->input->post('select');
            $Mandays = $this->input->post('mandays');
            $HeadCount = $this->input->post('headcount');
            $DailyFTE = $this->input->post('fte');


            foreach ($this->input->post('select') as $val => $key) {
                $data_mu = array();
                $data_mu['userlevel_id'] = $Level[$val];
                $data_mu['milestones_id'] = $mid;
                $data_mu['man_days'] = $Mandays[$val];
                $data_mu['head_count'] = $HeadCount[$val];
                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
                $data_mu['daily_fte'] = $DailyFTE[$val];
                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];

                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);

                $this->milestone_users_model->insert($data_mu);
            }

            if (isset($_POST['create_version'])) {
                $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');

                //$this->db->query("update intg_projects set project_cost =  $project_cost where id = '".$this->input->post('project_id')."'");
                $this->db->query("update intg_timesheet_details set status ='disabled' where mid = '" . $this->input->post('milestone_id') . "'"); // PLEASE CHECK ON THIS Alif 30 November 2017
            } else {
                if ($_POST['update'] != "save_versioned_ms") {
                    $project_cost = $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');
                    $this->db->query("update intg_projects set project_cost = project_cost + $project_cost where id = '" . $this->input->post('project_id') . "'");
                }
            }


            $taskpool = $this->input->post('task_pool');

            foreach ($this->input->post('task_pool') as $t => $k) {
                $data_mt = array();
                $data_mt['project_id'] = $this->input->post('project_id');
                $data_mt['milestone_id'] = $mid;
                $data_mt['task_pool_id'] = $taskpool[$t];
                $data_mt['created_date'] = date("Y-m-d");
                $data_mt['created_by'] = $uid;

                $this->milestone_tasks_model->insert($data_mt);
            }

            if ($_POST['save'] == "Save as Draft") {

                Template::set_message('Project & Milestones successfully saved as draft', 'success');
                redirect(SITE_AREA . '/projectmgmt/projects/draft');
            }

            if ($_POST['save'] == "Save &  Create New Milestone") {

                Template::set_message('Project & Milestones saved successfully', 'success');
                //redirect(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(5));
            }

            if ($_POST['save'] == "Submit For Approval") {
                //Alif: Workflow Codes, load workflow libraries..moved from create
                $this->load->library('workflow');

                //Alif: Workflow Codes, check workflow..moved from create
                list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();

                //Workflow 
                //echo "<br>ID : "+$id."<br>"; exit();
                //Workflow vars..Update the intg_projects
                $data_p = array();
                $project_id = $this->input->post('project_id');
                $typeworkflow == "none" ? $data['final_status'] = "none" : "";
                $data_p['final_approvers'] = $work_flow_approvers;
                $data_m['initiator'] = $this->auth->user_id();
                $data_p['other_approvers'] = $this->projects_model->get_other_approvers();
                $data_p['row_status'] = "final";


                //Update
                $this->projects_model->skip_validation(true); //set skip validation to true, Alif
                $this->projects_model->update($project_id, $data_p);
                //$this->workflow->create_approvers($project_id);
                $this->workflow->create_approvers($project_id);
                $this->sendmail($this->auth->user_id(), $project_id, $url);




                Template::set_message('Project Submitted For Approval', "success");
                redirect(SITE_AREA . '/projectmgmt/projects/approval');
            }
        }

        if (isset($_POST['update'])) {

//            echo var_dump($_POST); exit;

            $id = $this->input->post('milestone_id');
            $pid = $this->input->post('project_id');


            $uid = $this->auth->user_id();

            $data_m = array();

            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
            $data_m['start_date'] = date("Y-m-d", strtotime($this->input->post('projects_milestone_start_date')));
            $data_m['end_date'] = date("Y-m-d", strtotime($this->input->post('projects_milestone_end_date')));

            $data_m['material_cost'] = $this->input->post('material_cost');
            $data_m['mcost_desc'] = $this->input->post('mcost_desc');
            $data_m['new_capex'] = $this->input->post('new_capex');
            $data_m['ncapex_desc'] = $this->input->post('ncapex_desc');
            $data_m['outsourcing'] = $this->input->post('outsourcing');
            $data_m['outsourcing_desc'] = $this->input->post('outsourcing_desc');
            $data_m['others'] = $this->input->post('others');
            $data_m['others_desc'] = $this->input->post('others_desc');


            $this->milestone_model->update($id, $data_m);

            // echo $this->db->last_query();



            $Level = $this->input->post('select');
            $Mandays = $this->input->post('mandays');
            $HeadCount = $this->input->post('headcount');
            $DailyFTE = $this->input->post('fte');
            $UiD = $this->input->post('uid');

            $this->milestone_users_model->delete_where(array("milestones_id" => $id));
            //echo $this->db->last_query();	
            foreach ($this->input->post('select') as $val => $key) {



                $data_mu = array();
                $data_mu['userlevel_id'] = $Level[$val];
                $data_mu['milestones_id'] = $id;
                $data_mu['man_days'] = $Mandays[$val];
                $data_mu['head_count'] = $HeadCount[$val];
                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
                $data_mu['daily_fte'] = $DailyFTE[$val];
                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];
                $data_mu['milestones_id'] = $id;

                $this->milestone_users_model->insert($data_mu);

                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);
            }

            if ($_POST['update'] != "save_versioned_ms") {
                $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');

                $this->db->query("update intg_projects set project_cost =  $project_cost where id = '" . $this->input->post('project_id') . "'");
            }

            $taskpool = $this->input->post('task_pool');
            $taskid = implode(",", $this->input->post('task_id'));


            $this->db->query("delete  from intg_milestones_tasks where task_id IN ( " . $taskid . " )");
//echo $this->db->last_query();


            foreach ($this->input->post('task_pool') as $t => $k) {
                $data_mt = array();
                $data_mt['project_id'] = $pid;
                $data_mt['milestone_id'] = $id;
                $data_mt['task_pool_id'] = $taskpool[$t];
                $data_mt['created_date'] = date("Y-m-d");
                $data_mt['created_by'] = $uid;

                $this->milestone_tasks_model->insert($data_mt);
                //echo $this->db->last_query(); 
            }
        }

        if (isset($_POST['m_approval'])) {
            $this->load->library('workflow1');
            list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow1->checkflow1();
            if ($typeworkflow == 'none') {
                Template::set_message("No Workflow Available, Please Contact Administrator", 'error');
                redirect(SITE_AREA . '/projectmgmt/projects');
            }
            $data = array();

            $data['final_status'] = "No";
            $data['initiator'] = $dataroleuser;

            $other_approvers = $this->db->query('SELECT work_flow_approvers '
                            . 'FROM intg_work_flow '
                            . 'WHERE work_flow_doc_generator = ' . $this->auth->user_id() . ' '
                            . 'and work_flow_module_id = "42"')->result();


            foreach ($other_approvers as $b) {
                $options[] = $b->work_flow_approvers;
            }


            $result_milestone = $this->db->query("INSERT INTO "
                    . "`intg_milestone_approval`(`final_status`,"
                    . " `initiator`,"
                    . " `msother_approvers`,"
                    . "`final_approvers`,"
                    . " `project_code`,"
                    . " `created_by`,"
                    . " `project_id`) "
                    . "VALUES ('No'," . $this->auth->user_id() . ",'" . implode(",", $options) . "','" . $work_flow_approvers . "','" . $this->input->post("project_code") . "'," . $this->auth->user_id() . "," . $this->uri->segment(5) . ")");

            $last_id = $this->db->insert_id();


            $versioned_ms = $this->db->query("Select * from intg_milestones "
//                    . "where (STATUS = 'No' OR STATUS = 'Deactivated') " //alif added ,VO, Dec 05 2017
                            . "where STATUS = 'No' " //alif added ,VO, Dec 05 2017
                            . "AND created_by = " . $this->auth->user_id() . "  "
                            . "AND project_id = '" . $this->uri->segment(5) . "' ")->result();

            $datams = array();
            foreach ($versioned_ms as $mid) {
                $datams['milestone_name'] = $mid->milestone_name;
                $datams['start_date'] = $mid->start_date;
                $datams['end_date'] = $mid->end_date;

                $this->milestone_model->update($mid->milestone_id, array("ms_approval_id" => $last_id));
            }



            $this->workflow1->create_approvers($last_id);
            $this->mssendmail($data['initiator'], $last_id, $url, $datams);

            if (isset($result_milestone)) {
                //$this->db->query("UPDATE `intg_milestones` SET `status`='No' WHERE `project_id`= ".$this->uri->segment(5)."");
                Template::set_message('Milestone successfully sent for approval', 'success');
                //	redirect(SITE_AREA .'/projectmgmt/projects/milestone_approval');
            } else {
                echo "not successful";
                exit;
            }

            //exit;
        }
        /**/
        Template::render();
    }

    public function edit_milestone() {

        /* Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
          Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));
          Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css')); */

        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);


        Template::render();
    }

    public function deletems() {



        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->milestone_users_model->delete($this->input->post('murowid'));

        if ($this->uri->segment(5) == "edit") {
            $this->db->query("update intg_projects set project_cost =  project_cost - " . $_POST['timecost'] . " where id = '" . $this->input->post('project_id') . "'");
        }
        //echo $this->db->last_query(); exit;
    }

    public function view() {
        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);

        $uid = $this->auth->user_id();
        if ($this->uri->segment(5) == "insert_mu") {
            $id = $this->milestone_users_model->insert(array("milestones_id" => $this->input->post("milestone_id")));
            echo $id;
            exit;
        }

        if ($this->uri->segment(5) == "markcomplete") {
            $this->milestone_model->update($this->input->post("rowid"), array("completed_status" => "completed", "remarks" => $this->input->post("remarks")));
            exit;
        }




        if (isset($_POST['save']) || isset($_POST['create_version'])) {

            //print_r($_POST); exit;

            $id = $this->uri->segment(5);

            $data_m = array();
            $data_m['project_id'] = $this->input->post('project_id');
            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
            $data_m['start_date'] = date_format(date_create($this->input->post('projects_milestone_start_date')), "Y-m-d");
            $data_m['end_date'] = date_format(date_create($this->input->post('projects_milestone_end_date')), "Y-m-d");
            $data_m['created_date'] = date("Y-m-d");
            $data_m['created_by'] = $uid;
            $data_m['material_cost'] = $this->input->post('material_cost');
            $data_m['new_capex'] = $this->input->post('new_capex');
            $data_m['outsourcing'] = $this->input->post('outsourcing');
            $data_m['others'] = $this->input->post('others');

            $qmax = $this->db->query("select max(version_no) as rmax from intg_milestones where  parent_Rid='" . $this->input->post('milestone_id') . "'");
            $max = $qmax->row();
            if (isset($_POST['create_version'])) {
                $max_insert = $max->rmax + 1;
                $data_m['parent_Rid'] = $this->input->post('milestone_id');
                $data_m['version_no'] = $max_insert;
            } else {
                $max_insert = NULL;
                $data_m['parent_Rid'] = 0;
                $data_m['version_no'] = NULL;
            }



            $mid = $this->milestone_model->insert($data_m);

            //echo $mid;
            //echo $this->db->last_query(); 



            $Level = $this->input->post('select');
            $Mandays = $this->input->post('mandays');
            $HeadCount = $this->input->post('headcount');
            $DailyFTE = $this->input->post('fte');


            foreach ($this->input->post('select') as $val => $key) {
                $data_mu = array();
                $data_mu['userlevel_id'] = $Level[$val];
                $data_mu['milestones_id'] = $mid;
                $data_mu['man_days'] = $Mandays[$val];
                $data_mu['head_count'] = $HeadCount[$val];
                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
                $data_mu['daily_fte'] = $DailyFTE[$val];
                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];

                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);

                $this->milestone_users_model->insert($data_mu);
            }

            if (isset($_POST['create_version'])) {
                $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');

                $this->db->query("update intg_projects set project_cost =  $project_cost where id = '" . $this->input->post('project_id') . "'");
                $this->db->query("update intg_timesheet_details set status ='disabled' where mid = '" . $this->input->post('milestone_id') . "'");
            } else {

                $project_cost = $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');

                $this->db->query("update intg_projects set project_cost = project_cost + $project_cost where id = '" . $this->input->post('project_id') . "'");
            }


            $taskpool = $this->input->post('task_pool');

            foreach ($this->input->post('task_pool') as $t => $k) {
                $data_mt = array();
                $data_mt['project_id'] = $this->input->post('project_id');
                $data_mt['milestone_id'] = $mid;
                $data_mt['task_pool_id'] = $taskpool[$t];
                $data_mt['created_date'] = date("Y-m-d");
                $data_mt['created_by'] = $uid;

                $this->milestone_tasks_model->insert($data_mt);
            }

            if ($_POST['save'] == "Save_n_Exit") {
                Template::set_message('Project & Milestones saved successfully', 'success');
                redirect(SITE_AREA . '/projectmgmt/projects');
            }

            if ($_POST['save'] == "Save &  Create New Milestone") {

                Template::set_message('Project & Milestones saved successfully', 'success');
                //redirect(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(5));
            }
        }

        if (isset($_POST['update'])) {


            $id = $this->input->post('milestone_id');
            $pid = $this->input->post('project_id');


            $uid = $this->auth->user_id();

            $data_m = array();

            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
            $data_m['end_date'] = date_format(date_create($this->input->post('projects_milestone_end_date')), "Y-m-d");
            $data_m['material_cost'] = $this->input->post('material_cost');
            $data_m['new_capex'] = $this->input->post('new_capex');
            $data_m['outsourcing'] = $this->input->post('outsourcing');
            $data_m['others'] = $this->input->post('others');


            $this->milestone_model->update($id, $data_m);



            $Level = $this->input->post('select');
            $Mandays = $this->input->post('mandays');
            $HeadCount = $this->input->post('headcount');
            $DailyFTE = $this->input->post('fte');
            $UiD = $this->input->post('uid');


            foreach ($this->input->post('select') as $val => $key) {
                $data_mu = array();
                $data_mu['userlevel_id'] = $Level[$val];
                $data_mu['milestones_id'] = $id;
                $data_mu['man_days'] = $Mandays[$val];
                $data_mu['head_count'] = $HeadCount[$val];
                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
                $data_mu['daily_fte'] = $DailyFTE[$val];
                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];
                $data_mu['milestones_id'] = $id;
                $this->milestone_users_model->update($UiD[$val], $data_mu);

                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);
            }

            $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');

            $this->db->query("update intg_projects set project_cost =  $project_cost where id = '" . $this->input->post('project_id') . "'");


            $taskpool = $this->input->post('task_pool');
            $taskpoolold = $this->input->post('taskpoolold');
            $taskid = $this->input->post('task_id');

            $res = array_diff($taskpool, $taskpoolold);
            /*
              print_r($taskpool);
              print_r($taskpoolold);
              print_r($res); */


            if ($res = "") {
                
            } else {

                //print_r($_POST);
                //echo "PID".$this->input->post('project_id');


                $this->db->where_in('task_id', $taskid);
                $this->db->delete('intg_milestones_tasks');

                foreach ($this->input->post('task_pool') as $t => $k) {
                    $data_mt = array();
                    $data_mt['project_id'] = $pid;
                    $data_mt['milestone_id'] = $id;
                    $data_mt['task_pool_id'] = $taskpool[$t];
                    $data_mt['created_date'] = date("Y-m-d");
                    $data_mt['created_by'] = $uid;

                    $this->milestone_tasks_model->insert($data_mt);
                    //echo $this->db->last_query(); 
                }


//echo $this->db->last_query(); 
            }

            /* if ( $_POST['update']=="Save_n_Exit"  ) {
              Template::set_message('Project & Milestones saved successfully', 'success');
              redirect(SITE_AREA .'/projectmgmt/projects');

              }

              if ( $_POST['update']=="Save &  Create New Milestone" ) {

              Template::set_message('Project & Milestones saved successfully', 'success');
              redirect(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(5));

              } */
        }

        Assets::add_css('js/datepicker/datepicker.css');
        Assets::add_js('js/datepicker/bootstrap-datepicker.js');
        Template::render();
    }

    public function view_milestone() {
        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);

        $uid = $this->auth->user_id();
        if ($this->uri->segment(5) == "insert_mu") {
            $id = $this->milestone_users_model->insert(array("milestones_id" => $this->input->post("milestone_id")));
            echo $id;
            exit;
        }

        if ($this->uri->segment(5) == "markcomplete") {
            $this->milestone_model->update($this->input->post("rowid"), array("completed_status" => "completed", "remarks" => $this->input->post("remarks")));
            exit;
        }




//        if (isset($_POST['save']) || isset($_POST['create_version'])) {
//
//            //print_r($_POST); exit;
//
//            $id = $this->uri->segment(5);
//
//            $data_m = array();
//            $data_m['project_id'] = $this->input->post('project_id');
//            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
//            $data_m['start_date'] = date_format(date_create($this->input->post('projects_milestone_start_date')), "Y-m-d");
//            $data_m['end_date'] = date_format(date_create($this->input->post('projects_milestone_end_date')), "Y-m-d");
//            $data_m['created_date'] = date("Y-m-d");
//            $data_m['created_by'] = $uid;
//            $data_m['material_cost'] = $this->input->post('material_cost');
//            $data_m['new_capex'] = $this->input->post('new_capex');
//            $data_m['outsourcing'] = $this->input->post('outsourcing');
//            $data_m['others'] = $this->input->post('others');
//
//            $qmax = $this->db->query("select max(version_no) as rmax from intg_milestones where  parent_Rid='" . $this->input->post('milestone_id') . "'");
//            $max = $qmax->row();
//            if (isset($_POST['create_version'])) {
//                $max_insert = $max->rmax + 1;
//                $data_m['parent_Rid'] = $this->input->post('milestone_id');
//                $data_m['version_no'] = $max_insert;
//            } else {
//                $max_insert = NULL;
//                $data_m['parent_Rid'] = 0;
//                $data_m['version_no'] = NULL;
//            }
//
//
//
//            $mid = $this->milestone_model->insert($data_m);
//
//            //echo $mid;
//            //echo $this->db->last_query(); 
//
//
//
//            $Level = $this->input->post('select');
//            $Mandays = $this->input->post('mandays');
//            $HeadCount = $this->input->post('headcount');
//            $DailyFTE = $this->input->post('fte');
//
//
//            foreach ($this->input->post('select') as $val => $key) {
//                $data_mu = array();
//                $data_mu['userlevel_id'] = $Level[$val];
//                $data_mu['milestones_id'] = $mid;
//                $data_mu['man_days'] = $Mandays[$val];
//                $data_mu['head_count'] = $HeadCount[$val];
//                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
//                $data_mu['daily_fte'] = $DailyFTE[$val];
//                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];
//
//                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);
//
//                $this->milestone_users_model->insert($data_mu);
//            }
//
//            if (isset($_POST['create_version'])) {
//                $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');
//
//                $this->db->query("update intg_projects set project_cost =  $project_cost where id = '" . $this->input->post('project_id') . "'");
//                $this->db->query("update intg_timesheet_details set status ='disabled' where mid = '" . $this->input->post('milestone_id') . "'");
//            } else {
//
//                $project_cost = $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');
//
//                $this->db->query("update intg_projects set project_cost = project_cost + $project_cost where id = '" . $this->input->post('project_id') . "'");
//            }
//
//
//            $taskpool = $this->input->post('task_pool');
//
//            foreach ($this->input->post('task_pool') as $t => $k) {
//                $data_mt = array();
//                $data_mt['project_id'] = $this->input->post('project_id');
//                $data_mt['milestone_id'] = $mid;
//                $data_mt['task_pool_id'] = $taskpool[$t];
//                $data_mt['created_date'] = date("Y-m-d");
//                $data_mt['created_by'] = $uid;
//
//                $this->milestone_tasks_model->insert($data_mt);
//            }
//
//            if ($_POST['save'] == "Save & Exit") {
//                Template::set_message('Project & Milestones saved successfully', 'success');
//                redirect(SITE_AREA . '/projectmgmt/projects');
//            }
//
//            if ($_POST['save'] == "Save &  Create New Milestone") {
//
//                Template::set_message('Project & Milestones saved successfully', 'success');
//                //redirect(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(5));
//            }
//        }
//
//        if (isset($_POST['update'])) {
//
//
//            $id = $this->input->post('milestone_id');
//            $pid = $this->input->post('project_id');
//
//
//            $uid = $this->auth->user_id();
//
//            $data_m = array();
//
//            $data_m['milestone_name'] = $this->input->post('projects_mile_stone');
//            $data_m['end_date'] = date_format(date_create($this->input->post('projects_milestone_end_date')), "Y-m-d");
//            $data_m['material_cost'] = $this->input->post('material_cost');
//            $data_m['new_capex'] = $this->input->post('new_capex');
//            $data_m['outsourcing'] = $this->input->post('outsourcing');
//            $data_m['others'] = $this->input->post('others');
//
//
//            $this->milestone_model->update($id, $data_m);
//
//
//
//            $Level = $this->input->post('select');
//            $Mandays = $this->input->post('mandays');
//            $HeadCount = $this->input->post('headcount');
//            $DailyFTE = $this->input->post('fte');
//            $UiD = $this->input->post('uid');
//
//
//            foreach ($this->input->post('select') as $val => $key) {
//                $data_mu = array();
//                $data_mu['userlevel_id'] = $Level[$val];
//                $data_mu['milestones_id'] = $id;
//                $data_mu['man_days'] = $Mandays[$val];
//                $data_mu['head_count'] = $HeadCount[$val];
//                $data_mu['total_man_days'] = $Mandays[$val] * $HeadCount[$val];
//                $data_mu['daily_fte'] = $DailyFTE[$val];
//                $data_mu['time_cost'] = $Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val];
//                $data_mu['milestones_id'] = $id;
//                $this->milestone_users_model->update($UiD[$val], $data_mu);
//
//                $project_cost = $project_cost + ($Mandays[$val] * $HeadCount[$val] * $DailyFTE[$val]);
//            }
//
//            $project_cost = $_POST['new_proj_value'] + $project_cost + $this->input->post('material_cost') + $this->input->post('new_capex') + $this->input->post('outsourcing') + $this->input->post('others');
//
//            $this->db->query("update intg_projects set project_cost =  $project_cost where id = '" . $this->input->post('project_id') . "'");
//
//
//            $taskpool = $this->input->post('task_pool');
//            $taskpoolold = $this->input->post('taskpoolold');
//            $taskid = $this->input->post('task_id');
//
//            $res = array_diff($taskpool, $taskpoolold);
//            /*
//              print_r($taskpool);
//              print_r($taskpoolold);
//              print_r($res); */
//
//
//            if ($res = "") {
//                
//            } else {
//
//                //print_r($_POST);
//                //echo "PID".$this->input->post('project_id');
//
//
//                $this->db->where_in('task_id', $taskid);
//                $this->db->delete('intg_milestones_tasks');
//
//                foreach ($this->input->post('task_pool') as $t => $k) {
//                    $data_mt = array();
//                    $data_mt['project_id'] = $pid;
//                    $data_mt['milestone_id'] = $id;
//                    $data_mt['task_pool_id'] = $taskpool[$t];
//                    $data_mt['created_date'] = date("Y-m-d");
//                    $data_mt['created_by'] = $uid;
//
//                    $this->milestone_tasks_model->insert($data_mt);
//                    //echo $this->db->last_query(); 
//                }
//
//
////echo $this->db->last_query(); 
//            }
//
//            /* if ( $_POST['update']=="Save & Exit"  ) {
//              Template::set_message('Project & Milestones saved successfully', 'success');
//              redirect(SITE_AREA .'/projectmgmt/projects');
//
//              }
//
//              if ( $_POST['update']=="Save &  Create New Milestone" ) {
//
//              Template::set_message('Project & Milestones saved successfully', 'success');
//              redirect(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(5));
//
//              } */
//        }

        Template::render();
    }

    public function HCgraph() {

        /* 	Assets::add_js(Template::theme_url('js/HC/highcharts.js'));
          Assets::add_js(Template::theme_url('js/HC//modules/exporting.js'));
          Assets::add_css('js/datepicker/datepicker.css');
          Assets::add_js("js/datepicker/bootstrap-datepicker.js"); */


        /* if( $this->input->post('datefrom') !="" && $this->input->post('dateto') !="" ) { $between = "and progress_date between '".date('Y-m-d',strtotime($this->input->post('datefrom')))."' and '".date('Y-m-d',strtotime($this->input->post('dateto')))."' "; }
          else { $between=" "; } */

        /* $a = array(1, 2, 3);
          $b = array("one", "two", "three", "four", "five");
          $c = array("uno", "dos", "tres", "cuatro", "cinco");

          $d = array_map(null, $a, $b, $c);
          var_dump($d); */


        // get all the task belong to id and record its weight
        $tasks = $this->db->query("select id,weightage_percentage from intg_tasks where project_id = '" . $this->input->get('project') . "'")->result();
        //echo $this->db->last_query();

        foreach ($tasks as $t) {

            $expa = $this->db->query("select * from intg_project_progress where task_id = '" . $t->id . "' and context = 'E'  order by time_stamp asc")->result();

            //echo $this->db->last_query();

            foreach ($expa as $e) {

                $myArray[] = array("task" => $e->task_id, "date" => date("Y-m-d", strtotime($e->time_stamp)), "perc" => $e->progress / 100 * $t->weightage_percentage);

                //$expected[] =  '[gd('.date("Y, m, d",strtotime($e->time_stamp)).'),'.$e->progress.']'; 	
            }

            //$ach = $this->db->query("select * from intg_project_progress where task_id = '".$t->id."' and  context = 'A'  order by time_stamp asc")->result();
            foreach ($ach as $a) {

                //$achieved[] =  '[gd('.date("Y, m, d",strtotime($a->time_stamp)).'),'.$a->progress.']'; 	
            }
        }
        $sumArray = array();

        foreach ($myArray as $k => $subArray) {
            foreach ($subArray as $id => $value) {
                // echo  $sumArray[$id]+=$value;
            }
            // echo "<br>";
        }



        Template::set('myArray', $myArray);
        Template::set('achieved', $achieved);





        Template::render();
    }

    public function graph($project_id) {
        $this->load->model('project_progress/project_progress_model', null, true);

        $expected = $this->project_progress_model->select('task_id, time_stamp, progress')->where(array('context' => 'E'))->order_by('time_stamp', 'desc')->find_all();
        $achieved = $this->project_progress_model->select('task_id, time_stamp, progress')->where(array('project_id' => $project_id, 'context' => 'A'))->order_by('time_stamp', 'desc')->find_all();

        Template::set('expected', $expected);
        Template::set('achieved', $achieved);

        $this->load->model('tasks/tasks_model', null, true);
        Template::set('tasks', $this->tasks_model->select('id, task_name, weightage_percentage')->where(array('project_id' => $project_id))->find_all());

        Template::set('project', $this->projects_model->get_projects($project_id));

        Assets::add_js(Template::theme_url('js/HC/highcharts.js'));
        Assets::add_js(Template::theme_url('js/HC//modules/exporting.js'));
        Assets::add_css('js/datepicker/datepicker.css');
        Assets::add_js('js/datepicker/bootstrap-datepicker.js');

        Template::render();
    }

    public function delete($id) {
        $project = $this->projects_model->find($id);
        if ($project->created_by == $this->auth->user_id())
            $this->projects_model->delete($id);
        redirect(SITE_AREA . '/projectmgmt/projects');
    }

    public function all($id = 0) {
        echo json_encode($this->projects_model->get_projects($id));
    }

    //--------------------------------------------------------------------
    public function createnew() {

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));
        $this->auth->restrict('Projects.Projectmgmt.Create');

        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $this->load->model('milestone_tasks/milestone_tasks_model', null, true);

        $this->load->library('workflow');

        list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();

        //check if user@role have workflow or not
        if ($typeworkflow == 'none') {
            Template::set_message("No Workflow Available, Please Contact Administrator", 'error');
            redirect(SITE_AREA . '/projectmgmt/projects');
        }

        if (isset($_POST['save']) || isset($_POST['draft'])) {
            if ($inserted = $this->save_projects()) {
                // Log the activity
                log_activity($this->current_user->id, lang('projects_act_create_record') . ': ' . $inserted['id'] . ' : ' . $this->input->ip_address(), 'projects');
                if (isset($_POST['ajax'])) {
                    echo json_encode($inserted);
                    exit;
                } else {
                    $this->input->post('draft') == "Save as Draft" ? $lang = lang('projects_edit_draft_success') : $lang = lang('projects_edit_success');
                    Template::set_message($lang, 'success');

                    if ($this->input->post('save') == "Save & Create Milestone(s)") {

                        if ($this->input->post('rejected_project_id') != '') {

                            //get the rejected milestone
                            $rejected_ms = $this->db->query("select
                                                                    milestone_id,
                                                                    initiator,
                                                                    milestone_name,
                                                                    start_date,
                                                                    end_date,
                                                                    material_cost,
                                                                    mcost_desc,
                                                                    new_capex,
                                                                    ncapex_desc,
                                                                    outsourcing,
                                                                    outsourcing_desc,
                                                                    others,
                                                                    others_desc,
                                                                    remarks from intg_milestones m 
                                                                    where  m.project_id = '" . $this->input->post('rejected_project_id') . "' ")->result();

                            foreach ($rejected_ms as $val) {
                                $rejected_mid = $val->milestone_id;
                                unset($val->milestone_id);
                                ////1- intg_milestones data prep
                                $val->parent_Rid = 0;
                                $val->version_no = NULL;
                                $val->project_id = $inserted['id'];
                                $val->created_date = date("Y-m-d");
                                $val->created_by = $this->auth->user_id();
                                $new_mid = $this->milestone_model->insert($val);

                                //loop each ms to get the ms related data,then copy to new ms id.
                                //2- intg_milestones_users
                                $rejected_mu = $this->db->query("select
                                                                userlevel_id,
                                                                man_days,
                                                                head_count,
                                                                total_man_days,
                                                                daily_fte,
                                                                time_cost from intg_milestones_users where milestones_id='" . $rejected_mid . "'")->result();

                                foreach ($rejected_mu as $val_mu) {
                                    $val_mu->milestones_id = $new_mid;
                                    $this->milestone_users_model->insert($val_mu);
                                }

                                //3- intg_milestones_tasks data prep
                                $rejected_mt = $this->db->query("select task_pool_id from intg_milestones_tasks where milestone_id='" . $rejected_mid . "'")->result();

                                foreach ($rejected_mt as $val_mt) {
                                    $val_mt->milestone_id = $new_mid;

                                    $val_mt->project_id = $inserted['id'];
                                    $val_mt->milestone_id = $new_mid;
                                    $val_mt->created_date = date("Y-m-d");
                                    $val_mt->created_by = $this->auth->user_id();

                                    $this->milestone_tasks_model->insert($val_mt);
                                }
                            }
                        }


                        redirect(SITE_AREA . '/projectmgmt/projects/milestone/' . $inserted['id'] . '/' . $this->input->post('rejected_project_id') . '/');
                    } else {
                        redirect(SITE_AREA . '/projectmgmt/projects/draft');
                    }
                }
            } else {

                if ($this->upload->display_multi_errors()) {
                    Template::set_message("Upload Error" . $this->upload->display_multi_errors(), 'error');
                } else {
                    if (isset($_POST['ajax'])) {
                        echo json_encode(array('error' => $this->projects_model->error));
                        exit;
                    } else {
                        Template::set_message(lang('projects_create_failure') . $this->projects_model->error, 'error');
                    }
                }
            }
        }

        $this->load->model('currency/currency_model', null, true);
        Template::set('currencies', $this->currency_model->find_all());

        $this->load->model('companies/companies_model', null, true);
        Template::set('company', $company = $this->companies_model->find($this->auth->company_id()));

        
        $preferred_currency = $this->currency_model->find($company->currency_id);
        if (!$preferred_currency) {
            $preferred_currency = new stdClass();
            $preferred_currency->converted_currency = 'MYR';
        }
        Template::set('preferred_currency', $preferred_currency);

        $this->load->model('clients/clients_model', null, true);
        Template::set('clients', $this->clients_model->find_all());
//		Template::set('users', $this->user_model->find_all_by(array('company_id' => $this->auth->company_id(),'intg_users.deleted' => '0')));
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('authed_user', $this->user_model->find($this->auth->user_id()));
        Template::set('projects', reset($this->projects_model->get_projects($this->uri->segment(5))));
        Template::set('toolbar_title', lang('projects_create') . ' Projects');

        Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-regularfont.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-glyphicons.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-fontawesome.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.css'));
        Assets::add_js(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.js'));
        Template::render();
    }

    public function createnew2() {

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));

        /* 	Assets::add_css(Template::theme_url('js/dist/css/jquery.gridmanager.css'));
          Assets::add_js(Template::theme_url('js/dist/js/jquery.gridmanager.min.js')); */
        Assets::add_css(Template::theme_url('js/dist/grideditor.css'));
        Assets::add_js(Template::theme_url('js/dist/jquery.grideditor.min.js'));




        $this->auth->restrict('Projects.Projectmgmt.Create');

        if (isset($_POST['save']) || isset($_POST['draft'])) {
            if ($inserted = $this->save_projects()) {
                // Log the activity
                log_activity($this->current_user->id, lang('projects_act_create_record') . ': ' . $inserted['id'] . ' : ' . $this->input->ip_address(), 'projects');
                if (isset($_POST['ajax'])) {
                    echo json_encode($inserted);
                    exit;
                } else {
                    Template::set_message(lang('projects_create_success'), 'success');
                    redirect(SITE_AREA . '/projectmgmt/projects/milestone/' . $inserted['id']);
                }
            } else {

                if ($this->upload->display_multi_errors()) {
                    Template::set_message("Upload Error" . $this->upload->display_multi_errors(), 'error');
                } else {
                    if (isset($_POST['ajax'])) {
                        echo json_encode(array('error' => $this->projects_model->error));
                        exit;
                    } else {
                        Template::set_message(lang('projects_create_failure') . $this->projects_model->error, 'error');
                    }
                }
            }
        }

        $this->load->model('currency/currency_model', null, true);
        Template::set('currencies', $this->currency_model->find_all());

        $this->load->model('companies/companies_model', null, true);
        Template::set('company', $company = $this->companies_model->find($this->auth->company_id()));

        $preferred_currency = $this->currency_model->find($company->currency_id);
        if (!$preferred_currency) {
            $preferred_currency = new stdClass();
            $preferred_currency->converted_currency = 'MYR';
        }
        Template::set('preferred_currency', $preferred_currency);

        $this->load->model('clients/clients_model', null, true);
        Template::set('clients', $this->clients_model->find_all());
        Template::set('users', $this->user_model->find_all_by(array('company_id' => $this->auth->company_id())));
        Template::set('authed_user', $this->user_model->find($this->auth->user_id()));
        Template::set('toolbar_title', lang('projects_create') . ' Projects');

        Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-regularfont.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-glyphicons.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-fontawesome.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.css'));
        Assets::add_js(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.js'));
        Template::render();
    }

    /**
     * Allows editing of Projects data.
     *
     * @return void
     */
    public function edit() {
//            echo $this->input->post('draft');exit();

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('projects_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/projects');
        }

        if (isset($_POST['save']) || isset($_POST['draft'])) {
            $this->auth->restrict('Projects.Projectmgmt.Edit');

            if ($this->save_projects('update', $id)) {
                // Log the activity

                log_activity($this->current_user->id, lang('projects_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'projects');

                $this->input->post('draft') == "Save as Draft" ? $lang = lang('projects_edit_draft_success') : $lang = lang('projects_edit_success');
                Template::set_message($lang, 'success');

                if ($_POST['save'] == "Save & Next") {
                    redirect(SITE_AREA . '/projectmgmt/projects/milestone/' . $id);
                } else {
                    redirect(SITE_AREA . '/projectmgmt/projects/');
                }
            } else {
                Template::set_message(lang('projects_edit_failure') . $this->projects_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Projects.Projectmgmt.Delete');

            if ($this->projects_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('projects_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'projects');

                Template::set_message(lang('projects_delete_success'), 'success');

                redirect(SITE_AREA . '/projectmgmt/projects');
            } else {
                Template::set_message(lang('projects_delete_failure') . $this->projects_model->error, 'error');
            }
        }

        Assets::add_js(Template::theme_url('js/datetimepicker/moment.min.js'));
        Assets::add_js(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.js'));
        Assets::add_css(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-regularfont.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-glyphicons.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker-fontawesome.css'));
        Assets::add_css(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.css'));
        Assets::add_js(Template::theme_url('js/colorpicker/jquery.simplecolorpicker.js'));
        /*
          $this->load->model('assigned_to/assigned_to_model',null,true);
          if($assigned_to = $this->assigned_to_model->find_all_by(array(
          'item_type' => 'project',
          'item_id' => $id,
          )))
          $assignees = array_map(function($assignee){
          return $assignee->user_id;
          },$assigned_to);
          Template::set('assignees', $assignees);
         */
        $this->load->model('currency/currency_model', null, true);
        Template::set('currencies', $this->currency_model->find_all());
        Template::set('projects', reset($this->projects_model->get_projects($id)));
        Template::set('clients', $this->clients_model->find_all());
        //Template::set('users', $this->user_model->find_all_by(array('company_id' => $this->auth->company_id())));
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('toolbar_title', lang('projects_edit') . ' Projects');
        Template::render();
    }

    public function draft() {
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->projects_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('projects_delete_success'), 'success');
                } else {
                    Template::set_message(lang('projects_delete_failure') . $this->projects_model->error, 'error');
                }
            }
        }

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));

        $records = array();
        $filter_string = array();
        if ($this->input->get('from'))
            array_push($filter_string, 'DATE(project_start_date) >= "' . date('Y-m-d', strtotime($this->input->get('from'))) . '"');
        if ($this->input->get('to'))
            array_push($filter_string, 'DATE(project_end_date) <= "' . date('Y-m-d', strtotime($this->input->get('to'))) . '"');
        if ($this->input->get('search'))
            array_push($filter_string, 'project_name like "%' . $this->input->get('search') . '%" OR project_tag like "%' . $this->input->get('search') . '%" OR description like "%' . $this->input->get('search') . '%"');
        if ($filter_string)
            $records = $this->projects_model->get_projects(0, implode(' AND ', $filter_string), 'draft', 'no');
        else
            $records = $this->projects_model
                    ->where('row_status', 'draft')
                    ->where('deleted', '0')
                    ->where('initiator', $this->auth->user_id())
                    /* ->where('intg_invoice.company_id',$this->session->userdata('company_id'))
                      ->likes($field,$this->session->userdata('sapfvalue'),'both') */
//->limit($this->pager['per_page'], $offset)
//                    ->order_by($ord1)
                    ->find_all();


//echo $this->db->last_query();
//$records = $this->invoice_model->find_all();





        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Projects Draft');
        Template::render();
        //
    }

    public function draftOLD() {

        Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
        Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));

        $records = array();
        $filter_string = array();
        if ($this->input->get('from'))
            array_push($filter_string, 'DATE(project_start_date) >= "' . date('Y-m-d', strtotime($this->input->get('from'))) . '"');
        if ($this->input->get('to'))
            array_push($filter_string, 'DATE(project_end_date) <= "' . date('Y-m-d', strtotime($this->input->get('to'))) . '"');
        if ($this->input->get('search'))
            array_push($filter_string, 'project_name like "%' . $this->input->get('search') . '%" OR project_tag like "%' . $this->input->get('search') . '%" OR description like "%' . $this->input->get('search') . '%"');
        if ($filter_string)
            $records = $this->projects_model->get_projects(0, implode(' AND ', $filter_string), 'draft', 'no');
        else
            $records = $this->projects_model
                    ->where('row_status', 'draft')
                    ->where('initiator', $this->auth->user_id())
                    /* ->where('intg_invoice.company_id',$this->session->userdata('company_id'))
                      ->likes($field,$this->session->userdata('sapfvalue'),'both') */
//->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();


//echo $this->db->last_query();
//$records = $this->invoice_model->find_all();





        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Projects Draft');
        Template::render();
        //
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_projects($type = 'insert', $id = 0) {




        $config = array(
            'upload_path' => './uploads/',
            'allowed_types' => "gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|pdf|txt|",
//			'max_size' => '',
//			'max_width' => '',
//			'max_heigth' => '',
        );

        $this->load->library('upload');


        if ($type == 'update') {
            $_POST['id'] = $id;
        }
        $this->load->library('workflow');

        //list ($dataroleuser,$typeworkflow,$work_flow_approvers ) = $this->workflow->checkflow(); 
        // make sure we only pass in the fields we want
        $data = array();
        $data['parent_Rid'] = $this->input->post('parent_Rid');
        $typeworkflow == "none" ? $data['final_status'] = "none" : "";
        //$data['final_approvers'] = $work_flow_approvers;
        $data['initiator'] = $this->auth->user_id();
        //$data['other_approvers']	 = $this->projects_model->get_other_approvers();
        $data['project_name'] = $this->input->post('projects_project_name');


        $data['description'] = $this->input->post('projects_description');
        $coworker = implode(",", $this->input->post('projects_assigned_to')) . "," . $this->auth->user_id();
        $coworker2 = explode(",", $coworker);
        $coworker3 = array_unique($coworker2);
        $data['coworker'] = implode(",", $coworker3);
        if ($type == 'insert') {
            $data['status'] = $this->input->post('projects_status') ? $this->input->post('projects_status') : 'Pending';
        }
        $data['project_visibility'] = $this->input->post('projects_project_visibility');
        //$data['allow_comments']        = $this->input->post('projects_allow_comments');
        //$data['send_notifications']        = $this->input->post('projects_send_notifications');
        $data['projects_all_day'] = $this->input->post('projects_all_day') ? $this->input->post('projects_all_day') == 'true' : 1;
        $data['project_notes'] = $this->input->post('projects_project_notes');
        /* $data['project_start_date']        = $this->input->post('projects_project_start_date');
          $data['project_end_date']        = $this->input->post('projects_project_end_date'); */
        $data['project_start_date'] = date('Y-m-d', strtotime($this->input->post('projects_project_start_date')));
        $data['project_end_date'] = date('Y-m-d', strtotime($this->input->post('projects_project_end_date')));
        $data['cost_currency'] = $this->input->post('pay_unit') ? $this->input->post('pay_unit') : 'MYR';

        $data['project_tag'] = serialize($this->input->post('projects_project_tag'));
        $data['created_by'] = $this->auth->user_id(); //$this->input->post('created_by');
        $data['projects_color'] = $this->input->post('projects_color') ? $this->input->post('projects_color') : '#2e3e4e';
//		$data['prefix_io_number']        = $this->input->post('projects_prefix');
        $data['sbu_id'] = $this->input->post('projects_sbu');
        $data['cost_centre_id'] = $this->input->post('projects_cost_centre');
        $data['program_id'] = $this->input->post('projects_Programme');
        $data['introduction'] = $this->input->post('introduction');
        $data['total_days'] = $this->input->post('projects_duration_days');
        $data['experimental'] = $this->input->post('projects_experimental');
        $data['expected_outcome'] = $this->input->post('projects_expected_outcome');
        $data['justification'] = $this->input->post('projects_justification');
        $data['objective'] = $this->input->post('projects_objective');

        $data['row_status'] = $this->input->post('draft') == "Save as Draft" ||
                $this->input->post('save') == "Save & Next" || $this->input->post('save') == "Save & Create Milestone(s)" ? "draft" : "final";

//		 $data['row_status'] = "draft"; // changed 
//                    $data['row_status'] = "final";
        $data['subsidiary_id'] = $this->current_user->subsidiary_id;




        if ($_FILES['files']['error'][0] != 4) {

            $this->upload->do_multi_upload('files', $config);

            // upload errors
            $this->dota['upload_errors'] = $this->upload->display_multi_errors();
            if ($this->upload->display_multi_errors()) {
                return false;
            }

            foreach ($this->upload->multi_data() as $item => $value):
                $filename[] = $value['file_name'] . ":" . $value['orig_name'];
            endforeach;

            $data['attachment'] = implode(",", $filename);
        }



        $this->load->model('assigned_to/assigned_to_model', null, true);
        $user_ids = $this->input->post('projects_assigned_to');
        $endDate = strtotime($this->input->post('projects_project_end_date'));
        $startDate = strtotime($this->input->post('projects_project_start_date'));
        $workingDays = intval(abs($endDate - $startDate) / 86400 + 1);



        if ($type == 'insert') {

            $id = $this->projects_model->insert($data);

            //echo $this->db->last_query(); exit;

            if (is_numeric($id)) {
                //$this->workflow->create_approvers($id);
                //$this->sendmail($data['initiator'],$id,$url);
                if (!$user_ids)
                    $user_ids = array($this->auth->user_id());

                //$this->assigned_to_model->assign_user($id, 'project', $user_ids);
                /* $this->load->library('emailer/emailer');
                  $users = $this->user_model->where_in('id', $user_ids)->find_all();
                  $creator = $this->user_model->find($this->auth->user_id());
                  foreach($users as $user) {
                  $this->emailer->addAddress($user->email, $user->display_name);
                  }
                  $this->emailer->Subject = 'A project is assigned to you';
                  $this->emailer->Body = $this->load->view('_emails/project_and_task', array('item_type' => 'project', 'item_name' => $data['project_name'],'assignee_name' => $user->display_name,'start_date' => date('d-m-Y H:i', $startDate), 'end_date' => date('d-m-Y H:i', $endDate), 'creator_name'=> $creator->display_name ), TRUE);
                  $this->emailer->mail(); */


                $return = $data + array('id' => $id, 'type' => 'project');
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            unset($data['created_by']);
            //$this->assigned_to_model->assign_user($id, 'project', $user_ids);
            $return = $this->projects_model->update($id, $data);
        }

        return $return;
    }

    public function sendmail($generator, $id, $url) {

        $this->load->library('emailer/emailer');

        $sql = 'SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="40" and approval_status_mrowid = "' . $id . '"  ORDER BY id asc';
        $queryapprover = $this->db->query($sql);
        $rowapp = $queryapprover->row();
        $user_iids = (explode(",", $rowapp->approval_status_action_by));
        $size = sizeof($user_iids);
        $pro = $this->db->query('select * from intg_projects where id="' . $id . '"')->row();

        for ($i = 0; $i < $size; $i++) {
            $apdet = $this->db->query("SELECT display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
            $rowapp11 = $apdet->row();
            //echo $rowapp11->email."--".$rowapp->rolename;
            $this->emailer->AddAddress("" . $rowapp11->email . "", "" . $rowapp->rolename . "");

            $this->emailer->Subject = 'Project ' . $pro->project_name . ' is ready for approval';
            $this->emailer->Body = $this->load->view('_emails/project_app', array('item_type' => 'project', 'item_name' => $pro->project_name, 'assignee_name' => $rowapp11->display_name, 'start_date' => $pro->project_start_date, 'end_date' => $pro->project_end_date, 'creator_name' => get_from_any_table("intg_users", "display_name", "id", $generator)), TRUE);

            $this->emailer->mail();

            $this->load->model('notifications/notifications_model', null, true);
            $notification_data = array();
            $this->notifications_model->insert(array(
                'user_id' => $user_iids[$i],
                'title' => 'Project ' . $pro->project_name . ' is ready for approval',
                'content' => 'Project ' . $pro->project_name . ' created by ' . get_from_any_table("intg_users", "display_name", "id", $generator) . ' is ready for approval...',
                'link' => site_url('admin/projectmgmt/projects/approval'),
                'class' => 'fa-briefcase',
                'module' => 'ProjectMgmt',
                'type' => 'info'
            ));
        }
    }

    public function mssendmail($generator, $id, $url, $datams) {
        $this->load->library('emailer/emailer');

        $sql = 'SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="42" and approval_status_mrowid = "' . $id . '"  ORDER BY id asc';

        $user_list = array();
        $user_results = $this->user_model->find_all();

        foreach ($user_results as $val) {
            $user_list[$val->id] = $val->display_name;
        }

        $queryapprover = $this->db->query($sql);
        $rowapp = $queryapprover->row();
        $user_iids = (explode(",", $rowapp->approval_status_action_by));
        $size = sizeof($user_iids);

        for ($i = 0; $i < $size; $i++) {
            $apdet = $this->db->query("SELECT display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
            $rowapp11 = $apdet->row();
            $this->emailer->AddAddress("" . $rowapp11->email . "", "" . $rowapp->rolename . "");

            $this->emailer->Subject = 'Milestone Version(s) is ready for approval';
            $this->emailer->Body = $this->load->view('_emails/project_app', array(
                'item_type' => 'milestone',
                'item_name' => $datams['milestone_name'],
                'start_date' => $datams['start_date'],
                'end_date' => $datams['end_date'],
                'creator_name' => $user_list[$generator]), TRUE);

            $this->emailer->mail();

            $this->load->model('notifications/notifications_model', null, true);
            $this->notifications_model->insert(array(
                'user_id' => $user_iids[$i],
                'title' => 'Milestone ' . $datams['milestone_name'] . ' is ready for approval',
                'content' => 'Milestone ' . $datams['milestone_name'] . ' created by ' . $user_list[$generator] . ' is ready for approval',
                'link' => site_url('admin/projectmgmt/projects/milestone_approval'),
                'class' => 'fa-briefcase',
                'module' => 'ProjectMgmt',
                'type' => 'info'
            ));
        }
    }

    public function update_data_ajax() {
//    $data_p = array();
//    $field = "'".$_GET['field']."'";
//    $data_p[] = "'".$_GET['value']."'";    
//    $return = $this->projects_model->update($_GET['projectid'], $data_p);
//    echo var_dump(implode(",",$_GET['value']));
        $field = $_GET['field'];
        $value = $_GET['value'];
        $projectid = $_GET['projectid'];


        if ($field == "coworker") {
            $value = implode(",", $value);
        }

        $sql = "UPDATE intg_projects SET " . $field . " = '" . $value . "' WHERE id = " . $projectid . " ";

        $return = $this->db->query($sql);

        if ($return) {
            echo "Project Name or Co-Worker Saved";
        } else {
            echo "Project Name or Co-Worker failed to save";
        }
    }

    /**
     * 
     * @param type $project_id
     * @param type $milestone_id
     * @param type $action eg: activate or deactivate or m_approval
     */
    public function milestone_activate_deactivate($action, $project_id = null, $milestone_id = null) {
        $this->load->model('milestone/milestone_model', null, true);
        $this->load->model('milestone_users/milestone_users_model', null, true);
        $returned = array(
            'status' => false,
            'message' => 'No action done! Please contact system admin'
        );
        if ($action == 'activate' || $action == 'deactivate') {
            $action_text = $action == 'activate' ? 'activation' : 'deactivation';

            if ($this->settings_lib->item('ext.milestone_deactivation_wf') == 'yes') {

                //1) Add milestone to deactivation/activation list
                $return = $this->milestone_model->update($milestone_id, array("action" => $action));
                if ($return) {

                    //2) Create workflow for deactivation approval
                    $this->load->library('workflow1');
                    list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow1->checkflow1();
                    if ($typeworkflow == 'none') {
                        Template::set_message("No Workflow Available, Please Contact Administrator", 'error');
                        redirect(SITE_AREA . '/projectmgmt/projects');
                    }
                    $data = array();

                    $data['final_status'] = "No";
                    $data['initiator'] = $dataroleuser;

                    $other_approvers = $this->db->query('SELECT work_flow_approvers '
                                    . 'FROM intg_work_flow '
                                    . 'WHERE work_flow_doc_generator = ' . $this->auth->user_id() . ' '
                                    . 'and work_flow_module_id = "42"')->result();


                    foreach ($other_approvers as $b) {
                        $options[] = $b->work_flow_approvers;
                    }

                    $project = $this->db->query("Select * from intg_projects where id = " . $project_id)->row();
                    $result_milestone = $this->db->query("INSERT INTO "
                            . "`intg_milestone_approval`(`final_status`,"
                            . " `initiator`,"
                            . " `msother_approvers`,"
                            . "`final_approvers`,"
                            . " `project_code`,"
                            . " `created_by`,"
                            . " `project_id`) "
                            . "VALUES ('No'," . $this->auth->user_id() . ",'" . implode(",", $options) . "','" . $work_flow_approvers . "','" . $project->prefix_io_number . "'," . $this->auth->user_id() . "," . $project_id . ")");

                    $last_id = $this->db->insert_id();


                    $sent_ms = $this->db->query("Select * from intg_milestones where milestone_id = " . $milestone_id)->row();
                    $datams = array(
                        'milestone_name' => $sent_ms->milestone_name,
                        'start_date' => $sent_ms->start_date,
                        'end_date' => $sent_ms->end_date
                    );


                    if ($result_milestone) {

                        $this->milestone_model->update($milestone_id, array("ms_approval_id" => $last_id));
                        $this->workflow1->create_approvers($last_id);
                        $this->mssendmail($data['initiator'], $last_id, '', $datams);

                        $returned = array(
                            'status' => true,
                            'message' => 'Milestone successfully sent for approval'
                        );
                    } else {
                        $this->milestone_model->update($milestone_id, array("action" => ''));
                        $returned = array(
                            'status' => false,
                            'message' => 'Failed to sent milestones for approval'
                        );
                    }
                } else {
                    $returned = array(
                        'status' => false,
                        'message' => "Failed to add milestone into " . $action_text . " list"
                    );
                }
            } else {

                $action_stat = $action == 'activate' ? '' : 'deactivated';
                $return = $this->milestone_model->update($milestone_id, array("action" => $action_stat));
                if ($return) {
                    $returned = array(
                        'status' => true,
                        'message' => "Successfully " . $action . " the milestone "
                    );
                } else {
                    $returned = array(
                        'status' => false,
                        'message' => "Failed to " . $action . " the milestone "
                    );
                }
            }
        } else if ($action == 'delete') {

            //Look for the milestone id used in intg_timesheet_details or not
            $ms_check = $this->db->query("Select COUNT(*) AS cnt from intg_timesheet_details where mid = " . $milestone_id)->row()->cnt;
            if ($ms_check > 0) {
                $returned = array(
                    'status' => false,
                    'message' => "Failed to " . $action . " the milestone. It is being used in the Timesheet "
                );
            } else {

                $return = $this->milestone_model->delete($milestone_id);
                if ($return) {

                    $wheres = array(
                        'milestones_id' => $milestone_id
                    );
                    $delete_sub_table = $this->milestone_users_model->delete_where($wheres);

                    $returned = array(
                        'status' => true,
                        'message' => "Successfully " . $action . " the milestone "
                    );
                } else {
                    $returned = array(
                        'status' => false,
                        'message' => "Failed to " . $action . " the milestone "
                    );
                }
            }
        }
        echo json_encode($returned);
    }

    public function testDompdf() {
        $pdfFileName = "TEST_DOMPDF_" . time() . ".pdf";
        $this->load->library('MY_Dompdf');
        $config = $this->load->config('dompdf_config', true);
        $file = $config['path'] . '/' . $pdfFileName;
        Template::set('filenamepath', $file);
        Template::render();
    }

    public function change_project_ownership() {

        //save in json string hereee

        $rc = $this->projects_model->find_by('id', $this->input->post("projectid"));

        $created_by = $rc->created_by;
        $cbh = $rc->created_by_history;

        date_default_timezone_set('asia/kuala_lumpur');
        $created_by_history = array(
            'user_id' => $created_by,
            'changed_on' => date("Y-m-d h:i:sa"),
            'changed_by' => $this->auth->user_id(),
        );

        if ($cbh == '') {
            $cbh_to_save = json_encode($created_by_history);
        } else {
            $cbh_to_save = json_encode($created_by_history) . ',' . $cbh;
        }

//        echo $cbh_to_save;
//        echo $created_by;
//        echo $cbh;
//        echo $this->db->last_query();
//         print_r($_POST);die('yeeehaa');
//        die();

        $data_update = array(
            "created_by" => $this->input->post("userid"),
            'created_by_history' => $cbh_to_save
        );

        $this->db->where('id', $this->input->post("projectid"));
        $status = $this->db->update('intg_projects', $data_update);

        if ($status) {
            return 'success';
        } else {
            return 'failed';
        }
    }

    public function project_to_pdf() {
        $data = array();
        $pdfFileName = "PDF_Project_" . time() . ".pdf";
        $this->load->library('MY_Dompdf');
        $this->load->config('dompdf_config', true);
        $html = $this->load->view('projectmgmt/project_to_pdf', $data, true);

        $dompdf = new MY_Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->set_base_path(base_url());
        $dompdf->render();
        $dompdf->stream($pdfFileName);
    }

    //--------------------------------------------------------------------
}
