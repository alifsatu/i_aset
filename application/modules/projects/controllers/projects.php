<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * tasks controller
 */
class projects extends Front_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('projects_model', null, true);		
		$this->lang->load('projects');
	}

	//--------------------------------------------------------------------

public function index()
	{

		$id = $this->uri->segment(3);

			
		Template::set('project', $this->projects_model->find($id));
		//echo $this->db->last_query();

		
		Template::render();
	}
	/**
	 * Displays a list of form data.
	 *
	 * @return void
	 */
	
	
	function pdf()
    {
		$id = $this->uri->segment(3);	
		
		require 'pdfcrowd.php';

try
{   
    // create an API client instance
    $client = new Pdfcrowd("pasupathy", "da451f587530d992eaeda5180e4618c7");
	$client->setPageHeight("12.5in");
	$client->setPageWidth("12.5in");
    $client->setHorizontalMargin("0.4in");
    $client->setVerticalMargin("0.5in");
    $client->useSSL(true);

	$client->setUserPassword("123123123");
	//$pdf  = $client->setFooterUrl(base_url().'index.php/quote/footer/'.$id);
	//$pdf  = $client->setHeaderUrl(base_url().'index.php/quote/headers/'.$id);
    // convert a web page and store the generated PDF into a $pdf variable
    $pdf = $client->convertURI(base_url().'index.php/projects/index/'.$id.'/pdf');
	

	
    // set HTTP response headers
    header("Content-Type: application/pdf");
    /*header("Cache-Control: no-cache");
    header("Accept-Ranges: none");
    header("Content-Disposition: attachment; filename=\"quote".$id.".pdf\"");*/

    // send the generated PDF 
    echo $pdf;
}
catch(PdfcrowdException $e)
{
    echo "Pdfcrowd Error: " . $e->getMessage();
}
		
		}
	

	


}