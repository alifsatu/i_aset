<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['projects_manage']			= 'Manage';

$lang['projects_graph']			= 'Graph';
$lang['projects_edit']				= 'Edit';
$lang['projects_true']				= 'True';
$lang['projects_false']				= 'False';
$lang['projects_create']			= 'Save';
$lang['projects_list']				= 'List';
$lang['projects_new']				= 'New';
$lang['projects_edit_text']			= 'Edit this to suit your needs';
$lang['projects_no_records']		= 'There aren\'t any projects in the system.';
$lang['projects_create_new']		= 'Create a new Projects.';
$lang['projects_create_success']	= 'Project(s) successfully created.';
$lang['projects_create_failure']	= 'There was a problem creating the projects: ';
$lang['projects_create_new_button']	= 'Create New Projects';
$lang['projects_invalid_id']		= 'Invalid Projects ID.';
$lang['projects_edit_success']		= 'Projects successfully saved.';
$lang['projects_edit_draft_success']		= 'Projects successfully saved as draft.';
$lang['projects_edit_failure']		= 'There was a problem saving the projects: ';
$lang['projects_delete_success']	= 'record(s) successfully deleted.';

$lang['projects_purged']	= 'record(s) successfully purged.';
$lang['projects_success']	= 'record(s) successfully restored.';


$lang['projects_delete_failure']	= 'We could not delete the record: ';
$lang['projects_delete_error']		= 'You have not selected any records to delete.';
$lang['projects_actions']			= 'Actions';
$lang['projects_cancel']			= 'Cancel';
$lang['projects_delete_record']		= 'Delete';
$lang['projects_delete_confirm']	= 'Are you sure you want to delete this projects?';
$lang['projects_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['projects_action_edit']		= 'Save';
$lang['projects_action_create']		= 'Create';
$lang['projects_calendar']		= 'Calendar';
$lang['projects_create_error']	= 'Please fix the following errors:';
$lang['projects_name']	= 'Project Name';
$lang['projects_desc']	= 'Description';
$lang['projects_start_date']	= 'Start Date';
$lang['projects_end_date']	= 'End Date';
$lang['projects_assigned_to']	= 'Assigned To';
$lang['projects_visibility']	= 'Project Visibility';
$lang['projects_notes']	= 'Project Notes';
$lang['projects_cost']	= 'Project Cost';
$lang['overall_progress']	= 'Overall Progress';
$lang['projects_color']	= 'Color';
$lang['projects_status']	= 'Project Status';
$lang['projects_tag']	= 'Project Tag';
$lang['projects_client']	= 'Client';
$lang['projects_filter']	= 'Filter By:';
$lang['projects_fullcalender']	= 'Fullcalendar';
$lang['projects_default']	= 'Quick Add Project';
$lang['projects_task_default']	= 'Quick Add Task';
$lang['projects_gantt_view']	= 'Gantt View';
$lang['projects']	= 'Projects';
$lang['projects_search']	= 'Search Project Name';
$lang['search']	= 'Search';
$lang['to']	= 'to';
$lang['created_by']	= 'Created by';
$lang['task_name']	= 'Task Name';
$lang['Weightage']	= 'Weightage';
$lang['created_on']	= 'Created On';
$lang['Tags']	= 'Tags';
$lang['project']	= 'Project';
// Activities
$lang['projects_act_create_record']	= 'Created record with ID';
$lang['projects_act_edit_record']	= 'Updated record with ID';
$lang['projects_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['projects_column_created']	= 'Created';
$lang['projects_column_deleted']	= 'Deleted';
$lang['projects_column_modified']	= 'Modified';

$lang['quote_approvers_hierarchy']	= 'Approvers Hierarchy';
$lang['quote_approved_by']	= 'Approved By';
$lang['quote_role']	= 'Role';
$lang['quote_approver_status']	= 'Status';
$lang['quote_approver_remarks']	= 'Remarks';
$lang['quote_approver_datetime']	= 'Date & Time';
$lang['quote_approver_action']	= 'Approver Action';
$lang['quote_approver_action_status']	= 'Approval Status';
$lang['quote_approver_action_yes']	= 'YES';
$lang['quote_approver_action_no']	= 'NO';
