<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends BF_Model {

	protected $table_name	= "projects";
	protected $key			= "id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= false;
	protected $set_modified = false;

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array('delete_related_tasks');

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "projects_project_name",
			"label"		=> "Project Name",
			"rules"		=> "max_length[100]|required"
		),
		array(
			"field"		=> "projects_description",
			"label"		=> "Description",
			"rules"		=> "max_length[1000]"
		),
		array(
			"field"		=> "projects_client_id",
			"label"		=> "Client Id",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "projects_status",
			"label"		=> "Status",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "projects_project_visibility",
			"label"		=> "Project Visibility",
			"rules"		=> "max_length[45]"
		),/*
		array(
			"field"		=> "projects_allow_comments",
			"label"		=> "Allow Comments",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "projects_send_notifications",
			"label"		=> "Send Notifications",
			"rules"		=> "max_length[45]"
		),*/
		array(
			"field"		=> "projects_estimated_hours",
			"label"		=> "Estimated Hours",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "projects_project_notes",
			"label"		=> "Project Notes",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "projects_project_start_date",
			"label"		=> "Project Start Date",
			"rules"		=> "max_length[45]|required"
		),
		array(
			"field"		=> "projects_project_end_date",
			"label"		=> "Project End Date",
			"rules"		=> "max_length[45]|required"
		),
		array(
			"field"		=> "projects_project_cost",
			"label"		=> "Project Cost",
			"rules"		=> "max_length[45]"
		),/*
		array(
			"field"		=> "projects_project_tag",
			"label"		=> "Project Tag",
			"rules"		=> "max_length[1000]"
		),*/
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------
	public function get_projects($id = 0, $search_string = 'deleted = 0 AND id != 1',$rowstatus= '',$finalstatus='')
	{
		$project_ids = array();
		$this->load->model('assigned_to/assigned_to_model',null,true);
		$projects = false;
		
		if($id) {
		
			$projects = array($this->projects_model->find($id));
			
		} else {
		
			$assigned_projects = $this->assigned_to_model->find_all_by(array('user_id' => $this->auth->user_id(), 'item_type' => 'project'));
		
			if($assigned_projects)
				$project_ids = array_map(function($assigned_item) {
					return $assigned_item->item_id;
			}, $assigned_projects);
			/*
			$this->projects_model->where($search_string);
			
			if($project_ids)
				$this->projects_model->where_in('id', $project_ids);
				
			$projects = $this->projects_model->or_where('('.$search_string.' AND deleted = 0 AND project_visibility != "0" AND created_by IN (SELECT id FROM intg_users WHERE company_id = '.$this->auth->company_id().') OR ('.$search_string.' AND created_by = '.$this->auth->user_id().'))')->order_by('project_end_date','desc')->find_all(); // this is ugly, but who cares right?
			*/
			$project_query = array();
                        $rowstatusquery = '';
                        if($rowstatus!=''){
                            $rowstatusquery = "AND row_status='".$rowstatus."'";
                        }
                        
                        $finalstatusquery = '';
                        if($finalstatus!=''){
                            $finalstatusquery = "AND final_status= '".$finalstatus."'";
                        }
                        
                        
			if($project_ids)
				$project_query[] = '('.$search_string.' '.$rowstatusquery.'  AND id IN ('.implode(',', $project_ids).'))';
                        
			$project_query[] = '('.$search_string.' '.$rowstatusquery.' AND project_visibility != "0" AND created_by IN (SELECT id FROM intg_users WHERE company_id = '.$this->auth->company_id().'))';
                        
			//$project_query[] = '('.$search_string.' AND created_by = '.$this->auth->user_id().')';
			$project_query[] = '('.$search_string.'  AND (created_by = '.$this->auth->user_id().' AND FIND_IN_SET('.$this->auth->user_id().',other_approvers)) '.$finalstatusquery.' '.$rowstatusquery.')';
                        
			$projects = $this->db->query('SELECT * FROM intg_projects WHERE  '.implode(' AND ',$project_query) .' '.$rowstatusquery.'  order by project_end_date desc')->result();
			//echo $this->db->last_query();
		//	$projects = $this->db->query('SELECT * FROM intg_projects WHERE '.implode(' OR ',$project_query) .' order by id desc')->result();
		}
		
		if($projects) {
		
			$this->load->model('clients/clients_model',null,true);
			$clients_model = $this->clients_model;
			$assigned_to_model = $this->assigned_to_model;
			$user_model = $this->user_model;
			$this->load->model('tasks/tasks_model',null,true);
			$tasks_model = $this->tasks_model;
			/* $this->load->model('sbu/sbu_model',null,true);
			$sbu_model = $this->sbu_model; */
			$this->load->model('project_progress/project_progress_model',null,true);
			$project_progress_model = $this->project_progress_model;
			$db = $this->db;
			
			return array_map(function($project)use($assigned_to_model,$clients_model,$user_model,$tasks_model,$project_progress_model,$db){
				$project->assigned_to = $assigned_to_model->get_users($project->id, 'project');
				//$project->sbu = $sbu_model->find_all($project->sbu_id);
				//$project->client = $clients_model->find($project->client_id);
				$project->creator = $user_model->find($project->created_by);
				$project->tasks = $tasks_model->find_all_by(array('project_id' => $project->id, 'deleted' => 0));
				$project->group_chat = $db->query("SELECT gno as id FROM intg_ideas_group WHERE projectid = ".$project->id)->row();
				$project->overall_progress = 0;
				$project->total_completed_tasks = 0;
				$project->total_ongoing_tasks = 0;
				$project->total_overdue_tasks = 0;
				if($project->tasks) {
					foreach($project->tasks as $i => $task) {
						$project->overall_progress += ($task->progress_percentage/100) * ($task->weightage_percentage/100);
						if($task->progress_percentage == 100)
							$project->total_completed_tasks++;
						else {
							$project->total_ongoing_tasks++;
							if(time() > $task->end_date)
								$project->total_overdue_tasks++;
						}

						$project->tasks[$i]->assigned_to = $assigned_to_model->get_users($task->id, 'task');
					}
				}
				$project->tags = 'None';
				if($project->project_tag && $tags = @unserialize($project->project_tag))
					$project->tags = implode(', ',$tags);
					
				$last_activity = $project_progress_model->order_by('id','desc')->find_by(array('project_id' => $project->id));
				$project->last_activity = $last_activity ? $last_activity : (object)array('task_id'=>0,'remarks'=>'No Update', 'time_stamp'=>'Now');
					
				return $project;
			}, $projects);
		}
			
		return array();
	}
	
	protected function delete_related_tasks($id)
	{
		$this->load->model('tasks/tasks_model',null,true);
		$this->tasks_model->delete_where(array('project_id' => $id));
	}
	public function get_other_approvers()
	{
		
	$all_approvers = $this->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = '.$this->auth->user_id().' and work_flow_module_id = "40" ')->result();
	//$rowfappr = $queryfappr->row();
	//echo 'SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = '.$this->input->post('purchase_requisition_request_by').' ';
		//echo $rowfappr;
		
		$i=0;
		foreach($all_approvers as $b)
		{
		$options[$i] = $b->work_flow_approvers;
		$i++;
		}
		return implode(",",$options);	
		
	}
}
