<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Workflow1 {

    public function checkflow1() {
        $CI = & get_instance();

        $queryfappr = $CI->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = ' . $CI->auth->role_id() . ' AND rolename = work_flow_fappr  and work_flow_module_id = "42"  and flow_type="role" ORDER BY  id asc');

        if ($queryfappr->num_rows() > 0) {
            $rowfappr = $queryfappr->row();
            $typeworkflow = "role";
            $dataroleuser = $CI->auth->role_id();
            $approvers = $rowfappr->work_flow_approvers;
        } else {
            $queryfappr = $CI->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = ' . $CI->auth->user_id() . ' AND rolename = work_flow_fappr  and work_flow_module_id = "42"  and flow_type="user" ORDER BY  id asc');

            if ($queryfappr->num_rows() > 0) {
                $rowfappr = $queryfappr->row();
                $typeworkflow = "user";
                $dataroleuser = $CI->auth->user_id();
                $approvers = $rowfappr->work_flow_approvers;
            } else {
                $dataroleuser = $CI->auth->user_id();
                $typeworkflow = "none";
                $approvers = "none";
            }
        }
        return array($dataroleuser, $typeworkflow, $approvers);
    }

    public function create_approvers($iid) {
        $CI = & get_instance();
        list($dataroleuser, $typeworkflow, $approvers) = $this->checkflow1();

        if ($typeworkflow != "none") {

            $querym = $CI->db->query('select bfm.id as bfmid,bfwf.work_flow_approvers as approvers,bfwf.rolename as rname,bfwf.escalation from intg_modules bfm,intg_work_flow bfwf where bfm.id = bfwf.work_flow_module_id and bfwf.work_flow_doc_generator =' . $dataroleuser . ' and bfm.modules_module_name="intg_milestone_approval" order by bfwf.id asc');

            //	echo 'select bfm.id as bfmid,bfwf.work_flow_approvers as approvers,bfwf.rolename as rname,bfwf.escalation from intg_modules bfm,intg_work_flow bfwf where bfm.id = bfwf.work_flow_module_id and bfwf.work_flow_doc_generator ='.$dataroleuser.' and bfm.modules_module_name="intg_milestone_approval" order by bfwf.id asc'."<br>";
            //$rowm = $querym->row();
            //exit;
            $hierarchy = 0;
            $esc = NULL;
            $i = 0;
            foreach ($querym->result() as $rowm) {

                $esc = $esc + $rowm->escalation;
                $datam = array(
                    'approval_status_module_id' => '' . $rowm->bfmid . '', // module id
                    'approval_status_mrowid' => '' . $iid . '', // new intg_quote inserted row id
                    'approval_status_action_by' => '' . $rowm->approvers . '', // approver's role id
                    'hierarchy_status' => '' . $hierarchy . '', // initially set to 0 to escape the first approver in hierarchy
                    'rolename' => '' . $rowm->rname . '',
                    'escalation' => '' . $esc . '',
                    'approval_status_action_date' => '' . date('Y-m-d') . '',
                    'created_on' => '' . date('Y-m-d') . ''
                );



                $CI->db->insert('intg_approval_status', $datam);
// echo $CI->db->last_query();
                $hierarchy = $rowm->rname; // assign the first approver
                $insertid = mysql_insert_id();

                $approvers = explode(",", $rowm->approvers);
                foreach ($approvers as $val) {
                    $queryrole = $CI->db->query('SELECT role_id FROM intg_users WHERE id = "' . $val . '"');
                    //echo $CI->db->last_query(); 
                    $rowrole = $queryrole->row();
                    $datan = array(
                        'approvers_appstatrowid' => '' . $insertid . '',
                        'approvers_approver' => '' . $val . '',
                        'approvers_status' => 'No',
                        'approvers_role' => $rowrole->role_id,
                        'created_on' => '' . date('Y-m-d') . '',
                        'modified_on' => '' . date('Y-m-d') . ''
                    );


                    $CI->db->insert('intg_approvers', $datan);
                    //print_r($datan);
                    //echo "<br>";
                }
                $i++;
            }
        }
    }

    public function update_approvers_status() {

        $data3 = array('approval_status_status' => 'No',);
        $this->db->where('approval_status_module_id', '42');
        $this->db->where('approval_status_mrowid', $id);
        $this->db->update('bf_approval_status', $data3);
    }

}

?>