<style>
  .select2-drop-mask {
    z-index: 18888;
  }
  .select2-drop {
    z-index: 19999;
  }
</style>
<div id="mygantt" style='width:100%; height:90%;'></div>
<script type="text/javascript">
$(function(){
  var modalHtml = '<div class="modal fade" id="someModal"><div class="modal-dialog"><div class="modal-content">'
      +'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
      +'<h4 class="modal-title">Header</h4></div>'
      +'<div class="modal-body">Loading...</div>'
      +'<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>',
      users = <?php echo json_encode($users);?>;

  gantt.attachEvent('onMouseMove', function(task_id, e){
    var tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#2e3e4e;position:absolute;z-index:1001;padding:5px 5px 5px 5px ;  line-height: 200%;color:#fff;border-radius:.25em">' 
        + 'Loading...' 
        + '</div>';

    if(task_id) {

      $("body").append(tooltip);
      var $tooltiptopicevent = $('.tooltiptopicevent'),
        task = gantt.getTask(task_id), assigned_to = '';
      $(e.srcElement).mouseover(function (e) {
        $(this).css('z-index', 1000);
        $tooltiptopicevent.fadeIn('500').fadeTo('10', 1.9);
      }).mousemove(function (e) {
        $tooltiptopicevent.css('top', e.pageY + 10).css('left', e.pageX + 20);
      });

      $tooltiptopicevent.html('Task Name: '+task.text+'<br/>'+'Assigned to: '
        + task.assigned_to.map(function(id) {
          return users.filter(function(user) {
            return user.id == id;
          })[0].display_name;
        }).join(', ') +'<br/>Creator: '
        + users.filter(function(user) {
            return user.id == task.creator;
          })[0].display_name);
    } else {

      $(e.srcElement).css('z-index', 8);
      $('.tooltiptopicevent').remove();
    }
  });

  // init jquery plugin
  gantt.attachEvent("onLightbox", function(id) {
    var task = gantt.getTask(id),
    $input_daterange = $('.input-daterange'),
    start_date = moment(task.start_date).format('DD/MM/YYYY'),
    end_date = moment(task.end_date).format('DD/MM/YYYY');

    task.creator = <?php echo $this->auth->user_id();?>;

    $input_daterange.datepicker({
      format: "dd/mm/yyyy",
      daysOfWeekDisabled: "0,6",
      autoclose: true,
      todayHighlight: true
    });
    $input_daterange.data('datepicker').pickers[0].setDate(start_date);
    $input_daterange.data('datepicker').pickers[1].setDate(end_date);

    $assigned_to = $('[name="assigned_to[]"]').select2().select2('data', []);
    if(typeof task.assigned_to != 'undefined')
      $assigned_to.select2('data', users.filter(function(user) {
        return task.assigned_to.indexOf(user.id) >= 0;
      }).map(function(user) {
        return {id: user.id, text: user.display_name};
      }));
    /*
    var slider = new Slider('#ex1', {
      formatter: function(value) {
        return 'Current value: ' + value;
      }
    });
    */
    return true;
  });

  gantt.attachEvent("onAfterTaskUpdate", function(id,task) {
    console.log('onAfterTaskUpdate', task);
    var overall_progress = 0, project_id = 'project_<?php echo $project_id;?>', project = gantt.getTask(project_id);
    gantt.eachTask(function(task) {
      if(task.type == 'task')
        overall_progress += Math.round(task.progress * 100) / 100 * task.weightage / 100;
    });
    old_progress = project.progress;
    if(old_progress != overall_progress) {
      gantt.getTask(project_id).progress = overall_progress;
      gantt.updateTask(project_id);
    }

    var task_id = task.id.indexOf('task_') >= 0 ? task.id.replace('task_', '') : task.id.replace('project_', '');
    $.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/edit') ?>/"+task_id, 
        {
          save: 1,
          ajax: 1,
          gantt: 1,
          tasks_task_name: task.text,
          tasks_start_date: moment(task.start_date).format('YYYY/MM/DD 00:00'),
          tasks_end_date: moment(task.end_date).subtract(1, 'days').format('YYYY/MM/DD 00:00'),
          tasks_progress_percentage: Math.round(task.progress * 100),
          tasks_assigned_to: task.assigned_to
        },
        function(res) {
          console.log(res);
        }, 'json');
  });

  gantt.attachEvent("onAfterTaskAdd", function(id,task) {
    console.log(task);
    $.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/create') ?>", 
        {
          save: 1,
          ajax: 1,
          tasks_project_id: <?php echo $project_id;?>,
          parent_task_id: task.parent ? (task.parent.indexOf('task_') >= 0 ? task.parent.replace('task_', '') : '0') : '0',
          tasks_task_name: task.text,
          tasks_start_date: moment(task.start_date).format('YYYY/MM/DD 00:00'),
          tasks_end_date: moment(task.end_date).subtract(1, 'days').format('YYYY/MM/DD 00:00'),
          tasks_weightage_percentage: 10,
          tasks_progress_percentage: 0,
          tasks_assigned_to: task.assigned_to
        },
        function(res) {
          console.log(res);
          new_task_id = 'task_'+res.id;

          gantt.getTask(id).id = new_task_id;
          gantt.updateTask(id);

          gantt.addLink({
            id: new Date().getTime(),
            source: task.parent ? task.parent : 'project_<?php echo $project_id?>',
            target: new_task_id,
            type: 1
          });
        }, 'json');
    });
/*
  gantt.attachEvent("onBeforeTaskAdd", function(id,item){
    console.log(item);
    //any custom logic here
    return true;
  });

  gantt.attachEvent("onTaskCreated", function(id,item){
    // todo // modal add task
    //return false;
  });

  gantt.attachEvent('onTaskClick', function(task_id, e){
    
  });

  gantt.attachEvent('onTaskDblClick', function(task_id, e){

    $('.tooltiptopicevent').remove();
    $modal = $(modalHtml).modal();
    $modal.find('.modal-title').text('Task detail');
 
  });
*//*
	gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e){
		if(mode == "progress" & id.indexOf('project_') == 0){
			return false;
		}
		return true;
	});

  gantt.attachEvent("onTaskDrag", function(id, mode, task, original, e) {
    if(mode == "progress" && id.indexOf('task_') == 0) { 
    }
  });

  gantt.attachEvent("onBeforeTaskUpdate", function(id, new_task){
    //console.log(id, gantt.getTask(id), new_task);
    return false;
  });

	gantt.attachEvent("onBeforeTaskChanged", function(id, mode, task){
   		if(mode == "progress" & id.indexOf('task_') == 0){
   			if(task.creator == '<?php echo $this->auth->user_id()?>' 
          || task.assigned_to.indexOf('<?php echo $this->auth->user_id()?>') >= 0) {
          console.log(task);
          return confirm("Are you sure?");
        } else {
   				alert('you have no power here!:Theoden:');
   				return false;
   			}
		}
		if(['resize','move'].indexOf(mode) >= 0 && task.creator !== '<?php echo $this->auth->user_id()?>') {
			alert('only creator can edit, human');
			return false;
		}
		return true;
	});
*/
    gantt.config.columns = [
        {name:"text", label:"Task name", tree:true, width:'*', template: function(item) {
        	return '<i class="fa fa-'+ (item.type == 'project' ? 'briefcase' : 'tasks') +'"></i> '+item.text;
        } },
        {name:"text", label:"Weightage", width:66, align: "center", template: function(item) { 
          return (item.weightage ? item.weightage : 100)+'%';
        } },
        {name:"progress", label:"Progress", width:66, align: "center", template: function(item) { 
        	return (item.progress ? Math.round(item.progress * 100) : 0) +'%'; 
        } },
        {name:"add", label:"", width:35}
    ];

    gantt.form_blocks["my_period"] = {
    	render:function(sns) {
       		return '<div class="gantt_cal_ltext" style="height:35px;">'
            +'<div class="input-daterange input-group" id="datepicker">'
            +'<input type="text" class="input-sm form-control" name="start" readonly="readonly"/>'
            +'<span class="input-group-addon">to</span>'
            +'<input type="text" class="input-sm form-control" name="end" readonly="readonly"/>'
            +'</div>';
      },
    	set_value:function(node, value, task) {
          //console.log(node,value,task);
      },
    	get_value:function(node, task) {

          start_date = moment(node.getElementsByTagName('input')[0].value, 'DD/MM/YYYY');
          end_date = moment(node.getElementsByTagName('input')[1].value, 'DD/MM/YYYY');
          task.start_date = start_date.toDate();
          task.end_date = end_date.toDate();
          return [start_date.format('YYYY-MM-DD 00:00'), end_date.format('YYYY-MM-DD 00:00')];
      },
      focus:function(node) {
        //var a = node.childNodes[4];
        //a.select();
        //a.focus();
      }
	 };

    gantt.form_blocks["my_assignedto"] = {
      render:function(sns) {
          return '<div class="gantt_cal_ltext" style="height:60px;">'
            +'<select name="assigned_to[]" class="form-control" multiple=true>'
            + users.map(function(user) {return '<option value="'+user.id+'">'+user.display_name+'</option>';}).join()
            +'</select>'
            +'</div>';
      },
      set_value:function(node, value, task) {
          //console.log(node,value,task);
      },
      get_value:function(node, task) {
          task.assigned_to = $('[name="assigned_to[]"]').val();
          return task.assigned_to;
      },
      focus:function(node) {
        //var a = node.childNodes[4];
        //a.select();
        //a.focus();
      }
   };

    gantt.form_blocks["my_weightage"] = {
      render:function(sns) {
          return '<div class="gantt_cal_ltext" style="height:35px;">'
            +'<input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="5" data-slider-value="0"/>'
            +'</div>';
      },
      set_value:function(node, value, task) {
          //console.log(node,value,task);
      },
      get_value:function(node, task) {

          start_date = moment(node.getElementsByTagName('input')[0].value, 'DD/MM/YYYY');
          end_date = moment(node.getElementsByTagName('input')[1].value, 'DD/MM/YYYY');
          task.start_date = start_date.toDate();
          task.end_date = end_date.toDate();
          return [start_date.format('YYYY-MM-DD 00:00'), end_date.format('YYYY-MM-DD 00:00')];
      },
      focus:function(node) {
        //var a = node.childNodes[4];
        //a.select();
        //a.focus();
      }
   };
	
    gantt.locale.labels["section_name"] = "Task Name";
    gantt.locale.labels["section_assignedto"] = "Assigned To";

    gantt.config.lightbox.sections = [
        {name: "name", height: 70, map_to: "text", type: "textarea", focus: true},
        {name:"time", height:200, map_to:"auto", type:"my_period"},
        {name:"assignedto", height:200, map_to:"auto", type:"my_assignedto"},
    ];
	
	gantt.init('mygantt');
	gantt.parse(<?php echo ($gantt_data);?>);

  $('#mygantt').siblings('header.header').append('<button id="export-to-pdf" class="btn btn-info pull-right"><i class="fa fa-file-text"></i> PRINT / PDF</button>');
  // export to pdf
  $('#export-to-pdf').on('click', function() {
    gantt.exportToPDF();
  });
});
</script>