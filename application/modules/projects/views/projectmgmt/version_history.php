<div class="modal-dialog" style="margin-top:150px; width:900px">

    <section class="panel panel-default portlet-item" style="border-bottom:none;  padding-bottom:0">
        <header class="panel-heading"><b>Version History</b>
            <button type="button" class="close" data-dismiss="modal">×</button>
        </header>
        <section class="panel-body" id="quotafile" style="padding-bottom:0">
            <div class="panel-group m-b" id="accordion3">
                <?php
                $noms = 1;
                $ms = $this->db->query("select * from intg_milestones where parent_Rid IN (" . $this->uri->segment(5) . ") and milestone_id != '" . $this->uri->segment(6) . "' and project_id = '" . $this->uri->segment(7) . "' UNION "
                                . "select * from intg_milestones where milestone_id IN (" . $this->uri->segment(5) . ") and milestone_id != '" . $this->uri->segment(6) . "' and project_id = '" . $this->uri->segment(7) . "' order by milestone_id desc")->result();
                //echo $this->db->last_query();

                $collapse = "in";
                foreach ($ms as $ml) {
                    ?>

                    <div class="panel panel-default">

                        <div class="panel-heading clearfix"> <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#accordion3" href="#vml<?= $ml->milestone_id ?>"><strong>Milestone
                                    <?= $noms ?>
                                    : </strong>
                                <?= ucfirst($ml->milestone_name) ?>
                                &nbsp;&nbsp; <strong>Start Date: </strong>
                                <?= date("d/m/Y", strtotime($ml->start_date)) ?>
                                &nbsp;&nbsp; <strong>End Date: </strong>
                                <?= date("d/m/Y", strtotime($ml->end_date)) ?> 
                                <?php if ($ml->version_no > 0) { ?>  
                                    <span class='badge bg-info'>V<?= $ml->version_no ?></span> 
                                <?php } else { ?>
                                    <span class='badge bg-info'>Original Version</span> 
                                <?php } ?>
                            </a> 

                            <?php
                            switch ($ml->status) {
                                case "No" : $status = "<span class='badge btn-warning m-l text-xs'>No Action</span>";
                                    break;
                                case "Yes" : $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                                    break;
                                case "Reject" : $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                                    break;
                            }
                            echo $status;
                            ?>


                            <span id="mcostup<?= $ml->milestone_id ?>" class="pull-right"></span></div>

                        <div id="vml<?= $ml->milestone_id ?>" class="panel-collapse <?= $collapse ?>" style="height: auto;">
                            <div class="panel-body text-sm">

                                <div class="col-sm-16" id="lineitem_form">
                                    <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Users</header>
                                    <section class="panel panel-default">
                                        <div class="table-responsive ">
                                            <table class="table table-striped b-t b-light text-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Level</th>
                                                        <th>Man Days</th>
                                                        <th>Head Count</th>
                                                        <th>Total Man Days</th>
                                                        <th>Daily FTE (RM)</th>
                                                        <th>Time Cost (RM)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <? $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='".$ml->milestone_id."'")->result(); 

                                                    foreach ( $mu as $u ) {

                                                    ?>
                                                    <tr >
                                                        <td><?= $u->user_level ?></td>
                                                        <td><?= $u->man_days ?></td>
                                                        <td><?= $u->head_count ?></td>
                                                        <td><?= $u->total_man_days ?></td>
                                                        <td><?= $u->fte ?></td>
                                                        <td><?= number_format($u->time_cost, 2) ?></td>
                                                    </tr>
                                                    <? $milestone_cost = $milestone_cost + $u->time_cost; } 

                                                    ?>

                                                </tbody>

                                            </table>
                                            <span class="mcostdown" rowid="<?= $ml->milestone_id ?>" id="matcost<?= $ml->milestone_id ?>" style="display:none"><? echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span>

                                        </div>
                                    </section>


                                    <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
                                    <section class="panel panel-default">
                                        <div class="table-responsive ">
                                            <table class="table table-striped b-t b-light text-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Material Cost (RM)</th>
                                                        <th>New CAPEX (RM)</th>
                                                        <th>Outsourcing (RM)</th>
                                                        <th>Others (RM)</th>                       
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr >
                                                        <td><?= $ml->material_cost ?></td>
                                                        <td><?= $ml->new_capex ?></td>
                                                        <td><?= $ml->outsourcing ?></td>
                                                        <td><?= $ml->others ?></td>

                                                    </tr>

                                                </tbody>
                                                <thead>
                                                    <tr>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>                       
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr >
                                                        <td><?= $ml->mcost_desc ?></td>
                                                        <td><?= $ml->ncapex_desc ?></td>
                                                        <td><?= $ml->outsourcing_desc ?></td>
                                                        <td><?= $ml->others_desc ?></td>

                                                    </tr>

                                                </tbody>
                                            </table>


                                        </div>
                                    </section>
                                </div>
                                <div class="form-group" style="margin-top:10px">
                                    <label for="projects_assigned_to" class="control-label">Task & Description</label>
                                    <div class="controls">
                                        <?php
                                        $count = 1;
                                        $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '".$ml->milestone_id."'")->result();
                                        foreach ( $tp as $t ) {
                                        ?>
                                        <b><?= $count ?>. <?= $t->task_name ?> </b> <br>- <?= $t->description ?> <br>

                                    <?php $count++; } ?>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>


                <?php
                $collapse = "collapse";
                $noms++;  $milestone_cost = 0; 

                }

                ?>

            </div>
        </section>
    </section>
</div>
<script>
    $(".mcostdown").each(function () {
        var row = $(this).attr("rowid");
        $("#mcostup" + row).html("<b>Milestone Cost:</b> RM" + numberWithCommas(parseFloat($(this).text() * 1).toFixed(2)) + "&nbsp;&nbsp;");
    })
</script>