<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
    ?>

    <div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('projects_create_error') ?></h4>
        <?php echo $validation_errors; ?> </div>
    <?php
endif;

if (isset($projects)) {
    $projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';
?>
<style>
    .pad { padding:10px 15px; }

    .popover { width:300px; }
    .tooltip {
        position: fixed; 
    }
    .resimg {
        max-width:900px;
        height: auto;
        -ms-object-fit: cover;
        -moz-object-fit: cover;
        -o-object-fit: cover;
        -webkit-object-fit: cover;
        object-fit: cover;
        overflow: hidden;
    }
</style>
<div class="row" style="height:1000px">
    <div class="col-sm-12">
        <div class="panel-group m-b" id="accordion2">
            <div class="panel panel-default ">
                <div class="panel-heading clearfix "><?php echo form_open($this->uri->uri_string(), 'id="" data-parsley-validate'); ?>
                    <?php $pj = $this->db->query("select * from intg_projects where id = '" . $this->uri->segment(5) . "'")->row(); ?>
                    <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#" href="#project"><strong>Project: </strong>
                        <?= ucfirst($pj->project_name) ?>
                    </a> <span style="margin-left:30px"><strong><a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/milestone_chart/<?= $this->uri->segment(5) ?>" style="text-decoration:underline"> Time-cost Comparison Gantt</a></strong></span> <span class="pull-right"><strong>Project Costs: </strong>RM <span id="origpc">
                            <?= number_format($pj->project_cost, 2) ?>
                        </span></span>
                </div>

                <?php project_info($pj); ?>


            </div>
            <?php
            $noms = 1;
            $is_milestone_exist = 0;
                $ms = $this->db->query("select * from intg_milestones m where (m.parent_Rid= 0 OR m.parent_Rid is null)  and  m.project_id = '" . $this->uri->segment(5) . "' and status != 'Reject'  order by start_date  ")->result();
                $rc_approval = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $this->auth->user_id() . "  AND project_id = '" . $this->uri->segment(5) . "' ");
            
                foreach ($ms as $ml) {
//                if ($this->uri->segment(6)) {
//                    $ml->status = '';
//                    $ml->action = '';
//                }


                $v = $this->db->query("select max(m.version_no) as maxver from intg_milestones m where m.parent_Rid= '" . $ml->milestone_id . "' ")->row()->maxver;
                $mlid = $ml->milestone_id;

                if ($v > 0) {
//                                         
                    $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $ml->milestone_id . "' order by milestone_id desc limit 1")->row();
//                                                    echo "<br>Versioned id ".$ml->milestone_id."| Versioned status".$ml->status."| Versioned Parent m name".$ml->milestone_name;
                    $check = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->num_rows();
                    if ($ml->status == "Reject") {
//                                                        echo "<br>select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1";
//                                                        echo "Check ".$check;
                        if ($check > 0) {
                            $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                        } else {
                            $ml = $this->db->query("select * from intg_milestones where milestone_id = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                        }
                    } else {
                        
                    }
                }
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading clearfix"> 
                        <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#" href="#ml<?= $ml->milestone_id ?>"><strong>Milestone
                                <?= $noms ?>
                                : </strong>
                            <?= ucfirst($ml->milestone_name) ?>
                            <strong class="m-l">Start Date: </strong>
                            <?= date("d/m/Y", strtotime($ml->start_date)) ?>
                            <strong class="m-l">End Date: </strong>
                            <?= date("d/m/Y", strtotime($ml->end_date)) ?>
                        </a>
                        <?php
                        $status = '';
                        switch ($ml->status) {
                            case "No" :
                                if ($rc_approval->num_rows() == 0)
                                    $status = "<span class='badge btn-warning m-l text-xs'>Pending Approval</span>";
                                else
                                    $status = "<span class='badge btn-success m-l text-xs'>Sent For Approval</span>";
                                break;
                            case "Yes" :
                                $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                                break;
                            case "Reject" :
                                $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                                break;
                        }
                        echo $status;
                        $action = '';
                        if ($ml->action != '') {
                            switch ($ml->action) {
                                case "deactivate" :
                                    $action = "<span class='badge btn-warning m-l text-xs'>Sent For Deactivation Approval</span>";
                                    break;
                                case "activate" :
                                    $action = "<span class='badge btn-warning m-l text-xs'>Sent For Activation Approval</span>";
                                    break;
                                case "deactivated" :
                                    $action = "<span class='badge btn-danger m-l text-xs'>Deactivated</span>";
                                    break;
                                default :
                                    break;
                            }
                        }
                        echo $action;
                        ?>


                        <ul class="nav nav-pills nav-sm pull-left" style="padding:5px 20px">
                            <?php
                            if ($rc_approval->num_rows() == 0) {
                                if ($ml->action != 'deactivated') {
                                    ?>
                                    <li><a href="javascript:void(0)" class="edit_milestone  pull-right" milestone_id="<?= $ml->milestone_id ?>" project_id="<?= $ml->project_id ?>"><i class="fa fa-pencil"></i></a></li>
                                    <?php
                                }
                            }
                            ?>
    <li class="hour " data-toggle="popover"  data-content='<textarea name="comments"  cols="30" rows="2" class="form-control expand"><?= $ml->remarks ?></textarea><button class="btn btn-xs btn-primary cmpmark" style="margin-top:5px" rowid="<?= $ml->milestone_id ?>">Submit</button>' data-html="true" data-placement="bottom" data-original-title="Mark As Complete" data-comments="<?= $ml->remarks ?>"><!--<a href="javascript:void(0)"><i class="fa fa-info"></i></a>--></li>
                            <?php
                            if ($v > 0) {
                                ?>
                                <li>
                                    <?php
                                    if ($ml->version_no > 0) {
                                        $vno = "<span class='badge bg-info text-xs'>V" . $ml->version_no . " Version History</span> ";
                                    } else {
                                        $vno = "<span class='badge bg-info text-xs'>V0 Version History</span>";
                                    }

                                    if ($check == 0) {
                                        ?>
                                        <a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->milestone_id ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><?= $vno ?></a>
                                    <?php } else { ?>
                                        <a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->parent_Rid ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><?= $vno ?></a>
                                    <?php } ?>

                                </li>
                            <?php } ?>
                        </ul>


                        <span class="dropdown pull-right"> 
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-bars"></i> </a>
                            <ul class="dropdown-menu animated fadeInUp">
                                <?php if (($ml->action != '') && ($ml->action == 'deactivate' || $ml->action == 'activate')) { ?>
                                    <li>&nbsp;&nbsp;&nbsp;<i class="fa fa-exclamation"></i>&nbsp;&nbsp;&nbsp;Sent For Approval</li>
                                    <?php
                                }
                                if ($ml->action == '') {
                                    ?>
                                    <li><a href="#" onclick="milestone_activate_deactivate('deactivate',<?= $ml->project_id ?>,<?= $ml->milestone_id ?>)" >Deactivate This Milestone</a></li>
                                <?php } ?>

                                <?php if ($ml->action == 'activated') { ?><li><a href="#" onclick="milestone_activate_deactivate('deactivate',<?= $ml->project_id ?>,<?= $ml->milestone_id ?>)" >Deactivate This Milestone</a></li><?php } ?>
                                <?php if ($ml->action == 'deactivated') { ?><li><a href="#" onclick="milestone_activate_deactivate('activate',<?= $ml->project_id ?>,<?= $ml->milestone_id ?>)" >Activate This Milestone</a></li><?php } ?>
                                <li><a href="#" onclick="milestone_activate_deactivate('delete',<?= $ml->project_id ?>,<?= $ml->milestone_id ?>)" >Delete This Milestone</a></li>
                            </ul>
                        </span>


                        <span id="mcostup<?= $ml->milestone_id ?>" class="pull-right"></span> 
                    </div>
                    <div id="ml<?= $ml->milestone_id ?>" class="panel-collapse collapse" style="height: auto;">
                        <div class="panel-body text-sm">
                            <div class="col-sm-16" id="lineitem_form">
                                <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Resources</header>
                                <section class="panel panel-default">
                                    <div class="table-responsive ">
                                        <table class="table table-striped b-t b-light text-sm">
                                            <thead>
                                                <tr>
                                                    <th>Level</th>
                                                    <th>Man-days</th>
                                                    <th>Head Count</th>
                                                    <th>Total Man Days</th>
                                                    <th>Daily FTE (RM)</th>
                                                    <th>Time Cost (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='" . $ml->milestone_id . "'")->result();
                                                $milestone_cost = 0;
                                                foreach ($mu as $u) {
                                                    ?>
                                                    <tr >
                                                        <td><?= $u->user_level ?></td>
                                                        <td><?= $u->man_days ?></td>
                                                        <td><?= $u->head_count ?></td>
                                                        <td><?= $u->total_man_days ?></td>
                                                        <td><?= $u->fte ?></td>
                                                        <td align="right"><?= number_format($u->time_cost, 2) ?></td>
                                                    </tr>
                                                    <?php
                                                    $milestone_cost = $milestone_cost + $u->time_cost;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <span class="mcostdown" rowid="<?= $ml->milestone_id ?>" id="matcost<?= $ml->milestone_id ?>" style="display:none"><?php echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span> </div>
                                </section>
                                <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
                                <section class="panel panel-default">
                                    <div class="table-responsive ">
                                        <table class="table table-striped b-t b-light text-sm">
                                            <thead>
                                                <tr>
                                                    <th>Material Cost (RM)</th>
                                                    <th>New CAPEX (RM)</th>
                                                    <th>Outsourcing (RM)</th>
                                                    <th>Others (RM)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr >
                                                    <td><?= $ml->material_cost ?></td>
                                                    <td><?= $ml->new_capex ?></td>
                                                    <td><?= $ml->outsourcing ?></td>
                                                    <td><?= $ml->others ?></td>
                                                </tr>
                                            </tbody>

                                            <thead>
                                                <tr>
                                                    <th>Remarks</th>
                                                    <th>Remarks</th>
                                                    <th>Remarks</th>
                                                    <th>Remarks</th>                       
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr >
                                                    <td><?= $ml->mcost_desc ?></td>
                                                    <td><?= $ml->ncapex_desc ?></td>
                                                    <td><?= $ml->outsourcing_desc ?></td>
                                                    <td><?= $ml->others_desc ?></td>

                                                </tr>

                                            </tbody>

                                        </table>
                                    </div>
                                </section>
                            </div>
                            <div class="form-group" style="margin-top:10px">
                                <label for="projects_assigned_to" class="control-label"><strong>Task & Description</strong></label>
                                <div class="controls pad">

                                    <?php
                                    $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '" . $ml->milestone_id . "'")->result();
                                    $no = 1;
                                    foreach ($tp as $t) {
                                        ?>
                                        <b><?= $no ?>. <?= $t->task_name ?> </b>  <?= $t->description ?> <br>

                                        <?php
                                        $no++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $noms++;
                $is_milestone_exist++;
                $milestone_cost = 0;
            }
            ?>
        </div>
        <!--<section class="panel panel-default" id="formedit" <?php echo $pj->final_status == "Yes" ? 'style="display: none"' : "" ?> >-->

        <?php
        if (($pj->created_by == $this->auth->user_id()) && ($rc_approval->num_rows() == 0 || $this->uri->segment(6))) {
            //ADD MILESTONE SECTION
            ?>
            <section class="panel panel-default" id="formedit">
                <header class="panel-heading font-bold">Define Milestones & Tasks</header>
                <div class="panel-body" > <?php echo form_open($this->uri->uri_string(), 'id="formaly" data-parsley-validate'); ?>
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Milestone</label>
                            *
                            <input name="project_id" type="hidden" value="<?= $this->uri->segment(5) ?>" />
                            <input id="is_milestone_exist" name="is_milestone_exist" type="hidden" value="<?= $is_milestone_exist ?>" />
                            <input class="input-sm  form-control" id='projects_mile_stone' type='text' name='projects_mile_stone' maxlength="45" value="<?php echo set_value('projects_project_name', isset($projects['project_name']) ? $projects['project_name'] : ''); ?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label>Start Date</label>
                            *
                            <input style="cursor:pointer"  class="datepicker-input form-control input-xs input-sm" data-date-format="dd-mm-yyyy"  id='projects_milestone_start_date' type='text' name='projects_milestone_start_date' maxlength="45" value="<?php echo set_value('projects_milestone_start_date', isset($projects['project_start_date']) ? $projects['project_start_date'] : ''); ?>"  required readonly="readonly" />
                        </div>
                        <div class="col-sm-2">
                            <label>End Date*</label>
                            <input style="cursor:pointer"  class="  form-control input-xs input-sm"  id='projects_milestone_end_date'  type='text' name='projects_milestone_end_date' maxlength="45" value="<?php echo set_value('projects_project_end_date', isset($projects['project_end_date']) ? $projects['project_end_date'] : ''); ?>"  readonly="readonly"     data-date-start-date required />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-16" id="lineitem_form">
                        <header class="panel-heading font-bold bg-info" style="color: #ffffff; ">Resources </header>
                        <section class="panel panel-default">
                            <div class="table-responsive ">
                                <table class="table table-striped b-t b-light text-sm" id="expense_table">
                                    <thead>
                                        <tr>
                                            <th>Level*</th>
                                            <th>Man-days*</th>
                                            <th>Head Count*</th>
                                            <th>Total Man Days</th>
                                            <th>Daily FTE (RM)</th>
                                            <th>Time Cost (RM)</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="row1">
                                            <td><?php
                                                $ulevel = $this->db->query("select id,user_level from intg_user_level")->result();
                                                ?>
                                                <select name="select[]" id="select1" class="tc selecta form-control product" style="height:30px; font-size:12px; width:140px" onchange="select_fte(1)"  required>
                                                    <option></option>
                                                    <?php
                                                    foreach ($ulevel as $u) {
                                                        $fte = get_dailyfte_bydate($u->id, date('Y-m-d'));
                                                        $dfte = '0';
                                                        if ($fte) {
                                                            $dfte = $fte['daily_fte'];
                                                        } else {
                                                            $dfte = 'false';
                                                        }
                                                        ?>
                                                        <option value="<?= $u->id ?>" dfte="<?= $dfte ?>">
                                                            <?= $u->user_level ?>
                                                        </option>
                                                    <?php } ?>
                                                </select></td>
                                            <td><input  name="mandays[]" id="mandays1" disabled onChange="calc(1)"  class="input-sm input-xs  form-control resetit" data-parsley-type="number" required/></td>
                                            <td><input  name="headcount[]" id="headcount1" disabled  onChange="calc(1)"  class="input-sm  input-xs   form-control resetit" data-parsley-type="integer" required="required"/></td>
                                            <td><input type="hidden"  id="tmandays1" class="input-sm   input-xs  form-control" />
                                                <span id="dtmandays1" class="resetit"></span></td>
                                            <td><input type="hidden" name="fte[]" id="fte1" class="input-sm   input-xs  form-control"/>
                                                <span id="dfte1"></span></td>
                                            <td><input type="hidden"  class="tcosts input-sm input-xs  form-control resetit" id="tcost1"/>
                                                <span id="dtcost1" class="resetit"></span></td>
                                              <td><!--<i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="remrow('row1')"></i>--></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr id="lastrow">
                                            <td colspan="3"><a href="javascript:void(0)" onclick="addnewrow()"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Add Line</strong></a></td>
                                            <td colspan="2" align="right"><strong>Total Cost (RM)</strong></td>
                                            <td><span id="totalcost"></span></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-16">
                        <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Cost Involved<span class="showpc pull-right"></span></header>
                        <section class="panel panel-default">
                            <div class="panel-body" >
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6 column col-md-3">

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                                <label>Material Cost (RM)*</label>
                                                <input class="input-sm input-xs  form-control tcosts" id='material_cost' type='text' name='material_cost'  value="<?php echo set_value('material_cost', isset($projects['material_cost']) ? $projects['material_cost'] : ''); ?>" data-parsley-type="number" required="required" />
                                            </div>
                                            <div class="col-sm-6 col-xs-6 column col-md-12">
                                                <textarea name="mcost_desc" cols="40" rows="1" class="form-control input-sm input-s expand" placeholder="Remarks"  style="height: 30px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 column col-md-3">

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                                <label>New CAPEX (RM)*</label>
                                                <input class="input-sm input-xs  form-control tcosts" id='new_capex' type='text' name='new_capex'  value="<?php echo set_value('new_capex', isset($projects['new_capex']) ? $projects['new_capex'] : ''); ?>" data-parsley-type="number" required="required" />
                                            </div>
                                            <div class="col-sm-6 col-xs-6 column col-md-12">
                                                <textarea name="ncapex_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  placeholder="Remarks"  style="height: 30px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 column col-md-3">

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                                <label>Outsourcing (RM)*</label>
                                                <input class="input-sm input-xs  form-control tcosts" id='outsourcing' type='text' name='outsourcing'  value="<?php echo set_value('outsourcing', isset($projects['outsourcing']) ? $projects['outsourcing'] : ''); ?>" data-parsley-type="number" required="required" />  
                                            </div>
                                            <div class="col-sm-6 col-xs-6 column col-md-12">
                                                <textarea name="outsourcing_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  placeholder="Remarks"  style="height: 30px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 column col-md-3">

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                                <label>Others (RM)*</label>
                                                <input class="input-sm input-xs  form-control tcosts" id='others' type='text' name='others'  value="<?php echo set_value('others', isset($projects['others']) ? $projects['others'] : ''); ?>" data-parsley-type="number" required="required" />
                                            </div>
                                            <div class="col-sm-6 col-xs-6 column col-md-12">
                                                <textarea name="others_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  placeholder="Remarks"  style="height: 30px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                    <div class="form-group"> <?php
                        echo form_label('Task & Description*', 'projects_assigned_to', array('class' => 'control-label'));

                        $task_pool = $this->db->query("select * from intg_task_pool where status_nc IS NULL order by task_name asc")->result();
                        ?>
                        <div class='controls pad'>
                            <select multiple name="task_pool[]" id="task_pool" class="form-control" style=" width: 300px; height:auto; font-size:12px"  required>
                                <?php foreach ($task_pool as $tp): ?>
                                    <option value="<?= $tp->id ?>"   data-title="<?= $tp->description ?>"><?= $tp->task_name ?></option>
                                <?php endforeach; ?>
                                <option disabled="disabled"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class='controls pad'>
                            <input type="hidden" name="created_by" id="created_by" />
                            <input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                            <?php if ($pj->status != "Initiated") { ?>
                                                                                                        <!--<input type="submit" id="submit" name="save" class="btn btn-warning" value="Save as Draft" onclick="checkMilestoneExist();return confirm('Confirm Save As Draft')"  />-->
                                <input type="submit" id="submit" name="save" class="btn btn-warning" value="Save as Draft" onclick="checkMilestoneExist(this.value);"  />
                            <?php } ?>

                            &nbsp;&nbsp;
                            <!--<input type="submit" id="submit2" name="save" class="btn btn-info" value="Save &  Create New Milestone" onclick="checkMilestoneExist();return confirm('Confirm Save & Create Another Milestone')"  />-->
                            <input type="submit" id="submit2" name="save" class="btn btn-info" value="Save &  Create New Milestone" onclick="checkMilestoneExist(this.value);"  />
                            &nbsp;&nbsp;

                            <?php if ($pj->final_status != "Yes") { ?>
                                                                                                                                 <!--<input type="submit" id="submit3" name="save" class="btn btn-primary " value="Submit For Approval" onclick="checkMilestoneExist();return confirm('Do you want to submit this project for approval')"/>-->
                                <input type="submit" id="submit3" name="save" class="btn btn-primary " value="Submit For Approval" onclick="checkMilestoneExist(this.value);"/>
                            <?php } ?>
                        </div>
                    </div>
                    <?php echo form_close(); ?> </div>
            </section>
            <?php
            //END ADD MILESTONE SECTION
        }
        ?>
    </div>
    <?php echo form_open($this->uri->uri_string(), 'id="" data-parsley-validate'); ?>
    <?php
    $trws = $this->db->query("select count(*) as numrows from intg_milestones where project_id=" . $this->uri->segment(5) . " and status='No'")->row();
    $rows = $trws->numrows;
    if ($this->auth->user_id() == $pj->initiator && $rows > 0 && $pj->final_status == "Yes") {
        ?>
        <div class="form-actions">
            <div class='controls pad'>

                <?php
//              $rc = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = ".$this->auth->user_id()."  AND project_id = '".$this->uri->segment(5)."'");
                if ($rc_approval->num_rows() == 0) {
                    ?> 
                    <input type="hidden" name="created_by" id="created_by" />
                    <input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                    <input type="hidden" name="project_code" value="<?= $pj->prefix_io_number ?>">
                    <input type="submit" id="submit" name="m_approval" class="btn btn-primary" value="Submit For Milestone Approval"  />

                </div>
            </div>
            <?php
        }
    }
    ?>
    <?php echo form_close(); ?> 
    <!--end of col6--> 

</div>
<!--end of row-->
<?php if ($pj->attachment != "") { ?>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:auto">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">
                        <?= $att[1] ?>
                    </h4>
                </div>
                <div class="modal-body" align="center"> <img src="<?= base_url() ?>uploads/<?= $att[0] ?>" class="resimg"/> </div>
                <div class="modal-footer"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a> </div>
            </div>
        </div>
    </div>
    <?php
}

$psd = explode("-", $pj->project_start_date);
$ped = explode("-", $pj->project_end_date);
?>


<script src="<?php echo Template::theme_url('js/parsley/parsley.min.js'); ?>" cache="false"></script> 
<script>

                                    function inArray(needle, haystack) {
                                        var length = haystack.length;
                                        for (var i = 0; i < length; i++) {
                                            if (haystack[i] == needle)
                                                return true;
                                        }
                                        return false;
                                    }

                                    $(".hour").on("focus", function () {
                                        $(this).popover('destroy');
                                        $(".popover").hide();
                                    });

                                    $('body').on('click', '.popover button', function () {

                                        var a = $(this);
                                        var ta = $(this).parent().parent().find("textarea").val();

                                        $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/milestone/markcomplete') ?>", {remarks: ta, rowid: $(this).attr("rowid")}, function (data) {

                                            $(".popover").hide();
                                        });
                                    });

                                    function numberWithCommas(x) {
                                        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    }

                                    $(document).ready(function () {
                                        $("#formaly").parsley("validate");
                                        var enddate = $("#projects_milestone_editend_date").val();
                                        $("#projects_milestone_editend_date").datepicker().on('changeDate', function () {
                                            if (Math.round((parseDate($("#projects_milestone_editend_date").val()) - parseDate(enddate)) / (1000 * 60 * 60 * 24) + 1) < 1) {
                                                // $(".selecta").select2("val", "");
                                                $(".resetit").each(function () {
                                                    $(this).val("");
                                                    $(this).text("");
                                                });
                                            }
                                        });

                                        $(".mcostdown").each(function () {
                                            var row = $(this).attr("rowid");
                                            $("#mcostup" + row).html("<b>Milestone Cost:</b> RM " + numberWithCommas(parseFloat($(this).text() * 1).toFixed(2)) + "&nbsp;&nbsp;");
                                        })

                                        var pc = "<?= $pj->project_cost ?>";

                                        $(".tcosts").on("change", function () {
                                            var a = 0;
                                            $(".tcosts").each(function () {
                                                a = a + $(this).val() * 1;
                                                console.log("total:" + a);
                                            });

                                            var c = a + pc * 1;
                                            console.log(c);
                                            $(".showpc").text("Calculated Project Cost is: RM " + numberWithCommas(parseFloat(c).toFixed(2)));
                                        });



                                        window.ParsleyConfig = {
                                            errorsWrapper: '<div class="pe"></div>',
                                        };
                                    });

                                    $(".edit_milestone").on("click", function () {

                                        $("#project").collapse('hide');
                                        //$("#ml"+$(this).attr("milestone_id")).collapse('hide');
                                        waitingDialog.show('...launching milestone edit');
                                        $("#formedit").html("<i class='fa fa-spin fa-spinner'></i>");
                                        $.post("<?= site_url() ?>/admin/projectmgmt/projects/edit_milestone/" + $(this).attr("milestone_id") + "/" + $(this).attr("project_id"), {project_value: "<?= $pj->project_cost ?>", milestone_value: $("#matcost" + $(this).attr("milestone_id")).text()}, function (data) {
                                            if (!$('#formedit').is(':visible')) {
                                                $("#formedit").css('display', 'inherit');
                                            }
                                            $("#formedit").html(data);
                                            waitingDialog.hide();
                                        }).done(function () {
                                        });

                                    });

                                    function parseDate(str) {
                                        var mdy = str.split('-')
                                        //console.log(mdy);
                                        return new Date(mdy[2], mdy[1] - 1, mdy[0]);
                                    }

                                    function daydiff(first, second) {
                                        return Math.round((second - first) / (1000 * 60 * 60 * 24) + 1);
                                    }

                                    function calc(val)
                                    {

                                        var sd = $("#projects_milestone_start_date").val();
                                        var ed = $("#projects_milestone_end_date").val();
                                        var a = $("#mandays" + val).val() * 1;

                                        if (a > daydiff(parseDate(sd), parseDate(ed)))
                                        {
                                            alert("please enter value less than the start and end date difference ");
                                            $("#mandays" + val).val("");
                                            $("#mandays" + val).focus();
                                            return false;
                                        }
                                        if (isNaN(daydiff(parseDate(sd), parseDate(ed))))
                                        {
                                            alert("please enter Milestone dates");
                                            $("#mandays" + val).val("");
                                            $("#mandays" + val).focus();
                                            return false;
                                        }

                                        var b = $("#headcount" + val).val() * 1;
                                        var c = parseFloat(a * b);
                                        var f = $("#tmandays" + val).val(c);
                                        var d = parseInt($("#fte" + val).val());
                                        var e = c * d;
                                        $("#dtcost" + val).text(numberWithCommas(e));
                                        $("#dfte" + val).text(numberWithCommas(d));
                                        $("#dtmandays" + val).text(numberWithCommas(c));

                                        $("#tcost" + val).val(e);
                                        var pc = "<?= $pj->project_cost ?>";

                                        var a = 0;
                                        $(".tcosts").each(function () {
                                            a = a + $(this).val() * 1;
                                            $("#totalcost").text(numberWithCommas(parseFloat(a).toFixed(2)));

                                        });

                                        var c = a + pc * 1;

                                        $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));
                                    }

                                    function select_fte(val)
                                    {
                                        $("#headcount" + val).removeAttr("disabled");
                                        $("#mandays" + val).removeAttr("disabled");
                                        $("#fte" + val).val($("#select" + val + "  option:selected").attr("dfte"));
                                        calc(val);
                                    }

                                    function addnewrow()
                                    {
                                        var theId = $("#expense_table tr").length + 1;
                                        $('#lastrow').before('<tr id=row' + theId + '><td><select name="select[]" id="select' + theId + '" required class="selecta form-control product"   onChange="select_fte(' + theId + ')"  style="height:30px; font-size:12px;  width:140px" ><option></option></select></td><td><input type="number"  required  disabled data-parsley-type="number"  name="mandays[]" id="mandays' + theId + '"  onChange="calc(' + theId + ')" class="input-sm input-xs  form-control"/></td><td><input type="number" name="headcount[]"  data-parsley-type="number" required disabled onChange="calc(' + theId + ')"  id="headcount' + theId + '"  class="input-sm  input-xs   form-control"/></td><td><input type="hidden" name="tmandays[]" id="tmandays' + theId + '" class="input-sm   input-xs  form-control" /><span id="dtmandays' + theId + '"></span></td><td><input type="hidden" name="fte[]" id="fte' + theId + '" class="input-sm   input-xs  form-control" /><span id="dfte' + theId + '"></span></td><td><input type="hidden" name="tcost[]" class="tcosts input-sm input-xs  form-control" id="tcost' + theId + '" /><span id="dtcost' + theId + '"></span></td><td><i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="remrow(\'row' + theId + '\')"></i></td></tr>');

                                        var existing_catg = [];
                                        var Attendees_list = [];

                                        /* $('#select1 option').clone().appendTo('#select'+theId);		
                                         $('#select'+theId).select2(		
                                         { placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:true}		
                                         );*/
                                        $('#select' + theId).select2(
                                                {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true}
                                        );

<?php
foreach ($ulevel as $c) {
    $fte = get_dailyfte_bydate($c->id, date('Y-m-d'));
    $dfte = '0';
    if ($fte) {
        $dfte = $fte['daily_fte'];
    } else {
        $dfte = 'false';
    }
    ?>
                                            var attendees = new Object();
                                            attendees.id = "<?= $c->id ?>";
                                            attendees.user_level = "<?= $c->user_level ?>";
                                            attendees.daily_fte = "<?= $dfte ?>";
                                            Attendees_list.push(attendees);
<?php } ?>


                                        $(".product").each(function () {
                                            existing_catg.push($(this).val());
                                        });

                                        $.each(Attendees_list, function (key, value) {

                                            if (inArray(value.id, existing_catg) == true) {
                                            } else {
                                                $('#select' + theId + '').append("<option value='" + value.id + "' dfte='" + value.daily_fte + "'>" + value.user_level + "</option>");
                                            }

                                        });

                                        $('#formaly').parsley('addItem', '#select' + theId);
                                        $('#formaly').parsley('addItem', '#mandays' + theId);
                                        $('#formaly').parsley('addItem', '#headcount' + theId);

                                    }

                                    function remrow(val)
                                    {
                                        $("#" + val).hide('fast', function () {
                                            $("#" + val).remove();
                                        });
                                        setTimeout(function () {
                                        }, 500);
                                    }

                                    $(document).ready(function () {
                                        var psd, res;
                                        $('#projects_milestone_start_date').datepicker({
                                            format: "dd-mm-yyyy",
                                            startDate: "<?= $psd[2] . "/" . $psd[1] . "/" . $psd[0] ?>",
                                            endDate: "<?= $ped[2] . "/" . $ped[1] . "/" . $ped[0] ?>"
                                        }).on('changeDate', function () {

                                            $('#projects_milestone_end_date').datepicker('clearDates');

                                            psd = $('#projects_milestone_start_date').val();
                                            res = psd.split("-");
                                            console.log(res[0]);

                                            $("#projects_milestone_end_date").datepicker("setStartDate", res[0] + "/" + res[1] + "/" + res[2]);

                                            $('.datepicker').hide();
                                            $(this).removeClass('parsley-error');
                                            var aid = $(this).attr("data-parsley-id");
                                            $('#parsley-id-' + aid).remove();

                                        });

                                        $('#projects_milestone_end_date').datepicker({
                                            format: "dd-mm-yyyy",
                                            // startDate: ""+$('#projects_milestone_end_date').attr("data-date-start-date")+"",
                                            //endDate: "<?= $ped[2] . "/" . $ped[1] . "/" . $ped[0] ?>"
                                        }).on('changeDate', function () {
                                            var mystr = this.value;
                                            var myarr = mystr.split("-");

                                            var x = new Date(myarr[2] + "-" + myarr[1] + '-' + myarr[0]);
                                            var y = new Date('<?= $ped[0] . "-" . $ped[1] . "-" . $ped[2] ?>');


                                            if (x > y) {
                                                var r = confirm("You are selecting date that is after the project end date. This will automatically update the Project End Date according to your selection. Are you sure want to do this?");
                                                if (r == true) {

                                                } else {
                                                    $('#projects_milestone_end_date').val('');
                                                    return;
                                                }
                                            }

                                            $('.datepicker').hide()
                                        });

                                        $('#task_pool').select2(
                                                {
                                                    formatResult: format,
                                                    formatSelection: format,
                                                    placeholder: "Please Select",
                                                    allowClear: false, dropdownAutoWidth: true
                                                }
                                        );
                                    });

                                    function format(issue) {

                                        $('[data-toggle="tooltip"]').tooltip();

                                        var originalOption = issue.element;
                                        return "<div  data-toggle='tooltip' data-placement='top'  title='" + $(originalOption).data('title') + "'>" + issue.text + "</div>";
                                    }

                                    function checkMilestoneExist(msj) {
                                        var ms = $('#is_milestone_exist').val();

                                        var r = confirm("Confirm " + msj + "?");
                                        if (r == true) {

                                            // later, now switching back to enable submit
                                            $("#formaly").unbind("submit", preventDefault);

                                            if (msj == 'Submit For Approval') {
                                                if (ms > 0) {
                                                    $('#formaly').off('submit.Parsley');
                                                    $('#formaly').submit();
                                                }
                                            } else {

                                                $('#formaly').submit();
                                            }

                                        } else {
                                            //prevent default submit
                                            $("#formaly").bind("submit", preventDefault);
                                        }

                                    }

                                    function preventDefault(e) {
                                        e.preventDefault();
                                    }



                                    function milestone_activate_deactivate(action, project_id, milestone_id) {


                                        var r = confirm("Confirm Submit?");
                                        if (r == true) {

                                            var check = "<?= $this->settings_lib->item('ext.milestone_deactivation_wf'); ?>";

                                            var text = action;
                                            if (check == 'yes') {
                                                text = 'approval';
                                            } else {
                                                if (text == 'deactivate') {
                                                    text = 'deactivation';
                                                } else if (text == 'delete') {
                                                    text = 'deletion';
                                                } else {
                                                    text = 'activation';
                                                }
                                            }



                                            waitingDialog.show('...Submitting for ' + text + ', please wait');

                                            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/milestone_activate_deactivate/') ?>/" + action + '/' + project_id + '/' + milestone_id,
                                                    function (resp) {
                                                        if (resp.status == true) {
                                                            //                alert(resp.message);
                                                            waitingDialog.show();
                                                            alert(resp.message);
                                                            location.reload();
                                                        } else {
                                                            //                alert(resp.message);
                                                            waitingDialog.show();
                                                            alert(resp.message);
                                                            waitingDialog.hide();
                                                        }
                                                    }, 'json');

                                        }

                                    }



</script> 
