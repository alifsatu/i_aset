
<style type="text/css">
	span.simplecolorpicker.inline {
		display: table-row !important;
	}
	
.tooltip-inner  {
  max-width: 500px; height:auto;
}

</style>
<div id="myGrid">
<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
?>
<div class="alert alert-danger fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading"><? echo lang('projects_create_error')?></h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($projects))
{
	$projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';

?>

		
		<div class="row">
                <div class="col-sm-12">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold"><? echo lang('projects')?></header>
                    <div class="panel-body">
                          

	<?php echo form_open_multipart($this->uri->uri_string(), 'id="forma"'); ?>
    	
		<div class="form-group  clearfix">
		
		<div class="col-sm-3">
            <label><? echo lang('projects_name'); ?> *</label>
            <div class='controls'>
            <input class="input-sm input-s form-control" id='projects_project_name' type='text' name='projects_project_name' maxlength="255" value="<?php echo $this->input->post('projects_project_name'); ?>" data-required="true" />
          </div>
          </div>
		  
					 <?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
			 ?>
		  <div class="col-sm-3">
              <label>SBU *</label>
               <div class='controls'>
             <select name="projects_sbu" id="projects_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
              <option></option>
            <? foreach($prgm as $pn) { if ( $this->input->post('projects_sbu')==$pn->id ) {?>
             <option selected="selected" value=<?=$pn->id?>><b><?=$pn->sbu_name?></b></option><? } else { ?>
               <option value=<?=$pn->id?>><b><?=$pn->sbu_name?></b></option><? }} ?>
            </select> </div></div>
			<?php // Change the values in this array to populate your dropdown as required
				
$cc = $this->db->query('SELECT * FROM  intg_cost_centre WHERE deleted = 0 ORDER BY cost_centre ')->result();
			 ?>
<div class="col-sm-3">
            <label>Cost Centre *</label>
             <div class='controls'>
            <select name="projects_cost_centre" id="projects_cost_centre" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
             <option></option>
            <? foreach($cc as $c) { if ( $this->input->post('projects_cost_centre')==$c->id ) {?>
             <option selected value=<?=$c->id?>><b><?=$c->cost_centre?></b></option><? } else { ?>
             <option  value=<?=$c->id?>><b><?=$c->cost_centre?></b></option><? } } ?>
            </select>
			</div>
            </div>
             <?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_programs WHERE deleted = 0 ORDER BY program_name ')->result();
			 ?>
			 <div class="col-sm-3">
            <label>Programme *</label>
          <div class='controls'>
            <select name="projects_Programme" id="projects_Programme" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
           <option></option>
            
            <? foreach($prgm as $p) { if ( $this->input->post('projects_Programme')==$p->id ) {?>
                    
            
             <option selected value="<?=$p->id?>"><b><?=$p->program_name?></b></option><? }  else { ?>
             <option value=<?=$p->id?>><b><?=$p->program_name?></b></option><? } } ?>
            </select>  </div> </div>
		  </div>
			 
			 <div class="form-group  clearfix">
			
			 
				  <div class="col-sm-3">
				  <label>Justification Of Project *</label>
                  <div class='controls'>
				  <?php echo form_textarea( array( 'class' => 'form-control input-sm input-s expand', 'name' => 'projects_justification','data-required='=>'true', 'id' => 'projects_justification', 'rows' => '1', 'cols' => '40', 'value' => $this->input->post('projects_justification') ) ); ?>
					
             </div></div>
			  
			 <div class="col-sm-3">
				  <label>Co Worker *</label>
                    <div class='controls'>
				 <select multiple name="projects_assigned_to[]" id="projects_assigned_to" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
						<?php foreach($users as $user): 
						if ( in_array($user->id,$this->input->post('projects_assigned_to'))) { ?>
						<option selected="selected" value="<?=$user->id?>"><?=$user->display_name;?> (MYR <?php echo (number_format(($user->salary)/22));?>)</option><? } else { ?>
                        <option value="<?=$user->id?>"><?=$user->display_name;?> (MYR <?php echo (number_format(($user->salary)/22));?>)</option><? } ?>
						<?php endforeach;?>
					</select>
             </div></div>
			<div class="col-sm-3">
				  <label>Duration In Days *</label>
                   <div class='controls'>
				  <input   class="input-sm input-s form-control" id='projects_duration_days' type='text' name='projects_duration_days'  value="<?php echo $this->input->post('projects_duration_days'); ?>"  data-required="true" />
         	
             </div>  </div>
             <div class="col-sm-3">
				  <label>Project Color * </label>
                   <div class='controls'>
                 <select name="projects_color" id="projects_color" class="color" style="display:table-row">
				<!-- <select  name="projects_color" id="projects_color" class="form-control color selecta"  style="height:30px; font-size:12px; width:200px;display:table-row"  data-required="true" >-->
               
					<option value="#7bd148">Green</option>
						<option value="#5484ed">Bold blue</option>
						<option value="#a4bdfc">Blue</option>
						<option value="#46d6db">Turquoise</option>
						<option value="#7ae7bf">Light green</option>
						<option value="#51b749">Bold green</option>
						<option value="#fbd75b">Yellow</option>
						<option value="#ffb878" selected>Orange</option>
						<option value="#ff887c">Red</option>
						<option value="#dc2127">Bold red</option>
						<option value="#dbadff">Purple</option>
						<!--<option value="#e1e1e1">Gray</option>-->
					</select>
                     </div>
                 
				 </div>
			 
			 </div>
			
			 	 <div class="form-group  clearfix">
				
			   
			 <div class="col-sm-3">
				  <label>Start Date *</label>
                    <div class='controls'>
				  <input    class="datepicker-input form-control input-sm input-s"  id='projects_project_start_date' type='text' name='projects_project_start_date'  value="<?php echo $this->input->post('projects_project_start_date'); ?>"  data-required="true" />	
             </div> 
             </div>
			 <div class="col-sm-3">
				  <label>End Date *</label>
                  <div class='controls'>
				  <input  class="datepicker-input form-control input-sm input-s"  id='projects_project_end_date' type='text' name='projects_project_end_date'  value="<?php echo $this->input->post('projects_project_end_date'); ?>"  data-required="true" />
					
             </div></div>
			 
			
				 
</div>

			
			
			<div class="form-group  clearfix">
			
			 <div class="col-sm-10">
                      <label>Introduction</label>
                       <div class='controls'>
                     
                        <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                              <ul class="dropdown-menu">
                              </ul>
                          </div>
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                              <ul class="dropdown-menu">
                              <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                              <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                              <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                              </ul>
                          </div>
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                          </div>
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                          </div>
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                          </div>
                          <div class="btn-group">
                          <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                            <div class="dropdown-menu">
                              <div class="input-group m-l-xs m-r-xs">
                                <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink"/>
                                <div class="input-group-btn">
                                  <button class="btn btn-default btn-sm" type="button">Add</button>
                                </div>
                              </div>
                            </div>
                            <a class="btn btn-default btn-sm" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                          </div>
                          
                          
                          <div class="btn-group">
                            <a class="btn btn-default btn-sm" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                            <a class="btn btn-default btn-sm" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                          </div>
                        
                        </div>
                        <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px">
                          Go ahead&hellip;
                        </div>
                      </div>
                    </div>
            
			
			</div>
			
<div class="form-group  clearfix">
			<div class="form-actions col-sm-5"> <div class='controls' style="margin:5px 5px"><input type="file" name="files[]" multiple="multiple" /></div></div></div>
		
			<div class="form-group  clearfix">
			<div class="form-actions col-sm-5">
				<input type="hidden" name="created_by" id="created_by" />
				<input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                  <input type="hidden" name="introduction" />
				<input type="submit" id="submit" name="draft" class="btn btn-warning" value="Save as Draft & Next"  />
				&nbsp;&nbsp;
				<input type="submit" id="submit2"  name="save" class="btn btn-primary" value="Save & Next"  />
				
				
			</div></div>
		
					<?php echo form_close(); ?>
                    
                    
                    </div>
                    </section>
			</div><!--end of col6-->
		
    
</div> <!--end of row-->
</div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.2.7/tinymce.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.2.7/jquery.tinymce.min.js"></script>
<script src="<? echo Template::theme_url('js/parsleyold/parsley.min.js'); ?>" cache="false"></script> 
<script src="<?=Template::theme_url('js/wysiwyg/jquery.hotkeys.js')?>"></script>
<script src="<?=Template::theme_url('js/wysiwyg/bootstrap-wysiwyg.js')?>"></script>
<script src="<?=Template::theme_url('js/wysiwyg/demo.js')?>"></script>
<script>
         $(function() {
            $('#myGrid').gridEditor({
                new_row_layouts: [[12], [6,6], [9,3]],
                content_types: ['tinymce'],
            });
            
            // Get resulting html
            var html = $('#myGrid').gridEditor('getHtml');
            console.log(html);
        });
   
 $("#submit,#submit2").click(function(e) {
	

if ( !$("#forma").parsley('validate') ) {
e.preventDefault();
 
		}   
	
	}); 

   
   $("[name=save]").click(function() {
$("[name=introduction]").val($("#editor").cleanHtml());
  
});
$(document).ready(function() {
		$('.color').simplecolorpicker('selectColor', '<?=$this->input->post('projects_color')?>');
	
  /* $('#projects_project_start_date,#projects_project_end_date').datepicker().on('changeDate', function(){
          $('.datepicker').hide();
		   $(this).removeClass('parsley-error');
		   var aid = $(this).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
        });
		*/
		
		var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
 
var checkin = $('#projects_project_start_date').datepicker({
  onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
	
  }
}).on('changeDate', function(ev) {
  if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
	
	
	
	  $(this).removeClass('parsley-error');
		   var aid = $(this).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
  }
  checkin.hide();
  $('#projects_project_end_date')[0].focus();
}).data('datepicker');
var checkout = $('#projects_project_end_date').datepicker({
  onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
  }
}).on('changeDate', function(ev) {
  checkout.hide();
    $(this).removeClass('parsley-error');
		   var aid = $(this).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
}).data('datepicker');
		
 });
 
 

</script>
