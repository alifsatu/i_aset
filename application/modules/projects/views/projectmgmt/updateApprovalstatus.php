<?php
$statusText = "No Status";
if ($_POST['status'] == "No") {
    $statusText = "Pending";
}
if ($_POST['status'] == "Yes") {
    $statusText = "Approved";
}
if ($_POST['status'] == "Reject") {
    $statusText = "Rejected";
}


// project approvval
if (isset($_POST['type']) != "mile") {
    ?>	

    <div>
        <div id="inner1"><?php
            $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());
            $queryapprover = $this->db->query('SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="40" and approval_status_mrowid = "' . $_POST['mrowid'] . '"  ORDER BY id asc')->result();

            $val = NULL;
            foreach ($queryapprover as $v) {
                if ($val == 1) {
                    $nextapprover = $v->rolename;
                    $nextapprover_id = $v->approval_status_action_by;
                    break;
                }

                if ($v->rolename == $role_name) {
                    $val = 1;
                }
            }

            $last = end($queryapprover);

            $queryinitiator = $this->db->query('SELECT id,role_id,email,display_name FROM intg_users  WHERE id = "' . $_POST['initiator'] . '"');
            $rowinitiator = $queryinitiator->row();

            switch ($_POST['status']) {
                case "Yes" : $status = "Approved";
                    break;
                case "No" : $status = "Pending";
                    break;
                case "Reject" : $status = "Rejected";
                    break;
            }

            $data = array('approval_status_status' => $_POST['status'], 'approval_status_action_date' => date('Y-m-d h:i:s'),);

            if ($_POST['status'] == "Reject") {
                $this->db->where('approval_status_mrowid', $_POST['mrowid']);
                $this->db->update('intg_approval_status', $data);

                $this->db->query('UPDATE intg_projects SET final_status = "Reject" WHERE id = "' . $_POST['docid'] . '"');

                //Do if check here, If versioned milestone, set status Reject....For Deactivated milestone set status to original status or set to Status = Yes
                $this->db->query('UPDATE intg_milestones SET status = "Reject" WHERE project_id = "' . $_POST['docid'] . '" ');
            } else {
                $this->db->where('id', $_POST['rowid']);
                $this->db->update('intg_approval_status', $data);
            }

            $data2 = array(
                'approvers_status' => $_POST['status'], 'approvers_remarks' => $_POST['remarks'], 'approvers_approve_date' => date('Y-m-d h:i:s'));
            $this->db->where('approvers_appstatrowid', $_POST['rowid']);
            $this->db->where('approvers_approver', $this->auth->user_id());
            $this->db->update('intg_approvers', $data2);

            $fapp = $_POST['finalapprover'];
            $f_app = explode(",", $fapp);
            if ($this->auth->user_id() == $_POST['finalapprover'] || in_array($this->auth->user_id(), $f_app)) {

                $this->db->query('UPDATE intg_projects SET final_status = "' . $_POST['status'] . '" WHERE id = "' . $_POST['docid'] . '" AND FIND_IN_SET ("' . $this->auth->user_id() . '",final_approvers)');
                $this->db->query('UPDATE intg_milestones SET status = "' . $_POST['status'] . '" WHERE project_id = "' . $_POST['docid'] . '" ');

                // code to check if approval confirmed, check milestone start and date,check cso workflow and  populate timesheet and timesheet details
            }
            $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="40" and bas.approval_status_mrowid = "' . $_POST['mrowid'] . '" ORDER BY bas.id asc')->result();

            if ($_POST['act'] != "edit") {

                if ($_POST['status'] == "Reject") {
                    $btntype = "btn-danger";
                    $iconclass = "times";
                }
                if ($_POST['status'] == "Yes") {
                    $btntype = "btn-success";
                    $iconclass = "check";
                }
                ?>
                <span id="animationSandbox<?= $_POST['mrowid'] ?>" style="display: block;" class="flipInX animated"> <a href="#" id="p<?= $_POST['mrowid'] ?>" data-toggle="popover" data-html="true" data-placement="left" data-trigger="hover"  data-original-title="Approvers Hierarchy" 
                                                                                                                        data-content="<div class='scrollable' style='height:auto;width:500px'><table width='100%' class='table table-condensed'>
                                                                                                                        <tr>   
                                                                                                                        <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                                                                                                                        <td><strong><?php echo lang('quote_role') ?></strong></td>
                                                                                                                        <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                                                                                                                        <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                                                                                                                        <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                                                                                                                        </tr><?php
                                                                                                                        foreach ($queryappr as $rowappr) {

                                                                                                                            switch ($rowappr->approvers_status) {
                                                                                                                                case "No" : $status = "<span class='badge btn-warning'>Pending</span>";
                                                                                                                                    break;
                                                                                                                                case "Yes" : $status = "<span class='badge btn-primary'>Approved</span>";
                                                                                                                                    break;
                                                                                                                                case "Reject" : $status = "<span class='badge btn-danger'>Rejected</span>";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                            ?>

                                                                                                                            <tr>   
                                                                                                                            <td><?= $rowappr->display_name ?></td>
                                                                                                                            <td><?= $rowappr->role_name ?></td>
                                                                                                                            <td><?= $status ?></td>
                                                                                                                            <td><?= $rowappr->approvers_remarks ?></td>
                                                                                                                            <td><?php
                                                                                                                            if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                                                                                                                echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                                                                                                                            }
                                                                                                                            ?></td>

                                                                                                                            </tr><?php } ?></table>" class="btn <?= $btntype ?>"><i class="fa fa-<?= $iconclass ?>"></i> </a></span>

                <script>
                    $('#p' +<?= $_POST['mrowid'] ?>).popover('destroy');
                    $('#p' +<?= $_POST['mrowid'] ?>).hover(function () {
                        $('#p' +<?= $_POST['mrowid'] ?>).popover('show');
                    });

                    /*
                     $(document).ajaxStop(function(){
                     window.location.reload();
                     });*/
                </script><?php } ?>
            <!----------------Email & Notifications code----->
            <?php
            if ($_POST['status'] == "Yes") {
                $fapp = $_POST['finalapprover'];
                $f_app = explode(",", $fapp);
                if ($this->auth->user_id() == $_POST['finalapprover'] || in_array($this->auth->user_id(), $f_app)) {
                    //Project approver, For final  APPROVER email				
                    $this->load->library('emailer/emailer');
                    $ver = $this->db->query('SELECT parent_Rid,version_no,project_name,initiator,project_start_date,project_end_date FROM intg_projects WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
                    $ver_i = $ver->row();
                    if ($ver_i->parent_Rid == "0") {
                        $row_id = $_POST['docid'];
                    } else {
                        $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                    }

//                    switch ($_POST['status']) {
//                        case "No" : $s = "Pending";
//                            break;
//                        case "Yes" : $s = "Approved";
//                            break;
//                        case "Reject" : $s = "Rejected";
//                            break;
//                    }
                    //COMMENTED THE CODE BELOW TO PREVENT SENDING MAIL

                    $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);
                    $this->emailer->Subject = 'Project ' . $ver_i->project_name . ' has been Reviewed';
                    $this->emailer->Body = $this->load->view('_emails/project_status', array('item_type' => 'project', 'item_name' => $ver_i->project_name, 'assignee_name' => $rowinitiator->display_name, 'start_date' => $ver_i->project_start_date, 'end_date' => $ver_i->project_end_date, 'creator_name' => $creator->display_name, 'status' => $_POST['status']), TRUE);

                    $this->emailer->mail();
                    $this->load->model('notifications/notifications_model', null, true);
                    $notification_data = array();
                    $this->notifications_model->insert(array(
                        'user_id' => $rowinitiator->id,
                        'title' => 'Project ' . $ver_i->project_name . ' has been reviewed, ' . $statusText,
                        'content' => 'Project ' . $ver_i->project_name . ' was ' . $statusText . ' by ' . $current_user->display_name,
                        'link' => site_url('admin/projectmgmt/projects/approval'),
                        'class' => 'fa-briefcase',
                        'module' => 'ProjectMgmt',
                        'type' => 'info'
                    ));
                } else {

                    //Project approver, For next  APPROVER email
                    $approver = $nextapprover;
                    $user_iids = (explode(",", $nextapprover_id));
                    $size = sizeof($user_iids);

                    for ($i = 0; $i < $size; $i++) {
                        $apdet = $this->db->query("SELECT id,display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
                        $rowapp = $apdet->row();

                        $this->load->library('emailer/emailer');
                        $ver = $this->db->query('SELECT parent_Rid,version_no,project_name,initiator,project_start_date,project_end_date FROM intg_projects WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
                        $ver_i = $ver->row();
                        $creator = $this->db->query("SELECT display_name FROM intg_users WHERE id = '" . $ver_i->initiator . "'")->row();
                        if ($ver_i->parent_Rid == "0") {
                            $row_id = $_POST['docid'];
                        } else {
                            $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                        }
                        $this->emailer->addAddress($rowapp->email, $rowapp->display_name);
                        $this->emailer->Subject = 'Project ' . $ver_i->project_name . ' is ready for approval';
                        $this->emailer->Body = $this->load->view('_emails/project_app', array('item_type' => 'project', 'item_name' => $ver_i->project_name, 'assignee_name' => $rowapp->display_name, 'start_date' => $ver_i->project_start_date, 'end_date' => $ver_i->project_end_date, 'creator_name' => $creator->display_name), TRUE);

                        $this->emailer->mail();
                        $this->load->model('notifications/notifications_model', null, true);
                        $notification_data = array();
                        $this->notifications_model->insert(array(
                            'user_id' => $rowapp->id,
                            'title' => 'Project ' . $ver_i->project_name . ' is ready for approval',
                            'content' => 'Project ' . $ver_i->project_name . ' by ' . $this->auth->display_name_by_id($ver_i->initiator),
                            'link' => site_url('admin/projectmgmt/projects/approval'),
                            'class' => 'fa-briefcase',
                            'module' => 'ProjectMgmt',
                            'type' => 'info'
                        ));
                    }
                }
            }

            if ($_POST['status'] == "Reject") {
                $this->load->library('emailer/emailer');
                $ver = $this->db->query('SELECT parent_Rid,version_no,project_name,initiator,project_start_date,project_end_date FROM intg_projects WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
                $ver_i = $ver->row();
                if ($ver_i->parent_Rid == "0") {
                    $row_id = $_POST['docid'];
                } else {
                    $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                }
                $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);
                $this->emailer->Subject = 'Project ' . $ver_i->project_name . ' has been Reviewed';
                $this->emailer->Body = $this->load->view('_emails/project_status', array('item_type' => 'project', 'item_name' => $ver_i->project_name, 'assignee_name' => $rowinitiator->display_name, 'start_date' => $ver_i->project_start_date, 'end_date' => $ver_i->project_end_date, 'creator_name' => $creator->display_name, 'status' => $_POST['status']), TRUE);
                $this->emailer->mail();
                $this->load->model('notifications/notifications_model', null, true);
                $notification_data = array();
                $this->notifications_model->insert(array(
                    'user_id' => $rowinitiator->id,
                    'title' => 'Project ' . $ver_i->project_name . ' was rejected by ' . $current_user->display_name,
                    'content' => 'Project ' . $ver_i->project_name . ' was rejected by ' . $current_user->display_name,
                    'link' => site_url('admin/projectmgmt/projects/approval'),
                    'class' => 'fa-briefcase',
                    'module' => 'ProjectMgmt',
                    'type' => 'info'
                ));
            }
            ?>
            <!----------------Email code----->


        </div>
        <div id="inner2">&nbsp;</div>

        <?php
    } else {
// MILESTONE  approval START
        ?>
        <div>
            <div id="inner1"><?php
                $milestone_status_badge = '';
                $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());
                $queryapprover = $this->db->query('SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="42" and approval_status_mrowid = "' . $_POST['mrowid'] . '"  ORDER BY id asc')->result();

                $val = NULL;
                foreach ($queryapprover as $v) {
                    if ($val == 1) {
                        $nextapprover = $v->rolename;
                        $nextapprover_id = $v->approval_status_action_by;
                        break;
                    }

                    if ($v->rolename == $role_name) {
                        $val = 1;
                    }
                }
                $last = end($queryapprover);

                $queryinitiator = $this->db->query('SELECT role_id,email,display_name FROM intg_users  WHERE id = "' . $_POST['initiator'] . '"');
                $rowinitiator = $queryinitiator->row();


                switch ($_POST['status']) {
                    case "Yes" : $status = "Approved";
                        break;
                    case "No" : $status = "Pending";
                        break;
                    case "Reject" : $status = "Rejected";
                        break;
                }

                $data = array(
                    'approval_status_status' => $_POST['status'],
                    'approval_status_action_date' => date('Y-m-d h:i:s')
                );

                if ($_POST['status'] == "Reject") {
                    $this->db->where('approval_status_mrowid', $_POST['mrowid']);
                    $this->db->update('intg_approval_status', $data);

                    $i = $this->db->query('select * from intg_milestone_approval  WHERE id = "' . $_POST['docid'] . '" ')->row();
                    $this->db->query('UPDATE intg_milestones SET status = "' . $_POST['status'] . '" WHERE project_id = "' . $i->project_id . '"  and status="No" ');

                    if ($this->settings_lib->item('ext.milestone_deactivation_wf') == 'yes') {
                        //Alif, ACGT VO FOR ADD NEW MILESTONE or DEACTIVATE MILESTONE to HANDLE REJECT action...update all action to null, doesnt matter for activation or deactivation based on ms_approval_id
                        //if current action is deactivated and user want to activate but rejected by approver, action should be remain the same. 
                        $all_deactivate_activate_ms = $this->db->query('select * from intg_milestones WHERE ms_approval_id = "' . $_POST['docid'] . '" ')->result();

                        $action = '';
                        foreach ($all_deactivate_activate_ms as $ms) {
                            if ($ms->action == 'deactivated') {
                                $action = 'deactivated';
                            }

                            if ($ms->action == 'deactivate') {
                                $action = ''; //means keep active
                            }

                            if ($ms->action == 'activate') {
                                $action = 'deactivated';
                            }
                            $this->db->query('UPDATE intg_milestones SET action = "' . $action . '" WHERE milestone_id = "' . $ms->milestone_id . '" ');
                        }
                    }

                    $this->db->query('UPDATE intg_milestone_approval SET final_status = "Reject" WHERE id = "' . $_POST['docid'] . '"');
                } else {
                    $this->db->where('id', $_POST['rowid']);
                    $this->db->update('intg_approval_status', $data);
                }

                $data2 = array(
                    'approvers_status' => $_POST['status'],
                    'approvers_remarks' => $_POST['remarks'],
                    'approvers_approve_date' => date('Y-m-d h:i:s')
                );

                $this->db->where('approvers_appstatrowid', $_POST['rowid']);
                $this->db->where('approvers_approver', $this->auth->user_id());
                $this->db->update('intg_approvers', $data2);

                $final_approver_checks = $this->db->query('select * from intg_milestone_approval  WHERE id = "' . $_POST['docid'] . '" ')->row();

                if ($this->auth->user_id() == $_POST['finalapprover'] || $this->auth->user_id() == $final_approver_checks->final_approvers) {

                    $i = $this->db->query('select project_id from intg_milestone_approval  WHERE id = "' . $_POST['docid'] . '"')->row();
                    $versionMS = $this->db->query("select group_concat(milestone_id) as ids from intg_milestones where project_id = '" . $i->project_id . "' and status = 'No'")->row()->ids;

                    $this->load->model('timesheet_details/timesheet_details_model', null, true);
                    //commented as this query is throwing error when approving workflow for milestone version
                    //$this->timesheet_details_model->cso_timesheet_from_milestoneV($ver = $versionMS);

                    $this->db->query('UPDATE intg_milestones SET status = "' . $_POST['status'] . '" WHERE project_id = "' . $i->project_id . '"  and status="No" ');

                    if ($this->settings_lib->item('ext.milestone_deactivation_wf') == 'yes') {
                        //Alif, ACGT VO FOR ADD NEW MILESTONE or DEACTIVATE MILESTONE to HANDLE Approved/Yes action
                        $all_deactivate_activate_ms = $this->db->query('select * from intg_milestones WHERE ms_approval_id = "' . $_POST['docid'] . '" ')->result();

                        $action = '';
                        foreach ($all_deactivate_activate_ms as $ms) {
                            if ($ms->action == 'deactivate') {
                                $action = 'deactivated';
                                $statusText = 'deactivated';
                                $milestone_status_badge = "<span class='badge btn-danger m-l text-xs'>Deactivated</span>";
                            }

                            if ($ms->action == 'activate') {
                                $statusText = 'activated';
                            }
                            $this->db->query('UPDATE intg_milestones SET action = "' . $action . '" WHERE milestone_id = "' . $ms->milestone_id . '" ');
                        }
                    }

                    $this->db->query('UPDATE intg_milestone_approval SET final_status = "' . $_POST['status'] . '" WHERE id = "' . $_POST['docid'] . '" AND FIND_IN_SET ("' . $this->auth->user_id() . '",final_approvers)');

                    //After Final Approvers approved, check the milestone end date against project end date.If greater than project end date, update the project end date following the milestone end date.
                    $this->load->model('projects/projects_model', null, true);
                    $rc = $this->projects_model->find_by('id', $i->project_id);
                    $project_end_date = $rc->project_end_date;
                    
                    $miles_max_date = $this->db->query("SELECT MAX(end_date) as max_date FROM intg_milestones WHERE project_id = '" . $i->project_id . "' AND STATUS = 'yes' AND end_date > '" . $project_end_date. "'")->row()->max_date;
                    
                    $m_end_date = new DateTime($miles_max_date);
                    $p_end_date = new DateTime($project_end_date);

                    if ($m_end_date > $p_end_date) {

                        $data_p = array(
                            'project_end_date' => $miles_max_date
                        );
                        $this->projects_model->skip_validation(true); //set skip validation to true, Alif
                        $this->projects_model->update($i->project_id, $data_p);
                    } // Test Balik kenapa project end date tak updated
                     
                }
                

                $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="42" and bas.approval_status_mrowid = "' . $_POST['mrowid'] . '" ORDER BY bas.id asc')->result();

                if ($_POST['act'] != "edit") {

                    if ($_POST['status'] == "Reject") {
                        $btntype = "btn-danger";
                        $iconclass = "times";
                    }
                    if ($_POST['status'] == "Yes") {
                        $btntype = "btn-success";
                        $iconclass = "check";
                    }
                    ?>
                    <span id="animationSandbox<?= $_POST['mrowid'] ?>" style="display: block;" class="flipInX animated"> 
                        <a href="#" id="p<?= $_POST['mrowid'] ?>" data-toggle="popover" data-html="true" data-placement="left" data-trigger="hover"  data-original-title="Approvers Hierarchy" 
                           data-content="<div class='scrollable' style='height:auto;width:500px'>
                           <table width='100%' class='table table-condensed'>
                           <tr>   
                           <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                           <td><strong><?php echo lang('quote_role') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                           </tr><?php
                           foreach ($queryappr as $rowappr) {
                               switch ($rowappr->approvers_status) {
                                   case "No" : $status = "<span class='badge btn-warning m-l text-xs'>Pending</span>";
                                       break;
                                   case "Yes" : $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                                       break;
                                   case "Reject" : $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                                       break;
                                   case "Deactivated" : $status = "<span class='badge btn-danger m-l text-xs'>Deactivated</span>";
                                       break;
                               }
                               ?>

                               <tr>   
                               <td><?= $rowappr->display_name ?></td>
                               <td><?= $rowappr->role_name ?></td>
                               <td><?= $status ?></td>
                               <td><?= $rowappr->approvers_remarks ?></td>
                               <td><?php
                               if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                   echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                               }
                               ?></td>

                               </tr><?php } ?></table>" class="btn <?= $btntype ?>"><i class="fa fa-<?= $iconclass ?>"></i> </a></span>

                    <script>
                        $('#p' +<?= $_POST['mrowid'] ?>).popover('destroy');
                        $('#p' +<?= $_POST['mrowid'] ?>).hover(function () {
                            $('#p' +<?= $_POST['mrowid'] ?>).popover('show');
                        });

                    </script><?php } ?>

                <!----------------Email code----->
                <?php
                if ($_POST['status'] == "Yes") {


                    $fapp = $_POST['finalapprover'];
                    $f_app = explode(",", $fapp);
                    if ($this->auth->user_id() == $_POST['finalapprover'] || in_array($this->auth->user_id(), $f_app) || $this->auth->user_id() == $final_approver_checks->final_approvers) {
                        $this->load->library('emailer/emailer');
                        $ver = $this->db->query('SELECT parent_Rid,version_no,milestone_name,initiator,start_date,end_date FROM intg_milestones WHERE  milestone_id = "' . $_POST['docid'] . '"  ORDER BY milestone_id asc');
                        $ver_i = $ver->row();
                        if ($ver_i->parent_Rid == "0") {
                            $row_id = $_POST['docid'];
                        } else {
                            $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                        }

                        $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);
                        $this->emailer->Subject = 'Milestone ' . $ver_i->milestone_name . ' has been Reviewed';
                        $this->emailer->Body = $this->load->view('_emails/project_status', array('item_type' => 'milestone', 'item_name' => $ver_i->milestone_name, 'start_date' => $ver_i->start_date, 'end_date' => $ver_i->end_date, 'creator_name' => $creator->display_name, 'status' => $_POST['status']), TRUE);
                        $this->emailer->mail();

                        $this->load->model('notifications/notifications_model', null, true);
                        $notification_data = array();
                        $this->notifications_model->insert(array(
                            'user_id' => $rowinitiator->initiator,
                            'title' => 'Milestone ' . $ver_i->milestone_name . ' was ' . $statusText . ' by ' . $current_user->display_name,
                            'content' => 'Milestone ' . $ver_i->milestone_name . '  has been reviewed, ' . $statusText . ' by ' . $current_user->display_name,
                            'link' => site_url('admin/projectmgmt/projects/milestone_approval'),
                            'class' => 'fa-briefcase',
                            'module' => 'ProjectMgmt',
                            'type' => 'info'
                        ));
                    } else {
                        $approver = $nextapprover;
                        $user_iids = (explode(",", $nextapprover_id));
                        $size = sizeof($user_iids);
                        for ($i = 0; $i < $size; $i++) {
                            $apdet = $this->db->query("SELECT id,display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
                            $rowapp = $apdet->row();

                            // echo "SELECT id,display_name,email FROM intg_users WHERE id = '".$user_iids[$i]."'";
                            // echo 'SELECT parent_Rid,version_no,milestone_name,initiator,start_date,end_date FROM intg_milestones WHERE  milestone_id = "'.$_POST['docid'].'"  ORDER BY milestone_id asc';
                            //                                exit();
                            /* echo $nextapprover_id;
                              echo $this->db->last_query();
                              echo "approver:".$approver."</br>";
                              echo "email:".$rowapp->email."</br>";
                              exit; */

                            $this->load->library('emailer/emailer');
                            //        $ver = $this->db->query('SELECT parent_Rid,version_no,milestone_name,initiator,start_date,end_date FROM intg_milestones WHERE  milestone_id = "'.$_POST['docid'].'"  ORDER BY milestone_id asc');
                            $ver = $this->db->query('SELECT 
                        im.parent_Rid,
                        im.version_no,
                        im.milestone_name,
                        im.initiator,
                        im.start_date,
                        im.end_date 
                      FROM
                        intg_milestones im
                        JOIN intg_milestone_approval ima ON ima.id = im.`ms_approval_id` 
                      WHERE ima.id = "' . $_POST['docid'] . '"  ORDER BY im.milestone_id asc');

                            $ver_i = $ver->row();
                            $creator = $this->db->query("SELECT display_name FROM intg_users WHERE id = '" . $ver_i->initiator . "'")->row();
                            if ($ver_i->parent_Rid == "0") {
                                $row_id = $_POST['docid'];
                            } else {
                                $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                            }
                            $this->emailer->addAddress($rowapp->email, $rowapp->display_name);
                            $this->emailer->Subject = 'Milestone ' . $ver_i->milestone_name . ' is ready for approval';
                            $this->emailer->Body = $this->load->view('_emails/project_app', array('item_type' => 'milestone', 'item_name' => $ver_i->milestone_name, 'start_date' => $ver_i->start_date, 'end_date' => $ver_i->end_date, 'creator_name' => $creator->display_name), TRUE);

                            $this->emailer->mail();
                            $this->load->model('notifications/notifications_model', null, true);
                            $notification_data = array();
                            $this->notifications_model->insert(array(
                                'user_id' => $rowapp->id,
                                'content' => 'Milestone ' . $ver_i->milestone_name . ' is ready for approval',
                                'title' => 'Milestone ' . $ver_i->milestone_name . ' created by ' . get_from_any_table("intg_users", "display_name", "id", $ver_i->initiator) . ' is ready for approval',
                                'link' => site_url('admin/projectmgmt/projects/milestone_approval'),
                                'class' => 'fa-briefcase',
                                'module' => 'ProjectMgmt',
                                'type' => 'info'
                            ));
                        }
                    }
                }

                if ($_POST['status'] == "Reject") {
                    $this->load->library('emailer/emailer');
//$ver = $this->db->query('SELECT parent_Rid,version_no,milestone_name,initiator,start_date,end_date FROM intg_milestones WHERE  milestone_id = "'.$_POST['docid'].'"  ORDER BY milestone_id asc');
                    $ver = $this->db->query('SELECT 
                im.parent_Rid,
                im.version_no,
                im.milestone_name,
                im.initiator,
                im.start_date,
                im.end_date 
              FROM
                intg_milestones im
                JOIN intg_milestone_approval ima ON ima.id = im.`ms_approval_id` 
              WHERE ima.id = "' . $_POST['docid'] . '"  ORDER BY im.milestone_id asc');
                    $ver_i = $ver->row();
                    if ($ver_i->parent_Rid == "0") {
                        $row_id = $_POST['docid'];
                    } else {
                        $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                    }
                    $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);


                    $this->emailer->Subject = 'Milestone ' . $ver_i->milestone_name . ' has been Reviewed';
                    $this->emailer->Body = $this->load->view('_emails/project_status', array('item_type' => 'milestone', 'item_name' => $ver_i->milestone_name, 'start_date' => $ver_i->start_date, 'end_date' => $ver_i->end_date, 'creator_name' => $creator->display_name, 'status' => $_POST['status']), TRUE);

                    $this->emailer->mail();
                    $this->load->model('notifications/notifications_model', null, true);
                    $notification_data = array();
                    $this->notifications_model->insert(array(
                        'user_id' => $ver_i->initiator,
                        'title' => 'Milestone ' . $ver_i->milestone_name . ' was rejected by ' . $current_user->display_name,
                        'content' => 'Milestone ' . $ver_i->milestone_name . ' was rejected by ' . $current_user->display_name,
                        'link' => site_url('admin/projectmgmt/projects/milestone_approval'),
                        'class' => 'fa-briefcase',
                        'module' => 'ProjectMgmt',
                        'type' => 'info'
                    ));
                }
                ?>
                <!----------------Email code----->
            </div>
            <div id="inner2">&nbsp;</div>
            <div id="inner3"><?= $status ?><?= $milestone_status_badge ?></div>

        <?php } ?>