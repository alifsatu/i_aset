<style type="text/css">
    span.simplecolorpicker.inline {
        display: table-row !important;
    }
</style>
<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><? echo lang('projects_create_error')?></h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projects)) {
    $projects = (array) $projects;
    if ($projects['final_status'] == "Yes") {
        $PROJECT_APPROVED = true;
        $disabled = "disabled";

        echo '<div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-info-circle"></i> Only Project Name and Co-Worker list can be edited because Project ' . $projects['project_name'] . ' has been approved </div>';
    } else {
        $PROJECT_APPROVED = false;
        $disabled = "";
    }
}
//$id = isset($projects['id']) ? $projects['id'] : '';
?>
<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold"><? echo lang('projects')?></header>
            <div class="panel-body">


                <?php echo form_open_multipart($this->uri->uri_string(), 'id="forma"'); ?>

                <div class="form-group  clearfix">

                    <div class="col-sm-3">
                        <label><? echo lang('projects_name'); ?> *</label>
                        <div class='controls'>
                            <input class="input-sm input-s form-control" id='projects_project_name' type='text' name='projects_project_name' maxlength="45" value="<?php echo set_value('projects_project_name', isset($projects['project_name']) ? $projects['project_name'] : ''); ?>" data-required="true" />
                        </div>
                    </div>

                    <?php
// Change the values in this array to populate your dropdown as required

                    $prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
                    ?>
                    <div class="col-sm-3">
                        <label>SBU *</label>
                        <div class='controls'>
                            <?php if (!$PROJECT_APPROVED) { ?>

                                <select name="projects_sbu" id="projects_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" <?= $disabled ?> >
                                    <option></option>
                                    <?php
                                    foreach ($prgm as $pn) {
                                        if ($projects['sbu_id'] == $pn->id) {
                                            ?>
                                            <option selected="selected" value=<?= $pn->id ?>><b><?= $pn->sbu_name ?></b></option>
                                        <?php } else { ?>
                                            <option value=<?= $pn->id ?>><b><?= $pn->sbu_name ?></b></option><?php
                                        }
                                    }
                                    ?>
                                </select> 

                            <?php } else { ?> 

                                <input class="form-control input-sm input-s" type="text" name="projects_sbu_name" id="projects_sbu_name" value="<?= get_sbu_name($projects['sbu_id']) ?>" disabled>
                                <input type="hidden" name="projects_sbu" id="projects_sbu" value="<?= $projects['sbu_id'] ?>">

                            <?php } ?>
                        </div>
                    </div>
                    <?php
                    // Change the values in this array to populate your dropdown as required

                    $cc = $this->db->query('SELECT * FROM  intg_cost_centre WHERE deleted = 0 ORDER BY cost_centre ')->result();
                    ?>
                    <div class="col-sm-3">
                        <label>Cost Centre *</label>
                        <div class='controls'>
                            <?php if (!$PROJECT_APPROVED) { ?> 
                                <select name="projects_cost_centre" id="projects_cost_centre" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" <?= $disabled ?> >
                                    <option></option>
                                    <? foreach($cc as $c) { if ( $projects['cost_centre_id']==$c->id ) {?>
                                    <option selected value=<?= $c->id ?>><b><?= $c->cost_centre ?></b></option><? } else { ?>
                                    <option  value=<?= $c->id ?>><b><?= $c->cost_centre ?></b></option><? } } ?>
                                </select>

                            <?php } else { ?> 

                                <input class="form-control input-sm input-s" type="text" name="projects_cost_centre_name" id="projects_cost_centre_name" value="<?= get_cc_name($projects['cost_centre_id']) ?>" disabled>
                                <input type="hidden" name="projects_cost_centre" id="projects_cost_centre" value="<?= $projects['cost_centre_id'] ?>">

                            <?php } ?>

                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required

                    $prgm = $this->db->query('SELECT * FROM  intg_programs WHERE deleted = 0 ORDER BY program_name ')->result();
                    ?>
                    <div class="col-sm-3">
                        <label>Programme *</label>
                        <div class='controls'>

                            <?php if (!$PROJECT_APPROVED) { ?> 
                                <select name="projects_Programme" id="projects_Programme" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" <?= $disabled ?> >
                                    <option></option>

                                    <? foreach($prgm as $p) { if ( $projects['program_id']==$p->id ) {?>


                                    <option selected value="<?= $p->id ?>"><b><?= $p->program_name ?></b></option><? }  else { ?>
                                    <option value=<?= $p->id ?>><b><?= $p->program_name ?></b></option><? } } ?>
                                </select>

                            <?php } else { ?> 

                                <input class="form-control input-sm input-s" type="text" name="projects_Programme_name" id="projects_Programme_name" value="<?= get_programme_name($projects['program_id']) ?>" disabled>
                                <input type="hidden" name="projects_Programme" id="projects_Programme" value="<?= $projects['program_id'] ?>">

                            <?php } ?>

                        </div> 
                    </div>
                </div>

                <div class="form-group  clearfix">


                    <div class="col-sm-3">
                        <label>Justification of Project *</label>
                        <div class='controls'>
                            <?php
                            if (!$PROJECT_APPROVED) {
                                echo form_textarea(array('class' => 'form-control input-sm input-s expand', 'name' => 'projects_justification', 'data-required=' => 'true', 'id' => 'projects_justification', 'rows' => '1', 'cols' => '40', 'value' => $projects['justification']));
                            } else {
                                ?>

                                <textarea class="form-control input-sm input-s expand" name="projects_justification" id="projects_justification" rows="4" cols="25" contenteditable="false" readonly="readonly" ><?= $projects['justification'] ?></textarea>

                            <?php } ?>

                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label>Co-Worker *</label>
                        <div class='controls'>
                            <select multiple name="projects_assigned_to[]" id="projects_assigned_to" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" >
                                <?php
                                $user_level_list = get_user_level_list();
                                foreach ($users as $user):
                                    if (in_array($user->id, explode(",", $projects['coworker']))) {
                                    ?>
                                    <option selected="selected" value="<?= $user->id ?>"><?= $user->display_name; ?> (<?php echo $user->user_level_id != '' ? $user_level_list[$user->user_level_id] : 'N/A'; ?>)</option><?php } else { ?>
                                    <option value="<?= $user->id ?>"><?= $user->display_name; ?> (<?php echo $user->user_level_id != '' ? $user_level_list[$user->user_level_id] : 'N/A'; ?>)</option><?php } ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Duration in Days *</label>
                        <div class='controls'>

                            <?php if (!$PROJECT_APPROVED) { ?> 
                                <input   class="input-sm input-s form-control" id='projects_duration_days' type='text' name='projects_duration_days'  value="<?php echo $projects['total_days']; ?>"  data-required="true" <?= $disabled ?> />
                            <?php } else { ?> 

                                <input class="form-control input-sm input-s" type="text" name="projects_duration_days_name" id="projects_duration_days_name" value="<?= $projects['total_days'] ?>" disabled>
                                <input type="hidden" name="projects_duration_days" id="projects_duration_days" value="<?= $projects['total_days'] ?>">

                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <label>Project Color * </label>
                        <div class='controls'>
                            <?php if ($PROJECT_APPROVED) { ?>
                                <div style="background-color: <?= $projects['projects_color'] ?>;  width:80px; text-align:center">&nbsp;</div>
                                <input type="hidden" name="projects_color" id="projects_color" value="<?= $projects['projects_color'] ?>">
                            <?php } else { ?>
                                <select name="projects_color" id="projects_color" class="color" style="display:table-row">
                                               <!--  <select  name="projects_color" id="projects_color" class="form-control color selecta"  style="height:30px; font-size:12px; width:200px;display:table-row"  data-required="true" >-->

                                    <option value="#7bd148">Green</option>
                                    <option value="#5484ed">Bold blue</option>
                                    <option value="#a4bdfc">Blue</option>
                                    <option value="#46d6db">Turquoise</option>
                                    <option value="#7ae7bf">Light green</option>
                                    <option value="#51b749">Bold green</option>
                                    <option value="#fbd75b">Yellow</option>
                                    <option value="#ffb878">Orange</option>
                                    <option value="#ff887c">Red</option>
                                    <option value="#dc2127">Bold red</option>
                                    <option value="#dbadff">Purple</option>
                                    <option value="#e1e1e1" selected>Gray</option>
                                </select>
                            <?php } ?>
                        </div>

                    </div>

                </div>

                <div class="form-group  clearfix">


                    <div class="col-sm-3">
                        <label>Start Date *</label>
                        <div class='controls'>
                            <input    class="datepicker-input form-control input-sm input-s"  id='projects_project_start_date'  data-date-format="dd-mm-yyyy" type='text' name='projects_project_start_date'  value="<?php echo date("d-m-Y", strtotime($projects['project_start_date'])); ?>"  data-required="true" <?= $disabled ?> />	
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <label>End Date *</label>
                        <div class='controls'>
                            <input  class="datepicker-input form-control input-sm input-s"  id='projects_project_end_date'  data-date-format="dd-mm-yyyy" type='text' name='projects_project_end_date'  value="<?php echo date("d-m-Y", strtotime($projects['project_end_date'])); ?>"  data-required="true" <?= $disabled ?> />

                        </div>
                    </div>



                </div>
                <div class="form-group  clearfix">

                    <div class="col-sm-10">
                        <label>Introduction</label>
                        <div class='controls'>  
                            <?php if (!$PROJECT_APPROVED) { ?>
                                <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">


                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                    </div>
                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                        <div class="dropdown-menu">
                                            <div class="input-group m-l-xs m-r-xs">
                                                <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink"/>
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" type="button">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn btn-default btn-sm" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                    </div>


                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                        <a class="btn btn-default btn-sm" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                    </div>

                                </div>
                            <?php } ?>

                            <?php if (!$PROJECT_APPROVED) { ?>
                                <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px; font-size:12px" <?php echo $PROJECT_APPROVED ? 'data-toggle="tooltip" title="Editing Has Been Disabled Because Project Already Approved"' : '' ?> >
                                    <?= $projects['introduction'] ?>
                                </div>
                            <?php } else { ?>
                                <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px; font-size:12px" <?php echo $PROJECT_APPROVED ? 'data-toggle="tooltip" title="Editing Has Been Disabled Because Project Already Approved"' : '' ?> >
                                    <?= $projects['introduction'] ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php if (!$PROJECT_APPROVED) { ?>        
                    <div class="form-group  clearfix">
                        <div class="form-actions col-sm-5"> 
                            <div class='controls' style="margin:5px 5px"><input type="file" name="files[]" multiple="multiple" />
                            </div>
                        </div>
                    </div>
                <?php } ?>


                <div class="form-group  clearfix">
                    <div class="form-actions col-sm-5">
                        <input type="hidden" name="created_by" id="created_by" />
                        <input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                        <input type="hidden" value=true name="project_status" value="<?= $projects['status'] ?>"/>
                        <!--<input type="hidden" name="introduction" id="intro" value="<?= $projects['introduction'] ?>" />-->
                        <textarea name="introduction" id="intro" style="display:none;"></textarea>

                        <?php if ($projects['final_status'] == "Yes") { ?>
                            <input type="submit" id="submit2"  name="save" class="btn btn-primary" value="Save & Exit" onclick="return confirm('Confirm Save & Exit')"  />
                        <?php } else { ?>
                            <input type="submit" id="submit" name="draft" class="btn btn-warning" value="Save as Draft" onclick="return confirm('Save as Draft')"  />
                            &nbsp;&nbsp;
                            <input type="submit" id="submit2"  name="save" class="btn btn-primary" value="Save & Next" onclick="return confirm('Confirm Save & Next')"  />
                        <?php } ?>

                        <br><br>


                    </div>
                </div>

                <?php echo form_close(); ?>


            </div>
        </section>
    </div><!--end of col6-->


</div> <!--end of row-->
<?php Assets::add_js(Template::theme_url('js/datetimepicker/moment.min.js')) ?>
<?php Assets::add_js(Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.js')) ?>
<script src="<?= Template::theme_url('js/wysiwyg/jquery.hotkeys.js') ?>"></script>
<script src="<?= Template::theme_url('js/wysiwyg/bootstrap-wysiwyg.js') ?>"></script>
<script src="<?= Template::theme_url('js/wysiwyg/demo.js') ?>"></script>

<script type="text/javascript">
                            $("#submit,#submit2").click(function (e) {
                                $("#intro").val($("#editor").cleanHtml());


                            });
                            $(function () {
                                $('.color').simplecolorpicker('selectColor', '<?= $projects['projects_color'] ?>');



                                $('[name=projects_status][value=<?= $projects['status'] ?>]').prop('checked', true).closest('label').addClass('active');
<?php
if ($projects['project_tag']) {
    $tag_bg_class = array('dark', 'success', 'warning', 'danger', 'info');
    $tags = unserialize($projects['project_tag']);
    $tags_htmlstring = '';
    if ($tags)
        foreach ($tags as $i => $tag) {
            $tags_htmlstring .= '<li class="label bg-' . $tag_bg_class[$i % 5] . '"><input type="hidden" name="projects_project_tag[]" value="' . $tag . '">' . $tag . '</li>';
        }
    ?>
                                    $('#projects_project_tag.pillbox ul').append('<?= $tags_htmlstring ?>');
    <?php
}
?>

                                // add pillbox 
                                var addPill = function ($input) {
                                    var $text = $input.val(), $pills = $input.closest('.pillbox'), $repeat = false, $repeatPill, pillBgClass = ['dark', 'success', 'warning', 'danger', 'info'];
                                    if ($text == "")
                                        return;
                                    $("li", $pills).text(function (i, v) {
                                        if (v == $text) {
                                            $repeatPill = $(this);
                                            $repeat = true;
                                        }
                                    });
                                    if ($repeat) {
                                        $repeatPill.fadeOut().fadeIn();
                                        return;
                                    }
                                    ;
                                    $item = $('<li class="label bg-' + pillBgClass[$pills.pillbox('items').length % 5] + '"><input type="hidden" name="projects_project_tag[]" value="' + $text + '">' + $text + '</li> ');
                                    $item.insertBefore($input);
                                    $input.val('');
                                    $pills.trigger('change', $item);
                                };
                                // end pillbox
                                // Project Tag
                                $('#projects_project_tag input').on('blur', function () {
                                    addPill($(this));
                                });
                                $('#projects_project_tag input').on('keypress', function (e) {
                                    if (e.which == 13) {
                                        e.preventDefault();
                                        addPill($(this));
                                    }
                                });
                            });


                            function updateData(fieldname) {
                                var value = $("#" + fieldname).val();
//  alert("Value : "+value+", Fieldname : "+fieldname+", Project Id: "+<?php echo $this->uri->segment(5); ?>);

                                if (fieldname == "projects_assigned_to") {
                                    fieldname = "coworker";
                                }
                                if (fieldname == "projects_project_name") {
                                    fieldname = "project_name";
                                }


                                $.get('<?= base_url('index.php/admin/projectmgmt/projects/update_data_ajax') ?>', {
                                    field: fieldname,
                                    value: value,
                                    projectid: <?php echo $this->uri->segment(5); ?>
                                }, function (res) {

                                    alert(res);
                                    console.log(res);

                                });

                            }


                            $("#forma").submit(function (event) {

                                $('input[disabled]').removeAttr("disabled");
                            });

                            $(document).ready(function () {
<?php if ($PROJECT_APPROVED) { ?>
                                    $('div[contenteditable="true"]').attr('contenteditable', false);
<?php } ?>

                            });

</script>