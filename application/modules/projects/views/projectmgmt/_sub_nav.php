<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects') ?>" id="list"><?php echo lang('projects_list'); ?></a>
	</li>
	
        <?php if(has_permission("Projects.Projectmgmt.Create")){ ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects/createnew') ?>" id="create_new"><?php echo lang('projects_new'); ?></a>
	</li>
        <?php } ?>
   
<!--     <li <?php echo $this->uri->segment(4) == 'fullcalendar' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects/fullcalendar') ?>" id="list"><?php echo lang('projects_calendar'); ?></a>
	</li>-->
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Approvals<b class="caret"></b></a>
    <ul class="dropdown-menu text-left">
	<li <?php echo $this->uri->segment(4) == 'approval' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects/approval') ?>" id="list">Projects</a></li>   
    
    <li <?php echo $this->uri->segment(4) == 'milestone_approval' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects/milestone_approval') ?>"   id="list">Milestone</a></li>
    </ul></li>
    
     <li <?php echo $this->uri->segment(4) == 'draft' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/projects/draft') ?>" id="list">Draft</a>
	</li>
    
   
   
</ul>