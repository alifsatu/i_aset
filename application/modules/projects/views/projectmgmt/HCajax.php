

	
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'TASK PROGRESS<?=$_POST['dateto']?>'
        },
        subtitle: {
           /* text: 'Irregular time data in Highcharts JS'*/
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              
			    month: "%d/%m",
               
            },
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'PERCENTAGE (%)'
            },
			
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:1f} %'
        },

        series: [{
            name: 'Expected',
			 lineWidth: 2,
			 color:"#f00",
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [<? echo implode(",",$expected);?>
               
            ]
        }, {
            name: 'Achieved',
            data: [<? echo implode(",",$achieved);?>
            ]
        }]
    });
});


function gd(year, month, day) {
    return new Date(year, month -1, day + 1).getTime();
}

function loadgraph()
{
 
$('#container').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/projects/HCgraph') ?>", {dateto:$('#dateto').val()}, function(data){$('#container').html(data); } )

	
}

		</script>


 
<div class="row">
                 <section class="panel panel-default"><header class="panel-heading font-bold clearfix"> <div class="col-md-5">PROJECT PROGRESS</div>
             <div class="input-group  col-md-4">
                          <span class="input-group-addon btn-success">Range From</span>
                          <input type="text" class="input-sm input-xs datepicker-input form-control" id="datefrom" placeholder="date">
                        </div>
                        <div class="input-group  col-md-3">
                          <span class="input-group-addon btn-success">To</span>
                          <input type="text" class="input-sm input-xs datepicker-input form-control" id="dateto" placeholder="date"  onBlur="loadgraph()">
                        </div>
                        </header>
                    <div class="panel-body"> 
<div id="container" style=" height: 500px; margin:0 auto"></div>
</div></section></div>
<script>
$(document).ready(function() {
 $('#dateto').datepicker().on('changeDate', function(){
          $('.datepicker').hide();
		});
 });
</script>