<div class="row">
	<h3>Projects <div style="float:right">
	<?php 


echo form_open($this->uri->uri_string(),'class="form-search"'); ?>
	<label style="color:#fff">Search by fields</label>
<select name="select_field" id="select_field" onchange="setfname()">
<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>

<option>All Fields</option>		
<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_projects')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>



<input type="text" name="field_value" class="search-query input-xlarge" value="<?=$this->session->userdata('capfvalue')?>">
<button class="btn" type="submit" name="submit" value="Search" title="Search" ><i class="icon-search icon-black"></i></button>
<button type="submit" name="submit" class="btn" value="Reset" title="Reset"><i class="icon-refresh icon-black"></i></button>




	</h3>
	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Projects.Content.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_name') ? 'desc' : 'asc'); ?>'>
                    Project Name</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_description') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_client_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_client_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_client_id') ? 'desc' : 'asc'); ?>'>
                    Client Id</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_status') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_status&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_status') ? 'desc' : 'asc'); ?>'>
                    Status</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_visibility') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_visibility&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_visibility') ? 'desc' : 'asc'); ?>'>
                    Project Visibility</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_allow_comments') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_allow_comments&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_allow_comments') ? 'desc' : 'asc'); ?>'>
                    Allow Comments</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_send_notifications') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_send_notifications&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_send_notifications') ? 'desc' : 'asc'); ?>'>
                    Send Notifications</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_estimated_hours') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_estimated_hours&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_estimated_hours') ? 'desc' : 'asc'); ?>'>
                    Estimated Hours</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_notes') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_notes&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_notes') ? 'desc' : 'asc'); ?>'>
                    Project Notes</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_start_date') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_start_date&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_start_date') ? 'desc' : 'asc'); ?>'>
                    Project Start Date</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_end_date') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_end_date&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_end_date') ? 'desc' : 'asc'); ?>'>
                    Project End Date</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_cost') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_cost&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_cost') ? 'desc' : 'asc'); ?>'>
                    Project Cost</a></th>
					<th <?php if ($this->input->get('sort_by') == 'projects'.'_project_tag') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/content/projects?sort_by=projects_project_tag&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projects'.'_project_tag') ? 'desc' : 'asc'); ?>'>
                    Project Tag</a></th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Projects.Content.Delete')) : ?>
				<tr>
					<td colspan="14">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Projects.Content.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Projects.Content.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/content/projects/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->project_name) ?></td>
				<?php else: ?>
				<td><?php echo $record->project_name ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->description?></td>
				<td><?php echo $record->client_id?></td>
				<td><?php echo $record->status?></td>
				<td><?php echo $record->project_visibility?></td>
				<td><?php echo $record->allow_comments?></td>
				<td><?php echo $record->send_notifications?></td>
				<td><?php echo $record->estimated_hours?></td>
				<td><?php echo $record->project_notes?></td>
				<td><?php echo $record->project_start_date?></td>
				<td><?php echo $record->project_end_date?></td>
				<td><?php echo $record->project_cost?></td>
				<td><?php echo $record->project_tag?></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="14">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
          <?php  echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
</div>