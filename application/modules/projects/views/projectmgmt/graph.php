<?php
	$project = $project[0];
?>

		
                
		<div class="row">
        
			 <div class="col-md-8">
            <header class=" panel panel-heading font-bold clearfix" style=" background-color:rgb(245,245,245); border:rgb(232,232,232) solid 1px">           
			<div class="col-md-4">
			PROJECT NAME: <?php e($project->project_name);?>
			</div>
			<div class="col-md-4">
			START DATE: <?php e(date('d/m/Y H:i', strtotime($project->project_start_date)));?>
			</div>
			
            </header>
           
			<div class="form-group clearfix">
             <div class="input-group  col-md-3">
			 <span class="input-group-addon btn-info">Task</span>
			<select id="taska" class="form-control input-sm  selecta pull-left" style="max-width:200px;">
				<option value=0>Overall Progress</option>
			<?php if($tasks):?>
				<?php foreach($tasks as $task):?>
				<option value="<?php e($task->id);?>"><?php e($task->task_name);?></option>
				<?php endforeach;?>
			<?php endif;?>
			</select>
			</div>
              <div class="input-group  col-md-3">
            
                          <span class="input-group-addon btn-info">Range From</span>
                          <input type="text" class="input-sm  datepicker-input form-control" id="datefrom" placeholder="date">
                        </div>
                        <div class="input-group  col-md-3">
                          <span class="input-group-addon btn-info">To</span>
                          <input type="text" class="input-sm input-xs datepicker-input form-control" id="dateto" placeholder="date">
                        </div>
                        </div>
                        
                        
                        
                        
                        <div class="panel wrapper panel-success col-md-6">
                          <div class="row">
                          
                            <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e($project->total_ongoing_tasks)?></span>
                                <small class="text-muted">Active Tasks</small>
                              </a>
                            </div>
                            <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e($project->total_overdue_tasks)?></span>
                                <small class="text-muted">Overdue Tasks </small>
                              </a>
                            </div>
                            <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e($project->total_completed_tasks)?></span>
                                <small class="text-muted">Completed Tasks</small>
                              </a>
                            </div>
                           </div> 
                         
                           </div>
                         
                               <div class="panel wrapper panel-success col-md-6">
                          <div class="row">
                           
                            <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e($project->overall_progress * 100);?> %</span>
                                <small class="text-muted">% Complete</small>
                              </a>
                            </div>
                            <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e($project->tasks ? count($project->tasks) : 0);?></span>
                                <small class="text-muted">Total Tasks</small>
                              </a>
                            </div>
                              <div class="col-xs-4">
                              <a href="#">
                                <span class="m-b-xs h4 block"><?php e(date('d/m/Y', strtotime($project->project_end_date)));?></span>
                                <small class="text-muted">Finish Date </small>
                              </a>
                            </div>
                            </div> 
                          </div>
                      
   		
				<div id="container"></div>
			</div>
			
			<div class="col-md-4">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">ISSUES</header>
                    
                    <div class="panel-body">
                       
                     <section class="panel panel-default  col-lg-4" style="padding:0">
       <div class="text-center wrapper bg-light lt"><strong>Open</strong> </div>
      <div class="panel-body text-center">
        <div class="sparkline inline" data-type="pie" data-height="75" data-slice-colors="['']" align="center">52,23,45</div>
        
            
        
      </div>
      <ul class="list-group no-radius">
        <li class="list-group-item">  <span class="label bg-primary">Normal</span>:2 (67%)</li>
         <li class="list-group-item">  <span class="label bg-dark">Normal</span>:2 (67%)</li>
      </ul>
    </section>
    <section class="panel panel-default  col-lg-4"  style="padding:0">
       <div class="text-center wrapper bg-light lt"><strong>In Progress</strong> </div>
      <div class="panel-body text-center">
        <div class="sparkline inline" data-type="pie" data-height="75" data-slice-colors="['#17c587','#11586e','#f2f2f2']" align="center">12,23,45</div>
        
            
        
      </div>
      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-primary">1</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-dark">2</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-light">3</span>&nbsp;group idea1</li>
      </ul>
    </section>
    <section class="panel panel-default  col-lg-4"  style="padding:0">
       <div class="text-center wrapper bg-light lt"><strong>Closed</strong> </div>
      <div class="panel-body text-center">
               <div class="sparkline inline" data-type="pie" data-height="75" data-slice-colors="['#77c587','#41586e','#f2f2f2']" align="center">142,23,45</div>

        
            
        
      </div>
      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-primary">1</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-dark">2</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right"><i class="fa fa-thumbs-up text-success text"></i>&nbsp;1</span> <span class="label bg-light">3</span>&nbsp;group idea1</li>
      </ul>
    </section>
                  
                    
                      
                    </div>
                  </section>
                  
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">MILESTONES ( TASKS )</header>
                    
                    <div class="panel-body">
                     
                     <section class="panel panel-default" id="progressbar">
                    <header class="panel-heading">
                      <ul class="nav nav-pills pull-right">
                        <li><a href="#" data-toggle="progress" data-target="#progressbar"></a></li>
                      </ul>
                     Task 1 <br />Due Date:
                    </header>
                    <ul class="list-group">
                      <li class="list-group-item">
                        
                       
                       
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar progress-bar-danger" data-toggle="tooltip" data-original-title="30%" style="width: 30%"></div>
                        </div>
                      </li>
                      
                      
                    </ul>
                    
                     <header class="panel-heading">
                      <ul class="nav nav-pills pull-right">
                        <li><a href="#" data-toggle="progress" data-target="#progressbar">5 Days</a></li>
                      </ul>
                     Task 2 <br />Due Date:
                    </header>
                    <ul class="list-group">
                      <li class="list-group-item">
                        
                       
                       
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar progress-bar-success" data-toggle="tooltip" data-original-title="90%" style="width: 30%"></div>
                        </div>
                      </li>
                      
                      
                    </ul>
                    
                  </section>
                     
                    </div>
                  </section>
                  
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">IN PROGRESS ( ISSUES )</header>
                   
                    <div class="panel-body">
                      <section class="panel panel-default" id="progressbar">
                    <header class="panel-heading">
                      <ul class="nav nav-pills pull-right">
                        <li><a href="#" data-toggle="progress" data-target="#progressbar"></a></li>
                      </ul>
                     Task 1 <br />Due Date:
                    </header>
                    <ul class="list-group">
                      <li class="list-group-item">
                        
                       
                       
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar progress-bar-danger" data-toggle="tooltip" data-original-title="30%" style="width: 30%"></div>
                        </div>
                      </li>
                      
                      
                    </ul>
                    
                      <ul class="list-group no-radius">
        <li class="list-group-item"> <span class="pull-right badge btn-success">High</span> <span class="label bg-primary">1</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right badge btn-success">High</span> <span class="label bg-dark">2</span>&nbsp;erg</li>
        <li class="list-group-item"> <span class="pull-right badge btn-info">Normal</span> <span class="label bg-light">3</span>&nbsp;group idea1</li>
         <li class="list-group-item"> <span class="pull-right badge btn-warning">Low</span> <span class="label bg-light">4</span>&nbsp;group idea1</li>
      </ul>
                    
                  </section>
                    
                      </div>
                      </section>
                    </div>
                 
                </div>
		</div>
		
		
		
	</section>

<script type="text/javascript">
	
	var expected = <?php echo json_encode($expected);?>, achieved = <?php echo json_encode($achieved);?>, tasks = <?php echo json_encode($tasks);?>, tasks_id = $.map(tasks, function(task){return task.id;}), 
		$taska = $('#taska'), $dateto = $('#dateto'), $datefrom = $('#datefrom'),
		get_hc_data = function(datas){
			return $.map(
					$.grep(datas, function(data){
						return data.task_id == $taska.val() 
							&& ($dateto.val().trim() == '' || ($dateto.val().trim() != '' && moment($dateto.val(),'DD-MM-YYYY').format('X')*1000 > new Date(data.time_stamp).getTime())) 
							&& ($datefrom.val().trim() == '' || ($datefrom.val().trim() != '' && moment($datefrom.val(),'DD-MM-YYYY').format('X')*1000 < new Date(data.time_stamp).getTime()));
					}), function(data){
						return [[new Date(data.time_stamp).getTime(), parseInt(data.progress)]];
				}).sort();
			};

	var expected_dates = [], achieved_dates = [], all_dates = [];
	// get available dates for X
	$.each(expected, function(i, data) {
		var date = moment(data.time_stamp).format('YYYY-MM-DD');
		if($.inArray(date, expected_dates) < 0 && $.inArray(data.task_id, tasks_id) >= 0)
			expected_dates.push(date);
	});
	$.each(achieved, function(i, data) {
		var date = moment(data.time_stamp).format('YYYY-MM-DD');
		if($.inArray(date, achieved_dates) < 0)
			achieved_dates.push(date);
	});
	all_dates = expected_dates.concat(achieved_dates);
	all_dates = all_dates.filter(function (item, pos) {return all_dates.indexOf(item) == pos}).sort();
	// get values for Y
	expected_overall_progress = [], achieved_overall_progress = [];
	$.each(all_dates, function(i_date, date) {	
		expected_all_progress = 0, achieved_all_progress = 0;
		$.each(tasks, function(i, task) {

			expected_progress = 0;
			$.each(expected, function(i, data) {
				expected_date = moment(data.time_stamp).format('YYYY-MM-DD');
				if(expected_date == date && data.task_id == task.id) {
					expected_progress = data.progress;
					return false;
				}
			});
			expected_all_progress += parseInt(expected_progress) * task.weightage_percentage / 100;

			achieved_progress = 0;
			$.each(achieved, function(i, data) {
				achieved_date = moment(data.time_stamp).format('YYYY-MM-DD');
				if(achieved_date == date && data.task_id == task.id) {
					achieved_progress = data.progress;
					return false;
				}
			});
			achieved_all_progress += parseInt(achieved_progress) * task.weightage_percentage / 100;

		});
		expected_overall_progress.push({
			task_id: 0,
			time_stamp: date, 
			progress: expected_all_progress > 0 ?  expected_all_progress : (typeof expected_overall_progress[i_date-1] != 'undefined' ? expected_overall_progress[i_date-1].progress : 0)
		});
		achieved_overall_progress.push({
			task_id: 0,
			time_stamp: date, 
			progress: achieved_all_progress > 0 ?  achieved_all_progress : (typeof achieved_overall_progress[i_date-1] != 'undefined' ? achieved_overall_progress[i_date-1].progress : 0)
		});
	});
	
	$(document).ready(function() {
	
		$highchart = $('#container').highcharts({
			chart: {
				type: 'line'
			},
			title: {
				text: 'TASK PROGRESS'
			},
			subtitle: {
				/* text: 'Irregular time data in Highcharts JS'*/
			},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: { // don't display the dummy year
					month: "%d/%m",
               
				},
				title: {
					text: 'Date'
				}
			},
			yAxis: {
				title: {
					text: 'PERCENTAGE (%)',
				},	
				min:0,
				max: 100
			},
			tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.x:%e %b}: {point.y:1f} %'
			},
			series: [{
				name: 'Expected',
				lineWidth: 2,
				color:"#f00",
				// Define the data points. All series have a dummy year
				// of 1970/71 in order to be compared on the same x axis. Note
				// that in JavaScript, months start at 0 for January, 1 for February etc.
				data: []
			}, {
				name: 'Achieved',
				data: []
			}]
		}).highcharts();
		
		var draw_graph = function(){
			$highchart.series[0].update({
				data: get_hc_data($taska.val() > 0 ? expected : expected_overall_progress)
			});
			$highchart.series[1].update({
				data: get_hc_data($taska.val() > 0 ? achieved : achieved_overall_progress)
			});
		};
		
		$taska.on('change',function(e){
			draw_graph();
		}).trigger('change');
		
		$('#dateto, #datefrom').on('changeDate', function(){
			$('.datepicker').hide();
			draw_graph();
		});
	});	
</script>
<script type="text/javascript">$(function() {$('.sparkline').sparkline('html', {type: 'pie', disableHiddenCheck: true, height: '80px', width: '100px',
 sliceColors: ['#17c587','#11586e','#f2f2f2']});
});</script>
