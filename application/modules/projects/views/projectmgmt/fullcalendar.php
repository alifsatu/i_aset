<link rel="stylesheet" href="<?php echo Template::theme_url('js/fuelux/fuelux.css'); ?>" type="text/css" />
 <link rel="stylesheet" href="<?php echo Template::theme_url('js/fullcalendar/fullcalendar.css'); ?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo Template::theme_url('js/fullcalendar/theme.css'); ?>" type="text/css" />
   <link rel="stylesheet" href="<?php echo Template::theme_url('css/app.css'); ?>" type="text/css" />
  
  
     <section>
       
          <section class="hbox stretch">          
            <!-- .aside -->
            <aside>
              <section class="vbox">
                <section class="scrollable wrapper">
                  <section class="panel panel-default">
                    <header class="panel-heading bg-light">
                     <? echo lang('projects_fullcalender')?>
                    </header>
                    <div class="calendar" id="calendar">

                    </div>
                  </section>
                </section>
              </section>
            </aside>
            <!-- /.aside -->
            <!-- .aside -->
            <aside class="aside-lg b-l">
              <div class="padder">
                <h5><? echo lang('projects_default')?> <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Type project name & hit Enter, then drag & drop to calendar."></i></h5>
                <div class="line"></div>
                <div id="myEvents" class="pillbox clearfix m-b no-border no-padder myEvents" data-type="project">
                  <ul><!--
                    <li class="label bg-dark">Item One</li>
                    <li class="label bg-success">Item Two</li>
                    <li class="label bg-warning">Item Three</li>
                    <li class="label bg-danger">Item Four</li>
                    <li class="label bg-info">Item Five</li>
                    <li class="label bg-primary">Item Six</li>-->
                    <input type="text" placeholder="type name and hit Enter">
                  </ul>
                </div>
                <div class="line"></div>
                <h5><? echo lang('projects_task_default')?> <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Type task name & hit Enter, then drag & drop to calendar."></i></h5>
                <div class="line"></div>
                <div id="myEvents" class="pillbox clearfix m-b no-border no-padder myEvents" data-type="task">
                  <ul><!--
                    <li class="label bg-dark">Item One</li>
                    <li class="label bg-success">Item Two</li>
                    <li class="label bg-warning">Item Three</li>
                    <li class="label bg-danger">Item Four</li>
                    <li class="label bg-info">Item Five</li>
                    <li class="label bg-primary">Item Six</li>-->
                    <input type="text" placeholder="type name and hit Enter">
                  </ul>
                </div>
                 <div class="line"></div>
				<!--
                <div class="checkbox">
                  <label class="checkbox-custom"><input type='checkbox' id='drop-remove' /><i class="fa fa-square-o"></i> remove after drop</label>
                </div>
                 <div class="line"></div>
                 <p><a href="ganttview2" target="blank" class="btn btn-sm btn-default"><? echo lang('projects_gantt_view')?></a></p>
				 -->
              </div>
            </aside>
            <!-- /.aside -->
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
     
      
      </section>
   
 
  <script src="<?=Template::theme_url('js/jquery.min.js')?>"></script>
<script src="<?php echo Template::theme_url('js/jquery-ui.js'); ?>"></script>
   <script src="<?php echo Template::theme_url('js/bootstrap.js'); ?>"></script>
   <script src="<?php echo Template::theme_url('js/app.js'); ?>"></script>
       <script src="<?php echo Template::theme_url('js/app.plugin.js'); ?>"></script>
          <script src="<?php echo Template::theme_url('js/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
  <!-- fuelux -->
<script src="<?=Template::theme_url('js/fuelux/fuelux.js')?>" cache="false"></script>
<!-- fullcalendar -->
<script src="<?=Template::theme_url('js/jquery.ui.touch-punch.min.js')?>" cache="false"></script>
<script src="<?=Template::theme_url('js/jquery-ui-1.10.3.custom.min.js')?>" cache="false"></script>
<script src="<?=Template::theme_url('js/fullcalendar/fullcalendar.min.js')?>" cache="false"></script>
<script src="<?=Template::theme_url('js/datetimepicker/moment.min.js')?>" cache="false"></script>
<script src="<?=Template::theme_url('js/datetimepicker/bootstrap-datetimepicker.min.js')?>" cache="false"></script>
<script>
var projname,projusers,groupid,projid;
	var url = function(page,params){
		if(typeof page == 'undefined') {
			return window.location.href;
		}
		var foo = window.location.href.toString().split("/");
		var pages = page.split("/");
		$.each(pages, function(i, page){
			foo[foo.length-1-i] = page;
		});
		if(typeof params != 'undefined') {
			$.each(String(params).split("/"), function(i,param){
				foo.push(param);
			});
		}
		return foo.join("/");
	};
!function ($) {

  $(function(){

    // fullcalendar
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var addDragEvent = function($this){
      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($this.text()), // use the element's text as the event title
        className: $this.attr('class').replace('label',''),
		editable: true,
		type: $this.closest('.myEvents').data('type')
      };
      
      // store the Event Object in the DOM element so we can get to it later
      $this.data('eventObject', eventObject);
      
      // make the event draggable using jQuery UI
      $this.draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });
    };
    $('.calendar').each(function() {
      $(this).fullCalendar({
        header: {
          left: 'prev,next',
          center: 'title',
          right: 'today,month,agendaWeek,agendaDay'
        },
        //editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay, jsEvent, ui) { // this function is called when something is dropped
          
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
			
			$this = $(this);
			$.post('add_event_calendar', {
				name: copiedEventObject.title,
				date: moment(date).format('YYYY/MM/DD HH:mm'),
				type: copiedEventObject.type,
			}, function(res){
				copiedEventObject.id = res;
				copiedEventObject.title = copiedEventObject.type.charAt(0).toUpperCase()+ copiedEventObject.type.slice(1) + ': ' + copiedEventObject.title;
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				$this.remove();
			});
            
            // is the "remove after drop" checkbox checked?
            //if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              //$(this).remove();
            //}
          }
        ,
		eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
			console.log('drop '+event.type, event);
			$.post('move_event_calendar', {name: event.title, id: event.id, start: moment(event.start).format('YYYY/MM/DD HH:mm'), end: moment(event.end ? event.end : event.start).format('YYYY/MM/DD HH:mm'), type: event.type}, function(res){
				console.log(res);
			});
		},
		eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {
			console.log('resize '+event.type, event);
			$.post('move_event_calendar', {name: event.title, id: event.id, start: moment(event.start).format('YYYY/MM/DD HH:mm'), end: moment(event.end ? event.end : event.start).format('YYYY/MM/DD HH:mm'), type: event.type}, function(res){
				console.log(res);
			});
		},
		selectable: true,
		selectHelper: true,
		eventRender: function(event, element) {
			var icon_class = 'fa-tasks';
			if(event.type == 'project')
				icon_class = 'fa-briefcase';
			element.find('div.fc-event-inner').prepend('<i class="fa '+icon_class+'"></i> ').find('span.fc-event-title').prepend(event.type.charAt(0).toUpperCase()+event.type.slice(1)+': ');
		},
        events: [
			<?php if($projects):?>
			<?php foreach($projects as $project):?>
			{
				id: <?php echo $project->id?>,
				title: '<?php echo addslashes($project->project_name)?>',
				start: new Date('<?php echo $project->project_start_date ?>'),
				end: new Date('<?php echo $project->project_end_date ?>'),
				allDay: <?php echo $project->projects_all_day?>,
				<?php if($project->projects_color):?>
				backgroundColor: '<?=$project->projects_color?>',
				<?php else:?>
				className: 'bg-primary',
				<?php endif;?>
				type: 'project',
				editable: <?php e($project->created_by == $this->auth->user_id() ? 'true' : 'false');?>
			},
			<?php if($project->tasks):?>
			<?php foreach($project->tasks as $task):?>
			{
				id: <?php echo $task->id?>,
				title: '<?php echo addslashes($task->task_name)?>',
				start: new Date('<?php echo $task->task_start_date ?>'),
				end: new Date('<?php echo $task->end_date ?>'),
				allDay: <?php echo $task->tasks_all_day?>,
				type: 'task',
				projectId: <?php echo $task->project_id?>,
				editable: <?php e($task->created_by == $this->auth->user_id() ? 'true' : 'false');?>,
				<?php if($task->color):?>
				backgroundColor: '<?=$task->color?>',
				<?php endif;?>
				assigned_to: <?=json_encode($task->assigned_to)?>
			},
			<?php endforeach;?>
			<?php endif;?>
			
			<?php endforeach;?>
			<?php endif;?>
			
			<?php if($unassigned_tasks):?>
			<?php foreach($unassigned_tasks as $task):?>
			{
				id: <?php echo $task->id?>,
				title: '<?php echo addslashes($task->task_name)?>',
				start: new Date('<?php echo $task->task_start_date ?>'),
				end: new Date('<?php echo $task->end_date ?>'),
				allDay: <?php echo $task->tasks_all_day?>,
				type: 'task',
				projectId: <?php echo $task->project_id?>,
				editable: <?php e($task->created_by == $this->auth->user_id() ? 'true' : 'false');?>,
				<?php if($task->color):?>
				backgroundColor: '<?=$task->color?>',
				<?php endif;?>
				assigned_to: <?=json_encode($task->assigned_to)?>
			},
			<?php endforeach;?>
			<?php endif;?>
        ],
		eventClick: function(calEvent, jsEvent, view) {
			if(typeof calEvent.id == 'undefined')
				return false;
				
			var ucfirst = function(string) {
				return string.charAt(0).toUpperCase() + string.slice(1);
			};
			$('#someModal').remove();
			var modalHtml = '<div class="modal fade" id="someModal"><div class="modal-dialog"><div class="modal-content">'
			+'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
			+'<h4 class="modal-title" id="myModalLabel">'+ucfirst(calEvent.type)+' Details</h4></div>'
			+'<div class="modal-body">Loading...</div>'
			+'<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
			var $loadedModal = $(modalHtml).modal(),
				ajaxUrl = url('all',calEvent.id),
				$modalFooter = $loadedModal.find('.modal-footer');
			$('body').append($loadedModal);
			if(calEvent.type == 'task')
				ajaxUrl = url('all/tasks',calEvent.id);
			$.get(ajaxUrl, function(data){
				data = data[0];console.log(data);
				var htmlBody = '<table class="table">';
				if(calEvent.type == 'project') {
				
					if(data.created_by == <?=$authed_user->id?>)
						$modalFooter.append('<a href="edit/'+data.id+'" class="btn btn-success">Edit</a><a data-toggle="button" href="#" onclick="return confirm(\'Are you sure to delete this?\') ? location.href=\'delete/'+data.id+'\' : false" class="btn btn-danger">Delete</a>');
					projname = data.project_name;
						
					// htmlBody += '<tr><td><strong>Project ID</strong></td><td>'+data.id+'</td></tr>';
					htmlBody += '<thead><tr><td><strong>Project Name</strong></td><td>'+data.project_name+'</td></tr></thead><tbody>';
					htmlBody += '<tr><td><strong>Prefix(IO Number)</strong></td><td>'+data.prefix_io_number+'</td></tr>';
					htmlBody += '<tr><td><strong>SBU</strong></td><td>'+data.sbu_id+'</td></tr>';
					htmlBody += '<tr><td><strong>Description</strong></td><td>'+data.description+'</td></tr>';
//					htmlBody += '<tr><td><strong>Client</strong></td><td>'+data.client.client_name+'</td></tr>';
					htmlBody += '<tr><td><strong>Project Status</strong></td><td>'+data.status+'</td></tr>';
					htmlBody += '<tr><td><strong>Created by</strong></td><td>'+data.creator.display_name+'</td></tr>';
					htmlBody += '<tr><td><strong>Assigned To</strong></td><td>'+ $.map(data.assigned_to, function(e){return e.display_name;}).join(', ') +'</td></tr>';
					
					/*
					htmlBody += '<tr><td><strong>Project Visibility</strong></td><td>'+data.project_visibility+'</td></tr>';
					htmlBody += '<tr><td><strong>Allow Comments</strong></td><td>'+data.allow_comments+'</td></tr>';
					htmlBody += '<tr><td><strong>Send Notifications</strong></td><td>'+data.send_notifications+'</td></tr>';
					htmlBody += '<tr><td><strong>Project Notes</strong></td><td>'+data.project_notes+'</td></tr>';
					*/
					htmlBody += '<tr><td><strong>Project Start Date</strong></td><td>'+ $.fullCalendar.formatDate(new Date(data.project_start_date), 'dd-MM-yyyy HH:mm') +'</td></tr>';
					htmlBody += '<tr><td><strong>Project End Date</strong></td><td>'+ $.fullCalendar.formatDate(new Date(data.project_end_date), 'dd-MM-yyyy HH:mm') +'</td></tr>';
					htmlBody += '<tr><td><strong>Created On</strong></td><td>'+$.fullCalendar.formatDate(new Date(data.created_on), 'dd-MM-yyyy HH:mm')+'</td></tr>';
					htmlBody += '<tr><td><strong>Project Cost</strong></td><td>'+data.cost_currency+' '+data.project_cost+'</td></tr>';
					htmlBody += '<tr><td><strong>Overall Progress</strong></td><td>'+ data.overall_progress * 100 +'%</td></tr>';
					htmlBody += '<tr><td><strong>Remaining Days</strong></td><td>';
					if(new Date(data.project_end_date).getTime() > new Date().getTime()){
						if(new Date(data.project_start_date).getTime() < new Date().getTime())
							htmlBody += Math.round((new Date(data.project_end_date).getTime() - new Date().getTime())/(24*60*60*1000) + 1) +'/'+ Math.round((new Date(data.project_end_date).getTime() - new Date(data.project_start_date).getTime())/(24*60*60*1000) + 1);
						else
							htmlBody += '0/'+ Math.round((new Date(data.project_end_date).getTime() - new Date(data.project_start_date).getTime())/(24*60*60*1000) + 1);
					} else {
						htmlBody += Math.round((new Date(data.project_end_date).getTime() - new Date(data.project_start_date).getTime())/(24*60*60*1000) + 1) +'/'+ Math.round((new Date(data.project_end_date).getTime() - new Date(data.project_start_date).getTime())/(24*60*60*1000) + 1);
					}
					htmlBody += ' day(s)</td></tr>';
					//htmlBody += '<tr><td><strong>Project Tag</strong></td><td>'+data.project_tag+'</td></tr>';
				} else {
					var actionButtons = '', isAssigneesAndCreator = $.inArray('<?=$this->auth->user_id()?>', $.map(data.assigned_to, function(e){return e.id;})) != -1 || data.project.created_by == <?=$this->auth->user_id()?> || (data.project.id == 1 && data.created_by == <?=$this->auth->user_id()?>);
					if(isAssigneesAndCreator) {
						actionButtons += '<button type="button" class="btn btn-warning" id="get-progress-history">Progress History</button><button id="update-task" type="button" class="btn btn-info" data-toggle="class:show inline" data-target="#spin" data-loading-text="Saving...">Update</button>';
						if(data.project.created_by == <?=$this->auth->user_id()?> || (data.project.id == 1 && data.created_by == <?=$this->auth->user_id()?>))
							actionButtons += '<a href="'+url('edit/tasks',data.id)+'" class="btn btn-success">Edit</a><a data-toggle="button" href="#" onclick="return confirm(\'Are you sure to delete this?\') ? location.href=\''+url('delete/tasks',data.id)+'\' : false" class="btn btn-danger">Delete</a>';
					}
					$modalFooter.append(actionButtons);
					// htmlBody += '<tr><td><strong>Task ID</strong></td><td>'+data.id+'</td></tr>';
					htmlBody += '<thead><tr><td><strong>Project</strong></td><td>'+data.project.project_name+'</td></tr></thead><tbody>';
					htmlBody += '<tr><td><strong>Created by</td><td>'+data.creator.display_name+'</td></tr>';
					htmlBody += '<tr><td><strong>Task Name</td><td>'+data.task_name+'</td></tr>';
					htmlBody += '<tr><td><strong>Main Task</td><td><div id="tree1"></div></td></tr>';
					htmlBody += '<tr><td><strong>Description</td><td>'+data.description+'</td></tr>';htmlBody += '<tr><td><strong>Weightage</td><td>'+data.weightage_percentage+' %</td></tr>';
					htmlBody += '<tr><td><strong>Task Start Date</td><td>'+$.fullCalendar.formatDate(new Date(data.task_start_date), 'dd-MM-yyyy HH:mm')+'</td></tr>';
					htmlBody += '<tr><td><strong>Task End Date</td><td>'+$.fullCalendar.formatDate(new Date(data.end_date), 'dd-MM-yyyy HH:mm')+'</td></tr>';
					htmlBody += '<tr><td><strong>Created On</strong></td><td>'+$.fullCalendar.formatDate(new Date(data.created_on), 'dd-MM-yyyy HH:mm')+'</td></tr>';
					htmlBody += '<tr><td><strong>Assigned To</td><td>'+ $.map(data.assigned_to, function(e){return e.display_name;}).join(', ') +'</td></tr>';
					htmlBody += '<tr><td><strong>Remaining Days</strong></td><td>';
					if(new Date(data.end_date).getTime() > new Date().getTime()){
						if(new Date(data.task_start_date).getTime() < new Date().getTime())
							htmlBody +=  Math.round((new Date(data.end_date).getTime() - new Date().getTime())/(24*60*60*1000) + 1) +'/'+ Math.round((new Date(data.end_date).getTime() - new Date(data.task_start_date).getTime())/(24*60*60*1000) + 1) ;
						else
							htmlBody +=  '0/'+ Math.round((new Date(data.end_date).getTime() - new Date(data.task_start_date).getTime())/(24*60*60*1000) + 1) ;	
					} else {
							htmlBody +=  Math.round((new Date(data.end_date).getTime() - new Date(data.task_start_date).getTime())/(24*60*60*1000) + 1) +'/'+ Math.round((new Date(data.end_date).getTime() - new Date(data.task_start_date).getTime())/(24*60*60*1000) + 1) ;
					}
					htmlBody +=' day(s)</td></tr>';
					if (isAssigneesAndCreator)
						htmlBody += '<tr><td><strong>Update Progress %</td><td><label>0 %</label> <input class="slider slider-horizontal form-control" id="tasks_progress_percentage" type="text" name="tasks_progress_percentage" value="'+data.progress_percentage+'" data-slider-min="0" data-slider-max="100" data-slider-step="10" data-slider-value="'+data.progress_percentage+'" data-slider-orientation="horizontal" > <label>100 %</label></td></tr>'
									+'<tr><td><strong>Remarks</strong></td><td><textarea class="form-control" id="tasks_progress_remarks"></textarea></td></tr>';
					else
						htmlBody += '<tr><td><strong>Progress %</td><td>'+data.progress_percentage+' %</td>';
					/*
					htmlBody += '<tr><td><strong>Color</td><td>'+data.color+'</td></tr>';
					htmlBody += '<tr><td><strong>Send Notifications</td><td>'+data.send_notification+'</td></tr>';
					htmlBody += '<tr><td><strong>Task Notes</td><td>'+data.notes+'</td></tr>';
					htmlBody += '<tr><td><strong>Task Tag</td><td>'+data.task_tag+'</td></tr>';
					*/
				}
				htmlBody += '</tbody></table>';
				$loadedModal.find('.modal-body').html(htmlBody).promise().done(function(){
					if(calEvent.type == 'task') {
						$('#get-progress-history').off('click').on('click',function(e){
							var $progress_history_modal = $(modalHtml).modal();
							var progress_history_html = '<table class="table"><thead><tr><th>Progress</th><th>Time</th><th>User</th><th>Remarks</th></tr></thead><tbody>';
							if(data.progress_history.length > 0)
								$.each(data.progress_history, function(i,history){
									progress_history_html += '<tr><td>'+history.progress+'%</td><td>'+history.time_stamp+'</td><td>'+history.user.display_name+'</td><td>'+history.remarks+'</td></tr>';
								});
							progress_history_html += '</tbody></table>';
							$progress_history_modal.find('.modal-body').html(progress_history_html);
							$progress_history_modal.find('.modal-title').html('Progress History');
						});
						//jQuery('#tasks_progress_percentage').slider();
						new Slider('#tasks_progress_percentage');
						jQuery('#update-task').off('click').on('click',function(e){
							$.post(url('update_progress/tasks',data.id),{
									'progress': jQuery('#tasks_progress_percentage').val(),
									'tasks_id': data.id,
									'tasks_project_id': data.project.id,
									'tasks_time_stamp': new Date().getTime() / 1000,
									'remarks': jQuery('#tasks_progress_remarks').val(),
									'user_id': '<?=$authed_user->id?>'
								},function(data){
									$loadedModal.modal('hide');
							});
						});
						/*
						var tree_data2 = [], tree_data1 = new Object(), tree_data = new Object(), parent_task_id = data.parent_task_id; task = data; is_parent_exist = true;
						while(is_parent_exist) {
							
							if(typeof tree_data1 != 'object')
								tree_data1 = new Object();
							tree_data1.id = task.id;
							tree_data1.label = task.task_name;
							if(parent_task_id > 0) {
								tree_data = new Object();
								tree_data.children = [];
								tree_data.children[0] = tree_data1;
								tree_data1 = tree_data;
							}
							tree_data2[0] = tree_data1;
							
							if(parent_task_id > 0) {
								
								temp2 = task;
								task = temp2.parent;
								parent_task_id = task.parent_task_id;
								continue;
							}
							
							is_parent_exist = false;
						};
						
						$('#tree1').tree({
							data: tree_data2,
							selectable: false,
							autoOpen: true,
							onCreateLi: function(node, $li) {
								if(node.id == data.id)
									$li.find('.jqtree-title').css('text-decoration', 'underline');
							}
						});
						*/
						var task_id = data.id;
						$('#tree1').jstree({
							core: {
								data: {
									url: '<?php e(site_url(SITE_AREA .'/projectmgmt/projects/get_tasks_by_project_id'));?>/'+data.project_id
								}
							},
							settings: {
								core: {
									multiple: false
								}
							},
							types : {
								default : {
									icon : "fa fa-tasks"
								}
							},
							plugins : [ "types" ]
						}).on('load_node.jstree', function(e,data){
							console.log(e,data);
							$(e.target).jstree('select_node',task_id).jstree('open_all');
							$('ul li[role="treeitem"] > a').each(function() {
								$(this).click(false);
								$(this).dblclick(false);
							});
						}).on("click.jstree", ".jstree-ocl", function(e){
							$('ul li[role="treeitem"] > a').each(function() {
								$(this).click(false);
								$(this).dblclick(false);
							});
						});
					} else if(calEvent.type == 'project') {
						
						if(typeof data.group_chat.id == 'undefined') {
							
							$modalFooter.append('<button type="button" id="gotowall" class="btn btn-info pull-left"><i class="fa fa-bullhorn fa-1x"></i></button>').find('#gotowall').on('click', function(e){
								  
								swal({   
									title: "You are about to create a group conversation",   
									text: "",   
									type: "warning",   
									showCancelButton: true,   
									confirmButtonColor: "#65bd77",   
									confirmButtonText: "Yes, lets do it!",   
									closeOnConfirm: true
								}, function(){ 
									waitingDialog.show('...Creating conversation');
									$.ajax({
										type: "POST",
										url: "<?php echo site_url(SITE_AREA .'/ideabank/ideas/group_conference') ?>",
										data:{
											insert:1,
											gname:data.project_name,
											userid:$.map(data.assigned_to, function(e){return e.id;}),
											projectid:data.id
										},
										dataType: 'json',
										success: function(data){
											$loadedModal.modal('hide');
											waitingDialog.update('...Relocating');
	
										}
									}).done(function(data) {
										setTimeout(function(){ 
											window.location = "<?php echo site_url(SITE_AREA .'/ideabank/ideas/wall/group/') ?>/"+data.insertid;}, 5000);
									});
								});	
							});
						} else {
						
							$modalFooter.append('<button type="button" id="gotowall" class="btn btn-info pull-left"><i class="fa fa-eye fa-1x"></i></button>').find('#gotowall').on('click', function(e){
								window.location = "<?php echo site_url(SITE_AREA .'/ideabank/ideas/wall/group/') ?>/"+data.group_chat.id ;
							});
						}
					}
				});
			},'json');
		},
		select: function(start, end, allDay, jsEvent, view) {
			waitingDialog.show('Loading...');
			$('#someModal').remove();
			var $element = $(jsEvent.originalEvent.srcElement), indent = [],
				projects = '<option value="1">Unassigned Project</option>', project_assigned_to = [], weightage_percentage_remaining = [], project_subtasks = [], 
				get_subtask = function(tasks, task_id){
					var html = '';
					$.each(tasks, function(i,e){
						if(e.parent_task_id == task_id) {
							if(e.parent_task_id > 0)
								indent[e.id] = indent[e.parent_task_id]+1;
							else
								indent[e.id] = 0;
							html += '<option value="'+e.id+'">';
							for(var i = 0; i < indent[e.id]; i++)
							{
								html += '&nbsp;&nbsp;&nbsp;';
							}
							html += e.task_name+'</option>';
							html += get_subtask(tasks, e.id);
						}
					});
					return html;
				},
				unassigned_tasks = <?php echo json_encode($unassigned_tasks);?>;
			project_subtasks[1] = '';
			if(unassigned_tasks.length > 0)
				$.each(unassigned_tasks, function(i,e){
					if(e.parent_task_id == 0)
						project_subtasks[1] += get_subtask(unassigned_tasks,0);
				});
			$.get(url('all'), function(data){
			$.each(data, function(index,project){
				if(isNaN(project.project_start_date)) {
					project.project_start_date = new Date(project.project_start_date).getTime();
					project.project_end_date = new Date(project.project_end_date).getTime();
				}
				if(project.project_start_date <= start.getTime() && (project.project_end_date >= end.getTime() || (!allDay && project.projects_all_day && (project.project_end_date + 24*60*60*1000) >= end.getTime()))) {
					isCreateTaskAllowed = false, project_subtasks[project.id] = '';
					project_subtasks[project.id] += get_subtask(project.tasks, 0);
					if(project.created_by == <?=$this->auth->user_id()?>)
						isCreateTaskAllowed = true;
					if(project.assigned_to.length > 0) {
						project_assigned_to[project.id] = [];
						project_assigned_to[project.id].select2 = [];
						project_assigned_to[project.id].html = '';
						$.each(project.assigned_to, function(i,e){
							project_assigned_to[project.id].select2.push({id:e.id,text:e.display_name});
							project_assigned_to[project.id].html += '<option value="'+e.id+'">'+e.display_name+'</option>';
							if(e.id == <?=$this->auth->user_id()?>)
								isCreateTaskAllowed = true;
						});
						
						weightage_percentage_remaining[project.id] = 100;
						if(project.tasks)
							$.each(project.tasks, function(i,task){
								weightage_percentage_remaining[project.id] =  weightage_percentage_remaining[project.id] - task.weightage_percentage;
								if(task.parent_task_id == 0) {
									
								}
							});
					}
						
					if(isCreateTaskAllowed)
						projects += '<option value="'+project.id+'">'+project.project_name+'</option>';
				}
			});
			var modalHtml = '<div class="modal fade" id="someModal"><div class="modal-dialog"><div class="modal-content">'
			+'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
			+'<h5 class="modal-title" id="myModalLabel"><ul id="new-tab" class="nav nav-tabs">'
			+'<li class="active"><a href="#new-project" data-toggle="tab"><strong>New Project</strong></a></li>';
			if(projects)
				modalHtml += '<li><a href="#new-task" data-toggle="tab"><strong>New Task</strong></a></li>';
			modalHtml += '</ul></h5></div>'
			+'<div class="modal-body"><div class="tab-content"><div class="tab-pane active" id="new-project">Loading...</div>';
			if(projects)
				modalHtml += '<div class="tab-pane" id="new-task">Loading...</div>';
			modalHtml += '</div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <button id="submit-modal-form" class="btn btn-primary">Save</button></div></div></div></div>';			
			var $modal = $(modalHtml); 
			$('body').append($modal);
			var $loadedModal = $modal.modal();
			// add pillbox 
			var addPill = function($input){
				var $text = $input.val(), $pills = $input.closest('.pillbox'), $repeat = false, $repeatPill, pillBgClass = ['dark','success','warning','danger','info'];
				if($text == "") return;
				$("li", $pills).text(function(i,v){
					if(v == $text){
						$repeatPill = $(this);
						$repeat = true;
					}
				});
				if($repeat) {
					$repeatPill.fadeOut().fadeIn();
					return;
				};
				$item = $('<li class="label bg-'+pillBgClass[$pills.pillbox('items').length % 5]+'"><input type="hidden" name="projects_project_tag[]" value="'+$text+'">'+$text+'</li> ');
				$item.insertBefore($input);
				$input.val('');
				$pills.trigger('change', $item);
			};
			// end pillbox
			$loadedModal.find('.modal-body #new-project').load(url('create'),function(){
				$(this).children().eq(2).hide();
				$('[name=save]').parent().hide();
				$(this).find('h3').hide();
				
				$('#new-project .control-group').hide();
				$('#projects_project_name').closest('.control-group').show();
				$('#projects_description').closest('.control-group').show();
				$('#projects_client_id').closest('.control-group').show();
				$projects_project_start_date = $('#projects_project_start_date').datetimepicker({useMinutes:false,useCurrent:false});
				$projects_project_end_date = $('#projects_project_end_date').datetimepicker({useMinutes:false,useCurrent:false});
				$projects_project_start_date.on('dp.change',function(e){
					$projects_project_end_date.data('DateTimePicker').setMinDate(e.date);
				}).closest('.control-group').show();
				$projects_project_start_date.data('DateTimePicker').setDate($.fullCalendar.formatDate(start, 'yyyy/MM/dd HH:mm'));
				$projects_project_end_date.on('dp.change',function(e){
					$projects_project_start_date.data('DateTimePicker').setMaxDate(e.date);
				}).closest('.control-group').show();
				$projects_project_end_date.data('DateTimePicker').setDate($.fullCalendar.formatDate(end, 'yyyy/MM/dd HH:mm'));
				$('#projects_assigned_to').closest('.control-group').show();
				$('#projects_all_day').val(allDay);
				
				$(this).append('<a href="#" class="btn btn-sm btn-info btn-rounded more" style="margin-top:20px">More...</a>');
				$('.more').on('click',function(e){
					e.preventDefault();
					$(this).closest('.active').find('.control-group').show();
					$(this).remove();
				});
				jQuery('#projects_color').simplecolorpicker();
				$('#created_by').val('<?=$this->auth->user_id()?>');
				jQuery('select').not('#projects_color').select2({dropdownAutoWidth:true})
				.filter('#projects_assigned_to').select2('data',[{id: <?=$authed_user->id?>, text: '<?=$authed_user->display_name?>', locked: true}]);
				/*
				jQuery('#projects_client_id').on('select2-opening', function(){
					jQuery(this).select2('container').find('div.select2-drop').append('<div class="panel-footer text-sm"><a href="<?=base_url('index.php/admin/client/clients/create')?>" data-toggle="ajaxModal"><img src="<?php echo Template::theme_url('images/add-15.png'); ?>"/>&nbsp;&nbsp;Add New</a></div>').find('div.panel-footer').on('click', function(e){
						//window.open($(this).find('a').prop('href'),'_blank');
					});
				});
				*/
				// pillbox
				jQuery('#projects_project_tag input').on('blur', function() {
					addPill($(this));
				});
				jQuery('#projects_project_tag input').on('keypress', function(e) {
					if([13,32].indexOf(e.which) > -1) {
						e.preventDefault();
						addPill($(this));
					}
				});
				waitingDialog.hide();
			});
			var proceed_create_task = false;
			if(projects)
				$loadedModal.find('.modal-body #new-task').load(url('create/tasks'),function(){
					$(this).children().eq(2).hide();
					$('[name=save]').parent().hide();
					$(this).find('h3').hide();
					$('#new-task form').find('#tasks_project_id').html(projects);
					
					$('#new-task .control-group').hide();
					$('#tasks_project_id').closest('.control-group').show();
					$('#tasks_task_name').closest('.control-group').show().find('input:checkbox#is_subtask').on('click', function(e){
						is_subtask = $(this).prop('checked'), $subtask_dropdown = $('select#parent_task_id');
						if(is_subtask)
							$subtask_dropdown.select2('enable', true);
						else
							$subtask_dropdown.select2('enable', false);
					});
					$('#tasks_description').closest('.control-group').show();
					$tasks_start_date = $('#tasks_start_date').datetimepicker({useMinutes:false,useCurrent:false});
					$tasks_end_date = $('#tasks_end_date').datetimepicker({useMinutes:false,useCurrent:false});
					$tasks_start_date.on('dp.change', function(e){
						$tasks_end_date.data('DateTimePicker').setMinDate(e.date);
					}).closest('.control-group').show();
					$tasks_end_date.on('dp.change', function(e){
						$tasks_start_date.data('DateTimePicker').setMaxDate(e.date);
					}).closest('.control-group').show();
					$tasks_start_date.data('DateTimePicker').setDate($.fullCalendar.formatDate(start, 'yyyy/MM/dd HH:mm'));
					$tasks_end_date.data('DateTimePicker').setDate($.fullCalendar.formatDate(end, 'yyyy/MM/dd HH:mm'));
					$('#tasks_assigned_to').on('change', function(e) {
						proceed_create_task = false;
					}).closest('.control-group').show();
					$('#tasks_all_day').val(allDay);
					
					$(this).append('<a href="javascript:void(0)" class="btn btn-sm btn-info btn-rounded more" style="margin-top:20px">More...</a>');
					$('.more').on('click',function(){
						$(this).closest('.active').find('.control-group').show();
						$(this).remove();
					});
					jQuery('#tasks_color').simplecolorpicker();
					//jQuery('#tasks_progress_percentage').slider();
					new Slider('#tasks_progress_percentage');
					$tasks_weightage_percentage = jQuery('#tasks_weightage_percentage');
					//$tasks_weightage_percentage.removeAttr('data-slider-max');
					$select = jQuery('select').select2({dropdownAutoWidth:true});
					jQuery('#tasks_project_id').on('change',function(){
						$tasks_weightage_percentage.siblings('.slider').remove();
						$max_weightage = jQuery('#max_weightage').text(100);
						slider_options = {};
						if(typeof weightage_percentage_remaining[$(this).val()] != 'undefined') {
							$max_weightage.text(weightage_percentage_remaining[$(this).val()]);
							slider_options = {max: weightage_percentage_remaining[$(this).val()]};
						}
						new Slider('#tasks_weightage_percentage', slider_options);
						//if($tasks_weightage_percentage.parent().is('.slider'))
						//	$tasks_weightage_percentage.slider('destroy');
						//$tasks_weightage_percentage.bootstrapSlider();//{max:weightage_percentage_remaining[$(this).val()]}
						jQuery('#tasks_assigned_to').html($('#projects_assigned_to').html()).select2('data',[]);
						if(typeof project_assigned_to[$(this).val()] != 'undefined') {
							jQuery('#tasks_assigned_to').html(project_assigned_to[$(this).val()].html).select2('data',project_assigned_to[$(this).val()].select2);
						}
						
						if(typeof project_subtasks[$(this).val()] != 'undefined') {
							$subtask = $('select#parent_task_id').html(project_subtasks[$(this).val()]).prop('disabled',true);
							$is_subtask = $('input:checkbox#is_subtask').prop('checked',false).prop('disabled',false);
							$subtask.select2('val', $subtask.find('option:eq(0)').prop('value'));
							if($subtask.val() == null)
								$is_subtask.prop('checked',false).prop('disabled',true);
						}
					}).trigger('change');
					// pillbox
					jQuery('#tasks_task_tag input').on('blur', function() {
						addPill($(this));
					});
					jQuery('#tasks_task_tag input').on('keypress', function(e) {
						if([13,32].indexOf(e.which) > -1) {
							e.preventDefault();
							addPill($(this));
						}
					});
				});
			var activeTab = 'new-project', activeTagInput = 'projects_project_tag';
			$('#new-tab a').on('click',function(e){
				activeTab = $(this).attr('href').substr(1);
				activeTagInput = 'tasks_task_tag';
			});
			$('#submit-modal-form').on('click', function() {

				var $modalFooter = $loadedModal.find('.modal-footer'), 
					conflicted_user_index = -1,
					conflicted_tasks = $('#calendar').fullCalendar('clientEvents').filter(function(o){
						if(o.type == 'task' 
							&& o.start.getTime() <= end.getTime() 
							&& (o.end == null ? o.start.getTime() : o.end.getTime()) 
								>= start.getTime()) {
							if(typeof o.assigned_to != 'undefined' && o.assigned_to.length > 0 && $('#tasks_assigned_to').val() != null) {
								
								for(i = 0; i < o.assigned_to.length; i++) {
									conflicted_user_index = i;
									if($('#tasks_assigned_to').val().indexOf(o.assigned_to[i].id.toString()) >= 0)
										return true;
								}
							}
						}
						return false;
					});
				console.log(conflicted_tasks, $('#tasks_assigned_to').val(), conflicted_user_index);
				$modalFooter.find('.alert').remove();
				if(!proceed_create_task && conflicted_tasks.length > 0 && activeTab == 'new-task') {
					$modalFooter.prepend('<div class="alert alert-warning text-left">'+conflicted_tasks[0].assigned_to[conflicted_user_index].display_name+' already assigned to '+ conflicted_tasks[0].title+'. Click save to proceed anyway.</div>').find('.alert').fadeOut().fadeIn();
					$(this).button('reset');
					proceed_create_task = true;
					return;
				}
				
				var $form = $('#'+activeTab+' form'), action;
				if(activeTab == 'new-project') {
					if($('#projects_project_name').val().trim() == '') {
						alert('Project Name required');
						return false;
					}
					if($('#projects_assigned_to').val() == null) {
						alert('Assigned To required');
						return;
					}
					$('#projects_project_start_date').val(moment($('#projects_project_start_date').val(), 'DD/MM/YYYY HH:mm').format('YYYY/MM/DD HH:mm'));
					$('#projects_project_end_date').val(moment($('#projects_project_end_date').val(), 'DD/MM/YYYY HH:mm').format('YYYY/MM/DD HH:mm'));
					action = url('create');
					$('#projects_all_day').val( new Date($('#projects_project_start_date').val()).valueOf() == new Date($('#projects_project_start_date').val()).setHours(0,0,0,0) && new Date($('#projects_project_end_date').val()).valueOf() == new Date($('#projects_project_end_date').val()).setHours(0,0,0,0));
				} else {
					if($('#tasks_task_name').val().trim() == '') {
						alert('Task Name required');
						return false;
					}
					if($('#tasks_assigned_to').val() == null) {
						alert('Assigned To required');
						return;
					}
					$('#tasks_start_date').val(moment($('#tasks_start_date').val(), 'DD/MM/YYYY HH:mm').format('YYYY/MM/DD HH:mm'));
					$('#tasks_end_date').val(moment($('#tasks_end_date').val(), 'DD/MM/YYYY HH:mm').format('YYYY/MM/DD HH:mm'));
					action = url('create/tasks');
					$('#tasks_all_day').val( new Date($('#tasks_start_date').val()).valueOf() == new Date($('#tasks_start_date').val()).setHours(0,0,0,0) && new Date($('#tasks_end_date').val()).valueOf() == new Date($('#tasks_end_date').val()).setHours(0,0,0,0));				
				}
				var form_vals = $form.serialize()+'&ajax=&save=';
				//$loadedModal.find('input, textarea, select').attr('disabled',true);
				//$loadedModal.fadeTo(500, 0.6);
				$loadedModal.modal('hide');
				waitingDialog.show('Saving...');
				$.post(action, form_vals, function(data,textStatus){console.log(data);
					if(typeof data.error == 'undefined') {
						$('#calendar').fullCalendar('renderEvent', {
							id:data.id,
							start:new Date(activeTab == 'new-project' ? data.project_start_date : data.task_start_date),
							end:new Date(activeTab == 'new-project' ? data.project_end_date : data.end_date),
							title:activeTab == 'new-project' ? data.project_name : data.task_name,
							allDay:activeTab == 'new-project' ? data.projects_all_day : tasks_all_day,
							// className:activeTab == 'new-project' ? 'bg-primary' : '',
							type: data.type,
							backgroundColor: activeTab == 'new-project' ? $('#projects_color').val() : $('#tasks_color').val(),
						}, true);
					}
					//$('#someModal').modal('hide');
					waitingDialog.hide();
				}, 'json');
			});
		
			}, 'json');},
		eventMouseover: function (data, event, view) {
			tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#2e3e4e;position:absolute;z-index:10001;padding:5px 5px 5px 5px ;  line-height: 200%;color:#fff;border-radius:.25em">' 
				+ 'Loading...' 
				+ '</div>';

			$("body").append(tooltip);
			$(this).mouseover(function (e) {
				$(this).css('z-index', 10000);
				$('.tooltiptopicevent').fadeIn('500');
				$('.tooltiptopicevent').fadeTo('10', 1.9);
			}).mousemove(function (e) {
				$('.tooltiptopicevent').css('top', e.pageY + 10);
				$('.tooltiptopicevent').css('left', e.pageX + 20);
			});
			var ajaxUrl = url('all',data.id), type = data.type;
			if(type == 'task')
				ajaxUrl = url('all/tasks',data.id);
			$.get(ajaxUrl, function(data){
				data = data[0];
				$('.tooltiptopicevent').html( (type == 'task' ? 'Project Name: '+data.project.project_name+'<br/>' : '' ) +'Created by: '+ data.creator.display_name + '<br/>Assigned To: ' + $.map(data.assigned_to, function(e){return e.display_name;}).join(', '));
			}, 'json');
		},
		eventMouseout: function (data, event, view) {
			$(this).css('z-index', 8);

			$('.tooltiptopicevent').remove();

		},
      })
	  <?php if($this->input->get('project')):?>
		.fullCalendar('gotoDate', new Date('<?=$projects[0]->project_start_date?>'));
	  <?php endif;?>
	  ;
    });
    $('.myEvents').on('change', function(e, item){
      addDragEvent($(item));
    });

    $('.myEvents li').each(function() {
      addDragEvent($(this));
    });
	$(document).on('click', '#get-gantt', function(e){
		$('#ganttModal').remove();
		var modalHtml = '<div class="modal fade" id="ganttModal"><div class="modal-dialog" style="width:100%"><div class="modal-content">'
			+'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
			+'<h4 class="modal-title" id="myModalLabel">Gantt View</h4></div>'
			+'<div class="modal-body">Loading...</div>'
			+'<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>';
			var $modal = $(modalHtml); 
			$('body').append($modal);
			var $loadedModal = $modal.modal();
			$loadedModal.find('.modal-body').load(url('ganttview'));
	});
  });
}(window.jQuery);
</script>