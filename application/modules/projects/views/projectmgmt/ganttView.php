<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="<?=Template::theme_url('js/jquery.ganttView/lib/jquery-ui-1.8.4.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=Template::theme_url('js/jquery.ganttView/example/reset.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=Template::theme_url('js/jquery.ganttView/jquery.ganttView.css')?>" />
	<style type="text/css">
		body {
			/*font-family: tahoma, verdana, helvetica;
			font-size: 0.8em;
			padding: 10px;*/
		}
	</style>
	<title>jQuery Gantt</title>
</head>
<body>
<div class="modal-dialog" style="width:100%;font-family: tahoma, verdana, helvetica;
			font-size: 0.8em;
			padding: 10px;"><div class="modal-content" ><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>  <h4 class="modal-title">Gantt Chart</h4></div>
  <div class="modal-body">
	<div id="ganttChart"></div>
	<br/><br/>
	<div id="eventMessage"></div>

	<script type="text/javascript" src="<?=Template::theme_url('js/jquery.ganttView/lib/jquery-1.4.2.js')?>"></script>
	<script type="text/javascript" src="<?=Template::theme_url('js/jquery.ganttView/lib/date.js')?>"></script>
	<script type="text/javascript" src="<?=Template::theme_url('js/jquery.ganttView/lib/jquery-ui-1.8.4.js')?>"></script>
	<script type="text/javascript" src="<?=Template::theme_url('js/jquery.ganttView/jquery.ganttView.js')?>"></script>
	<script type="text/javascript" src="<?=Template::theme_url('js/jquery.ganttView/example/data.js')?>"></script>
	<script type="text/javascript">
		$(function () {
			$("#ganttChart").ganttView({ 
				data: [
				<?php if($ganttData):?>
					<?php foreach ($ganttData as $data):?>
					{
						id: <?=$data['id']?>,
						name: "<?=$data['name']?>",
						series: [
						<?php if($data['series']):?>
						<?php foreach ($data['series'] as $series):?>
							{
								name: "<?=$series['name']?>",
								start: <?=$series['start']?>,
								end: <?=$series['end']?>
								
							},
						<?php endforeach;?>
						<?php endif;?>
						]
					},
					<?php endforeach;?>
				<?php endif;?>
				],
				slideWidth: 900,
				behavior: {
					onClick: function (data) { 
						var msg = "You clicked on an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
						console.log(msg);
					},
					onResize: function (data) { 
						var msg = "You resized an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
						console.log(msg);
					},
					onDrag: function (data) { 
						var msg = "You dragged an event: { start: " + data.start.toString("M/d/yyyy") + ", end: " + data.end.toString("M/d/yyyy") + " }";
						console.log(msg);
					}
				}
			});
			
			// $("#ganttChart").ganttView("setSlideWidth", 600);
		});
	</script>
    </div>  <div class="modal-footer">
      <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
      
    </div></div></div>

</body>
</html>
