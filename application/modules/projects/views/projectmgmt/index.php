<?php

$num_columns	= 14;
$can_delete	= $this->auth->has_permission('Projects.Content.Delete');
$can_edit		= $this->auth->has_permission('Projects.Content.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
	<div class="panel-body">
    <?php echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
		<div class="row">
			<div class="col-md-6">  <h4>Projects</h4></div>
			<div class="col-md-3">
				<select name="select_field" id="select_field" class="form-control m-b"  onchange="setfname()">
					<option>All Fields</option>		
				</select>
			</div>
			<div class="col-md-3">
				<div class="input-group">
                    <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
					<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>
					<span class="input-group-btn">
						<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
						<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
					</span>
                </div>
			</div>
		</div>
	<?php echo form_close(); ?>       

    <div class="table-responsive">
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th>Project</th>                  
                   <th>Client</th>
                    
                   <th>Start Date</th>
                    
                   <th>End Date</th>
                    
                   <th>Status</th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('projects_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = 1;
					foreach ($records as $record) : 
				?>
				<tr>
				<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
				<?php else:?>
					<td><?php echo $record->id; ?></td>
				<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/projectmgmt/projects/edit/' . $record->id, '<i class="fa fa-edit icon"></i>' .  $record->project_name); ?></td>
				<?php else : ?>
					<td><?php e($record->project_name); ?></td>
				<?php endif; ?>
             
					<td><?php e($record->client->client_name) ?></td>
					<td><?php e($record->project_start_date) ?></td>
					<td><?php e($record->project_end_date) ?></td>
					<td><?php e($record->status) ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php echo form_close(); ?>
	</section>
<footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">         
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing 1 - 10 of 30</small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">      
                    </div>
                  </div>
                </footer>
                
                </div>
</div>