<!--<script src="<?php //echo Template::theme_url('js/parsley/parsley.min.js');     ?>" cache="false"></script> 
-->
<header class="panel-heading font-bold">Edit Milestones & Tasks</header>
<div class="panel-body" > 
    <form action="<?= base_url() ?>index.php/admin/projectmgmt/projects/milestone/<?= $this->uri->segment(6) ?>" method="post" id="editform" data-parsley-validate>
        <?php
        $ms = $this->db->query("select * from intg_milestones where milestone_id = '" . $this->uri->segment(5) . "'")->row();
        $prj = $this->db->query("select * from intg_projects where id = '" . $ms->project_id . "'")->row();
        $psd = explode("-", $prj->project_start_date);
        $ped = explode("-", $prj->project_end_date);
        ?>
        <div class="form-group pull-in clearfix">
            <div class="col-sm-3">
                <label>Milestone</label>
                <input class="input-sm  form-control" id='projects_mile_stone' type='text' name='projects_mile_stone' maxlength="45" value="<?php echo $ms->milestone_name ?>" required />
            </div>
            <div class="col-sm-3">
                <label>Start Date</label>
                <input style="cursor:pointer"  class="<?php echo $prj->row_status == "draft" ? "datepicker-input" : "disabled" ?> form-control input-sm input-s" data-date-format="dd-mm-yyyy"    id='projects_milestone_editstart_date' type='text' name='projects_milestone_start_date'  value="<?php echo date("d-m-Y", strtotime($ms->start_date)); ?>"    required readonly='' />
            </div>
            <div class="col-sm-3">
                <label>End Date</label>
                </br>
                <input style="cursor:pointer"  class="datepicker-input form-control input-sm input-s"  data-date-format="dd-mm-yyyy" id='projects_milestone_editend_date' type='text' name='projects_milestone_end_date'  value="<?php echo date("d-m-Y", strtotime($ms->end_date)) ?>"  required readonly='' />
            </div>
            <div class="col-sm-3"> </div>
            <div class="col-sm-3"> </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-16" id="lineitem_form">
            <header class="panel-heading font-bold  bg-info" style="color: #ffffff; ">Resources</header>
            <section class="panel panel-default">
                <div class="table-responsive ">
                    <table class="table table-striped b-t b-light text-sm">
                        <thead>
                            <tr>
                                <th>Level</th>
                                <th>Man Days</th>
                                <th>Head Count</th>
                                <th>Total Man Days</th>
                                <th>Daily FTE</th>
                                <th>Time Cost (RM)</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='" . $ms->milestone_id . "'")->result(); // echo $this->db->last_query(); 
                            $no = 1;
                            $first = true;
                            $tot = 0;
                            foreach ($mu as $ul) {
                                if ($first) {
                                    // first record
                                    $uid = $ul->uid;
                                    $first = false;
                                } else {
                                    // not first record
                                }
                                ?>
                                <tr id="row<?= $ul->uid ?>">
                                    <td><?php
                                        $ulevel = $this->db->query("select id,user_level from intg_user_level")->result();
                                        ?>
                                        <select name="select[]" id="select<?= $ul->uid ?>" class="selecta form-control product" style="height:30px; font-size:12px; width:140px" onchange="edit_select_fte(<?= $ul->uid ?>)"  required>
                                            <option></option>
                                            <?php
                                            foreach ($ulevel as $u) {
                                                 $fte = get_dailyfte_bydate($u->id,date('Y-m-d'));
                                                    $dfte = '0';
                                                    if($fte){
                                                        $dfte = $fte['daily_fte'];
                                                    }else{
                                                        $dfte = 'false';
                                                    }
                                                
                                                
                                                if ($u->id == $ul->userlevel_id) {
                                                    ?>
                                                    <option selected value="<?= $u->id ?>" dfte="<?= $dfte ?>">
                                                        <?= $u->user_level ?>
                                                    </option>
                                                <?php } else { ?>
                                                    <option value="<?= $u->id ?>" dfte="<?= $dfte ?>">
                                                        <?= $u->user_level ?>
                                                    </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select></td>
                                        <td><input name="mandays[]" id="mandays<?= $ul->uid ?>" onChange="edit_calc(<?= $ul->uid ?>)" value="<?= $ul->man_days ?>" data-parsley-type="number"   class="input-sm input-xs  form-control resetit"  required/></td>
                                    <td><input name="headcount[]" id="headcount<?= $ul->uid ?>"  value="<?= $ul->head_count ?>"  onChange="edit_calc(<?= $ul->uid ?>)" data-parsley-type="integer" class="input-sm  input-xs   form-control resetit"  required/></td>
                                    <td><input type="hidden"  id="tmandays<?= $ul->uid ?>" class="input-sm  input-xs  form-control"  value="<?= $ul->total_man_days ?>"/>
                                        <span id="dtmandays<?= $ul->uid ?>" class="resetit">
                                            <?= number_format($ul->total_man_days) ?>
                                        </span></td>
                                    <td><input type="hidden" name="fte[]" id="fte<?= $ul->uid ?>" class="input-sm   input-xs  form-control"  value="<?= $ul->fte ?>"/>
                                        <span id="dfte<?= $ul->uid ?>">
                                            <?= $ul->fte ?>
                                        </span></td>
                                    <td><input type="hidden"  class="input-sm input-xs  form-control tcosts tcostse  resetit" id="tcost<?= $ul->uid ?>"  value="<?= $ul->time_cost ?>" />
                                        <span class="resources  resetit" id="dtcost<?= $ul->uid ?>">
                                            <?= number_format($ul->time_cost, 2) ?>
                                        </span>
                                        <input type="hidden" name="uid[]" value="<?= $ul->uid ?>" /></td>
                                    <td><?php if ($no > 1) { ?>
                                            <i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="edit_remrow('<?= $ul->uid ?>')"></i>
                                        <?php } ?></td>
                                </tr>
                                <?php
                                $tot = $tot + $ul->time_cost;
                                $no++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr id="lastrow" counter="<?= $ul->uid ?>">
                                <td colspan="3"><a href="javascript:void(0)" onclick="edit_addnewrow()"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong class="m-r">Add Line</strong><span id="ajaxadd"></span></a></td>
                                <td colspan="2" align="right"><strong>Total Cost (RM)</strong></td>
                                <td><span id="totalcost"><?= number_format($tot, 2) ?></span></td>
                                <td>&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </section>
        </div>
        <div class="col-sm-16">
            <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Cost Involved<span class="showpc pull-right"></span></header>
            <section class="panel panel-default">
                <div class="panel-body" >
                    <div class="row">
                        <div class="col-sm-6 col-xs-6 column col-md-3">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                    <label>Material Cost (RM)*</label>
                                    <input class="input-sm input-xs  form-control tcostse" id='material_cost' onblur="costinv()"  type="text"   name='material_cost'   value="<?php echo $ms->material_cost ?>"  data-parsley-type="number"  required />
                                </div>
                                <div class="col-sm-6 col-xs-6 column col-md-12">
                                    <textarea name="mcost_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  style="height: 30px;"><?php echo $ms->mcost_desc ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3 column col-md-3">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                    <label>New CAPEX (RM)*</label>
                                    <input class="input-sm input-xs  form-control tcostse" id='new_capex' onblur="costinv()"  type='text' name='new_capex'   value="<?php echo $ms->new_capex ?>" data-parsley-type="number" required />
                                </div>
                                <div class="col-sm-6 col-xs-6 column col-md-12">
                                    <textarea name="ncapex_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  style="height: 30px;"><?php echo $ms->ncapex_desc ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6 column col-md-3">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                    <label>Outsourcing (RM)*</label>
                                    <input class="input-sm input-xs  form-control tcostse" id='outsourcing'  onblur="costinv()" type='text' name='outsourcing'  value="<?php echo $ms->outsourcing ?>" data-parsley-type="number" required />
                                </div>
                                <div class="col-sm-6 col-xs-6 column col-md-12">
                                    <textarea name="outsourcing_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  style="height: 30px;"><?php echo $ms->outsourcing_desc ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3 column col-md-3">
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 column col-md-12 m-b">
                                    <label>Others (RM)*</label>
                                    <input class="input-sm input-xs  form-control tcostse" id='others' type='text' onblur="costinv()"  name='others'  value="<?php echo $ms->others ?>" data-parsley-type="number" required />
                                </div>
                                <div class="col-sm-6 col-xs-6 column col-md-12">
                                    <textarea name="others_desc" cols="40" rows="1" class="form-control input-sm input-s expand"  style="height: 30px;"><?php echo $ms->others_desc ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="form-group"> <?php
            echo form_label('Task & Description*', 'projects_assigned_to', array('class' => 'control-label'));


            $tps = $this->db->query("select task_id,task_pool_id from intg_milestones_tasks  where milestone_id = '" . $ms->milestone_id . "'")->result();

            //  echo $this->db->last_query();
            foreach ($tps as $s) {
                $l[] = $s->task_pool_id;
                ?>
                <input type="hidden" name="task_id[]" value="<?= $s->task_id ?>" />
                <input type="hidden" name="taskpoolold[]" value="<?= $s->task_pool_id ?>"/>
                <?php
            }



            $task_pool = $this->db->query("select * from intg_task_pool order by task_name asc")->result();
            //if ( in_array(2,$l)) { echo "true"; } else { echo "falase"; }
            ?>
            <div class='controls'>
                <select multiple name="task_pool[]" id="task_pool" class="form-control" style=" width: 300px; height:auto; font-size:12px"  required>
                    <?php
                    foreach ($task_pool as $tp) {
                        if (in_array($tp->id, $l)) {
                            ?>
                            <option selected value="<?= $tp->id ?>"   data-title="<?= $tp->description ?>">
                                <?= $tp->task_name ?>
                            </option>
                        <?php } else { ?>
                            <option value="<?= $tp->id ?>"   data-title="<?= $tp->description ?>">
                                <?= $tp->task_name ?>
                            </option>
                        <?php } ?>
                    <?php } ?>
                    <option disabled="disabled"></option>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <div class='controls'>
                <input type="hidden" name="new_proj_value" value="<?= $_POST['project_value'] - $_POST['milestone_value'] ?>" />
                <input type="hidden" name="created_by" id="created_by" />
                <input name="project_id" type="hidden" value="<?= $this->uri->segment(6) ?>" />
                <input type="button" id="cancel" name="cancel" class="btn btn-danger" value="Cancel"  />
                <?php
                $rc_appr = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $this->auth->user_id() . "  AND project_id = '" . $ms->project_id . "' ");

//             echo "approval :".$rc_appr->num_rows()." - Version_no : ".$ms->version_no."- Ms Status:".$ms->status;

                if ($prj->status == "Initiated" && $prj->final_status == "Yes") {
//                    ($ms->version_no > 0 || $ms->version_no != "" || $ms->version_no != NULL)
                    if ($rc_appr->num_rows() == 0 && $ms->status == 'No') {
                        ?>
                        <input name="milestone_id" type="hidden" value="<?= $this->uri->segment(5) ?>" />
                        <input type="submit" id="submit" name="update" class="btn btn-primary" value="Save"  />
                    <?php } else { ?>
                        <input name="milestone_id" type="hidden" value="<?= $ms->parent_Rid == 0 ? $this->uri->segment(5) : $ms->parent_Rid ?>" />
                        <input type="submit" id="submit3" name="create_version" class="btn btn-info" value="Create Version"  />
                        <?php
                    }
                }

                if ($prj->status == "Initiated" && $prj->final_status == "Yes") {
                    ?>
    <!--<input type="submit" id="submit3" name="create_version" class="btn btn-info" value="Create Version"  />-->
                <?php } else { ?>
                    <input name="milestone_id" type="hidden" value="<?= $ms->parent_Rid == 0 ? $this->uri->segment(5) : $ms->parent_Rid ?>" />
                    <input type="submit" id="submit" name="update" class="btn btn-primary" value="Save & Exit"  />
                <?php } ?>
                <span id="spin"></span> </div>
        </div>
    </form>
    <?php
    $ped = explode("-", $prj->project_end_date);
    ?>
</div>
<script src="<?php echo Template::theme_url('js/parsley/parsley.min.js'); ?>" cache="false"></script> 
<script>

                                        function numberWithCommas(x)
                                        {
                                            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        }

                                        function costinv() {
                                            var pc = "<?= $_POST['project_value'] - $_POST['milestone_value'] ?>";

                                            console.log(pc);

                                            var a = 0;
                                            $(".tcostse").each(function () {

                                                a = a + $(this).val() * 1;
                                                console.log("inside tcostse:" + a);
                                                // $("#totalcost").text(numberWithCommas(parseFloat(a).toFixed(2)));

                                            });

                                            console.log("outside:" + a);
                                            //numberWithCommas(parseFloat($(".resources").text()).toFixed(2))
                                            var c = a + pc * 1;

                                            $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));
                                        }

                                        $('.selecta').select2(
                                                {
                                                    placeholder: "Please Select",
                                                    allowClear: false, dropdownAutoWidth: true
                                                }


                                        );

                                        $('#task_pool').select2(
                                                {
                                                    formatResult: format,
                                                    formatSelection: format,
                                                    placeholder: "Please Select",
                                                    allowClear: false, dropdownAutoWidth: true
                                                }


                                        );

                                        function parseDate(str) {
                                            var mdy = str.split('-')
                                            //console.log("PD:"+mdy);
                                            return new Date(mdy[2], mdy[1] - 1, mdy[0]);
                                        }

                                        function daydiff(first, second) {
                                            console.log("DD:" + Math.round((second - first) / (1000 * 60 * 60 * 24) + 1));
                                            return Math.round((second - first) / (1000 * 60 * 60 * 24) + 1);
                                        }

                                        function edit_calc(val)
                                        {

                                            var sd = $("#projects_milestone_editstart_date").val();
                                            var ed = $("#projects_milestone_editend_date").val();


                                            var a = $("#mandays" + val).val() * 1;


                                            if (a > daydiff(parseDate(sd), parseDate(ed))) {
                                                alert("please enter value less than the start and end date difference ");
                                                $("#mandays" + val).val("");
                                                $("#mandays" + val).focus();
                                                return false;
                                            }

                                            var b = $("#headcount" + val).val() * 1;

                                            var c = parseFloat(a * b);

                                            var f = parseFloat($("#tmandays" + val).val(c));

                                            var d = parseFloat($("#fte" + val).val() * 1);


                                            var e = c * d;

                                            console.log("total" + c);
                                            $("#dtcost" + val).text(numberWithCommas(e));
                                            $("#dfte" + val).text((d));
                                            $("#dtmandays" + val).text(numberWithCommas(c));

                                            $("#tcost" + val).val(e);
                                            var pc = "<?= $_POST['project_value'] - $_POST['milestone_value'] ?>";

                                            var a = 0;
                                            $(".tcostse").each(function () {

                                                a = a + $(this).val() * 1;
                                                console.log("inside tcostse:" + a);
                                                $("#totalcost").text(numberWithCommas(parseFloat(a).toFixed(2)));

                                            });

                                            console.log("outside:" + a);
                                            var c = a + pc * 1;

                                            $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

                                        }

                                        function edit_select_fte(val)
                                        {
                                            $("#headcount" + val).removeAttr("disabled");
                                            $("#mandays" + val).removeAttr("disabled");
                                            $("#fte" + val).val($("#select" + val + "  option:selected").attr("dfte"));

                                            edit_calc(val);
                                        }

                                        $(document).ready(function () {
                                            $("#editform").parsley('validate');



                                            var enddate = $("#projects_milestone_editend_date").val();

                                                <?php if ($prj->row_status == "draft") { ?>
                                                $('#projects_milestone_editstart_date').datepicker({
                                                    format: "dd-mm-yyyy",
                                                    startDate: "<?= $psd[2] . "/" . $psd[1] . "/" . $psd[0] ?>",
                                                    endDate: "<?= $ped[2] . "/" . $ped[1] . "/" . $ped[0] ?>"
                                                });
                                                  <?php } ?>


                                            $("#projects_milestone_editend_date").datepicker().on('changeDate', function () {

                                                if (Math.round((parseDate($("#projects_milestone_editend_date").val()) - parseDate(enddate)) / (1000 * 60 * 60 * 24) + 1) < 1) {

                                                    // $(".selecta").select2("val", "");

                                                    $(".resetit").each(function () {



                                                        $(this).val("");
                                                        $(this).text("");

                                                    });

                                                }
                                                
                                                
                                                var mystr = this.value;
                                                var myarr = mystr.split("-");

                                                var x = new Date(myarr[2]+"-"+myarr[1]+'-'+myarr[0]);
                                                var y = new Date('<?= $ped[0] . "-" . $ped[1] . "-" . $ped[2] ?>');


                                                if(x > y){
                                                    var r = confirm("You are selecting date that is after the project end date. This will automatically update the Project End Date according to your selection. Are you sure want to do this?");
                                                    if (r == true) {

                                                    } else {
                                                        $('#projects_milestone_editend_date').val('');
                                                        return;
                                                    }
                                                }


                                            });


                                            if ($('input[type="submit"]').val() == "Create Version") {

                                                $("#projects_milestone_editstart_date").datepicker({startDate: new Date()}).on('changeDate', function (selected) {
                                                    var minDate = new Date(selected.date.valueOf());
                                                    console.log(minDate);
                                                    $('#projects_milestone_editend_date').datepicker('setStartDate', minDate);
                                                });
                                                $("#projects_milestone_editend_date").datepicker().on('changeDate', function (selected) {
                                                    var minDate = new Date(selected.date.valueOf());
                                                    $('#projects_milestone_editstart_date').datepicker('setEndDate', minDate);
                                                });

                                            } else {

                                                var psd, res;
                                                psd = $('#projects_milestone_editstart_date').val();
                                                res = psd.split("-");
                                                $('#projects_milestone_editend_date').datepicker({
                                                    format: "dd-mm-yyyy",
                                                    startDate: res[0] + "/" + res[1] + "/" + res[2],
                                                    endDate: "<?= $ped[2] . "/" . $ped[1] . "/" . $ped[0] ?>"
                                                })


                                            }

                                            var pc = "<?= $_POST['project_value'] - $_POST['milestone_value'] ?>";







                                            window.ParsleyConfig = {
                                                errorsWrapper: '<div class="pe"></div>',
                                                //errorTemplate: '<div class="parsley-required bounceIn animated"></div>'
                                            };
                                        });

                                        function edit_remrow(val)
                                        {


                                            $("#row" + val).hide('fast', function () {
                                                $("#row" + val).remove();
                                            });

                                        }



                                        function inArray(needle, haystack) {
                                            var length = haystack.length;
                                            for (var i = 0; i < length; i++) {
                                                if (haystack[i] == needle)
                                                    return true;
                                            }
                                            return false;
                                        }

                                        function edit_addnewrow()
                                        {
                                            var theId = $("#lastrow").attr("counter");
                                            var theId = parseInt(theId) + 1;
                                            $("#lastrow").attr("counter", theId);

                                            //  $("#ajaxadd").html("<i class='fa fa-spin fa-spinner'></i>");
                                            var existing_catg = [];

                                            var Attendees_list = [];




                                            $('#lastrow').before('<tr id=row' + theId + '><td><select name="select[]" id="select' + theId + '" required class="selecta form-control product"   onchange="edit_select_fte(' + theId + ')"  style="height:30px; font-size:12px;  width:140px" ><option></option></select></td><td><input type="number"  data-parsley-type="number"   name="mandays[]" id="mandays' + theId + '" disabled  onChange="edit_calc(' + theId + ')" class="input-sm input-xs  form-control" required/></td><td><input type="number" name="headcount[]"    onChange="edit_calc(' + theId + ')"  disabled id="headcount' + theId + '" data-parsley-type="number" class="input-sm  input-xs   form-control" required/></td><td><input type="hidden" name="tmandays[]" id="tmandays' + theId + '" class="input-sm   input-xs  form-control" /><span id="dtmandays' + theId + '"></span></td><td><input type="hidden" name="fte[]" id="fte' + theId + '" class="input-sm   input-xs  form-control" /><span id="dfte' + theId + '"></span></td><td><input type="hidden" name="tcost[]" class="tcosts input-sm input-xs tcostse form-control" id="tcost' + theId + '" /><input type="hidden" name="uid[]" value="' + theId + '" /><span id="dtcost' + theId + '"></span></td><td><i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="edit_remrow(\'' + theId + '\')"></i></td></tr>');


                                            $('#select' + theId).select2(
                                                    {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true}
                                            );
                                            //  $('#select' + theId).select2("val", "");


                                            <?php
                                            foreach ($ulevel as $c) {
                                                $fte = get_dailyfte_bydate($c->id,date('Y-m-d'));
                                                $dfte = '0';
                                                if($fte){
                                                    $dfte = $fte['daily_fte'];
                                                }else{
                                                    $dfte = 'false';
                                                }
                                                ?>
                                                var attendees = new Object();
                                                attendees.id = "<?= $c->id ?>";
                                                attendees.user_level = "<?= $c->user_level ?>";
                                                attendees.daily_fte = "<?= $dfte ?>";
                                                Attendees_list.push(attendees);
                                            <?php } ?>


                                            $(".product").each(function () {
                                                existing_catg.push($(this).val());
                                            });

                                            $.each(Attendees_list, function (key, value) {

                                                if (inArray(value.id, existing_catg) == true) {
                                                } else {
                                                    $('#select' + theId + '').append("<option value='" + value.id + "' dfte='" + value.daily_fte + "'>" + value.user_level + "</option>");
                                                }

                                            });

                                            $('#editform').parsley('addItem', '#select' + theId);
                                            $('#editform').parsley('addItem', '#mandays' + theId);
                                            $('#editform').parsley('addItem', '#headcount' + theId);



                                        }




                                        $("#cancel").click(function (e) {

                                            location.href = "<?= $_SERVER['HTTP_REFERER'] ?>";

                                        });



                                        $("#submit,#submit2,#submit3").submit(function (e) {
                                            //console.log($("#editform").serialize());
                                            console.log($("#editform").parsley('validate'));

                                            // e.preventDefault();
                                            if (!$("#editform").parsley('validate')) {
                                                //alert("alll");
                                                e.preventDefault();
                                            } else {

                                                //alert("ttttttttalll");
                                                console.log($(this).val());

                                                var a;
                                                var b;
                                                if (this.id == "submit3") {
                                                    a = "create_version"
                                                    b = "";
                                                } else {
                                                    a = "update";
                                                    if ($(this).val() == "Save") {
                                                        b = "save_versioned_ms";
                                                    } else {
                                                        b = "save_n_exit";
                                                    }
                                                }

                                                console.log($("#editform").serialize());
                                                $("#spin").html("<i class='fa fa-spin fa-spinner'></i>");
                                                /*$.post("<?php //echo site_url(SITE_AREA .'/projectmgmt/projects/milestone/'.$this->uri->segment(6))     ?>", $("#editform").serialize() + '&' + a + '=' + b, function (data) {
                                                 
                                                 $("#spin").html("<i class='fa fa-check' style='color:rgb(142, 193, 101)'></i>");                                             
                                                 
                                                 // window.location.href = window.location.href;
                                                 
                                                 });*/
                                            }

                                        });





</script>