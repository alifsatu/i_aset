<?php

$num_columns	= 14;
$can_delete	= $this->auth->has_permission('Projects.Content.Delete');
$can_edit		= $this->auth->has_permission('Projects.Content.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
	<div class="panel-body">
  <style type="text/css">
	.mix{
		display: none;
	}
	@media (max-width:500px) {
		.mix {
			width: 100%;
		}
	}
	.input-group {
    display: inline-table;
    vertical-align: middle;
}
    .input-group .input-group-addon,
    .input-group .input-group-btn,
    .input-group .form-control {
        width: auto !important;
    }
</style>
            
			  <div class="m-b-md">
				<div class="row">
					<div class="col-sm-6">
					<form class="form-inline">
						<div class="form-group">
							<div class="input-group">
								<input class="form-control" type="text" placeholder="<? echo lang('projects_search'); ?>" id="search_project_name_text" value="<?=$this->input->get('search')?>"/>
								<span class="input-group-btn"> 
									<a href="#" onclick="location.href='?search='+document.getElementById('search_project_name_text').value" class="btn btn-default" type="button"><? echo lang('search'); ?></a> 
								</span>
							</div>
						</div>
					</form>
				  </div>
				<div class="col-sm-6">
				<form class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control datepicker-input" placeholder="From Date" id="from-date" value="<?=$this->input->get('from')?>" data-date-format="dd-mm-yyyy"> 
						</div>
						<? echo lang('to'); ?>
						<div class="form-group">
							<div class="input-group">
								<input type="text" class="form-control datepicker-input" placeholder="To Date" id="to-date" value="<?=$this->input->get('to')?>" data-date-format="dd-mm-yyyy">
								<span class="input-group-btn">	
									<a href="#" class="btn btn-info" data-toggle="class:show inline" data-target="#spin" data-loading-text="Searching..." id="filter-by-date"><i class="fa fa-search"></i></a>
									<a href="draft" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
								</span>
							</div>
						</div>
					</form>
					</div>
				</div>
				</div>      

    <div class="table-responsive">
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th>Project</th>                  
                  
                   <th>Start Date</th>
                    
                   <th>End Date</th>
                    
                   <th>Status</th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('projects_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = 1;
					foreach ($records as $record) : 
				?>
				<tr>
				<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
				<?php else:?>
					<td><?php echo $record->id; ?></td>
				<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/projectmgmt/projects/edit/' . $record->id, '<i class="fa fa-edit icon"></i>' .  $record->project_name); ?></td>
				<?php else : ?>
					<td><?php e($record->project_name); ?></td>
				<?php endif; ?>
             
					
					<td><?php echo date("d-m-Y",strtotime($record->project_start_date)); ?></td>
					<td><?php echo date("d-m-Y",strtotime($record->project_end_date)); ?></td>
					<td><?php e($record->row_status) ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
	<?php echo form_close(); ?>
	</section>
<footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">         
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing 1 - 10 of 30</small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">      
                    </div>
                  </div>
                </footer>
                
                </div>
</div>
<script>
$('a#filter-by-date').on('click',function(e){
						location.href='?from='+$('input#from-date').val()+'&to='+$('input#to-date').val();
					});
					</script>