<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
    ?>

    <div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('projects_create_error') ?></h4>
        <?php echo $validation_errors; ?> </div>
    <?php
endif;
if (isset($projects)) {
    $projects = (array) $projects;
}else{
    $projects = (array) $this->db->query("select * from intg_projects where id = '" . $this->uri->segment(5) . "'")->row();
}
$id = isset($projects['id']) ? $projects['id'] : '';
?>
<style>
    .pad { padding:10px 15px; }

    .popover { width:300px; }
    .tooltip {
        position: fixed; 
    }
    .resimg {
        max-width:900px;
        height: auto;
        -ms-object-fit: cover;
        -moz-object-fit: cover;
        -o-object-fit: cover;
        -webkit-object-fit: cover;
        object-fit: cover;
        overflow: hidden;
    }
</style>
<?php   $pj = $this->db->query("select * from intg_projects where id = '".$this->uri->segment(5)."'")->row(); ?>
<div class="row" style="height:1000px">
    <div class="col-sm-12">

        <div class="panel-group m-b" id="accordion2">

            <div class="panel panel-default ">
                <div class="panel-heading clearfix "><?php echo form_open($this->uri->uri_string(), 'id="formaly" data-parsley-validate'); ?>

                    <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#accordion2" href="#project"><strong>Project: </strong>
                        <?= ucfirst($pj->project_name) ?>
                    </a> <span class="pull-right"><strong>Project Costs: </strong><span id="origpc">RM <?= number_format($pj->project_cost, 2) ?></span></span></div>
                <div id="project" class="panel-collapse collapse" style="height: auto;">
                    <div class="panel-body">
                      <div class="form-group  clearfix">
                        <div class="col-sm-3">
                          <header class="panel-heading bg-light  no-border"><strong>Project Code</strong></header>
                          <div class="pad">
                    <?= $pj->prefix_io_number ?>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <header class="panel-heading bg-light  no-border"><strong>SBU</strong></header>
                          <div class="controls pad">
                    <?= $this->db->query("select sbu_name from intg_sbu where id ='" . $pj->sbu_id . "'")->row()->sbu_name; ?>
                          </div>
                        </div>
                        <div class="col-sm-3">
                           <header class="panel-heading bg-light  no-border"><strong>Cost Centre</strong></header>
                          <div class="controls pad">
                    <?= $this->db->query("select cost_centre from intg_cost_centre where id ='" . $pj->cost_centre_id . "'")->row()->cost_centre; ?>
                          </div>
                        </div>
                         <div class="col-sm-3">
                         <header class="panel-heading bg-light  no-border"><strong>Duration in Days</strong></header>
                          <div class="controls pad">
                    <?= $pj->total_days ?>
                          </div>
                        </div>
                      </div>
                     <div class="form-group  clearfix">
                        <div class="col-sm-3">
                         <header class="panel-heading bg-light  no-border"><strong>Programme</strong></header>
                          <div class="controls pad">
                    <?= $this->db->query("select program_name from intg_programs where id ='" . $pj->program_id . "'")->row()->program_name; ?>
                          </div>
                        </div>
                        <div class="col-sm-3">
                         <header class="panel-heading bg-light   no-border"><strong>Justification of Project</strong></header>
                          <div class="controls pad" style="text-align:justify">
                    <?= $pj->justification ?>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <header class="panel-heading bg-light  no-border"><strong>Co-Worker</strong></header>
                          <div class="controls pad">
                    <?php
                    $us = $this->db->query("select display_name from intg_users where id IN ( " . $pj->coworker . ")")->result();
                    foreach ($us as $up) {
                        $upa[] = $up->display_name;
                    }
                    echo implode(",&nbsp;", $upa);
                    ?>
                          </div>
                        </div>
                       <div class="col-sm-3">
                          <header class="panel-heading bg-light  no-border"><strong>Attachment</strong></header>
                          <div class="controls pad">
                    <?php
                    if ($pj->attachment != "") {
                        $att = explode(":", $pj->attachment);
                        ?>
                                             <a href="<?= base_url() ?>uploads/<?= $att[0] ?>"><?= $att[0] ?></a>
                    <?php } ?>
                         
                          </div>
                        </div>
                      </div>
                      <div class="form-group  clearfix">
                       <div class="col-sm-3">
                         <header class="panel-heading bg-light  no-border"><strong>Created By</strong></header>
                       
            <div class="controls pad">
                    <?= $this->auth->display_name_by_id($pj->created_by) ?>
                          </div>
                        </div>
                        <div class="col-sm-3">
                         <header class="panel-heading bg-light  no-border"><strong>Start Date </strong>(dd/mm/yyyy) </header>
                       
            <div class="controls pad">
                    <?= date("d/m/Y", strtotime($pj->project_start_date)) ?>
                          </div>
                        </div>
                        
                        <div class="col-sm-3">
                         <header class="panel-heading bg-light no-border"><strong>End Date</strong> (dd/mm/yyyy)</header>
            
            <div class="controls pad">
                    <?= date("d/m/Y", strtotime($pj->project_end_date)) ?>
            </div>
                        </div>
                        <div class="col-sm-3">
                           <header class="panel-heading bg-light no-border"><strong>Project Color</strong></header>
                          <div class="controls pad">
                         <div style="background-color: <?= $pj->projects_color ?>;  width:80px; text-align:center">&nbsp;</div>
                          </div>
                        </div>
<!--                                    <div class="col-sm-3">
                           <header class="panel-heading bg-light  no-border"><strong>Approvers Hierarchy</strong></header>
                          <div class="controls pad">
                           <a href="#"   data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i>&nbsp;&nbsp;View</a>
                                                                    <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">
            
                
                <div class="modal-content">
                  <div class="modal-body" align="center" width="400" height="300">
                     
                    <?php
                    $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="42" and bas.approval_status_mrowid = "' . $pj->id . '" ORDER BY bas.id asc');
                    //echo 'SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="40" and bas.approval_status_mrowid = "'.$projects['id'].'" ORDER BY bas.id asc';
                    // echo $this->db->last_query();
                    $rowreject = $queryappr->row();
                    ?>
                      
                     
                  <h4><?php echo lang('quote_approvers_hierarchy') ?></h4>
                  <table width='80%' class='table table-bordered'>
                    <tr>
                      <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                      <td><strong><?php echo lang('quote_role') ?></strong></td>
                      <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                      <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                      <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                    </tr>
                    <?php
                    foreach ($queryappr->result() as $rowappr) {

                        switch ($rowappr->approvers_status) {
                            case "No" : $status = " <span class='badge bg-warning m-l'>Pending</span>";
                                break;
                            case "Yes" : $status = " <span class='badge bg-success m-l'>Approved</span>";
                                break;
                            case "Reject" : $status = " <span class='badge bg-important m-l'>Rejected</span>";
                                break;
                        }
                        ?>
                                        <tr>
                                          <td><?= $rowappr->display_name ?></td>
                                          <td><?= $rowappr->role_name ?>
                                          <td><?php
                        if (($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No")) {

                            echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        } else {
                            echo $status;
                        }
                        ?></td>
                                          <td><?= $rowappr->approvers_remarks ?></td>
                                          <td><?php
                        if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                            echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                        }
                        ?></td>
                                        </tr>
                    <?php }
                    ?>
              <tr>   
                <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>
                     
              </tr>
                  </table>
               
                      
                      
                       
                    </div>
                 
                  <div class="modal-footer">
                   <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                
                  </div>
                </div>
            
              </div>
            </div>
                          </div>
                        </div>-->
                        
                      </div>
                      <div class="form-group  clearfix">
                        <div class="col-sm-10">
                          <header class="panel-heading bg-light  no-border"><strong>Introduction</strong></header>
                          <div class="controls pad">
                    <?= $pj->introduction ?>
                          </div>
                        </div>
                  </div>
                </div>
                </div>

                <?php
                $noms = 1;
                $ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '" . $this->uri->segment(5) . "'")->result();


                foreach ($ms as $ml) {

                    $v = $this->db->query("select max(version_no) as maxver from intg_milestones where parent_Rid= '" . $ml->milestone_id . "'")->row()->maxver;
                    if ($v > 0) {

                        $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $ml->milestone_id . "' order by milestone_id desc limit 1")->row();
                    }
                    ?>
                    <div class="panel panel-default">

                        <div class="panel-heading clearfix"> <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#accordion2" href="#ml<?= $ml->milestone_id ?>"><strong>Milestone
                                    <?= $noms ?>
                                    : </strong>
                                <?= ucfirst($ml->milestone_name) ?>
                                <strong class="m-l">Start Date: </strong>
                                <?= date("d/m/Y", strtotime($ml->start_date)) ?>
                                <strong class="m-l">End Date: </strong>
                                <?= date("d/m/Y", strtotime($ml->end_date)) ?>
                            </a> 

                            <?php milestone_approval_badges($ml, $this->auth->user_id(), $ml->project_id);
//                            $rc_approval = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $this->auth->user_id() . "  AND project_id = '" . $this->uri->segment(5) . "' ");
//
//                            switch ($ml->status) {
//                                case "No" :
//                                    if ($rc_approval->num_rows() == 0)
//                                        $status = "<span class='badge btn-warning m-l text-xs'>Pending Approval</span>";
//                                    else
//                                        $status = "<span class='badge btn-success m-l text-xs'>Sent For Approval</span>";
//                                    break;
//                                case "Yes" :
//                                    $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
//                                    break;
//                                case "Reject" :
//                                    $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
//                                    break;
//                            }
//                            echo $status;
//                            
//                            $action = '';
//                            if ($ml->action != '') {
//                                switch ($ml->action) {
//                                    case "deactivate" :
//                                        $action = "<span class='badge btn-warning m-l text-xs'>Pending Deactivation Approval</span>";
//                                        break;
//                                    case "activate" :
//                                        $action = "<span class='badge btn-warning m-l text-xs'>Pending Activation Approval</span>";
//                                        break;
//                                    case "deactivated" :
//                                        $action = "<span class='badge btn-danger m-l text-xs'>Deactivated</span>";
//                                        break;
//                                    default :
//                                        break;
//                                }
//                            }
//                            echo $action;
                            ?>

                            <ul class="nav nav-pills nav-sm pull-left" style="padding:5px 20px">


                                <li></li>
                                <li class="hour" data-toggle="popover"  data-content='<textarea name="comments"  cols="30" rows="2" class="form-control expand"><?= $ml->remarks ?></textarea><button class="btn btn-xs btn-primary cmpmark" style="margin-top:5px" rowid="<?= $ml->milestone_id ?>">Submit</button>' data-html="true" data-placement="bottom" data-original-title="Mark As Complete" data-comments="<?= $ml->remarks ?>"><a href="#"></a></li>

                                <?php
                                if ($v > 0) {

                                    if ($ml->version_no > 0) {
                                        $vno = "<span class='badge bg-info text-xs'>V" . $ml->version_no . " Version History</span> ";
                                    } else {
                                        $vno = "<span class='badge bg-info text-xs'>V0 Version History</span>";
                                    }
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->parent_Rid ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><?= $vno ?></a>
                                        <!--<a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->parent_Rid ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><span class='badge bg-info'>V<?= $v ?></span></a>-->
                                    </li>
                                <?php } ?>
                            </ul>


                            <span id="mcostup<?= $ml->milestone_id ?>" class="pull-right"></span>

                        </div>

                        <div id="ml<?= $ml->milestone_id ?>" class="panel-collapse collapse" style="height: auto;">
                            <div class="panel-body text-sm">

                                <div class="col-sm-16" id="lineitem_form">
                                    <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Users</header>
                                    <section class="panel panel-default">
                                        <div class="table-responsive ">
                                            <table class="table table-striped b-t b-light text-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Level</th>
                                                        <th>Man Days</th>
                                                        <th>Head Count</th>
                                                        <th>Total Man Days</th>
                                                        <th>Daily FTE (RM)</th>
                                                        <th>Time Cost (RM)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='" . $ml->milestone_id . "'")->result();
                                                    $milestone_cost = 0;
                                                    foreach ($mu as $u) {
                                                        ?>
                                                        <tr >
                                                            <td><?= $u->user_level ?></td>
                                                            <td><?= $u->man_days ?></td>
                                                            <td><?= $u->head_count ?></td>
                                                            <td><?= $u->total_man_days ?></td>
                                                            <td><?= $u->fte ?></td>
                                                            <td><?= number_format($u->time_cost, 2) ?></td>
                                                        </tr>
                                                        <?php
                                                        $milestone_cost = $milestone_cost + $u->time_cost;
                                                    }
                                                    ?>

                                                </tbody>

                                            </table>
                                            <span class="mcostdown" rowid="<?= $ml->milestone_id ?>" id="matcost<?= $ml->milestone_id ?>" style="display:none"><?php echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span>

                                        </div>
                                    </section>


                                    <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
                                    <section class="panel panel-default">
                                        <div class="table-responsive ">
                                            <table class="table table-striped b-t b-light text-sm">
                                                <thead>
                                                    <tr>
                                                        <th>Material Cost (RM)</th>
                                                        <th>New CAPEX (RM)</th>
                                                        <th>Outsourcing (RM)</th>
                                                        <th>Others (RM)</th>                       
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr >
                                                        <td><?= number_format($ml->material_cost, 2) ?></td>
                                                        <td><?= number_format($ml->new_capex, 2) ?></td>
                                                        <td><?= number_format($ml->outsourcing, 2) ?></td>
                                                        <td><?= number_format($ml->others, 2) ?></td>

                                                    </tr>

                                                </tbody>

                                                <thead>
                                                    <tr>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>
                                                        <th>Remarks</th>                       
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr >
                                                        <td><?= $ml->mcost_desc ?></td>
                                                        <td><?= $ml->ncapex_desc ?></td>
                                                        <td><?= $ml->outsourcing_desc ?></td>
                                                        <td><?= $ml->others_desc ?></td>

                                                    </tr>

                                                </tbody>

                                            </table>


                                        </div>
                                    </section>
                                </div>
                                <div class="form-group" style="margin-top:10px">
                                    <label for="projects_assigned_to" class="control-label"><strong>Task & Description</strong></label>
                                    <div class="controls pad">
                                        <?php
                                        $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '".$ml->milestone_id."'")->result(); $no=1;
                                        foreach ( $tp as $t ) {
                                        ?>

                                        <b><?= $no ?>. <?= $t->task_name ?> </b> <br> <?= $t->description ?> <br>

                                        <?php
                                        $no++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                </div><?php			

                $noms++; $milestone_cost = 0 ; 

                } 


                ?>
            </div>
            <?php
            $qq = $this->db->query('select id from intg_milestone_approval where project_id=' . $this->uri->segment(5) . ' order by id DESC')->row();
            $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="42" and bas.approval_status_mrowid = "' . $qq->id . '" ORDER BY bas.id asc');
// echo 'SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="42" and bas.approval_status_mrowid = "'.$qq->id.'" ORDER BY bas.id asc';
// echo $this->db->last_query();
            $rowreject = $queryappr->row();







            if ($projects['initiator'] != $this->auth->user_id()) {
                ?>
                <div class="col-sm-6">
                    <h4><?php echo lang('quote_approvers_hierarchy') ?></h4>
                    <table width='80%' class='table table-bordered' >
                        <tr>
                            <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                            <td><strong><?php echo lang('quote_role') ?></strong></td>
                            <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                            <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                            <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                        </tr>
                        <?php
                        foreach ($queryappr->result() as $rowappr) {

                            switch ($rowappr->approvers_status) {
                                case "No" : $status = "<span class='badge bg-warning'>&nbsp;Pending&nbsp;</span>";
                                    break;
                                case "Yes" : $status = "<span class='badge bg-success'>Approved</span>";
                                    break;
                                case "Reject" : $status = "<span class='badge bg-important'>Rejected&nbsp;</span>";
                                    break;
                            }
                            ?>
                            <tr>
                                <td><?= $rowappr->display_name ?></td>
                                <td><?= $rowappr->role_name ?>
                                <td><?php
                                    if (($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No")) {

                                        echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                                    } else {
                                        echo $status;
                                    }
                                    ?></td>
                                <td><?= $rowappr->approvers_remarks ?></td>
                                <td><?php
                                    if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                        echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                                    }
                                    ?></td>
                            </tr>
                        <?php }
                        ?>
                        <tr>   
                            <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>

                        </tr>
                    </table>
                </div>
                <?php
                $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "42" and approval_status_mrowid = "' . $qq->id . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by)');
                //echo $this->db->last_query();			
                $rowm = $querym->row();


                if ($querym->num_rows() > 0) {
                    ?>
                    <div id="load" class="col-sm-3">
                        <h4><?php echo lang('quote_approver_action') ?></h4>
                        <form id="quickapprove">
                            <table width="80%" class="table table-bordered">
                                <tr>
                                    <td><?php echo lang('quote_approver_action_status') ?></td>
                                </tr>
                                <tr>
                                    <td><div class="btn-group" data-toggle="buttons" style="width:70%">
                                            <ul>
                                            </ul>
                                            <label class="btn btn-sm btn-info active" id="label_yes">
                                                <input type="radio" name="approval_status_status" checked="checked" id="option1"  value="Yes">
                                                <i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_yes') ?> </label>
                                            <label class="btn btn-sm btn-danger" id="label_no">
                                                <input type="radio" name="approval_status_status" id="option2" value="Reject">
                                                <i class="fa fa-check text-active"></i><?php echo lang('quote_approver_action_no') ?> </label>
                                        </div></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('quote_approver_remarks') ?></td>
                                </tr>
                                <tr>
                                    <td><textarea name="approval_status_remarks" style="border: #ccc solid 1px" cols="50" rows="4" id="asr"></textarea></td>
                                </tr>
                                <tr>
                                    <td><input name="updateappr" type="button" value="Submit" class="btn btn-success" onclick="updateApprovalstatus('<?= $qq->id ?>', '<?= $rowm->id ?>', '<?= $rowm->rolename ?>', '<?= $rowm->approval_status_mrowid ?>', 'edit', '<?= $this->auth->user_id() ?>', '<?= $pj->final_approvers ?>', '<?= $pj->initiator ?>', 'mile')" data-loading-text="Updating..." />
                                                <!--<input type="button" class="btn" onclick="getValue()" value="See Value First"> -->		</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<!--end of col6--> 

</div>
<!--end of row--> 
<?php if ($pj->attachment != "") { ?>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width:auto">
            <div class="modal-content">	
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"><?= $att[1] ?></h4>
                </div>	 
                <div class="modal-body" align="center">

                    <img src="<?= base_url() ?>uploads/<?= $att[0] ?>" class="resimg"/>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

                </div>
            </div>
        </div>
    </div><?php } ?>
<script src="<?php echo Template::theme_url('js/parsley/parsley.min.js'); ?>" cache="false"></script> 
<script>



                                        $(".hour").on("focus", function () {
                                            $(this).popover('destroy');
                                            $(".popover").hide();
                                        });


                                        $('body').on('click', '.popover button', function () {


                                            var a = $(this);
                                            var ta = $(this).parent().parent().find("textarea").val();



                                            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/milestone/markcomplete') ?>", {remarks: ta, rowid: $(this).attr("rowid")}, function (data) {

                                                $(".popover").hide();
                                            })


                                        });

                                        function numberWithCommas(x) {
                                            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        }

                                        $(document).ready(function () {

                                            $(".mcostdown").each(function () {
                                                var row = $(this).attr("rowid");
                                                $("#mcostup" + row).html("<b>Milestone Cost:</b> RM " + numberWithCommas(parseFloat($(this).text() * 1).toFixed(2)) + "&nbsp;&nbsp;");
                                            })

                                            var pc = "<?= $pj->project_cost ?>";

                                            $(".tcosts").on("change", function () {
                                                var a = 0;
                                                $(".tcosts").each(function () {

                                                    a = a + $(this).val() * 1;
                                                    console.log(a);

                                                });

                                                var c = a + pc * 1;
                                                console.log(c);

                                                $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

                                            });



                                            window.ParsleyConfig = {
                                                errorsWrapper: '<div class="pe"></div>',
                                            };
                                        });

                                        $(".edit_milestone").on("click", function () {

                                            $("#project").collapse('hide');
                                            //$("#ml"+$(this).attr("milestone_id")).collapse('hide');

                                            $("#formedit").html("<i class='fa fa-spin fa-spinner'></i>");
                                            $.post("<?= site_url() ?>/admin/projectmgmt/projects/edit_milestone/" + $(this).attr("milestone_id") + "/" + $(this).attr("project_id"), {project_value: "<?= $pj->project_cost ?>", milestone_value: $("#matcost" + $(this).attr("milestone_id")).text()}, function (data) {
                                                $("#formedit").html(data);
                                            });
                                        });








                                        $("#submit,#submit2").click(function (e) {
                                            if ($('#formaly').parsley().validate())
                                            {
                                                var txt;
                                                var r = confirm("Confirm Insert?");
                                                if (r == true) {
                                                    txt = "You pressed OK!";
                                                } else {
                                                    e.preventDefault();
                                                }
                                            } else {
                                                e.preventDefault();
                                            }


                                        });



                                        function calc(val)
                                        {

                                            var a = $("#mandays" + val).val() * 1;
                                            var b = $("#headcount" + val).val() * 1;
                                            var c = parseFloat(a * b);
                                            var f = $("#tmandays" + val).val(c);
                                            var d = parseInt($("#fte" + val).val());
                                            var e = c * d;
                                            $("#dtcost" + val).text(numberWithCommas(e));
                                            $("#dfte" + val).text(numberWithCommas(d));
                                            $("#dtmandays" + val).text(numberWithCommas(c));

                                            $("#tcost" + val).val(e);
                                            var pc = "<?= $pj->project_cost ?>";

                                            var a = 0;
                                            $(".tcosts").each(function () {

                                                a = a + $(this).val() * 1;

                                            });

                                            var c = a + pc * 1;

                                            $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

                                        }

                                        function select_fte(val)
                                        {

                                            $("#fte" + val).val($("#select" + val + "  option:selected").attr("dfte"));

                                            calc(val);
                                        }

                                        function addnewrow()
                                        {
                                            var theId = $("#expense_table tr").length + 1;
                                            $('#lastrow').before('<tr id=row' + theId + '><td><select name="select[]" id="select' + theId + '" required class="selecta form-control"   onchange="select_fte(' + theId + ')"  style="height:30px; font-size:12px;  width:140px" ></select></td><td><input type="text"  required   name="mandays[]" id="mandays' + theId + '"  onblur="calc(' + theId + ')" class="input-sm input-xs  form-control"/></td><td><input type="text" name="headcount[]"  required  onblur="calc(' + theId + ')"  id="headcount' + theId + '"  class="input-sm  input-xs   form-control"/></td><td><input type="hidden" name="tmandays[]" id="tmandays' + theId + '" class="input-sm   input-xs  form-control" /><span id="dtmandays' + theId + '"></span></td><td><input type="hidden" name="fte[]" id="fte' + theId + '" class="input-sm   input-xs  form-control" /><span id="dfte' + theId + '"></span></td><td><input type="hidden" name="tcost[]" class="tcosts input-sm input-xs  form-control" id="tcost' + theId + '" /><span id="dtcost' + theId + '"></span></td><td><i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="remrow(\'row' + theId + '\')"></i></td></tr>');

                                            $('#select1 option').clone().appendTo('#select' + theId);
                                            $('#select' + theId).select2(
                                                    {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true}
                                            );
                                            $('.selecta').select2(
                                                    {

                                                        placeholder: "Please Select",
                                                        allowClear: false, dropdownAutoWidth: true
                                                    });

                                            $('#formaly').parsley('addItem', '#select' + theId);
                                            $('#formaly').parsley('addItem', '#mandays' + theId);
                                            $('#formaly').parsley('addItem', '#headcount' + theId);

                                        }

                                        function remrow(val)
                                        {
                                            $("#" + val).hide('fast', function () {
                                                $("#" + val).remove();
                                            });
                                            setTimeout(function () {



                                            }, 500);
                                        }
                                        $(document).ready(function () {
                                            /* $('#projects_milestone_end_date,#projects_milestone_start_date').datepicker().on('changeDate', function(){
                                             $('.datepicker').hide();
                                             $(this).removeClass('parsley-error');
                                             var aid = $(this).attr("data-parsley-id");
                                             $('#parsley-id-'+aid).remove();
                                             });
                                             */
                                            $('#task_pool').select2(
                                                    {
                                                        formatResult: format,
                                                        formatSelection: format,
                                                        placeholder: "Please Select",
                                                        allowClear: false, dropdownAutoWidth: true
                                                    }


                                            );
                                        });

                                        function format(issue) {

                                            $('[data-toggle="tooltip"]').tooltip();


                                            var originalOption = issue.element;
                                            return "<div  data-toggle='tooltip' data-placement='top' title='" + $(originalOption).data('title') + "'>" + issue.text + "</div>";
                                        }

                                        function updateApprovalstatus(docid, rowid, rolename, mrowid, act, userid, finalapprover, initiator, type) {

                                            var status = $('input[name=approval_status_status]:checked').val();
                                            var remarks = $('#asr').val();
//                                            alert(status);
//                                            alert(remarks);
                                            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/updateApprovalstatus') ?>", {
                                                status: status,
                                                remarks: remarks,
                                                rowid: rowid,
                                                rolename: rolename,
                                                docid: docid,
                                                mrowid: mrowid,
                                                act: act,
                                                userid: userid,
                                                finalapprover: finalapprover,
                                                initiator: initiator,
                                                type: type
                                            }, function (data) {
                                                console.log(data);
                                            });


                                            $(document).ajaxStop(function () {
                                                window.location = '<?php echo base_url('index.php/admin/projectmgmt/projects/milestone_approval'); ?>';
                                            });

                                        }

                                        $('#label_yes, #label_no').click(function () {
                                            if ($(this).hasClass('active')) {
                                                return false;
                                            }
                                        });

                                        function getValue() {
// $("#app_val").val($('input[name="approval_status_status"]:checked').val());
                                            alert("Value :" + $('input[name="approval_status_status"]:checked').val());
                                        }

</script> 
