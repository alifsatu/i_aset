

<link rel="stylesheet" href="<?php echo Template::theme_url('css/bootstrap.css'); ?>">
<link rel="stylesheet" href="<?php echo Template::theme_url('css/app.css'); ?>">
<link rel="stylesheet" href="<?php echo Template::theme_url('css/font-awesome.min.css'); ?>">
<link rel="stylesheet" href="<?php echo Template::theme_url('css/font.css'); ?>">
<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
    ?>

    <div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('projects_create_error') ?></h4>
        <?php echo $validation_errors; ?> </div>
    <?php
endif;

if (isset($projects)) {
    $projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';
?>
<style>
    .pad { padding:10px 15px; }
    .popover { width:300px; }
    .tooltip {
        position: fixed; 
    }
    .resimg {
        max-width:900px;
        height: auto;
        -ms-object-fit: cover;
        -moz-object-fit: cover;
        -o-object-fit: cover;
        -webkit-object-fit: cover;
        object-fit: cover;
        overflow: hidden;
    }
</style>

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

    <div class="panel-group m-b" id="accordion2">

        <div class="panel panel-default ">
            <div class="panel-heading clearfix ">
                <?php
                $rolename = $this->auth->role_name_by_id($this->auth->role_id());
                $pj = $this->db->query("select * from intg_projects where id = '" . $this->uri->segment(5) . "'  and (`initiator` = " . $this->auth->user_id() . " OR 
   FIND_IN_SET( '" . $this->auth->user_id() . "',coworker) OR
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers) 
	OR FIND_IN_SET('$rolename','CEO,Finance Team'))")->row();
                ?>
                <div class="row  clearfix">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <span class=""><strong>Project: </strong>
                            <?= ucfirst($pj->project_name) ?></span>
                        <br>
                        <span class=""><strong>Project Costs: </strong>
                            <span id="origpc">RM <?= number_format($pj->project_cost, 2) ?></span>
                        </span>
                        
                    </div>

                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                        
                    </div>
                </div>
            </div>

            <div id="project" class="panel-collapse in" style="height: auto;">
                <div class="panel-body">


                    <!-- ROW 1 -->
                    <div class="form-group  clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>Project Code</strong></header>
                            <div class="pad">
                                <?= $pj->prefix_io_number ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>SBU</strong></header>
                            <div class="controls pad">
                                <?= $this->db->query("select sbu_name from intg_sbu where id ='" . $pj->sbu_id . "'")->row()->sbu_name; ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>Cost Centre</strong></header>
                            <div class="controls pad">
                                <?= $this->db->query("select cost_centre from intg_cost_centre where id ='" . $pj->cost_centre_id . "'")->row()->cost_centre; ?>
                            </div>
                        </div>
                    </div>

                    <!-- ROW 2 -->
                    <div class="form-group  clearfix">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>Duration in Days</strong></header>
                            <div class="controls pad">
                                <?= $pj->total_days ?>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>Programme</strong></header>
                            <div class="controls pad">
                                <?= $this->db->query("select program_name from intg_programs where id ='" . $pj->program_id . "'")->row()->program_name; ?>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                            <header class="panel-heading bg-light  no-border"><strong>Created By</strong></header>

                            <div class="controls pad">
                                <?= $this->auth->display_name_by_id($pj->created_by) ?>
                            </div>
                        </div>


                    </div>

                    <!-- ROW 3 -->
                    <div class="form-group  clearfix">
                        
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <header class="panel-heading bg-light  no-border"><strong>Co-Worker</strong></header>
                            <div class="controls pad">
                                <?php
                                $user_level_list = get_user_level_list();
                                $us = $this->db->query("select display_name,user_level_id from intg_users where id IN ( " . $pj->coworker . ")")->result();
                                foreach ($us as $up) {
                                    
                                    $cw =  $up->display_name.' (';
                                    $cw .= $up->user_level_id != '' ? $user_level_list[$up->user_level_id] : 'N/A';
                                    $cw .=  '), ' ;
                                    $upa[] = $cw;
                                }
                                echo implode("&nbsp;", $upa);
                                ?>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group  clearfix">

                        

                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <header class="panel-heading bg-light  no-border"><strong>Start Date </strong>(dd/mm/yyyy) </header>

                            <div class="controls pad">
                                <?= date("d/m/Y", strtotime($pj->project_start_date)) ?>
                            </div>
                        </div>

                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <header class="panel-heading bg-light no-border"><strong>End Date</strong> (dd/mm/yyyy)</header>

                            <div class="controls pad">
                                <?= date("d/m/Y", strtotime($pj->project_end_date)) ?>
                            </div>
                        </div>


<!--                    </div>

                     ROW 4 
                    <div class="form-group  clearfix">-->

                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <header class="panel-heading bg-light no-border"><strong>Project Color</strong></header>
                            <div class="controls pad">
                                <div style="background-color: <?= $pj->projects_color ?>;  width:80px; text-align:center">&nbsp;</div>
                            </div>
                        </div>

                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                            <header class="panel-heading bg-light  no-border"><strong>Attachment</strong></header>
                            <div class="controls pad">
                                <?php
                                if ($pj->attachment != "") {
                                    $att = explode(":", $pj->attachment);
                                    echo $att[0];
                                }
                                ?>

                            </div>
                        </div>
                    </div>


                    <div class="form-group  clearfix">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                            <header class="panel-heading bg-light   no-border"><strong>Justification of Project</strong></header>
                            <div class="controls pad" style="text-align:justify">
                                <?= $pj->justification ?>
                            </div>

                        </div>
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <header class="panel-heading bg-light  no-border"><strong>Approvers Hierarchy</strong></header>
                            <div class="controls pad">
                                <!------app list-->
                                <?php
                                $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="40" and bas.approval_status_mrowid = "' . $pj->id . '" ORDER BY bas.id asc');
                                $rowreject = $queryappr->row();
                                ?>


                                <h4><?php echo lang('quote_approvers_hierarchy') ?></h4>
                                <table width='100%' class='table table-bordered' id="approver_table">
                                    <tr>
                                        <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                                        <td><strong><?php echo lang('quote_role') ?></strong></td>
                                        <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                                        <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                                        <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                                    </tr>
                                    <?php
                                    foreach ($queryappr->result() as $rowappr) {

                                        switch ($rowappr->approvers_status) {
                                            case "No" : $status = " <span class='badge bg-warning m-l text-xs'>Pending</span>";
                                                break;
                                            case "Yes" : $status = " <span class='badge bg-success m-l text-xs'>Approved</span>";
                                                break;
                                            case "Reject" : $status = " <span class='badge bg-important m-l text-xs'>Rejected</span>";
                                                break;
                                        }
                                        ?>
                                        <tr>
                                            <td><?= $rowappr->display_name ?></td>
                                            <td><?= $rowappr->role_name ?>
                                            <td><?php
                                                if (($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No")) {

                                                    echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                                                } else {
                                                    echo $status;
                                                }
                                                ?></td>
                                            <td><?= $rowappr->approvers_remarks ?></td>
                                            <td><?php
                                                if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                                    echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                                                }
                                                ?></td>
                                        </tr>
                                    <?php }
                                    ?>
                                    <tr>   
                                        <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>

                                    </tr>
                                </table>

                                <!------app list-->
                            </div>

                        </div>
                    </div>


                    <div class="form-group  clearfix">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <header class="panel-heading bg-light  no-border"><strong>Introduction</strong></header>
                            <div class="controls pad">
                                <?= $pj->introduction ?>
                            </div>
                        </div>

                    </div>

                    <div class="form-group  clearfix">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <header class="panel-heading bg-light no-border"><strong>Cost Summary (RM) </strong></header>
                            <div class="controls pad">
                                <?php $ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '" . $this->uri->segment(5) . "'")->result(); ?>
                                <table class="table table-striped m-b-none text-sm">
                                    <tr>
                                        <td width="120">&nbsp;</td>
                                        <?php
                                        
                                        $all_ms_grand_total = array();
                                        $no = 1;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><b>
                                                    Milestone <?= $no ?>
                                                </b></td>

                                            <?php
                                            $no++;
                                        }
                                        ?>
                                        <td><strong>Total (RM)</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Material Cost</strong></td>
                                        <?php
                                        $mstotal = 0;
                                        foreach ($ms as $cs) {
                                            $total{$cs->milestone_id} = $cs->material_cost + $cs->new_capex + $cs->outsourcing + $cs->others;
                                            ?>
                                            <td><?= number_format($cs->material_cost, 2) ?></td>

                                            <?php
                                            $mstotal = $mstotal + $cs->material_cost;
                                        }
                                        ?>
                                        <td><?php echo number_format($mstotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>New CAPEX</strong></td>
                                        <?php
                                        $nptotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?= number_format($cs->new_capex, 2) ?></td>

                                            <?php
                                            $nptotal = $nptotal + $cs->new_capex;
                                        }
                                        ?>
                                        <td><?php echo number_format($nptotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Outsourcing</strong></td>
                                        <?php
                                        $ostotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?= number_format($cs->outsourcing, 2) ?></td>

                                            <?php
                                            $ostotal = $ostotal + $cs->outsourcing;
                                        }
                                        ?>
                                        <td><?php echo number_format($ostotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Others</strong></td>
                                        <?php
                                        $ottotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?php echo number_format($cs->others, 2); ?></td>

                                            <?php
                                            $ottotal = $ottotal + $cs->others;
                                        }
                                        ?>
                                        <td><?php echo number_format($ottotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td  width="120"><strong>Time Cost</strong></td>
                                        <?php
                                        $timetotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?php
                                                $ts{$cs->milestone_id} = $this->db->query("select sum(time_cost) as timecost from intg_milestones_users where milestones_id = '" . $cs->milestone_id . "'")->row()->timecost;
                                                echo number_format($ts{$cs->milestone_id}, 2);
                                                ?></td>

                                            <?php
                                            $timetotal = $timetotal + $ts{$cs->milestone_id};
                                        }
                                        ?>
                                        <td><?php echo number_format($timetotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td  width="120"><strong>Total (RM)</strong></td>
                                        <?php
                                        $gtotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><strong><?php
                                                    echo number_format($total{$cs->milestone_id} + $ts{$cs->milestone_id}, 2);
                                                    ?>
                                                </strong></td>

                                            <?php
                                            $gtotal = $gtotal + $total{$cs->milestone_id} + $ts{$cs->milestone_id};
                                            $all_ms_grand_total[$cs->milestone_id] = number_format($gtotal, 2);
                                        }
                                        ?>
                                        <td><strong><?php echo number_format($gtotal, 2); ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <?php
        $noms = 1;
        $is_milestone_exist = 0;
        $ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '" . $this->uri->segment(5) . "'")->result();


        foreach ($ms as $ml) {

            $v = $this->db->query("select max(m.version_no) as maxver from intg_milestones m where m.parent_Rid= '" . $ml->milestone_id . "' ")->row()->maxver;
            $mlid = $ml->milestone_id;

            if ($v > 0) {
//                                         
                $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $ml->milestone_id . "' order by milestone_id desc limit 1")->row();
//                                                    echo "<br>Versioned id ".$ml->milestone_id."| Versioned status".$ml->status."| Versioned Parent m name".$ml->milestone_name;
                $check = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->num_rows();
                if ($ml->status == "Reject") {
//                                                        echo "<br>select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1";
//                                                        echo "Check ".$check;
                    if ($check > 0) {
                        $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                    } else {
                        $ml = $this->db->query("select * from intg_milestones where milestone_id = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                    }
                } else {
                    
                }
            }
            ?>
        <br>
            <div class="panel panel-default">
                <div class="panel-heading clearfix"> 
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="" >
                        <b>Milestone<?= $noms ?>: </b><?= ucfirst($ml->milestone_name) ?>
                        <strong class="m-l">Start Date: </strong>
                        <?= date("d/m/Y", strtotime($ml->start_date)) ?>
                        <strong class="m-l">End Date: </strong>
                        <?= date("d/m/Y", strtotime($ml->end_date)) ?>
                    </a>
                    
                    <?php
                    $rc_approval = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $this->auth->user_id() . "  AND project_id = '" . $this->uri->segment(5) . "' ");

                    switch ($ml->status) {
                        case "No" :
                            if ($rc_approval->num_rows() == 0)
                                $status = "<span class='badge bg-warning m-l text-xs'>Pending</span>";
                            else
                                $status = "<span class='badge bg-info m-l text-xs'>Sent For Approval</span>";

                            break;
                        case "Yes" :
                            $status = "<span class='badge bg-primary m-l text-xs'>Approved</span>";
                            break;
                        case "Reject" :
                            $status = "<span class='badge bg-danger m-l text-xs'>Rejected</span>";
                            break;
                    }
                    echo $status;
                    ?>


                    <?php
                        if ($v > 0) {
                            ?>
                                <?php
                                if ($ml->version_no > 0) {
                                    $vno = "<span class='badge bg-info text-xs'>V" . $ml->version_no . "</span> ";
                                } else {
                                    $vno = "<span class='badge bg-info text-xs'>V0</span>";
                                }
                                 echo $vno;
                                     ?>

                        <?php } ?>
                    <br>
                    <span id="" class=""><b>RM</b> <?=$all_ms_grand_total[$ml->milestone_id]?></span> 
                </div>
                    
                    
                <div id="ml<?= $ml->milestone_id ?>" class="panel-collapse in" style="height: auto;">
                    <div class="panel-body text-sm">
                        <div class="col-md-16 col-lg-16 col-sm-16 col-xs-16" id="lineitem_form">
                            <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Resources</header>
                            <section class="panel panel-default">
                                <div class="table-responsive ">
                                    <table class="table table-striped b-t b-light text-sm">
                                        <thead>
                                            <tr>
                                                <th>Level</th>
                                                <th>Man-days</th>
                                                <th>Head Count</th>
                                                <th>Total Man Days</th>
                                                <th>Daily FTE (RM)</th>
                                                <th>Time Cost (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $milestone_cost = 0;
                                            $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='" . $ml->milestone_id . "'")->result();

                                            foreach ($mu as $u) {
                                                ?>
                                                <tr >
                                                    <td><?= $u->user_level ?></td>
                                                    <td><?= $u->man_days ?></td>
                                                    <td><?= $u->head_count ?></td>
                                                    <td><?= $u->total_man_days ?></td>
                                                    <td><?= $u->fte ?></td>
                                                    <td align="right"><?= number_format($u->time_cost, 2) ?></td>
                                                </tr>
                                                <?php
                                                $milestone_cost = $milestone_cost + $u->time_cost;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <span class="mcostdown" rowid="<?= $ml->milestone_id ?>" id="matcost<?= $ml->milestone_id ?>" style="display:none"><?php echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span> </div>
                            </section>
                            <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
                            <section class="panel panel-default">
                                <div class="table-responsive ">
                                    <table class="table table-striped b-t b-light text-sm">
                                        <thead>
                                            <tr>
                                                <th>Material Cost (RM)</th>
                                                <th>New CAPEX (RM)</th>
                                                <th>Outsourcing (RM)</th>
                                                <th>Others (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr >
                                                <td><?= $ml->material_cost ?></td>
                                                <td><?= $ml->new_capex ?></td>
                                                <td><?= $ml->outsourcing ?></td>
                                                <td><?= $ml->others ?></td>
                                            </tr>
                                        </tbody>

                                        <thead>
                                            <tr>
                                                <th>Remarks</th>
                                                <th>Remarks</th>
                                                <th>Remarks</th>
                                                <th>Remarks</th>                       
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr >
                                                <td><?= $ml->mcost_desc ?></td>
                                                <td><?= $ml->ncapex_desc ?></td>
                                                <td><?= $ml->outsourcing_desc ?></td>
                                                <td><?= $ml->others_desc ?></td>

                                            </tr>

                                        </tbody>

                                    </table>
                                </div>
                            </section>
                        </div>
                        <div class="form-group" style="margin-top:10px">
                            <label for="projects_assigned_to" class="control-label"><strong>Task & Description</strong></label>
                            <div class="controls pad">

                                <?php
                                $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '" . $ml->milestone_id . "'")->result();
                                $no = 1;
                                foreach ($tp as $t) {
                                    ?>
                                    <b><?= $no ?>. <?= $t->task_name ?> </b>  <?= $t->description ?> <br>

                                    <?php
                                    $no++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $noms++;
            $is_milestone_exist++;
            $milestone_cost = 0;
        }
        ?>
    </div>
</div> </section>
<!--end of col6--> 

</div>
<!--end of row--> 
<?php if ($pj->attachment != "") { ?>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width:auto">
            <div class="modal-content">	
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"><?= $att[1] ?></h4>
                </div>	 
                <div class="modal-body" align="center">

                    <img src="<?= base_url() ?>uploads/<?= $att[0] ?>" class="resimg"/>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

                </div>
            </div>
        </div>
    </div><?php } ?>
<script>

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(document).ready(function () {
        $(".mcostdown").each(function () {
            var row = $(this).attr("rowid");
            $("#mcostup" + row).html("<b>Milestone Cost:</b> RM " + numberWithCommas(parseFloat($(this).text() * 1).toFixed(2)) + "&nbsp;&nbsp;");
        })

        var pc = "<?= $pj->project_cost ?>";



        window.ParsleyConfig = {
            errorsWrapper: '<div class="pe"></div>',
        };
    });



    function calc(val)
    {

        var a = $("#mandays" + val).val() * 1;
        var b = $("#headcount" + val).val() * 1;
        var c = parseFloat(a * b);
        var f = $("#tmandays" + val).val(c);
        var d = parseInt($("#fte" + val).val());
        var e = c * d;
        $("#dtcost" + val).text(numberWithCommas(e));
        $("#dfte" + val).text(numberWithCommas(d));
        $("#dtmandays" + val).text(numberWithCommas(c));

        $("#tcost" + val).val(e);
        var pc = "<?= $pj->project_cost ?>";

        var a = 0;
        $(".tcosts").each(function () {

            a = a + $(this).val() * 1;

        });

        var c = a + pc * 1;

        $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

    }

    function select_fte(val)
    {

        $("#fte" + val).val($("#select" + val + "  option:selected").attr("dfte"));

        calc(val);
    }

    function format(issue) {

        $('[data-toggle="tooltip"]').tooltip();


        var originalOption = issue.element;
        return "<div  data-toggle='tooltip' data-placement='top' title='" + $(originalOption).data('title') + "'>" + issue.text + "</div>";
    }
</script> 