<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($projects))
{
	$projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';

?>
<div class="row">
	<h3>Projects</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('project_name') ? 'error' : ''; ?>">
				<?php echo form_label('Project Name', 'projects_project_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_name' type='text' name='projects_project_name' maxlength="45" value="<?php echo set_value('projects_project_name', isset($projects['project_name']) ? $projects['project_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'projects_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'projects_description', 'id' => 'projects_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('projects_description', isset($projects['description']) ? $projects['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_id') ? 'error' : ''; ?>">
				<?php echo form_label('Client Id', 'projects_client_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_client_id' type='text' name='projects_client_id' maxlength="11" value="<?php echo set_value('projects_client_id', isset($projects['client_id']) ? $projects['client_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status', 'projects_status', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_status' type='text' name='projects_status' maxlength="45" value="<?php echo set_value('projects_status', isset($projects['status']) ? $projects['status'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_visibility') ? 'error' : ''; ?>">
				<?php echo form_label('Project Visibility', 'projects_project_visibility', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_visibility' type='text' name='projects_project_visibility' maxlength="45" value="<?php echo set_value('projects_project_visibility', isset($projects['project_visibility']) ? $projects['project_visibility'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_visibility'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('allow_comments') ? 'error' : ''; ?>">
				<?php echo form_label('Allow Comments', 'projects_allow_comments', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_allow_comments' type='text' name='projects_allow_comments' maxlength="45" value="<?php echo set_value('projects_allow_comments', isset($projects['allow_comments']) ? $projects['allow_comments'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('allow_comments'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('send_notifications') ? 'error' : ''; ?>">
				<?php echo form_label('Send Notifications', 'projects_send_notifications', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_send_notifications' type='text' name='projects_send_notifications' maxlength="45" value="<?php echo set_value('projects_send_notifications', isset($projects['send_notifications']) ? $projects['send_notifications'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('send_notifications'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('estimated_hours') ? 'error' : ''; ?>">
				<?php echo form_label('Estimated Hours', 'projects_estimated_hours', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_estimated_hours' type='text' name='projects_estimated_hours' maxlength="45" value="<?php echo set_value('projects_estimated_hours', isset($projects['estimated_hours']) ? $projects['estimated_hours'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('estimated_hours'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_notes') ? 'error' : ''; ?>">
				<?php echo form_label('Project Notes', 'projects_project_notes', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'projects_project_notes', 'id' => 'projects_project_notes', 'rows' => '5', 'cols' => '80', 'value' => set_value('projects_project_notes', isset($projects['project_notes']) ? $projects['project_notes'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('project_notes'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_start_date') ? 'error' : ''; ?>">
				<?php echo form_label('Project Start Date', 'projects_project_start_date', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_start_date' type='text' name='projects_project_start_date' maxlength="45" value="<?php echo set_value('projects_project_start_date', isset($projects['project_start_date']) ? $projects['project_start_date'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_start_date'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_end_date') ? 'error' : ''; ?>">
				<?php echo form_label('Project End Date', 'projects_project_end_date', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_end_date' type='text' name='projects_project_end_date' maxlength="45" value="<?php echo set_value('projects_project_end_date', isset($projects['project_end_date']) ? $projects['project_end_date'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_end_date'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_cost') ? 'error' : ''; ?>">
				<?php echo form_label('Project Cost', 'projects_project_cost', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_cost' type='text' name='projects_project_cost' maxlength="45" value="<?php echo set_value('projects_project_cost', isset($projects['project_cost']) ? $projects['project_cost'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_cost'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('project_tag') ? 'error' : ''; ?>">
				<?php echo form_label('Project Tag', 'projects_project_tag', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='projects_project_tag' type='text' name='projects_project_tag' maxlength="45" value="<?php echo set_value('projects_project_tag', isset($projects['project_tag']) ? $projects['project_tag'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_tag'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/content/projects', lang('projects_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Projects.Content.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('projects_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>