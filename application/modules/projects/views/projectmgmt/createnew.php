<style>
    span.simplecolorpicker.inline {
        display: table-row !important;
    }

    .tooltip-inner  {
        max-width: 500px; height:auto;
    }
    .placeholderText {
	color: #777;
}
</style>
<?php

$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
    ?>
    <div class="alert alert-danger fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('projects_create_error') ?></h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projects)) {
    $projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';
?>


<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold"><?php echo lang('projects') ?></header>
            <div class="panel-body">


                <?php echo form_open_multipart($this->uri->uri_string(), 'id="forma"'); ?>
                <input type="hidden" name="rejected_project_id" value="<?=$id?>">
                <div class="form-group  clearfix">

                    <div class="col-sm-3">
                        <label><?php echo lang('projects_name'); ?> *</label>
                        <div class='controls'>
                            <input class="input-sm input-s form-control" id='projects_project_name' type='text' name='projects_project_name' maxlength="255" value="<?php echo set_value('projects_project_name', isset($projects['project_name']) ? $projects['project_name'] : ''); ?>" data-required="true" />
                        </div>
                    </div>

                    <?php
// Change the values in this array to populate your dropdown as required

                    $prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
                    ?>
                    <div class="col-sm-3">
                        <label>SBU *</label>
                        <div class='controls'>
                            <select name="projects_sbu" id="projects_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
                                <option></option>
                                <?php
                                foreach ($prgm as $pn) {
                                    if ($projects['sbu_id'] == $pn->id) {
                                        ?>
                                        <option selected="selected" value=<?= $pn->id ?>><b><?= $pn->sbu_name ?></b></option><?php } else { ?>
                                        <option value=<?= $pn->id ?>><b><?= $pn->sbu_name ?></b></option><?php
                                    }
                                }
                                ?>
                            </select> </div>
                    </div>
                    <?php
                    // Change the values in this array to populate your dropdown as required

                    $cc = $this->db->query('SELECT * FROM  intg_cost_centre WHERE deleted = 0 ORDER BY cost_centre ')->result();
                    ?>
                    <div class="col-sm-3">
                        <label>Cost Centre *</label>
                        <div class='controls'>
                            <select name="projects_cost_centre" id="projects_cost_centre" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
                                <option></option>
                                <?php
                                foreach ($cc as $c) {
                                    if ($projects['cost_centre_id'] == $c->id) {
                                        ?>
                                        <option selected value=<?= $c->id ?>><b><?= $c->cost_centre ?></b></option><?php } else { ?>
                                        <option  value=<?= $c->id ?>><b><?= $c->cost_centre ?></b></option><?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php
                    // Change the values in this array to populate your dropdown as required

                    $prgm = $this->db->query('SELECT * FROM  intg_programs WHERE deleted = 0 ORDER BY program_name')->result();
                    ?>

                    <div class="col-sm-3">
                        <label>Programme *</label>
                        <div class='controls'>
                            <select name="projects_Programme" id="projects_Programme" class="selecta form-control"  style="height:30px;width:200px; font-size:12px"  data-required="true" >
                                <option></option>

                                <?php
                                foreach ($prgm as $p) {
                                    if ($projects['program_id'] == $p->id) {
                                        ?>
                                        <option selected value="<?= $p->id ?>"><b><?= $p->program_name ?></b></option><?php } else { ?>
                                        <option value=<?= $p->id ?>><b><?= $p->program_name ?></b></option><?php
                                    }
                                }
                                ?>
                            </select>  
                        </div> 
                    </div>

                </div>

                <div class="form-group  clearfix">
                    <div class="col-sm-3">
                        <label>Justification of Project *</label>
                        <div class='controls'>
                            <?php echo form_textarea(array('class' => 'form-control input-sm input-s expand', 'name' => 'projects_justification', 'data-required=' => 'true', 'id' => 'projects_justification', 'rows' => '1', 'cols' => '40', 'value' => $projects['justification'])); ?>

                        </div></div>

                    <div class="col-sm-3">
                        <label>Co-Worker *</label>
                        <div class='controls'>
                            <select multiple name="projects_assigned_to[]" id="projects_assigned_to" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
                                <?php
                                $user_level_list = get_user_level_list();
                                foreach ($users as $user) :
                                    if ($user->id != $current_user->id) {
                                        if (in_array($user->id, explode(",", $projects['coworker']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->display_name; ?> (<?php echo $user->user_level_id != '' ? $user_level_list[$user->user_level_id] : 'N/A'; ?>)</option>
                                        <?php } else { ?>
                                            <option value="<?= $user->id ?>"><?= $user->display_name; ?> ( <?php echo $user->user_level_id != '' ? $user_level_list[$user->user_level_id] : 'N/A'; ?> )</option>
                                            <?php
                                        }
                                    }
                                    ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Duration in Days *</label>
                        <div class='controls'>
                            <input   class="input-sm input-s form-control" id='projects_duration_days' type='number' name='projects_duration_days'  value="<?php echo $projects['total_days']; ?>"  data-required="true" />
                        </div>  
                    </div>
                    <div class="col-sm-3">
                        <label>Project Color * </label>
                        <div class='controls'>
                            <select name="projects_color" id="projects_color" class="color" style="display:table-row">
                                           <!-- <select  name="projects_color" id="projects_color" class="form-control color selecta"  style="height:30px; font-size:12px; width:200px;display:table-row"  data-required="true" >-->

                                <option <?php e($projects['projects_color'] == '#7bd148' ? 'selected' : '') ?> value="#7bd148">Green</option>
                                <option  <?php e($projects['projects_color'] == '#5484ed' ? 'selected' : '') ?> value="#5484ed">Bold blue</option>
                                <option  <?php e($projects['projects_color'] == '#a4bdfc' ? 'selected' : '') ?> value="#a4bdfc">Blue</option>
                                <option  <?php e($projects['projects_color'] == '#46d6db' ? 'selected' : '') ?> value="#46d6db">Turquoise</option>
                                <option  <?php e($projects['projects_color'] == '#7ae7bf' ? 'selected' : '') ?> value="#7ae7bf">Light green</option>
                                <option  <?php e($projects['projects_color'] == '#51b749' ? 'selected' : '') ?> value="#51b749">Bold green</option>
                                <option  <?php e($projects['projects_color'] == '#fbd75b' ? 'selected' : '') ?> value="#fbd75b">Yellow</option>
                                <option  <?php e($projects['projects_color'] == '#ffb878' ? 'selected' : '') ?> value="#ffb878" >Orange</option>
                                <option  <?php e($projects['projects_color'] == '#ff887c' ? 'selected' : '') ?> value="#ff887c">Red</option>
                                <option  <?php e($projects['projects_color'] == '#dc2127' ? 'selected' : '') ?> value="#dc2127">Bold red</option>
                                <option  <?php e($projects['projects_color'] == '#dbadff' ? 'selected' : '') ?> value="#dbadff">Purple</option>
                                <!--<option value="#e1e1e1">Gray</option>-->
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group  clearfix">
                    <div class="col-sm-3">
                        <label>Start Date *</label>
                        <div class='controls'>
                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projects_project_start_date' type='text' name='projects_project_start_date'  value="<?php echo isset($projects['project_start_date']) ? date("d-m-Y", strtotime($projects['project_start_date'])) : ''; ?>"  data-required="true" readonly="readonly"/>	
                        </div> 
                    </div>
                    <div class="col-sm-3">
                        <label>End Date *</label>
                        <div class='controls'>
                            <input  class="datepicker-input form-control input-sm input-s"  id='projects_project_end_date'  data-date-format="dd-mm-yyyy"  type='text' name='projects_project_end_date'  value="<?php echo isset($projects['project_end_date']) ? date("d-m-Y", strtotime($projects['project_end_date'])) : ''; ?>"  data-required="true" readonly="readonly"/>
                        </div></div>
                </div>
                <div class="form-group  clearfix">

                    <div class="col-sm-10">
                        <label>Introduction</label>
                        <div class='controls'>

                            <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                    <div class="dropdown-menu">
                                        <div class="input-group m-l-xs m-r-xs">
                                            <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-default btn-sm" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-default btn-sm" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                    <a class="btn btn-default btn-sm" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                </div>
                            </div>
                            <div id="editor" placeholder="Write your project introduction here" class="form-control" style="overflow:scroll;height:150px;max-height:150px; font-size:12px"><?php e(isset($projects['introduction']) ? $projects['introduction'] : 'Write your project introduction here...') ?></div>
                        </div>
                    </div>
                </div>
                <div class="form-group  clearfix">
                    <div class="form-actions col-sm-5"> <div class='controls' style="margin:5px 5px"><input type="file" name="files[]" multiple="multiple" /></div></div></div>

                <div class="form-group  clearfix">
                    <div class="form-actions col-sm-5">
                        <input type="hidden" name="created_by" id="created_by" />
                        <input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                        <input type="hidden" name="introduction" />
                        <input type="submit" id="submit" name="draft" class="btn btn-warning" value="Save as Draft" onclick="return confirm('Confirm Save as Draft');"  />
                        &nbsp;&nbsp;
                        <input type="submit" id="submit2"  name="save" class="btn btn-primary" value="Save & Create Milestone(s)" onclick="return confirm('Confirm Save & Create Milestone(s)');"  />
                    </div>
                </div>

                <?php echo form_close(); ?>


            </div>
        </section>
    </div><!--end of col6-->


</div> <!--end of row-->

<script src="<?php echo Template::theme_url('js/parsleyold/parsley.min.js'); ?>" cache="false"></script> 
<script src="<?= Template::theme_url('js/wysiwyg/jquery.hotkeys.js') ?>"></script>
<script src="<?= Template::theme_url('js/wysiwyg/bootstrap-wysiwyg.js') ?>"></script>
<script src="<?= Template::theme_url('js/wysiwyg/demo.js') ?>"></script>
<script>

                            $("#submit,#submit2").click(function (e) {
                                if (!$("#forma").parsley('validate')) {
                                    e.preventDefault();
                                }
                            });


                            $("[name=save]").click(function () {
                                $("[name=introduction]").val($("#editor").cleanHtml());
                            });

                            $("[name=draft]").click(function () {
                                $("[name=introduction]").val($("#editor").cleanHtml());
                            });

                            $(document).ready(function () {
                                $('.color').simplecolorpicker('selectColor', '<?= $this->input->post('projects_color') ?>');

                                /* $('#projects_project_start_date,#projects_project_end_date').datepicker().on('changeDate', function(){
                                 $('.datepicker').hide();
                                 $(this).removeClass('parsley-error');
                                 var aid = $(this).attr("data-parsley-id");
                                 $('#parsley-id-'+aid).remove();
                                 });
                                 */

                                var nowTemp = new Date();
                                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

                                var checkin = $('#projects_project_start_date').datepicker({
                                    onRender: function (date) {
                                        return date.valueOf() < now.valueOf() ? 'disabled' : '';

                                    }
                                }).on('changeDate', function (ev) {
                                    if (ev.date.valueOf() > checkout.date.valueOf()) {
                                        var newDate = new Date(ev.date)
                                        newDate.setDate(newDate.getDate() + 1);
                                        checkout.setValue(newDate);

                                        $(this).removeClass('parsley-error');
                                        var aid = $(this).attr("data-parsley-id");
                                        $('#parsley-id-' + aid).remove();
                                    }
                                    checkin.hide();
                                    $('#projects_project_end_date')[0].focus();
                                }).data('datepicker');

                                var checkout = $('#projects_project_end_date').datepicker({
                                    onRender: function (date) {
                                        return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
                                    }
                                }).on('changeDate', function (ev) {
                                    checkout.hide();
                                    $(this).removeClass('parsley-error');
                                    var aid = $(this).attr("data-parsley-id");
                                    $('#parsley-id-' + aid).remove();
                                }).data('datepicker');

                            });



</script>
