
<script>
$(document).ready(function() {
	
	
function gd(year, month, day) {
    return new Date(year, month -1, day + 1).getTime();
}
	
	
	
 $('#dateto').on('changeDate', function(){
          $('.datepicker').hide();
		  loadgraph();
		});
		
 });
 

		
	var defaultE = [ [gd(2014,11,12),8] ,[gd(2014,11,13),24],[gd(2014,11,14),40],[gd(2014,11,15),84],[gd(2014,11,16),90],[gd(2014,11,17),92],[gd(2014,11,18),100]];	
		var defaultA = [  [gd(2014,11,12),4] ,[gd(2014,11,13),28],[gd(2014,11,14),48],[gd(2014,11,15),84],[gd(2014,11,16),88],[gd(2014,11,17),94],[gd(2014,11,18),100]];	
		
			
function gd(year, month, day) {
    return new Date(year, month -1, day + 1).getTime();
}
	
</script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
		
		var task1E =[];
		var task1A =[];
		var task2E =[];
		var task2A =[];
		var task3E =[];
		var task3A =[];
		
		
		<? if ($_POST['taska']==1) { ?>
		 var defaultE = [ [gd(2014,11,12),20] ,[gd(2014,11,13),30],[gd(2014,11,14),50],[gd(2014,11,15),100]];	
		 var defaultA = [ [gd(2014,11,12),10] ,[gd(2014,11,13),30],[gd(2014,11,14),60],[gd(2014,11,15),100]];
		
		<? } ?>
		
		<? if ($_POST['taska']==2) { ?>
		var defaultE = [ [gd(2014,11,13),30] ,[gd(2014,11,14),50],[gd(2014,11,15),100]];	
		var defaultA = [ [gd(2014,11,13),40] ,[gd(2014,11,14),60],[gd(2014,11,15),100]];
		
		<? } ?>
		
		<? if ($_POST['taska']==3) { ?>
		var defaultE = [ [gd(2014,11,15),20],[gd(2014,11,16),50],[gd(2014,11,17),60],[gd(2014,11,18),100]];	
		var defaultA = [ [gd(2014,11,15),20],[gd(2014,11,16),40],[gd(2014,11,17),70],[gd(2014,11,18),100]];
		
		<? } ?>	
		
		
		
	
		
			
function gd(year, month, day) {
    return new Date(year, month -1, day + 1).getTime();
}
		
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'PROJECT PROGRESS'
        },
        subtitle: {
           /* text: 'Irregular time data in Highcharts JS'*/
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
              
			    month: "%d/%m",
               
            },
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'PERCENTAGE (%)',
				 
            },
			
            min:0,max: 100
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:1f} %'
        },

        series: [{
            name: 'Expected',
			 lineWidth: 2,
			 color:"#f00",
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: defaultE

            
        }, {
            name: 'Achieved',
            data: defaultA
      
        }]
    });
});



function loadgraph()
{
 
$('#container').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/projects/HCgraph') ?>", {dateto:$('#dateto').val(),taska:$("#taska").val()}, function(data){$('#container').html(data); } )

	
}

		</script>


 
<div class="row">
                 <section class="panel panel-default"><header class="panel-heading font-bold clearfix"> <div class="col-md-3">PROJECT PROGRESS</div>
                  <select id="taska" class="form-control col-md-3" style="width:100px; margin-right:10px; height:30px" onchange="loadgraph()">
                  <option value="default">Default</option>
             <option value="1">Task 1</option>
             <option value="2">Task 2</option>
             <option value="3">Task 3</option>
           
             </select>
             <div class="input-group  col-md-4">
            
                          <span class="input-group-addon btn-success">Range From</span>
                          <input type="text" class="input-sm input-xs datepicker-input form-control" id="datefrom" placeholder="date">
                        </div>
                        <div class="input-group  col-md-3">
                          <span class="input-group-addon btn-success">To</span>
                          <input type="text" class="input-sm input-xs datepicker-input form-control" id="dateto" placeholder="date">
                        </div>
                        </header>
                    <div class="panel-body"> 
<div id="container"></div>
</div></section></div>
