<script>
    $('#label_yes, #label_no').click(function () {
        if ($(this).hasClass('active')) {
            return false;
        }
    });
</script>

<?php
if ($_POST['type'] != "mile") {

    $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "40" and approval_status_mrowid = "' . $_POST['docid'] . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by)   order by id desc');
    $rowm = $querym->row();
    ?>
    <table width='100%' class='table table-condensed'>
        <tr>
            <td><strong><?php echo lang('quote_approver_action_status')?></strong></td>
            <td>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-sm btn-info active" id="label_yes">
                        <input type="radio" name="approval_status_status" id="option1"  value="Yes"  checked="checked" ><i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_yes')?> 
                    </label>
                    <label class="btn btn-sm btn-danger"  id="label_no">
                        <input type="radio" name="approval_status_status" id="option2" value="Reject"><i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_no')?>
                    </label>		                
                </div>

            </td>   
        </tr>
        <tr>
            <td colspan="2"><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
        </tr>
        <tr>
            <td colspan="2"><textarea name="approval_status_remarks" style="border:#ccc solid 1px" cols="50" rows="2" id="asr"></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <input name="updateappr" type="button" value="Submit" class="btn" onclick="updateApprovalstatus('<?= $_POST['docid'] ?>', '<?= $rowm->id ?>', '<?= $rowm->rolename ?>', '<?= $rowm->approval_status_mrowid ?>', 'qa', '<?= $this->auth->user_id() ?>', '<?= $_POST['finalapprover'] ?>', '<?= $_POST['initiator'] ?>')" /></td>
        </tr>
    </table>

    <?php
} else {
    ?> 
    <?php
    $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "42" and approval_status_mrowid = "' . $_POST['docid'] . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by)   order by id desc');
    $rowm = $querym->row();
    ?>
    <table width='100%' class='table table-condensed'>
        <tr>
            <td><strong><?php echo lang('quote_approver_action_status')?></strong></td>
            <td>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-sm btn-info active">
                        <input type="radio" name="approval_status_status" id="option1"  value="Yes"  checked="checked" ><i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_yes')?> 
                    </label>
                    <label class="btn btn-sm btn-danger">
                        <input type="radio" name="approval_status_status" id="option2" value="Reject"><i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_no')?>
                    </label>		                
                </div>

            </td>   
        </tr>
        <tr>
            <td colspan="2"><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
        </tr>
        <tr>
            <td colspan="2"><textarea name="approval_status_remarks" style="border:#ccc solid 1px" cols="50" rows="2" id="asr"></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <input name="updateappr" type="button" value="Submit" class="btn" onclick="updateApprovalstatus('<?= $_POST['docid'] ?>', '<?= $rowm->id ?>', '<?= $rowm->rolename ?>', '<?= $rowm->approval_status_mrowid ?>', 'qa', '<?= $this->auth->user_id() ?>', '<?= $_POST['finalapprover'] ?>', '<?= $_POST['initiator'] ?>', '<?= $_POST['type'] ?>')" /></td>
        </tr>
    </table>
<?php } ?>       
