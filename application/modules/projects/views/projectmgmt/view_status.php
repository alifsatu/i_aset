
<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
$user_name_list = get_user_name_list();
if ($validation_errors) :
    ?>

    <div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading"><?php echo lang('projects_create_error') ?></h4>
        <?php echo $validation_errors; ?> </div>
    <?php
endif;

if (isset($projects)) {
    $projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';
?>
<style>
    .pad { padding:10px 15px; }
    .popover { width:300px; }
    .tooltip {
        position: fixed; 
    }
    .resimg {
        max-width:900px;
        height: auto;
        -ms-object-fit: cover;
        -moz-object-fit: cover;
        -o-object-fit: cover;
        -webkit-object-fit: cover;
        object-fit: cover;
        overflow: hidden;
    }
</style>

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

    <div class="panel-group m-b" id="accordion2">

        <div class="panel panel-default ">
            <div class="panel-heading clearfix "><?php echo form_open($this->uri->uri_string(), 'id="formaly" data-parsley-validate'); ?>
                <?php
                $rolename = $this->auth->role_name_by_id($this->auth->role_id());
                $pj = $this->db->query("select * from intg_projects where id = '" . $this->uri->segment(5) . "'  and (`initiator` = " . $this->auth->user_id() . " OR 
   FIND_IN_SET( '" . $this->auth->user_id() . "',coworker) OR
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers) 
	OR FIND_IN_SET('$rolename','CEO,Finance Team'))")->row();
                ?>


                <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="" href="#project"><strong>Project: </strong>
                    <?= ucfirst($pj->project_name) ?>



                </a>
                <?php echo send_email_button('accordion2'); ?>
                <span class="pull-right"><strong>Project Costs: </strong><span id="origpc">RM <?= number_format($pj->project_cost, 2) ?></span></span>
            </div>

            <?php project_info($pj); ?>

        </div>

        <?php
        $noms = 1;
        $is_milestone_exist = 0;
                            
        $ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '" . $this->uri->segment(5) . "' and status != 'Reject' order by start_date  ")->result();


        foreach ($ms as $ml) {

            $v = $this->db->query("select max(m.version_no) as maxver from intg_milestones m where m.parent_Rid= '" . $ml->milestone_id . "' ")->row()->maxver;
            $mlid = $ml->milestone_id;

            if ($v > 0) {
//                                         
                $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $ml->milestone_id . "' order by milestone_id desc limit 1")->row();
//                                                    echo "<br>Versioned id ".$ml->milestone_id."| Versioned status".$ml->status."| Versioned Parent m name".$ml->milestone_name;
                $check = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->num_rows();
                if ($ml->status == "Reject") {
//                                                        echo "<br>select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1";
//                                                        echo "Check ".$check;
                    if ($check > 0) {
                        $ml = $this->db->query("select * from intg_milestones where parent_Rid = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                    } else {
                        $ml = $this->db->query("select * from intg_milestones where milestone_id = '" . $mlid . "' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                    }
                } else {
                    
                }
            }
            ?>
            <div class="panel panel-default html2pdf__page-break">
                <div class="panel-heading clearfix"> <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="" href="#ml<?= $ml->milestone_id ?>"><strong>Milestone
                            <?= $noms ?>
                            : </strong>
                        <?= ucfirst($ml->milestone_name) ?>
                        <strong class="m-l">Start Date: </strong>
                        <?= date("d/m/Y", strtotime($ml->start_date)) ?>
                        <strong class="m-l">End Date: </strong>
                        <?= date("d/m/Y", strtotime($ml->end_date)) ?>
                    </a>
                    <?php
                    $rc_approval = $this->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $this->auth->user_id() . "  AND project_id = '" . $this->uri->segment(5) . "' ");

                    switch ($ml->status) {
                        case "No" :
                            if ($rc_approval->num_rows() == 0)
                                $status = "<span class='badge btn-warning m-l text-xs'>Pending</span>";
                            else
                                $status = "<span class='badge btn-success m-l text-xs'>Sent For Approval</span>";

                            break;
                        case "Yes" :
                            $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                            break;
                        case "Reject" :
                            $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                            break;
                    }
                    echo $status;
                    ?>


                    <ul class="nav nav-pills nav-sm pull-left" style="padding:5px 20px">
                        <?php if ($rc_approval->num_rows() == 0) { ?>
                            <li><a href="javascript:void(0)" class=" pull-right" milestone_id="<?= $ml->milestone_id ?>" project_id="<?= $ml->project_id ?>"></a></li>
                        <?php } ?>
    <li class="hour " data-toggle="popover"  data-content='<textarea name="comments"  cols="30" rows="2" class="form-control expand"><?= $ml->remarks ?></textarea><button class="btn btn-xs btn-primary cmpmark" style="margin-top:5px" rowid="<?= $ml->milestone_id ?>">Submit</button>' data-html="true" data-placement="bottom" data-original-title="Mark As Complete" data-comments="<?= $ml->remarks ?>"><!--<a href="javascript:void(0)"><i class="fa fa-info"></i></a>--></li>
                        <?php
                        if ($v > 0) {
                            ?>
                            <li>
                                <?php
                                if ($ml->version_no > 0) {
                                    $vno = "<span class='badge bg-info text-xs'>V" . $ml->version_no . " Version History</span> ";
                                } else {
                                    $vno = "<span class='badge bg-info text-xs'>V0 Version History</span>";
                                }

                                if ($check == 0) {
                                    ?>
                                    <a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->milestone_id ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><?= $vno ?></a>
                                <?php } else { ?>
                                    <a href="<?= base_url() ?>index.php/admin/projectmgmt/projects/version_history/<?= $ml->parent_Rid ?>/<?= $ml->milestone_id ?>/<?= $ml->project_id ?>" data-toggle='ajaxModal'><?= $vno ?></a>
                                <?php } ?>

                            </li>
                        <?php } ?>
                    </ul>
                    <span id="mcostup<?= $ml->milestone_id ?>" class="pull-right"></span> </div>
                <div id="ml<?= $ml->milestone_id ?>" class="panel-collapse in" style="height: auto;">
                    <div class="panel-body text-sm">
                        <div class="col-md-16 col-lg-16 col-sm-16 col-xs-16" id="lineitem_form">
                            <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Resources</header>
                            <section class="panel panel-default">
                                <div class="table-responsive ">
                                    <table class="table table-striped b-t b-light text-sm">
                                        <thead>
                                            <tr>
                                                <th>Level</th>
                                                <th>Man-days</th>
                                                <th>Head Count</th>
                                                <th>Total Man Days</th>
                                                <th>Daily FTE (RM)</th>
                                                <th>Time Cost (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $milestone_cost = 0;
                                            $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='" . $ml->milestone_id . "'")->result();

                                            foreach ($mu as $u) {
                                                ?>
                                                <tr >
                                                    <td><?= $u->user_level ?></td>
                                                    <td><?= $u->man_days ?></td>
                                                    <td><?= $u->head_count ?></td>
                                                    <td><?= $u->total_man_days ?></td>
                                                    <td><?= $u->fte ?></td>
                                                    <td align="right"><?= number_format($u->time_cost, 2) ?></td>
                                                </tr>
                                                <?php
                                                $milestone_cost = $milestone_cost + $u->time_cost;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <span class="mcostdown" rowid="<?= $ml->milestone_id ?>" id="matcost<?= $ml->milestone_id ?>" style="display:none"><?php echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span> </div>
                            </section>
                            <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
                            <section class="panel panel-default">
                                <div class="table-responsive ">
                                    <table class="table table-striped b-t b-light text-sm">
                                        <thead>
                                            <tr>
                                                <th>Material Cost (RM)</th>
                                                <th>New CAPEX (RM)</th>
                                                <th>Outsourcing (RM)</th>
                                                <th>Others (RM)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr >
                                                <td><?= $ml->material_cost ?></td>
                                                <td><?= $ml->new_capex ?></td>
                                                <td><?= $ml->outsourcing ?></td>
                                                <td><?= $ml->others ?></td>
                                            </tr>
                                        </tbody>

                                        <thead>
                                            <tr>
                                                <th>Remarks</th>
                                                <th>Remarks</th>
                                                <th>Remarks</th>
                                                <th>Remarks</th>                       
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr >
                                                <td><?= $ml->mcost_desc ?></td>
                                                <td><?= $ml->ncapex_desc ?></td>
                                                <td><?= $ml->outsourcing_desc ?></td>
                                                <td><?= $ml->others_desc ?></td>

                                            </tr>

                                        </tbody>

                                    </table>
                                </div>
                            </section>
                        </div>
                        <div class="form-group" style="margin-top:10px">
                            <label for="projects_assigned_to" class="control-label"><strong>Task & Description</strong></label>
                            <div class="controls pad">

                                <?php
                                $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '" . $ml->milestone_id . "'")->result();
                                $no = 1;
                                foreach ($tp as $t) {
                                    ?>
                                    <b><?= $no ?>. <?= $t->task_name ?> </b>  <?= $t->description ?> <br>

                                    <?php
                                    $no++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $noms++;
            $is_milestone_exist++;
            $milestone_cost = 0;
        }
        ?>
    </div>
    <?php if ($this->auth->user_id() == $pj->initiator) { ?>
        <section class="panel panel-default" id="formedit">
            <header class="panel-heading font-bold"><strong>Project Status</strong></header>
            <div class="panel-body" > <?php echo form_open($this->uri->uri_string(), 'id="formaly" data-parsley-validate'); ?>
                <div class="form-group pull-in clearfix">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                        <label><strong>Project Status</strong>  (Current Status : <label><?php echo $pj->status == '' ? 'N/A' : $pj->status; ?></label>)</label>
                        <div class='controls'>
                            <input name="project_id" type="hidden" value="<?= $this->uri->segment(5) ?>" />
                            <select name="projects_state" id="projects_state" class="selecta form-control input-sm input-s" style="max-width:200px" required>
                                <option value="">Please Select</option>
                                <?php foreach ($pro_status as $status): ?>
                                    <option value="<?= $status ?>"><?= $status ?></option>
                                <?php endforeach; ?>
                            </select>

                        </div> </div>
                    <?php if ($pj->status != "Initiated") { ?>
                        <div id="prjct_pre" class="col-sm-3" style="display: none">
                            <label>Project Code*</label>
                            <div class='controls'>
                                <input data-parsley-pattern="^[a-zA-Z]+$" class="input-sm input-s form-control" id='projects_prefix' maxlength="4"  type='text' name='projects_prefix'  value="<?php echo $this->input->post('projects_prefix'); ?>" required="" placeholder="Example : PMM (Alphabet Only)" />

                            </div>  
                        </div>

                        <div id="prjct_suff" class="col-sm-3" style="display: none">
                            <label>Project Number*</label>
                            <div class='controls'>
                                <input   class="input-sm input-s form-control" id='projects_suffix' type='number' name='projects_suffix'  value="<?php echo $this->input->post('projects_suffix'); ?>" placeholder="Example : 001" required="" />

                            </div>  
                        </div>
                    <?php } ?>


                    <div id="prjct_remarks" class="col-sm-3">
                        <label>Project Remarks</label>
                        <div class='controls'>
                            <input   class="input-sm form-control" id='project_remarks' type='text' name='project_remarks'  value="<?php echo $this->input->post('project_remarks'); ?>" placeholder="Write your remarks here..." required="" />

                            <?php
                            if ($pj->remarks == '' || $pj->remarks == null) {
                                
                            } else {
                                echo '<a href="#"   data-toggle="modal" data-target="#myRemarksModal"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Remark History</a>';
                                $obj = json_decode($pj->remarks, true);
                                ?>

                                <div id="myRemarksModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body" align="center" width="400" height="300">
                                                <h4>Remarks History:</h4>
                                                <table width='100%' class='table table-bordered'>
                                                    <thead>
                                                    <th><strong>No</strong></th>
                                                    <th><strong>Status</strong></th>
                                                    <th><strong>Remarks</strong></th>
                                                    <th><strong>Date</strong></th>
                                                    <th><strong>By</strong></th>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $loop = 1;
                                                        foreach ($obj as $val) {
//                                            echo $loop.") <b>Status:</b> ".$val['status'].', <b>Remarks:</b> '.$val['text'].' ('.date("F jS, Y", strtotime($val['datetime'])).')<br>'; 
                                                            ?>
                                                            <tr>
                                                                <td><?= $loop ?></td>
                                                                <td><?= $val['status'] ?></td>
                                                                <td><?= $val['text'] ?></td>
                                                                <td><?= date("jS F, Y  g:i a", strtotime($val['datetime'])) ?></td>
                                                                <td><?= $user_name_list[$val['user_id']] ?></td>
                                                            </tr>

                                                            <?php
                                                            $loop++;
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            <?php } ?>
                        </div>  
                    </div>




                    <div class="clearfix"></div>



                    <div class="form-actions">
                        <div class='controls pad'>
                            <input type="hidden" name="created_by" id="created_by" />
                            <input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
                            <input type="submit" id="submit" name="save" class="btn btn-primary" value="Save"  />
                            &nbsp;&nbsp;
                            <!--<input type="submit" id="submit2" name="save" class="btn btn-warning" value="Save &  Create New Milestone"  />-->
                        </div>
                    </div>
                    <?php echo form_close(); ?> </div>


            <?php } ?>
        </div> </section>
    <!--end of col6--> 

</div>
<div id="pdfappend"></div>
<!--end of row--> 
<?php if ($pj->attachment != "") { ?>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width:auto">
            <div class="modal-content">	
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">Ã—</button>
                    <h4 class="modal-title"><?= $att[1] ?></h4>
                </div>	 
                <div class="modal-body" align="center">

                    <img src="<?= base_url() ?>uploads/<?= $att[0] ?>" class="resimg"/>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

                </div>
            </div>
        </div>
    </div><?php } ?>
<script src="<?php echo Template::theme_url('js/parsley/parsley.min.js'); ?>" cache="false"></script> 
<script>



    $(".hour").on("focus", function () {
        $(this).popover('destroy');
        $(".popover").hide();
    });

    $("#change_created_by").on("change", function () {
        if (confirm("Confirm change ownership?")) {
            waitingDialog.show();
            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/change_project_ownership') ?>", {projectid: <?= $this->uri->segment(5) ?>, userid: $(this).val()}, function (data) {

                if (data == 'success') {
                    waitingDialog.update('Project Owner Changed.');
                } else {
                    waitingDialog.update('Error in updating project owner.Please try again.');
                }
                waitingDialog.hide();
            });
        } else {
            return false;
        }
    });

    $("#projects_state").on("change", function () {
//            alert($(this).val());
        var pjct_status = "<?= $pj->status ?>";
        if (($(this).val() == pjct_status)) {
            alert('Project already ' + pjct_status);
            $("#projects_state").select2("val", "");
            $("#projects_state").focus();
        }

        if ($(this).val() == "Initiated") {
            $("#prjct_pre").fadeIn();
            $("#prjct_suff").fadeIn();
            $('#projects_prefix').attr('required', '');
            $('#projects_suffix').attr('required', '');
            $('#projects_prefix').attr('data-parsley-required', 'true');
            $('#projects_suffix').attr('data-parsley-required', 'true');

            //New VO to add remarks
            $("#prjct_remarks").fadeOut();
            $('#project_remarks').removeAttr('required');
            $('#project_remarks').attr('data-parsley-required', 'false');

        } else {
            $("#projects_prefix").val("");
            $("#prjct_pre").fadeOut();
            $("#prjct_suff").fadeOut();
            $('#projects_prefix').removeAttr('required');
            $('#projects_suffix').removeAttr('required');
            $('#projects_prefix').attr('data-parsley-required', 'false');
            $('#projects_suffix').attr('data-parsley-required', 'false');

            //New VO to add remarks
            $("#prjct_remarks").fadeIn();
            $('#project_remarks').attr('required', '');
            $('#project_remarks').attr('data-parsley-required', 'true');

        }
    });


    $('body').on('click', '.popover button', function () {


        var a = $(this);
        var ta = $(this).parent().parent().find("textarea").val();



        $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/milestone/markcomplete') ?>", {remarks: ta, rowid: $(this).attr("rowid")}, function (data) {

            $(".popover").hide();
        })


    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(document).ready(function () {
        $(".mcostdown").each(function () {
            var row = $(this).attr("rowid");
            $("#mcostup" + row).html("<b>Milestone Cost:</b> RM " + numberWithCommas(parseFloat($(this).text() * 1).toFixed(2)) + "&nbsp;&nbsp;");
        })

        var pc = "<?= $pj->project_cost ?>";

        $(".tcosts").on("change", function () {
            var a = 0;
            $(".tcosts").each(function () {

                a = a + $(this).val() * 1;
                console.log(a);

            });

            var c = a + pc * 1;
            console.log(c);

            $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

        });



        window.ParsleyConfig = {
            errorsWrapper: '<div class="pe"></div>',
        };
    });









    $("#submit,#submit2").click(function (e) {
        if ($('#formaly').parsley().validate())
        {
            var txt;
            var r = confirm("Confirm Submit?");
            if (r == true) {
                txt = "You pressed OK!";
            } else {
                e.preventDefault();
            }
        }


    });



    function calc(val)
    {

        var a = $("#mandays" + val).val() * 1;
        var b = $("#headcount" + val).val() * 1;
        var c = parseFloat(a * b);
        var f = $("#tmandays" + val).val(c);
        var d = parseInt($("#fte" + val).val());
        var e = c * d;
        $("#dtcost" + val).text(numberWithCommas(e));
        $("#dfte" + val).text(numberWithCommas(d));
        $("#dtmandays" + val).text(numberWithCommas(c));

        $("#tcost" + val).val(e);
        var pc = "<?= $pj->project_cost ?>";

        var a = 0;
        $(".tcosts").each(function () {

            a = a + $(this).val() * 1;

        });

        var c = a + pc * 1;

        $(".showpc").text("Calculated Project Cost is: " + numberWithCommas(parseFloat(c).toFixed(2)));

    }

    function select_fte(val)
    {

        $("#fte" + val).val($("#select" + val + "  option:selected").attr("dfte"));

        calc(val);
    }

    function addnewrow()
    {
        var theId = $("#expense_table tr").length + 1;
        $('#lastrow').before('<tr id=row' + theId + '><td><select name="select[]" id="select' + theId + '" required class="selecta form-control"   onchange="select_fte(' + theId + ')"  style="height:30px; font-size:12px;  width:140px" ></select></td><td><input type="text"  required   name="mandays[]" id="mandays' + theId + '"  onblur="calc(' + theId + ')" class="input-sm input-xs  form-control"/></td><td><input type="text" name="headcount[]"  required  onblur="calc(' + theId + ')"  id="headcount' + theId + '"  class="input-sm  input-xs   form-control"/></td><td><input type="hidden" name="tmandays[]" id="tmandays' + theId + '" class="input-sm   input-xs  form-control" /><span id="dtmandays' + theId + '"></span></td><td><input type="hidden" name="fte[]" id="fte' + theId + '" class="input-sm   input-xs  form-control" /><span id="dfte' + theId + '"></span></td><td><input type="hidden" name="tcost[]" class="tcosts input-sm input-xs  form-control" id="tcost' + theId + '" /><span id="dtcost' + theId + '"></span></td><td><i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="remrow(\'row' + theId + '\')"></i></td></tr>');

        $('#select1 option').clone().appendTo('#select' + theId);
        $('#select' + theId).select2(
                {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true}
        );
        $('.selecta').select2(
                {

                    placeholder: "Please Select",
                    allowClear: false, dropdownAutoWidth: true
                });

        $('#formaly').parsley('addItem', '#select' + theId);
        $('#formaly').parsley('addItem', '#mandays' + theId);
        $('#formaly').parsley('addItem', '#headcount' + theId);

    }

    function remrow(val)
    {
        $("#" + val).hide('fast', function () {
            $("#" + val).remove();
        });
        setTimeout(function () {



        }, 500);
    }
    $(document).ready(function () {
        $('#projects_milestone_end_date,#projects_milestone_start_date').datepicker().on('changeDate', function () {
            $('.datepicker').hide();
            $(this).removeClass('parsley-error');
            var aid = $(this).attr("data-parsley-id");
            $('#parsley-id-' + aid).remove();
        });

        $('#task_pool').select2(
                {
                    formatResult: format,
                    formatSelection: format,
                    placeholder: "Please Select",
                    allowClear: false, dropdownAutoWidth: true
                }


        );
    });

    function format(issue) {

        $('[data-toggle="tooltip"]').tooltip();


        var originalOption = issue.element;
        return "<div  data-toggle='tooltip' data-placement='top' title='" + $(originalOption).data('title') + "'>" + issue.text + "</div>";
    }

    $('#pdf').click(function () {

        window.location = '<?= base_url() ?>index.php/projects/pdf/<?= $this->uri->segment(5) ?>';
            });
</script> 
