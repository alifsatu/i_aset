<style type="text/css">
    html, body{ height:100%; padding:0px; margin:0px; overflow: hidden;}
</style>
<script src="<?php echo Template::theme_url('js/datepicker/bootstrap-datepicker.js') ?>"></script>
<link rel="stylesheet" href="<?php echo Template::theme_url('js/datepicker/datepicker.css') ?>" />



<link rel="stylesheet" href="<?php echo Template::theme_url('js/dhtmlxganttfree/skins/dhtmlxgantt_broadway.css'); ?>" type="text/css" media="screen" title="no title" charset="utf-8">

<script src="<?php echo Template::theme_url('js/dhtmlxganttfree/dhtmlxgantt.js'); ?>" cache="false"></script> 
<script src="<?php echo Template::theme_url('js/dhtmlxganttfree/ext/dhtmlxgantt_tooltip.js'); ?>" cache="false"></script>

<input type="radio" id="scale1" name="scale" value="1"  /><label for="scale1" class="m-r">&nbsp;Day scale</label>
<input type="radio" id="scale2" name="scale" value="2" /><label for="scale2" class="m-r">&nbsp;Week scale</label>
<input type="radio" id="scale3" name="scale" value="3" /><label for="scale3" class="m-r">&nbsp;Month scale</label>
<input type="radio" id="scale4" name="scale" value="4" checked/><label for="scale4" class="m-r">&nbsp;Year scale</label>



<div id="mygantt"></div>

<script type="text/javascript">

    $(document).ready(function () {

        LoadGantt(false);

    });

    $('#nav').on('click', function () {
//        alert('clicked');
        setTimeout(clicked, 500);
    });

    function clicked() {
        LoadGantt(true);
    }

    $(window).resize(function () {
        LoadGantt(true);
    });

    function LoadGantt(IsResize) {
        //ponin den nak kiro size ning wehhhhhhhhhhh
        console.log(" content H : " + $('#content').height());
        console.log(" content W : " + $('#content').width());


        console.log("body h : " + $('body').height());
        console.log("header h : " + $('header').height());
        console.log(".navbar-header h : " + $('.navbar-header').height());

        console.log("body w : " + $('body').width());
        console.log("nav w : " + $('#nav').width());

        var height = $('body').height() - $('header').height() - $('.navbar-header').height() - $('header').height();//tolok tinggi header ngan nav-bar header.
//        var width = $('body').width()-$('#nav').width()-30;//tolok size nav menu, dpt width container, tolok 30 utk padding 15kiri 15 kanang, beyehhh
        var width = $('#content').width() - 30;

        console.log("calculated h : " + height);
        console.log("calculated w : " + width);

        $('#mygantt').css({'height': height});
        $('#mygantt').css({'width': width});

        if (IsResize == true) {

            gantt.setSizes();

        } else {

            gantt.render();

        }

    }

    function setScaleConfig(value) {
        switch (value) {
            case "1":
                gantt.config.scale_unit = "day";
                gantt.config.step = 1;
                gantt.config.date_scale = "%d %M";
                gantt.config.subscales = [{unit: "day", step: 1, date: "%j, %D"}
                ];
                gantt.config.scale_height = 27;
                gantt.templates.date_scale = null;
                break;
            case "2":
                var weekScaleTemplate = function (date) {
                    var dateToStr = gantt.date.date_to_str("%d %M");
                    var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                    return dateToStr(date) + " - " + dateToStr(endDate);
                };
                gantt.config.scale_unit = "week";
                gantt.config.step = 1;
                gantt.templates.date_scale = weekScaleTemplate;
                gantt.config.subscales = [
                    {unit: "day", step: 1, date: "%D"}
                ];
                gantt.config.scale_height = 50;
                break;
            case "3":
                gantt.config.scale_unit = "month";
                gantt.config.date_scale = "%F, %Y";
                gantt.config.subscales = [
                    {unit: "day", step: 1, date: "%j, %D"}
                ];
                gantt.config.scale_height = 50;
                gantt.templates.date_scale = null;
                break;
            case "4":
                gantt.config.scale_unit = "year";
                gantt.config.step = 1;
                gantt.config.date_scale = "%Y";
                gantt.config.min_column_width = 50;
                gantt.config.scale_height = 90;
                gantt.templates.date_scale = null;
                gantt.config.subscales = [
                    {unit: "month", step: 1, date: "%M"}
                ];
                break;
        }
    }
    setScaleConfig('4');
    var func = function (e) {
        e = e || window.event;
        var el = e.target || e.srcElement;
        var value = el.value;
        setScaleConfig(value);
        gantt.render();
    };
    var els = document.getElementsByName("scale");
    for (var i = 0; i < els.length; i++) {
        els[i].onclick = func;
    }

    var tasks = {
        data: [
<?php
$ms = $this->db->query("select *,abs(DATEDIFF(start_date,end_date)) as duration from intg_milestones where parent_Rid=0 and project_id = '" . $this->uri->segment(5) . "' AND status = 'yes' order by start_date ")->result();
$pj = $this->db->query("select * from intg_projects where id = '" . $this->uri->segment(5) . "'")->row();
$no = 1;
foreach ($ms as $ml) {

    $mstu = $this->db->query("select sum(time_cost) as totalcost from intg_milestones_users where milestones_id='" . $ml->milestone_id . "'")->row();
    $mstuactual = $this->db->query("SELECT SUM(actualcost) AS actualcost FROM (
        SELECT (( intg_timesheet_details.user_level_details_fte_rate / 8.5) * SUM(intg_timesheet_details.tsd_hours)) AS actualcost 
        FROM
        intg_timesheet_details 
        JOIN intg_timesheet 
        ON intg_timesheet.`id` = intg_timesheet_details.`tid` 
        JOIN intg_users 
        ON intg_users.id = intg_timesheet.uid 
        JOIN intg_user_level 
        ON intg_users.user_level_id = intg_user_level.id 
        WHERE intg_timesheet.final_status = 'Yes' 
        AND intg_timesheet_details.`mid` = '" . $ml->milestone_id . "' 
        GROUP BY intg_timesheet.uid
        ) T1")->row();
    ?>
    {id:<?= $no ?>, text: " <?= $ml->milestone_name ?>", start_date: "<?= date("d-m-Y", strtotime($ml->start_date)) ?>", duration: "<?= $ml->duration + 1 ?>", progress: 1, cost: "<?= number_format($mstu->totalcost) ?>", actualcost: "<?= number_format($mstuactual->actualcost, 2) ?>", mid: <?= $ml->milestone_id ?>, open: true},
    <?php
    $wt = $this->db->query("select *,abs(DATEDIFF(start_date,end_date)) as duration from intg_milestones where parent_Rid = '" . $ml->milestone_id . "'  and status = 'Yes'")->result();
    $bo = $no + 1;
    foreach ($wt as $wl) {
        
        
        
        $wstu = $this->db->query("select sum(time_cost) as totalcost from intg_milestones_users where milestones_id='" . $wl->milestone_id . "'")->row();
        $wstuactual = $this->db->query("                         
        SELECT SUM(actualcost) AS actualcost FROM (
        SELECT 
        ((intg_timesheet_details.user_level_details_fte_rate / 8.5) * SUM(intg_timesheet_details.tsd_hours)) AS actualcost
        FROM
        intg_timesheet_details 
        JOIN intg_timesheet 
        ON intg_timesheet.id = intg_timesheet_details.tid 
        JOIN intg_users 
        ON intg_users.id = intg_timesheet.uid 
        JOIN intg_user_level 
        ON intg_users.user_level_id = intg_user_level.id 
        JOIN intg_projects 
        ON intg_projects.id = intg_timesheet_details.pid 
        JOIN intg_milestones_users 
        ON intg_milestones_users.`milestones_id` = intg_timesheet_details.`mid` 
        JOIN intg_milestones 
        ON intg_milestones.`milestone_id` = intg_timesheet_details.`mid` 
        WHERE 
        intg_timesheet.final_status = 'Yes' 
        AND
        intg_milestones.`milestone_id` = '" . $wl->milestone_id . "' 
        GROUP BY intg_users.id
        ) T1")->row();

        $actualcost = $mstuactual->actualcost + $wstuactual->actualcost;
        ?>
                    {id:<?= $bo ?>, text: "V<?= $wl->version_no ?> <?= $wl->milestone_name ?>", start_date: "<?= date("d-m-Y", strtotime($wl->start_date)) ?>", duration:<?= $wl->duration + 1 ?>, progress: 0, cost: "<?= number_format($wstu->totalcost) ?>", actualcost: "<?= number_format($actualcost, 2) ?>", mid: <?= $wl->milestone_id ?>, parent:<?= $no ?>},
        <?php
        $bo++;
    }
    ?>


    <?php
    $no = $bo;
}
?>
        ]

    };
    /*gantt.config.scale_unit = "month";
     gantt.config.date_scale = "%F, %Y";
     */
    gantt.config.scale_unit = "month";
    gantt.config.date_scale = "%Y-%M";
    gantt.config.scale_height = 50;
    gantt.config.subscales = [
        {unit: "day", step: 1, date: "%j, %D"}
    ];
    gantt.config.readonly = true;
    gantt.templates.task_class = function (st, end, item) {
        return item.$level == 0 ? "gantt_project" : ""
    };
    gantt.config.date_grid = "%d/%m/%Y";
    gantt.templates.rightside_text = function (start, end, task) {
        return "<em style='font-size: 9px'><b>BC:</b> RM" + task.cost + "" + "<b> AC :</b> RM" + task.actualcost + "</em>";
    };
    gantt.templates.tooltip_text = function (start, end, task) {
        return task.text + ",<br><b>Budget Cost:</b> RM" + task.cost + "<br><b> Actual Cost  : </b>RM" + task.actualcost + " ";
//                    "<br><b>Start Date:</b> "+task.start_date+
//                    "<br><b>End Date:</b> "+task.end_date;
    };
    gantt.init('mygantt');
    gantt.parse(tasks);
    $('#mygantt').siblings('header.header').append('<button id="export-to-pdf" class="btn btn-info pull-right"><i class="fa fa-file-text"></i> PRINT / PDF</button>');
    // export to pdf
    $('#export-to-pdf').on('click', function () {
        gantt.exportToPDF();
    });</script>
<script>

    $(document).ready(function () {

        $('.datepicker').datepicker({
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months"
        }).on("changeDate", function () {

            $('#datepicker').datepicker('hide');
        });
    });
    $("#datesubmit").on("click", function () {


        $('#mygantt').html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
        $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/ms_gantt') ?>", {dateval: $("#datepicker").val(), project:<?= $this->uri->segment(5) ?>}, function (data) {
            $('#mygantt').html(data);
        });

        gantt.clearAll();
    });
</script>

