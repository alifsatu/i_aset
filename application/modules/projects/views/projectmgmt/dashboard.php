<style type="text/css">
    .mix{
        display: none;
    }
    @media (max-width:500px) {
        .mix {
            width: 100%;
        }
    }
    .input-group {
        display: inline-table;
        vertical-align: middle;
    }
    .input-group .input-group-addon,
    .input-group .input-group-btn,
    .input-group .form-control {
        width: auto !important;
    }
</style>
<div class="m-b-md">
    <div class="row">
        <div class="col-sm-6">
            <!--<h3 class="m-b-none m-t-sm">Projects</h3>-->
            <form class="form-inline" role="form"><div class="form-group">
                    <? echo lang('projects_filter')?> 
                    <div class="btn-group" data-toggle="buttons"> 
                        <label class="btn btn-sm btn-default filter" data-filter="all"><input type="radio" name="options"><i class="fa fa-check text-active"></i>All</label> 

                        <label class="btn btn-sm btn-default filter" data-filter=".Initiated"><input type="radio" name="options"><i class="fa fa-check text-active"></i>Initiated/ Active</label> 
                        <label class="btn btn-sm btn-warning filter" data-filter=".Completed"><input type="radio" name="options"><i class="fa fa-check text-active"></i>Completed</label> 
                        <label class="btn btn-sm btn-success filter" data-filter=".Suspended"><input type="radio" name="options"><i class="fa fa-check text-active"></i>Suspended</label>
                        <label class="btn btn-sm btn-danger filter" data-filter=".Terminated"><input type="radio" name="options"><i class="fa fa-check text-active"></i>Terminated</label> 
                        <label class="btn btn-sm btn-primary filter" data-filter=".Myproject"><input type="radio" name="options"><i class="fa fa-check text-active"></i>My Project</label> 
                    </div></div></form>
        </div>
    </div>
</div>
<div class="m-b-md ">
    <div class="row">
        <!--					<div class="col-sm-6">
                                                <form class="form-inline">
                                                        <div class="form-group">
                                                                <div class="input-group">
                                                                        <input class="form-control" type="text" placeholder="<? echo lang('projects_search'); ?>" id="search_project_name_text" value="<?= $this->input->get('search') ?>"/>
                                                                        <span class="input-group-btn"> 
                                                                                <a href="#" id="linksearch" onclick="//location.href='?search='+document.getElementById('search_project_name_text').value" class="btn btn-default" type="button"><? echo lang('search'); ?></a> 
                                                                        </span>
                                                                </div>
                                                        </div>
                                                </form>
                                          </div>-->
        <div class="col-sm-12">
            <form class="form-inline">
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="<? echo lang('projects_search'); ?>" id="search_project_name_text" value="<?= $this->input->get('search') ?>"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control datepicker-input" placeholder="Start Date" id="from-date" value="<?= $this->input->get('from') ?>" data-date-format="dd-mm-yyyy"> 
                </div>
                <? echo lang('to'); ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control datepicker-input" placeholder="End Date" id="to-date" value="<?= $this->input->get('to') ?>" data-date-format="dd-mm-yyyy">
                        <span class="input-group-btn">	
                            <a href="#" class="btn btn-info" data-toggle="class:show inline" data-target="#spin" data-loading-text="Searching..." id="filter-by-date"><i class="fa fa-search"></i></a>
                            <a href="projects" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div>
        <label>Filter:</label>
        <input type="text" class="form-control" id="filter" placeholder="Search anything (Case Sensitive)" />
    </div>
</div>
<div class="row" id="all-projects">

    <?php
    $status = array("Completed", "Initiated");
    if($records){
    foreach ($records as $project):
        ?>
        <div class="col-lg-4 mix <?= $project->status ?> <?= $project->created_by == $this->auth->user_id() ? 'Myproject' : '' ?> ">
            <section class="panel panel-default">
                <header class="panel-heading" style="background-color:<?= $project->projects_color ?>"><span class="pull-right badge btn-danger"><?= isset($project->total_ongoing_tasks) ? $project->total_ongoing_tasks : ''; ?></span><span class="pull-right badge btn-success"><?= isset($project->total_completed_tasks) ? $project->total_completed_tasks : ''; ?></span>
                    <a href="projects/view_status/<?= $project->id ?>"><strong><u><?= $project->project_name ?></u></strong></a>
                    <?php
                    if ($project->created_by == $this->auth->user_id() || preg_match('/\b' . $this->auth->user_id() . '\b/', $project->other_approvers) || preg_match('/\b' . $this->auth->user_id() . '\b/', $project->coworker)) {
                        ?>
                        <span class="pull-right badge btn-default">
                            <?php if ($project->created_by == $this->auth->user_id()) { ?>
                                <i class="fa fa-user text-muted" data-toggle="tooltip" data-placement="top" title="Your Created Project"></i>

                                <?php
                            }
//                            if(preg_match('/\b' . $this->auth->user_id() . '\b/', $project->final_approvers)){ 
                            ?>
                                                                                                                                                                <!--<i class="fa fa-user  text-muted" style="color: blue;" data-toggle="tooltip" data-placement="top" title="Your are the Final Approver"></i>-->

                            <?php
                            // }
                            if (preg_match('/\b' . $this->auth->user_id() . '\b/', $project->other_approvers)) {
                                ?>
                                <i class="fa fa-user  fa-border text-muted" data-toggle="tooltip" style="color: #59955C;" data-placement="top" title="You are the Approver"></i>

                                <?php
                            }
                            if (preg_match('/\b' . $this->auth->user_id() . '\b/', $project->coworker)) {
                                ?>
                                <i class="fa fa-users text-muted" data-toggle="tooltip" data-placement="top" title="Project Assigned To You"></i>
                            <?php } ?>
                        </span>
                    <?php } ?>

                </header>
                <div class="panel-body" id="walltemp<?= $project->id ?>"> 


                    <div class="row text-center m-t"> 
                        <div class="col-xs-6"> <p><?= $project->status ?></p> </div> 
                        <div class="col-xs-6"> <p><?= $project->prefix_io_number ?></p> </div> 
                    </div> 
                    <div id="b-c" class="text-center"> 
                        <div class="inline">
                            <?php
                            $proj_start_current_diff = abs(strtotime(date("Y-m-d")) - strtotime($project->project_start_date));
                            $proj_start_end_diff = abs(strtotime($project->project_end_date) - strtotime($project->project_start_date));
                            ?>
                            <div class="easypiechart easyPieChart" data-percent="<? e(strtotime(date("Y-m-d")) > strtotime($project->project_end_date) ? 100 :$proj_start_current_diff / $proj_start_end_diff * 100)?>" data-animate="2000" data-line-width="16" data-loop="false" data-size="188" style="width: 188px; height: 188px; line-height: 188px;">
                                 <span class="h2 step"><?= $project->overall_progress * 100 ?></span>%
                                <div class="easypie-text">Completed</div>
                                <canvas width="188" height="188"></canvas></div>
                        </div> 
                    </div> 
                    <div class="row text-center m-t"> 
                        <div class="col-xs-6"> <p><?= date('d-m-Y', strtotime($project->project_start_date)) ?></p> </div> 
                        <div class="col-xs-6"> <p><?= date('d-m-Y', strtotime($project->project_end_date)) ?></p> </div> 
                    </div> 
                </div>
                <div class="clearfix panel-footer"> 

                    <a href="projects/milestone_chart/<?= $project->id ?>"  class="m-r"><i class="fa fa-align-left text-muted"  data-toggle="tooltip" data-placement="fixed" title="View Gantt Chart"></i></a>
                    <a href="tasks/timesheets/0/4" class="m-r"><i class="fa fa-clock-o text-muted" data-toggle="tooltip" data-placement="top" title="Go To Timesheet"></i></a>


                    <?php if ($project->created_by == $this->auth->user_id() && $this->auth->has_permission('Projects.Projectmgmt.Edit') && in_array($project->status, $status)): ?>

                        <a href="projects/milestone/<?= $project->id ?>" class="m-r"><i class="fa fa-pencil text-muted" data-toggle="tooltip" data-placement="top" title="Edit Milestone"></i></a>


                        <span class="pull-right">
                            <a href="projects/edit/<?= $project->id ?>" class="m-r"><i class="fa fa-edit text-muted" data-toggle="tooltip" data-placement="top" title="Edit Project"></i></a> 
                        </span>
                    <?php endif; ?>
                </div>
            </section>
        </div>
        <!-- Project Details modal -->
        <div class="modal fade" id="p<?= $project->id ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Project Details</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr><td>Description</td><td><?= $project->description ?></td></tr>
                            </thead>
                            <tbody>
                                <tr><td>Client</td><td><?= $project->client->client_name ?></td></tr>
                                <tr><td>Created By</td><td><?= $project->creator->display_name ?></td></tr>
                                <tr><td>Assigned to</td><td><?=
                                        $project->assigned_to ? implode(', ', array_map(function($user) {
                                                            return $user->display_name;
                                                        }, $project->assigned_to)) : ''
                                        ?></td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                </div>
            </div>
        </div>
        <!-- Recent Updated Task modal -->
        <div class="modal fade" id="tp<?= $project->id ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Task Details</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <?php if ($project->tasks): ?>
                                <?php foreach ($project->tasks as $task): ?>
                                    <?php if ($task->id == $project->last_activity->task_id): ?>
                                        <thead>
                                            <tr><td>Name</td><td><?= $task->task_name ?></td></tr>
                                        </thead>
                                        <tbody>
                                            <tr><td>Description</td><td><?= $task->description ?></td></tr>
                                        </tbody>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </table>
                    </div>
                    <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
                </div>
            </div>
        </div>
    <?php endforeach; 
    
    }else{ ?>
            <div class="col-lg-12" >
                <div class="alert alert-info">
                    <strong>Info!</strong> No Projects To Show. You can create a new project <a href="<?= base_url('index.php/admin/projectmgmt/projects/createnew') ?>" class="btn btn-info" role="button">here</a>.
                </div>
            </div>
        
    <?php }
    
    ?>

</div>

                         <!--<script src="<?= Template::theme_url('js/pusher.min.js') ?>"></script>-->
<script type="text/javascript">
    $(document).ready(function(){
    $('#search_project_name_text').keypress(function(e){
    if (e.keyCode == 13)
            $('#linksearch').click();
    });
    });
    $(function() {
    /*
     var pusher = new Pusher('aa9987615953ad4f30ec');
     var channel = pusher.subscribe('my_pusher');
     channel.bind('my_event', function(data){
     console.log(data);
     alert('An event was triggered with message: ' + data.message);
     });
     */
    var modalHtml = '<div class="modal fade"><div class="modal-dialog"><div class="modal-content">'
            + '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title"></h4></div>'
            + '<div class="modal-body"></div><div class="modal-footer"></div>'
            + '</div></div></div>';
    $('.taskDetail').on('click', function(e){
    $modal = $(modalHtml).modal();
    $('body').append($modal);
    $modal.find('.modal-title').text('Task Details');
    id = $(this).data('id');
    $.get("<?= base_url('index.php/admin/projectmgmt/tasks/all') ?>/" + id, function(data){
    data = data[0]; console.log(data);
    var htmlBody = '<table class="table">';
    htmlBody += '<thead><tr><td><strong><? echo lang('projects')?></strong></td><td>' + data.project.project_name + '</td></tr></thead><tbody>';
    htmlBody += '<tr><td><strong><? echo lang('created_by')?></td><td>' + data.project.creator.display_name + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('task_name')?></td><td>' + data.task_name + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_desc')?></td><td>' + data.description + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('Weightage')?></td><td>' + data.weightage_percentage + ' %</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_start_date')?></td><td>' + data.task_start_date + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_end_date')?></td><td>' + data.end_date + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('created_on')?></strong></td><td>' + data.created_on + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_assigned_to')?></td><td>' + $.map(data.assigned_to, function(e){return e.display_name; }).join(', ') + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('Tags')?></td><td>' + data.tags + '</td></tr>';
    htmlBody += '</tbody></table>';
    $modal.find('.modal-body').html(htmlBody);
    }, 'json');
    });
    $('.projectDetail').on('click', function(e){
    $modal = $(modalHtml).modal();
    $('body').append($modal);
    $modal.find('.modal-title').text('Project Details');
    id = $(this).data('id');
    $.get("<?= base_url('index.php/admin/projectmgmt/projects/all') ?>/" + id, function(data){
    data = data[0];
    var htmlBody = '<table class="table">';
    htmlBody += '<thead><tr><td><strong><? echo lang('projects')?></strong></td><td>' + data.project_name + '</td></tr></thead><tbody>';
    htmlBody += '<tr><td><strong><? echo lang('projects_desc')?></strong></td><td>' + data.description + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_client')?></strong></td><td>' + data.client.client_name + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_status')?></strong></td><td>' + data.status + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('created_by')?></strong></td><td>' + data.creator.display_name + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_assigned_to')?></strong></td><td>' + $.map(data.assigned_to, function(e){return e.display_name; }).join(', ') + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_start_date')?></strong></td><td>' + data.project_start_date + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_end_date')?></strong></td><td>' + data.project_end_date + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('created_on')?></strong></td><td>' + data.created_on + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('projects_cost')?></strong></td><td>' + data.cost_currency + ' ' + data.project_cost + '</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('overall_progress')?></strong></td><td>' + data.overall_progress * 100 + '%</td></tr>';
    htmlBody += '<tr><td><strong><? echo lang('Tags')?></strong></td><td>' + data.tags + '</td></tr>';
    htmlBody += '</tbody></table>';
    $modal.find('.modal-body').html(htmlBody);
    }, 'json');
    });
    $('a#filter-by-date').on('click', function(e){
    location.href = '?from=' + $('input#from-date').val() + '&to=' + $('input#to-date').val() + '&search=' + $('input#search_project_name_text').val();
    });
    });
    jQuery(document).on('click', '[data-toggle="ajaxModal2"]',
            function(e) {
            jQuery('#datefrom').datepicker({ dateFormat: 'yy-mm-dd'});
            $('#ajaxModal').remove();
            e.preventDefault();
            var $this = $(this)
                    var $remote = $this.data('remote') || $this.attr('href'),
                    $modal = $('<div class="modal fade" id="ajaxModal" style="width:auto;height:auto;max-height:100%"><div class="modal-dialog2-lg"><div class="modal-content">' +
                            '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">&nbsp;</h4></div>' +
                            '<div class="modal-body"></div>' +
                            '</div></div></div>');
            $('body').append($modal);
            $modal.modal();
            $modal.find('.modal-body').load($remote, function(){

            $('#container').css('width', '836px');
            jQuery('#datefrom').datepicker({ dateFormat: 'yy-mm-dd'});
            jQuery('#dateto').datepicker({ dateFormat: 'yy-mm-dd'});
            $('.modal-body .btn').remove();
            });
//$modal.find('.modal-body').children().eq(2).hide();
            //$('[name=save]').parent().hide();
            }
    );
    function gotowall(val) {


    swal({
    title: "You are about to create a group conversation",
            type: "warning",
            text: "",
            showCancelButton: true,
            confirmButtonColor: "#65bd77",
            confirmButtonText: "Yes, lets do it!",
            closeOnConfirm: true
    }, function(){



    $.ajax({
    type: "POST",
            url: "<?php echo site_url(SITE_AREA . '/ideabank/ideas/group_conference') ?>",
            data:{
            insert:1,
                    gname:$("#gotowall" + val).attr("projname"),
                    userid:[$("#gotowall" + val).attr("assignee")],
                    projectid:$("#gotowall" + val).attr("projid")
            },
            dataType: 'json',
            success: function(data){

            $('#walltemp' + $("#gotowall" + val).attr("projid")).html('<i class="fa fa-spinner  fa-3x fa-spin"></i>&nbsp;&nbsp;Hold on, relocating....');
            }

    }).done(function(data) {


    setTimeout(function(){

    window.location = "<?php echo site_url(SITE_AREA . '/ideabank/ideas/wall/group/') ?>/" + data.insertid;
    },
            5000);
    });
    });
    }

</script>