<script>

    function callpop(val)
    {
        //alert('quick approve');
        $('[rel=popover]').popover('destroy');

        if ($("#mypop" + val).hasClass('pop')) {
            $("#mypop" + val).removeClass('pop')
        } else {
            waitingDialog.show('...Starting quick approval dialog');
            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/quickapprove') ?>", {
                docid: $("#mypop" + val).attr("qid"),
//                userid: $("#mypop" + val).attr("Clientid"), 
                initiator: $("#mypop" + val).attr("initiator"),
                finalapprover: $("#mypop" + val).attr("fapprovers"),
                type: "mile"}, function (data) {
                $("#mypop" + val).attr('data-content', data).popover("show").addClass('pop');
                waitingDialog.hide();

            });

        }
    }



    function updateApprovalstatus(docid, rowid, rolename, mrowid, act, userid, finalapprover, initiator, type)
    {

        var status = $("input[name='approval_status_status']:checked").val();
        var remarks = $('#asr').val();

        $('#appr2' + docid, '#appr1' + docid).html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
        waitingDialog.show('...Approval in process, please wait');

        $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/projects/updateApprovalstatus') ?>", {
            status: status,
            remarks: remarks,
            rowid: rowid,
            rolename: rolename,
            docid: docid,
            mrowid: mrowid,
            act: act,
            userid: userid,
            finalapprover: finalapprover,
            initiator: initiator,
            type: type
        }, function (resp) {
            console.log(resp);
            var inn1 = $('#inner1', resp).html();
            var inn2 = $('#inner2', resp).html();
            var inn3 = $('#inner3', resp).html();
            $("#appr2" + docid).html(inn1);
            $("#appr1" + docid).html(inn2);
            $("#appr3" + docid).html(inn3);
            waitingDialog.hide();
        });



    }

    function attache(val)
    {
        var a = $("#fileField").val();
    }
    

</script>

<style>

</style>
<?php $apprstat = NULL; ?>
<?php if ($has_records) : ?>

<?php endif; ?>
    <tbody>
        <?php
        if ($has_records) : $no = $this->input->get('per_page') + 1;
            ?>
            <tr>
                <?php if ($can_delete) : ?>
                    <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
                <?php endif; ?>
                <td><?php echo $numbering; ?></td>




                <?php /* if ($can_edit) : ?>
                  <td><?php echo anchor(SITE_AREA . '/projectmgmt/projects/edit/'.$record->id, '<span class="fa fa-pencil"></span>&nbsp;' .  $record->client_name); ?></td>
                  <?php else : ?>
                  <td><?php e($record->client_name); ?></td>
                  <?php endif; */ ?>
                <td><?php
                    $a = $this->db->query("select project_name from intg_projects where id=" . $record->project_id . "")->row();
                    echo $a->project_name;
                    ?></td>
                <td>
                    <?php
                    //$a = $this->db->query("select prefix_io_number from intg_projects where id=".$record->project_id."")->row(); 
                    //echo $a->prefix_io_number;
                    echo $record->project_code;
                    ?>
                </td>
                <td>
                    
                    <?php
                        $projectormilestone = '';
                        $ml = $this->db->query("select * from intg_milestones where ms_approval_id=" . $record->id . "");
                        if ($ml->num_rows() == 1) {
                            $single = $ml->row();
                            $single_milestone_approval_id = $single->milestone_id;
                            echo $single->milestone_name;
                            echo '</td><td ><div id="appr3'.$record->id.'">';
                            milestone_approval_badges($single, $record->created_by, $record->project_id);
                        } else {
                            $plural = $ml->result();
                            $no = 1;
                            foreach ($plural as $m) {
                                echo $no.') '.$m->milestone_name;
                                echo '<br>';
                                $no++;
                            }
                            echo '</td><td ><div id="appr3'.$record->id.'">';
                            $no2 = 1;
                            foreach ($plural as $m) {
                                echo $no2.')';
                                 milestone_approval_badges($m, $record->created_by, $record->project_id);
                                echo '<br>';
                                $no2++;
                            }
                        }
                        ?>
                </div>
            </td>
                <td><?php
                    $b = $this->db->query("select display_name from intg_users where id = " . $record->created_by . "")->row();
                    echo $b->display_name;
                    ?></td>
                <td><?php echo $record->created_on == "0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s", strtotime($record->created_on)); ?></td>


                                        <!--<td><?php echo date('d-m-Y h:i:s ', strtotime($record->quote_created)) ?></td>-->


                <td>
                    <div id="appr1<?= $record->id ?>">
                        <?php                        
                        if ($record->final_status == "Yes") {

                            // check if logged in user is the generator of the quote		
                            if ($this->auth->user_id() == $record->initiator) {
                                echo anchor(SITE_AREA . '/projectmgmt/projects/view_status/' . $record->project_id . '/' . $record->initiator . '/', '<i class=" fa fa-pencil">&nbsp;</i>', 'title="View Project"');
                            }
                        } else if ($record->final_status == "No") {
                            if ($this->auth->user_id() == $record->initiator) {
                                echo anchor(SITE_AREA . '/projectmgmt/projects/view_milestone/' . $record->project_id, '<i class="fa fa-eye">&nbsp;</i>', 'title="View Milestones"');
                            }

                            $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "42" and approval_status_mrowid = "' . $record->id . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by) order by id desc');
                            $rowm = $querym->row();

                            if ($querym->num_rows() > 0) {
                                $apprstat = $rowm->approval_status_status;
                                $querym2 = $this->db->query("SELECT approval_status_status,hierarchy_status,rolename from intg_approval_status WHERE approval_status_module_id = '42'  AND approval_status_mrowid = '" . $record->id . "' AND hierarchy_status = '" . $rowm->hierarchy_status . "' order by id desc");
                                $rowm2 = $querym2->row();

                                if ($rowm2->approval_status_status == "No" && $rowm->hierarchy_status == "0") {


                                    echo anchor(SITE_AREA . '/projectmgmt/projects/view_milestone/' . $record->project_id, '<i class="fa fa-pencil">&nbsp;</i>', 'title="View and Approve"')
                                    ?></a>&nbsp;&nbsp;
                                    <a href="#"  rel="popover"  data-html="true"  data-original-title="Approvers Action" data-placement="left"  data-trigger="click" id="mypop<?= $record->id ?>" onclick="callpop('<?= $record->id ?>')" title="Quick Approve"   qid="<?= $record->id ?>" initiator="<?= $record->initiator ?>" fapprovers="<?= $record->final_approvers ?>" ><i class="fa  fa-check"></i></a><div style="position:relative"><div id="load<?= $record->id ?>"   style="display:none; position:absolute; top:0; right:100px;"></div></div>
                                    <?php
                                }

                                if ($rowm2->approval_status_status == "No" && $rowm->hierarchy_status != "0") {
                                    $querym3 = $this->db->query("SELECT approval_status_status,hierarchy_status,rolename from intg_approval_status WHERE  approval_status_module_id = '42' and  approval_status_mrowid = '" . $record->id . "' AND rolename = '" . $rowm2->hierarchy_status . "'  order by id desc");
                                    $rowm3 = $querym3->row();


                                    if ($rowm3->approval_status_status == "Yes") {

                                        echo anchor(SITE_AREA . '/projectmgmt/projects/view_milestone/' . $record->project_id, '<i class="fa fa-pencil">&nbsp;</i>', 'title="View and Approve"')
                                        ?></a>&nbsp;&nbsp;<a href="#"  rel="popover"  data-html="true"  data-original-title="Approvers Action" data-placement="left"  data-trigger="click" id="mypop<?= $record->id ?>" onclick="callpop('<?= $record->id ?>')"   qid="<?= $record->id ?>" initiator="<?= $record->initiator ?>" fapprovers="<?= $record->final_approvers ?>" ><i class="fa fa-check"></i></a>
                                        <?php
                                            }
                                        }
                                    }
                                }
                                ?></div>
                </td>
                <td>

                    <div id="appr2<?= $record->id ?>"  class="swing animated">
    <?php
    $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,bu.id,bu.display_name,bu.profileimage,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="42" and bas.approval_status_mrowid = "' . $record->id . '" ORDER BY bas.id asc');
    ?>



                        <a href="#" data-toggle="popover" data-html="true" data-placement="left" data-trigger="hover"  data-original-title="Approvers" 
                           data-content="<div class='scrollable'>
                           <table width='100%' class='table table-condensed'>
                           <tr> 
                           <td width='30%'><strong><?php echo lang('quote_approved_by') ?></strong></td>
                           <td><strong><?php echo lang('quote_role') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                           <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                           </tr><?php
                    foreach ($queryappr->result() as $rowappr) {

                        switch ($rowappr->approvers_status) {
                            case "No" : $status = "<span class='badge btn-warning'>Pending</span>";
                                break;
                            case "Yes" : $status = "<span class='badge btn-primary'>Approved</span>";
                                break;
                            case "Reject" : $status = "<span class='badge btn-danger'>Rejected</span>";
                                break;
                        }
        ?>

                               <tr>

                               <td><?php
                       $img_url = Template::theme_url('images/userimg/default.jpg');
                       if ($rowappr->profileimage)
                           $img_url = Template::theme_url('images/userimg/' . $rowappr->profileimage);
                       echo "&nbsp;<img data-toggle='tooltip' title='" . $rowappr->display_name . "' src='" . $img_url . "' style='width:30px;height:30px;background-repeat:no-repeat;border-radius:50%;-webkit-border-radius:50%;-moz-border-radius:50%'/>";
        ?>&nbsp;<?= $rowappr->display_name ?></td>
                               <td><?= $rowappr->role_name ?></td>
                               <td><?php
                       if (($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No")) {

                           echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                       } else {
                           echo $status;
                       }
        ?></td>
                               <td><?= wordwrap($rowappr->approvers_remarks, 100, "<br />\n"); ?></td>
                               <td><?php
                       if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                           echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                       }
        ?></td>

                               </tr><?php } if ($record->parent_Rid != 0) { ?><?php } ?>
                           <tr>   
                           <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>

                           </tr>
                           </table></div>"

    <?php
    if ($record->final_status == "No") {
        if ($apprstat == "Yes") {
            echo 'class="btn btn-success"><i class=" fa fa-check">';
        } else {
            echo 'class="btn btn-warning" ><i class="fa fa-cogs">';
        }
    } else if ($record->final_status == "Reject") {
        echo 'class="btn btn-danger"><i class="fa fa-times">';
    } else if ($record->final_status == "Yes") {
        echo 'class="btn btn-success"><i class=" fa fa-check">';
    }
    ?> </i></a></div>

                </td>
            </tr>
<?php endif; ?>
                
