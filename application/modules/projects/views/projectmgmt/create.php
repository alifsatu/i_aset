<?php
$validation_errors = validation_errors();
$defaultDropdown = array('yes' => 'Yes', 'no' => 'No');
if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading"><? echo lang('projects_create_error')?></h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($projects))
{
	$projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';

?>
<div class="admin-box">
	<h3><? echo lang('projects')?></h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('project_name') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_name'), 'projects_project_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input class="input-sm input-s form-control" id='projects_project_name' type='text' name='projects_project_name' maxlength="45" value="<?php echo set_value('projects_project_name', isset($projects['project_name']) ? $projects['project_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_name'); ?></span>
				</div>
			</div>
			<div class="control-group <?php echo form_error('client_id') ? 'error' : ''; ?>">
				<?php echo form_label('Prefix(IO Number)', 'projects_client_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<select name="projects_Prefix" id="projects_Prefix" class="selecta form-control input-sm input-s" style="max-width:200px">
						<?php //foreach($clients as $client):?>
						
             <option value="60BT-PBM001A">60BT-PBM001A</option>
             <option value="60BT-PBM002A">60BT-PBM002A</option>
						<?php //endforeach;?>
					</select>
					<span class='help-inline'><?php echo form_error('client_id'); ?></span>
				</div>
			</div>
			
			<?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
			 ?>
			 <div class="control-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('SBU'. lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label') ); ?>
				<div class='controls'>
					 <select name="projects_sbu" id="projects_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
             
            <? foreach($prgm as $pn) { ?>
                    
            
             <option value=<?=$pn->id?>><b><?=$pn->sbu_name?></b></option><? }  ?>
            </select>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>
			
			<?php // Change the values in this array to populate your dropdown as required
				
$cc = $this->db->query('SELECT * FROM  intg_cost_centre WHERE deleted = 0 ORDER BY cost_centre ')->result();
			 ?>
			 <div class="control-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('Cost Centre'. lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label') ); ?>
				<div class='controls'>
					 <select name="projects_cost_centre" id="projects_cost_centre" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
            
            <? foreach($cc as $c) { ?>
                    
            
             <option value=<?=$c->id?>><b><?=$c->cost_centre?></b></option><? }  ?>
            </select>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>
			
			<?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_programs WHERE deleted = 0 ORDER BY program_name ')->result();
			 ?>
			 <div class="control-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('Programme'. lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label') ); ?>
				<div class='controls'>
					 <select name="projects_Programme" id="projects_Programme" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
            
            <? foreach($prgm as $p) { ?>
                    
            
             <option value=<?=$p->id?>><b><?=$p->program_name?></b></option><? }  ?>
            </select>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Justification Of Project', 'projects_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'class' => 'form-control input-sm input-s expand', 'name' => 'projects_description', 'id' => 'projects_description', 'rows' => '1', 'cols' => '40', 'value' => set_value('projects_description', isset($projects['description']) ? $projects['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_id') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_client'), 'projects_client_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<select name="projects_client_id" id="projects_client_id" class="selecta form-control input-sm input-s" style="max-width:200px">
						<?php foreach($clients as $client):?>
						<option value="<?=$client->id?>"><?=$client->client_name?></option>
						<?php endforeach;?>
					</select>
					<span class='help-inline'><?php echo form_error('client_id'); ?></span>
				</div>
			</div>

				<table><tbody><tr>
				<td>
			<div class="control-group <?php echo form_error('project_start_date') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_start_date'), 'projects_project_start_date', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input style="cursor:pointer" data-date-format="DD/MM/YYYY HH:mm" data-format="YYYY/MM/DD HH:mm" class="datepicker-input form-control input-sm" readonly=readonly id='projects_project_start_date' type='text' name='projects_project_start_date' maxlength="45" value="<?php echo set_value('projects_project_start_date', isset($projects['project_start_date']) ? $projects['project_start_date'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_start_date'); ?></span>
				</div>
			</div>

				</td>
				<td>
			<div class="control-group <?php echo form_error('project_end_date') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_end_date'), 'projects_project_end_date', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input style="cursor:pointer" data-date-format="DD/MM/YYYY HH:mm" data-format="YYYY/MM/DD HH:mm" class="datepicker-input form-control input-sm" readonly=readonly id='projects_project_end_date' type='text' name='projects_project_end_date' maxlength="45" value="<?php echo set_value('projects_project_end_date', isset($projects['project_end_date']) ? $projects['project_end_date'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_end_date'); ?></span>
				</div>
			</div>
				</td>
				</tr></tbody></table>


			<div class="control-group <?php echo form_error('projects_assigned_to') ? 'error' : ''; ?>">

				<?php echo form_label(lang('projects_assigned_to'), 'projects_assigned_to', array('class' => 'control-label') ); ?>

				<div class='controls'>
					
					<select multiple name="projects_assigned_to[]" id="projects_assigned_to" class="form-control selecta">
						<?php foreach($users as $user): if ( $user->id != $this->current_user->id ) {?>
						<option value="<?=$user->id?>"><?=$user->display_name?> <?php //if($this->auth->role_id() == "12" ) { ?> (MYR <?php echo (number_format(($user->salary)/22));?>) <?php //}  ?></option>
						<?php } endforeach;?>
					</select>

					<span class='help-inline'><?php echo form_error('projects_assigned_to'); ?></span>

				</div>

			</div>
			
			<div class="control-group <?php echo form_error('project_visibility') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_visibility'), 'projects_project_visibility', array('class' => 'control-label') ); ?>
				<div class='controls'>
				<label class="switch">
                          <input type="checkbox" name="projects_project_visibility" id="projects_project_visibility">
                          <span></span>
                        </label>
					<span class='help-inline'><?php echo form_error('project_visibility'); ?></span>
				</div>
			</div>
			
			<div class="control-group <?php echo form_error('project_notes') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_notes'), 'projects_project_notes', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'class' => 'form-control input-sm input-s expand', 'name' => 'projects_project_notes', 'id' => 'projects_project_notes', 'rows' => '1', 'cols' => '40', 'value' => set_value('projects_project_notes', isset($projects['project_notes']) ? $projects['project_notes'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('project_notes'); ?></span>
				</div>
			</div>
			
			<div class="control-group <?php echo form_error('project_cost') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_cost'), 'projects_project_cost', array('class' => 'control-label') ); ?>
				<div class='controls'>
				<div class="input-group input-s">
                              <div class="input-group-btn">
                                <button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                  <span class="dropdown-label"><?=$preferred_currency ? $preferred_currency->converted_currency : 'MYR'?></span>  
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-select" style="z-index:1111">
                                  <?php foreach($currencies as $currency):?>
								  <li class="<?=$currency->id == $company->currency_id || (!$preferred_currency && $currency->id == 1) ? 'active' : ''?>">
									<a href="#"><input type="radio" value="<?=$currency->converted_currency?>" name="pay_unit" <?=$currency->id == $company->currency_id || (!$preferred_currency && $currency->id == 1) ? 'checked' : ''?>><?=$currency->converted_currency?></a>
								  </li>
								  <?php endforeach;?>
                                </ul>
                              </div>
                              <input type="text" onkeyup="if(isNaN(this.value)){alert('Invalid input, only numbers are accepted');this.value=this.value.slice(0,-1)}" id='projects_project_cost' type='text' name='projects_project_cost' maxlength="45" value="<?php echo set_value('projects_project_cost', isset($projects['project_cost']) ? $projects['project_cost'] : ''); ?>" class="input-sm form-control">
                          </div>
					<span class='help-inline'><?php echo form_error('project_cost'); ?></span>
				</div>
			</div>
			
			<div class="control-group <?php echo form_error('color') ? 'error' : ''; ?>">

				<?php echo form_label(lang('projects_color'), 'projects_color', array('class' => 'control-label') ); ?>

				<div class='controls'>

					<select name="projects_color" id="projects_color" class="color">
						<option value="#7bd148">Green</option>
						<option value="#5484ed">Bold blue</option>
						<option value="#a4bdfc">Blue</option>
						<option value="#46d6db">Turquoise</option>
						<option value="#7ae7bf">Light green</option>
						<option value="#51b749">Bold green</option>
						<option value="#fbd75b">Yellow</option>
						<option value="#ffb878">Orange</option>
						<option value="#ff887c">Red</option>
						<option value="#dc2127">Bold red</option>
						<option value="#dbadff">Purple</option>
						<option value="#e1e1e1" selected>Gray</option>
					</select>
					<span class='help-inline'><?php echo form_error('color'); ?></span>

				</div>

			</div>

			<div class="control-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_status'), 'projects_status', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<div class="btn-group" data-toggle="buttons">
		                <label class="btn btn-sm btn-info active">
							<input type="radio" name="projects_status" value="Ongoing"><i class="fa fa-check text-active"></i> Ongoing
		                </label>
		                <label class="btn btn-sm btn-primary">
							<input type="radio" name="projects_status" value="Completed"><i class="fa fa-check text-active"></i> Completed
		                </label>
		                <label class="btn btn-sm btn-danger">
							<input type="radio" name="projects_status" value="Overdue"><i class="fa fa-check text-active"></i> Overdue
		                </label>
		                <label class="btn btn-sm btn-dark">
							<input type="radio" name="projects_status" value="Dropped"><i class="fa fa-check text-active"></i> Dropped
		                </label>
		            </div>
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>
			
			<div class="control-group <?php echo form_error('project_tag') ? 'error' : ''; ?>">
				<?php echo form_label(lang('projects_tag'), 'projects_project_tag', array('class' => 'control-label') ); ?>
				<div class='controls'>
                        <div id="projects_project_tag" class="pillbox clearfix">
                          <ul>
                            <li class="label bg-dark"><input type="hidden" name="projects_project_tag[]" value="New">New</li>
                            <input type="text" placeHolder="Type and Hit Enter">
                          </ul>
                        </div>
					<span class='help-inline'><?php echo form_error('project_tag'); ?></span>
				</div>
			</div>
			
			<div class="form-actions">
				<input type="hidden" name="created_by" id="created_by" />
				<input type="hidden" value=true name="projects_all_day" id="projects_all_day" />
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projects_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/content/projects', lang('projects_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>