<?php


if (isset($projects))
{
	$projects = (array) $projects;
}
$id = isset($projects['id']) ? $projects['id'] : '';

?>
<style>
.pad { padding:10px 15px; }
.popover { width:300px; }
.tooltip {
    position: fixed; 
}
.resimg {
	max-width:900px;
	height: auto;
	-ms-object-fit: cover;
	-moz-object-fit: cover;
	-o-object-fit: cover;
	-webkit-object-fit: cover;
	object-fit: cover;
	overflow: hidden;
}
</style>
<div class="row" style="height:1000px">
<div class="col-sm-12">

  <div class="panel-group m-b" id="accordion2">
  
    <div class="panel panel-default ">
      <div class="panel-heading clearfix ">
        <?php   
			
		$pj = $this->db->query("select * from intg_projects where id = '".$this->uri->segment(3)."'")->row();  ?>
        
        
        <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#accordion2" href="#project"><strong>Project: </strong>
        <?=ucfirst($pj->project_name)?>
        </a></span></div>
      <div id="project" class="panel-collapse in" style="height: auto;">
        <div class="panel-body">
          <div class="form-group  clearfix">
            <div class="col-sm-3">
              <header class="panel-heading bg-light  no-border"><strong>Project Code</strong></header>
              <div class="pad">
                <?=$pj->prefix_io_number?>
              </div>
            </div>
            <div class="col-sm-3">
              <header class="panel-heading bg-light  no-border"><strong>SBU</strong></header>
              <div class="controls pad">
                <?=$this->db->query("select sbu_name from intg_sbu where id ='".$pj->sbu_id."'")->row()->sbu_name;?>
              </div>
            </div>
            <div class="col-sm-3">
               <header class="panel-heading bg-light  no-border"><strong>Cost Centre</strong></header>
              <div class="controls pad">
                <?=$this->db->query("select cost_centre from intg_cost_centre where id ='".$pj->cost_centre_id."'")->row()->cost_centre;?>
              </div>
            </div>
             <div class="col-sm-3">
             <header class="panel-heading bg-light  no-border"><strong>Duration in Days</strong></header>
              <div class="controls pad">
                <?=$pj->total_days?>
              </div>
            </div>
          </div>
         <div class="form-group  clearfix">
            <div class="col-sm-3">
             <header class="panel-heading bg-light  no-border"><strong>Programme</strong></header>
              <div class="controls pad">
                <?=$this->db->query("select program_name from intg_programs where id ='".$pj->program_id."'")->row()->program_name;?>
              </div>
            </div>
            <div class="col-sm-3">
             <header class="panel-heading bg-light   no-border"><strong>Justification of Project</strong></header>
              <div class="controls pad" style="text-align:justify">
                <?=$pj->justification?>
              </div>
            </div>
            <div class="col-sm-3">
              <header class="panel-heading bg-light  no-border"><strong>Co-Worker</strong></header>
              <div class="controls pad">
             <?php
                  $us = $this->db->query("select display_name,user_level_id from intg_users where id IN ( ".$pj->coworker.")")->result();
			  foreach ( $us as $up ) {
                              $upa[] = $up->display_name." (".get_from_any_table('intg_user_level','user_level','id',$up->user_level_id).")";
                              
                          }
                                echo implode(",&nbsp;",$upa);
			  ?>
              </div>
            </div>
           <div class="col-sm-3">
              <header class="panel-heading bg-light  no-border"><strong>Attachment</strong></header>
              <div class="controls pad">
              <? if ( $pj->attachment !="" ) { $att = explode(":",$pj->attachment); ?>
             <a href="<?=base_url()?>uploads/<?=$att[0]?>"><?=$att[0]?></a>
              <? } ?>
             
              </div>
            </div>
          </div>
          <div class="form-group  clearfix">
           <div class="col-sm-3">
             <header class="panel-heading bg-light  no-border"><strong>Created By</strong></header>
           
<div class="controls pad">
                <?=$this->db->query("select display_name from intg_users where id = '".$pj->created_by."'")->row()->display_name?>
              </div>
            </div>
            <div class="col-sm-3">
             <header class="panel-heading bg-light  no-border"><strong>Start Date </strong>(dd/mm/yyyy) </header>
           
<div class="controls pad">
                <?=date("d/m/Y",strtotime($pj->project_start_date))?>
              </div>
            </div>
            
            <div class="col-sm-3">
             <header class="panel-heading bg-light no-border"><strong>End Date</strong> (dd/mm/yyyy)</header>

<div class="controls pad">
  <?=date("d/m/Y",strtotime($pj->project_end_date))?>
</div>
            </div>
            <div class="col-sm-3">
               <header class="panel-heading bg-light no-border"><strong>Project Color</strong></header>
              <div class="controls pad">
             <div style="background-color: <?=$pj->projects_color?>;  width:80px; text-align:center">&nbsp;</div>
              </div>
            </div>
			<div class="col-sm-3">
               <header class="panel-heading bg-light  no-border"><strong>Approvers Hierarchy</strong></header>
              <div class="controls pad">
             
							
  
   
   
	  <!------app list-->
	  <?php
   
			 $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="40" and bas.approval_status_mrowid = "'.$pj->id.'" ORDER BY bas.id asc');
			
			 $rowreject = $queryappr->row();
			
			 ?>
	  
	 
      <h4><?php echo lang('quote_approvers_hierarchy')?></h4>
      <table width='100%' class='table table-bordered'>
        <tr>
          <td><strong><?php echo lang('quote_approved_by')?></strong></td>
          <td><strong><?php echo lang('quote_role')?></strong></td>
          <td><strong><?php echo lang('quote_approver_status')?></strong></td>
          <td><strong><?php echo lang('quote_approver_remarks')?></strong></td>
          <td><strong><?php echo lang('quote_approver_datetime')?></strong></td>
        </tr>
        <?php
           
			   foreach ( $queryappr->result() as $rowappr ) { 
			   
			   switch($rowappr->approvers_status)
			   {
				  case "No" : $status = " <span class='badge bg-warning m-l text-xs'>Pending</span>"; break;
				   case "Yes" : $status = " <span class='badge bg-success m-l text-xs'>Approved</span>"; break;
				   case "Reject" : $status = " <span class='badge bg-important m-l text-xs'>Rejected</span>"; break;
			   }
			   ?>
        <tr>
          <td><?=$rowappr->display_name?></td>
          <td><?=$rowappr->role_name?>
          <td><?php if(($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No"))
	
	{
	
	echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
	} 
	else 
	{ 
	echo $status;
	}  ?></td>
          <td><?=$rowappr->approvers_remarks?></td>
          <td><?php if ( $rowappr->approvers_approve_date != "0000-00-00 00:00:00") { echo  date('d-m-y h:i:s',strtotime($rowappr->approvers_approve_date)); }?></td>
        </tr>
        <?   }  			
 ?>
  <tr>   
    <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>
	 
  </tr>
      </table>
   
             </div>
            </div>
            
          </div>
          <div class="form-group  clearfix">
            <div class="col-sm-10">
              <header class="panel-heading bg-light  no-border"><strong>Introduction</strong></header>
              <div class="controls pad">
                <?=$pj->introduction?>
              </div>
            </div>
			
            </div>
			 <div class="form-group  clearfix">
              <div class="col-sm-10">
                <header class="panel-heading bg-light   no-border"><strong>Cost Summary (RM) </strong></header>
                <div class="controls pad">
                  <? $ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '".$this->uri->segment(3)."'")->result(); ?>
                  <table class="table table-striped m-b-none text-sm">
                    <tr>
                      <td width="120">&nbsp;</td>
                      <? $no=1;
				   foreach ( $ms as $cs ) { ?>
                      <td><b>
                       Milestone <?=$no?>
                        </b></td>
                     
                      <? $no++; } ?>
                       <td><strong>Total (RM)</strong></td>
                    </tr>
                    <tr>
                      <td width="120"><strong>Material Cost</strong></td>
                      <?
					  $mstotal = 0;
					  $nptotal = 0;
					  $ostotal = 0;
					  $ottotal = 0;
					  $timetotal =0;
					  $gtotal = 0;
				   foreach ( $ms as $cs ) { $total{$cs->milestone_id} = $cs->material_cost + $cs->new_capex+ $cs->outsourcing + $cs->others;?>
                      <td><?=number_format($cs->material_cost,2)?></td>
                     
                      <? $mstotal = $mstotal + $cs->material_cost; } ?>
                       <td><?  echo number_format($mstotal,2); ?></td>
                    </tr>
                    <tr>
                      <td width="120"><strong>New CAPEX</strong></td>
                      <?
				   foreach ( $ms as $cs ) { ?>
                      <td><?=number_format($cs->new_capex,2)?></td>
                    
                      <? $nptotal = $nptotal + $cs->new_capex; } ?>
                       <td><?  echo number_format($nptotal,2); ?></td>
                    </tr>
                    <tr>
                      <td width="120"><strong>Outsourcing</strong></td>
                      <?
				   foreach ( $ms as $cs ) { ?>
                      <td><?=number_format($cs->outsourcing,2)?></td>
                     
                      <? $ostotal = $ostotal + $cs->outsourcing; } ?>
                      <td><?  echo number_format($ostotal,2); ?></td>
                    </tr>
                    <tr>
                      <td width="120"><strong>Others</strong></td>
                      <?
				   foreach ( $ms as $cs ) { ?>
                      <td><?                   echo number_format($cs->others,2);				   ?></td>
                      
                      <? $ottotal = $ottotal + $cs->others; } ?>
                       <td><?  echo number_format($ottotal,2); ?></td>
                    </tr>
                    <tr>
                      <td  width="120"><strong>Time Cost</strong></td>
                      <?
				   foreach ( $ms as $cs ) { ?>
                      <td><?
                  
				$ts{$cs->milestone_id} = $this->db->query("select sum(time_cost) as timecost from intg_milestones_users where milestones_id = '".$cs->milestone_id."'")->row()->timecost;
				  echo number_format($ts{$cs->milestone_id},2);
				   
				   
				   ?></td>
                     
                      <? $timetotal = $timetotal + $ts{$cs->milestone_id}; } ?>
                       <td><?  echo number_format($timetotal,2); ?></td>
                    </tr>
                    <tr>
                      <td  width="120"><strong>Total (RM)</strong></td>
                      <?
				   foreach ( $ms as $cs ) { ?>
                      <td><strong><?php
                  echo number_format($total{$cs->milestone_id} + $ts{$cs->milestone_id},2);
				   
				   ?>
                        </strong></td>
                   
                      <?  $gtotal = $gtotal + $total{$cs->milestone_id} + $ts{$cs->milestone_id};} ?>
                         <td><strong><? echo number_format($gtotal,2); ?></strong></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
			
            
          </div>
        </div>
      </div>
     
      <?php
				$noms= 1;
				$ms = $this->db->query("select * from intg_milestones where parent_Rid= 0 and project_id = '".$this->uri->segment(3)."'")->result();
				
					
				foreach ( $ms as $ml ) {
                                    
					$v =  $this->db->query("select max(m.version_no) as maxver from intg_milestones m where m.parent_Rid= '".$ml->milestone_id."' ")->row()->maxver;
                                        $mlid = $ml->milestone_id;
                                        
                                        if ( $v > 0 ) {
//                                         
                                                    $ml = $this->db->query("select * from intg_milestones where parent_Rid = '".$ml->milestone_id."' order by milestone_id desc limit 1")->row();
//                                                    echo "<br>Versioned id ".$ml->milestone_id."| Versioned status".$ml->status."| Versioned Parent m name".$ml->milestone_name;
                                                        $check = $this->db->query("select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1")->num_rows();
                                                    if($ml->status == "Reject"){
//                                                        echo "<br>select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1";
                                                        
//                                                        echo "Check ".$check;
                                                        if($check > 0){
                                                                $ml = $this->db->query("select * from intg_milestones where parent_Rid = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                                                        }else{
                                                                $ml = $this->db->query("select * from intg_milestones where milestone_id = '".$mlid."' AND status != 'Reject' order by milestone_id desc limit 1")->row();
                                                            
                                                        }
                                                        
                                                    }else{
                                                        
                                                    }

					}
				
				?>
      <div class="panel panel-default">
        <div class="panel-heading clearfix"><strong>Milestone
          <?=$noms?>
          : </strong>
          <?=ucfirst($ml->milestone_name)?>
           <strong class="m-l">Start Date: </strong>
          <?=date("d/m/Y",strtotime($ml->start_date))?>
           <strong class="m-l">End Date: </strong>
          <?=date("d/m/Y",strtotime($ml->end_date))?>
         
          <?php
            
            $rc_approval = $this->db->query("Select * from intg_milestone_approval where final_status = 'No'   AND project_id = '".$this->uri->segment(3)."' ");
            
		  switch($ml->status)
			   {
                                    case "No" : 
                                        if($rc_approval->num_rows() == 0)
                                        $status = "<span class='badge btn-warning m-l text-xs'>Pending</span>"; 
                                        else
                                        $status = "<span class='badge btn-success m-l text-xs'>Sent For Approval</span>"; 
                                            
                                        break;
                                    case "Yes" : 
                                        $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>"; 
                                        break;
                                    case "Reject" : 
                                        $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>"; 
                                        break;
			    }
				echo $status;
		  ?>
            
            
          <ul class="nav nav-pills nav-sm pull-left" style="padding:5px 20px">
              <?php if($rc_approval->num_rows() == 0){ ?>
                <li><a href="javascript:void(0)" class=" pull-right" milestone_id="<?=$ml->milestone_id?>" project_id="<?=$ml->project_id?>"></a></li>
              <?php } ?>
            <li class="hour " data-toggle="popover"  data-content='<textarea name="comments"  cols="30" rows="2" class="form-control expand"><?=$ml->remarks?></textarea><button class="btn btn-xs btn-primary cmpmark" style="margin-top:5px" rowid="<?=$ml->milestone_id?>">Submit</button>' data-html="true" data-placement="bottom" data-original-title="Mark As Complete" data-comments="<?=$ml->remarks?>"><!--<a href="javascript:void(0)"><i class="fa fa-info"></i></a>--></li>
            <?php 
				   if ( $v > 0 ) { 
				   ?>
            <li>
                                <?php 
                               
                                if($ml->version_no > 0 ){ 
                   $vno =  "<span class='badge bg-info text-xs'>V".$ml->version_no." Version History</span> ";
               }else{ 
                    $vno = "<span class='badge bg-info text-xs'>V0 Version History</span>";
               }
                                
                                if ($check == 0 ){ ?>
                        <a href="<?=base_url()?>index.php/admin/projectmgmt/projects/version_history/<?=$ml->milestone_id?>/<?=$ml->milestone_id?>/<?=$ml->project_id?>" data-toggle='ajaxModal'><?=$vno?></a>
                <?php }else{ ?>
                        <a href="<?=base_url()?>index.php/admin/projectmgmt/projects/version_history/<?=$ml->parent_Rid?>/<?=$ml->milestone_id?>/<?=$ml->project_id?>" data-toggle='ajaxModal'><?=$vno?></a>
               <?php } ?>
                
            </li>
            <?php } ?>
          </ul>
          <span id="mcostup<?=$ml->milestone_id?>" class="pull-right"></span> </div>
        <div id="ml<?=$ml->milestone_id?>" class="panel-collapse">
          <div class="panel-body text-sm">
            <div class="col-sm-16" id="lineitem_form">
              <header class="panel-heading font-bold bg-success" style="color: #ffffff;">Resources</header>
              <section class="panel panel-default">
                <div class="table-responsive ">
                  <table class="table table-striped b-t b-light text-sm">
                    <thead>
                      <tr>
                        <th>Level</th>
                        <th>Man-days</th>
                        <th>Head Count</th>
                        <th>Total Man Days</th>
                        <th>Daily FTE (RM)</th>
                        <th>Time Cost (RM)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $mu = $this->db->query("select *,imt.daily_fte as fte from intg_milestones_users imt,intg_user_level iul where imt.userlevel_id = iul.id  and imt.milestones_id='".$ml->milestone_id."'")->result(); 
					$milestone_cost = 0;
					foreach ( $mu as $u ) {
					
					?>
                      <tr >
                        <td><?=$u->user_level?></td>
                        <td><?=$u->man_days?></td>
                        <td><?=$u->head_count?></td>
                        <td><?=$u->total_man_days?></td>
                        <td><?=$u->fte?></td>
                        <td align="right"><?=number_format($u->time_cost,2)?></td>
                      </tr>
                      <?php $milestone_cost = $milestone_cost + $u->time_cost; } 
					  
					  ?>
                    </tbody>
                  </table>
                  <span class="mcostdown" rowid="<?=$ml->milestone_id?>" id="matcost<?=$ml->milestone_id?>" style="display:none"><?php echo $milestone_cost = $milestone_cost + $ml->material_cost + $ml->new_capex + $ml->outsourcing + $ml->others; ?></span> </div>
              </section>
              <header class="panel-heading font-bold bg-info" style="color: #ffffff;  margin-top:5px;">Cost Involved</header>
              <section class="panel panel-default">
                <div class="table-responsive ">
                  <table class="table table-striped b-t b-light text-sm">
                    <thead>
                      <tr>
                        <th>Material Cost (RM)</th>
                        <th>New CAPEX (RM)</th>
                        <th>Outsourcing (RM)</th>
                        <th>Others (RM)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr >
                        <td><?=$ml->material_cost?></td>
                        <td><?=$ml->new_capex?></td>
                        <td><?=$ml->outsourcing?></td>
                        <td><?=$ml->others?></td>
                      </tr>
                    </tbody>
                    
                    <thead>
                      <tr>
                        <th>Remarks</th>
                        <th>Remarks</th>
                        <th>Remarks</th>
                        <th>Remarks</th>                       
                      </tr>
                    </thead>
                    <tbody>
                   
                      <tr >
                        <td><?=$ml->mcost_desc?></td>
                        <td><?=$ml->ncapex_desc?></td>
                        <td><?=$ml->outsourcing_desc?></td>
                        <td><?=$ml->others_desc?></td>
                       
                      </tr>
                      
                    </tbody>
                    
                  </table>
                </div>
              </section>
            </div>
            <div class="form-group" style="margin-top:10px">
              <label for="projects_assigned_to" class="control-label"><strong>Task & Description</strong></label>
              <div class="controls pad">
              
              <?php
			  $tp = $this->db->query("select task_name,description from intg_task_pool itp,intg_milestones_tasks imt where imt.task_pool_id=itp.id  and imt.milestone_id = '".$ml->milestone_id."'")->result(); $no=1;
			  foreach ( $tp as $t ) {
			  ?>
                          <b><?=$no?>. <?=$t->task_name?> </b>  <?=$t->description?> <br>
              
              <?php $no++;} ?>
            </div>
            </div>
          </div>
        </div>
      </div>
      <?php				
	  
	 $noms++; 
	 
	 $milestone_cost = 0 ; 
	 
	 } 
	 
	
	 ?>
    </div>
	
  </div>

  </section>
  <!--end of col6--> 
  
</div>
<!--end of row--> 
<? if ( $pj->attachment !="" ) { ?>
      <h4 class="modal-title"><?=$att[1]?></h4>
             <img src="<?=base_url()?>uploads/<?=$att[0]?>" class="resimg"/>
 
<? } ?>
