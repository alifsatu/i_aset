<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Pool_Entry.Settings.View');
		$this->load->model('pool_entry_model', null, true);
		$this->lang->load('pool_entry');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('pool_entry', 'pool_entry.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->pool_entry_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('pool_entry_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('pool_entry_delete_failure') . $this->pool_entry_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('pool_entryfield',$this->input->post('select_field'));
$this->session->set_userdata('pool_entryfvalue',$this->input->post('field_value'));
$this->session->set_userdata('pool_entryfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('pool_entryfield');
$this->session->unset_userdata('pool_entryfvalue');
$this->session->unset_userdata('pool_entryfname');
break;
}

if ( $this->session->userdata('pool_entryfield')!='') { $field=$this->session->userdata('pool_entryfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('pool_entryfield') =='All Fields') 
{ 
 		
  $total = $this->pool_entry_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('pool_entryfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->pool_entry_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('pool_entryfvalue'),'both')  
  ->count_all();
}

		//$records = $this->pool_entry_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('pool_entryfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('pool_entryfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->pool_entry_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('pool_entryfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->pool_entry_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('pool_entry', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Pool Entry');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_pool_entry SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('pool_entry_success'), 'success');
				}
				else
				{
					Template::set_message(lang('pool_entry_restored_error'). $this->pool_entry_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_pool_entry where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('pool_entry_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('pool_entryfield',$this->input->post('select_field'));
$this->session->set_userdata('pool_entryfvalue',$this->input->post('field_value'));
$this->session->set_userdata('pool_entryfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('pool_entryfield');
$this->session->unset_userdata('pool_entryfvalue');
$this->session->unset_userdata('pool_entryfname');
break;
}


if ( $this->session->userdata('pool_entryfield')!='') { $field=$this->session->userdata('pool_entryfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('pool_entryfield') =='All Fields') 
{ 
 		
  $total = $this->pool_entry_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('pool_entryfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->pool_entry_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('pool_entryfvalue'),'both')  
  ->count_all();
}

//$records = $this-pool_entry_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('pool_entryfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('pool_entryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('pool_entryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('pool_entryfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->pool_entry_model
->where('deleted','1')
->likes($field,$this->session->userdata('pool_entryfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->pool_entry_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('pool_entry', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Pool Entry');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Pool Entry object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Pool_Entry.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_pool_entry())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('pool_entry_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'pool_entry');

				Template::set_message(lang('pool_entry_create_success'), 'success');
				redirect(SITE_AREA .'/settings/pool_entry');
			}
			else
			{
				Template::set_message(lang('pool_entry_create_failure') . $this->pool_entry_model->error, 'error');
			}
		}
		Assets::add_module_js('pool_entry', 'pool_entry.js');

		Template::set('toolbar_title', lang('pool_entry_create') . ' Pool Entry');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Pool Entry data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('pool_entry_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/pool_entry');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Pool_Entry.Settings.Edit');

			if ($this->save_pool_entry('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('pool_entry_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'pool_entry');

				Template::set_message(lang('pool_entry_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('pool_entry_edit_failure') . $this->pool_entry_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Pool_Entry.Settings.Delete');

			if ($this->pool_entry_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('pool_entry_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'pool_entry');

				Template::set_message(lang('pool_entry_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/pool_entry');
			}
			else
			{
				Template::set_message(lang('pool_entry_delete_failure') . $this->pool_entry_model->error, 'error');
			}
		}
		Template::set('pool_entry', $this->pool_entry_model->find($id));
		Template::set('toolbar_title', lang('pool_entry_edit') .' Pool Entry');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_pool_entry SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('pool_entry_success'), 'success');
											redirect(SITE_AREA .'/settings/pool_entry/deleted');
				}
				else
				{
					
					Template::set_message(lang('pool_entry_error') . $this->pool_entry_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_pool_entry where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('pool_entry_purged'), 'success');
					redirect(SITE_AREA .'/settings/pool_entry/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('pool_entry_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/pool_entry');
		}

		
		
		
		Template::set('pool_entry', $this->pool_entry_model->find($id));
		
		Assets::add_module_js('pool_entry', 'pool_entry.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Pool_entry');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_pool_entry($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['entry_name']        = $this->input->post('pool_entry_entry_name');
		$data['desc']        = $this->input->post('pool_entry_desc');

		if ($type == 'insert')
		{
			$id = $this->pool_entry_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->pool_entry_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}