
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/pool_entry') ?>"style="border-radius:0"  id="list"><?php echo lang('pool_entry_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Pool_Entry.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/pool_entry/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('pool_entry_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Pool_Entry.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/pool_entry/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
