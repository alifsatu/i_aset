<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($pool_entry))
{
	$pool_entry = (array) $pool_entry;
}
$id = isset($pool_entry['id']) ? $pool_entry['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Pool Entry</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('entry_name') ? 'error' : ''; ?>">
				<?php echo form_label('Pool Entry for Project Unit Code'. lang('bf_form_label_required'), 'pool_entry_entry_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='pool_entry_entry_name' class='input-sm input-s  form-control' type='text' name='pool_entry_entry_name' maxlength="255" value="<?php echo set_value('pool_entry_entry_name', isset($pool_entry['entry_name']) ? $pool_entry['entry_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('entry_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('desc') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'pool_entry_desc', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'pool_entry_desc', 'id' => 'pool_entry_desc', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('pool_entry_desc', isset($pool_entry['desc']) ? $pool_entry['desc'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('desc'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/pool_entry', lang('pool_entry_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Pool_Entry.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('pool_entry_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>