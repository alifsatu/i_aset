<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['pool_entry_manage']			= 'Manage';
$lang['pool_entry_edit']				= 'Edit';
$lang['pool_entry_true']				= 'True';
$lang['pool_entry_false']				= 'False';
$lang['pool_entry_create']			= 'Save';
$lang['pool_entry_list']				= 'List';
$lang['pool_entry_new']				= 'New';
$lang['pool_entry_edit_text']			= 'Edit this to suit your needs';
$lang['pool_entry_no_records']		= 'There aren\'t any pool_entry in the system.';
$lang['pool_entry_create_new']		= 'Create a new Pool Entry.';
$lang['pool_entry_create_success']	= 'Pool Entry successfully created.';
$lang['pool_entry_create_failure']	= 'There was a problem creating the pool_entry: ';
$lang['pool_entry_create_new_button']	= 'Create New Pool Entry';
$lang['pool_entry_invalid_id']		= 'Invalid Pool Entry ID.';
$lang['pool_entry_edit_success']		= 'Pool Entry successfully saved.';
$lang['pool_entry_edit_failure']		= 'There was a problem saving the pool_entry: ';
$lang['pool_entry_delete_success']	= 'record(s) successfully deleted.';

$lang['pool_entry_purged']	= 'record(s) successfully purged.';
$lang['pool_entry_success']	= 'record(s) successfully restored.';


$lang['pool_entry_delete_failure']	= 'We could not delete the record: ';
$lang['pool_entry_delete_error']		= 'You have not selected any records to delete.';
$lang['pool_entry_actions']			= 'Actions';
$lang['pool_entry_cancel']			= 'Cancel';
$lang['pool_entry_delete_record']		= 'Delete';
$lang['pool_entry_delete_confirm']	= 'Are you sure you want to delete this pool_entry?';
$lang['pool_entry_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['pool_entry_action_edit']		= 'Save';
$lang['pool_entry_action_create']		= 'Create';

// Activities
$lang['pool_entry_act_create_record']	= 'Created record with ID';
$lang['pool_entry_act_edit_record']	= 'Updated record with ID';
$lang['pool_entry_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['pool_entry_column_created']	= 'Created';
$lang['pool_entry_column_deleted']	= 'Deleted';
$lang['pool_entry_column_modified']	= 'Modified';
