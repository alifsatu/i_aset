<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['sejarah_aset_manage']			= 'Manage';
$lang['sejarah_aset_edit']				= 'Edit';
$lang['sejarah_aset_true']				= 'True';
$lang['sejarah_aset_false']				= 'False';
$lang['sejarah_aset_create']			= 'Save';
$lang['sejarah_aset_list']				= 'List';
$lang['sejarah_aset_new']				= 'New';
$lang['sejarah_aset_edit_text']			= 'Edit this to suit your needs';
$lang['sejarah_aset_no_records']		= 'There aren\'t any sejarah_aset in the system.';
$lang['sejarah_aset_create_new']		= 'Create a new Sejarah Aset.';
$lang['sejarah_aset_create_success']	= 'Sejarah Aset successfully created.';
$lang['sejarah_aset_create_failure']	= 'There was a problem creating the sejarah_aset: ';
$lang['sejarah_aset_create_new_button']	= 'Create New Sejarah Aset';
$lang['sejarah_aset_invalid_id']		= 'Invalid Sejarah Aset ID.';
$lang['sejarah_aset_edit_success']		= 'Sejarah Aset successfully saved.';
$lang['sejarah_aset_edit_failure']		= 'There was a problem saving the sejarah_aset: ';
$lang['sejarah_aset_delete_success']	= 'record(s) successfully deleted.';

$lang['sejarah_aset_purged']	= 'record(s) successfully purged.';
$lang['sejarah_aset_success']	= 'record(s) successfully restored.';


$lang['sejarah_aset_delete_failure']	= 'We could not delete the record: ';
$lang['sejarah_aset_delete_error']		= 'You have not selected any records to delete.';
$lang['sejarah_aset_actions']			= 'Actions';
$lang['sejarah_aset_cancel']			= 'Cancel';
$lang['sejarah_aset_delete_record']		= 'Delete';
$lang['sejarah_aset_delete_confirm']	= 'Are you sure you want to delete this sejarah_aset?';
$lang['sejarah_aset_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['sejarah_aset_action_edit']		= 'Save';
$lang['sejarah_aset_action_create']		= 'Create';

// Activities
$lang['sejarah_aset_act_create_record']	= 'Created record with ID';
$lang['sejarah_aset_act_edit_record']	= 'Updated record with ID';
$lang['sejarah_aset_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['sejarah_aset_column_created']	= 'Created';
$lang['sejarah_aset_column_deleted']	= 'Deleted';
$lang['sejarah_aset_column_modified']	= 'Modified';
