<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * aset controller
 */
class aset extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Sejarah_Aset.Aset.View');
        $this->load->model('sejarah_aset_model', null, true);
        $this->lang->load('sejarah_aset');

        Assets::add_css("js/datepicker/datepicker.css");
        Assets::add_css("js/datatables/datatables.min.css");

        Assets::add_js("js/datepicker/bootstrap-datepicker.js");
        Assets::add_js("js/libs/moment.min.js");

        //alif

        Assets::add_js(array(Template::theme_url('js/datatables/datatables.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.flash.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/JSZip-2.5.0/jszip.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/pdfmake.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/vfs_fonts.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.html5.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.print.min.js')));

        //ckeditor
        Assets::add_js(Template::theme_url('js/ckeditor/ckeditor'));

        Template::set_block('sub_nav', 'aset/_sub_nav');

        Assets::add_module_js('sejarah_aset', 'sejarah_aset.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->sejarah_aset_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('sejarah_aset_delete_success'), 'success');
                } else {
                    Template::set_message(lang('sejarah_aset_delete_failure') . $this->sejarah_aset_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sejarah_asetfield', $this->input->post('select_field'));
                $this->session->set_userdata('sejarah_asetfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sejarah_asetfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('sejarah_asetfield');
                $this->session->unset_userdata('sejarah_asetfvalue');
                $this->session->unset_userdata('sejarah_asetfname');
                break;
        }

        if ($this->session->userdata('sejarah_asetfield') != '') {
            $field = $this->session->userdata('sejarah_asetfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('sejarah_asetfield') == 'All Fields') {

            $total = $this->sejarah_aset_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->sejarah_aset_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->sejarah_aset_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('sejarah_asetfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->sejarah_aset_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->sejarah_aset_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);

        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('sejarah_aset', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Sejarah Aset');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_sejarah_aset SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('sejarah_aset_success'), 'success');
                } else {
                    Template::set_message(lang('sejarah_aset_restored_error') . $this->sejarah_aset_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_sejarah_aset where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('sejarah_aset_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sejarah_asetfield', $this->input->post('select_field'));
                $this->session->set_userdata('sejarah_asetfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sejarah_asetfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('sejarah_asetfield');
                $this->session->unset_userdata('sejarah_asetfvalue');
                $this->session->unset_userdata('sejarah_asetfname');
                break;
        }


        if ($this->session->userdata('sejarah_asetfield') != '') {
            $field = $this->session->userdata('sejarah_asetfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('sejarah_asetfield') == 'All Fields') {

            $total = $this->sejarah_aset_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->sejarah_aset_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-sejarah_aset_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('sejarah_asetfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->sejarah_aset_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('sejarah_asetfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->sejarah_aset_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('sejarah_aset', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Sejarah Aset');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Sejarah Aset object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Sejarah_Aset.Aset.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_sejarah_aset()) {
                // Log the activity
                log_activity($this->current_user->id, lang('sejarah_aset_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'sejarah_aset');

                Template::set_message(lang('sejarah_aset_create_success'), 'success');
                redirect(SITE_AREA . '/aset/sejarah_aset');
            } else {
                Template::set_message(lang('sejarah_aset_create_failure') . $this->sejarah_aset_model->error, 'error');
            }
        }
        Assets::add_module_js('sejarah_aset', 'sejarah_aset.js');

        Template::set('toolbar_title', lang('sejarah_aset_create') . ' Sejarah Aset');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Sejarah Aset data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('sejarah_aset_invalid_id'), 'error');
            redirect(SITE_AREA . '/aset/sejarah_aset');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Sejarah_Aset.Aset.Edit');

            if ($this->save_sejarah_aset('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('sejarah_aset_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'sejarah_aset');

                Template::set_message(lang('sejarah_aset_edit_success'), 'success');
            } else {
                Template::set_message(lang('sejarah_aset_edit_failure') . $this->sejarah_aset_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Sejarah_Aset.Aset.Delete');

            if ($this->sejarah_aset_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('sejarah_aset_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'sejarah_aset');

                Template::set_message(lang('sejarah_aset_delete_success'), 'success');

                redirect(SITE_AREA . '/aset/sejarah_aset');
            } else {
                Template::set_message(lang('sejarah_aset_delete_failure') . $this->sejarah_aset_model->error, 'error');
            }
        }
        Template::set('sejarah_aset', $this->sejarah_aset_model->find($id));
        Template::set('toolbar_title', lang('sejarah_aset_edit') . ' Sejarah Aset');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_sejarah_aset SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('sejarah_aset_success'), 'success');
                redirect(SITE_AREA . '/aset/sejarah_aset/deleted');
            } else {

                Template::set_message(lang('sejarah_aset_error') . $this->sejarah_aset_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_sejarah_aset where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('sejarah_aset_purged'), 'success');
                redirect(SITE_AREA . '/aset/sejarah_aset/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('sejarah_aset_invalid_id'), 'error');
            redirect(SITE_AREA . '/aset/sejarah_aset');
        }




        Template::set('sejarah_aset', $this->sejarah_aset_model->find($id));

        Assets::add_module_js('sejarah_aset', 'sejarah_aset.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Sejarah_aset');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_sejarah_aset($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['aset_id'] = $this->input->post('sejarah_aset_aset_id');
        $data['month_year'] = $this->input->post('sejarah_aset_month_year');
        $data['used_life'] = $this->input->post('sejarah_aset_used_life');
        $data['annual_depreciation'] = $this->input->post('sejarah_aset_annual_depreciation');
        $data['aggregate_depreciation'] = $this->input->post('sejarah_aset_aggregate_depreciation');
        $data['net_book_value'] = $this->input->post('sejarah_aset_net_book_value');
        $data['remarks'] = $this->input->post('sejarah_aset_remarks');
        $data['remaining_life'] = $this->input->post('sejarah_aset_remaining_life');

        if ($type == 'insert') {
            $id = $this->sejarah_aset_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->sejarah_aset_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
