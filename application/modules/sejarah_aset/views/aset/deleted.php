<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 

echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>Sejarah Aset</h4></div>
 <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

<? if ( $this->session->userdata('sejarah_asetfield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('sejarah_asetfield')?>"><?=$this->session->userdata('sejarah_asetfield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_sejarah_aset')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>
</div><div class="col-md-3"><div class="input-group">
<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('sejarah_asetfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('sejarah_asetfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('sejarah_asetfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

<?php echo form_close(); ?>
     <div class="table-responsive">   
  


	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_aset_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_aset_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_aset_id') ? 'desc' : 'asc'); ?>'>
                    Aset</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_month_year') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_month_year&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_month_year') ? 'desc' : 'asc'); ?>'>
                    Bulan-Tahun</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_used_life') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_used_life&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_used_life') ? 'desc' : 'asc'); ?>'>
                    Used Life</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_annual_depreciation') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_annual_depreciation&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_annual_depreciation') ? 'desc' : 'asc'); ?>'>
                    Annual Depreciation</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_aggregate_depreciation') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_aggregate_depreciation&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_aggregate_depreciation') ? 'desc' : 'asc'); ?>'>
                    Aggregate Depreciation</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_net_book_value') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_net_book_value&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_net_book_value') ? 'desc' : 'asc'); ?>'>
                    Net Book Value</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_remarks') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_remarks&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_remarks') ? 'desc' : 'asc'); ?>'>
                    Remarks</a></th>
					<th <?php if ($this->input->get('sort_by') == 'sejarah_aset'.'_remaining_life') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/aset/sejarah_aset?sort_by=sejarah_aset_remaining_life&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sejarah_aset'.'_remaining_life') ? 'desc' : 'asc'); ?>'>
                    Remaining Life</a></th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Delete')) : ?>
				<tr>
					<td colspan="11">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
				<!--	<input type="submit" name="purge" class="btn btn-danger" value="<?php //echo lang('bf_action_purge') ?>" onclick="return confirm('<?php //echo lang('us_purge_del_confirm'); ?>')">-->
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) :  $no = $this->input->get('per_page')+1;?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/aset/sejarah_aset/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->aset_id) ?></td>
				<?php else: ?>
				<td><?php echo $record->aset_id ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->month_year?></td>
				<td><?php echo $record->used_life?></td>
				<td><?php echo $record->annual_depreciation?></td>
				<td><?php echo $record->aggregate_depreciation?></td>
				<td><?php echo $record->net_book_value?></td>
				<td><?php echo $record->remarks?></td>
				<td><?php echo $record->remaining_life?></td>
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
			<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
			<?php $no++; endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="11">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         
	<?php echo form_close(); ?>
  <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>