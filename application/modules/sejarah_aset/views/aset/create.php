<?php
$asset_list = get_asset_list();


$vo = isset($view_only) ? $view_only : false;
$eo = isset($edit_only) ? $edit_only : false;

$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($sejarah_aset)) {
    $sejarah_aset = (array) $sejarah_aset;
}
$id = isset($sejarah_aset['id']) ? $sejarah_aset['id'] : '';
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Sejarah Aset</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <?php
// Change the values in this array to populate your dropdown as required
//                    $options = array(
//                        11 => 11,
//                    );
//                    echo form_dropdown('sejarah_aset_aset_id', $options, set_value('sejarah_aset_aset_id', isset($sejarah_aset['aset_id']) ? $sejarah_aset['aset_id'] : ''), 'Aset' . lang('bf_form_label_required'));
                    ?>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><?= 'Aset' ?></label>
                                <div class='controls'>
                                    <?php
                                    if ($vo) {
                                        foreach ($asset_list as $key => $val) {
                                            if ($key == $sejarah_aset['aset_id']) {
                                                echo $val;
                                            }
                                        }
                                    } else {
                                        ?>
                                        <select name="sejarah_aset_aset_id" id="sejarah_aset_aset_id" class="form-control selecta"  >
                                            <option value="">--Sila Pilih--</option>
                                            <?php
                                            foreach ($asset_list as $key => $val) {
                                                if ($key == $sejarah_aset['aset_id']) {
                                                    ?>
                                                    <option selected="selected" value="<?= $key ?>"><?= $val ?></option>
                                                <?php } else { ?>
                                                    <option value="<?= $key ?>"><?= $val; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label><?= 'Bulan-Tahun' . lang('bf_form_label_required') ?> (<em style='font-size: 9px'>Data as of bulan dan tahun berapa</em>)</label>
                                <div class='controls'>
                                    <?php
                                    if ($vo) {
                                        echo isset($sejarah_aset['tarikh_terima_permohonan']) ? $sejarah_aset['month_year'] : 'N/A';
                                    } else {
                                        ?>
                                        <input    class="datepicker-input form-control " data-date-format="mm-yyyy"  id='sejarah_aset_month_year' type='text' name='sejarah_aset_month_year'  value="<?php echo isset($sejarah_aset['month_year']) ? $sejarah_aset['month_year'] : ''; ?>"  data-required="true" readonly="readonly" required="true"/>
                                    <?php } ?>
                                </div>
                            </div>


                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group <?php echo form_error('remaining_life') ? 'error' : ''; ?>">
                                <?php echo form_label('Remaining Life', 'sejarah_aset_remaining_life', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='sejarah_aset_remaining_life' class='input-sm input-s  form-control' type='text' name='sejarah_aset_remaining_life' maxlength="30" value="<?php echo set_value('sejarah_aset_remaining_life', isset($sejarah_aset['remaining_life']) ? $sejarah_aset['remaining_life'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('remaining_life'); ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group <?php echo form_error('used_life') ? 'error' : ''; ?>">
                                <?php echo form_label('Used Life', 'sejarah_aset_used_life', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='sejarah_aset_used_life' class='input-sm input-s  form-control' type='text' name='sejarah_aset_used_life' maxlength="30" value="<?php echo set_value('sejarah_aset_used_life', isset($sejarah_aset['used_life']) ? $sejarah_aset['used_life'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('used_life'); ?></span>
                                </div>
                            </div>
                        </div>

                        

                    </div>
                    <div class="row">
                        
                        <div class="col-md-3">

                            <div class="form-group <?php echo form_error('annual_depreciation') ? 'error' : ''; ?>">
                                <?php echo form_label('Annual Depreciation (RM)', 'sejarah_aset_annual_depreciation', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='sejarah_aset_annual_depreciation' class='input-sm input-s  form-control' type='text' name='sejarah_aset_annual_depreciation' maxlength="30" value="<?php echo set_value('sejarah_aset_annual_depreciation', isset($sejarah_aset['annual_depreciation']) ? $sejarah_aset['annual_depreciation'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('annual_depreciation'); ?></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group <?php echo form_error('aggregate_depreciation') ? 'error' : ''; ?>">
                                <?php echo form_label('Aggregate Depreciation (RM)', 'sejarah_aset_aggregate_depreciation', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='sejarah_aset_aggregate_depreciation' class='input-sm input-s  form-control' type='text' name='sejarah_aset_aggregate_depreciation' maxlength="30" value="<?php echo set_value('sejarah_aset_aggregate_depreciation', isset($sejarah_aset['aggregate_depreciation']) ? $sejarah_aset['aggregate_depreciation'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('aggregate_depreciation'); ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group <?php echo form_error('net_book_value') ? 'error' : ''; ?>">
                                <?php echo form_label('Net Book Value (RM)', 'sejarah_aset_net_book_value', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='sejarah_aset_net_book_value' class='input-sm input-s  form-control' type='text' name='sejarah_aset_net_book_value' maxlength="30" value="<?php echo set_value('sejarah_aset_net_book_value', isset($sejarah_aset['net_book_value']) ? $sejarah_aset['net_book_value'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('net_book_value'); ?></span>
                                </div>
                            </div>
                        </div>



                        

                        <div class="col-md-3">
                            <div class="form-group <?php echo form_error('remarks') ? 'error' : ''; ?>">
                                <?php echo form_label('Remarks', 'sejarah_aset_remarks', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <?php echo form_textarea(array('name' => 'sejarah_aset_remarks', 'id' => 'sejarah_aset_remarks', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('sejarah_aset_remarks', isset($sejarah_aset['remarks']) ? $sejarah_aset['remarks'] : ''))); ?>
                                    <span class='help-inline'><?php echo form_error('remarks'); ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-actions">
                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('sejarah_aset_action_create'); ?>"  />
                            &nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/aset/sejarah_aset', lang('sejarah_aset_cancel'), 'class="btn btn-warning"'); ?>

                        </div>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>