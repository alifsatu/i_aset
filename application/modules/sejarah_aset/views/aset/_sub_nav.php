
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/aset/sejarah_aset') ?>"style="border-radius:0"  id="list"><?php echo lang('sejarah_aset_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/aset/sejarah_aset/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('sejarah_aset_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Sejarah_Aset.Aset.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/aset/sejarah_aset/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
