<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($subsidiary))
{
	$subsidiary = (array) $subsidiary;
}
$id = isset($subsidiary['id']) ? $subsidiary['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Subsidiary</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('subsidiary_name') ? 'error' : ''; ?>">
				<?php echo form_label('Subsidiary Name'. lang('bf_form_label_required'), 'subsidiary_subsidiary_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='subsidiary_subsidiary_name' class='input-sm input-s  form-control' type='text' name='subsidiary_subsidiary_name' maxlength="500" value="<?php echo set_value('subsidiary_subsidiary_name', isset($subsidiary['subsidiary_name']) ? $subsidiary['subsidiary_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('subsidiary_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'subsidiary_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'subsidiary_description', 'id' => 'subsidiary_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('subsidiary_description', isset($subsidiary['description']) ? $subsidiary['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('subsidiary_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/subsidiary', lang('subsidiary_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>