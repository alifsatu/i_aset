<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Subsidiary.Settings.View');
		$this->load->model('subsidiary_model', null, true);
		$this->lang->load('subsidiary');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('subsidiary', 'subsidiary.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->subsidiary_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('subsidiary_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('subsidiary_delete_failure') . $this->subsidiary_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('subsidiaryfield',$this->input->post('select_field'));
$this->session->set_userdata('subsidiaryfvalue',$this->input->post('field_value'));
$this->session->set_userdata('subsidiaryfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('subsidiaryfield');
$this->session->unset_userdata('subsidiaryfvalue');
$this->session->unset_userdata('subsidiaryfname');
break;
}

if ( $this->session->userdata('subsidiaryfield')!='') { $field=$this->session->userdata('subsidiaryfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('subsidiaryfield') =='All Fields') 
{ 
 		
  $total = $this->subsidiary_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->subsidiary_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('subsidiaryfvalue'),'both')  
  ->count_all();
}

		//$records = $this->subsidiary_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('subsidiaryfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('subsidiaryfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->subsidiary_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('subsidiaryfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->subsidiary_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('subsidiary', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage subsidiary');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_subsidiary SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('subsidiary_success'), 'success');
				}
				else
				{
					Template::set_message(lang('subsidiary_restored_error'). $this->subsidiary_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_subsidiary where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('subsidiary_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('subsidiaryfield',$this->input->post('select_field'));
$this->session->set_userdata('subsidiaryfvalue',$this->input->post('field_value'));
$this->session->set_userdata('subsidiaryfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('subsidiaryfield');
$this->session->unset_userdata('subsidiaryfvalue');
$this->session->unset_userdata('subsidiaryfname');
break;
}


if ( $this->session->userdata('subsidiaryfield')!='') { $field=$this->session->userdata('subsidiaryfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('subsidiaryfield') =='All Fields') 
{ 
 		
  $total = $this->subsidiary_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->subsidiary_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('subsidiaryfvalue'),'both')  
  ->count_all();
}

//$records = $this-subsidiary_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('subsidiaryfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('subsidiaryfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('subsidiaryfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('subsidiaryfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->subsidiary_model
->where('deleted','1')
->likes($field,$this->session->userdata('subsidiaryfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->subsidiary_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('subsidiary', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Subsidiary');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a subsidiary object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Subsidiary.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_subsidiary())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('subsidiary_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'subsidiary');

				Template::set_message(lang('subsidiary_create_success'), 'success');
				redirect(SITE_AREA .'/settings/subsidiary');
			}
			else
			{
				Template::set_message(lang('subsidiary_create_failure') . $this->subsidiary_model->error, 'error');
			}
		}
		Assets::add_module_js('subsidiary', 'subsidiary.js');

		Template::set('toolbar_title', lang('subsidiary_create') . ' subsidiary');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of subsidiary data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('subsidiary_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/subsidiary');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Subsidiary.Settings.Edit');

			if ($this->save_subsidiary('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('subsidiary_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'subsidiary');

				Template::set_message(lang('subsidiary_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('subsidiary_edit_failure') . $this->subsidiary_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Subsidiary.Settings.Delete');

			if ($this->subsidiary_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('subsidiary_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'subsidiary');

				Template::set_message(lang('subsidiary_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/subsidiary');
			}
			else
			{
				Template::set_message(lang('subsidiary_delete_failure') . $this->subsidiary_model->error, 'error');
			}
		}
		Template::set('subsidiary', $this->subsidiary_model->find($id));
		Template::set('toolbar_title', lang('subsidiary_edit') .' subsidiary');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_subsidiary SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('subsidiary_success'), 'success');
											redirect(SITE_AREA .'/settings/subsidiary/deleted');
				}
				else
				{
					
					Template::set_message(lang('subsidiary_error') . $this->subsidiary_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_subsidiary where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('subsidiary_purged'), 'success');
					redirect(SITE_AREA .'/settings/subsidiary/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('subsidiary_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/subsidiary');
		}

		
		
		
		Template::set('subsidiary', $this->subsidiary_model->find($id));
		
		Assets::add_module_js('subsidiary', 'subsidiary.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Subsidiary');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_subsidiary($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['subsidiary_name']        = $this->input->post('subsidiary_subsidiary_name');
		$data['description']        = $this->input->post('subsidiary_description');

		if ($type == 'insert')
		{
			$id = $this->subsidiary_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->subsidiary_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}