<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['subsidiary_manage']			= 'Manage';
$lang['subsidiary_edit']				= 'Edit';
$lang['subsidiary_true']				= 'True';
$lang['subsidiary_false']				= 'False';
$lang['subsidiary_create']			= 'Save';
$lang['subsidiary_list']				= 'List';
$lang['subsidiary_new']				= 'New';
$lang['subsidiary_edit_text']			= 'Edit this to suit your needs';
$lang['subsidiary_no_records']		= 'There aren\'t any subsidiary in the system.';
$lang['subsidiary_create_new']		= 'Create a new subsidiary.';
$lang['subsidiary_create_success']	= 'subsidiary successfully created.';
$lang['subsidiary_create_failure']	= 'There was a problem creating the subsidiary: ';
$lang['subsidiary_create_new_button']	= 'Create new subsidiary';
$lang['subsidiary_invalid_id']		= 'Invalid subsidiary ID.';
$lang['subsidiary_edit_success']		= 'Subsidiary successfully saved.';
$lang['subsidiary_edit_failure']		= 'There was a problem saving the subsidiary: ';
$lang['subsidiary_delete_success']	= 'record(s) successfully deleted.';

$lang['subsidiary_purged']	= 'record(s) successfully purged.';
$lang['subsidiary_success']	= 'record(s) successfully restored.';


$lang['subsidiary_delete_failure']	= 'We could not delete the record: ';
$lang['subsidiary_delete_error']		= 'You have not selected any records to delete.';
$lang['subsidiary_actions']			= 'Actions';
$lang['subsidiary_cancel']			= 'Cancel';
$lang['subsidiary_delete_record']		= 'Delete';
$lang['subsidiary_delete_confirm']	= 'Are you sure you want to delete this subsidiary?';
$lang['subsidiary_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['subsidiary_action_edit']		= 'Save';
$lang['subsidiary_action_create']		= 'Create';

// Activities
$lang['subsidiary_act_create_record']	= 'Created record with ID';
$lang['subsidiary_act_edit_record']	= 'Updated record with ID';
$lang['subsidiary_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['subsidiary_column_created']	= 'Created';
$lang['subsidiary_column_deleted']	= 'Deleted';
$lang['subsidiary_column_modified']	= 'Modified';
