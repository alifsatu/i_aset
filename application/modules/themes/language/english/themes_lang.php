<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['themes_manage']			= 'Manage';
$lang['themes_edit']				= 'Edit';
$lang['themes_true']				= 'True';
$lang['themes_false']				= 'False';
$lang['themes_create']			= 'Save';
$lang['themes_list']				= 'List';
$lang['themes_new']				= 'New';
$lang['themes_edit_text']			= 'Edit this to suit your needs';
$lang['themes_no_records']		= 'There aren\'t any themes in the system.';
$lang['themes_create_new']		= 'Create a new Themes.';
$lang['themes_create_success']	= 'Themes successfully created.';
$lang['themes_create_failure']	= 'There was a problem creating the themes: ';
$lang['themes_create_new_button']	= 'Create New Themes';
$lang['themes_invalid_id']		= 'Invalid Themes ID.';
$lang['themes_edit_success']		= 'Themes successfully saved.';
$lang['themes_edit_failure']		= 'There was a problem saving the themes: ';
$lang['themes_delete_success']	= 'record(s) successfully deleted.';

$lang['themes_purged']	= 'record(s) successfully purged.';
$lang['themes_success']	= 'record(s) successfully restored.';


$lang['themes_delete_failure']	= 'We could not delete the record: ';
$lang['themes_delete_error']		= 'You have not selected any records to delete.';
$lang['themes_actions']			= 'Actions';
$lang['themes_cancel']			= 'Cancel';
$lang['themes_delete_record']		= 'Delete';
$lang['themes_delete_confirm']	= 'Are you sure you want to delete this themes?';
$lang['themes_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['themes_action_edit']		= 'Save';
$lang['themes_action_create']		= 'Create';

// Activities
$lang['themes_act_create_record']	= 'Created record with ID';
$lang['themes_act_edit_record']	= 'Updated record with ID';
$lang['themes_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['themes_column_created']	= 'Created';
$lang['themes_column_deleted']	= 'Deleted';
$lang['themes_column_modified']	= 'Modified';
