<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($themes))
{
	$themes = (array) $themes;
}
$id = isset($themes['id']) ? $themes['id'] : '';

?>
 <div class="row">
                <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading clearfix">Themes</header>
                    <div class="panel-body">
                          

	<?php echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>
    	<fieldset>
        
	

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'themes_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='themes_name' type='text' class="input-sm input-s  form-control" name='themes_name' maxlength="100" value="<?php echo set_value('themes_name', isset($themes['name']) ? $themes['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'themes_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'themes_description', 'id' => 'themes_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('themes_description', isset($themes['description']) ? $themes['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('active') ? 'error' : ''; ?>">
				<?php echo form_label('Active'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'themes_active_label') ); ?>
				<div class='controls' aria-labelled-by='themes_active_label'>
					
				
						
                    
                      <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-sm btn-info">
                    <input type="radio" name="themes_active" id="themes_active_option1" value="YES"> <i class="fa fa-check text-active"></i>YES
                  </label>
                  <label class="btn btn-sm btn-success active">
                    <input type="radio" name="themes_active" id="themes_active_option2" value="NO"> <i class="fa fa-check text-active"></i>NO
                  </label>
                 
                </div>
					<span class='help-inline'><?php echo form_error('active'); ?></span>
				</div>
			</div>
          

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('themes_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/themes', lang('themes_cancel'), 'class="btn btn-warning"'); ?>
				
			 </div>
					
					</fieldset>
					<?php echo form_close(); ?>
                    
                    
                    </div>
                    </section>
			</div><!--end of col6-->
		
    
</div> <!--end of row-->
