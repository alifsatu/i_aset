<ul class="nav nav-tabs nav-white">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/settings/themes') ?>"style="border-radius:0"  id="list"><?php echo lang('themes_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Themes.Settings.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/themes/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('themes_new'); ?></a>
	</li>
	<?php endif; ?>
     <?php if ($this->auth->has_permission('Themes.Settings.Deleted')) : ?>
    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/settings/themes/deleted') ?>" id="deleted">Deleted</a>
	</li>
    <?php endif; ?>
</ul>