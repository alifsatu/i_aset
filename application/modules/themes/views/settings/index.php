<?php

$num_columns	= 7;
$can_delete	= $this->auth->has_permission('Themes.Settings.Delete');
$can_edit		= $this->auth->has_permission('Themes.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
 <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">

	
    <?php 
    
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">  <div class="col-md-8">  <h4>THEMES</h4></div>
	<div class="col-md-2"><select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">
<!--<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>
-->
<option>All Fields</option>		
<option value="personal_particulars_name">Name</option>
<option value="personal_particulars_nric">NRIC</option>
<option value="personal_particulars_htel">Handphone</option>
<option value="personal_particulars_email">Email</option>
<option value="personal_particulars_citizenship">Citizenship</option>
</select></div><div class="col-md-2"><div class="input-group">
   <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>
  <span class="input-group-btn">
                          
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><i class="fa fa-refresh"></i></button>

                          </span>
                        </div></div>
      </div>


 <?php echo form_close(); ?>   
 
 
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                    Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'description') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_active') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=active&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'active') ? 'desc' : 'asc'); ?>'>
                    Active</a></th>
					<th><?php echo lang("themes_column_deleted"); ?></th>
					<th><?php echo lang("themes_column_created"); ?></th>
					<th><?php echo lang("themes_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('themes_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/themes/edit/' . $record->id, '<span class="icon-pencil"></span>' .  $record->name); ?></td>
				<?php else : ?>
					<td><?php e($record->name); ?></td>
				<?php endif; ?>
					<td><?php e($record->description) ?></td>
					<td><?php e($record->active) ?></td>
					<td><?php echo $record->deleted > 0 ? lang('themes_true') : lang('themes_false')?></td>
					<td><?php e($record->created_on) ?></td>
					<td><?php e($record->modified_on) ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
        
  <?php echo form_close(); ?> 
  
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div> </section>
