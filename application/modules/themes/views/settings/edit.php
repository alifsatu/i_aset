<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($themes))
{
	$themes = (array) $themes;
}
$id = isset($themes['id']) ? $themes['id'] : '';

?>
<div class="row">
	<h3>Themes</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'themes_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='themes_name' type='text' name='themes_name' maxlength="100" value="<?php echo set_value('themes_name', isset($themes['name']) ? $themes['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'themes_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'themes_description', 'id' => 'themes_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('themes_description', isset($themes['description']) ? $themes['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('active') ? 'error' : ''; ?>">
				<?php echo form_label('Active'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'themes_active_label') ); ?>
				<div class='controls' aria-labelled-by='themes_active_label'>
					<label class='radio' for='themes_active_option1'>
						<input id='themes_active_option1' name='themes_active' type='radio' class='' value='YES' <?php if ( $themes['active'] =='YES') {?> checked="checked"<? }?> />
						YES
					</label>
					<label class='radio' for='themes_active_option2'>
						<input id='themes_active_option2' name='themes_active' type='radio' class='' value='NO' <<?php if ( $themes['active'] =='NO') {?> checked="checked"<? }?> />
						NO
					</label>
					<span class='help-inline'><?php echo form_error('active'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('themes_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/themes', lang('themes_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Themes.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('themes_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('themes_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>