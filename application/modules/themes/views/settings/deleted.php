<?php

$num_columns	= 7;
$can_delete	= $this->auth->has_permission('Themes.Settings.Delete');
$can_edit		= $this->auth->has_permission('Themes.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
 <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">

	
    <?php 
    
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">  <div class="col-md-8">  <h4>THEMES</h4></div>
	<div class="col-md-2"><select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">
<!--<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>
-->
<option>All Fields</option>		
<option value="personal_particulars_name">Name</option>
<option value="personal_particulars_nric">NRIC</option>
<option value="personal_particulars_htel">Handphone</option>
<option value="personal_particulars_email">Email</option>
<option value="personal_particulars_citizenship">Citizenship</option>
</select></div><div class="col-md-2"><div class="input-group">
   <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>
  <span class="input-group-btn">
                          
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><i class="fa fa-refresh"></i></button>

                          </span>
                        </div></div>
      </div>


 <?php echo form_close(); ?>   
	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Themes.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					<th <?php if ($this->input->get('sort_by') == 'themes'.'_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=themes_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'themes'.'_name') ? 'desc' : 'asc'); ?>'>
                    Name</a></th>
					<th <?php if ($this->input->get('sort_by') == 'themes'.'_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=themes_description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'themes'.'_description') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
					<th <?php if ($this->input->get('sort_by') == 'themes'.'_active') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/themes?sort_by=themes_active&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'themes'.'_active') ? 'desc' : 'asc'); ?>'>
                    Active</a></th>
					<th>Deleted</th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Themes.Settings.Delete')) : ?>
				<tr>
					<td colspan="7">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Themes.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Themes.Settings.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/settings/themes/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->name) ?></td>
				<?php else: ?>
				<td><?php echo $record->name ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->description?></td>
				<td><?php echo $record->active?></td>
				<td><?php echo $record->deleted > 0 ? lang('themes_true') : lang('themes_false')?></td>
				<td><?php echo $record->created_on?></td>
				<td><?php echo $record->modified_on?></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="7">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
          <?php  //echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div> </section>