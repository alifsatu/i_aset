<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['status_permohonan_manage']			= 'Manage';
$lang['status_permohonan_edit']				= 'Edit';
$lang['status_permohonan_true']				= 'True';
$lang['status_permohonan_false']				= 'False';
$lang['status_permohonan_create']			= 'Save';
$lang['status_permohonan_list']				= 'List';
$lang['status_permohonan_new']				= 'New';
$lang['status_permohonan_edit_text']			= 'Edit this to suit your needs';
$lang['status_permohonan_no_records']		= 'There aren\'t any status_permohonan in the system.';
$lang['status_permohonan_create_new']		= 'Create a new Status Permohonan.';
$lang['status_permohonan_create_success']	= 'Status Permohonan successfully created.';
$lang['status_permohonan_create_failure']	= 'There was a problem creating the status_permohonan: ';
$lang['status_permohonan_create_new_button']	= 'Create New Status Permohonan';
$lang['status_permohonan_invalid_id']		= 'Invalid Status Permohonan ID.';
$lang['status_permohonan_edit_success']		= 'Status Permohonan successfully saved.';
$lang['status_permohonan_edit_failure']		= 'There was a problem saving the status_permohonan: ';
$lang['status_permohonan_delete_success']	= 'record(s) successfully deleted.';

$lang['status_permohonan_purged']	= 'record(s) successfully purged.';
$lang['status_permohonan_success']	= 'record(s) successfully restored.';


$lang['status_permohonan_delete_failure']	= 'We could not delete the record: ';
$lang['status_permohonan_delete_error']		= 'You have not selected any records to delete.';
$lang['status_permohonan_actions']			= 'Actions';
$lang['status_permohonan_cancel']			= 'Cancel';
$lang['status_permohonan_delete_record']		= 'Delete';
$lang['status_permohonan_delete_confirm']	= 'Are you sure you want to delete this status_permohonan?';
$lang['status_permohonan_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['status_permohonan_action_edit']		= 'Save';
$lang['status_permohonan_action_create']		= 'Create';

// Activities
$lang['status_permohonan_act_create_record']	= 'Created record with ID';
$lang['status_permohonan_act_edit_record']	= 'Updated record with ID';
$lang['status_permohonan_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['status_permohonan_column_created']	= 'Created';
$lang['status_permohonan_column_deleted']	= 'Deleted';
$lang['status_permohonan_column_modified']	= 'Modified';
