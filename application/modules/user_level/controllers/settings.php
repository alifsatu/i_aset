<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('User_Level.Settings.View');
        $this->load->model('user_level_model', null, true);
        $this->load->model('user_level_details_model', null, true);
        $this->lang->load('user_level');

        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('user_level', 'user_level.js');
        Assets::add_css(Template::theme_url('js/datepicker2/bootstrap-datepicker.css'));
        Assets::add_js(Template::theme_url("js/datepicker2/bootstrap-datepicker.js"));
        Assets::add_js(Template::theme_url("js/date.js"));
        Assets::add_js("js/parsley/parsley.min.js");
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->user_level_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('user_level_delete_success'), 'success');
                } else {
                    Template::set_message(lang('user_level_delete_failure') . $this->user_level_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('user_levelfield', $this->input->post('select_field'));
                $this->session->set_userdata('user_levelfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('user_levelfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('user_levelfield');
                $this->session->unset_userdata('user_levelfvalue');
                $this->session->unset_userdata('user_levelfname');
                break;
        }

        if ($this->session->userdata('user_levelfield') != '') {
            $field = $this->session->userdata('user_levelfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('user_levelfield') == 'All Fields') {

            $total = $this->user_level_model
                    ->where('deleted', '0')
                    ->likes('user_level', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('level_code', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('daily_fte', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('descrition', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('created_by', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('company_id', $this->session->userdata('user_levelfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->user_level_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('user_levelfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->user_level_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('user_levelfield') == 'All Fields') {
            $records = $this->user_level_model
                    ->where('deleted', '0')
                    ->likes('user_level', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('level_code', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('daily_fte', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('descrition', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('created_by', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('company_id', $this->session->userdata('user_levelfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->user_level_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('user_levelfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->user_level_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
//        Template::set('role_name', $role_name);
//        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('user_level', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage User Level');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_user_level SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('user_level_success'), 'success');
                } else {
                    Template::set_message(lang('user_level_restored_error') . $this->user_level_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_user_level where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('user_level_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('user_levelfield', $this->input->post('select_field'));
                $this->session->set_userdata('user_levelfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('user_levelfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('user_levelfield');
                $this->session->unset_userdata('user_levelfvalue');
                $this->session->unset_userdata('user_levelfname');
                break;
        }


        if ($this->session->userdata('user_levelfield') != '') {
            $field = $this->session->userdata('user_levelfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('user_levelfield') == 'All Fields') {

            $total = $this->user_level_model
                    ->where('deleted', '1')
                    ->likes('user_level', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('level_code', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('daily_fte', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('descrition', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('created_by', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('company_id', $this->session->userdata('user_levelfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->user_level_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('user_levelfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-user_level_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('user_levelfield') == 'All Fields') {
            $records = $this->user_level_model
                    ->where('deleted', '1')
                    ->likes('user_level', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('level_code', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('daily_fte', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('descrition', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('created_by', $this->session->userdata('user_levelfvalue'), 'both')
                    ->likes('company_id', $this->session->userdata('user_levelfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->user_level_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('user_levelfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->user_level_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('user_level', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage User Level');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a User Level object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('User_Level.Settings.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_user_level()) {
                // Log the activity
                log_activity($this->current_user->id, lang('user_level_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'user_level');

                Template::set_message(lang('user_level_create_success'), 'success');
                redirect(SITE_AREA . '/settings/user_level');
            } else {
                Template::set_message(lang('user_level_create_failure') . $this->user_level_model->error, 'error');
            }
        }
        Assets::add_module_js('user_level', 'user_level.js');

        Template::set('toolbar_title', lang('user_level_create') . ' User Level');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of User Level data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (isset($_POST['action']) and $_POST['action'] == "deleteFTE") {
            $del = $this->user_level_details_model->delete($this->input->post('id'));
            $data = array();
            if ($del) {
                log_activity($this->current_user->id, lang('user_level_details_act_delete_fte_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'user_level_details');
                $data['status'] = 'success';
                $data['status_msg'] = 'Record successfully deleted';
                echo json_encode($data);
            } else {
                $data['status'] = 'success';
                $data['status_msg'] = $this->user_level_details_model->error.'. Please contact system administrator';
                echo json_encode($data);
               
            }
             die();
        }

        if (empty($id)) {
            Template::set_message(lang('user_level_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/user_level');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('User_Level.Settings.Edit');

            if ($this->save_user_level('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('user_level_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'user_level');

                Template::set_message(lang('user_level_edit_success'), 'success');
            } else {
                Template::set_message(lang('user_level_edit_failure') . $this->user_level_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('User_Level.Settings.Delete');

            if ($this->user_level_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('user_level_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'user_level');

                Template::set_message(lang('user_level_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/user_level');
            } else {
                Template::set_message(lang('user_level_delete_failure') . $this->user_level_model->error, 'error');
            }
        }
        Template::set('user_level', $this->user_level_model->find($id));
        Template::set('toolbar_title', lang('user_level_edit') . ' User Level');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------

    public function view() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('user_level_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/user_level');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('User_Level.Settings.Edit');

            if ($this->save_user_level('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('user_level_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'user_level');

                Template::set_message(lang('user_level_edit_success'), 'success');
            } else {
                Template::set_message(lang('user_level_edit_failure') . $this->user_level_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('User_Level.Settings.Delete');

            if ($this->user_level_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('user_level_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'user_level');

                Template::set_message(lang('user_level_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/user_level');
            } else {
                Template::set_message(lang('user_level_delete_failure') . $this->user_level_model->error, 'error');
            }
        }
        Template::set('user_level', $this->user_level_model->find($id));
        Template::set('toolbar_title', 'View User Level');
        Template::render();
    }

    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_user_level SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('user_level_success'), 'success');
                redirect(SITE_AREA . '/settings/user_level/deleted');
            } else {

                Template::set_message(lang('user_level_error') . $this->user_level_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_user_level where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('user_level_purged'), 'success');
                redirect(SITE_AREA . '/settings/user_level/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('user_level_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/user_level');
        }




        Template::set('user_level', $this->user_level_model->find($id));

        Assets::add_module_js('user_level', 'user_level.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' User_level');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_user_level($type = 'insert', $id = 0) {
//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['user_level'] = $this->input->post('user_level_user_level');
        $data['level_code'] = $this->input->post('user_level_level_code');
//        $data['daily_fte'] = $this->input->post('user_level_daily_fte');
        $data['descrition'] = $this->input->post('user_level_descrition');
        $data['created_by'] = $this->auth->user_id();
        $data['company_id'] = $this->auth->company_id();


        //FTE list //VO NOV 2017
        $data_fte_list = array();
        $fte_start_date = $this->input->post('start_date' . $id);
//        $fte_end_date = $this->input->post('end_date' . $id);
        $fte_daily_fte = $this->input->post('daily_fte' . $id);


        $rowid = $this->input->post('rowid' . $id);





        if ($type == 'insert') {
            $id = $this->user_level_model->insert($data);

            if (is_numeric($id)) {
                foreach ($rowid as $rkey => $rval) {

                    $data_fte_list[$rkey] = array(
                        'parent_user_level_id' => $id,
                        'start_date' => $fte_start_date[$rkey],
//                        'end_date' => $fte_end_date[$rkey],
                        'daily_fte' => $fte_daily_fte[$rkey]
                    );
                    if ($rowid[$rkey] == "") {
                        $this->user_level_details_model->insert($data_fte_list[$rkey]);
                    } else {
                        $this->user_level_details_model->update($rowid[$rkey], $data_fte_list[$rkey]);
                    }
                }
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->user_level_model->update($id, $data);

            foreach ($rowid as $rkey => $rval) {

                $data_fte_list[$rkey] = array(
                    'parent_user_level_id' => $id,
                    'start_date' => $fte_start_date[$rkey],
//                    'end_date' => $fte_end_date[$rkey],
                    'daily_fte' => $fte_daily_fte[$rkey]
                );
//                echo $rowid[$rkey];
                if ($rowid[$rkey] == "") {
                    $this->user_level_details_model->insert($data_fte_list[$rkey]);
                } else {
                    $this->user_level_details_model->update($rowid[$rkey], $data_fte_list[$rkey]);
                }
            }
        }
        return $return;
    }
    
    public function copyUserLevel(){
        //To continue tomorrow, copy all daily_fte insert into user_level_details table
//        $records = $this->user_level_model->find_all();
        $records = $this->db->query('SELECT * FROM intg_user_level_original')->result();

        $data = array();
        foreach($records as $val){
            $data['parent_user_level_id'] = $val->id;
            $data['daily_fte'] = $val->daily_fte;
            $data['start_date'] = '2015-01-01';
            $data['deleted'] = $val->deleted;
            $data['created_by'] = $val->created_by;
            $data['created_on'] = $val->created_on;
            $data['modified_on'] = $val->modified_on;
            echo "<pre>";
            print_r($data);
            echo "</pre>";
            $this->user_level_details_model->insert($data);
        }
        
    }
    
        public function updateTimesheetDetailsFTE(){
            
            $records = $this->db->query("SELECT 
                iu.id,iu.`display_name`,
                iu.`user_level_id`,
                iul.id as iul_id,
                iuld.`id` as iuld_id ,
                iuld.`daily_fte` AS iuld_rate
                FROM intg_users_copy iu ,intg_user_level iul
                JOIN intg_user_level_details iuld ON iuld.`parent_user_level_id` = iul.id 
                WHERE
                iu.`user_level_id` = iul.id ")->result();
            
//            echo "<pre>";
//            print_r($records);
//            echo "</pre>";
            
            foreach($records as $val){
                
                $records = $this->db->query("UPDATE intg_timesheet_details a 
                        JOIN intg_timesheet b ON a.`tid` = b.`id`
                    SET a.`user_level_details_id` = '".$val->iuld_id."', 
                        a.`user_level_details_fte_rate` = '".$val->iuld_rate."'
                    WHERE b.`uid` = '".$val->id."'");
                if($records){
                    
                    echo 'updated UserID: '.$val->id.' : '.$val->display_name.'- UserLevelDetailID :'.$val->iuld_id.'- RATE'.$val->iuld_rate.'<br>';
                }else{
                    echo 'error updating '.$val->id.' : '.$val->display_name.'<br>';
                }
            }
            
            
        }

    //--------------------------------------------------------------------
}
