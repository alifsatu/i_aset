<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

$edit_only = FAlSE;
if (isset($user_level)) {
    $user_level = (array) $user_level;
    $edit_only = TRUE;
}
$id = isset($user_level['id']) ? $user_level['id'] : '0';
?>
<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">User Level</header>
            <div class="panel-body">

                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('user_level') ? 'error' : ''; ?>">
                        <?php echo form_label('User Level' . lang('bf_form_label_required'), 'user_level_user_level', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='user_level_user_level' class='input-sm input-s  form-control' type='text' name='user_level_user_level' maxlength="255" value="<?php echo set_value('user_level_user_level', isset($user_level['user_level']) ? $user_level['user_level'] : ''); ?>" required="required" />
                            <span class='help-inline'><?php echo form_error('user_level'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('level_code') ? 'error' : ''; ?>">
                        <?php echo form_label('Level Code' . lang('bf_form_label_required'), 'user_level_level_code', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='user_level_level_code' class='input-sm input-s  form-control' type='text' name='user_level_level_code' maxlength="255" value="<?php echo set_value('user_level_level_code', isset($user_level['level_code']) ? $user_level['level_code'] : ''); ?>" required="required" />
                            <span class='help-inline'><?php echo form_error('level_code'); ?></span>
                        </div>
                    </div>

<!--                    <div class="form-group <?php echo form_error('daily_fte') ? 'error' : ''; ?>">
                    <?php echo form_label('Daily FTE Rate' . lang('bf_form_label_required'), 'user_level_daily_fte', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='user_level_daily_fte' class='input-sm input-s  form-control' type='text' name='user_level_daily_fte' maxlength="255" value="<?php echo set_value('user_level_daily_fte', isset($user_level['daily_fte']) ? $user_level['daily_fte'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('daily_fte'); ?></span>
                        </div>
                    </div>-->

                    <div class="form-group <?php echo form_error('descrition') ? 'error' : ''; ?>">
                        <?php echo form_label('Description', 'user_level_descrition', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <?php echo form_textarea(array('name' => 'user_level_descrition', 'id' => 'user_level_descrition', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('user_level_descrition', isset($user_level['descrition']) ? $user_level['descrition'] : ''))); ?>
                            <span class='help-inline'><?php echo form_error('descrition'); ?></span>
                        </div>
                    </div>

                    <?php
                    $data['user_level_id'] = $id;
                    if ($edit_only) {
                        $data['edit_only'] = $edit_only;
                    }
                    $this->load->view('rate_list', $data);
                    ?>

                    <?php if ($this->uri->segment(4) == 'create') { ?>
                        <div class="form-actions">
                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('user_level_action_create'); ?>"  />
                            &nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/settings/user_level', lang('user_level_cancel'), 'class="btn btn-warning"'); ?>

                        </div>
                    <?php } else { ?>



                        <div class="form-actions">
                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('user_level_action_edit'); ?>"  />
                            &nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/settings/user_level', lang('user_level_cancel'), 'class="btn btn-warning"'); ?>

                            <?php if ($this->auth->has_permission('User_Level.Settings.Delete')) : ?>
                                &nbsp;&nbsp;
                                <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('user_level_delete_confirm'))); ?>');">
                                    <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('user_level_delete_record'); ?>
                                </button>
                            <?php endif; ?>
                        </div>
                    <?php } ?>

                </fieldset>
                <?php echo form_close(); ?>
            </div>


            <script type="text/javascript">

                $("#forma").submit(function (event) {
                    var check = checkOverlapDates();
                    var checkD = checkDuplicateDates();
                    
                    if (checkD === 2) {
                        alert("Duplicated dates found!. Please check your date and confirm before submitting");
                        event.preventDefault();
                    }
                    if (check.overlap === true) {
                        alert("Overlap dates found!. Please check your date and confirm before submitting\n"+JSON.stringify(check.ranges, null, 2));
                        event.preventDefault();
                    }
                    
                    var counter = parseInt($("#lastrow_<?= $id ?>").attr("counter"));
                    if (counter < 0) {
                        alert("Please add at least 1 FTE Rate ");
                        event.preventDefault();
                    }
                });
            </script>