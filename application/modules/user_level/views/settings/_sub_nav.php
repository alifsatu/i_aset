
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/user_level') ?>"style="border-radius:0"  id="list"><?php echo lang('user_level_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('User_Level.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/user_level/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('user_level_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('User_Level.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/user_level/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
