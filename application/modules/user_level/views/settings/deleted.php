<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 

echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>User Level</h4></div>
 <div class="col-md-3">
	
<select name="select_field" id="select_field" class="selecta form-control m-b" onchange="setfname()">
<option></option>
<option value="All Fields" <?=$this->session->userdata('user_levelfield')=='All Fields' ? 'selected' : ''?>>All Fields</option>
<option value="user_level" <?=$this->session->userdata('user_levelfield')=='user_level' ? 'selected' : ''?>>User level</option>
<option value="level_code" <?=$this->session->userdata('user_levelfield')=='level_code' ? 'selected' : ''?>>Level code</option>
<option value="daily_fte" <?=$this->session->userdata('user_levelfield')=='daily_fte' ? 'selected' : ''?>>Daily fte</option>
<option value="description" <?=$this->session->userdata('user_levelfield')=='description' ? 'selected' : ''?>>Description</option>
<option value="created_by" <?=$this->session->userdata('user_levelfield')=='created_by' ? 'selected' : ''?>>Created by</option>
<option value="company_id" <?=$this->session->userdata('user_levelfield')=='company_id' ? 'selected' : ''?>>Company id</option>
</select>
</div><div class="col-md-3"><div class="input-group">
<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('user_levelfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('user_levelfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('user_levelfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

<?php echo form_close(); ?>
     <div class="table-responsive m-t">   
  


	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('User_Level.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					<th <?php if ($this->input->get('sort_by') == 'user_level'.'_user_level') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_user_level&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_user_level') ? 'desc' : 'asc'); ?>'>
                    User Level</a></th>
					<th <?php if ($this->input->get('sort_by') == 'user_level'.'_level_code') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_level_code&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_level_code') ? 'desc' : 'asc'); ?>'>
                    Level Code</a></th>
					<th <?php if ($this->input->get('sort_by') == 'user_level'.'_daily_fte') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_daily_fte&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_daily_fte') ? 'desc' : 'asc'); ?>'>
                    Daily FTE Rate</a></th>
					<th <?php if ($this->input->get('sort_by') == 'user_level'.'_descrition') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_descrition&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_descrition') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
					<!--<th <?php if ($this->input->get('sort_by') == 'user_level'.'_created_by') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_created_by&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_created_by') ? 'desc' : 'asc'); ?>'>
                    Created_by</a></th>
					<th <?php if ($this->input->get('sort_by') == 'user_level'.'_company_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/user_level?sort_by=user_level_company_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'user_level'.'_company_id') ? 'desc' : 'asc'); ?>'>
                    Company Name</a></th>-->
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('User_Level.Settings.Delete')) : ?>
				<tr>
					<td colspan="9">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
				<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) :  $no = $this->input->get('per_page')+1;?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('User_Level.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('User_Level.Settings.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/settings/user_level/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->user_level) ?></td>
				<?php else: ?>
				<td><?php echo $record->user_level ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->level_code?></td>
				<td><?php echo $record->daily_fte?></td>
				<td><?php echo $record->descrition?></td>
				<!--<td><?php echo $record->created_by?></td>
				<td><?php echo $record->company_id?></td>-->
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
			<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
			<?php $no++; endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="9">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         
	<?php echo form_close(); ?>
  <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>