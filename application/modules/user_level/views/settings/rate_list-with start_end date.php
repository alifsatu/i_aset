<?php
$puli = $user_level_id;
$vo = isset($view_only) ? $view_only : false;
$eo = isset($edit_only) ? $edit_only : false;
?>
<div class="panel b-a">
    <div class="panel-heading b-b"><?= lang("user_level_details_fte_list") ?></div>
    <div class="panel-body" id="fte_list">


        <div class="alert alert-info">
            <strong>Reminder!</strong> <br>
            1) Past dated FTE cannot be added/edited.<br>
            2) FTE Rate need to be added in sequences.
        </div>


        <div class="row">
            <!--            <div class="col-md-1">
                           No.
                        </div>-->
            <div class="col-md-3">
                <?php echo form_label(lang("user_level_details_start_date").lang('bf_form_label_required'), 'user_level_details_start_date', array('class' => 'font-bold control-label')); ?>
            </div>
            <div class="col-md-3">
                <?php echo form_label(lang("user_level_details_end_date").lang('bf_form_label_required'), 'user_level_details_end_date', array('class' => 'font-bold control-label')); ?>
            </div>
            <div class="col-md-3">
                <?php echo form_label(lang("user_level_details_daily_fte").lang('bf_form_label_required'), 'user_level_daily_fte', array('class' => 'font-bold control-label')); ?>
            </div>

            <div class="col-md-2">
            </div>
        </div>

        <?php
        $this->db->order_by('start_date', 'ASC');
        $rate_list = $this->db->get_where($this->db->dbprefix('user_level_details'), array("parent_user_level_id" => $puli, "deleted" => "0"))->result();
        $atno = -1;
        $date_now = date("Y-m-d");

        foreach ($rate_list as $at) {

            // this format is string comparable
            $date_less_than = false;
            if ($date_now >= $at->start_date) {
                $date_less_than = true;
            } else {
                $date_less_than = false;
            }
//            echo $date_now." > ".$at->start_date;
            $atno++;
            ?>
            <br>
            <div class="row row-fluid checkRow " id="row_<?= $puli ?>_<?php e($atno) ?>">
                <!--                <div class="col-md-1">
                <?= $atno ?>
                                </div>-->
                <input type="hidden" name="rowid<?= $puli ?>[]" value="<?= $at->id ?>"/>
                <div class="<?= $date_less_than ? '' : 'input-daterange' ?> ">
                    <div class="col-md-3">
                        <?php
                        if ($vo) {
                            echo $at->start_date;
                        } else {
                            ?>

                            <input type="text" placeholder="Start Date" class="input-sm  form-control start_date" id="start_date<?= $puli ?>_<?= e($atno) ?>" name="start_date<?= $puli ?>[]"  value="<?= e($at->start_date) ?>" readonly="readonly" <?= $date_less_than ? 'title="Past dated FTE Rate"' : '' ?> required="required" />
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?php
                        if ($vo) {
                            echo $at->end_date;
                        } else {
                            ?>
                            <input type="text"  class="input-sm  form-control end_date" placeholder="End Date" name="end_date<?= $puli ?>[]" id="end_date<?= $puli ?>_<?php e($atno) ?>"  value="<?= $at->end_date ?>" readonly="readonly" <?= $date_less_than ? 'title="Past dated FTE Rate"' : '' ?> required="required" />
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <?php
                    if ($vo) {
                        echo $at->daily_fte;
                    } else {
                        ?>
                        <input type="number"  class="input-sm  form-control" placeholder="Daily FTE Rate" name="daily_fte<?= $puli ?>[]"  id="daily_fte<?= $puli ?>_<?php e($atno) ?>"  value="<?= e($at->daily_fte) ?>" <?= $date_less_than ? 'readonly="readonly" title="Past dated FTE Rate"' : '' ?> required="required" min="1" />
                        <?php
                    }
                    ?>
                </div>  

                <div class="col-md-2 ">
                    <?php
                    if ($vo == false) {
                        if ($date_less_than == false) {
                            if ($atno >= 1) {
                                ?>
                                <a href="javascript:void(0)" >
                                    <i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick="remrow('<?= e($atno) ?>', '<?= e($at->id) ?>');"></i>
                                </a>
                                <?php
                            }
                        }
                    }
                    ?>   
                </div>

            </div> 
        <?php } ?>

        <?php if ($vo == false) { ?>
            <div id="lastrow_<?= $puli ?>"  counter="<?= e($atno) ?>"></div>
            <div class="row"> 
                <div class="col-md-2">
                    <a href="javascript:void(0)" onclick="addnewrow()"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Add Line</strong></a>
                </div>
            </div>
        <?php } ?>

    </div>

</div>

<!-- End Attendees -->

<script type="text/javascript">
    $(document).ready(function () {
        initDatePicker();
    });
    
    $(".start_date,.end_date").on('change',function(){
//        checkOverlapDates();
    });
    
    $(".start_date,.end_date").on('changeDate', function (ev) {
//        checkOverlapDates();
    });

    function initDatePicker(prev_row_end_date) {
        var date = new Date();
        if (prev_row_end_date != '') {
            date.setDate(date.getDate() + 1);
        }

        $('.input-daterange').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            startDate : new Date(),
            beforeShowYear: function (date) {
                if (date.getFullYear() == 2007) {
                    return false;
                }
            }
        });
    }

    function addnewrow()
    {


        var date;
        var dt = '';
        var theId = parseInt($("#lastrow_<?= $puli ?>").attr("counter")) + 1;

        var prev_row_end_date = $("input.end_date:last").val();
        var prev_row_start_date = $("input.start_date:last").val();

        if ((prev_row_end_date == "" || prev_row_start_date == "") && theId > 0) {
            alert('Please complete both start and end date');
            prev_row_end_date = "";
            return false;
        } else {
            date = new Date(prev_row_end_date);
            dt = date.setDate(date.getDate() + 1);
        }




        $("#lastrow_<?= $puli ?>").attr("counter", parseInt(theId));

        var row = '<br><div class="row fadeInUp animated checkRow" id="row_<?= $puli ?>_' + theId + '" >' +
                '<input type="hidden" name="rowid<?= $puli ?>[]"/>'
//                + '<div class="col-md-1">no</div>'
                + '<div class="input-daterange"><div class="col-md-3">'
                + '<input type="text" class="input-sm form-control start_date readonly" placeholder="Start Date" name="start_date<?= $puli ?>[]"  id="start_date<?= $puli ?>_' + theId + '" value="' + formatDate(dt) + '" required="required" onkeydown="javascript: return false;" ></div>'
                + '<div class="col-md-3"><input type="text" class="input-sm form-control end_date readonly" placeholder="End Date" name="end_date<?= $puli ?>[]"  id="end_date<?= $puli ?>_' + theId + '" value="' + formatDate(dt) + '" required="required" onkeydown="javascript: return false;" ></div></div>'
                + '<div class="col-md-3 "><input type="number" class="input-sm  form-control" placeholder="Daily FTE Rate" name="daily_fte<?= $puli ?>[]"  id="daily_fte<?= $puli ?>_' + theId + '" required="required" min="1" ></div>'
                + '<div class="col-md-2 "><a href="javascript:void(0)" ><i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick=remTemp("#row_<?= $puli ?>_' + theId + '")></i></a></div>';

        $('#lastrow_<?= $puli ?>').before(row);

        initDatePicker(prev_row_end_date);
    }

    function remrow(val, id)
    {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
            var theId = parseInt($("#lastrow_<?= $puli ?>").attr("counter")) - 1;
            $("#row_<?= $puli ?>_" + val).slideUp('slow', function () {
                $("#row_<?= $puli ?>_" + val).remove();
                $("#lastrow_<?= $puli ?>").attr("counter", theId);

                $.post("", {action: "deleteFTE", id: id},
                        function (data) {
                            var dt = JSON.parse(data);
                            waitingDialog.show(dt.status_msg);
                            waitingDialog.hide();
                        });
            });
            setTimeout(function () {
                
            }, 500);
        }
    }

    function remTemp(val)
    {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
            var theId = parseInt($("#lastrow_<?= $puli ?>").attr("counter")) - 1;
            $(val).slideUp('slow', function () {
                $(val).remove();
                $("#lastrow_<?= $puli ?>").attr("counter", theId);
            });
            setTimeout(function () {
            }, 500);
        }
    }

//    function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
//        if (a_start <= b_start && b_start <= a_end)
//            return true; // b starts in a
//        if (a_start <= b_end && b_end <= a_end)
//            return true; // b ends in a
//        if (b_start < a_start && a_end < b_end)
//            return true; // a in b
//        return false;
//    }
//
//    function multipleDateRangeOverlaps() {
//        var i, j;
//        if (arguments.length % 2 !== 0)
//            throw new TypeError('Arguments length must be a multiple of 2');
//        for (i = 0; i < arguments.length - 2; i += 2) {
//            for (j = i + 2; j < arguments.length; j += 2) {
//                if (
//                        dateRangeOverlaps(
//                                arguments[i], arguments[i + 1],
//                                arguments[j], arguments[j + 1]
//                                )
//                        )
//                    return true;
//            }
//        }
//        return false;
//    }



    function formatDate(date) {
        if (isNaN(date)) {
            return '';
        }

        var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }


    function checkOverlapDates() {

        var ranges = [];

        $('.checkRow').each(function (i, obj) {
            ranges.push({
                start: new Date($(this).find('.start_date').val()),
                end: new Date($(this).find('.end_date').val())
            });
        });

        var output = overlap(ranges);
        console.log(overlap(ranges));
        
        if(output.overlap === false){
            alert("Overlap Dates Found!");
        }

    }
// this function takes an array of date ranges in this format:
// [{ start: Date, end: Date}]
// the array is first sorted, and then checked for any overlap

//REF : https://derickbailey.com/2015/09/07/check-for-date-range-overlap-with-javascript-arrays-sorting-and-reducing/

    /**
     * 
     * @param {type} dateRanges
     * @returns {unresolved}
     * @sampleoutput var r1 = {
     start: new Date("2/4/2001"),
     end: new Date("7/1/2002")
     };
     
     var r2 = {
     start: new Date("7/2/2002"),
     end: new Date("2/4/2003")
     };
     
     // start date overlaps with end date of previous
     var r3 = {
     start: new Date("2/4/2003"),
     end: new Date("5/12/2007")
     };
     
     var ranges = [r1, r3, r2];
     
     var output = JSON.stringify(overlap(ranges), null, 2)
     console.log(output);   
     */
    function overlap(dateRanges) {
        var sortedRanges = dateRanges.sort((previous, current) => {

            // get the start date from previous and current
            var previousTime = previous.start.getDate();
            var currentTime = current.start.getTime();

            // if the previous is earlier than the current
            if (previousTime < currentTime) {
                return -1;
            }

            // if the previous time is the same as the current time
            if (previousTime === currentTime) {
                return 0;
            }

            // if the previous time is later than the current time
            return 1;
        });

        var result = sortedRanges.reduce((result, current, idx, arr) => {
            // get the previous range
            if (idx === 0) {
                return result;
            }
            var previous = arr[idx - 1];

            // check for any overlap
            var previousEnd = previous.end.getTime();
            var currentStart = current.start.getTime();
            var overlap = (previousEnd >= currentStart);

            // store the result
            if (overlap) {
                // yes, there is overlap
                result.overlap = true;
                // store the specific ranges that overlap
                result.ranges.push({
                    previous: previous,
                    current: current
                })
            }

            return result;

            // seed the reduce  
        }, {overlap: false, ranges: []});


        // return the final results  
        return result;
    }

</script>