<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($user_level))
{
	$user_level = (array) $user_level;
}
$id = isset($user_level['id']) ? $user_level['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">User Level</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('user_level') ? 'error' : ''; ?>">
				<?php echo form_label('User Level'. lang('bf_form_label_required'), 'user_level_user_level', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='user_level_user_level' class='input-sm input-s  form-control' type='text' name='user_level_user_level' maxlength="255" value="<?php echo set_value('user_level_user_level', isset($user_level['user_level']) ? $user_level['user_level'] : '');?>" />
					<span class='help-inline'><?php echo form_error('user_level'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('level_code') ? 'error' : ''; ?>">
				<?php echo form_label('Level Code'. lang('bf_form_label_required'), 'user_level_level_code', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='user_level_level_code' class='input-sm input-s  form-control' type='text' name='user_level_level_code' maxlength="255" value="<?php echo set_value('user_level_level_code', isset($user_level['level_code']) ? $user_level['level_code'] : '');?>" />
					<span class='help-inline'><?php echo form_error('level_code'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('daily_fte') ? 'error' : ''; ?>">
				<?php echo form_label('Daily FTE Rate'. lang('bf_form_label_required'), 'user_level_daily_fte', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='user_level_daily_fte' class='input-sm input-s  form-control' type='text' name='user_level_daily_fte' maxlength="255" value="<?php echo set_value('user_level_daily_fte', isset($user_level['daily_fte']) ? $user_level['daily_fte'] : '');?>" />
					<span class='help-inline'><?php echo form_error('daily_fte'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('descrition') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'user_level_descrition', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'user_level_descrition', 'id' => 'user_level_descrition', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('user_level_descrition', isset($user_level['descrition']) ? $user_level['descrition'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('descrition'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created_by', 'user_level_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='user_level_created_by' class='input-sm input-s  form-control' type='text' name='user_level_created_by' maxlength="255" value="<?php echo set_value('user_level_created_by', isset($user_level['created_by']) ? $user_level['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name', 'user_level_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='user_level_company_id' class='input-sm input-s  form-control' type='text' name='user_level_company_id' maxlength="255" value="<?php echo set_value('user_level_company_id', isset($user_level['company_id']) ? $user_level['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/user_level', lang('user_level_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('User_Level.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('user_level_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>