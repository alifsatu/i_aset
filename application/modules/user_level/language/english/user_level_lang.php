<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['user_level_manage']			= 'Manage';
$lang['user_level_edit']				= 'Edit';
$lang['user_level_true']				= 'True';
$lang['user_level_false']				= 'False';
$lang['user_level_create']			= 'Save';
$lang['user_level_list']				= 'List';
$lang['user_level_new']				= 'New';
$lang['user_level_edit_text']			= 'Edit this to suit your needs';
$lang['user_level_no_records']		= 'There aren\'t any user_level in the system.';
$lang['user_level_create_new']		= 'Create a new User Level.';
$lang['user_level_create_success']	= 'User Level successfully created.';
$lang['user_level_create_failure']	= 'There was a problem creating the user_level: ';
$lang['user_level_create_new_button']	= 'Create New User Level';
$lang['user_level_invalid_id']		= 'Invalid User Level ID.';
$lang['user_level_edit_success']		= 'User Level successfully saved.';
$lang['user_level_edit_failure']		= 'There was a problem saving the user_level: ';
$lang['user_level_delete_success']	= 'record(s) successfully deleted.';

$lang['user_level_purged']	= 'record(s) successfully purged.';
$lang['user_level_success']	= 'record(s) successfully restored.';


$lang['user_level_delete_failure']	= 'We could not delete the record: ';
$lang['user_level_delete_error']		= 'You have not selected any records to delete.';
$lang['user_level_actions']			= 'Actions';
$lang['user_level_cancel']			= 'Cancel';
$lang['user_level_delete_record']		= 'Delete';
$lang['user_level_delete_confirm']	= 'Are you sure you want to delete this user_level?';
$lang['user_level_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['user_level_action_edit']		= 'Save';
$lang['user_level_action_create']		= 'Create';

// Activities
$lang['user_level_act_create_record']	= 'Created record with ID';
$lang['user_level_act_edit_record']	= 'Updated record with ID';
$lang['user_level_act_delete_record']	= 'Deleted record with ID';
$lang['user_level_details_act_delete_fte_record']	= 'Deleted FTE Rate record with ID';
$lang['user_level_details_daily_fte']	= 'Daily FTE';
$lang['user_level_details_fte_list']	= 'FTE List';
$lang['user_level_details_start_date']	= 'Effective Start Date';
$lang['user_level_details_end_date']	= 'Effective End Date';

// Column Headings
$lang['user_level_column_created']	= 'Created';
$lang['user_level_column_deleted']	= 'Deleted';
$lang['user_level_column_modified']	= 'Modified';
