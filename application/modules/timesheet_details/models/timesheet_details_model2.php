<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Timesheet_details_model extends BF_Model {

	protected $table_name	= "timesheet_details";
	protected $key			= "id";
	protected $soft_deletes	= false;
	protected $date_format	= "date";

	protected $log_user 	= FALSE;

	protected $set_created	= false;
	protected $set_modified = false;

	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= TRUE;

	//--------------------------------------------------------------------

	public function cso_timesheet_from_project($projectid)
	{
		

		
		 $this->load->model('timesheet/timesheet_model', null, true);
		 
		
		$taskpool = $this->db->query("select imt.task_pool_id,imt.milestone_id,im.start_date,im.end_date,DATEDIFF(end_date,start_date) as days_diff  from intg_milestones im,intg_milestones_tasks imt,intg_milestones_users imu where im.project_id = ".$projectid." and im.milestone_id = imu.milestones_id and im.milestone_id = imt.milestone_id and imu.userlevel_id = ".$this->config->item('csoulevelid')."")->result();
		
		//echo $this->db->last_query();
			
			foreach ( $taskpool as $t ) 
			{
				
				
				$a = strtotime($t->start_date);
				$b = strtotime($t->end_date);
				$a1 = date("Y-m",strtotime($t->start_date));
				$b1 = date("Y-m",strtotime($t->end_date));
				$c = strtotime( $a1);
				$d = strtotime($b1 );
				$monthdiff = round(($d-$c) / 60 / 60 / 24 / 30 ) + 1;
							

				
					
					
			  $day = date("d", strtotime($t->start_date));
		 	$period = "1";
            if($day < 16){
                $period = "1";
				//$daydiff = 16;
				
            }else{
                $period = "2" ;
				//$daydiff = date("t",strtotime($t->start_date)) - 15;
            }

			$period_arr = array("1","2");
			
			 $na = date("n",$a);
			 $na2 = $monthdiff + $na;
			
			
			while ( $na < $na2 ) {
				
			$c = strtotime(date("Y-m-d", $a) );
			echo date("Y-m-d",$c)."<br>";
			
		foreach ( $period_arr as $pa ) {
			
			
			if ( $pa == "1" ) { $daydiff = 16; }
			if ( $pa == "2" ) { $daydiff = date("t",$c) + 1; }
            $r = $this->db->query("select * from intg_timesheet where uid = '" . $this->config->item('csouid') . "' and mth = '" . date("n",$c) . "' and yr ='" . date("Y",$c) . "'  and period = '".$pa."' order by id desc");
			
			echo $this->db->last_query()."<br>";
			
			
            $s = $r->result();

            $id = NULL;

            if ( $r->num_rows() == 0) {  // submitted means sent for approval via workflow

				
		$datats = array();							
							
		$datats['uid']        = $this->config->item('csouid');							
		//$datats['mth']        = date("n");
		$datats['mth']        = date("n",strtotime($t->start_date));
		$datats['period']        = $pa;
		$datats['yr']        = date("Y",strtotime($t->start_date));
		$datats['status']        = 'Created';
		$datats['initiator'] = $this->config->item('csouid');
		$datats['final_approvers'] = $this->db->query("select work_flow_approvers from intg_work_flow where work_flow_module_id = 41 and work_flow_doc_generator = ".$this->config->item('csouid')." ")->row()->work_flow_approvers;
               
				//$id = $this->timesheet_model->insert($datats); // insert if existing first row is submitted or if there is no row
            }

            //$tid = $id == NULL ? $s[0]->id : $id;
			
 
$i = $pa=="1" ? date("d",strtotime($t->start_date)) : 16;  
$month = date("m",strtotime($t->start_date));
$year = date("Y",strtotime($t->start_date));
$sd = $pa=="1" ? $t->start_date : $year."-".$month."-16";
        
while ( $i < $daydiff   ) {
				
			//	$this->db->query("INSERT DELAYED INTO intg_timesheet_details (tid,pid,mid,ptskid,tsd_date,tsd_hours,comments) VALUES ('".$tid."','".$projectid."','".$t->milestone_id."','".$t->task_pool_id."','".$sd."','0',NULL)");
				$sd = date("Y-m-d",strtotime("+1 day",strtotime($sd)));
				$i++;
				
}
		}
		
		
		$c = strtotime(date("Y-m-d", $a) . " +1 month");
		$a = $c;
		$na++;	}
            } 
		
	}	
	
	
	
	
	public function cso_timesheet_from_milestoneV($milestoneids)
	{
		
		
		$taskpool = $this->db->query("select imt.task_pool_id,imt.milestone_id,im.start_date,im.end_date,im.project_id,DATEDIFF(end_date,start_date) as days_diff  from intg_milestones im,intg_milestones_tasks imt,intg_milestones_users imu where  im.milestone_id = imu.milestones_id and im.milestone_id = imt.milestone_id and imu.userlevel_id = ".$this->config->item('csoulevelid')." and im.milestone_id IN ( ".$milestoneids.")")->result();
		
		
		  $timesheet = $this->db->query("select id from intg_timesheet where uid = '" . $this->config->item('csouid') . "' and mth = '" . date("n") . "' and yr ='" . date("Y") . "' order by id desc")->row();
			
			foreach ( $taskpool as $t ) 
			{
				$data_mt = array();
				$data_mt['project_id']	=$t->project_id;
				$data_mt['milestone_id']=$t->milestone_id;
				$data_mt['task_pool_id']=$t->task_pool_id;
				$data_mt['created_date']=$t->start_date;
				$data_mt['created_by']	=$this->config->item('csouid');
	
 
$i = -1;  
$sd = $t->start_date;
        
while ( $i < $t->days_diff   ) {

               
				
				$this->db->query("INSERT DELAYED INTO intg_timesheet_details (tid,pid,mid,ptskid,tsd_date,tsd_hours,comments) VALUES ('".$timesheet->id."','".$t->project_id."','".$t->milestone_id."','".$t->task_pool_id."','".$sd."','0',NULL)");
				$sd = date("Y-m-d",strtotime("+1 day",strtotime($sd)));
				
				$i++;
				
}
               
            } 
        
			
			
		
	}	
		
	
	
}
