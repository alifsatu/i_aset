<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['cost_centre_manage']			= 'Manage';
$lang['cost_centre_edit']				= 'Edit';
$lang['cost_centre_true']				= 'True';
$lang['cost_centre_false']				= 'False';
$lang['cost_centre_create']			= 'Save';
$lang['cost_centre_list']				= 'List';
$lang['cost_centre_new']				= 'New';
$lang['cost_centre_edit_text']			= 'Edit this to suit your needs';
$lang['cost_centre_no_records']		= 'There aren\'t any cost_centre in the system.';
$lang['cost_centre_create_new']		= 'Create a new Cost Centre.';
$lang['cost_centre_create_success']	= 'Cost Centre successfully created.';
$lang['cost_centre_create_failure']	= 'There was a problem creating the cost_centre: ';
$lang['cost_centre_create_new_button']	= 'Create New Cost Centre';
$lang['cost_centre_invalid_id']		= 'Invalid Cost Centre ID.';
$lang['cost_centre_edit_success']		= 'Cost Centre successfully saved.';
$lang['cost_centre_edit_failure']		= 'There was a problem saving the cost_centre: ';
$lang['cost_centre_delete_success']	= 'record(s) successfully deleted.';

$lang['cost_centre_purged']	= 'record(s) successfully purged.';
$lang['cost_centre_success']	= 'record(s) successfully restored.';


$lang['cost_centre_delete_failure']	= 'We could not delete the record: ';
$lang['cost_centre_delete_error']		= 'You have not selected any records to delete.';
$lang['cost_centre_actions']			= 'Actions';
$lang['cost_centre_cancel']			= 'Cancel';
$lang['cost_centre_delete_record']		= 'Delete';
$lang['cost_centre_delete_confirm']	= 'Are you sure you want to delete this cost_centre?';
$lang['cost_centre_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['cost_centre_action_edit']		= 'Save';
$lang['cost_centre_action_create']		= 'Create';

// Activities
$lang['cost_centre_act_create_record']	= 'Created record with ID';
$lang['cost_centre_act_edit_record']	= 'Updated record with ID';
$lang['cost_centre_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['cost_centre_column_created']	= 'Created';
$lang['cost_centre_column_deleted']	= 'Deleted';
$lang['cost_centre_column_modified']	= 'Modified';
