<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($cost_centre))
{
	$cost_centre = (array) $cost_centre;
}
$id = isset($cost_centre['id']) ? $cost_centre['id'] : '';

?>
 <div class="row">
 <div class="col-sm-7">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Cost Centre</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('cost_centre') ? 'error' : ''; ?> clearfix">
              <div class="col-md-5">
				<?php echo form_label('Cost Centre'. lang('bf_form_label_required'), 'cost_centre_cost_centre', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='cost_centre_cost_centre' class='input-sm input-s  form-control' type='text' name='cost_centre_cost_centre' maxlength="255" value="<?php echo set_value('cost_centre_cost_centre', isset($cost_centre['cost_centre']) ? $cost_centre['cost_centre'] : '');?>" />
					<span class='help-inline'><?php echo form_error('cost_centre'); ?></span>
				</div></div>
                
                <?php // Change the values in this array to populate your dropdown as required
                $prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
			 ?>
			 <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>" clearfix>
             <div class="col-md-3">
				<?php echo form_label('SBU'. lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label') ); ?>
				<div class='controls'>
					 <select name="cost_centre_sbu" id="cost_centre_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
             <option></option>
            <?php foreach($prgm as $pn) { ?>
             <option value=<?=$pn->id?> <?php if ($cost_centre['sbu']==$pn->id) { echo "selected"; }?>><b><?=$pn->sbu_name?></b></option><?php }  ?>
            </select>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div></div>
			</div>
			</div>

			
		<?php // Change the values in this array to populate your dropdown as required
				
$pool = $this->db->query('SELECT * FROM  intg_pool_entry WHERE deleted = 0 ORDER BY entry_name ')->result();
			 ?>
			 <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>" clearfix>
             <div class="col-md-5">
				<?php echo form_label('Project Unit Code', 'cost_centre_sbu', array('class' => 'control-label') ); ?>
				<div class='controls'>
					 <select name="pool_entry" id="pool_entry" class="selecta form-control"  style="height:30px;width:200px; font-size:12px">
             <option></option>
            <?php foreach($pool as $p) { ?>
             <option value=<?=$p->id?> <?php if ($cost_centre['pool_entry_id'] == $p->id) { echo "selected"; }?>><b><?=$p->entry_name?></b></option><?php }  ?>
            </select>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div></div>
			</div>
		

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?> clearfix">
             <div class="col-md-5">
				<?php echo form_label('Description', 'cost_centre_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'cost_centre_description', 'id' => 'cost_centre_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('cost_centre_description', isset($cost_centre['description']) ? $cost_centre['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
                </div>
			</div>


			<div class="form-actions col-md-6 clearfix">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('cost_centre_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/cost_centre', lang('cost_centre_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Cost_Centre.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('cost_centre_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('cost_centre_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>
<script src="<?php echo Template::theme_url('js/parsleyold/parsley.min.js'); ?>" cache="false"></script> 
<script>
  $("#subm").click(function (e) {
 if (!$("#forma").parsley('validate')) {
            e.preventDefault();
        }
  });
		</script>