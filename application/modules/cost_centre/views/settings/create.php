<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($cost_centre)) {
    $cost_centre = (array) $cost_centre;
}
$id = isset($cost_centre['id']) ? $cost_centre['id'] : '';
$prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Cost Centre</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('cost_centre') ? 'error' : ''; ?> clearfix">
                        <div class="col-md-5">
                            <?php echo form_label('Cost Centre' . lang('bf_form_label_required'), 'cost_centre_cost_centre', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <input id='cost_centre_cost_centre' class='input-sm input-s  form-control' required="required" type='text' name='cost_centre_cost_centre' maxlength="255" value="<?php echo set_value('cost_centre_cost_centre', isset($cost_centre['cost_centre']) ? $cost_centre['cost_centre'] : ''); ?>" />
                                <span class='help-inline'><?php echo form_error('cost_centre'); ?></span>
                            </div></div>

                        <div class="col-md-3">
                            <?php echo form_label('SBU' . lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <select name="cost_centre_sbu" id="cost_centre_sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
                                    <option></option>
                                    <?php foreach ($prgm as $pn) { ?>
                                        <option value="<?= $pn->id ?>"><b><?= $pn->sbu_name ?></b></option>
                                    <?php } ?>
                                </select>
                                <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
                            </div>
                        </div>
                    </div>



                    <?php
// Change the values in this array to populate your dropdown as required

                    $poolentry = $this->db->query('SELECT * FROM  intg_pool_entry WHERE deleted = 0 ORDER BY entry_name ')->result();
                    ?>
                    <div class="form-group <?php echo form_error('pool_entry') ? 'error' : ''; ?> clearfix">
                        <div class="col-md-5">
                            <?php echo form_label('Project Unit Code', 'pool_entry', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <select name="pool_entry" id="pool_entry" class="selecta form-control"  style="height:30px;width:200px; font-size:12px">
                                    <option></option>
                                    <?php foreach ($poolentry as $p) { ?>
                                        <option value=<?= $p->id ?>><b><?= $p->entry_name ?></b></option>
                                    <?php } ?>
                                </select>
                                <span class='help-inline'><?php echo form_error('pool_entry'); ?></span>
                            </div></div>
                    </div>




                    <div class="form-group <?php echo form_error('description') ? 'error' : ''; ?> clearfix">
                        <div class="col-md-5">
                            <?php echo form_label('Description', 'cost_centre_description', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php echo form_textarea(array('name' => 'cost_centre_description', 'id' => 'cost_centre_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('cost_centre_description', isset($cost_centre['description']) ? $cost_centre['description'] : ''))); ?>
                                <span class='help-inline'><?php echo form_error('description'); ?></span>
                            </div></div>
                    </div>



                    <div class="form-actions col-md-5 clearfix">
                        <input type="submit" id="subm" name="save" class="btn btn-primary" value="<?php echo lang('cost_centre_action_create'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/cost_centre', lang('cost_centre_cancel'), 'class="btn btn-warning"'); ?>

                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>

            <script src="<? echo Template::theme_url('js/parsleyold/parsley.min.js'); ?>" cache="false"></script> 
            <script>
                        $("#subm").click(function (e) {
                    if (!$("#forma").parsley('validate')) {
                        e.preventDefault();
                    }
                });
            </script>
