
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/cost_centre') ?>"style="border-radius:0"  id="list"><?php echo lang('cost_centre_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Cost_Centre.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/cost_centre/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('cost_centre_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Cost_Centre.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/cost_centre/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
