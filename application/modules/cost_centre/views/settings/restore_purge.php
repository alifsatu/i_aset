<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($cost_centre))
{
	$cost_centre = (array) $cost_centre;
}
$id = isset($cost_centre['id']) ? $cost_centre['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Cost Centre</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('cost_centre') ? 'error' : ''; ?>">
				<?php echo form_label('Cost Centre'. lang('bf_form_label_required'), 'cost_centre_cost_centre', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='cost_centre_cost_centre' class='input-sm input-s  form-control' type='text' name='cost_centre_cost_centre' maxlength="255" value="<?php echo set_value('cost_centre_cost_centre', isset($cost_centre['cost_centre']) ? $cost_centre['cost_centre'] : '');?>" />
					<span class='help-inline'><?php echo form_error('cost_centre'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('cost_centre_sbu', $options, set_value('cost_centre_sbu', isset($cost_centre['sbu']) ? $cost_centre['sbu'] : ''), 'SBU'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'cost_centre_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'cost_centre_description', 'id' => 'cost_centre_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('cost_centre_description', isset($cost_centre['description']) ? $cost_centre['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created By', 'cost_centre_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='cost_centre_created_by' class='input-sm input-s  form-control' type='text' name='cost_centre_created_by' maxlength="255" value="<?php echo set_value('cost_centre_created_by', isset($cost_centre['created_by']) ? $cost_centre['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company', 'cost_centre_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='cost_centre_company_id' class='input-sm input-s  form-control' type='text' name='cost_centre_company_id' maxlength="255" value="<?php echo set_value('cost_centre_company_id', isset($cost_centre['company_id']) ? $cost_centre['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/cost_centre', lang('cost_centre_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Cost_Centre.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('cost_centre_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>