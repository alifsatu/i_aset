<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Cost_Centre.Settings.View');
		$this->load->model('cost_centre_model', null, true);
		$this->lang->load('cost_centre');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('cost_centre', 'cost_centre.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->cost_centre_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('cost_centre_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('cost_centre_delete_failure') . $this->cost_centre_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('cost_centrefield',$this->input->post('select_field'));
$this->session->set_userdata('cost_centrefvalue',$this->input->post('field_value'));
$this->session->set_userdata('cost_centrefname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('cost_centrefield');
$this->session->unset_userdata('cost_centrefvalue');
$this->session->unset_userdata('cost_centrefname');
break;
}

if ( $this->session->userdata('cost_centrefield')!='') { $field=$this->session->userdata('cost_centrefield'); }
else { $field = 'intg_cost_centre.id'; }

if ( $this->session->userdata('cost_centrefield') =='All Fields') 
{ 
 	 $this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');	
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');	

  $total = $this->cost_centre_model
   ->where('intg_cost_centre.deleted','0')
   ->likes('cost_centre',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('sbu_name',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('description',$this->session->userdata('cost_centrefvalue'),'both')
  ->count_all();
  
}
else
{
	 $this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');	
  $total = $this->cost_centre_model
    ->where('intg_cost_centre.deleted','0')
  ->likes($field,$this->session->userdata('cost_centrefvalue'),'both')  
  ->count_all();
}

		//$records = $this->cost_centre_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('cost_centrefield') =='All Fields') 
{
	 $this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');	
$records = $this->cost_centre_model
 
    ->where('intg_cost_centre.deleted','0')
   ->likes('cost_centre',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('sbu_name',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('description',$this->session->userdata('cost_centrefvalue'),'both')

  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_cost_centre.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_cost_centre.id'=>'desc',
	 );	}
	 $this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');	 	 	
$records = $this->cost_centre_model
  ->where('intg_cost_centre.deleted','0')
->likes($field,$this->session->userdata('cost_centrefvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->cost_centre_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('cost_centre', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Cost Centre');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_cost_centre SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('cost_centre_success'), 'success');
				}
				else
				{
					Template::set_message(lang('cost_centre_restored_error'). $this->cost_centre_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_cost_centre where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('cost_centre_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('cost_centrefield',$this->input->post('select_field'));
$this->session->set_userdata('cost_centrefvalue',$this->input->post('field_value'));
$this->session->set_userdata('cost_centrefname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('cost_centrefield');
$this->session->unset_userdata('cost_centrefvalue');
$this->session->unset_userdata('cost_centrefname');
break;
}


if ( $this->session->userdata('cost_centrefield')!='') { $field=$this->session->userdata('cost_centrefield'); }
else { $field = 'intg_cost_centre.id'; }

if ( $this->session->userdata('cost_centrefield') =='All Fields') 
{ 
 $this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');	
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');	
  $total = $this->cost_centre_model
  ->where('intg_cost_centre.deleted','1')
   ->likes('cost_centre',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('sbu_name',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('description',$this->session->userdata('cost_centrefvalue'),'both')
  ->count_all();


}
else
{
	$this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');
  $total = $this->cost_centre_model
   ->where('intg_cost_centre.deleted','1')
  ->likes($field,$this->session->userdata('cost_centrefvalue'),'both')  
  ->count_all();
}

//$records = $this-cost_centre_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('cost_centrefield') =='All Fields') 
{
	$this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');
$records = $this->cost_centre_model
 
  ->where('intg_cost_centre.deleted','1')
 ->likes('cost_centre',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('sbu_name',$this->session->userdata('cost_centrefvalue'),'both') 
  ->likes('description',$this->session->userdata('cost_centrefvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_cost_centre.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_cost_centre.id'=>'desc',
	 );	}
	 
	 	$this->db->select('*,intg_cost_centre.id as cost_centre_id,intg_cost_centre.created_on as cost_centre_created,intg_cost_centre.modified_on as cost_centre_modified ');
$this->db->join('intg_sbu','intg_sbu.id=intg_cost_centre.sbu');
//$this->db->join('intg_users','intg_users.id=intg_cost_centre.created_by');
$records = $this->cost_centre_model
->where('intg_cost_centre.deleted','1')
->likes($field,$this->session->userdata('cost_centrefvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->cost_centre_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('cost_centre', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Cost Centre');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Cost Centre object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Cost_Centre.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_cost_centre())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('cost_centre_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'cost_centre');

				Template::set_message(lang('cost_centre_create_success'), 'success');
				redirect(SITE_AREA .'/settings/cost_centre');
			}
			else
			{
				Template::set_message(lang('cost_centre_create_failure') . $this->cost_centre_model->error, 'error');
			}
		}
		Assets::add_module_js('cost_centre', 'cost_centre.js');

		Template::set('toolbar_title', lang('cost_centre_create') . ' Cost Centre');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Cost Centre data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('cost_centre_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/cost_centre');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Cost_Centre.Settings.Edit');

			if ($this->save_cost_centre('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('cost_centre_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'cost_centre');

				Template::set_message(lang('cost_centre_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('cost_centre_edit_failure') . $this->cost_centre_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Cost_Centre.Settings.Delete');

			if ($this->cost_centre_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('cost_centre_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'cost_centre');

				Template::set_message(lang('cost_centre_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/cost_centre');
			}
			else
			{
				Template::set_message(lang('cost_centre_delete_failure') . $this->cost_centre_model->error, 'error');
			}
		}
		Template::set('cost_centre', $this->cost_centre_model->find($id));
		Template::set('toolbar_title', lang('cost_centre_edit') .' Cost Centre');
		Template::render();
	}

	//--------------------------------------------------------------------

public function view()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('cost_centre_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/cost_centre');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Cost_Centre.Settings.Edit');

			if ($this->save_cost_centre('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('cost_centre_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'cost_centre');

				Template::set_message(lang('cost_centre_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('cost_centre_edit_failure') . $this->cost_centre_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Cost_Centre.Settings.Delete');

			if ($this->cost_centre_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('cost_centre_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'cost_centre');

				Template::set_message(lang('cost_centre_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/cost_centre');
			}
			else
			{
				Template::set_message(lang('cost_centre_delete_failure') . $this->cost_centre_model->error, 'error');
			}
		}
		Template::set('cost_centre', $this->cost_centre_model->find($id));
		Template::set('toolbar_title', lang('cost_centre_edit') .' Cost Centre');
		Template::render();
	}
	
	

public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_cost_centre SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('cost_centre_success'), 'success');
											redirect(SITE_AREA .'/settings/cost_centre/deleted');
				}
				else
				{
					
					Template::set_message(lang('cost_centre_error') . $this->cost_centre_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_cost_centre where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('cost_centre_purged'), 'success');
					redirect(SITE_AREA .'/settings/cost_centre/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('cost_centre_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/cost_centre');
		}

		
		
		
		Template::set('cost_centre', $this->cost_centre_model->find($id));
		
		Assets::add_module_js('cost_centre', 'cost_centre.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Cost_centre');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_cost_centre($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['cost_centre']        	= $this->input->post('cost_centre_cost_centre');
		$data['pool_entry_id'] 			= $this->input->post('pool_entry') == '' ? '0' : $this->input->post('pool_entry') ;
		$data['sbu']        			= $this->input->post('cost_centre_sbu');
		$data['description']        	= $this->input->post('cost_centre_description');
		$data['created_by']        		= $this->auth->user_id();
		$data['company_id']        		= $this->auth->company_id();

		if ($type == 'insert')
		{
			$id = $this->cost_centre_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->cost_centre_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}