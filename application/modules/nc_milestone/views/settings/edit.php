<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($nc_milestone))
{
	$nc_milestone = (array) $nc_milestone;
}
$id = isset($nc_milestone['id']) ? $nc_milestone['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">NC Milestone</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('milestone_name') ? 'error' : ''; ?>">
				<?php echo form_label('Milestone Name'. lang('bf_form_label_required'), 'nc_milestone_milestone_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='nc_milestone_milestone_name' class='input-sm input-s  form-control' type='text' name='nc_milestone_milestone_name' maxlength="500" value="<?php echo set_value('nc_milestone_milestone_name', isset($nc_milestone['milestone_name']) ? $nc_milestone['milestone_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('milestone_name'); ?></span>
				</div>
			</div>

			<!--<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('company name', 'nc_milestone_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='nc_milestone_company_id' class='input-sm input-s  form-control' type='text' name='nc_milestone_company_id' maxlength="255" value="<?php echo set_value('nc_milestone_company_id', isset($nc_milestone['company_id']) ? $nc_milestone['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>-->

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('nc_milestone_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/nc_milestone', lang('nc_milestone_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('NC_Milestone.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('nc_milestone_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('nc_milestone_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>