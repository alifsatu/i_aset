<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('NC_Milestone.Settings.View');
		$this->load->model('nc_milestone_model', null, true);
		$this->lang->load('nc_milestone');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('nc_milestone', 'nc_milestone.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->nc_milestone_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('nc_milestone_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('nc_milestone_delete_failure') . $this->nc_milestone_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('nc_milestonefield',$this->input->post('select_field'));
$this->session->set_userdata('nc_milestonefvalue',$this->input->post('field_value'));
$this->session->set_userdata('nc_milestonefname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('nc_milestonefield');
$this->session->unset_userdata('nc_milestonefvalue');
$this->session->unset_userdata('nc_milestonefname');
break;
}

if ( $this->session->userdata('nc_milestonefield')!='') { $field=$this->session->userdata('nc_milestonefield'); }
else { $field = 'id'; }

if ( $this->session->userdata('nc_milestonefield') =='All Fields') 
{ 
 		
  $total = $this->nc_milestone_model
   ->where('deleted','0')
  ->likes('milestone_name',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->likes('company_id',$this->session->userdata('nc_milestonefvalue'),'both') 
   ->count_all();
  
}
else
{
	
  $total = $this->nc_milestone_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('nc_milestonefvalue'),'both')  
  ->count_all();
}

		//$records = $this->nc_milestone_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('nc_milestonefield') =='All Fields') 
{
$records = $this->nc_milestone_model
 
    ->where('deleted','0')
 ->likes('milestone_name',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->likes('company_id',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->nc_milestone_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('nc_milestonefvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->nc_milestone_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('nc_milestone', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage NC Milestone');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_nc_milestone SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('nc_milestone_success'), 'success');
				}
				else
				{
					Template::set_message(lang('nc_milestone_restored_error'). $this->nc_milestone_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_nc_milestone where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('nc_milestone_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('nc_milestonefield',$this->input->post('select_field'));
$this->session->set_userdata('nc_milestonefvalue',$this->input->post('field_value'));
$this->session->set_userdata('nc_milestonefname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('nc_milestonefield');
$this->session->unset_userdata('nc_milestonefvalue');
$this->session->unset_userdata('nc_milestonefname');
break;
}


if ( $this->session->userdata('nc_milestonefield')!='') { $field=$this->session->userdata('nc_milestonefield'); }
else { $field = 'id'; }

if ( $this->session->userdata('nc_milestonefield') =='All Fields') 
{ 
 		
  $total = $this->nc_milestone_model
  ->where('deleted','1')
  ->likes('milestone_name',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->likes('company_id',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->nc_milestone_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('nc_milestonefvalue'),'both')  
  ->count_all();
}

//$records = $this-nc_milestone_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('nc_milestonefield') =='All Fields') 
{
$records = $this->nc_milestone_model
 
  ->where('deleted','1')
   ->likes('milestone_name',$this->session->userdata('nc_milestonefvalue'),'both') 
  ->likes('company_id',$this->session->userdata('nc_milestonefvalue'),'both') 
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->nc_milestone_model
->where('deleted','1')
->likes($field,$this->session->userdata('nc_milestonefvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->nc_milestone_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('nc_milestone', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage NC Milestone');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a NC Milestone object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('NC_Milestone.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_nc_milestone())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('nc_milestone_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'nc_milestone');

				Template::set_message(lang('nc_milestone_create_success'), 'success');
				redirect(SITE_AREA .'/settings/nc_milestone');
			}
			else
			{
				Template::set_message(lang('nc_milestone_create_failure') . $this->nc_milestone_model->error, 'error');
			}
		}
		Assets::add_module_js('nc_milestone', 'nc_milestone.js');

		Template::set('toolbar_title', lang('nc_milestone_create') . ' NC Milestone');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of NC Milestone data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('nc_milestone_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/nc_milestone');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('NC_Milestone.Settings.Edit');

			if ($this->save_nc_milestone('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('nc_milestone_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'nc_milestone');

				Template::set_message(lang('nc_milestone_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('nc_milestone_edit_failure') . $this->nc_milestone_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('NC_Milestone.Settings.Delete');

			if ($this->nc_milestone_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('nc_milestone_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'nc_milestone');

				Template::set_message(lang('nc_milestone_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/nc_milestone');
			}
			else
			{
				Template::set_message(lang('nc_milestone_delete_failure') . $this->nc_milestone_model->error, 'error');
			}
		}
		Template::set('nc_milestone', $this->nc_milestone_model->find($id));
		Template::set('toolbar_title', lang('nc_milestone_edit') .' NC Milestone');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_nc_milestone SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('nc_milestone_success'), 'success');
											redirect(SITE_AREA .'/settings/nc_milestone/deleted');
				}
				else
				{
					
					Template::set_message(lang('nc_milestone_error') . $this->nc_milestone_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_nc_milestone where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('nc_milestone_purged'), 'success');
					redirect(SITE_AREA .'/settings/nc_milestone/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('nc_milestone_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/nc_milestone');
		}

		
		
		
		Template::set('nc_milestone', $this->nc_milestone_model->find($id));
		
		Assets::add_module_js('nc_milestone', 'nc_milestone.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Nc_milestone');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_nc_milestone($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['milestone_name']        = $this->input->post('nc_milestone_milestone_name');
		$data['company_id']        = $this->input->post('nc_milestone_company_id');

		if ($type == 'insert')
		{
			$id = $this->nc_milestone_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->nc_milestone_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}