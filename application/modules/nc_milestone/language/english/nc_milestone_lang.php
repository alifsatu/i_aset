<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['nc_milestone_manage']			= 'Manage';
$lang['nc_milestone_edit']				= 'Edit';
$lang['nc_milestone_true']				= 'True';
$lang['nc_milestone_false']				= 'False';
$lang['nc_milestone_create']			= 'Save';
$lang['nc_milestone_list']				= 'List';
$lang['nc_milestone_new']				= 'New';
$lang['nc_milestone_edit_text']			= 'Edit this to suit your needs';
$lang['nc_milestone_no_records']		= 'There aren\'t any nc_milestone in the system.';
$lang['nc_milestone_create_new']		= 'Create a new NC Milestone.';
$lang['nc_milestone_create_success']	= 'NC Milestone successfully created.';
$lang['nc_milestone_create_failure']	= 'There was a problem creating the nc_milestone: ';
$lang['nc_milestone_create_new_button']	= 'Create New NC Milestone';
$lang['nc_milestone_invalid_id']		= 'Invalid NC Milestone ID.';
$lang['nc_milestone_edit_success']		= 'NC Milestone successfully saved.';
$lang['nc_milestone_edit_failure']		= 'There was a problem saving the nc_milestone: ';
$lang['nc_milestone_delete_success']	= 'record(s) successfully deleted.';

$lang['nc_milestone_purged']	= 'record(s) successfully purged.';
$lang['nc_milestone_success']	= 'record(s) successfully restored.';


$lang['nc_milestone_delete_failure']	= 'We could not delete the record: ';
$lang['nc_milestone_delete_error']		= 'You have not selected any records to delete.';
$lang['nc_milestone_actions']			= 'Actions';
$lang['nc_milestone_cancel']			= 'Cancel';
$lang['nc_milestone_delete_record']		= 'Delete';
$lang['nc_milestone_delete_confirm']	= 'Are you sure you want to delete this nc_milestone?';
$lang['nc_milestone_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['nc_milestone_action_edit']		= 'Save';
$lang['nc_milestone_action_create']		= 'Create';

// Activities
$lang['nc_milestone_act_create_record']	= 'Created record with ID';
$lang['nc_milestone_act_edit_record']	= 'Updated record with ID';
$lang['nc_milestone_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['nc_milestone_column_created']	= 'Created';
$lang['nc_milestone_column_deleted']	= 'Deleted';
$lang['nc_milestone_column_modified']	= 'Modified';
