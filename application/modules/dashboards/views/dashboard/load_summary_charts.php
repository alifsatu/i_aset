<script type="text/javascript">

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        initMainDashboard();
    });
</script>

<script type="text/javascript">

    $(function () {
        var chart1;
        var options = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {

                title: {
                    margin: 10,
                    text: 'Jumlah Projek'
                },
            },
            //
            legend: {
                enabled: true,
            },
            title: {
                text: ''
            },
            //
            plotOptions: {
                series: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                },
                pie: {
                    plotBorderWidth: 0,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '100%',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: <b>{point.y}</b>'
                    },
                    showInLegend: true
                }
            },
            //
            series: [{
                    name: "Daerah",
                    colorByPoint: true,
                    data:
                            [<?php
foreach ($PermohonanVSdaerah as $t) {

    echo "{ name :\"" . $t->name . "\",y:" . $t->y . "},";
}
?>]
                }],
            //
            drilldown: {
                series: []
            }
        };
        options.chart.renderTo = 'containerDynamic';
        options.chart.type = 'column';
        chart1 = new Highcharts.Chart(options);
        chartfunc = function () {

            var column = document.getElementById('column');
            var bar = document.getElementById('bar');
            var pie = document.getElementById('pie');
            var line = document.getElementById('line');
            if (column.checked)
            {
                options.chart.renderTo = 'containerDynamic';
                options.chart.type = 'column';
                var chart1 = new Highcharts.Chart(options);
            } else if (bar.checked) {
                options.chart.renderTo = 'containerDynamic';
                options.chart.type = 'bar';
                var chart1 = new Highcharts.Chart(options);
            } else if (pie.checked) {
                options.chart.renderTo = 'containerDynamic';
                options.chart.type = 'pie';
                var chart1 = new Highcharts.Chart(options);
            } else {
                options.chart.renderTo = 'containerDynamic';
                options.chart.type = 'line';
                var chart1 = new Highcharts.Chart(options);
            }

        }

        loadChartData = function (selectedYear, selectedMonth) {
//            var selectedYear = $("#tahun_pilihan").val();

            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/getPermohonanVSdaerah') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options.chart.renderTo = 'containerDynamic';
//                options.chart.type = 'column';
                options.series = [{
                        name: "Daerah",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart1 = new Highcharts.Chart(options);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });




</script>




<script type="text/javascript">

    $(function () {
        var chart2;
        var options2 = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {

                title: {
                    margin: 10,
                    text: 'Jumlah Projek'
                },
            },
            //
            legend: {
                enabled: true,
            },
            title: {
                text: ''
            },
            //
            plotOptions: {
                series: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    },
                    showInLegend: true
                },
                pie: {
                    plotBorderWidth: 0,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '100%',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: <b>{point.y}</b>'
                    },
                    showInLegend: true
                }
            },
            //
            series: [{
                    name: "Pegawai",
                    colorByPoint: true,
                    data:
                            [<?php
foreach ($PermohonanVSpegawai as $t) {

    echo "{ name :\"" . $t->name . "\",y:" . $t->y . "},";
}
?>]
                }],
            //
            drilldown: {
                series: []
            }
        };

        options2.chart.renderTo = 'containerDynamicPegawai';
        options2.chart.type = 'column';
        chart2 = new Highcharts.Chart(options2);
        chartfunc2 = function () {

            var column = document.getElementById('column2');
            var bar = document.getElementById('bar2');
            var pie = document.getElementById('pie2');
            var line = document.getElementById('line2');
            if (column.checked)
            {
                options2.chart.renderTo = 'containerDynamicPegawai';
                options2.chart.type = 'column';
                var chart2 = new Highcharts.Chart(options2);
            } else if (bar.checked) {
                options2.chart.renderTo = 'containerDynamicPegawai';
                options2.chart.type = 'bar';
                var chart2 = new Highcharts.Chart(options2);
            } else if (pie.checked) {
                options2.chart.renderTo = 'containerDynamicPegawai';
                options2.chart.type = 'pie';
                var chart2 = new Highcharts.Chart(options2);
            } else {
                options2.chart.renderTo = 'containerDynamicPegawai';
                options2.chart.type = 'line';
                var chart2 = new Highcharts.Chart(options2);
            }

        }

        loadChartData2 = function (selectedYear, selectedMonth) {




            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/getPermohonanVSpegawai') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options2.chart.renderTo = 'containerDynamicPegawai';
//                options2.chart.type = 'column';
                options2.series = [{
                        name: "Pegawai",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart2 = new Highcharts.Chart(options2);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });




</script>