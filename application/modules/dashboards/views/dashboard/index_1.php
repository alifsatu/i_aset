
<style>
    .event_popover .popover {
        width: 200px;
    }

    .legendLabel { color:#000 }
.ui-datepicker-calendar {
    display: none;
    }
</style>

     
                
                <section class="scrollable padder">

 <!--  <li><a href="index.html"><i class="fa fa-home"></i> <?php echo lang('dashboards_home') ?></a></li>
   <li class="active"> <?php echo lang('dashboards') ?></li>-->
                    <!--<div class="m-b-md">
                      <h3 class="m-b-none">Dashboard</h3>
                      <small>Welcome back, Noteman</small>
                    </div>-->

                    <section class="panel panel-default">
                        <div class="row m-l-none m-r-none bg-light lter">
                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom" title="Remaining days for any submission in current month">
                                <span class="fa-stack fa-2x pull-left m-r-sm">
                                    <i class="fa fa-circle fa-stack-2x text-info"></i>
                                    <i class="fa fa-calendar fa-stack-1x text-white"></i>
                                </span>
                                <!--<a class="clear" href="#">-->
                                <span class="h3 block m-t-xs"><strong><?= $remaining ?></strong></span>
                                <small class="text-muted text-uc">Days Remaining</small>
                                <!--</a>-->
                            </div>
                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom" title="Total created project by you">
                                <span class="fa-stack fa-2x pull-left m-r-sm">
                                    <i class="fa fa-circle fa-stack-2x text-warning"></i>
                                    <i class="fa fa-list-ol fa-stack-1x text-white"></i>
                                </span>
                                <span class="h3 block m-t-xs"><strong><?= $user_owned_projects->num ?></strong></span>

                                <!--</a>-->
                                <small class="text-muted text-uc">Owned Project<?php echo $user_owned_projects->num > 1 ? 's' : '' ?></small>
                            </div>

                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom" title="Total assigned project to you">                     
                                <span class="fa-stack fa-2x pull-left m-r-sm" >
                                    <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                    <i class="fa fa-list-alt fa-stack-1x text-white"></i>
                                </span>
                                <span class="h3 block m-t-xs"><strong><?= $assigned_projects->num ?></strong></span>
                                <small class="text-muted text-uc" >Assigned Project<?php echo $assigned_projects->num > 1 ? 's' : '' ?></small>
                            </div>

                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom"  title="Total created Non-chargeable project by you">
                                <span class="fa-stack fa-2x pull-left m-r-sm">
                                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                    <i class="fa fa-list-ul fa-stack-1x text-white"></i>
                                </span>

                                <span class="h3 block m-t-xs"><strong><?= $nc_projects->total ?></strong></span>
                                <small class="text-muted text-uc text-sm">NC Project</small>

                            </div>

                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom"  title="Total milestones you involved in">
                                <span class="fa-stack fa-2x pull-left m-r-sm">
                                    <i class="fa fa-circle fa-stack-2x text-success"></i>
                                    <i class="fa fa-tasks fa-stack-1x text-white"></i>
                                </span>

                                <span class="h3 block m-t-xs"><strong><?php echo $milestone; ?></strong></span>
                                <small class="text-muted text-uc">Milestones</small>

                            </div>

                            <div class="col-sm-2 col-md-2 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom"  title="Total hours spent on projects">
                                <span class="fa-stack fa-2x pull-left m-r-sm">
                                    <i class="fa fa-circle fa-stack-2x text-dark"></i>
                                    <i class="fa fa-clock-o fa-stack-1x text-white"></i>
                                </span>

                                <span class="h3 block m-t-xs"><strong><?= number_format($total_hours, 2) ?></strong></span>
                                <small class="text-sm text-muted text-uc">Total Hour Spent</small>

                            </div>

                        </div>
                    </section>
                    <div class="row">
                        
                        <div class="input-group col-md-2">
                            <h2>Month:<?php echo isset($selectedMonth) ? $selectedMonth : '';?></h2><br>
                        </div>
                       
                        <div class="input-group col-md-2">
                                <?php echo form_open($this->uri->uri_string(),    'id="formdate"'); ?>
                                <label for="startDate">Change Month: </label><input placeholder="Select month" readonly="readonly" name="selectedMonth" id="selectedMonth" class="input-sm date-picker disabled" /><i class="fa fa-calendar"></i>
                                 <?php echo form_close(); ?>
                        </div>

                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <section class="panel panel-default">
                                <header class="panel-heading font-bold">Work history
                                    <span class="pull-right">
                                        <a href="#" data-toggle="modal" data-target="#myModal" title="Click For Detailed Chart">
                                            <i class="fa fa-external-link" >
                                            </i><label style="cursor: pointer"> More</label>
                                        </a>
                                    </span>
                                </header>
                                <div class="panel-body" style="position:relative">
                                    <div id="container" style="min-width: 250px; height: 400px; margin: 0 auto"></div>
                                </div>

                            </section>
                        </div>
                        <div class="col-md-6">
                            <section class="panel panel-default">
                                <header class="panel-heading font-bold">Time spent on projects</header>
                                <div class="panel-body">

                                    <div id="container2" style="min-width: 200px; height: 400px; max-width: 600px; margin: 0 auto"></div>

                                </div>

                            </section>
                        </div>



                    </div>


                </section>
                
            </section>
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
            <div class="wrapper"><?php echo lang('dashboards_Notification') ?></div>
        </aside>
    </section>

</section> 

  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-lg " style="width:auto !important">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Work History</h4>
        </div>
        <div class="modal-body">

                
                <form id="forma" >
                    <div class="input-group  col-md-2">
                        <label>Projects</label>
                        <div class="controls">
                            <select name="projects" id="projects" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="requery('projects')">
                                <option></option>
                                <option value="%%">ALL</option>
                                <?php
                                foreach ($pr as $p) {
                                    ?>
                                    <option value="<?= $p->id ?>"><?= $p->project_name ?></option>
                                <?php } ?>
                            </select></div></div>
                    <div class="input-group  col-md-2">
                        <label>Date From</label>
                        <div class="controls">
                            <input name="datefrom" type="text" data-date-format="dd-mm-yyyy" class="input-s input-sm datepicker-input form-control"  style="z-index:100001" id="datefrom"  />
                        </div>
                    </div>
                    <div class="input-group  col-md-2">
                        <label>Date To</label>
                        <div class="controls">
                            <input name="dateto" data-date-format="dd-mm-yyyy" type="text" class="input-s input-sm datepicker-input form-control"    id="dateto" >
                        </div>
                    </div>
                </form> 


                <div class="input-group  col-md-2">
                    <label>Users</label>
                    <div class="controls">
                        <select name="users" id="users" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px;" onchange="requery('users')">
                            <option></option>
                            <?php
                            foreach ($us as $u) {
                                ?>
                                <option value="<?= $u->userid ?>"><?= $u->display_name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="input-group  col-md-2">
                    <label>Your Projects</label>
                    <div class="controls"><a href="#" onclick="requery('projleader')" class="btn btn-primary"><i class="fa fa-bar-chart"></i></a></div></div>
                <div id="containercommon"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">


    jQuery(document).on('click', '[data-toggle="ajaxModal1"]',
            function (e) {


                $('#ajaxModal').remove();
                e.preventDefault();
                var $this = $(this)


                var $remote = $this.data('remote') || $this.attr('href'),
                        $modal = $('<div class="modal fade"  id="ajaxModal"><div class="modal-dialog2">' +
                                '<div class="modal-body" style="padding:0;margin-top:100px;"></div>' +
                                '</div></div>');
                // $('body').append($modal);
                $modal.modal();
                $modal.find('.modal-body').load($remote, function () {




                });
            });</script>
<script type="text/javascript">


    $(function () {
        Highcharts.setOptions({
            colors: ['rgb(76, 192, 193)', 'rgb(101, 189, 119)', 'rgb(255, 195, 51)', 'rgb(251, 107, 91)', 'rgb(46, 62, 78)']
        });
        //$("#dateto,#datefrom").datepciker({format:"dd-mm-yyyy"});

        $("#dateto").on("changeDate", function () {
            requery('projects');
            $(this).datepicker('hide');
        });
        $("#datefrom").on("changeDate", function () {
            $(this).datepicker('hide');
        });
        // Create the chart
        $('#containercommon').highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: '(Project)'
                }
            },
            yAxis: {
                title: {
                    text: 'Total project work history (hours)'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} hours</b> of total<br/>'
            },
            series: [{
                    name: "Projects",
                    colorByPoint: true,
                    data:
                            [

<?php
foreach ($tspent as $t) {

    echo "{ name :\"".$t-> name."\",y:".intval($t->y).", drilldown:\"".$t-> name . "\"},";
}
?>]
                }],
            drilldown: {
                series: [
<?php
foreach ($projectname as $p => $qey) {
    echo "
                                            {
                                            name: \"" . $qey . "\",
                                                    id: \"" . $qey . "\",
                                                    data: [ ";

    foreach ($taskdrilldown1 as $td => $key) {

        if ($qey == $projref[$td]) {

            echo "
                                                    [
                                                            \"" . $key . "\",
                                                            " . intval($taskdrilldown2[$td]) . "
                                                    ], ";
        }
    }



    echo " ] }, ";
}
?>]
            }
        });

        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: '(Project)'
                }
            },
            yAxis: {
                title: {
                    text: 'Total project work history (hours)'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} hours</b> of total<br/>'
            },

            series: [{
                    name: "Projects",
                    colorByPoint: true,
                    data:
                            [<?php
foreach ($tspent as $t) {

    echo "{ name :\"" . $t->name . "\",y:" . intval($t->y) . "},";
}
?>]
                }],
            drilldown: {
                series: [{
                        name: "Microsoft Internet Explorer",
                        id: "Microsoft Internet Explorer",
                        data: [

                        ]
                    }, {}, ]
            }
        });
    });
</script>
<script type="text/javascript">
    $(function () {

        // Radialize the colors

        Highcharts.setOptions({
            colors: ['rgb(76, 192, 193)', 'rgb(101, 189, 119)', 'rgb(255, 195, 51)', 'rgb(251, 107, 91)', 'rgb(46, 62, 78)']
        });
        $('#container2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

            legend: {
                itemStyle: {
                    font: '9pt Trebuchet MS, Verdana, sans-serif',
                    color: '#A0A0A0',
                    width: '50px',
                    textOverflow: 'ellipsis',
                    overflow: 'hidden'
                },

                /* labelFormatter: function() {
                 // do truncation here and return string
                 // this.name holds the whole label
                 // for example:
                 return this.name.slice(0, 15) + '...'
                 }*/


            },

            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,

                    size: 180
                }
            },
            series: [{
                    name: "Projects",
                    colorByPoint: true,
                    data: [<?php
foreach ($tspent as $t) {

    echo "{ name :\"" . $t->name . "\",y:" . intval($t->y) . ", drilldown:\"" . $t->name . "\"},";
}
?>

                    ]

                            //[{name:"poejectx",y:34.00},{name:"projectY",y:18.00}]


                }],
            drilldown: {
                series: [
<?php
foreach ($projectname as $p => $qey) {
    echo "
{
name: \"" . $qey . "\",
id: \"" . $qey . "\",
data: [ ";

    foreach ($taskdrilldown1 as $td => $key) {

        if ($qey == $projref[$td]) {

            echo "
[
\"" . $key . "\",
" . intval($taskdrilldown2[$td]) . "
], ";
        }
    }



    echo " ] }, ";
}
?>]
            }
        });


    });

    function requery(val)
    {

        $("#containercommon").html('<p style="padding:20%"><i class="fa fa-spinner fa-spin fa-3x"></i></p>');
        if (val == "projects") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", $("#forma").serialize() + '&projdaterange=', function (chartData) {

                console.log($("#forma").serialize());
                $("#containercommon").html(chartData).show('slow');
            });

        }

        if (val == "users") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", $("#users").serialize() + '&useronly=', function (chartData) {

                console.log(chartData);
                $("#containercommon").html(chartData).show('slow');
            });

        }




        if (val == "projleader") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", {projectleader: "<?= $this->auth->user_id() ?>"}, function (chartData) {

                console.log(chartData);
                $("#containercommon").html(chartData).show('slow');
            });

        }
    }

    function resetdd()
    {
        $(".selecta").select2("val", "");
        $(".datepicker-input").val("");
        $("#containercommon").html("");
    }
    
    $(function() {
    $('.date-picker').datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        defaultDate: <?=$selectedMonth?>
    });
    
    
     $('#selectedMonth').change(function(){
         
       $('#formdate').submit();
       waitingDialog.show('Retrieveing and compiling data from server...');
    });
    
    $('#selectedMonth').on("changeDate", function (ev) {
        var newDate = new Date(ev.date);
        var mth = newDate.getMonth() + 1;
        var yr = newDate.getFullYear();
        $('input[name="selectedMonth"]').val(mth +'-'+yr);
        
        $('#formdate').submit();
    });
    
});
</script>

