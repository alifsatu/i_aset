<?php /**
 *  START SUMMARY SUMBANGAN MODAL TAB CONTENT 
 */
?>
<div class="tab-pane fade-in" id="summary2">

    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">
                    Graf Kutipan Bayaran Sumbangan Modal
                </header>
                <div class="panel-body" style="position:relative">
                    <div class="row">
                        <div class="input-group col-md-3 col-sm-3">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan_SummaryBSM" id="tahun_pilihan_SummaryBSM" multiple="" class="selecta input-sm form-control"  onchange="loadChartData_SummaryBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun_bayaran as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-3 col-sm-3">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan_SummaryBSM" id="bulan_pilihan_SummaryBSM" multiple="" class="selecta input-sm form-control"  onchange="checkTahunSelected('tahun_pilihan_SummaryBSM', 'bulan_pilihan_SummaryBSM', 'loadChartData_SummaryBSM')">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-3 col-sm-3">
                            <label>Daerah</label>
                            <div class="controls">
                                <select name="daerah_pilihan_SummaryBSM" id="daerah_pilihan_SummaryBSM" multiple="" class="selecta input-sm form-control"  onchange="loadChartData_SummaryBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_daerah as $p) {
                                        ?>
                                        <option value="<?= $p->id ?>"><?= $p->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-3 col-sm-3">
                            <label>Status Permohonan</label>
                            <div class="controls">
                                <select name="statuspermohonan_pilihan_SummaryBSM" id="statuspermohonan_pilihan_SummaryBSM" multiple="" class="selecta input-sm form-control"  onchange="loadChartData_SummaryBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_statuspermohonan as $p) {
                                        ?>
                                        <option value="<?= $p->id ?>"><?= $p->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-6 col-sm-6">
                            <label>Jenis Bayaran</label>
                            <div class="controls">
                                <select name="jenisbayaran_pilihan_SummaryBSM" id="jenisbayaran_pilihan_SummaryBSM" multiple="" class="selecta input-sm form-control" onchange="loadChartData_SummaryBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_jenis_bayaran as $p) {
                                        ?>
                                        <option value="<?= $p->id ?>"><?= $p->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">

                        <div class="input-group col-md-6 col-sm-6">
                            <label>Jenis Graf</label>
                            <div class="controls">
                                <input type="radio" name="mychart_SummaryBSM" class="mychart" id= "column_SummaryBSM" value="column" onclick= "chartfunc_SummaryBSM()" checked>Column
                                <input type="radio" name="mychart_SummaryBSM" class="mychart" id= "bar_SummaryBSM" value="bar" onclick= "chartfunc_SummaryBSM()">Bar
                                <input type="radio" name="mychart_SummaryBSM" class="mychart" id= "pie_SummaryBSM" value="pie" onclick= "chartfunc_SummaryBSM()">Pie
                                <input type="radio" name="mychart_SummaryBSM" class="mychart" id= "line_SummaryBSM" value="line" onclick= "chartfunc_SummaryBSM()">Line
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <br>
                        <div id="containerSummaryBSM" style="min-width: 250px; height: 400px; margin: 0 auto">
                            Loading Graph...
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Jadual Kutipan Bayaran Sumbangan Modal</header>
                <div class="panel-body">

                    <div class="row">
                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan_JKBSM" id="tahun_pilihan_JKBSM" multiple="" class="selecta input-sm form-control"  onchange="loadTableJKBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun_bayaran as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan_JKBSM" id="bulan_pilihan_JKBSM" multiple="" class="selecta input-sm form-control" onchange="loadTableJKBSM()">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-3 col-sm-3">
                            <label>Daerah</label>
                            <div class="controls">
                                <select name="daerah_pilihan_JKBSM" id="daerah_pilihan_JKBSM" multiple="" class="selecta input-sm form-control"  onchange="loadTableJKBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_daerah as $p) {
                                        ?>
                                        <option value="<?= $p->id ?>"><?= $p->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-3 col-sm-3">
                            <label>Status Permohonan</label>
                            <div class="controls">
                                <select name="statuspermohonan_pilihan_JKBSM" id="statuspermohonan_pilihan_JKBSM" multiple="" class="selecta input-sm form-control"  onchange="loadTableJKBSM()">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_statuspermohonan as $p) {
                                        ?>
                                        <option value="<?= $p->id ?>"><?= $p->nama ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                    </div>



                    <div class="row">
                        <br>
                        <div id="dvData">
                            <div class="table-responsive">   
                                <table class="table table-striped table-hover table-condensed text-sm dt-nowrap table-bordered" style="width:100%" id='table_JKBSM'>

                                    <thead>
                                    <th>Bil</th>
                                    <th>Bulan</th>

                                    <?php foreach ($list_jenis_bayaran as $val) { ?>
                                        <th><?= $val->nama ?></th>
                                    <?php } ?>
                                    <th>Jumlah (RM)</th>

                                    </thead>
                                    <?php
                                    $count = 1;
                                    $bulan_list = list_bulan_melayu_panjang();

                                    foreach ($bulan_list as $key => $bul) {
                                        ?>

                                        <tr>
                                            <td><?= $count++ ?></td>
                                            <th><?= $bul ?></th>
                                            <?php
                                            $total = 0;
                                            foreach ($list_jenis_bayaran as $val) {
                                                echo "<td>";
                                                foreach ($JKBSM as $rec) {

                                                    if ($key == $rec->bulan && $rec->jenis_bayaran == $val->id) {

                                                        echo number_format($rec->total, 2, '.', '');
                                                        $total += $rec->total;
                                                    }
                                                }
                                                echo "</td>";
                                            }
                                            ?>
                                            <th class="nowrap"><?php
                                                echo number_format(floatval($total), 2, '.', '');
                                                ?>
                                            </th>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>

                                            <?php for ($c = 0; $c < count($list_jenis_bayaran) - 1; $c++) { ?>
                                                <th></th>
                                            <?php } ?>
                                            <th style="text-align:right">Jumlah (RM):</th>
                                            <th ></th>
                                        </tr>
                                    </tfoot>


                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </section>
        </div>
    </div>




</div>

<?php /**
 *  END SUMMARY SUMBANGAN MODAL TAB CONTENT 
 */
?>


<script type="text/javascript">

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(function () {
            var chart_SummaryBSM;
            var options = {
                chart: {
                    events: {
                        drilldown: function (e) {
                            if (!e.seriesOptions) {

                                var chart = this;
                                // Show the loading label
                                chart.showLoading('Loading ...');
                                setTimeout(function () {
                                    chart.hideLoading();
                                    chart.addSeriesAsDrilldown(e.point, series);
                                }, 1000);
                            }

                        }
                    },
                    plotBorderWidth: 0
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    title: {
                        text: 'Tahun Bayaran'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Jumlah Kutipan'
                    }

                },
                legend: {
                    labelFormatter: function () {
                        var total = 0;
                        for (var i = this.yData.length; i--; ) {
                            total += this.yData[i];
                        }
                        ;
                        return this.name + '- Total: ' + numberWithCommas(total.toFixed(2));
                    }
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: 'RM {point.y:,.2f}'
                        },
                        showInLegend: true
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
                },
                series: [{
                        name: "Tahun",
                        colorByPoint: true,
                        data:
                                [<?php
foreach ($jumlah_bayaran_terima_by_tahun as $t) {

    echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
}
?>]
                    }],
                //
                drilldown: {
                    series: []
                }
            };
            options.chart.renderTo = 'containerSummaryBSM';
            options.chart.type = 'column';
            chart_SummaryBSM = new Highcharts.Chart(options);
            chartfunc_SummaryBSM = function () {

                var column = document.getElementById('column_SummaryBSM');
                var bar = document.getElementById('bar_SummaryBSM');
                var pie = document.getElementById('pie_SummaryBSM');
                var line = document.getElementById('line_SummaryBSM');
                if (column.checked)
                {
                    options.chart.renderTo = 'containerSummaryBSM';
                    options.chart.type = 'column';
                    var chart_SummaryBSM = new Highcharts.Chart(options);
                } else if (bar.checked) {
                    options.chart.renderTo = 'containerSummaryBSM';
                    options.chart.type = 'bar';
                    var chart_SummaryBSM = new Highcharts.Chart(options);
                } else if (pie.checked) {
                    options.chart.renderTo = 'containerSummaryBSM';
                    options.chart.type = 'pie';
                    var chart_SummaryBSM = new Highcharts.Chart(options);
                } else {
                    options.chart.renderTo = 'containerSummaryBSM';
                    options.chart.type = 'line';
                    var chart_SummaryBSM = new Highcharts.Chart(options);
                }

            }

            loadChartData_SummaryBSM = function () {
                var selectedYear = $("#tahun_pilihan_SummaryBSM").val();
                var selectedMonth = $("#bulan_pilihan_SummaryBSM").val();
                var selectedDaerah = $("#daerah_pilihan_SummaryBSM").val();
                var selectedStatusPermohonan = $("#statuspermohonan_pilihan_SummaryBSM").val();
                var selectedJenisBayaran = $("#jenisbayaran_pilihan_SummaryBSM").val();
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    data: {
                        tahun: selectedYear,
                        bulan: selectedMonth,
                        daerah: selectedDaerah,
                        statusPermohonan: selectedStatusPermohonan,
                        jenisBayaran: selectedJenisBayaran,
                        ajax: 'ajax'
                    },
                    url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadChartData_SummaryBSM') ?>"
                }).done(function (data, textStatus, jqXHR) {

                    var arr = [];
                    for (var i = 0; i < data.length; i++) {
                        var obj = {};
                        obj['name'] = data[i].tahun;
                        obj['y'] = parseFloat(data[i].total);
                        arr.push(obj);
                    }

                    options.chart.renderTo = 'containerSummaryBSM';
//                options.chart.type = 'column';
                    options.series = [{
                            name: "Tahun",
                            colorByPoint: true,
                            data: arr
                        }];
                    var chart_SummaryBSM = new Highcharts.Chart(options);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                    console.log(jqXHR);
                    console.log("There was an error." + errorThrown);
                });
            }

        });
    });
    $(document).ready(function () {

        loadTableJKBSM('All', 'All');
    });

    function loadTableJKBSM() {


        var selectedYear = $("#tahun_pilihan_JKBSM").val();
        var selectedMonth = $("#bulan_pilihan_JKBSM").val();
        var selectedDaerah = $("#daerah_pilihan_JKBSM").val();
        var selectedStatusPermohonan = $("#statuspermohonan_pilihan_JKBSM").val();
        var selectedJenisBayaran = $("#jenisbayaran_pilihan_JKBSM").val();
//        var tahun = ' Semua Tahun';
//
//        if (selectedYear != 'All') {
//            var tahun = ' Bagi Tahun  ' + selectedYear;
//        }
//
//        var bulan = ' dan Semua bulan';
//
//        if (selectedMonth != 'All') {
//            var bulan = ' dan Bulan  ' + selectedMonth;
//        }

        $.ajax({
            dataType: "json",
            type: "POST",
            data: {
                tahun: selectedYear,
                bulan: selectedMonth,
                daerah: selectedDaerah,
                statusPermohonan: selectedStatusPermohonan,
                jenisBayaran: selectedJenisBayaran,
                ajax: 'ajax'
            },
            url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/getTableJKBSM') ?>/"
        }).done(function (data, textStatus, jqXHR) {


            var arr = [];
            var arr_col = [];
            var list_jenis_bayaran = <?php echo json_encode($list_jenis_bayaran); ?>;
            var list_bulan = <?php echo json_encode($bulan_list); ?>;
//            console.log(list_jenis_bayaran);
//            console.log(list_bulan);

            var obj_col = {};
            var obj_colbil = {};
            obj_colbil['data'] = "bil";
            arr_col.push(obj_colbil);
            obj_col['data'] = "bulan";
            arr_col.push(obj_col);
            for (var c = 0; c < list_jenis_bayaran.length; c++) {
                var obj_col2 = {};
                obj_col2['data'] = list_jenis_bayaran[c].id;
                arr_col.push(obj_col2);
            }
            var obj_col3 = {};
            obj_col3['data'] = "jumlah";
            arr_col.push(obj_col3);
            var jumlah_column = arr_col.length - 1;
//            console.log("list_bulan", list_bulan);
//            console.log("list_jenis_bayaran", list_jenis_bayaran);
//            console.log("column", arr_col);
//            console.log("jumlah column", jumlah_column);

//            console.log(Object.keys(list_bulan).length);
//            for (var a = 1; a < list_bulan.length; a++) {
            var length = 1;
            for (var key in list_bulan) {


                var obj = {};
                obj['bil'] = length;
                obj['bulan'] = list_bulan[key];
                var jumlah = 0.00;
                for (var d = 0; d < list_jenis_bayaran.length; d++) {
                    var jumlah_kecil = 0.00;
                    for (var i = 0; i < data.length; i++) {

                        if (data[i].jenis_bayaran === list_jenis_bayaran[d].id && data[i].bulan === key) {
                            jumlah_kecil = numberWithCommas(parseFloat(data[i].total).toFixed(2));
                            jumlah += parseFloat(data[i].total);
                        }

                    }

                    obj[list_jenis_bayaran[d].id] = jumlah_kecil === 0 ? '' : jumlah_kecil;
                }
                obj['jumlah'] = numberWithCommas(parseFloat(jumlah).toFixed(2));
                arr.push(obj);
                length++;
            }

//            console.log("Array Data: ", arr);


            $('#table_JKBSM').dataTable({
                //here is the solution
                destroy: true,
                "pageLength": 12,
                "columnDefs": [
                    {className: "h4", "targets": [1]}
                ],
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    total = api
                            .column(jumlah_column)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    // Total over this page
                    pageTotal = api
                            .column(jumlah_column, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    // Update footer
                    $(api.column(jumlah_column).footer()).html(numberWithCommas(total.toFixed(2)));
                },
                dom: 'Bfrtip',
                buttons: [
                    {extend: 'copyHtml5', footer: true},
                    {extend: 'excelHtml5', footer: true},
                    {extend: 'csvHtml5', footer: true},
                    {extend: 'pdfHtml5', footer: true,
                        orientation: 'landscape'
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                    .find('table')
                                    .append(
                                            '<tr><td></td><td><b>Jumlah : </b></td><td><b>' + pageTotal.toFixed(2) + '<b></td></tr>'
                                            );

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                "data": arr,
                "columns": arr_col
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(jqXHR);
            console.log("There was an error." + errorThrown);
        });
    }


    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

    $(document).ready(function () {
        LoadDiv();
        $('#subNav-Header').remove();
    });
    $('#nav').on('click', function () {
        setTimeout(clicked, 500);
    });

    function clicked() {
        LoadDiv();
    }


    $(window).resize(function () {
        LoadDiv();
    });

    function LoadDiv() {
        var height = $('body').height() - $('header').height() - $('.navbar-header').height() - $('.header').height() - $('#footer').height() - $('.filter_area').height() - $('.filter_area').height();//tolok tinggi header ngan nav-bar header.
        var width = $('#content').width() - 40;

        $('#dvData').css({'height': height});
        $('#dvData').css({'width': width});
        $('#dvData').css({'max-width': width});
        $('#dvData').css({'overflow-x': 'auto'});

    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<

</script>