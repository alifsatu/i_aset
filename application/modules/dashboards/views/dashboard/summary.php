<?php /**
 *  START SUMMARY TAB CONTENT 
 */
?>
<div class="tab-pane fade-in" id="summary">

    <div class="row">
        <div class="col-md-6">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Ringkasan Bilangan Permohonan vs Daerah Yang Diterima</header>
                <div class="panel-body">

                    <!--                        <div id="containerSummary1" style="min-width: 200px; height: 400px; max-width: 600px; margin: 0 auto">
                                                Loading Graph...
                                            </div>-->
                    <div class="row">
                        <div class="input-group col-md-2 col-sm-2">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan" id="tahun_pilihan" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData(this.value, 'All')">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-2 col-sm-2">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan" id="bulan_pilihan" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan', 'bulan_pilihan', 'loadChartDatadaerah')">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>



                        <div class="input-group col-md-6 col-sm-6">
                            <label>Jenis Graf</label>
                            <div class="controls">
                                <input type="radio" name="mychart" class="mychart" id= "column" value="column" onclick= "chartfunc()" checked>Column
                                <input type="radio" name="mychart" class="mychart" id= "bar" value="bar" onclick= "chartfunc()">Bar
                                <input type="radio" name="mychart" class="mychart" id= "pie" value="pie" onclick= "chartfunc()">Pie
                                <input type="radio" name="mychart" class="mychart" id= "line" value="line" onclick= "chartfunc()">Line
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <br>
                        <div id="containerDynamic" style="min-width: 200px; height: 400px; margin: 0 auto"></div>
                    </div>

                </div>

            </section>
        </div>

        <div class="col-md-6">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Ringkasan Bilangan Permohonan vs Pegawai Yang Diterima</header>
                <div class="panel-body">

                    <div class="row">
                        <div class="input-group col-md-2 col-sm-2">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan2" id="tahun_pilihan2" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData2(this.value, 'All')">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-2 col-sm-2">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan2" id="bulan_pilihan2" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan2', 'bulan_pilihan2', 'loadChartDatapegawai')">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>

                        <div class="input-group col-md-6 col-sm-6">
                            <label>Jenis Graf</label>
                            <div class="controls">
                                <input type="radio" name="mychart2" class="mychart" id= "column2" value="column" onclick= "chartfunc2()" checked>Column
                                <input type="radio" name="mychart2" class="mychart" id= "bar2" value="bar" onclick= "chartfunc2()">Bar
                                <input type="radio" name="mychart2" class="mychart" id= "pie2" value="pie" onclick= "chartfunc2()">Pie
                                <input type="radio" name="mychart2" class="mychart" id= "line2" value="line" onclick= "chartfunc2()">Line
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div id="containerDynamicPegawai" style="min-width: 200px; height: 400px; margin: 0 auto"></div>
                    </div>

                </div>

            </section>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Jadual Ringkasan Bilangan Permohonan vs Daerah Yang Diterima</header>
                <div class="panel-body">

                    <!--                        <div id="containerSummary1" style="min-width: 200px; height: 400px; max-width: 600px; margin: 0 auto">
                                                Loading Graph...
                                            </div>-->
                    <div class="row">
                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan_table1" id="tahun_pilihan_table1" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadTableDaerah(this.value, 'All')">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan_table1" id="bulan_pilihan_table1" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_table1', 'bulan_pilihan_table1', 'loadtabledaerah')">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>

                    </div>



                    <div class="row">
                        <br>
                        <div class="table table-responsive">

                            <table id="table_daerah" class="table table-bordered table-condensed table-striped table-hover" >

                                <thead>
                                <th>Bil.</th>
                                <th>Daerah</th>
                                <th>Bilangan Permohonan</th>
                                </thead>
                                <?php
                                $count = 1;
                                $total = 0;
                                foreach ($getPermohonanVSdaerah as $rec) {
                                    ?>

                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= $rec->name ?></td>
                                        <td><?php
                                            echo intval($rec->y);
                                            $total += $rec->y;
                                            ?>
                                        </td>
                                    </tr>

                                <?php } ?>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th style="text-align:right">Jumlah:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>


                            </table>
                        </div>
                    </div>

                </div>

            </section>
        </div>

        <div class="col-md-6">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">Jadual Ringkasan Bilangan Permohonan vs Pegawai Yang Diterima</header>
                <div class="panel-body">

                    <div class="row">
                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Tahun</label>
                            <div class="controls">
                                <select name="tahun_pilihan_table2" id="tahun_pilihan_table2" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadTablePegawai(this.value, 'All')">
                                    <option value="All">All</option>
                                    <?php
                                    foreach ($list_tahun as $p) {
                                        ?>
                                        <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="input-group  col-md-2 col-sm-2">
                            <label>Bulan</label>
                            <div class="controls">
                                <select name="bulan_pilihan_table2" id="bulan_pilihan_table2" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_table2', 'bulan_pilihan_table2', 'loadtablepegawai')">
                                    <option value="All">All</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Mac</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Julai</option>
                                    <option value="08">Ogos</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Disember</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="table table-responsive">

                            <table id="table_pegawai" class="table table-bordered table-condensed table-striped table-hover" >

                                <thead>
                                <th>Bil.</th>
                                <th>Pegawai Ulasan</th>
                                <th>Bilangan Permohonan</th>
                                </thead>
                                <?php
                                $count2 = 1;
                                $total2 = 0;
                                foreach ($getPermohonanVSpegawai as $rec) {
                                    ?>

                                    <tr>
                                        <td><?= $count2++ ?></td>
                                        <td><?= $rec->name ?></td>
                                        <td><?php
                                            echo intval($rec->y);
                                            $total2 += $rec->y;
                                            ?>
                                        </td>
                                    </tr>

                                <?php } ?>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th style="text-align:right">Jumlah:</th>
                                        <th></th>
                                    </tr>
                                </tfoot>


                            </table>
                        </div>
                    </div>

                </div>

            </section>
        </div>
    </div>


</div>
<?php /**
 *  END SUMMARY TAB CONTENT 
 */
?>



<script type="text/javascript">
    function initSummaryTab(tahun = '') {

        $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/load_summary_charts') ?>", $("#forma").serialize() + '&tahun_pilihan=' + tahun + '&tahun_pilihan2=' + tahun, function (chartData) {
            $("#containerDynamic").html(chartData).show('slow');
            $("#containerDynamicPegawai").html(chartData).show('slow');
        });
    }


    function loadTableDaerah(selectedYear, selectedMonth) {

        var tahun = ' Semua Tahun';

        if (selectedYear != 'All') {
            var tahun = ' Bagi Tahun  ' + selectedYear;
        }

        var bulan = ' dan Semua bulan';

        if (selectedMonth != 'All') {
            var bulan = ' dan Bulan  ' + selectedMonth;
        }

        $.ajax({
            dataType: "json",
            type: "GET",
            url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/getPermohonanVSdaerah') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
        }).done(function (data, textStatus, jqXHR) {

            var arr = [];

            for (var i = 0; i < data.length; i++) {
                var obj = {};
                obj['bil'] = i + 1;
                obj['name'] = data[i].name;
                obj['y'] = parseInt(data[i].y);
                arr.push(obj);
            }


            $('#table_daerah').DataTable({
                //here is the solution
                destroy: true,
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                            .column(2)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    // Total over this page
                    pageTotal = api
                            .column(2, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(pageTotal + ' ( ' + total + ' Keseluruhan)' + tahun + bulan);
                },
                dom: 'Bfrtip',
                buttons: [
                    {extend: 'copyHtml5', footer: true},
                    {extend: 'excelHtml5', footer: true},
                    {extend: 'csvHtml5', footer: true},
                    {extend: 'pdfHtml5', footer: true},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                    .find('table')
                                    .append(
                                            '<tr><td></td><td><b>Jumlah : </b></td><td><b>' + pageTotal + ' ( ' + total + ' Keseluruhan)' + tahun + bulan + '<b></td></tr>'
                                            );

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }

                ],
                "data": arr,
                "columns": [
                    {"data": "bil"},
                    {"data": "name"},
                    {"data": "y"}
                ]
            });


        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(jqXHR);
            console.log("There was an error." + errorThrown);
        });


    }
    function loadTablePegawai(selectedYear, selectedMonth) {

        var tahun = ' Semua Tahun';

        if (selectedYear != 'All') {
            var tahun = ' Bagi Tahun  ' + selectedYear;
        }

        var bulan = ' dan Semua bulan';

        if (selectedMonth != 'All') {
            var bulan = ' dan Bulan  ' + selectedMonth;
        }

        $.ajax({
            dataType: "json",
            type: "GET",
            url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/getPermohonanVSpegawai') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
        }).done(function (data, textStatus, jqXHR) {

            var arr = [];

            for (var i = 0; i < data.length; i++) {
                var obj = {};
                obj['bil'] = i + 1;
                obj['name'] = data[i].name;
                obj['y'] = parseInt(data[i].y);
                arr.push(obj);
            }


            $('#table_pegawai').DataTable({
                //here is the solution
                destroy: true,
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                            .column(2)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    // Total over this page
                    pageTotal = api
                            .column(2, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    // Update footer
                    $(api.column(2).footer()).html(pageTotal + ' ( ' + total + ' Keseluruhan)' + tahun + bulan);
                },
                dom: 'Bfrtip',
                buttons: [
                    {extend: 'copyHtml5', footer: true},
                    {extend: 'excelHtml5', footer: true},
                    {extend: 'csvHtml5', footer: true},
                    {extend: 'pdfHtml5', footer: true},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body)
                                    .find('table')
                                    .append(
                                            '<tr><td></td><td><b>Jumlah : </b></td><td><b>' + pageTotal + ' ( ' + total + ' Keseluruhan)' + tahun + bulan + '<b></td></tr>'
                                            );

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }

                ],
                "data": arr,
                "columns": [
                    {"data": "bil"},
                    {"data": "name"},
                    {"data": "y"}
                ]
            });


        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(jqXHR);
            console.log("There was an error." + errorThrown);
        });


    }

    
</script>