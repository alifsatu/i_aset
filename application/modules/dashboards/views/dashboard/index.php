
<style>
    .event_popover .popover {
        width: 200px;
    }

    .legendLabel { color:#000 }
    .ui-datepicker-calendar {
        display: none;
    }



    @media screen and (min-width: 601px) {
        .statistic_font{
            font-size:2vw
        }
    }
    @media screen and (max-width: 600px) {
        .statistic_font{
            font-size:5vw
        }
    }
</style>


<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane fade-in active" id="main">
        <br>
        <section class="scrollable padder">

            <section class="panel panel-default">
                <div class="row m-l-none m-r-none bg-light lter">
                    <div class="col-sm-3 col-md-3 padder-v b-r b-light center" data-toggle="tooltip" data-placement="bottom" title="Jumlah Projek Keseluruhan">
                        <span class="fa-stack fa-2x m-r-sm">
                            <i class="fa fa-circle fa-stack-2x text-warning"></i>
                            <i class="fa fa-list-ol fa-stack-1x text-white"></i>
                        </span><br>
                        <span class="h3 block m-t-xs statistic_font">
                            <strong><?= $all_projects->num ?></strong></span>
                        <br>
                        <hr>
                        <small class="text-muted text-uc">Jumlah Projek Keseluruhan</small>
                    </div>

                    <div class="col-sm-3 col-md-3 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom" title="Jumlah Projek Dicipta Oleh Anda">                     
                        <span class="fa-stack fa-2x m-r-sm" >
                            <i class="fa fa-circle fa-stack-2x text-danger"></i>
                            <i class="fa fa-list-alt fa-stack-1x text-white"></i>
                        </span><br>
                        <span class="h3 block m-t-xs statistic_font" >
                            <strong><?= $total_created_projects->num ?></strong>
                        </span>
                        <br>
                        <hr>
                        <small class="text-muted text-uc" >Jumlah Projek Anda</small>
                    </div>

                    <div class="col-sm-3 col-md-3 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom"  title="Jumlah Sumbangan Modal Keseluruhan">
                        <span class="fa-stack fa-2x m-r-sm">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-list-ul fa-stack-1x text-white"></i>
                        </span>

                        <span class="h3 block m-t-xs center statistic_font">
                            <strong>RM<?= number_format($total_sum_modal->total, 2); ?></strong>
                        </span>
                        <br>
                        <hr>
                        <small class="text-muted text-uc text-sm">Jumlah Kutipan Sumbangan Modal Keseluruhan</small>

                    </div>

                    <div class="col-sm-3 col-md-3 padder-v b-r b-light" data-toggle="tooltip" data-placement="bottom"  title="Jumlah Sumbangan Modal Projek Anda">
                        <span class="fa-stack fa-2x m-r-sm">
                            <i class="fa fa-circle fa-stack-2x text-success"></i>
                            <i class="fa fa-tasks fa-stack-1x text-white"></i>
                        </span>

                        <span class="h3 block m-t-xs statistic_font">
                            <strong>RM<?= number_format($total_sum_modal_by_user->total, 2); ?></strong>
                        </span>
                        <br>
                        <hr>
                        <small class="text-muted text-uc">Jumlah Sumbangan Modal Projek Anda</small>

                    </div>
                </div>
            </section>


            <!--    <div class="row">
                    <hr>
                    <div class="input-group col-md-6">
                        <h4>Bulan Pilihan : <?php echo isset($MonthName) ? $MonthName : ''; ?> </h4><br>
                    </div>
                    
                    <div class="input-group col-md-6">
            <?php echo form_open($this->uri->uri_string(), 'id="formdate"'); ?>
                        <label for="startDate">Tukar Bulan  </label>:  <input placeholder="Select month" readonly="readonly" name="selectedMonth" id="selectedMonth" class="input-sm date-picker disabled" /><i class="fa fa-calendar"></i>
            <?php echo form_close(); ?>
                    </div>
                    <hr>     
                </div>-->



            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Kutipan Sumbangan Modal Secara Keseluruhan Bagi Tahun
        <!--                    <span class="pull-right">
                                <a href="#" data-toggle="modal" data-target="#myModal" title="Click For Detailed Chart">
                                    <i class="fa fa-external-link" >
                                    </i><label style="cursor: pointer"> More</label>
                                </a>
                            </span>-->
                        </header>
                        <div class="panel-body" style="position:relative">

                            <div class="row">
                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Tahun</label>
                                    <div class="controls">
                                        <select name="tahun_pilihan_KSMT0" id="tahun_pilihan_KSMT0" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData_KSMT0(this.value, 'All')">
                                            <option value="All">All</option>
                                            <?php
                                            foreach ($list_tahun as $p) {
                                                ?>
                                                <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Bulan</label>
                                    <div class="controls">
                                        <select name="bulan_pilihan_KSMT0" id="bulan_pilihan_KSMT0" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_KSMT0', 'bulan_pilihan_KSMT0', 'loadChartData_KSMT0')">
                                            <option value="All">All</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Mac</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Julai</option>
                                            <option value="08">Ogos</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Disember</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="input-group col-md-6 col-sm-6">
                                    <label>Jenis Graf</label>
                                    <div class="controls">
                                        <input type="radio" name="mychart_KSMT0" class="mychart" id= "column_KSMT0" value="column" onclick= "chartfunc_KSMT0()" checked>Column
                                        <input type="radio" name="mychart_KSMT0" class="mychart" id= "bar_KSMT0" value="bar" onclick= "chartfunc_KSMT0()">Bar
                                        <input type="radio" name="mychart_KSMT0" class="mychart" id= "pie_KSMT0" value="pie" onclick= "chartfunc_KSMT0()">Pie
                                        <input type="radio" name="mychart_KSMT0" class="mychart" id= "line_KSMT0" value="line" onclick= "chartfunc_KSMT0()">Line
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="container" style="min-width: 250px; height: 400px; margin: 0 auto">
                                    Loading Graph...
                                </div>
                            </div>

                        </div>

                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Kutipan Sumbangan Modal Anda Bagi Tahun </header>
                        <div class="panel-body">

                            <div class="row">
                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Tahun</label>
                                    <div class="controls">
                                        <select name="tahun_pilihan_KSMT1" id="tahun_pilihan_KSMT1" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData_KSMT1(this.value, 'All')">
                                            <option value="All">All</option>
                                            <?php
                                            foreach ($list_tahun as $p) {
                                                ?>
                                                <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Bulan</label>
                                    <div class="controls">
                                        <select name="bulan_pilihan_KSMT1" id="bulan_pilihan_KSMT1" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_KSMT1', 'bulan_pilihan_KSMT1', 'loadChartData_KSMT1')">
                                            <option value="All">All</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Mac</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Julai</option>
                                            <option value="08">Ogos</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Disember</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="input-group col-md-6 col-sm-6">
                                    <label>Jenis Graf</label>
                                    <div class="controls">
                                        <input type="radio" name="mychart_KSMT1" class="mychart" id= "column_KSMT1" value="column" onclick= "chartfunc_KSMT1()" checked>Column
                                        <input type="radio" name="mychart_KSMT1" class="mychart" id= "bar_KSMT1" value="bar" onclick= "chartfunc_KSMT1()">Bar
                                        <input type="radio" name="mychart_KSMT1" class="mychart" id= "pie_KSMT1" value="pie" onclick= "chartfunc_KSMT1()">Pie
                                        <input type="radio" name="mychart_KSMT1" class="mychart" id= "line_KSMT1" value="line" onclick= "chartfunc_KSMT1()">Line
                                    </div>
                                </div>
                            </div>



                            <div class="row">

                                <div id="container2" style="min-width: 200px; height: 400px; margin: 0 auto">
                                    Loading Graph...
                                </div>
                            </div>

                        </div>

                    </section>
                </div>



            </div>

            <div class="row">

                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">
                            Kutipan Sumbangan Modal Secara Keseluruhan Bagi Tahun<br>
                            (Berdasarkan Tahun Permohonan dan Status Permohonan = Lulus)
                        </header>
                        <div class="panel-body" style="position:relative">
                            <div class="row">
                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Tahun</label>
                                    <div class="controls">
                                        <select name="tahun_pilihan_KSMT3" id="tahun_pilihan_KSMT3" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData_KSMT3(this.value, 'All')">
                                            <option value="All">All</option>
                                            <?php
                                            foreach ($list_tahun as $p) {
                                                ?>
                                                <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Bulan</label>
                                    <div class="controls">
                                        <select name="bulan_pilihan_KSMT3" id="bulan_pilihan_KSMT3" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_KSMT3', 'bulan_pilihan_KSMT3', 'loadChartData_KSMT3')">
                                            <option value="All">All</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Mac</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Julai</option>
                                            <option value="08">Ogos</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Disember</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="input-group col-md-6 col-sm-6">
                                    <label>Jenis Graf</label>
                                    <div class="controls">
                                        <input type="radio" name="mychart_KSMT3" class="mychart" id= "column_KSMT3" value="column" onclick= "chartfunc_KSMT3()" checked>Column
                                        <input type="radio" name="mychart_KSMT3" class="mychart" id= "bar_KSMT3" value="bar" onclick= "chartfunc_KSMT3()">Bar
                                        <input type="radio" name="mychart_KSMT3" class="mychart" id= "pie_KSMT3" value="pie" onclick= "chartfunc_KSMT3()">Pie
                                        <input type="radio" name="mychart_KSMT3" class="mychart" id= "line_KSMT3" value="line" onclick= "chartfunc_KSMT3()">Line
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <br>
                                <div id="container5" style="min-width: 250px; height: 400px; margin: 0 auto">
                                    Loading Graph...
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">
                            Kutipan Sumbangan Modal Anda Bagi Tahun <br>
                            (Berdasarkan Tahun Permohonan dan Status Permohonan = Lulus)
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Tahun</label>
                                    <div class="controls">
                                        <select name="tahun_pilihan_KSMT4" id="tahun_pilihan_KSMT4" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="loadChartData_KSMT4(this.value, 'All')">
                                            <option value="All">All</option>
                                            <?php
                                            foreach ($list_tahun as $p) {
                                                ?>
                                                <option value="<?= $p->tahun ?>"><?= $p->tahun ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-group col-md-2 col-sm-2">
                                    <label>Bulan</label>
                                    <div class="controls">
                                        <select name="bulan_pilihan_KSMT4" id="bulan_pilihan_KSMT4" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="checkTahunSelected('tahun_pilihan_KSMT4', 'bulan_pilihan_KSMT4', 'loadChartData_KSMT4')">
                                            <option value="All">All</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Mac</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Julai</option>
                                            <option value="08">Ogos</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Disember</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="input-group col-md-6 col-sm-6">
                                    <label>Jenis Graf</label>
                                    <div class="controls">
                                        <input type="radio" name="mychart_KSMT4" class="mychart" id= "column_KSMT4" value="column" onclick= "chartfunc_KSMT4()" checked>Column
                                        <input type="radio" name="mychart_KSMT4" class="mychart" id= "bar_KSMT4" value="bar" onclick= "chartfunc_KSMT4()">Bar
                                        <input type="radio" name="mychart_KSMT4" class="mychart" id= "pie_KSMT4" value="pie" onclick= "chartfunc_KSMT4()">Pie
                                        <input type="radio" name="mychart_KSMT4" class="mychart" id= "line_KSMT4" value="line" onclick= "chartfunc_KSMT4()">Line
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div id="container6" style="min-width: 200px; height: 400px; margin: 0 auto">
                                    Loading Graph...
                                </div>
                            </div>

                        </div>

                    </section>
                </div>



            </div>

            <div class="row">
                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Jumlah Projek Mengikut Status Permohonan</header>
                        <div class="panel-body">

                            <div id="container3" style="min-width: 200px; height: 400px; margin: 0 auto">
                                Loading Graph...
                            </div>

                        </div>

                    </section>
                </div>

                <div class="col-md-6">
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Jumlah Projek Mengikut Status Terkini Projek</header>
                        <div class="panel-body">

                            <div id="container4" style="min-width: 200px; height: 400px; margin: 0 auto">
                                Loading Graph...
                            </div>

                        </div>

                    </section>
                </div>
            </div>


        </section>

    </div>


    <?php include('summary.php'); ?>
    <?php include('summary2.php'); ?>


</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg " style="width:auto !important">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Work History</h4>
            </div>
            <div class="modal-body">


                <form id="forma" >
                    <div class="input-group  col-md-2">
                        <label>Projects</label>
                        <div class="controls">
                            <select name="projects" id="projects" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px; width:100px;" onchange="requery('projects')">
                                <option></option>
                                <option value="%%">ALL</option>
                                <?php
                                foreach ($pr as $p) {
                                    ?>
                                    <option value="<?= $p->id ?>"><?= $p->project_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="input-group  col-md-2">
                        <label>Date From</label>
                        <div class="controls">
                            <input name="datefrom" type="text" data-date-format="dd-mm-yyyy" class="input-s input-sm datepicker-input form-control"  style="z-index:100001" id="datefrom"  />
                        </div>
                    </div>
                    <div class="input-group  col-md-2">
                        <label>Date To</label>
                        <div class="controls">
                            <input name="dateto" data-date-format="dd-mm-yyyy" type="text" class="input-s input-sm datepicker-input form-control"    id="dateto" >
                        </div>
                    </div>
                </form> 


                <div class="input-group  col-md-2">
                    <label>Users</label>
                    <div class="controls">
                        <select name="users" id="users" class="selecta input-sm form-control" style="height:24px; border: #ccc solid 1px;" onchange="requery('users')">
                            <option></option>
                            <?php
                            foreach ($us as $u) {
                                ?>
                                <option value="<?= $u->userid ?>"><?= $u->display_name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="input-group  col-md-2">
                    <label>Your Projects</label>
                    <div class="controls"><a href="#" onclick="requery('projleader')" class="btn btn-primary"><i class="fa fa-bar-chart"></i></a></div></div>
                <div id="containercommon"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        initMainDashboard();

        $('#table_daerah').dataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                        .column(2)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(2, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $(api.column(2).footer()).html(pageTotal + ' ( ' + total + ' Keseluruhan) Semua Tahun');
            },
            dom: 'Bfrtip',
            buttons: [
                {extend: 'copyHtml5', footer: true},
                {extend: 'excelHtml5', footer: true},
                {extend: 'csvHtml5', footer: true},
                {extend: 'pdfHtml5', footer: true},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body)
                                .find('table')
                                .append(
                                        '<tr><td></td><td><b>Jumlah : </b></td><td><b>' + pageTotal + ' ( ' + total + ' Keseluruhan)' + '<b></td></tr>'
                                        );

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });
        $('#table_pegawai').dataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                        .column(2)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Total over this page
                pageTotal = api
                        .column(2, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                // Update footer
                $(api.column(2).footer()).html(pageTotal + ' ( ' + total + ' Keseluruhan) Semua Tahun');
            },
            dom: 'Bfrtip',
            buttons: [
                {extend: 'copyHtml5', footer: true},
                {extend: 'excelHtml5', footer: true},
                {extend: 'csvHtml5', footer: true},
                {extend: 'pdfHtml5', footer: true},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body)
                                .find('table')
                                .append(
                                        '<tr><td></td><td><b>Jumlah : </b></td><td><b>' + pageTotal + ' ( ' + total + ' Keseluruhan)' + '<b></td></tr>'
                                        );

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });

    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        initMainDashboard();
        initSummaryTab();
    });




    jQuery(document).on('click', '[data-toggle="ajaxModal1"]',
            function (e) {


                $('#ajaxModal').remove();
                e.preventDefault();
                var $this = $(this)


                var $remote = $this.data('remote') || $this.attr('href'),
                        $modal = $('<div class="modal fade"  id="ajaxModal"><div class="modal-dialog2">' +
                                '<div class="modal-body" style="padding:0;margin-top:100px;"></div>' +
                                '</div></div>');
                // $('body').append($modal);
                $modal.modal();
                $modal.find('.modal-body').load($remote, function () {




                });
            });



    function initMainDashboard() {


//        $("#container,#container2,#container3,#container4,#container5,#container6").html('<p style="padding:20%"><i class="fa fa-spinner fa-spin fa-3x"></i></p>');

        Highcharts.setOptions({
//            colors: ['rgb(76, 192, 193)', 'rgb(101, 189, 119)', 'rgb(255, 195, 51)', 'rgb(251, 107, 91)', 'rgb(46, 62, 78)'],
            lang: {
                thousandsSep: ','
            }
        });
        //$("#dateto,#datefrom").datepciker({format:"dd-mm-yyyy"});

        $("#dateto").on("changeDate", function () {
            requery('projects');
            $(this).datepicker('hide');
        });
        $("#datefrom").on("changeDate", function () {
            $(this).datepicker('hide');
        });

        /*
         $('#container_xxx').highcharts({
         chart: {
         type: 'column'
         },
         title: {
         text: ''
         },
         subtitle: {
         text: ''
         },
         xAxis: {
         type: 'category',
         title: {
         text: 'Tahun Kutipan'
         }
         },
         yAxis: {
         title: {
         text: 'Jumlah Kutipan'
         }
         
         },
         legend: {
         enabled: true
         },
         plotOptions: {
         series: {
         borderWidth: 0,
         dataLabels: {
         enabled: true,
         format: 'RM {point.y:,.2f}'
         },
         showInLegend: true
         }
         },
         
         tooltip: {
         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
         },
         lang: {
         thousandsSep: ','
         },
         
         series: [{
         name: "Tahun",
         colorByPoint: true,
         data:
         [<?php
                            foreach ($jumlah_bayaran_terima_by_tahun as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
         }]
         });
         
         $('#container2_xxx').highcharts({
         chart: {
         type: 'column'
         },
         title: {
         text: ''
         },
         subtitle: {
         text: ''
         },
         xAxis: {
         type: 'category',
         title: {
         text: 'Tahun Kutipan'
         }
         },
         yAxis: {
         title: {
         text: 'Jumlah Kutipan'
         }
         
         },
         legend: {
         enabled: true
         },
         plotOptions: {
         series: {
         borderWidth: 0,
         dataLabels: {
         enabled: true,
         format: 'RM {point.y:,.2f}'
         },
         showInLegend: true
         }
         },
         
         tooltip: {
         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
         },
         
         series: [{
         name: "Tahun",
         colorByPoint: true,
         data:
         [<?php
                            foreach ($jumlah_bayaran_terima_by_tahun_user as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
         }]
         });
         */

        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Jumlah Projek'
                }
            },
            yAxis: {
                title: {
                    text: 'Status Permohonan'
                }

            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b> {point.y}</b><br/>'
            },

            series: [{
                    name: "Status Permohonan",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_projek_by_status_permohonan as $t) {

                                echo "{ name :\"" . $t->nama . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }]
        });

        $('#container4').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Jumlah Projek'
                }
            },
            yAxis: {
                title: {
                    text: 'Status Permohonan'
                }

            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b> {point.y}</b><br/>'
            },

            series: [{
                    name: "Status Permohonan",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_projek_by_status_terkini as $t) {

                                echo "{ name :\"" . $t->nama . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }]
        });

        /*
         $('#container5xx').highcharts({
         chart: {
         type: 'column'
         },
         title: {
         text: ''
         },
         subtitle: {
         text: ''
         },
         xAxis: {
         type: 'category',
         title: {
         text: 'Tahun Terima Permohonan'
         }
         },
         yAxis: {
         title: {
         text: 'Jumlah Kutipan'
         }
         
         },
         legend: {
         enabled: true
         },
         plotOptions: {
         series: {
         borderWidth: 0,
         dataLabels: {
         enabled: true,
         format: 'RM {point.y:,.2f}'
         },
         showInLegend: true
         }
         },
         
         tooltip: {
         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
         },
         
         series: [{
         name: "Tahun",
         colorByPoint: true,
         data:
         [<?php
                            foreach ($jumlah_permohonan_terima_by_tahun as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
         }]
         });
         
         $('#container6xx').highcharts({
         chart: {
         type: 'column'
         },
         title: {
         text: ''
         },
         subtitle: {
         text: ''
         },
         xAxis: {
         type: 'category',
         title: {
         text: 'Tahun Terima Permohonan'
         }
         },
         yAxis: {
         title: {
         text: 'Jumlah Kutipan'
         }
         
         },
         legend: {
         enabled: true
         },
         plotOptions: {
         series: {
         borderWidth: 0,
         dataLabels: {
         enabled: true,
         format: 'RM {point.y:,.2f}'
         },
         showInLegend: true
         }
         },
         
         tooltip: {
         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
         },
         
         series: [{
         name: "Tahun",
         colorByPoint: true,
         data:
         [<?php
                            foreach ($jumlah_permohonan_terima_by_tahun_user as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
         }]
         });
         */



    }

    function requery(val)
    {

        $("#containercommon").html('<p style="padding:20%"><i class="fa fa-spinner fa-spin fa-3x"></i></p>');
        if (val == "projects") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", $("#forma").serialize() + '&projdaterange=', function (chartData) {

                console.log($("#forma").serialize());
                $("#containercommon").html(chartData).show('slow');
            });

        }

        if (val == "users") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", $("#users").serialize() + '&useronly=', function (chartData) {

                console.log(chartData);
                $("#containercommon").html(chartData).show('slow');
            });

        }




        if (val == "projleader") {
            $.post("<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadchart') ?>", {projectleader: "<?= $this->auth->user_id() ?>"}, function (chartData) {

                console.log(chartData);
                $("#containercommon").html(chartData).show('slow');
            });

        }
    }

    function resetdd()
    {
        $(".selecta").select2("val", "");
        $(".datepicker-input").val("");
        $("#containercommon").html("");
    }


    $(function () {
        $('.date-picker').datepicker({
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months",
            defaultDate: <?= $selectedMonth ?>
        });


        $('#selectedMonth').change(function () {

            $('#formdate').submit();
            waitingDialog.show('Retrieveing and compiling data from server...');
        });

        $('#selectedMonth').on("changeDate", function (ev) {
            var newDate = new Date(ev.date);
            var mth = newDate.getMonth() + 1;
            var yr = newDate.getFullYear();
            $('input[name="selectedMonth"]').val(mth + '-' + yr);

            $('#formdate').submit();
        });

    });

//    chart_KSMT0 container
    //Kutipan Sumbangan Modal Secara Keseluruhan Bagi Tahun
    $(function () {
        var chart_KSMT0;
        var options = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Tahun Kutipan'
                }
            },
            yAxis: {
                title: {
                    text: 'Jumlah Kutipan'
                }

            },
            legend: {
                enabled: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: 'RM {point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
            },
            lang: {
                thousandsSep: ','
            },

            series: [{
                    name: "Tahun",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_bayaran_terima_by_tahun as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }],
            //
            drilldown: {
                series: []
            }
        };
        options.chart.renderTo = 'container';
        options.chart.type = 'column';
        chart_KSMT1 = new Highcharts.Chart(options);
        chartfunc_KSMT0 = function () {

            var column = document.getElementById('column_KSMT0');
            var bar = document.getElementById('bar_KSMT0');
            var pie = document.getElementById('pie_KSMT0');
            var line = document.getElementById('line_KSMT0');
            if (column.checked)
            {
                options.chart.renderTo = 'container';
                options.chart.type = 'column';
                var chart_KSMT0 = new Highcharts.Chart(options);
            } else if (bar.checked) {
                options.chart.renderTo = 'container';
                options.chart.type = 'bar';
                var chart_KSMT0 = new Highcharts.Chart(options);
            } else if (pie.checked) {
                options.chart.renderTo = 'container';
                options.chart.type = 'pie';
                var chart_KSMT0 = new Highcharts.Chart(options);
            } else {
                options.chart.renderTo = 'container';
                options.chart.type = 'line';
                var chart_KSMT0 = new Highcharts.Chart(options);
            }

        }

        loadChartData_KSMT0 = function (selectedYear, selectedMonth) {
//            var selectedYear = $("#tahun_pilihan").val();

            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadChartData_KSMT0') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options.chart.renderTo = 'container';
//                options.chart.type = 'column';
                options.series = [{
                        name: "Tahun",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart_KSMT0 = new Highcharts.Chart(options);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });

//    chart_KSMT1 container2
    //Kutipan Sumbangan Modal Anda Bagi Tahun
    $(function () {
        var chart_KSMT1;
        var options = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Tahun Kutipan'
                }
            },
            yAxis: {
                title: {
                    text: 'Jumlah Kutipan'
                }

            },
            legend: {
                enabled: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: 'RM {point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
            },

            series: [{
                    name: "Tahun",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_bayaran_terima_by_tahun_user as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }],
            //
            drilldown: {
                series: []
            }
        };
        options.chart.renderTo = 'container2';
        options.chart.type = 'column';
        chart_KSMT1 = new Highcharts.Chart(options);
        chartfunc_KSMT1 = function () {

            var column = document.getElementById('column_KSMT1');
            var bar = document.getElementById('bar_KSMT1');
            var pie = document.getElementById('pie_KSMT1');
            var line = document.getElementById('line_KSMT1');
            if (column.checked)
            {
                options.chart.renderTo = 'container2';
                options.chart.type = 'column';
                var chart_KSMT1 = new Highcharts.Chart(options);
            } else if (bar.checked) {
                options.chart.renderTo = 'container2';
                options.chart.type = 'bar';
                var chart_KSMT1 = new Highcharts.Chart(options);
            } else if (pie.checked) {
                options.chart.renderTo = 'container2';
                options.chart.type = 'pie';
                var chart_KSMT1 = new Highcharts.Chart(options);
            } else {
                options.chart.renderTo = 'container2';
                options.chart.type = 'line';
                var chart_KSMT1 = new Highcharts.Chart(options);
            }

        }

        loadChartData_KSMT1 = function (selectedYear, selectedMonth) {
//            var selectedYear = $("#tahun_pilihan").val();

            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadChartData_KSMT1') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options.chart.renderTo = 'container2';
//                options.chart.type = 'column';
                options.series = [{
                        name: "Tahun",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart_KSMT3 = new Highcharts.Chart(options);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });





    //    chart_KSMT3 container5
    //Kutipan Sumbangan Modal Secara Keseluruhan Bagi Tahun
    //(Berdasarkan Tahun Permohonan dan Status Permohonan = Lulus)
    $(function () {
        var chart_KSMT3;
        var options = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Tahun Terima Permohonan'
                }
            },
            yAxis: {
                title: {
                    text: 'Jumlah Kutipan'
                }

            },
            legend: {
                enabled: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: 'RM {point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
            },

            series: [{
                    name: "Tahun",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_permohonan_terima_by_tahun as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }],
            //
            drilldown: {
                series: []
            }
        };
        options.chart.renderTo = 'container5';
        options.chart.type = 'column';
        chart_KSMT3 = new Highcharts.Chart(options);
        chartfunc_KSMT3 = function () {

            var column = document.getElementById('column_KSMT3');
            var bar = document.getElementById('bar_KSMT3');
            var pie = document.getElementById('pie_KSMT3');
            var line = document.getElementById('line_KSMT3');
            if (column.checked)
            {
                options.chart.renderTo = 'container5';
                options.chart.type = 'column';
                var chart_KSMT3 = new Highcharts.Chart(options);
            } else if (bar.checked) {
                options.chart.renderTo = 'container5';
                options.chart.type = 'bar';
                var chart_KSMT3 = new Highcharts.Chart(options);
            } else if (pie.checked) {
                options.chart.renderTo = 'container5';
                options.chart.type = 'pie';
                var chart_KSMT3 = new Highcharts.Chart(options);
            } else {
                options.chart.renderTo = 'container5';
                options.chart.type = 'line';
                var chart_KSMT3 = new Highcharts.Chart(options);
            }

        }

        loadChartData_KSMT3 = function (selectedYear, selectedMonth) {
//            var selectedYear = $("#tahun_pilihan").val();

            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadChartData_KSMT3') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options.chart.renderTo = 'container5';
//                options.chart.type = 'column';
                options.series = [{
                        name: "Tahun",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart_KSMT3 = new Highcharts.Chart(options);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });



//    chart_KSMT4 container6
    //Kutipan Sumbangan Modal Anda Bagi Tahun 
    //(Berdasarkan Tahun Permohonan dan Status Permohonan = Lulus)
    $(function () {
        var chart_KSMT4;
        var options = {
            chart: {
                events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            // Show the loading label
                            chart.showLoading('Loading ...');
                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                title: {
                    text: 'Tahun Terima Permohonan'
                }
            },
            yAxis: {
                title: {
                    text: 'Jumlah Kutipan'
                }

            },
            legend: {
                enabled: true
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: 'RM {point.y:,.2f}'
                    },
                    showInLegend: true
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>RM {point.y:,.2f}</b><br/>'
            },

            series: [{
                    name: "Tahun",
                    colorByPoint: true,
                    data:
                            [<?php
                            foreach ($jumlah_permohonan_terima_by_tahun_user as $t) {

                                echo "{ name :\"" . $t->tahun . "\",y:" . $t->total . "},";
                            }
                            ?>]
                }],
            //
            drilldown: {
                series: []
            }
        };
        options.chart.renderTo = 'container6';
        options.chart.type = 'column';
        chart_KSMT3 = new Highcharts.Chart(options);
        chartfunc_KSMT4 = function () {

            var column = document.getElementById('column_KSMT4');
            var bar = document.getElementById('bar_KSMT4');
            var pie = document.getElementById('pie_KSMT4');
            var line = document.getElementById('line_KSMT4');
            if (column.checked)
            {
                options.chart.renderTo = 'container6';
                options.chart.type = 'column';
                var chart_KSMT4 = new Highcharts.Chart(options);
            } else if (bar.checked) {
                options.chart.renderTo = 'container6';
                options.chart.type = 'bar';
                var chart_KSMT4 = new Highcharts.Chart(options);
            } else if (pie.checked) {
                options.chart.renderTo = 'container6';
                options.chart.type = 'pie';
                var chart_KSMT4 = new Highcharts.Chart(options);
            } else {
                options.chart.renderTo = 'container6';
                options.chart.type = 'line';
                var chart_KSMT4 = new Highcharts.Chart(options);
            }

        }

        loadChartData_KSMT4 = function (selectedYear, selectedMonth) {
//            var selectedYear = $("#tahun_pilihan").val();

            $.ajax({
                dataType: "json",
                type: "GET",
                url: "<?php echo site_url(SITE_AREA . '/dashboard/dashboards/loadChartData_KSMT4') ?>/" + selectedYear + '/' + selectedMonth + '/ajax'
            }).done(function (data, textStatus, jqXHR) {

                var arr = [];

                for (var i = 0; i < data.length; i++) {
                    var obj = {};
                    obj['name'] = data[i].name;
                    obj['y'] = parseInt(data[i].y);
                    arr.push(obj);
                }

                options.chart.renderTo = 'container6';
//                options.chart.type = 'column';
                options.series = [{
                        name: "Tahun",
                        colorByPoint: true,
                        data: arr
                    }];

                var chart_KSMT4 = new Highcharts.Chart(options);


            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(jqXHR);
                console.log("There was an error." + errorThrown);
            });


        }

    });


    function checkTahunSelected(yearselectID, monthSelectID, tableName) {

        if (yearselectID == '' || yearselectID == 'All') {
            alert('Sila Buat Pilihan Tahun Dahulu!');
        } else {
            if (tableName == 'loadtablepegawai') {
                loadTablePegawai($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadtabledaerah') {
                loadTableDaerah($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadChartDatadaerah') {
                loadChartData($('#' + yearselectID).val(), $('#' + monthSelectID).val());

            } else if (tableName == 'loadChartDatapegawai') {
                loadChartData2($('#' + yearselectID).val(), $('#' + monthSelectID).val());

            } else if (tableName == 'loadChartData_KSMT0') {
                loadChartData_KSMT0($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadChartData_KSMT1') {
                loadChartData_KSMT1($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadChartData_KSMT3') {
                loadChartData_KSMT3($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadChartData_KSMT4') {
                loadChartData_KSMT4($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            } else if (tableName == 'loadChartData_SummaryBSM') {
                loadChartData_SummaryBSM($('#' + yearselectID).val(), $('#' + monthSelectID).val());

            } else if (tableName == 'loadtableJKBSM') {
                loadTableJKBSM($('#' + yearselectID).val(), $('#' + monthSelectID).val());
            }
        }


    }
</script>

