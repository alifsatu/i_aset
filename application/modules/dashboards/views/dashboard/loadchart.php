
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#containercommon').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total project work history'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			<?php if ( isset($tspent) ) {?>
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} hours</b> of total'
			<?php } else { ?>
			pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} hours</b> of total<br/>By:{point.uidname}<br/>Cost:{point.uidcost}'
			<?php } ?>
        },

        series: [{
            name: "Projects",
            colorByPoint: true,
            data: 
			
			[<?php 
			
			if ( isset($tspent) ) {
			
			foreach ( $tspent as $t ) { 
				
				echo "{ name :\"".$t->name."\",y:".intval($t->y).", drilldown:\"".$t->name."\"},";
				
				 }}
				 
				 if ( isset($projleader) ) {
			
			foreach ( $projleader as $t ) { 
				
				echo $t;
				
				 }}
				 
				 
				 ?>]
        }],
      drilldown: {
            series: [
			<?php foreach ( $projectname as $p=>$qey ) { echo "
			{
                name: \"".$qey."\",
                  id: \"".$qey."\",
                data: [ ";
				
				foreach ( $taskdrilldown1 as $td=>$key ) { 
				
				if ($qey == $projref[$td] ) {
				
				echo "
                    [
                        \"".$key ."\",
                        ".intval($taskdrilldown2[$td])."
                    ], ";
					
				}
				
				}
					
				
                    
               echo " ] }, "; 
			   }  ?> ]
        }
    });
});
		</script>
      