<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['dashboards_manage']			= 'Manage';
$lang['dashboards_edit']				= 'Edit';
$lang['dashboards_true']				= 'True';
$lang['dashboards_false']				= 'False';
$lang['dashboards_create']			= 'Save';
$lang['dashboards_list']				= 'List';
$lang['dashboards_new']				= 'New';
$lang['dashboards_edit_text']			= 'Edit this to suit your needs';
$lang['dashboards_no_records']		= 'There aren\'t any dashboards in the system.';
$lang['dashboards_create_new']		= 'Create a new Dashboards.';
$lang['dashboards_create_success']	= 'Dashboards successfully created.';
$lang['dashboards_create_failure']	= 'There was a problem creating the dashboards: ';
$lang['dashboards_create_new_button']	= 'Create New Dashboards';
$lang['dashboards_invalid_id']		= 'Invalid Dashboards ID.';
$lang['dashboards_edit_success']		= 'Dashboards successfully saved.';
$lang['dashboards_edit_failure']		= 'There was a problem saving the dashboards: ';
$lang['dashboards_delete_success']	= 'record(s) successfully deleted.';

$lang['dashboards_purged']	= 'record(s) successfully purged.';
$lang['dashboards_success']	= 'record(s) successfully restored.';


$lang['dashboards_delete_failure']	= 'We could not delete the record: ';
$lang['dashboards_delete_error']		= 'You have not selected any records to delete.';
$lang['dashboards_actions']			= 'Actions';
$lang['dashboards_cancel']			= 'Cancel';
$lang['dashboards_delete_record']		= 'Delete';
$lang['dashboards_delete_confirm']	= 'Are you sure you want to delete this dashboards?';
$lang['dashboards_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['dashboards_action_edit']		= 'Save';
$lang['dashboards_action_create']		= 'Create';

// Activities
$lang['dashboards_act_create_record']	= 'Created record with ID';
$lang['dashboards_act_edit_record']	= 'Updated record with ID';
$lang['dashboards_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['dashboards_column_created']	= 'Created';
$lang['dashboards_column_deleted']	= 'Deleted';
$lang['dashboards_column_modified']	= 'Modified';




$lang['dashboards_home']	= 'Home';
$lang['dashboards']	= 'Dashboard';
$lang['dashboards_earnings']	= 'Earnings this month';
$lang['dashboards_due_week']	= 'Due this week';
$lang['dashboards_due_month']	= 'Due this month';
$lang['dashboards_profit']	= 'Profit';
$lang['dashboards_order']	= 'Orders';
$lang['dashboards_statistics']	= 'Statistics';
$lang['dashboards_sellings']	= 'Sellings';
$lang['dashboards_items']	= 'Items';
$lang['dashboards_analysis']	= 'Analysis';
$lang['dashboards_customer']	= 'Customers';
$lang['dashboards_verticalbar']	= 'Stock Data';
$lang['dashboards_task_list']	= 'Tasks List';
$lang['dashboards_Notification']	= 'Notification';
