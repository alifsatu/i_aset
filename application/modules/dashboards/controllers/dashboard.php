<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dashboard controller
 */
class dashboard extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Dashboards.Dashboard.View');
        $this->load->model('dashboards_model', null, true);
        //$this->load->model('payments/payments_model', null, true);
        //$this->load->model('invoice/invoice_model',null,true);
        Assets::add_css('js/datepicker/datepicker.css');
        Assets::add_js("js/datepicker/bootstrap-datepicker.js");


        Assets::add_css(array(Template::theme_url('js/datatables/datatables.min.css')));
        Assets::add_css(array(Template::theme_url('js/datatables/Buttons-1.5.4/css/buttons.dataTables.min.css')));


        Assets::add_js(array(Template::theme_url('js/datatables/datatables.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.flash.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/JSZip-2.5.0/jszip.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/pdfmake.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/vfs_fonts.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.html5.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.print.min.js')));



        $this->lang->load('dashboards');

        Template::set_block('sub_nav', 'dashboard/_sub_nav');

        Assets::add_module_js('dashboards', 'dashboards.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index1() {
        $this->index();
    }

    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $mth = date('m');
        $yr = date('Y');
        $selectedMonth = $mth . '-' . $yr;
        $MonthName = date('F', mktime(0, 0, 0, $mth, 10)) . '-' . $yr;

        if (isset($_POST['selectedMonth'])) {
            $arr = explode('-', $_POST['selectedMonth']);
            $mth = $arr[0];
            $yr = $arr[1];

            $MonthName = date('F', mktime(0, 0, 0, $mth, 10)) . ' ' . $yr;
            ;

            $selectedMonth = $mth . '-' . $yr;
        }

        Template::set('selectedMonth', $selectedMonth);
        Template::set('MonthName', $MonthName);

        $all_projects = $this->db->query("SELECT count(*) as num FROM  intg_projek "
                        . "where deleted=0 ")->row();

        $total_created_projects = $this->db->query("SELECT count(*) as num FROM  intg_projek "
                        . "where pegawai_ulasan = '" . $this->auth->user_id() . "'  "
                        . "AND deleted=0 ")->row();

        $total_sum_modal = $this->db->query("SELECT 
  SUM(jumlah_bayaran) AS total
FROM
  intg_bayaran 
WHERE deleted = 0 AND jenis_bayaran IN (1,2,3) ")->row();

        $total_sum_modal_by_user = $this->db->query("SELECT 
  SUM(a.jumlah_bayaran) AS total
FROM
  intg_bayaran a,
  intg_projek b
WHERE a.deleted = 0 AND a.id_projek = b.id AND a.jenis_bayaran IN (1,2,3) AND b.pegawai_ulasan = '" . $this->auth->user_id() . "'")->row();


        $jumlah_bayaran_terima_by_tahun = $this->db->query("SELECT SUM(jumlah_bayaran) AS total,YEAR(tarikh_bayaran) AS tahun FROM intg_bayaran WHERE deleted = 0 GROUP BY YEAR(tarikh_bayaran)")->result();

        $jumlah_bayaran_terima_by_tahun_user = $this->db->query("SELECT 
                                                    SUM(jumlah_bayaran) AS total,
                                                    YEAR(tarikh_bayaran) AS tahun
                                                  FROM
                                                    intg_bayaran, intg_projek
                                                  WHERE intg_bayaran.deleted = 0 
                                                  and intg_bayaran.`id_projek` = intg_projek.`id`
                                                  AND intg_projek.`status_permohonan` IN (8,9)
                                                  and intg_projek.`pegawai_ulasan` = '" . $this->auth->user_id() . "'
                                                  GROUP BY YEAR(tarikh_bayaran)")->result();

        $jumlah_permohonan_terima_by_tahun = $this->db->query("SELECT 
                                                                SUM(jumlah_bayaran) AS total,
                                                                YEAR(tarikh_terima_permohonan) AS tahun 
                                                              FROM
                                                                intg_bayaran,
                                                                intg_projek 
                                                              WHERE intg_bayaran.deleted = 0 
                                                                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                                                                AND intg_projek.`status_permohonan` IN (8,9)
                                                              GROUP BY YEAR(tarikh_terima_permohonan)")->result();

        $jumlah_permohonan_terima_by_tahun_user = $this->db->query("SELECT 
                                                                SUM(jumlah_bayaran) AS total,
                                                                YEAR(tarikh_terima_permohonan) AS tahun 
                                                              FROM
                                                                intg_bayaran,
                                                                intg_projek 
                                                              WHERE intg_bayaran.deleted = 0 
                                                                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                                                                and intg_projek.`pegawai_ulasan` = '" . $this->auth->user_id() . "'
                                                                AND intg_projek.`status_permohonan` IN (8,9)
                                                              GROUP BY YEAR(tarikh_terima_permohonan)")->result();

        $jumlah_projek_by_status_permohonan = $this->db->query("  SELECT COUNT(*) AS total,b.`nama` FROM intg_projek a
                                                    JOIN intg_status_permohonan b ON b.`id` = a.`status_permohonan`
                                                    WHERE a.`status_permohonan` IS NOT NULL
                                                    GROUP BY a.`status_permohonan`")->result();

        $jumlah_projek_by_status_terkini = $this->db->query("  SELECT COUNT(*) AS total,b.`nama` FROM intg_projek a
                                                    JOIN intg_status_terkini_projek b ON b.`id` = a.`status_terkini_projek`
                                                    WHERE a.`status_terkini_projek` IS NOT NULL
                                                    GROUP BY a.`status_terkini_projek`")->result();

        $JKBSM = $this->db->query("SELECT 
                    SUM(jumlah_bayaran) AS total,
                    YEAR(tarikh_bayaran) AS tahun,
                    MONTH(tarikh_bayaran) AS bulan,
                    jenis_bayaran AS jenis_bayaran 
                  FROM
                    intg_bayaran
                  WHERE deleted = 0 
                  GROUP BY MONTH(tarikh_bayaran) ")->result();


        Template::set('total_created_projects', $total_created_projects);
        Template::set('all_projects', $all_projects);
        Template::set('total_sum_modal', $total_sum_modal);
        Template::set('total_sum_modal_by_user', $total_sum_modal_by_user);
        Template::set('jumlah_bayaran_terima_by_tahun', $jumlah_bayaran_terima_by_tahun);
        Template::set('jumlah_bayaran_terima_by_tahun_user', $jumlah_bayaran_terima_by_tahun_user);
        Template::set('jumlah_projek_by_status_permohonan', $jumlah_projek_by_status_permohonan);
        Template::set('jumlah_projek_by_status_terkini', $jumlah_projek_by_status_terkini);
        Template::set('jumlah_permohonan_terima_by_tahun', $jumlah_permohonan_terima_by_tahun);
        Template::set('jumlah_permohonan_terima_by_tahun_user', $jumlah_permohonan_terima_by_tahun_user);
        Template::set('JKBSM', $JKBSM);
        Template::set('list_daerah', $this->db->query('SELECT id,nama FROM intg_daftar_daerah')->result());
        Template::set('list_statuspermohonan', $this->db->query('SELECT id,nama FROM intg_status_permohonan')->result());
        Template::set('list_jenis_bayaran', $this->db->query('SELECT id,nama FROM intg_jenis_bayaran ')->result());
        Template::set('list_tahun', $this->db->query('SELECT DISTINCT(YEAR(tarikh_terima_permohonan)) AS tahun FROM intg_projek ORDER BY tarikh_terima_permohonan ')->result());
        Template::set('list_tahun_bayaran', $this->db->query('SELECT DISTINCT(YEAR(tarikh_bayaran)) AS tahun FROM intg_bayaran ORDER BY tarikh_bayaran ')->result());

        Template::set('getPermohonanVSpegawai', $this->getPermohonanVSpegawai());
        Template::set('getPermohonanVSdaerah', $this->getPermohonanVSdaerah());

        Assets::add_module_css('dashboards', 'bootstrap_calendar.css');
        Assets::add_module_js('dashboards', 'dashboards.js');
        Assets::add_js(Template::theme_url('js/HC/highcharts.js'));
        Assets::add_js(Template::theme_url('js/HC/modules/drilldown.js'));
        Assets::add_js(Template::theme_url('js/HC/modules/exporting.js'));
        Assets::add_module_js('dashboards', 'calendar/bootstrap_calendar.js');
        Template::set('toolbar_title', 'Manage Dashboards');
        Template::render();
    }

    public function loadchart() {
        if (isset($_POST['projdaterange'])) {

            //print_r($_POST); exit;
            $range = "";
            if ($this->input->post('datefrom') != "" && $this->input->post('dateto') != "") {
                $to = date("Y-m-d", strtotime($this->input->post('dateto')));
                $from = date("Y-m-d", strtotime($this->input->post('datefrom')));
                //echo "dateto:".$this->input->post('dateto')."<br>";
                //echo $to."<br>";
                //echo $to; exit;

                $range = "AND t.tsd_date >=  ('" . $from . "') AND t.tsd_date <= ('" . $to . "')";
            }
            $sql = "SELECT p.id as projectid,
                p.project_name as name,
                sum(t.tsd_hours) as y 
                FROM 
                intg_timesheet_details t,
                intg_timesheet it,
                intg_projects p 
                WHERE 
                it.`final_status` = 'Yes' 
                AND p.final_status = 'Yes'
                AND p.status = 'Initiated'
                AND it.`id` = t.`tid` 
                AND p.id = t.pid 
                AND t.tsd_hours > 0 ";

            if ($this->input->post('projects') != '%%') {
                $sql .= " AND p.id LIKE ('" . $this->input->post('projects') . "') ";
            }

            $sql .= "group by t.pid "
                    . "order by p.id,t.id desc ";

            $timespent = $this->db->query($sql)->result();

            foreach ($timespent as $tm) {

                $task = $this->db->query("select  
                    *,sum(tsd_hours) as hours  
                    FROM intg_timesheet_details itd,
                    intg_task_pool ip ,
                    intg_timesheet it 
                    WHERE 
                    itd.ptskid = ip.id 
                    AND itd.pid = '" . $tm->projectid . "' 
                    AND itd.`tid` = it.`id`
                    AND it.`final_status` = 'yes' group by ptskid")->result();

                foreach ($task as $t) {

                    $projectname[] = $tm->name;
                    $taskdrilldown1[] = $t->task_name;
                    $taskdrilldown2[] = intval($t->hours);
                    $projref[] = $tm->name;
                }
            }
            //print_r($taskdrilldown1);//task name
            //print_r($taskdrilldown2);//task hours

            if ($timespent == array()) {
                echo "<p style='padding:20%'>No Data Available</p>";
                exit;
            }

            Template::set('tspent', $timespent);
            Template::set('taskdrilldown1', $taskdrilldown1); //task name
            Template::set('taskdrilldown2', $taskdrilldown2); //task hours
            Template::set('projref', $projref);
            Template::set('projectname', $projectname);
        }


        if (isset($_POST['useronly'])) {


            $timespent = $this->db->query("SELECT *,p.project_name as name,sum(t.tsd_hours) as y FROM intg_timesheet_details t,intg_timesheet it,intg_projects p where it.final_status = 'Yes' AND it.id=t.tid and p.id = t.pid and it.uid = '" . $this->input->post('users') . "' AND t.tsd_hours > 0  group by t.pid order by t.id desc limit 0,5")->result();

            // echo $this->db->last_query(); exit;
            // print_r($timespent); exit;

            if ($timespent == array()) {
                echo "<p style='padding:20%'>No Data Available</p>";
                exit;
            }

            Template::set('tspent', $timespent);
        }


        if (isset($_POST['projectleader'])) {

            // print_r($_POST); exit;
            $timespent = $this->db->query("select distinct p.id as projectid,project_name from intg_projects p, intg_milestones m where p.id=m.project_id and p.initiator = " . $this->auth->user_id() . " ")->result();

            foreach ($timespent as $tm) {

                $timesheet = $this->db->query("select  sum(tsd_hours) as hours,display_name,mid,uid from intg_timesheet t,intg_timesheet_details ts,intg_users iu, intg_projects ip where ip.id = ts.pid AND t.final_status = 'Yes' AND t.uid = '" . $this->auth->user_id() . "' AND t.uid = iu.id and t.id = ts.tid and ts.pid = '" . $tm->projectid . "' AND ts.tsd_hours > 0  AND ip.initiator = '" . $this->auth->user_id() . "' group by uid ")->result();

                foreach ($timesheet as $ts) {

                    $cost = $this->db->query("select sum(imu.daily_fte) as total from intg_milestones_users imu,intg_timesheet_details itd,intg_timesheet it where it.id = itd.tid and itd.mid = imu.milestones_id and itd.mid = '" . $ts->mid . "' and it.uid = '" . $ts->uid . "'")->row();

                    $projleader[] = "{ name :\"" . $tm->project_name . "\",y:" . intval($ts->hours) . ",uidname:'" . $ts->display_name . "',uidcost:'RM" . number_format($cost->total * intval($ts->hours) / 8.5) . "'},";
                }
            }


            if ($timespent == array()) {
                echo "<p style='padding:20%'>No Data Available</p>";
                exit;
            }

            Template::set('projleader', $projleader);
        }


        Template::render();
    }

    public function getPermohonanVSdaerah($tahun = '', $bulan = '', $ajax = '') {

        $sql = "SELECT b.nama AS name,CAST(COUNT(*) AS UNSIGNED) AS y  FROM intg_projek a, intg_daftar_daerah b "
                . "WHERE a.daerah = b.id ";

        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(a.tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(a.tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " AND a.`deleted` = '0' GROUP BY b.id ORDER BY b.id ";
//        echo $sql;
        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function getPermohonanVSpegawai($tahun = '', $bulan = '', $ajax = '') {



        $sql = "SELECT b.`display_name` AS name,CAST(COUNT(*) AS UNSIGNED) AS y FROM intg_projek a, intg_users b "
                . "WHERE a.`pegawai_ulasan` = b.`id`";

        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(a.tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(a.tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " GROUP BY b.id ORDER BY b.id ";

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function loadChartData_KSMT0($tahun = '', $bulan = '', $ajax = '') {
        $sql = "SELECT 
                SUM(jumlah_bayaran) AS y,
                YEAR(tarikh_terima_permohonan) AS name 
              FROM
                intg_bayaran,
                intg_projek 
              WHERE intg_bayaran.deleted = 0 
                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                AND intg_projek.`status_permohonan` IN (8,9) ";

        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " GROUP BY YEAR(tarikh_terima_permohonan)";

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function loadChartData_KSMT1($tahun = '', $bulan = '', $ajax = '') {


        $sql = "SELECT 
                SUM(jumlah_bayaran) AS y,
                YEAR(tarikh_terima_permohonan) AS name 
              FROM
                intg_bayaran,
                intg_projek 
              WHERE intg_bayaran.deleted = 0 
                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                AND intg_projek.`status_permohonan` IN (8,9) and intg_projek.`pegawai_ulasan` = '" . $this->auth->user_id() . "' ";

        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " GROUP BY YEAR(tarikh_terima_permohonan)";

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function loadChartData_KSMT3($tahun = '', $bulan = '', $ajax = '') {


        $sql = "SELECT 
                SUM(jumlah_bayaran) AS y,
                YEAR(tarikh_terima_permohonan) AS name 
              FROM
                intg_bayaran,
                intg_projek 
              WHERE intg_bayaran.deleted = 0 
                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                AND intg_projek.`status_permohonan` IN (8,9) ";

        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " GROUP BY YEAR(tarikh_terima_permohonan)";

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function loadChartData_KSMT4($tahun = '', $bulan = '', $ajax = '') {





        $sql = "SELECT 
                SUM(jumlah_bayaran) AS y,
                YEAR(tarikh_terima_permohonan) AS name 
              FROM
                intg_bayaran,
                intg_projek 
              WHERE intg_bayaran.deleted = 0 
                AND intg_bayaran.`id_projek` = intg_projek.`id` 
                AND intg_projek.`status_permohonan` IN (8,9) and intg_projek.`pegawai_ulasan` = '" . $this->auth->user_id() . "' ";


        if ($tahun == 'All' || $tahun == '') {
            
        } else {
            $sql .= "  AND YEAR(tarikh_terima_permohonan) = '" . $tahun . "' ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(tarikh_terima_permohonan) = '" . $bulan . "' ";
        }


        $sql .= " GROUP BY YEAR(tarikh_terima_permohonan)";

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function load_summary_charts() {

        if (isset($_POST['tahun_pilihan']) and $_POST['tahun_pilihan'] != '') {

            Template::set('PermohonanVSdaerah', $this->getPermohonanVSdaerah($_POST['tahun_pilihan']));
        } else {
            Template::set('PermohonanVSdaerah', $this->getPermohonanVSdaerah());
        }

        if (isset($_POST['tahun_pilihan2']) and $_POST['tahun_pilihan2'] != '') {
            Template::set('PermohonanVSpegawai', $this->getPermohonanVSpegawai($_POST['tahun_pilihan2']));
        } else {
            Template::set('PermohonanVSpegawai', $this->getPermohonanVSpegawai());
        }
        Template::render();
    }

    public function loadChartData_SummaryBSM() {

        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $daerah = $this->input->post('daerah');
        $statusPermohonan = $this->input->post('statusPermohonan');
        $jenisBayaran = $this->input->post('jenisBayaran');
        $ajax = $this->input->post('ajax');

        if (is_array($tahun)) {
            // explodable
            $tahun = implode(',', $tahun);
        }
        if (is_array($bulan)) {
            // explodable
            $bulan = implode(',', $bulan);
        }
        if (is_array($daerah)) {
            // explodable
            $daerah = implode(',', $daerah);
        }
        if (is_array($statusPermohonan)) {
            // explodable
            $statusPermohonan = implode(',', $statusPermohonan);
        }
        if (is_array($jenisBayaran)) {
            // explodable
            $jenisBayaran = implode(',', $jenisBayaran);
        }



        $sql = "SELECT 
                    SUM(a.jumlah_bayaran) AS total,YEAR(a.tarikh_bayaran) AS tahun
                  FROM
                    intg_bayaran a,
                    intg_projek b
                  WHERE 
                    a.`id_projek` = b.`id`
                    ";

        if ($tahun == 'All' || $tahun == '') {
//            $sql .= " AND YEAR(a.tarikh_bayaran) IN ('" . DATE('Y') . "') ";
        } else {
            $sql .= "  AND YEAR(a.tarikh_bayaran) IN (" . $tahun . " ) ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(a.tarikh_bayaran) IN (" . $bulan . ") ";
        }

        if ($daerah == 'All' || $daerah == '') {
            
        } else {
            $sql .= "  AND b.daerah IN (" . $daerah . ") ";
        }

        if ($statusPermohonan == 'All' || $statusPermohonan == '') {
            
        } else {
            $sql .= "  AND b.status_permohonan IN (" . $statusPermohonan . ") ";
        }

        if ($jenisBayaran == 'All' || $jenisBayaran == '') {
            
        } else {
            $sql .= "  AND a.jenis_bayaran IN (" . $jenisBayaran . ") ";
        }


        $sql .= " AND a.deleted = 0 
                  GROUP BY YEAR(a.tarikh_bayaran) ";
//        echo $sql;

        $recs = $this->db->query($sql)->result();

//        print_r_pre($recs);

        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

    public function getTableJKBSM() {


        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $daerah = $this->input->post('daerah');
        $statusPermohonan = $this->input->post('statusPermohonan');
        $jenisBayaran = $this->input->post('jenisBayaran');
        $ajax = $this->input->post('ajax');

        if (is_array($tahun)) {
            // explodable
            $tahun = implode(',', $tahun);
        }
        if (is_array($bulan)) {
            // explodable
            $bulan = implode(',', $bulan);
        }
        if (is_array($daerah)) {
            // explodable
            $daerah = implode(',', $daerah);
        }
        if (is_array($statusPermohonan)) {
            // explodable
            $statusPermohonan = implode(',', $statusPermohonan);
        }
        if (is_array($jenisBayaran)) {
            // explodable
            $jenisBayaran = implode(',', $jenisBayaran);
        }

        $sql = "SELECT 
                    SUM(a.jumlah_bayaran) AS total,
                    YEAR(a.tarikh_bayaran) AS tahun,
                    MONTH(a.tarikh_bayaran) AS bulan,
                    a.jenis_bayaran AS jenis_bayaran FROM
                    intg_bayaran a,
                    intg_projek b
                  WHERE 
                    a.`id_projek` = b.`id`  ";




        if ($tahun == 'All' || $tahun == '') {
//            $sql .= " AND YEAR(a.tarikh_bayaran) IN ('" . DATE('Y') . "') ";
        } else {
            $sql .= "  AND YEAR(a.tarikh_bayaran) IN (" . $tahun . " ) ";
        }

        if ($bulan == 'All' || $bulan == '') {
            
        } else {
            $sql .= "  AND MONTH(a.tarikh_bayaran) IN (" . $bulan . ") ";
        }

        if ($daerah == 'All' || $daerah == '') {
            
        } else {
            $sql .= "  AND b.daerah IN (" . $daerah . ") ";
        }

        if ($statusPermohonan == 'All' || $statusPermohonan == '') {
            
        } else {
            $sql .= "  AND b.status_permohonan IN (" . $statusPermohonan . ") ";
        }

        if ($jenisBayaran == 'All' || $jenisBayaran == '') {
            
        } else {
            $sql .= "  AND a.jenis_bayaran IN (" . $jenisBayaran . ") ";
        }




        $sql .= " AND a.deleted = 0  GROUP BY MONTH(tarikh_bayaran),jenis_bayaran ";
//       echo $sql;

        $recs = $this->db->query($sql)->result();
        if ($ajax != '') {
            echo json_encode($recs);
            die;
        }

        return $recs;
    }

}
