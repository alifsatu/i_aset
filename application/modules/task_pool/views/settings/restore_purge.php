<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($task_pool))
{
	$task_pool = (array) $task_pool;
}
$id = isset($task_pool['id']) ? $task_pool['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Task Pool</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('task_name') ? 'error' : ''; ?>">
				<?php echo form_label('Task Name'. lang('bf_form_label_required'), 'task_pool_task_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='task_pool_task_name' class='input-sm input-s  form-control' type='text' name='task_pool_task_name' maxlength="255" value="<?php echo set_value('task_pool_task_name', isset($task_pool['task_name']) ? $task_pool['task_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('task_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'task_pool_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'task_pool_description', 'id' => 'task_pool_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('task_pool_description', isset($task_pool['description']) ? $task_pool['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created By', 'task_pool_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='task_pool_created_by' class='input-sm input-s  form-control' type='text' name='task_pool_created_by' maxlength="255" value="<?php echo set_value('task_pool_created_by', isset($task_pool['created_by']) ? $task_pool['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name', 'task_pool_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='task_pool_company_id' class='input-sm input-s  form-control' type='text' name='task_pool_company_id' maxlength="255" value="<?php echo set_value('task_pool_company_id', isset($task_pool['company_id']) ? $task_pool['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/task_pool', lang('task_pool_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Task_Pool.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('task_pool_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>