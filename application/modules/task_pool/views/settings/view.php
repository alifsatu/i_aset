<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($task_pool))
{
	$task_pool = (array) $task_pool;
}
$id = isset($task_pool['id']) ? $task_pool['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Task Pool</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('task_name') ? 'error' : ''; ?>">
				<?php echo form_label('Task Name', 'task_pool_task_name', array('class' => 'control-label') ); ?> : <? echo $task_pool['task_name']; ?>
				<div class='controls'>
					<!--<input id='task_pool_task_name' class='input-sm input-s  form-control' type='text' name='task_pool_task_name' maxlength="255" value="<?php echo set_value('task_pool_task_name', isset($task_pool['task_name']) ? $task_pool['task_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('task_name'); ?></span>-->
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'task_pool_description', array('class' => 'control-label') ); ?>  : <? echo $task_pool['description']; ?>
				<div class='controls'>
					<?php //echo form_textarea( array( 'name' => 'task_pool_description', 'id' => 'task_pool_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('task_pool_description', isset($task_pool['description']) ? $task_pool['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

		
		</fieldset>
    <?php echo form_close(); ?>
</div>