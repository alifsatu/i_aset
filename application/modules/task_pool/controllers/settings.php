<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Task_Pool.Settings.View');
		$this->load->model('task_pool_model', null, true);
		$this->lang->load('task_pool');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('task_pool', 'task_pool.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->task_pool_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('task_pool_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('task_pool_delete_failure') . $this->task_pool_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('task_poolfield',$this->input->post('select_field'));
$this->session->set_userdata('task_poolfvalue',$this->input->post('field_value'));
$this->session->set_userdata('task_poolfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('task_poolfield');
$this->session->unset_userdata('task_poolfvalue');
$this->session->unset_userdata('task_poolfname');
break;
}

if ( $this->session->userdata('task_poolfield')!='') { $field=$this->session->userdata('task_poolfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('task_poolfield') =='All Fields') 
{ 
 		
  $total = $this->task_pool_model
   ->where('deleted','0')
  ->likes('task_name',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('description',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('created_by',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('company_id',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('status_nc',$this->session->userdata('task_poolfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->task_pool_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('task_poolfvalue'),'both')  
  ->count_all();
}

		//$records = $this->task_pool_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('task_poolfield') =='All Fields') 
{
$records = $this->task_pool_model
 
    ->where('deleted','0')
  ->likes('task_name',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('description',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('created_by',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('company_id',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('status_nc',$this->session->userdata('task_poolfvalue'),'both') 
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->task_pool_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('task_poolfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->task_pool_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('task_pool', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Task Pool');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_task_pool SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('task_pool_success'), 'success');
				}
				else
				{
					Template::set_message(lang('task_pool_restored_error'). $this->task_pool_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_task_pool where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('task_pool_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('task_poolfield',$this->input->post('select_field'));
$this->session->set_userdata('task_poolfvalue',$this->input->post('field_value'));
$this->session->set_userdata('task_poolfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('task_poolfield');
$this->session->unset_userdata('task_poolfvalue');
$this->session->unset_userdata('task_poolfname');
break;
}


if ( $this->session->userdata('task_poolfield')!='') { $field=$this->session->userdata('task_poolfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('task_poolfield') =='All Fields') 
{ 
 		
  $total = $this->task_pool_model
  ->where('deleted','1')
  ->likes('task_name',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('description',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('created_by',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('company_id',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('status_nc',$this->session->userdata('task_poolfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->task_pool_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('task_poolfvalue'),'both')  
  ->count_all();
}

//$records = $this-task_pool_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('task_poolfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
  ->likes('task_name',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('description',$this->session->userdata('task_poolfvalue'),'both') 
  ->likes('created_by',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('company_id',$this->session->userdata('task_poolfvalue'),'both')
  ->likes('status_nc',$this->session->userdata('task_poolfvalue'),'both') 
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->task_pool_model
->where('deleted','1')
->likes($field,$this->session->userdata('task_poolfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->task_pool_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('task_pool', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Task Pool');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Task Pool object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Task_Pool.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_task_pool())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('task_pool_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'task_pool');

				Template::set_message(lang('task_pool_create_success'), 'success');
				redirect(SITE_AREA .'/settings/task_pool');
			}
			else
			{
				Template::set_message(lang('task_pool_create_failure') . $this->task_pool_model->error, 'error');
			}
		}
		Assets::add_module_js('task_pool', 'task_pool.js');

		Template::set('toolbar_title', lang('task_pool_create') . ' Task Pool');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Task Pool data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('task_pool_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/task_pool');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Task_Pool.Settings.Edit');

			if ($this->save_task_pool('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('task_pool_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'task_pool');

				Template::set_message(lang('task_pool_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('task_pool_edit_failure') . $this->task_pool_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Task_Pool.Settings.Delete');

			if ($this->task_pool_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('task_pool_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'task_pool');

				Template::set_message(lang('task_pool_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/task_pool');
			}
			else
			{
				Template::set_message(lang('task_pool_delete_failure') . $this->task_pool_model->error, 'error');
			}
		}
		Template::set('task_pool', $this->task_pool_model->find($id));
		Template::set('toolbar_title', lang('task_pool_edit') .' Task Pool');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_task_pool SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('task_pool_success'), 'success');
											redirect(SITE_AREA .'/settings/task_pool/deleted');
				}
				else
				{
					
					Template::set_message(lang('task_pool_error') . $this->task_pool_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_task_pool where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('task_pool_purged'), 'success');
					redirect(SITE_AREA .'/settings/task_pool/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('task_pool_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/task_pool');
		}

		
		
		
		Template::set('task_pool', $this->task_pool_model->find($id));
		
		Assets::add_module_js('task_pool', 'task_pool.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Task_pool');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_task_pool($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['task_name']        = $this->input->post('task_pool_task_name');
		$data['description']        = $this->input->post('task_pool_description');
		$data['created_by']        = $this->auth->user_id();
		$data['company_id']        = $this->auth->company_id();

		if ($type == 'insert')
		{
			$id = $this->task_pool_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->task_pool_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}