<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['task_pool_manage']			= 'Manage';
$lang['task_pool_edit']				= 'Edit';
$lang['task_pool_true']				= 'True';
$lang['task_pool_false']				= 'False';
$lang['task_pool_create']			= 'Save';
$lang['task_pool_list']				= 'List';
$lang['task_pool_new']				= 'New';
$lang['task_pool_edit_text']			= 'Edit this to suit your needs';
$lang['task_pool_no_records']		= 'There aren\'t any task_pool in the system.';
$lang['task_pool_create_new']		= 'Create a new Task Pool.';
$lang['task_pool_create_success']	= 'Task Pool successfully created.';
$lang['task_pool_create_failure']	= 'There was a problem creating the task_pool: ';
$lang['task_pool_create_new_button']	= 'Create New Task Pool';
$lang['task_pool_invalid_id']		= 'Invalid Task Pool ID.';
$lang['task_pool_edit_success']		= 'Task Pool successfully saved.';
$lang['task_pool_edit_failure']		= 'There was a problem saving the task_pool: ';
$lang['task_pool_delete_success']	= 'record(s) successfully deleted.';

$lang['task_pool_purged']	= 'record(s) successfully purged.';
$lang['task_pool_success']	= 'record(s) successfully restored.';


$lang['task_pool_delete_failure']	= 'We could not delete the record: ';
$lang['task_pool_delete_error']		= 'You have not selected any records to delete.';
$lang['task_pool_actions']			= 'Actions';
$lang['task_pool_cancel']			= 'Cancel';
$lang['task_pool_delete_record']		= 'Delete';
$lang['task_pool_delete_confirm']	= 'Are you sure you want to delete this task_pool?';
$lang['task_pool_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['task_pool_action_edit']		= 'Save';
$lang['task_pool_action_create']		= 'Create';

// Activities
$lang['task_pool_act_create_record']	= 'Created record with ID';
$lang['task_pool_act_edit_record']	= 'Updated record with ID';
$lang['task_pool_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['task_pool_column_created']	= 'Created';
$lang['task_pool_column_deleted']	= 'Deleted';
$lang['task_pool_column_modified']	= 'Modified';
