<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($tasks))
{
	$tasks = (array) $tasks;
}
$id = isset($tasks['id']) ? $tasks['id'] : '';

?>
<div class="row">
	<h3>Tasks</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/projectmgmt/tasks', lang('tasks_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Tasks.Projectmgmt.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('tasks_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>