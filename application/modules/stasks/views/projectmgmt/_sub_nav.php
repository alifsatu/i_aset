<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks') ?>" style="border-radius:0"   id="list"><?php echo lang('tasks_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Tasks.Projectmgmt.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('tasks_new'); ?></a>
	</li>
	<?php endif; ?>
     <?php if ($this->auth->has_permission('Tasks.Projectmgmt.Deleted')) : ?>
    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>
	</li>
    <?php endif; ?>
</ul>