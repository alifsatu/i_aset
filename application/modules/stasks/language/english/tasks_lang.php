<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['tasks_manage']			= 'Manage';
$lang['tasks_edit']				= 'Edit';
$lang['tasks_true']				= 'True';
$lang['tasks_false']				= 'False';
$lang['tasks_create']			= 'Save';
$lang['tasks_list']				= 'List';
$lang['tasks_new']				= 'New';
$lang['tasks_edit_text']			= 'Edit this to suit your needs';
$lang['tasks_no_records']		= 'There aren\'t any tasks in the system.';
$lang['tasks_create_new']		= 'Create a new Tasks.';
$lang['tasks_create_success']	= 'Tasks successfully created.';
$lang['tasks_create_failure']	= 'There was a problem creating the tasks: ';
$lang['tasks_create_new_button']	= 'Create New Tasks';
$lang['tasks_invalid_id']		= 'Invalid Tasks ID.';
$lang['tasks_edit_success']		= 'Tasks successfully saved.';
$lang['tasks_edit_failure']		= 'There was a problem saving the tasks: ';
$lang['tasks_delete_success']	= 'record(s) successfully deleted.';

$lang['tasks_purged']	= 'record(s) successfully purged.';
$lang['tasks_success']	= 'record(s) successfully restored.';


$lang['tasks_delete_failure']	= 'We could not delete the record: ';
$lang['tasks_delete_error']		= 'You have not selected any records to delete.';
$lang['tasks_actions']			= 'Actions';
$lang['tasks_cancel']			= 'Cancel';
$lang['tasks_delete_record']		= 'Delete';
$lang['tasks_delete_confirm']	= 'Are you sure you want to delete this tasks?';
$lang['tasks_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['tasks_action_edit']		= 'Save';
$lang['tasks_action_create']		= 'Create';

// Activities
$lang['tasks_act_create_record']	= 'Created record with ID';
$lang['tasks_act_edit_record']	= 'Updated record with ID';
$lang['tasks_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['tasks_column_created']	= 'Created';
$lang['tasks_column_deleted']	= 'Deleted';
$lang['tasks_column_modified']	= 'Modified';
