<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_jurutera_perunding_manage']			= 'Manage';
$lang['daftar_jurutera_perunding_edit']				= 'Edit';
$lang['daftar_jurutera_perunding_true']				= 'True';
$lang['daftar_jurutera_perunding_false']				= 'False';
$lang['daftar_jurutera_perunding_create']			= 'Save';
$lang['daftar_jurutera_perunding_list']				= 'List';
$lang['daftar_jurutera_perunding_new']				= 'New';
$lang['daftar_jurutera_perunding_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_jurutera_perunding_no_records']		= 'There aren\'t any daftar_jurutera_perunding in the system.';
$lang['daftar_jurutera_perunding_create_new']		= 'Create a new Daftar Jurutera Perunding.';
$lang['daftar_jurutera_perunding_create_success']	= 'Daftar Jurutera Perunding successfully created.';
$lang['daftar_jurutera_perunding_create_failure']	= 'There was a problem creating the daftar_jurutera_perunding: ';
$lang['daftar_jurutera_perunding_create_new_button']	= 'Create New Daftar Jurutera Perunding';
$lang['daftar_jurutera_perunding_invalid_id']		= 'Invalid Daftar Jurutera Perunding ID.';
$lang['daftar_jurutera_perunding_edit_success']		= 'Daftar Jurutera Perunding successfully saved.';
$lang['daftar_jurutera_perunding_edit_failure']		= 'There was a problem saving the daftar_jurutera_perunding: ';
$lang['daftar_jurutera_perunding_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_jurutera_perunding_purged']	= 'record(s) successfully purged.';
$lang['daftar_jurutera_perunding_success']	= 'record(s) successfully restored.';


$lang['daftar_jurutera_perunding_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_jurutera_perunding_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_jurutera_perunding_actions']			= 'Actions';
$lang['daftar_jurutera_perunding_cancel']			= 'Cancel';
$lang['daftar_jurutera_perunding_delete_record']		= 'Delete';
$lang['daftar_jurutera_perunding_delete_confirm']	= 'Are you sure you want to delete this daftar_jurutera_perunding?';
$lang['daftar_jurutera_perunding_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_jurutera_perunding_action_edit']		= 'Save';
$lang['daftar_jurutera_perunding_action_create']		= 'Create';

// Activities
$lang['daftar_jurutera_perunding_act_create_record']	= 'Created record with ID';
$lang['daftar_jurutera_perunding_act_edit_record']	= 'Updated record with ID';
$lang['daftar_jurutera_perunding_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_jurutera_perunding_column_created']	= 'Created';
$lang['daftar_jurutera_perunding_column_deleted']	= 'Deleted';
$lang['daftar_jurutera_perunding_column_modified']	= 'Modified';
