<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Daftar_Jurutera_Perunding.Settings.View');
        $this->load->model('daftar_jurutera_perunding_model', null, true);
        $this->lang->load('daftar_jurutera_perunding');

        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('daftar_jurutera_perunding', 'daftar_jurutera_perunding.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->daftar_jurutera_perunding_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('daftar_jurutera_perunding_delete_success'), 'success');
                } else {
                    Template::set_message(lang('daftar_jurutera_perunding_delete_failure') . $this->daftar_jurutera_perunding_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('daftar_jurutera_perundingfield', $this->input->post('select_field'));
                $this->session->set_userdata('daftar_jurutera_perundingfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('daftar_jurutera_perundingfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('daftar_jurutera_perundingfield');
                $this->session->unset_userdata('daftar_jurutera_perundingfvalue');
                $this->session->unset_userdata('daftar_jurutera_perundingfname');
                break;
        }

        if ($this->session->userdata('daftar_jurutera_perundingfield') != '') {
            $field = $this->session->userdata('daftar_jurutera_perundingfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('daftar_jurutera_perundingfield') == 'All Fields') {

            $total = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '0')
                    ->likes('nama', $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->daftar_jurutera_perunding_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 50;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('daftar_jurutera_perundingfield') == 'All Fields') {
            $records = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '0')
                    ->likes('nama', $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->daftar_jurutera_perunding_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('daftar_jurutera_perunding', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Daftar Jurutera Perunding');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_daftar_jurutera_perunding SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('daftar_jurutera_perunding_success'), 'success');
                } else {
                    Template::set_message(lang('daftar_jurutera_perunding_restored_error') . $this->daftar_jurutera_perunding_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_daftar_jurutera_perunding where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('daftar_jurutera_perunding_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('daftar_jurutera_perundingfield', $this->input->post('select_field'));
                $this->session->set_userdata('daftar_jurutera_perundingfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('daftar_jurutera_perundingfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('daftar_jurutera_perundingfield');
                $this->session->unset_userdata('daftar_jurutera_perundingfvalue');
                $this->session->unset_userdata('daftar_jurutera_perundingfname');
                break;
        }


        if ($this->session->userdata('daftar_jurutera_perundingfield') != '') {
            $field = $this->session->userdata('daftar_jurutera_perundingfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('daftar_jurutera_perundingfield') == 'All Fields') {

            $total = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '1')
                    ->likes('nama', $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-daftar_jurutera_perunding_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('daftar_jurutera_perundingfield') == 'All Fields') {
            $records = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '1')
                    ->likes('nama', $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->daftar_jurutera_perunding_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('daftar_jurutera_perundingfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->daftar_jurutera_perunding_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('daftar_jurutera_perunding', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Daftar Jurutera Perunding');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Daftar Jurutera Perunding object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Daftar_Jurutera_Perunding.Settings.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_daftar_jurutera_perunding()) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_jurutera_perunding_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'daftar_jurutera_perunding', $_POST, $insert_id);

                Template::set_message(lang('daftar_jurutera_perunding_create_success'), 'success');
                redirect(SITE_AREA . '/settings/daftar_jurutera_perunding');
            } else {
                Template::set_message(lang('daftar_jurutera_perunding_create_failure') . $this->daftar_jurutera_perunding_model->error, 'error');
            }
        }
        Assets::add_module_js('daftar_jurutera_perunding', 'daftar_jurutera_perunding.js');

        Template::set('toolbar_title', lang('daftar_jurutera_perunding_create') . ' Daftar Jurutera Perunding');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Daftar Jurutera Perunding data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('daftar_jurutera_perunding_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/daftar_jurutera_perunding');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Daftar_Jurutera_Perunding.Settings.Edit');

            if ($this->save_daftar_jurutera_perunding('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_jurutera_perunding_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_jurutera_perunding', $_POST, $id);

                Template::set_message(lang('daftar_jurutera_perunding_edit_success'), 'success');
            } else {
                Template::set_message(lang('daftar_jurutera_perunding_edit_failure') . $this->daftar_jurutera_perunding_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Daftar_Jurutera_Perunding.Settings.Delete');

            if ($this->daftar_jurutera_perunding_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_jurutera_perunding_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_jurutera_perunding', $_POST, $id);

                Template::set_message(lang('daftar_jurutera_perunding_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/daftar_jurutera_perunding');
            } else {
                Template::set_message(lang('daftar_jurutera_perunding_delete_failure') . $this->daftar_jurutera_perunding_model->error, 'error');
            }
        }
        Template::set('daftar_jurutera_perunding', $this->daftar_jurutera_perunding_model->find($id));
        Template::set('toolbar_title', lang('daftar_jurutera_perunding_edit') . ' Daftar Jurutera Perunding');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_daftar_jurutera_perunding SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                log_activity($this->current_user->id, lang('daftar_jurutera_perunding_success') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_jurutera_perunding', $_POST, $id);
                Template::set_message(lang('daftar_jurutera_perunding_success'), 'success');
                redirect(SITE_AREA . '/settings/daftar_jurutera_perunding/deleted');
            } else {

                Template::set_message(lang('daftar_jurutera_perunding_error') . $this->daftar_jurutera_perunding_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_daftar_jurutera_perunding where id = ' . $id . '');

            if ($result) {
                log_activity($this->current_user->id, lang('daftar_jurutera_perunding_purged') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_jurutera_perunding', $_POST, $id);
                Template::set_message(lang('daftar_jurutera_perunding_purged'), 'success');
                redirect(SITE_AREA . '/settings/daftar_jurutera_perunding/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('daftar_jurutera_perunding_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/daftar_jurutera_perunding');
        }




        Template::set('daftar_jurutera_perunding', $this->daftar_jurutera_perunding_model->find($id));

        Assets::add_module_js('daftar_jurutera_perunding', 'daftar_jurutera_perunding.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Daftar_jurutera_perunding');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_daftar_jurutera_perunding($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['nama'] = $this->input->post('daftar_jurutera_perunding_nama');
        $data['status'] = $this->input->post('daftar_jurutera_perunding_status');

        if ($type == 'insert') {
            $id = $this->daftar_jurutera_perunding_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->daftar_jurutera_perunding_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
