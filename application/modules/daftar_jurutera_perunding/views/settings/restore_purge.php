<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_jurutera_perunding))
{
	$daftar_jurutera_perunding = (array) $daftar_jurutera_perunding;
}
$id = isset($daftar_jurutera_perunding['id']) ? $daftar_jurutera_perunding['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Jurutera Perunding</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama'. lang('bf_form_label_required'), 'daftar_jurutera_perunding_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_jurutera_perunding_nama' class='input-sm input-s  form-control' type='text' name='daftar_jurutera_perunding_nama' maxlength="255" value="<?php echo set_value('daftar_jurutera_perunding_nama', isset($daftar_jurutera_perunding['nama']) ? $daftar_jurutera_perunding['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'daftar_jurutera_perunding_status_label') ); ?>
				<div class='controls' aria-labelled-by='daftar_jurutera_perunding_status_label'>
					<label class='radio' for='daftar_jurutera_perunding_status_option1'>
						<input id='daftar_jurutera_perunding_status_option1' name='daftar_jurutera_perunding_status' type='radio'  value='option1' <?php echo set_radio('daftar_jurutera_perunding_status', 'option1', TRUE); ?> />
						Radio option 1
					</label>
					<label class='radio' for='daftar_jurutera_perunding_status_option2'>
						<input id='daftar_jurutera_perunding_status_option2' name='daftar_jurutera_perunding_status' type='radio' value='option2' <?php echo set_radio('daftar_jurutera_perunding_status', 'option2'); ?> />
						Radio option 2
					</label>
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/daftar_jurutera_perunding', lang('daftar_jurutera_perunding_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Daftar_Jurutera_Perunding.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('daftar_jurutera_perunding_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>