<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_jurutera_perunding))
{
	$daftar_jurutera_perunding = (array) $daftar_jurutera_perunding;
}
$id = isset($daftar_jurutera_perunding['id']) ? $daftar_jurutera_perunding['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Jurutera Perunding</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama'. lang('bf_form_label_required'), 'daftar_jurutera_perunding_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_jurutera_perunding_nama' class='input-sm input-s  form-control' type='text' name='daftar_jurutera_perunding_nama' maxlength="255" value="<?php echo set_value('daftar_jurutera_perunding_nama', isset($daftar_jurutera_perunding['nama']) ? $daftar_jurutera_perunding['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'daftar_jurutera_perunding_status_label') ); ?>
				<div class='controls' aria-labelled-by='daftar_jurutera_perunding_status_label'>
					<label class='radio' for='daftar_jurutera_perunding_status_option1'>
						<input id='daftar_jurutera_perunding_status_option1' name='daftar_jurutera_perunding_status' type='radio'  value='1' <?php echo set_radio('daftar_jurutera_perunding_status', '1', isset($daftar_jurutera_perunding['status']) && $daftar_jurutera_perunding['status'] == 1  ? TRUE : FALSE ); ?> />
						Aktif
					</label>
					<label class='radio' for='daftar_jurutera_perunding_status_option2'>
                                            <input id='daftar_jurutera_perunding_status_option2' name='daftar_jurutera_perunding_status' type='radio' value='2' <?php echo set_radio('daftar_jurutera_perunding_status', '2', isset($daftar_jurutera_perunding['status']) && $daftar_jurutera_perunding['status'] == 2 ? TRUE : FALSE ); ?> />
						Tidak Aktif
					</label>
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_jurutera_perunding_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/daftar_jurutera_perunding', lang('daftar_jurutera_perunding_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>