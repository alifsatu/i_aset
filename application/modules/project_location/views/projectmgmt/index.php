<?php

$num_columns	= 5;
$can_delete	= $this->auth->has_permission('Project_Location.projectmgmt.Delete');
$can_edit		= $this->auth->has_permission('Project_Location.projectmgmt.Edit');
$has_records	= isset($records) && is_array($records) && count($records);
?>

<div class="row">
<section class="panel panel-default">
  <div class="panel-body">
    <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
    <div class="row">
      <div class="col-md-6">        <h4>Project Location</h4>      </div>
        <div class="col-md-3">
      <select name="select_field" id="select_field" onchange="setfname()" class="form-control m-b">
        <option value="<?=$this->session->userdata('capfield')?>">
        <?=$this->session->userdata('capfname')?>
        </option>
        <option>All Fields</option>
        <option value="personal_particulars_name">Name</option>
        <option value="personal_particulars_nric">NRIC</option>
        <option value="personal_particulars_htel">Handphone</option>
        <option value="personal_particulars_email">Email</option>
        <option value="personal_particulars_citizenship">Citizenship</option>
      </select>
     </div><div class="col-md-3"><div class="input-group">
      <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">

<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>

                          </span>
                        </div></div>
      </div>
   <?php echo form_close(); ?>    

     <div class="table-responsive">
	<?php echo form_open($this->uri->uri_string()); ?>
		 
                 <table class="table table-striped datagrid m-b-sm">
          <tr>
            <?php if ($can_delete && $has_records) : ?>
            <th class="column-check"><input class="check-all" type="checkbox" /></th>
            <?php endif;?>
            <th>#</th>
            <th<?php if ($this->input->get('sort_by') == '_project_location') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>> <a href='<?php echo base_url() .'index.php/admin/projectmgmt/project_location?sort_by=project_location&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_location') ? 'desc' : 'asc'); ?>'> Project Location</a></th>
            <th><?php echo lang("project_location_column_deleted"); ?></th>
            <th><?php echo lang("project_location_column_created"); ?></th>
            <th><?php echo lang("project_location_column_modified"); ?></th>
          </tr>
        </thead>
        <?php if ($has_records) : ?>
        <tfoot>
          <?php if ($can_delete) : ?>
          <tr>
            <td colspan="<?php echo $num_columns; ?>"><?php echo lang('bf_with_selected'); ?>
              <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('project_location_delete_confirm'))); ?>')" /></td>
          </tr>
          <?php endif; ?>
        </tfoot>
        <?php endif; ?>
        <tbody>
          <?php

				if ($has_records) : $no = $this->input->get('per_page')+1;

					foreach ($records as $record) : 

				?>
          <tr>
            <?php if ($can_delete) : ?>
            <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>
            <td><?=$no;?></td>
            <?php endif;?>
            <?php if ($can_edit) : ?>
            <td><?php echo anchor(SITE_AREA . '/projectmgmt/project_location/edit/' . $record->id, '<span class="icon-pencil"></span>' .  $record->project_location); ?></td>
            <?php else : ?>
            <td><?php e($record->project_location); ?></td>
            <?php endif; ?>
            <td><?php echo $record->deleted > 0 ? lang('project_location_true') : lang('project_location_false')?></td>
            <td><?php e($record->created_on) ?></td>
            <td><?php e($record->modified_on) ?></td>
          </tr>
          <?php $no++;

					endforeach;

				else:

				?>
          <tr>
            <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
          </tr>
          <?php endif; ?>
        </tbody>
      </table>
     <?php echo form_close(); ?>


<footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div>
                  
                
              
                </section>