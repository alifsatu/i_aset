
<div class="row">
<section class="panel panel-default">
  <div class="panel-body">
    <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
    <div class="row">
      <div class="col-md-6">        <h4>Project Location</h4>      </div>
        <div class="col-md-3">
      <select name="select_field" id="select_field" onchange="setfname()" class="form-control m-b">
        <option value="<?=$this->session->userdata('capfield')?>">
        <?=$this->session->userdata('capfname')?>
        </option>
        <option>All Fields</option>
        <option value="personal_particulars_name">Name</option>
        <option value="personal_particulars_nric">NRIC</option>
        <option value="personal_particulars_htel">Handphone</option>
        <option value="personal_particulars_email">Email</option>
        <option value="personal_particulars_citizenship">Citizenship</option>
      </select>
     </div><div class="col-md-3"><div class="input-group">
      <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">

<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>

                          </span>
                        </div></div>
      </div>
   <?php echo form_close(); ?>    

     <div class="table-responsive">
	<?php echo form_open($this->uri->uri_string()); ?>
		 
                 <table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Project_Location.Quotes.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					<th <?php if ($this->input->get('sort_by') == 'project_location'.'_project_location') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/quotes/project_location?sort_by=project_location_project_location&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_location'.'_project_location') ? 'desc' : 'asc'); ?>'>
                    Project Location</a></th>
					<th>Deleted</th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Project_Location.Quotes.Delete')) : ?>
				<tr>
					<td colspan="5">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Project_Location.Quotes.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Project_Location.Quotes.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/quotes/project_location/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->project_location) ?></td>
				<?php else: ?>
				<td><?php echo $record->project_location ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->deleted > 0 ? lang('project_location_true') : lang('project_location_false')?></td>
				<td><?php echo $record->created_on?></td>
				<td><?php echo $record->modified_on?></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="5">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
          <?php  echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
</div>