<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($project_location))
{
	$project_location = (array) $project_location;
}
$id = isset($project_location['id']) ? $project_location['id'] : '';

?>
<div class="row">
                <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading clearfix">Project Location</header>
                    <div class="panel-body">
	<?php echo form_open($this->uri->uri_string(),  'id="forma" role="form" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('project_location') ? 'error' : ''; ?>">
				<?php echo form_label('Project Location'. lang('bf_form_label_required'), 'project_location_project_location', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='project_location_project_location' type='text' class="input-sm input-s  form-control" name='project_location_project_location' maxlength="255" value="<?php echo set_value('project_location_project_location', isset($project_location['project_location']) ? $project_location['project_location'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_location'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('project_location_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/quotes/project_location', lang('project_location_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Project_Location.Quotes.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('project_location_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('project_location_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>