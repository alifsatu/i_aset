
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/project_location') ?>" style="border-radius:0"   id="list"><?php echo lang('project_location_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Project_Location.projectmgmt.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/project_location/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('project_location_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Project_Location.projectmgmt.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/projectmgmt/project_location/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
