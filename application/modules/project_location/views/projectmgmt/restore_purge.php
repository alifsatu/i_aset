<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($project_location))
{
	$project_location = (array) $project_location;
}
$id = isset($project_location['id']) ? $project_location['id'] : '';

?>
<div class="row">
	<h3>Project Location</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('project_location') ? 'error' : ''; ?>">
				<?php echo form_label('Project Location'. lang('bf_form_label_required'), 'project_location_project_location', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='project_location_project_location' type='text' name='project_location_project_location' maxlength="255" value="<?php echo set_value('project_location_project_location', isset($project_location['project_location']) ? $project_location['project_location'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_location'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/quotes/project_location', lang('project_location_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Project_Location.Quotes.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('project_location_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>