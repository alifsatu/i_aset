<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['project_location_manage']			= 'Manage';
$lang['project_location_edit']				= 'Edit';
$lang['project_location_true']				= 'True';
$lang['project_location_false']				= 'False';
$lang['project_location_create']			= 'Save';
$lang['project_location_list']				= 'List';
$lang['project_location_new']				= 'New';
$lang['project_location_edit_text']			= 'Edit this to suit your needs';
$lang['project_location_no_records']		= 'There aren\'t any project_location in the system.';
$lang['project_location_create_new']		= 'Create a new Project Location.';
$lang['project_location_create_success']	= 'Project Location successfully created.';
$lang['project_location_create_failure']	= 'There was a problem creating the project_location: ';
$lang['project_location_create_new_button']	= 'Create New Project Location';
$lang['project_location_invalid_id']		= 'Invalid Project Location ID.';
$lang['project_location_edit_success']		= 'Project Location successfully saved.';
$lang['project_location_edit_failure']		= 'There was a problem saving the project_location: ';
$lang['project_location_delete_success']	= 'record(s) successfully deleted.';

$lang['project_location_purged']	= 'record(s) successfully purged.';
$lang['project_location_success']	= 'record(s) successfully restored.';


$lang['project_location_delete_failure']	= 'We could not delete the record: ';
$lang['project_location_delete_error']		= 'You have not selected any records to delete.';
$lang['project_location_actions']			= 'Actions';
$lang['project_location_cancel']			= 'Cancel';
$lang['project_location_delete_record']		= 'Delete';
$lang['project_location_delete_confirm']	= 'Are you sure you want to delete this project_location?';
$lang['project_location_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['project_location_action_edit']		= 'Save';
$lang['project_location_action_create']		= 'Create';

// Activities
$lang['project_location_act_create_record']	= 'Created record with ID';
$lang['project_location_act_edit_record']	= 'Updated record with ID';
$lang['project_location_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['project_location_column_created']	= 'Created';
$lang['project_location_column_deleted']	= 'Deleted';
$lang['project_location_column_modified']	= 'Modified';
