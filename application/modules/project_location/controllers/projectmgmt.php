<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



/**

 * quotes controller

 */

class projectmgmt extends Admin_Controller

{



	//--------------------------------------------------------------------





	/**

	 * Constructor

	 *

	 * @return void

	 */

	public function __construct()

	{

		parent::__construct();



		$this->auth->restrict('Project_Location.projectmgmt.View');

		$this->load->model('project_location_model', null, true);

		$this->lang->load('project_location');

		

		Template::set_block('sub_nav', 'projectmgmt/_sub_nav');



		Assets::add_module_js('project_location', 'project_location.js');

	}



	//--------------------------------------------------------------------





	/**

	 * Displays a list of form data.

	 *

	 * @return void

	

	public function index() */

	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')

	{

		$this->load->library('session');

		$this->load->library('pagination');

		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		

		$user_id =  $this->auth->user_id();



		// Deleting anything?

		if (isset($_POST['delete']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					$result = $this->project_location_model->delete($pid);

				}



				if ($result)

				{

					Template::set_message(count($checked) .' '. lang('project_location_delete_success'), 'success');

				}

				else

				{

					Template::set_message(lang('project_location_delete_failure') . $this->project_location_model->error, 'error');

				}

			}

		}

		

				

switch ( $this->input->post('submit') ) 

{

case 'Search':

$this->session->set_userdata('sapfield',$this->input->post('select_field'));

$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));

$this->session->set_userdata('sapfname',$this->input->post('field_name'));





break;

case 'Reset':

$this->session->unset_userdata('sapfield');

$this->session->unset_userdata('sapfvalue');

$this->session->unset_userdata('sapfname');

break;

}



if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }

else { $field = 'id'; }



if ( $this->session->userdata('sapfield') =='All Fields') 

{ 

 		

  $total = $this->project_location_model

   ->where('deleted','0')

  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 

  ->count_all();

  

}

else

{

	

  $total = $this->project_location_model

    ->where('deleted','0')

  ->likes($field,$this->session->userdata('sapfvalue'),'both')  

  ->count_all();

}



		//$records = $this->project_location_model->find_all();

		/**************************Pagination********************************/

$offset = $this->input->get('per_page');

$this->pager['base_url'] = current_url() .'?';

$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');

$this->pager['total_rows'] = $total;

$this->pager['per_page'] = 5;

$this->pager['num_links'] = 20;

$this->pager['page_query_string']= TRUE;

$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case "admin" :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case "todo" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case "notebook" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}






if ( $this->session->userdata('sapfield') =='All Fields') 

{

$records = $this->personal_particulars_model

 

    ->where('deleted','0')

   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')

  ->limit($this->pager['per_page'], $offset)

	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 

                 ->find_all();

	 

}

else

{

	if ( $this->input->get('sort_by') !='' )

	{

		$ord1 = array (	

	$this->input->get('sort_by') => $this->input->get('sort_order'),

	'id'=>'desc',

	 );		

	}

	else { $ord1 = array (	

		'id'=>'desc',

	 );	}

	 	 	

$records = $this->project_location_model

  ->where('deleted','0')

->likes($field,$this->session->userdata('sapfvalue'),'both')

->limit($this->pager['per_page'], $offset)

	->order_by( $ord1 ) 

                 ->find_all();	

		

}

//$records = $this->project_location_model->find_all();

		

$this->pagination->initialize($this->pager);

Template::set('current_url', current_url());

Template::set('sort_by', $sort_by);

Template::set('role_name', $role_name);

Template::set('user_id', $user_id);

Template::set('sort_order', $sort_order);

Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('project_location', 'style.css');



		Template::set('records', $records);

		Template::set('toolbar_title', 'Manage Project Location');

		Template::render();

	}



	//--------------------------------------------------------------------





	

	

	

	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')

	{

		$this->load->library('session');

		$this->load->library('pagination');

		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());

		

		$user_id =  $this->auth->user_id();

		

				

				

if (isset($_POST['restore']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					

					$result = $this->db->query('UPDATE intg_project_location SET deleted = 0 where id = '.$pid.'');			

				}

				

											



				if ($result)

				{

					Template::set_message(lang('project_location_success'), 'success');

				}

				else

				{

					Template::set_message(lang('project_location_restored_error'). $this->project_location_model->error, 'error');

				}

			}

		}

		

		if (isset($_POST['purge']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					$result = 	$this->db->query('delete from intg_project_location where id = '.$pid.'');

		

				}



				if ($result)

				{

					

					Template::set_message(count($checked) .' '. lang('project_location_purged'), 'success');

				}

				

			}

		}

		

switch ( $this->input->post('submit') ) 

{

case 'Search':

$this->session->set_userdata('sapfield',$this->input->post('select_field'));

$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));

$this->session->set_userdata('sapfname',$this->input->post('field_name'));





break;

case 'Reset':

$this->session->unset_userdata('sapfield');

$this->session->unset_userdata('sapfvalue');

$this->session->unset_userdata('sapfname');

break;

}





if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }

else { $field = 'id'; }



if ( $this->session->userdata('sapfield') =='All Fields') 

{ 

 		

  $total = $this->project_location_model

  ->where('deleted','1')

  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 

  ->count_all();





}

else

{

	

  $total = $this->project_location_model

   ->where('deleted','1')

  ->likes($field,$this->session->userdata('sapfvalue'),'both')  

  ->count_all();

}



//$records = $this-project_location_model->find_all();

/**************************Pagination********************************/

$offset = $this->input->get('per_page');

$this->pager['base_url'] = current_url() .'?';

$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');

$this->pager['total_rows'] = $total;

$this->pager['per_page'] = 5;

$this->pager['num_links'] = 20;

$this->pager['page_query_string']= TRUE;

$this->pager['uri_segment'] = 5;


switch ($this->config->item('template.admin_theme') )
{
case "admin" :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case "todo" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case "notebook" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}



if ( $this->session->userdata('sapfield') =='All Fields') 

{

$records = $this->personal_particulars_model

 

  ->where('deleted','1')

   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')

 

 



->limit($this->pager['per_page'], $offset)

	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 

                 ->find_all();

	 

}

else

{

	if ( $this->input->get('sort_by') !='' )

	{

		$ord1 = array (	

	$this->input->get('sort_by') => $this->input->get('sort_order'),

	'id'=>'desc',

	 );		

	}

	else { $ord1 = array (	

		'id'=>'desc',

	 );	}

	 

	 	

$records = $this->project_location_model

->where('deleted','1')

->likes($field,$this->session->userdata('sapfvalue'),'both')

->limit($this->pager['per_page'], $offset)

	->order_by( $ord1 ) 

                 ->find_all();	

		

}



		//$records = $this->project_location_model->find_all();

		

$this->pagination->initialize($this->pager);

Template::set('current_url', current_url());

Template::set('sort_by', $sort_by);

Template::set('role_name', $role_name);

Template::set('user_id', $user_id);

Template::set('sort_order', $sort_order);

Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('project_location', 'style.css');



		Template::set('records', $records);

		Template::set('toolbar_title', 'Manage Project Location');

		Template::render();

	}



	//--------------------------------------------------------------------







	/**

	 * Creates a Project Location object.

	 *

	 * @return void

	 */

	public function create()

	{

		$this->auth->restrict('Project_Location.Quotes.Create');



		if (isset($_POST['save']))

		{

			if ($insert_id = $this->save_project_location())

			{

				// Log the activity

				log_activity($this->current_user->id, lang('project_location_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'project_location');



				Template::set_message(lang('project_location_create_success'), 'success');

				redirect(SITE_AREA .'/projectmgmt/project_location');

			}

			else

			{

				Template::set_message(lang('project_location_create_failure') . $this->project_location_model->error, 'error');

			}

		}

		Assets::add_module_js('project_location', 'project_location.js');



		Template::set('toolbar_title', lang('project_location_create') . ' Project Location');

		Template::render();

	}



	//--------------------------------------------------------------------





	/**

	 * Allows editing of Project Location data.

	 *

	 * @return void

	 */

	public function edit()

	{

		$id = $this->uri->segment(5);



		if (empty($id))

		{

			Template::set_message(lang('project_location_invalid_id'), 'error');

			redirect(SITE_AREA .'/projectmgmt/project_location');

		}



		if (isset($_POST['save']))

		{

			$this->auth->restrict('Project_Location.projectmgmt.Edit');



			if ($this->save_project_location('update', $id))

			{

				// Log the activity

				log_activity($this->current_user->id, lang('project_location_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'project_location');



				Template::set_message(lang('project_location_edit_success'), 'success');

			}

			else

			{

				Template::set_message(lang('project_location_edit_failure') . $this->project_location_model->error, 'error');

			}

		}

		else if (isset($_POST['delete']))

		{

			$this->auth->restrict('Project_Location.projectmgmt.Delete');



			if ($this->project_location_model->delete($id))

			{

				// Log the activity

				log_activity($this->current_user->id, lang('project_location_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'project_location');



				Template::set_message(lang('project_location_delete_success'), 'success');



				redirect(SITE_AREA .'/projectmgmt/project_location');

			}

			else

			{

				Template::set_message(lang('project_location_delete_failure') . $this->project_location_model->error, 'error');

			}

		}

		Template::set('project_location', $this->project_location_model->find($id));

		Template::set('toolbar_title', lang('project_location_edit') .' Project Location');

		Template::render();

	}



	//--------------------------------------------------------------------







public function restore_purge()

	{

		$id = $this->uri->segment(5);

		if (isset($_POST['restore']))

		{

				

					$result = $this->db->query('UPDATE intg_project_location SET deleted = 0 where id = '.$id.'');			



				if ($result)

				{

						Template::set_message(lang('project_location_success'), 'success');

											redirect(SITE_AREA .'/projectmgmt/project_location/deleted');

				}

				else

				{

					

					Template::set_message(lang('project_location_error') . $this->project_location_model->error, 'error');

				}

		}

		

		

		if (isset($_POST['purge']))

		{

					

					$result = 	$this->db->query('delete from intg_project_location where id = '.$id.'');		



				if ($result)

				{

					

						Template::set_message(lang('project_location_purged'), 'success');

					redirect(SITE_AREA .'/projectmgmt/project_location/deleted');

				}

				

			

		}

		



		if (empty($id))

		{

				Template::set_message(lang('project_location_invalid_id'), 'error');

			redirect(SITE_AREA .'/projectmgmt/project_location');

		}



		

		

		

		Template::set('project_location', $this->project_location_model->find($id));

		

		Assets::add_module_js('project_location', 'project_location.js');



		Template::set('toolbar_title', 'Restore / Purge' . ' Project_location');

		Template::render();

	}






	//--------------------------------------------------------------------

	// !PRIVATE METHODS

	//--------------------------------------------------------------------



	/**

	 * Summary

	 *

	 * @param String $type Either "insert" or "update"

	 * @param Int	 $id	The ID of the record to update, ignored on inserts

	 *

	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE

	 */

	private function save_project_location($type='insert', $id=0)

	{

		if ($type == 'update')

		{

			$_POST['id'] = $id;

		}



		// make sure we only pass in the fields we want

		

		$data = array();
		$data['project_location']        = $this->input->post('project_location_project_location');



		if ($type == 'insert')

		{

			$id = $this->project_location_model->insert($data);



			if (is_numeric($id))

			{

				$return = $id;

			}

			else

			{

				$return = FALSE;

			}

		}

		elseif ($type == 'update')

		{

			$return = $this->project_location_model->update($id, $data);

		}



		return $return;

	}



	//--------------------------------------------------------------------






}
