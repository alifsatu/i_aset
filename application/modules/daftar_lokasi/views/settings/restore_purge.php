<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_lokasi))
{
	$daftar_lokasi = (array) $daftar_lokasi;
}
$id = isset($daftar_lokasi['id']) ? $daftar_lokasi['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Lokasi</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_lokasi_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_lokasi_name' class='input-sm input-s  form-control' type='text' name='daftar_lokasi_name' maxlength="255" value="<?php echo set_value('daftar_lokasi_name', isset($daftar_lokasi['name']) ? $daftar_lokasi['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_lokasi_district_id', $options, set_value('daftar_lokasi_district_id', isset($daftar_lokasi['district_id']) ? $daftar_lokasi['district_id'] : ''), 'District'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Descriptin', 'daftar_lokasi_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'daftar_lokasi_description', 'id' => 'daftar_lokasi_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('daftar_lokasi_description', isset($daftar_lokasi['description']) ? $daftar_lokasi['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/daftar_lokasi', lang('daftar_lokasi_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Daftar_Lokasi.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('daftar_lokasi_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>