<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_lokasi_manage']			= 'Manage';
$lang['daftar_lokasi_edit']				= 'Edit';
$lang['daftar_lokasi_true']				= 'True';
$lang['daftar_lokasi_false']				= 'False';
$lang['daftar_lokasi_create']			= 'Save';
$lang['daftar_lokasi_list']				= 'List';
$lang['daftar_lokasi_new']				= 'New';
$lang['daftar_lokasi_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_lokasi_no_records']		= 'There aren\'t any daftar_lokasi in the system.';
$lang['daftar_lokasi_create_new']		= 'Create a new Daftar Lokasi.';
$lang['daftar_lokasi_create_success']	= 'Daftar Lokasi successfully created.';
$lang['daftar_lokasi_create_failure']	= 'There was a problem creating the daftar_lokasi: ';
$lang['daftar_lokasi_create_new_button']	= 'Create New Daftar Lokasi';
$lang['daftar_lokasi_invalid_id']		= 'Invalid Daftar Lokasi ID.';
$lang['daftar_lokasi_edit_success']		= 'Daftar Lokasi successfully saved.';
$lang['daftar_lokasi_edit_failure']		= 'There was a problem saving the daftar_lokasi: ';
$lang['daftar_lokasi_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_lokasi_purged']	= 'record(s) successfully purged.';
$lang['daftar_lokasi_success']	= 'record(s) successfully restored.';


$lang['daftar_lokasi_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_lokasi_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_lokasi_actions']			= 'Actions';
$lang['daftar_lokasi_cancel']			= 'Cancel';
$lang['daftar_lokasi_delete_record']		= 'Delete';
$lang['daftar_lokasi_delete_confirm']	= 'Are you sure you want to delete this daftar_lokasi?';
$lang['daftar_lokasi_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_lokasi_action_edit']		= 'Save';
$lang['daftar_lokasi_action_create']		= 'Create';

// Activities
$lang['daftar_lokasi_act_create_record']	= 'Created record with ID';
$lang['daftar_lokasi_act_edit_record']	= 'Updated record with ID';
$lang['daftar_lokasi_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_lokasi_column_created']	= 'Created';
$lang['daftar_lokasi_column_deleted']	= 'Deleted';
$lang['daftar_lokasi_column_modified']	= 'Modified';
