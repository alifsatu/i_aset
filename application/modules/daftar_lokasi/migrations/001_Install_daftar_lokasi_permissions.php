<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_daftar_lokasi_permissions extends Migration
{

	/**
	 * Permissions to Migrate
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name' => 'Daftar_Lokasi.Aset.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Aset.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Aset.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Aset.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Aset.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Aset.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Projectmgmt.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Reports.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Settings.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Daftar_Lokasi.Developer.Delete',
			'description' => '',
			'status' => 'active',
		),
	);

	/**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $table_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->table_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->table_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->table_name, array('name' => $permission_value['name']));
		}
	}

	//--------------------------------------------------------------------

}