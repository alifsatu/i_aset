<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['currency_manage']			= 'Manage';
$lang['currency_edit']				= 'Edit';
$lang['currency_true']				= 'True';
$lang['currency_false']				= 'False';
$lang['currency_create']			= 'Save';
$lang['currency_list']				= 'List';
$lang['currency_new']				= 'New';
$lang['currency_edit_text']			= 'Edit this to suit your needs';
$lang['currency_no_records']		= 'There aren\'t any currency in the system.';
$lang['currency_create_new']		= 'Create a new Currency.';
$lang['currency_create_success']	= 'Currency successfully created.';
$lang['currency_create_failure']	= 'There was a problem creating the currency: ';
$lang['currency_create_new_button']	= 'Create New Currency';
$lang['currency_invalid_id']		= 'Invalid Currency ID.';
$lang['currency_edit_success']		= 'Currency successfully saved.';
$lang['currency_edit_failure']		= 'There was a problem saving the currency: ';
$lang['currency_delete_success']	= 'record(s) successfully deleted.';

$lang['currency_purged']	= 'record(s) successfully purged.';
$lang['currency_success']	= 'record(s) successfully restored.';


$lang['currency_delete_failure']	= 'We could not delete the record: ';
$lang['currency_delete_error']		= 'You have not selected any records to delete.';
$lang['currency_actions']			= 'Actions';
$lang['currency_cancel']			= 'Cancel';
$lang['currency_delete_record']		= 'Delete';
$lang['currency_delete_confirm']	= 'Are you sure you want to delete this currency?';
$lang['currency_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['currency_action_edit']		= 'Save';
$lang['currency_action_create']		= 'Create';

// Activities
$lang['currency_act_create_record']	= 'Created record with ID';
$lang['currency_act_edit_record']	= 'Updated record with ID';
$lang['currency_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['currency_column_created']	= 'Created';
$lang['currency_column_deleted']	= 'Deleted';
$lang['currency_column_modified']	= 'Modified';
