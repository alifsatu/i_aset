<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($currency))
{
	$currency = (array) $currency;
}
$id = isset($currency['id']) ? $currency['id'] : '';

?>
<div class="row">
	<h3>Currency</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('currency_company_id', $options, set_value('currency_company_id', isset($currency['company_id']) ? $currency['company_id'] : ''), 'Company Name'. lang('bf_form_label_required'));
			?>

			<div class="control-group <?php echo form_error('home_currency') ? 'error' : ''; ?>">
				<?php echo form_label('Home Currency'. lang('bf_form_label_required'), 'currency_home_currency', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='currency_home_currency' type='text' name='currency_home_currency' maxlength="45" value="<?php echo set_value('currency_home_currency', isset($currency['home_currency']) ? $currency['home_currency'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('home_currency'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('converted_currency') ? 'error' : ''; ?>">
				<?php echo form_label('Converted Currency'. lang('bf_form_label_required'), 'currency_converted_currency', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='currency_converted_currency' type='text' name='currency_converted_currency' maxlength="45" value="<?php echo set_value('currency_converted_currency', isset($currency['converted_currency']) ? $currency['converted_currency'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('converted_currency'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('conversion_rate') ? 'error' : ''; ?>">
				<?php echo form_label('Conversion Rate', 'currency_conversion_rate', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='currency_conversion_rate' type='text' name='currency_conversion_rate' maxlength="45" value="<?php echo set_value('currency_conversion_rate', isset($currency['conversion_rate']) ? $currency['conversion_rate'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('conversion_rate'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('currency_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/currency', lang('currency_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Currency.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('currency_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('currency_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>