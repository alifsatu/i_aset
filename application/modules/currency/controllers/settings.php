<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Currency.Settings.View');
		$this->load->model('currency_model', null, true);
		$this->lang->load('currency');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('currency', 'currency.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		
		$user_id =  $this->auth->user_id();

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->currency_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('currency_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('currency_delete_failure') . $this->currency_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}

if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->currency_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->currency_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

		//$records = $this->currency_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;



if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->currency_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->currency_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('currency', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Currency');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_currency SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('currency_success'), 'success');
				}
				else
				{
					Template::set_message(lang('currency_restored_error'). $this->currency_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_currency where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('currency_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}


if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->currency_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->currency_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

//$records = $this-currency_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;


if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->currency_model
->where('deleted','1')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->currency_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('currency', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Currency');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Currency object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Currency.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_currency())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('currency_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'currency');

				Template::set_message(lang('currency_create_success'), 'success');
				redirect(SITE_AREA .'/settings/currency');
			}
			else
			{
				Template::set_message(lang('currency_create_failure') . $this->currency_model->error, 'error');
			}
		}
		Assets::add_module_js('currency', 'currency.js');

		Template::set('toolbar_title', lang('currency_create') . ' Currency');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Currency data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('currency_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/currency');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Currency.Settings.Edit');

			if ($this->save_currency('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('currency_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'currency');

				Template::set_message(lang('currency_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('currency_edit_failure') . $this->currency_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Currency.Settings.Delete');

			if ($this->currency_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('currency_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'currency');

				Template::set_message(lang('currency_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/currency');
			}
			else
			{
				Template::set_message(lang('currency_delete_failure') . $this->currency_model->error, 'error');
			}
		}
		Template::set('currency', $this->currency_model->find_by(array('id'=>$id,'company_id'=>$this->current_user->company_id)));
		Template::set('toolbar_title', lang('currency_edit') .' Currency');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_currency SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('currency_success'), 'success');
											redirect(SITE_AREA .'/settings/currency/deleted');
				}
				else
				{
					
					Template::set_message(lang('currency_error') . $this->currency_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_currency where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('currency_purged'), 'success');
					redirect(SITE_AREA .'/settings/currency/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('currency_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/currency');
		}

		
		
		
		Template::set('currency', $this->currency_model->find_by(array('id'=>$id,'company_id'=>$this->current_user->company_id)));
		
		Assets::add_module_js('currency', 'currency.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Currency');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_currency($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['company_id']        = $this->input->post('currency_company_id');
		$data['home_currency']        = $this->input->post('currency_home_currency');
		$data['converted_currency']        = $this->input->post('currency_converted_currency');
		$data['conversion_rate']        = $this->input->post('currency_conversion_rate');

		if ($type == 'insert')
		{
			$id = $this->currency_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->currency_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}