<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project_progress_model extends BF_Model {

	protected $table_name	= "project_progress";
	protected $key			= "id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= false;
	protected $set_modified = false;

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "project_id",
			"label"		=> "Project ID",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "task_id",
			"label"		=> "Task ID",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "user_id",
			"label"		=> "User ID",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "timestamp",
			"label"		=> "Time",
			"rules"		=> "max_length[11]"
		),
		array(
			"field"		=> "progress_percentage",
			"label"		=> "Progress %",
			"rules"		=> "max_length[3]"
		),
		array(
			"field"		=> "remarks",
			"label"		=> "Remarks",
			"rules"		=> "max_length[500]"
		),
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

	public function get_progress_history($task_id)
	{
		if($progress_history = $this->project_progress_model->find_all_by('task_id', $task_id)) {
			$user_model = $this->user_model;
			return array_map(function($progress)use($user_model){
				$progress->user = $user_model->find($progress->user_id);
				return $progress;
			}, $progress_history);
		}
		return false;
	}
}
