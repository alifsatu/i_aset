<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['ccc_manage']			= 'Manage';
$lang['ccc_edit']				= 'Edit';
$lang['ccc_true']				= 'True';
$lang['ccc_false']				= 'False';
$lang['ccc_create']			= 'Save';
$lang['ccc_list']				= 'List';
$lang['ccc_new']				= 'New';
$lang['ccc_edit_text']			= 'Edit this to suit your needs';
$lang['ccc_no_records']		= 'There aren\'t any ccc in the system.';
$lang['ccc_create_new']		= 'Create a new CCC.';
$lang['ccc_create_success']	= 'CCC successfully created.';
$lang['ccc_create_failure']	= 'There was a problem creating the ccc: ';
$lang['ccc_create_new_button']	= 'Create New CCC';
$lang['ccc_invalid_id']		= 'Invalid CCC ID.';
$lang['ccc_edit_success']		= 'CCC successfully saved.';
$lang['ccc_edit_failure']		= 'There was a problem saving the ccc: ';
$lang['ccc_delete_success']	= 'record(s) successfully deleted.';

$lang['ccc_purged']	= 'record(s) successfully purged.';
$lang['ccc_success']	= 'record(s) successfully restored.';


$lang['ccc_delete_failure']	= 'We could not delete the record: ';
$lang['ccc_delete_error']		= 'You have not selected any records to delete.';
$lang['ccc_actions']			= 'Actions';
$lang['ccc_cancel']			= 'Cancel';
$lang['ccc_delete_record']		= 'Delete';
$lang['ccc_delete_confirm']	= 'Are you sure you want to delete this ccc?';
$lang['ccc_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['ccc_action_edit']		= 'Save';
$lang['ccc_action_create']		= 'Create';

// Activities
$lang['ccc_act_create_record']	= 'Created record with ID';
$lang['ccc_act_edit_record']	= 'Updated record with ID';
$lang['ccc_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['ccc_column_created']	= 'Created';
$lang['ccc_column_deleted']	= 'Deleted';
$lang['ccc_column_modified']	= 'Modified';
