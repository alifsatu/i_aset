<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * projectmgmt controller
 */
class projectmgmt extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('CCC.Projectmgmt.View');
        $this->load->model('ccc_model', null, true);
        $this->lang->load('ccc');

        $this->load->model('projek/projek_model', null, true);
        $this->load->model('daftar_status_permohonan_sokongan_ccc/daftar_status_permohonan_sokongan_ccc_model', null, true);
        $this->load->model('daftar_kaedah_jaminan_ccc/daftar_kaedah_jaminan_ccc_model', null, true);
        $this->load->model('daftar_status_jaminan_ccc/daftar_status_jaminan_ccc_model', null, true);


        Assets::add_css("js/datepicker/datepicker.css");
        Assets::add_js("js/datepicker/bootstrap-datepicker.js");
        Template::set_block('sub_nav', 'projectmgmt/_sub_nav');

        Assets::add_css("js/datepicker/datepicker.css");
        Assets::add_css("js/datatables/datatables.min.css");

        Assets::add_js("js/datepicker/bootstrap-datepicker.js");
        Assets::add_js("js/libs/moment.min.js");
        Assets::add_js("js/libs/countdown.min.js");
        Assets::add_js("js/libs/moment-countdown.min.js");

        //alif

        Assets::add_js(array(Template::theme_url('js/datatables/datatables.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.flash.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/JSZip-2.5.0/jszip.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/pdfmake.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/vfs_fonts.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.html5.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.print.min.js')));

        Assets::add_module_js('ccc', 'ccc.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->ccc_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('ccc_delete_success'), 'success');
                } else {
                    Template::set_message(lang('ccc_delete_failure') . $this->ccc_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('cccfield', $this->input->post('select_field'));
                $this->session->set_userdata('cccfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('cccfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('cccfield');
                $this->session->unset_userdata('cccfvalue');
                $this->session->unset_userdata('cccfname');
                break;
        }

        $field = '';
        $ord1 = '';
        if ($this->session->userdata('cccfield') != '') {
            $field = $this->session->userdata('cccfield');
        }
//        else {
//            $field = 'id';
//        }

        if ($this->session->userdata('cccfield') == 'All Fields') {

            $this->ccc_model->where('deleted', '0');
            $this->ccc_model->likes('personal_particulars_name', $this->session->userdata('cccfvalue'), 'both');
//            $this->ccc_model->likes('personal_particulars_nric', $this->session->userdata('cccfvalue'), 'both');
//            $this->ccc_model->likes('personal_particulars_htel', $this->session->userdata('cccfvalue'), 'both');
//            $this->ccc_model->likes('personal_particulars_email', $this->session->userdata('cccfvalue'), 'both');
//            $this->ccc_model->likes('personal_particulars_citizenship', $this->session->userdata('cccfvalue'), 'both');
            $total = $this->ccc_model->count_all();
        } else if ($this->session->userdata('cccfield') == 'intg_ccc.id') {

            $this->ccc_model->where('deleted', '0');
            $this->ccc_model->likes($field, $this->session->userdata('cccfvalue'), 'both');
            $total = $this->ccc_model->count_all();
        } else {
            $this->ccc_model->where('deleted', '0');
            if ($field != '') {
                $this->ccc_model->where($field, $this->session->userdata('cccfvalue'));
            }
            $total = $this->ccc_model->count_all();
        }

        if ($this->session->userdata('cccfield') == 'All Fields') {
            $likes = str_replace(" ", '%', $this->session->userdata('cccfvalue'));
            if ($this->session->userdata('cccfvalue') != '') {
                if ($likes[0] != '%') {
                    $likes = '%' . $likes;
                }

                if (substr($this->session->userdata('cccfvalue'), -1) != '%') {
                    $likes = $likes . '%';
                }
            }

            $this->ccc_model->where('deleted', '0');
            $this->ccc_model->where("nama_projek like '" . $likes . "'");
            $this->ccc_model->or_where("nama_pemilik like '" . $likes . "'");
            $this->ccc_model->or_where("no_rujukan like '" . $likes . "'");
        } else if ($this->session->userdata('cccfield') == 'intg_ccc.id') {

            $this->ccc_model->where('deleted', '0');
            $this->ccc_model->where('id', $this->session->userdata('cccfvalue'));
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $this->ccc_model->where('deleted', '0');
            if ($field != '') {
                $this->ccc_model->where($field, $this->session->userdata('cccfvalue'));
            }
        }

        if ($this->auth->has_permission('CCC.Projectmgmt.Admin')) {
            
        } else if ($this->auth->user()->daerah != '' || $this->auth->user()->daerah != '0') {

            $this->projek_model->select('id');
            $this->projek_model->where('deleted', '0');
            $this->projek_model->where('daerah', $this->auth->user()->daerah);
            $projek_list = $this->projek_model->find_all();
            
            $projeklist = array();
            
            foreach($projek_list as $val){
                $projeklist[] = $val->id;
            }
            
//            print_r_pre(implode(',', $projeklist));die;

            $this->ccc_model->where_in('projek_id', $projeklist);
        } else {
//
//            $this->ccc_model->where('created_by', $this->auth->user_id());
        }

        if ($ord1 != '') {
            $this->ccc_model->order_by($ord1);
        }

        $this->ccc_model->where('deleted', '0');
        $records = $this->ccc_model->find_all();
//        echo $this->db->last_query();die;

        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Assets::add_module_css('ccc', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage CCC');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_ccc SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('ccc_success'), 'success');
                } else {
                    Template::set_message(lang('ccc_restored_error') . $this->ccc_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_ccc where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('ccc_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('cccfield', $this->input->post('select_field'));
                $this->session->set_userdata('cccfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('cccfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('cccfield');
                $this->session->unset_userdata('cccfvalue');
                $this->session->unset_userdata('cccfname');
                break;
        }


        if ($this->session->userdata('cccfield') != '') {
            $field = $this->session->userdata('cccfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('cccfield') == 'All Fields') {

            $total = $this->ccc_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('cccfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->ccc_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('cccfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-ccc_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('cccfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('cccfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('cccfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->ccc_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('cccfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->ccc_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('ccc', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage CCC');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a CCC object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('CCC.Projectmgmt.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_ccc()) {
                // Log the activity
                log_activity($this->current_user->id, lang('ccc_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'ccc');

                Template::set_message(lang('ccc_create_success'), 'success');
                redirect(SITE_AREA . '/projectmgmt/ccc');
            } else {
                Template::set_message(lang('ccc_create_failure') . $this->ccc_model->error, 'error');
            }
        }

        //if isset projek_id
        if ($this->input->get('id')) {
            Template::set('projek', $this->projek_model->find($this->input->get('id')));
            Template::set('projek_id', $this->input->get('id'));
        }


        Assets::add_module_js('ccc', 'ccc.js');

        Template::set('status_permohonan_sokongan_ccc', $this->daftar_status_permohonan_sokongan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('kaedah_jaminan_ccc', $this->daftar_kaedah_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_jaminan_ccc', $this->daftar_status_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        Template::set('toolbar_title', lang('ccc_create') . ' CCC');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of CCC data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('ccc_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/ccc');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('CCC.Projectmgmt.Edit');

            if ($this->save_ccc('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ccc_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ccc');

                Template::set_message(lang('ccc_edit_success'), 'success');
            } else {
                Template::set_message(lang('ccc_edit_failure') . $this->ccc_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('CCC.Projectmgmt.Delete');

            if ($this->ccc_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('ccc_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'ccc');

                Template::set_message(lang('ccc_delete_success'), 'success');

                redirect(SITE_AREA . '/projectmgmt/ccc');
            } else {
                Template::set_message(lang('ccc_delete_failure') . $this->ccc_model->error, 'error');
            }
        }

        $ccc = $this->ccc_model->find($id);

        if ($this->input->get('id')) {
            Template::set('projek', $this->projek_model->find($this->input->get('id')));
            Template::set('projek_id', $this->input->get('id'));
        } else {
            Template::set('projek_id', $ccc->projek_id);
            Template::set('projek', $this->projek_model->find($ccc->projek_id));
        }

        Template::set('status_permohonan_sokongan_ccc', $this->daftar_status_permohonan_sokongan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('kaedah_jaminan_ccc', $this->daftar_kaedah_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_jaminan_ccc', $this->daftar_status_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        Template::set('ccc', $ccc);
        Template::set('toolbar_title', lang('ccc_edit') . ' CCC');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_ccc SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('ccc_success'), 'success');
                redirect(SITE_AREA . '/projectmgmt/ccc/deleted');
            } else {

                Template::set_message(lang('ccc_error') . $this->ccc_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_ccc where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('ccc_purged'), 'success');
                redirect(SITE_AREA . '/projectmgmt/ccc/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('ccc_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/ccc');
        }




        Template::set('ccc', $this->ccc_model->find($id));

        Assets::add_module_js('ccc', 'ccc.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Ccc');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_ccc($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['tarikh_terima_permohonan_sokongan_ccc'] = $this->input->post('ccc_tarikh_terima_permohonan_sokongan_ccc') ? date_format(date_create($this->input->post('ccc_tarikh_terima_permohonan_sokongan_ccc')), "Y-m-d") : '0000-00-00';
        $data['status_permohonan_sokongan_ccc'] = $this->input->post('ccc_status_permohonan_sokongan_ccc') == '' ? '0' : $this->input->post('ccc_status_permohonan_sokongan_ccc');
        $data['tarikh_sokongan_ccc_dikeluarkan'] = $this->input->post('ccc_tarikh_sokongan_ccc_dikeluarkan') ? date_format(date_create($this->input->post('ccc_tarikh_sokongan_ccc_dikeluarkan')), "Y-m-d") : '0000-00-00';
        $data['tarikh_mula_liabiliti'] = $this->input->post('ccc_tarikh_mula_liabiliti') ? date_format(date_create($this->input->post('ccc_tarikh_mula_liabiliti')), "Y-m-d") : '0000-00-00';
        $data['tarikh_tamat_liabiliti'] = $this->input->post('ccc_tarikh_tamat_liabiliti') ? date_format(date_create($this->input->post('ccc_tarikh_tamat_liabiliti')), "Y-m-d") : '0000-00-00';
        $data['kaedah_jaminan'] = $this->input->post('ccc_kaedah_jaminan') == '' ? '0' : $this->input->post('ccc_kaedah_jaminan');
        $data['jumlah_jaminan'] = $this->input->post('ccc_jumlah_jaminan');
        $data['no_resit_bg'] = $this->input->post('ccc_no_resit_bg');
        $data['tarikh_tamat_jaminan'] = $this->input->post('ccc_tarikh_tamat_jaminan') ? date_format(date_create($this->input->post('ccc_tarikh_tamat_jaminan')), "Y-m-d") : '0000-00-00';
        $data['nama_pemilik_syarikat'] = $this->input->post('ccc_nama_pemilik_syarikat');
        $data['no_kad_pengenalan'] = $this->input->post('ccc_no_kad_pengenalan');
        $data['no_syarikat'] = $this->input->post('ccc_no_syarikat');
        $data['nama_bank'] = $this->input->post('ccc_nama_bank');
        $data['no_akaun_bank'] = $this->input->post('ccc_no_akaun_bank');
        $data['no_fail'] = $this->input->post('ccc_no_fail');
        $data['no_tel'] = $this->input->post('ccc_no_tel');
        $data['projek_id'] = $this->input->post('projek_id');

        $data['status_jaminan'] = $this->input->post('ccc_status_jaminan') == '' ? '0' : $this->input->post('ccc_status_jaminan');
        $data['jumlah_wang_jaminan_dipulangkan'] = $this->input->post('ccc_jumlah_wang_jaminan_dipulangkan');
        $data['tarikh_wang_jaminan_dipulangkan'] = $this->input->post('ccc_tarikh_wang_jaminan_dipulangkan') ? date_format(date_create($this->input->post('ccc_tarikh_wang_jaminan_dipulangkan')), "Y-m-d") : '0000-00-00';
        $data['no_baucer'] = $this->input->post('ccc_no_baucer');

        if ($type == 'insert') {
            $id = $this->ccc_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->ccc_model->update($id, $data);
        }

        return $return;
    }

    public function view() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('ccc_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/ccc');
        }

        $ccc = $this->ccc_model->find($id);

        if ($this->input->get('id')) {
            Template::set('projek', $this->projek_model->find($this->input->get('id')));
            Template::set('projek_id', $this->input->get('id'));
        } else {
            Template::set('projek_id', $ccc->projek_id);
            Template::set('projek', $this->projek_model->find($ccc->projek_id));
        }

        Template::set('status_permohonan_sokongan_ccc', $this->daftar_status_permohonan_sokongan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('kaedah_jaminan_ccc', $this->daftar_kaedah_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_jaminan_ccc', $this->daftar_status_jaminan_ccc_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        Template::set('view_only', true);
        Template::set('ccc', $ccc);
        Template::set('toolbar_title', lang('ccc_edit') . ' CCC');
        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------
}
