<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ccc_model extends BF_Model {

	protected $table_name	= "ccc";
	protected $key			= "id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";

	protected $log_user = true;

	protected $set_created	= true;
	protected $set_modified = true;
	protected $created_field = "created_on";
	protected $modified_field = "modified_on";

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "ccc_tarikh_terima_permohonan_sokongan_ccc",
			"label"		=> "Tarikh Terima Permohonan Sokongan CCC",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_status_permohonan_sokongan_ccc",
			"label"		=> "Status Permohonan Sokongan CCC",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_tarikh_sokongan_ccc_dikeluarkan",
			"label"		=> "Tarikh Sokongan CCC Dikeluarkan",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_tarikh_mula_liabiliti",
			"label"		=> "Tarikh Mula Liabiliti",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_tarikh_tamat_liabiliti",
			"label"		=> "Tarikh Tamat Liabiliti",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_kaedah_jaminan",
			"label"		=> "Kaedah Jaminan",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_jumlah_jaminan",
			"label"		=> "Jumlah Jaminan",
			"rules"		=> "is_decimal|max_length[255]"
		),
		array(
			"field"		=> "ccc_no_resit_bg",
			"label"		=> "No. Resit atau BG",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_tarikh_tamat_jaminan",
			"label"		=> "Tarikh Tamat Jaminan",
			"rules"		=> ""
		),
		array(
			"field"		=> "ccc_nama_pemilik_syarikat",
			"label"		=> "Nama Pemilik atau Syarikat",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_no_kad_pengenalan",
			"label"		=> "No. Kad Pengenalan",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_no_syarikat",
			"label"		=> "No. Syarikat",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_nama_bank",
			"label"		=> "Nama Bank",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_no_akaun_bank",
			"label"		=> "No Akaun Bank",
			"rules"		=> "max_length[255]"
		),
		array(
			"field"		=> "ccc_no_fail",
			"label"		=> "No Fail",
			"rules"		=> "max_length[255]"
		),
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

}
