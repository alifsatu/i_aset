<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($ccc))
{
	$ccc = (array) $ccc;
}
$id = isset($ccc['id']) ? $ccc['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">CCC</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('tarikh_terima_permohonan_sokongan_ccc') ? 'error' : ''; ?>">
				<?php echo form_label('Tarikh Terima Permohonan Sokongan CCC', 'ccc_tarikh_terima_permohonan_sokongan_ccc', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_tarikh_terima_permohonan_sokongan_ccc' class='input-sm input-s  form-control' type='text' name='ccc_tarikh_terima_permohonan_sokongan_ccc'  value="" />
					<span class='help-inline'><?php echo form_error('tarikh_terima_permohonan_sokongan_ccc'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('ccc_status_permohonan_sokongan_ccc', $options, set_value('ccc_status_permohonan_sokongan_ccc', isset($ccc['status_permohonan_sokongan_ccc']) ? $ccc['status_permohonan_sokongan_ccc'] : ''), 'Status Permohonan Sokongan CCC');
			?>

			<div class="form-group <?php echo form_error('tarikh_sokongan_ccc_dikeluarkan') ? 'error' : ''; ?>">
				<?php echo form_label('Tarikh Sokongan CCC Dikeluarkan', 'ccc_tarikh_sokongan_ccc_dikeluarkan', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_tarikh_sokongan_ccc_dikeluarkan' class='input-sm input-s  form-control' type='text' name='ccc_tarikh_sokongan_ccc_dikeluarkan'  value="<?php echo ;?>" />
					<span class='help-inline'><?php echo form_error('tarikh_sokongan_ccc_dikeluarkan'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('tarikh_mula_liabiliti') ? 'error' : ''; ?>">
				<?php echo form_label('Tarikh Mula Liabiliti', 'ccc_tarikh_mula_liabiliti', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_tarikh_mula_liabiliti' class='input-sm input-s  form-control' type='text' name='ccc_tarikh_mula_liabiliti'  value="<?php echo ;?>" />
					<span class='help-inline'><?php echo form_error('tarikh_mula_liabiliti'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('tarikh_tamat_liabiliti') ? 'error' : ''; ?>">
				<?php echo form_label('Tarikh Tamat Liabiliti', 'ccc_tarikh_tamat_liabiliti', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_tarikh_tamat_liabiliti' class='input-sm input-s  form-control' type='text' name='ccc_tarikh_tamat_liabiliti'  value="<?php echo ;?>" />
					<span class='help-inline'><?php echo form_error('tarikh_tamat_liabiliti'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
				);

				echo form_dropdown('ccc_kaedah_jaminan', $options, set_value('ccc_kaedah_jaminan', isset($ccc['kaedah_jaminan']) ? $ccc['kaedah_jaminan'] : ''), 'Kaedah Jaminan');
			?>

			<div class="form-group <?php echo form_error('jumlah_jaminan') ? 'error' : ''; ?>">
				<?php echo form_label('Jumlah Jaminan', 'ccc_jumlah_jaminan', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_jumlah_jaminan' class='input-sm input-s  form-control' type='text' name='ccc_jumlah_jaminan' maxlength="255" value="<?php echo set_value('ccc_jumlah_jaminan', isset($ccc['jumlah_jaminan']) ? $ccc['jumlah_jaminan'] : '');?>" />
					<span class='help-inline'><?php echo form_error('jumlah_jaminan'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('no_resit_bg') ? 'error' : ''; ?>">
				<?php echo form_label('No. Resit atau BG', 'ccc_no_resit_bg', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_no_resit_bg' class='input-sm input-s  form-control' type='text' name='ccc_no_resit_bg' maxlength="255" value="<?php echo set_value('ccc_no_resit_bg', isset($ccc['no_resit_bg']) ? $ccc['no_resit_bg'] : '');?>" />
					<span class='help-inline'><?php echo form_error('no_resit_bg'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('tarikh_tamat_jaminan') ? 'error' : ''; ?>">
				<?php echo form_label('Tarikh Tamat Jaminan', 'ccc_tarikh_tamat_jaminan', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_tarikh_tamat_jaminan' class='input-sm input-s  form-control' type='text' name='ccc_tarikh_tamat_jaminan'  value="<?php echo set_value('ccc_no_resit_bg', isset($ccc['no_resit_bg']) ? $ccc['no_resit_bg'] : '');?>" />
					<span class='help-inline'><?php echo form_error('tarikh_tamat_jaminan'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('nama_pemilik_syarikat') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Pemilik atau Syarikat', 'ccc_nama_pemilik_syarikat', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_nama_pemilik_syarikat' class='input-sm input-s  form-control' type='text' name='ccc_nama_pemilik_syarikat' maxlength="255" value="<?php echo set_value('ccc_nama_pemilik_syarikat', isset($ccc['nama_pemilik_syarikat']) ? $ccc['nama_pemilik_syarikat'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama_pemilik_syarikat'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('no_kad_pengenalan') ? 'error' : ''; ?>">
				<?php echo form_label('No. Kad Pengenalan', 'ccc_no_kad_pengenalan', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_no_kad_pengenalan' class='input-sm input-s  form-control' type='text' name='ccc_no_kad_pengenalan' maxlength="255" value="<?php echo set_value('ccc_no_kad_pengenalan', isset($ccc['no_kad_pengenalan']) ? $ccc['no_kad_pengenalan'] : '');?>" />
					<span class='help-inline'><?php echo form_error('no_kad_pengenalan'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('no_syarikat') ? 'error' : ''; ?>">
				<?php echo form_label('No. Syarikat', 'ccc_no_syarikat', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_no_syarikat' class='input-sm input-s  form-control' type='text' name='ccc_no_syarikat' maxlength="255" value="<?php echo set_value('ccc_no_syarikat', isset($ccc['no_syarikat']) ? $ccc['no_syarikat'] : '');?>" />
					<span class='help-inline'><?php echo form_error('no_syarikat'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('nama_bank') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Bank', 'ccc_nama_bank', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_nama_bank' class='input-sm input-s  form-control' type='text' name='ccc_nama_bank' maxlength="255" value="<?php echo set_value('ccc_nama_bank', isset($ccc['nama_bank']) ? $ccc['nama_bank'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama_bank'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('no_akaun_bank') ? 'error' : ''; ?>">
				<?php echo form_label('No Akaun Bank', 'ccc_no_akaun_bank', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_no_akaun_bank' class='input-sm input-s  form-control' type='text' name='ccc_no_akaun_bank' maxlength="255" value="<?php echo set_value('ccc_no_akaun_bank', isset($ccc['no_akaun_bank']) ? $ccc['no_akaun_bank'] : '');?>" />
					<span class='help-inline'><?php echo form_error('no_akaun_bank'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('no_fail') ? 'error' : ''; ?>">
				<?php echo form_label('No Fail', 'ccc_no_fail', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='ccc_no_fail' class='input-sm input-s  form-control' type='text' name='ccc_no_fail' maxlength="255" value="<?php echo set_value('ccc_no_fail', isset($ccc['no_fail']) ? $ccc['no_fail'] : '');?>" />
					<span class='help-inline'><?php echo form_error('no_fail'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('ccc_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/projectmgmt/ccc', lang('ccc_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('CCC.Projectmgmt.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('ccc_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('ccc_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>