<?php
$Admin_Can_Edit = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Edit');
$Admin_Can_Delete = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Delete');

$vo = isset($view_only) ? $view_only : false;
$eo = isset($edit_only) ? $edit_only : false;


$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($ccc)) {
    $ccc = (array) $ccc;
}
$id = isset($ccc['id']) ? $ccc['id'] : '';
?>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">CCC</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <input type="hidden" name="projek_id" value="<?= $projek_id ?>">
                <div class="row">
                    <div class="col-md-12">

                        <h5 class="well"><b>ID Projek</b> : <?= $projek_id ?> <br><b>Nama Projek</b> : <?= $projek->nama_projek ?>

                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('tarikh_terima_permohonan_sokongan_ccc') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh Terima Permohonan Sokongan CCC', 'ccc_tarikh_terima_permohonan_sokongan_ccc', array('class' => 'control-label')); ?>
                            <div class='controls'>

                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_terima_permohonan_sokongan_ccc']) && $ccc['tarikh_terima_permohonan_sokongan_ccc'] != '0000-00-00' ? $ccc['tarikh_terima_permohonan_sokongan_ccc'] : '';
                                } else {
                                    ?>
                                    <input id='ccc_tarikh_terima_permohonan_sokongan_ccc' class="datepicker-input form-control " data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_terima_permohonan_sokongan_ccc'  value="<?php echo set_value('ccc_tarikh_terima_permohonan_sokongan_ccc', isset($ccc['tarikh_terima_permohonan_sokongan_ccc']) && $ccc['tarikh_terima_permohonan_sokongan_ccc'] != '0000-00-00' ? $ccc['tarikh_terima_permohonan_sokongan_ccc'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('tarikh_terima_permohonan_sokongan_ccc'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Status Permohonan Sokongan CCC' ?></label>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    foreach ($status_permohonan_sokongan_ccc as $sp) {
                                        if ($sp->id == $ccc['status_permohonan_sokongan_ccc']) {
                                            echo $sp->nama;
                                        }
                                    }
                                } else {
                                    ?>


                                    <select name="ccc_status_permohonan_sokongan_ccc" id="ccc_status_permohonan_sokongan_ccc" class="form-control selecta"  >
                                        <option value="">--Sila Pilih--</option>
                                        <?php
                                        foreach ($status_permohonan_sokongan_ccc as $sp) {
                                            if ($sp->id == $ccc['status_permohonan_sokongan_ccc']) {
                                                ?>
                                                <option selected="selected" value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('tarikh_sokongan_ccc_dikeluarkan') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh Sokongan CCC Dikeluarkan', 'ccc_tarikh_sokongan_ccc_dikeluarkan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_sokongan_ccc_dikeluarkan']) && $ccc['tarikh_sokongan_ccc_dikeluarkan'] != '0000-00-00' ? $ccc['tarikh_sokongan_ccc_dikeluarkan'] : '';
                                } else {
                                    ?>
                                    <input autocomplete="off" id='ccc_tarikh_sokongan_ccc_dikeluarkan'class="datepicker-input form-control " data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_sokongan_ccc_dikeluarkan'  value="<?php echo set_value('ccc_tarikh_sokongan_ccc_dikeluarkan', isset($ccc['tarikh_sokongan_ccc_dikeluarkan']) && $ccc['tarikh_sokongan_ccc_dikeluarkan'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_sokongan_ccc_dikeluarkan'])) : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('tarikh_sokongan_ccc_dikeluarkan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('tarikh_mula_liabiliti') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh Mula Liabiliti', 'ccc_tarikh_mula_liabiliti', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_mula_liabiliti']) && $ccc['tarikh_mula_liabiliti'] != '0000-00-00' ? $ccc['tarikh_mula_liabiliti'] : '';
                                } else {
                                    ?>
                                    <input autocomplete="off" id='ccc_tarikh_mula_liabiliti' class="datepicker-input form-control " data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_mula_liabiliti'  value="<?php echo set_value('ccc_tarikh_mula_liabiliti', isset($ccc['tarikh_mula_liabiliti']) && $ccc['tarikh_mula_liabiliti'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_mula_liabiliti'])) : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('tarikh_mula_liabiliti'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('tarikh_tamat_liabiliti') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh Tamat Liabiliti', 'ccc_tarikh_tamat_liabiliti', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_tamat_liabiliti']) && $ccc['tarikh_tamat_liabiliti'] != '0000-00-00' ? $ccc['tarikh_tamat_liabiliti'] : '';
                                } else {
                                    ?>
                                    <input autocomplete="off" id='ccc_tarikh_tamat_liabiliti' class="datepicker-input form-control " data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_tamat_liabiliti'  value="<?php echo set_value('ccc_tarikh_tamat_liabiliti', isset($ccc['tarikh_tamat_liabiliti']) && $ccc['tarikh_tamat_liabiliti'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_tamat_liabiliti'])) : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('tarikh_tamat_liabiliti'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Kaedah Jaminan' ?></label>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    foreach ($kaedah_jaminan_ccc as $sp) {
                                        if ($sp->id == $ccc['kaedah_jaminan']) {
                                            echo $sp->nama;
                                        }
                                    }
                                } else {
                                    ?>


                                    <select name="ccc_kaedah_jaminan" id="ccc_kaedah_jaminan" class="form-control selecta"  >
                                        <option value="">--Sila Pilih--</option>
                                        <?php
                                        foreach ($kaedah_jaminan_ccc as $sp) {
                                            if ($sp->id == $ccc['kaedah_jaminan']) {
                                                ?>
                                                <option selected="selected" value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('jumlah_jaminan') ? 'error' : ''; ?>">
                            <?php echo form_label('Jumlah Jaminan', 'ccc_jumlah_jaminan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['jumlah_jaminan']) ? 'RM' . number_format((float) $ccc['jumlah_jaminan'], 2, '.', '') : '';
                                } else {
                                    ?>
                                    <input id='ccc_jumlah_jaminan' class='  form-control' type='text' pattern="^\d+(\.\d{2})?$" data-type="currency" name='ccc_jumlah_jaminan' maxlength="255" value="<?php echo set_value('ccc_jumlah_jaminan', isset($ccc['jumlah_jaminan']) ? $ccc['jumlah_jaminan'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('jumlah_jaminan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_resit_bg') ? 'error' : ''; ?>">
                            <?php echo form_label('No. Resit atau BG', 'ccc_no_resit_bg', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_resit_bg']) ? $ccc['no_resit_bg'] : '';
                                } else {
                                    ?>
                                    <input id='ccc_no_resit_bg' class='  form-control' type='text' name='ccc_no_resit_bg' maxlength="255" value="<?php echo set_value('ccc_no_resit_bg', isset($ccc['no_resit_bg']) ? $ccc['no_resit_bg'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_resit_bg'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('tarikh_tamat_jaminan') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh Tamat Jaminan', 'ccc_tarikh_tamat_jaminan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_tamat_jaminan']) && $ccc['tarikh_tamat_jaminan'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_tamat_jaminan'])) : '';
                                    ?>
                                    <span class='help-inline' id="countdownxx">

                                        <?php
                                        if (isset($ccc['tarikh_tamat_jaminan']) && $ccc['tarikh_tamat_jaminan'] != '0000-00-00') {
                                            echo "<b>Bil. Hari : </b>" . $days = floor((strtotime($ccc['tarikh_tamat_jaminan']) - time()) / 60 / 60 / 24);
//                                            $years_remaining = intval($days / 365); //divide by 365 and throw away the remainder
//                                            $days_remaining = $days % 365;
//                                            echo '<b> Tamat Dalam</b> ' . $years_remaining . " tahun," . $days_remaining . " Hari";

                                            $date1 = new DateTime($ccc['tarikh_tamat_jaminan']);
                                            $date2 = new DateTime(date('Y-m-d'));

                                            $interval = $date2->diff($date1);
                                            echo " | <b> Tamat Dalam</b> " . $interval->format('%Y tahun, %m bulan, %d hari') . "|";
                                        } else {
                                            echo ' Tarikh Tamat Jaminan Belum Ditetapkan';
                                        }
                                        ?>

                                    </span>
                                    <?php
                                } else {
                                    ?>
                                    <input autocomplete="off" id='ccc_tarikh_tamat_jaminan' class="datepicker-input form-control " data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_tamat_jaminan'  value="<?php echo set_value('ccc_tarikh_tamat_jaminan', isset($ccc['tarikh_tamat_jaminan']) && $ccc['tarikh_tamat_jaminan'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_tamat_jaminan'])) : ''); ?>" />
                                    <span class='help-inline' id="countdownxxx"></span>
                                    <span class='help-inline'><?php echo form_error('tarikh_tamat_jaminan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('nama_pemilik_syarikat') ? 'error' : ''; ?>">
                            <?php echo form_label('Nama Pemilik atau Syarikat', 'ccc_nama_pemilik_syarikat', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['nama_pemilik_syarikat']) ? $ccc['nama_pemilik_syarikat'] : '';
                                } else {
                                    ?>
                                    <input id='ccc_nama_pemilik_syarikat' class='  form-control' type='text' name='ccc_nama_pemilik_syarikat' maxlength="255" value="<?php echo set_value('ccc_nama_pemilik_syarikat', isset($ccc['nama_pemilik_syarikat']) ? $ccc['nama_pemilik_syarikat'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('nama_pemilik_syarikat'); ?></span>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_kad_pengenalan') ? 'error' : ''; ?>">
                            <?php echo form_label('No. Kad Pengenalan', 'ccc_no_kad_pengenalan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_kad_pengenalan']) ? $ccc['no_kad_pengenalan'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_kad_pengenalan' class='  form-control' type='text' name='ccc_no_kad_pengenalan' maxlength="255" value="<?php echo set_value('ccc_no_kad_pengenalan', isset($ccc['no_kad_pengenalan']) ? $ccc['no_kad_pengenalan'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_kad_pengenalan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_syarikat') ? 'error' : ''; ?>">
                            <?php echo form_label('No. Syarikat', 'ccc_no_syarikat', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_syarikat']) ? $ccc['no_syarikat'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_syarikat' class='  form-control' type='text' name='ccc_no_syarikat' maxlength="255" value="<?php echo set_value('ccc_no_syarikat', isset($ccc['no_syarikat']) ? $ccc['no_syarikat'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_syarikat'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('nama_bank') ? 'error' : ''; ?>">
                            <?php echo form_label('Nama Bank', 'ccc_nama_bank', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['nama_bank']) ? $ccc['nama_bank'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_nama_bank' class='  form-control' type='text' name='ccc_nama_bank' maxlength="255" value="<?php echo set_value('ccc_nama_bank', isset($ccc['nama_bank']) ? $ccc['nama_bank'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('nama_bank'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_akaun_bank') ? 'error' : ''; ?>">
                            <?php echo form_label('No Akaun Bank', 'ccc_no_akaun_bank', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_akaun_bank']) ? $ccc['no_akaun_bank'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_akaun_bank' class='  form-control' type='text' name='ccc_no_akaun_bank' maxlength="255" value="<?php echo set_value('ccc_no_akaun_bank', isset($ccc['no_akaun_bank']) ? $ccc['no_akaun_bank'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_akaun_bank'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_fail') ? 'error' : ''; ?>">
                            <?php echo form_label('No Fail', 'ccc_no_fail', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_fail']) ? $ccc['no_fail'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_fail' class='  form-control' type='text' name='ccc_no_fail' maxlength="255" value="<?php echo set_value('ccc_no_fail', isset($ccc['no_fail']) ? $ccc['no_fail'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_fail'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_tel') ? 'error' : ''; ?>">
                            <?php echo form_label('No Tel.', 'ccc_no_tel', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_tel']) ? $ccc['no_tel'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_tel' class='  form-control' type='text' name='ccc_no_tel' maxlength="255" value="<?php echo set_value('ccc_no_tel', isset($ccc['no_tel']) ? $ccc['no_tel'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_tel'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?= 'Status Jaminan' ?></label>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    foreach ($status_jaminan_ccc as $sp) {
                                        if ($sp->id == $ccc['status_jaminan']) {
                                            echo $sp->nama;
                                        }
                                    }
                                } else {
                                    ?>


                                    <select onchange="check_status_jaminan()" onblur="check_status_jaminan()" name="ccc_status_jaminan" id="ccc_status_jaminan" class="form-control selecta" >
                                        <option value="">--Sila Pilih--</option>
                                        <?php
                                        foreach ($status_jaminan_ccc as $sp) {
                                            if ($sp->id == $ccc['status_jaminan']) {
                                                ?>
                                                <option selected="selected" value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $sp->id ?>"><?= $sp->nama; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group <?php echo form_error('jumlah_wang_jaminan_dipulangkan') ? 'error' : ''; ?>">
                            <?php echo form_label('Jumlah WJ/BG Dipulangkan', 'ccc_no_fail', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['jumlah_wang_jaminan_dipulangkan']) ? 'RM' . number_format((float) $ccc['jumlah_wang_jaminan_dipulangkan'], 2, '.', '') : '';
                                } else {
                                    ?> 
                                    <input id='ccc_jumlah_wang_jaminan_dipulangkan' class='  form-control ' <?= isset($ccc['status_jaminan']) && $ccc['status_jaminan'] == 1 ? 'readonly' : '' ?> type='text' pattern="^\d+(\.\d{2})?$" data-type="currency" name='ccc_jumlah_wang_jaminan_dipulangkan' maxlength="255" value="<?php echo set_value('ccc_jumlah_wang_jaminan_dipulangkan', isset($ccc['jumlah_wang_jaminan_dipulangkan']) ? $ccc['jumlah_wang_jaminan_dipulangkan'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('jumlah_wang_jaminan_dipulangkan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group <?php echo form_error('tarikh_wang_jaminan_dipulangkan') ? 'error' : ''; ?>">
                            <?php echo form_label('Tarikh WJ/BG Dipulangkan', 'ccc_tarikh_wang_jaminan_dipulangkan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['tarikh_wang_jaminan_dipulangkan']) && $ccc['tarikh_wang_jaminan_dipulangkan'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_wang_jaminan_dipulangkan'])) : '';
                                } else {
                                    ?> 
                                    <input autocomplete="off" id='ccc_tarikh_wang_jaminan_dipulangkan' class="datepicker-input form-control " <?= isset($ccc['status_jaminan']) && $ccc['status_jaminan'] == 1 ? 'readonly' : '' ?> data-date-format="dd-mm-yyyy"  type='text' name='ccc_tarikh_wang_jaminan_dipulangkan'  value="<?php echo set_value('ccc_tarikh_wang_jaminan_dipulangkan', isset($ccc['tarikh_wang_jaminan_dipulangkan']) && $ccc['tarikh_wang_jaminan_dipulangkan'] != '0000-00-00' ? date("d-m-Y", strtotime($ccc['tarikh_wang_jaminan_dipulangkan'])) : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('tarikh_wang_jaminan_dipulangkan'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group <?php echo form_error('no_baucer') ? 'error' : ''; ?>">
                            <?php echo form_label('No. Baucer', 'ccc_no_baucer', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                if ($vo) {
                                    echo isset($ccc['no_baucer']) ? $ccc['no_baucer'] : '';
                                } else {
                                    ?> 
                                    <input id='ccc_no_baucer' class='  form-control' type='text' <?= isset($ccc['status_jaminan']) && $ccc['status_jaminan'] == 1 ? 'readonly' : '' ?> name='ccc_no_baucer' maxlength="255" value="<?php echo set_value('ccc_no_baucer', isset($ccc['no_baucer']) ? $ccc['no_baucer'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('no_baucer'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        if ($this->uri->segment(4) == 'create') {
                            ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('ccc_action_create'); ?>"  />
                                &nbsp;&nbsp;
                                <?php echo anchor(SITE_AREA . '/projectmgmt/ccc', lang('ccc_cancel'), 'class="btn btn-warning"'); ?>

                            </div>
                            <?php
                        } elseif ($this->uri->segment(4) == 'view') {
                            
                        } else {
                            ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary cannot_edit" value="<?php echo lang('ccc_action_edit'); ?>"  />
                                &nbsp;&nbsp;
                                <?php echo anchor(SITE_AREA . '/projectmgmt/ccc', lang('ccc_cancel'), 'class="btn btn-warning cannot_edit"'); ?>

                                <?php if ($this->auth->has_permission('CCC.Projectmgmt.Delete')) : ?>
                                    &nbsp;&nbsp;
                                    <button type="submit" name="delete" class="btn btn-danger cannot_edit" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('ccc_delete_confirm'))); ?>');">
                                        <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('ccc_delete_record'); ?>
                                    </button>
                                <?php endif; ?>
                            </div>
                        <?php }
                        ?>
                    </div>

                </div>
                <?php echo form_close(); ?>
            </div>
        </section>
    </div>
</div>


<script type="text/javascript">

    function getDateAfter(whichdate, monthcount) {
        console.log(whichdate);

        var futureDate = moment(new Date(whichdate)).add(monthcount, 'months');

        return futureDate.format('DD-MM-YYYY');

    }
    $(document).ready(function () {

        check_tarikh_tamat_jaminan();

        $('#ccc_tarikh_mula_liabiliti').datepicker()
                .on('changeDate', function (e) {
                    if ($('#ccc_tarikh_mula_liabiliti').val() == '') {
                        $('#ccc_tarikh_tamat_liabiliti').val('').datepicker('update');
                    } else {
                        waitingDialog.show('Loading...');
                        var dt = getDateAfter(moment($('#ccc_tarikh_mula_liabiliti').val(), 'DD-MM-YYYY').format('YYYY-MM-DD'), 24);
                        $('#ccc_tarikh_tamat_liabiliti').datepicker('setValue', dt);


                        if ($('#ccc_kaedah_jaminan').val() == '1' || $('#ccc_kaedah_jaminan').val() == '2') {
                            var dtjaminan = getDateAfter(moment($('#ccc_tarikh_tamat_liabiliti').val(), 'DD-MM-YYYY').format('YYYY-MM-DD'), 3);
                            $('#ccc_tarikh_tamat_jaminan').datepicker('setValue', dtjaminan);
                            check_tarikh_tamat_jaminan();
                        }


                        waitingDialog.hide();
                    }
                });

    });

    function check_tarikh_tamat_jaminan() {
        if ($('#ccc_tarikh_tamat_jaminan').val() != '') {
            $('#countdown').html('<em><b>Tamat Dalam : </b> ' + moment($('#ccc_tarikh_tamat_jaminan').val()).countdown().toString() + '</em>');
        } else {
            $('#countdown').html('<em>Tarikh Tamat Jaminan Belum Ditetapkan</em>');
        }
    }

    function check_status_jaminan() {
        if ($('#ccc_status_jaminan').val() == '1') {
            $('#ccc_jumlah_wang_jaminan_dipulangkan').attr('readonly', true);
            $('#ccc_tarikh_wang_jaminan_dipulangkan').attr('readonly', true);
            $('#ccc_no_baucer').attr('readonly', true);
        } else {
            $('#ccc_jumlah_wang_jaminan_dipulangkan').removeAttr('readonly');
            $('#ccc_tarikh_wang_jaminan_dipulangkan').removeAttr('readonly');
            $('#ccc_no_baucer').removeAttr('readonly');
        }

        if ($('#ccc_status_jaminan').val() == '3') {
            $('#ccc_no_baucer').attr('readonly', true);
        } else if ($('#ccc_status_jaminan').val() != '1' && $('#ccc_status_jaminan').val() != '3') {
            $('#ccc_no_baucer').removeAttr('readonly');
        }

    }

// Jquery Dependency

    $("input[data-type='currency']").on({
        keyup: function () {
            formatCurrency($(this));
        },
        blur: function () {
            formatCurrency($(this), "blur");
        }
    });


    function formatNumber(n) {
        // format number 1000000 to 1,234,567
//        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return n.replace(/\D/g, "");
    }


    function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") {
            return;
        }

        // original length
        var original_len = input_val.length;

        // initial caret position 
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // validate right side
            right_side = formatNumber(right_side);

            // On blur make sure 2 numbers after decimal
            if (blur === "blur") {
                right_side += "00";
            }

            // Limit decimal to only 2 digits
            right_side = right_side.substring(0, 2);

            // join number by .
//            input_val = "RM" + left_side + "." + right_side;
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
//            input_val = "RM" + input_val;

            // final formatting
            if (blur === "blur") {
                input_val += ".00";
            }
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }



</script>