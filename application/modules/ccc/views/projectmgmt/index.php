<?php
$num_columns = 19;
$can_delete = $this->auth->has_permission('CCC.Projectmgmt.Delete');
$can_edit = $this->auth->has_permission('CCC.Projectmgmt.Edit');
$user_list = get_user_name_list();

$Admin_Can_Edit = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Edit');
$Admin_Can_Delete = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Delete');

$has_records = isset($records) && is_array($records) && count($records);

$kaedah_jaminan_ccc_list = get_kaedah_jaminan_ccc_list();
$status_permohonan_ccc_sokongan_list = get_status_permohonan_ccc_sokongan_list();
?>
<style>
    #reset {
        -webkit-animation: flash 6s infinite linear;
        animation: flash 6s infinite linear;
    }
    div.dataTables_wrapper {
        width: 100vw;
        margin: 0 auto;
    }
</style>
<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">

            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>
            <div class="row filter_area" >
                <div class="col-md-2">
                    <h4>CCC</h4>
                </div>
                <div class="col-md-3">
                    <select name="select_field" id="select_field" class="form-control">
                        <option value=''>--Pilih Jenis Saringan--</option>
                        <option <?php echo $this->session->userdata('cccfield') == "All Fields" ? "selected" : "" ?> value="All Fields">All Fields</option>
                        <option <?php echo $this->session->userdata('cccfield') == "intg_ccc.projek_id" ? "selected" : "" ?> value="intg_ccc.projek_id">ID Projek</option>
                        <!--<option <?php echo $this->session->userdata('cccfield') == "intg_projek.daerah" ? "selected" : "" ?> value="intg_projek.daerah">Daerah</option>-->
                        <!--<option <?php echo $this->session->userdata('cccfield') == "intg_projek.pegawai_ulasan" ? "selected" : "" ?> value="intg_projek.pegawai_ulasan">Pegawai Ulasan</option>-->
                        <!--<option <?php echo $this->session->userdata('cccfield') == "intg_projek.nama_jurutera_perunding" ? "selected" : "" ?> value="intg_projek.nama_jurutera_perunding">Jurutera Perunding</option>-->
                    </select>


                </div>
                <div class="col-md-6">
                    <div class="input-group">

                        <input type="hidden" name="field_name" id="field_name" value="<?= $this->session->userdata('cccfname') ?>"/>
                        <select name="field_value" class="selecta form-control field_value_select" id="projects" >
                            <option ></option><?php
                            $proj = '';
                            switch ($this->session->userdata('cccfield')) {

                                case "intg_projek.pegawai_ulasan" :
                                    $proj = $this->db->query("select id as rowid,display_name as selectedname from intg_users where deleted = 0")->result();
                                    break;

                                case "intg_projek.daerah" :
                                    $proj = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_daerah where deleted = 0")->result();
                                    break;

                                case "intg_projek.nama_jurutera_perunding" :
                                    $proj = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_jurutera_perunding where status = 1")->result();
                                    break;

                                default :
//                                    $proj = $this->db->query("select id as rowid,nama_projek as selectedname from intg_projek where deleted =0")->result();
                                    break;
                            }
                            if ($proj != '') {
                                foreach ($proj as $p) {
                                    if ($this->session->userdata('cccfvalue') == $p->rowid) {
                                        ?>
                                        <option  selected="selected" value="<?= $p->rowid ?>"><?= $p->rowid ?> - <?= $p->selectedname ?></option>
                                    <?php } else { ?><option   value="<?= $p->rowid ?>"><?= $p->rowid ?> - <?= $p->selectedname ?></option><?php
                                    }
                                }
                            }
                            ?>
                        </select>

                        <input id='search_input' class='input-sm input-s  form-control field_value_input' type='number' min="0" step="1" name='field_value' value="<?= $this->session->userdata('cccfvalue') ?>" placeholder="Sila Masukkan Kunci Carian" />


                        <span class="input-group-btn">
                            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info">
                                <span class="fa  fa-search"></span>
                            </button>
                            <button type="submit" name="submit"   class="btn btn-primary btn-icon" <?= $this->session->userdata('cccfield') ? 'id="reset"' : '' ?> value="Reset" title="Reset">
                                <span class="fa  fa-refresh"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <?php echo form_close(); ?>
            <hr>
            <?php echo form_open($this->uri->uri_string()); ?>
            <div id="dvData">
                <div class="table-responsive">   
                    <table class="table table-striped table-hover table-condensed text-sm dt-nowrap table-bordered" style="width:100%" id='indexTable'>
                        <thead>
                            <tr>
                                <?php if ($can_delete && $has_records) { ?>
                                    <th class="column-check">Bil <input class="check-all" type="checkbox" /></th>
                                <?php } else { ?>
                                    <th>Bil </th>
                                <?php } ?>
                                <th>Tindakan</th>
                                <th>ID Projek</th>
                                <th>Didaftar Oleh</th>
                                <th>Tarikh Terima Permohonan Sokongan CCC</th>
                                <th>Status Permohonan Sokongan CCC</th>
                                <th>Tarikh Sokongan CCC Dikeluarkan</th>
                                <th>Tarikh Mula Liabiliti</th>
                                <th>Tarikh Tamat Liabiliti</th>
                                <th>Kaedah Jaminan</th>
                                <th>Jumlah Jaminan</th>
                                <th>No. Resit atau BG</th>
                                <th>Tarikh Tamat Jaminan</th>
                                <th>Nama Pemilik atau Syarikat</th>
                                <th>No. Kad Pengenalan</th>
                                <th>No. Syarikat</th>
                                <th>Nama Bank</th>
                                <th>No Akaun Bank</th>
                                <th>No Fail</th>
                                <th>No. Tel</th>
                                <th>No Baucer</th>
                            </tr>
                        </thead>
                        <?php if ($has_records) : ?>
                                                                                               <!-- <tfoot>
                            <?php if ($can_delete) : ?>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td colspan="<?php echo $num_columns; ?>">
                                <?php echo lang('bf_with_selected'); ?>
                                                                                                                                                                                    <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('ccc_delete_confirm'))); ?>')" />
                                                                                                                                                                                </td>
                                                                                                                                                                            </tr>
                            <?php endif; ?>
                                                                                                </tfoot> -->
                        <?php endif; ?>
                        <tbody>
                            <?php
                            if ($has_records) : $no = 1;
                                foreach ($records as $record) :
                                    ?>
                                    <tr>
                                        <?php if (($can_delete && $this->auth->user_id() == $record->created_by) || $Admin_Can_Delete) { ?>
                                            <td class="column-check"><?= $no ?>&nbsp;&nbsp;
                                                <input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" />
                                            </td>

                                        <?php } else { ?>
                                            <td><?= $no ?></td>
                                        <?php } ?>
                                        <td>
                                            <?php if (($can_edit && $this->auth->user_id() == $record->created_by) || $Admin_Can_Edit) { ?>
                                                <?php echo anchor(SITE_AREA . '/projectmgmt/ccc/edit/' . $record->id, "<i class='fa fa-pencil'> Edit </i>&nbsp;&nbsp", " data-toggle='tooltip' data-placement='top'  title='Kemaskini Projek' class='btn bg-info' "); ?> 
                                            <?php } ?>

                                            &nbsp;&nbsp;
                                            <?php
                                            if ($this->auth->has_permission('Projek.Projectmgmt.View')) {
                                                echo anchor(SITE_AREA . '/projectmgmt/ccc/view/' . $record->id, "<i class='fa fa-eye'> View</i>", " data-toggle='tooltip' data-placement='top'  title='Lihat CCC' class='btn bg-success'  ");
                                            }
                                            ?>

                                        </td>
                                        <td><?php e($record->projek_id); ?></td>
                                        <td><?php echo $record->created_by != '' ? $user_list[$record->created_by] : '' ?></td>
                                        <td><?php echo $record->tarikh_terima_permohonan_sokongan_ccc == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_terima_permohonan_sokongan_ccc)); ?></td>
                                        <td><?php echo $record->status_permohonan_sokongan_ccc == '0' ? '' : $status_permohonan_ccc_sokongan_list[$record->status_permohonan_sokongan_ccc] ?></td>
                                        <td><?php echo $record->tarikh_sokongan_ccc_dikeluarkan == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_sokongan_ccc_dikeluarkan)); ?></td>
                                        <td><?php echo $record->tarikh_mula_liabiliti == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_mula_liabiliti)); ?></td>
                                        <td><?php echo $record->tarikh_tamat_liabiliti == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_tamat_liabiliti)); ?></td>
                                        <td><?php echo $record->kaedah_jaminan == '0' ? '' : $kaedah_jaminan_ccc_list[$record->kaedah_jaminan] ?></td>
                                        <td><?php e($record->jumlah_jaminan) ?></td>
                                        <td><?php e($record->no_resit_bg) ?></td>
                                        <td><?php echo $record->tarikh_tamat_jaminan == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_tamat_jaminan)); ?></td>
                                        <td><?php e($record->nama_pemilik_syarikat) ?></td>
                                        <td><?php e($record->no_kad_pengenalan) ?></td>
                                        <td><?php e($record->no_syarikat) ?></td>
                                        <td><?php e($record->nama_bank) ?></td>
                                        <td><?php e($record->no_akaun_bank) ?></td>
                                        <td><?php e($record->no_fail) ?></td>
                                        <td><?php e($record->no_tel) ?></td>
                                        <td><?php e($record->no_baucer) ?></td>
            <!--                                    <td><?php // echo date("d/m/Y h:i:s", strtotime($record->created_on));                                 ?></td>
                                        <td><?php // echo $record->modified_on == "0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s", strtotime($record->modified_on));                                 ?></td>-->
                                    </tr>
                                    <?php
                                    $no++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div> 
            </div>
            <?php echo form_close(); ?>

            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-4 hidden-xs"></div>
                    <div class="col-sm-4 text-center"></div>
                    <div class="col-sm-4 text-right text-center-xs"></div>
                </div>
            </footer>                
        </div>


</div>
</section>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var x = '<?= $this->session->userdata('cccfield') ?>';
        if (x == 'All Fields' || x == 'intg_ccc.id') {
            $("#search_input").attr('name', 'field_value');
            $("#projects").attr('name', 'field_value_x');

            if (x == 'intg_projek.id') {
                $("#search_input").attr('type', 'number');
            } else if (x == 'All Fields') {
                $("#search_input").attr('type', 'text');
            }


            $('.field_value_select').hide();
            $('.field_value_input').show();
        } else {

            $("#search_input").attr('name', 'field_value_x');
            $("#projects").attr('name', 'field_value');
            $('.field_value_input').hide();
            $('.field_value_select').show();
        }

<?php if ($has_records) { ?>

            // Setup - add a text input to each header cell
            var count = 1;
            $('#indexTable thead th').each(function () {
                if (count > 2) {
                    var title = $('#indexTable thead th').eq($(this).index()).text();
                    $(this).html(title + '<br><input type="text" >');
                }
                count++;
            });

            var example = $('#indexTable').DataTable({
                "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                //            dom: 'Bflrpitip',
                dom: "<'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>>" +
                        "<'row'<'col-md-4'i><'col-md-4'p>>" +
                        "<'row'<'col-md-12'tr>>" +
                        "<'row'<'col-md-4'i><'col-md-4'p>>",
                columnDefs: [
                    {targets: 3, width: "500px"}
                ],
                buttons: [
                    {
                        extend: 'copyHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'csvHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'print',
                        footer: true,
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        footer: true,
                        orientation: 'landscape',
                        pageSize: 'A4',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    }


                ],
                oLanguage: {
                    sLengthMenu: "Record to show : _MENU_",
                },
                order: [[2, "desc"]]
            });


            // Apply the search
            example.columns().eq(0).each(function (colIdx) {
                $('input', example.column(colIdx).header()).on('keyup change', function () {
                    example
                            .column(colIdx)
                            .search(this.value)
                            .draw();
                });

                $('input', example.column(colIdx).header()).on('click', function (e) {
                    e.stopPropagation();
                });
            });


<?php } ?>
    }
    );
    $("#select_field").change(function () {
        waitingDialog.show('Loading...');
        if ($(this).val() == 'All Fields' || $(this).val() == 'intg_ccc.projek_id') {
            $("#search_input").attr('name', 'field_value');
            $("#projects").attr('name', 'field_value_x');

            if ($(this).val() == 'intg_projek.id') {
                $("#search_input").attr('type', 'number');
            } else if ($(this).val() == 'All Fields') {
                $("#search_input").attr('type', 'text');
            }

            $('.field_value_select').hide();
            $('.field_value_input').show();
            waitingDialog.hide();
        } else {
            $("#search_input").attr('name', 'field_value_x');
            $("#projects").attr('name', 'field_value');
            $('.field_value_input').hide();
            $('.field_value_select').show();
            $.getJSON("<?php echo site_url(SITE_AREA . '/projectmgmt/projek/search') ?>",
                    {
                        type: $(this).val(),
                        ajax: 'true'
                    }, function (j) {

                $("#projects").select2("val", "");
                console.log(j);
                var options = '<option selected></option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].rowid + '"> ' + j[i].selectedname + '</option>';
                }
                $("#projects").html(options);
                waitingDialog.hide();
            });
        }

    });



    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

    $(document).ready(function () {
        LoadDiv();
        $('#subNav-Header').remove();
    });
    $('#nav').on('click', function () {
        setTimeout(clicked, 500);
    });

    function clicked() {
        LoadDiv();
    }


    $(window).resize(function () {
        LoadDiv();
    });

    function LoadDiv() {
        var height = $('body').height() - $('header').height() - $('.navbar-header').height() - $('.header').height() - $('#footer').height() - $('.filter_area').height() - $('.filter_area').height();//tolok tinggi header ngan nav-bar header.
        var width = $('#content').width() - 40;

        $('#dvData').css({'height': height});
        $('#dvData').css({'width': width});
        $('#dvData').css({'max-width': width});
        $('#dvData').css({'overflow-x': 'auto'});

    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<

</script>
