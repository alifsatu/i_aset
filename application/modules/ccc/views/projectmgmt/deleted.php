<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 

echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>CCC</h4></div>
 <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

<? if ( $this->session->userdata('cccfield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('cccfield')?>"><?=$this->session->userdata('cccfield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_ccc')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>
</div><div class="col-md-3"><div class="input-group">
<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('cccfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('cccfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('cccfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

<?php echo form_close(); ?>
     <div class="table-responsive">   
  


	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('CCC.Projectmgmt.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_tarikh_terima_permohonan_sokongan_ccc') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_tarikh_terima_permohonan_sokongan_ccc&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_tarikh_terima_permohonan_sokongan_ccc') ? 'desc' : 'asc'); ?>'>
                    Tarikh Terima Permohonan Sokongan CCC</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_status_permohonan_sokongan_ccc') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_status_permohonan_sokongan_ccc&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_status_permohonan_sokongan_ccc') ? 'desc' : 'asc'); ?>'>
                    Status Permohonan Sokongan CCC</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_tarikh_sokongan_ccc_dikeluarkan') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_tarikh_sokongan_ccc_dikeluarkan&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_tarikh_sokongan_ccc_dikeluarkan') ? 'desc' : 'asc'); ?>'>
                    Tarikh Sokongan CCC Dikeluarkan</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_tarikh_mula_liabiliti') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_tarikh_mula_liabiliti&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_tarikh_mula_liabiliti') ? 'desc' : 'asc'); ?>'>
                    Tarikh Mula Liabiliti</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_tarikh_tamat_liabiliti') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_tarikh_tamat_liabiliti&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_tarikh_tamat_liabiliti') ? 'desc' : 'asc'); ?>'>
                    Tarikh Tamat Liabiliti</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_kaedah_jaminan') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_kaedah_jaminan&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_kaedah_jaminan') ? 'desc' : 'asc'); ?>'>
                    Kaedah Jaminan</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_jumlah_jaminan') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_jumlah_jaminan&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_jumlah_jaminan') ? 'desc' : 'asc'); ?>'>
                    Jumlah Jaminan</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_no_resit_bg') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_no_resit_bg&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_no_resit_bg') ? 'desc' : 'asc'); ?>'>
                    No. Resit atau BG</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_tarikh_tamat_jaminan') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_tarikh_tamat_jaminan&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_tarikh_tamat_jaminan') ? 'desc' : 'asc'); ?>'>
                    Tarikh Tamat Jaminan</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_nama_pemilik_syarikat') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_nama_pemilik_syarikat&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_nama_pemilik_syarikat') ? 'desc' : 'asc'); ?>'>
                    Nama Pemilik atau Syarikat</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_no_kad_pengenalan') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_no_kad_pengenalan&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_no_kad_pengenalan') ? 'desc' : 'asc'); ?>'>
                    No. Kad Pengenalan</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_no_syarikat') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_no_syarikat&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_no_syarikat') ? 'desc' : 'asc'); ?>'>
                    No. Syarikat</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_nama_bank') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_nama_bank&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_nama_bank') ? 'desc' : 'asc'); ?>'>
                    Nama Bank</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_no_akaun_bank') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_no_akaun_bank&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_no_akaun_bank') ? 'desc' : 'asc'); ?>'>
                    No Akaun Bank</a></th>
					<th <?php if ($this->input->get('sort_by') == 'ccc'.'_no_fail') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/ccc?sort_by=ccc_no_fail&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'ccc'.'_no_fail') ? 'desc' : 'asc'); ?>'>
                    No Fail</a></th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('CCC.Projectmgmt.Delete')) : ?>
				<tr>
					<td colspan="18">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
				<!--	<input type="submit" name="purge" class="btn btn-danger" value="<?php //echo lang('bf_action_purge') ?>" onclick="return confirm('<?php //echo lang('us_purge_del_confirm'); ?>')">-->
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) :  $no = $this->input->get('per_page')+1;?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('CCC.Projectmgmt.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('CCC.Projectmgmt.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/projectmgmt/ccc/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->tarikh_terima_permohonan_sokongan_ccc) ?></td>
				<?php else: ?>
				<td><?php echo $record->tarikh_terima_permohonan_sokongan_ccc ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->status_permohonan_sokongan_ccc?></td>
					<td><?php echo $record->tarikh_sokongan_ccc_dikeluarkan=="0000-00-00" ? "" : date("d/m/Y",strtotime($record->tarikh_sokongan_ccc_dikeluarkan)); ?></td>
					<td><?php echo $record->tarikh_mula_liabiliti=="0000-00-00" ? "" : date("d/m/Y",strtotime($record->tarikh_mula_liabiliti)); ?></td>
					<td><?php echo $record->tarikh_tamat_liabiliti=="0000-00-00" ? "" : date("d/m/Y",strtotime($record->tarikh_tamat_liabiliti)); ?></td>
				<td><?php echo $record->kaedah_jaminan?></td>
				<td><?php echo $record->jumlah_jaminan?></td>
				<td><?php echo $record->no_resit_bg?></td>
					<td><?php echo $record->tarikh_tamat_jaminan=="0000-00-00" ? "" : date("d/m/Y",strtotime($record->tarikh_tamat_jaminan)); ?></td>
				<td><?php echo $record->nama_pemilik_syarikat?></td>
				<td><?php echo $record->no_kad_pengenalan?></td>
				<td><?php echo $record->no_syarikat?></td>
				<td><?php echo $record->nama_bank?></td>
				<td><?php echo $record->no_akaun_bank?></td>
				<td><?php echo $record->no_fail?></td>
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
			<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
			<?php $no++; endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="18">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         
	<?php echo form_close(); ?>
  <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>