<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['fasa_manage']			= 'Manage';
$lang['fasa_edit']				= 'Edit';
$lang['fasa_true']				= 'True';
$lang['fasa_false']				= 'False';
$lang['fasa_create']			= 'Save';
$lang['fasa_list']				= 'List';
$lang['fasa_new']				= 'New';
$lang['fasa_edit_text']			= 'Edit this to suit your needs';
$lang['fasa_no_records']		= 'There aren\'t any fasa in the system.';
$lang['fasa_create_new']		= 'Create a new Fasa.';
$lang['fasa_create_success']	= 'Fasa successfully created.';
$lang['fasa_create_failure']	= 'There was a problem creating the fasa: ';
$lang['fasa_create_new_button']	= 'Create New Fasa';
$lang['fasa_invalid_id']		= 'Invalid Fasa ID.';
$lang['fasa_edit_success']		= 'Fasa successfully saved.';
$lang['fasa_edit_failure']		= 'There was a problem saving the fasa: ';
$lang['fasa_delete_success']	= 'record(s) successfully deleted.';

$lang['fasa_purged']	= 'record(s) successfully purged.';
$lang['fasa_success']	= 'record(s) successfully restored.';


$lang['fasa_delete_failure']	= 'We could not delete the record: ';
$lang['fasa_delete_error']		= 'You have not selected any records to delete.';
$lang['fasa_actions']			= 'Actions';
$lang['fasa_cancel']			= 'Cancel';
$lang['fasa_delete_record']		= 'Delete';
$lang['fasa_delete_confirm']	= 'Are you sure you want to delete this fasa?';
$lang['fasa_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['fasa_action_edit']		= 'Save';
$lang['fasa_action_create']		= 'Create';

// Activities
$lang['fasa_act_create_record']	= 'Created record with ID';
$lang['fasa_act_edit_record']	= 'Updated record with ID';
$lang['fasa_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['fasa_column_created']	= 'Created';
$lang['fasa_column_deleted']	= 'Deleted';
$lang['fasa_column_modified']	= 'Modified';
