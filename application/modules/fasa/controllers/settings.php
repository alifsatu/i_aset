<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Fasa.Settings.View');
		$this->load->model('fasa_model', null, true);
		$this->lang->load('fasa');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('fasa', 'fasa.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->fasa_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('fasa_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('fasa_delete_failure') . $this->fasa_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('fasafield',$this->input->post('select_field'));
$this->session->set_userdata('fasafvalue',$this->input->post('field_value'));
$this->session->set_userdata('fasafname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('fasafield');
$this->session->unset_userdata('fasafvalue');
$this->session->unset_userdata('fasafname');
break;
}

if ( $this->session->userdata('fasafield')!='') { $field=$this->session->userdata('fasafield'); }
else { $field = 'id'; }

if ( $this->session->userdata('fasafield') =='All Fields') 
{ 
 		
  $total = $this->fasa_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('fasafvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->fasa_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('fasafvalue'),'both')  
  ->count_all();
}

		//$records = $this->fasa_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('fasafield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('fasafvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->fasa_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('fasafvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->fasa_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('fasa', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Fasa');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_fasa SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('fasa_success'), 'success');
				}
				else
				{
					Template::set_message(lang('fasa_restored_error'). $this->fasa_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_fasa where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('fasa_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('fasafield',$this->input->post('select_field'));
$this->session->set_userdata('fasafvalue',$this->input->post('field_value'));
$this->session->set_userdata('fasafname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('fasafield');
$this->session->unset_userdata('fasafvalue');
$this->session->unset_userdata('fasafname');
break;
}


if ( $this->session->userdata('fasafield')!='') { $field=$this->session->userdata('fasafield'); }
else { $field = 'id'; }

if ( $this->session->userdata('fasafield') =='All Fields') 
{ 
 		
  $total = $this->fasa_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('fasafvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->fasa_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('fasafvalue'),'both')  
  ->count_all();
}

//$records = $this-fasa_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('fasafield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('fasafvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('fasafvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('fasafvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->fasa_model
->where('deleted','1')
->likes($field,$this->session->userdata('fasafvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->fasa_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('fasa', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Fasa');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Fasa object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Fasa.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_fasa())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fasa_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'fasa');

				Template::set_message(lang('fasa_create_success'), 'success');
				redirect(SITE_AREA .'/settings/fasa');
			}
			else
			{
				Template::set_message(lang('fasa_create_failure') . $this->fasa_model->error, 'error');
			}
		}
		Assets::add_module_js('fasa', 'fasa.js');

		Template::set('toolbar_title', lang('fasa_create') . ' Fasa');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Fasa data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('fasa_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/fasa');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Fasa.Settings.Edit');

			if ($this->save_fasa('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fasa_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'fasa');

				Template::set_message(lang('fasa_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('fasa_edit_failure') . $this->fasa_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Fasa.Settings.Delete');

			if ($this->fasa_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('fasa_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'fasa');

				Template::set_message(lang('fasa_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/fasa');
			}
			else
			{
				Template::set_message(lang('fasa_delete_failure') . $this->fasa_model->error, 'error');
			}
		}
		Template::set('fasa', $this->fasa_model->find($id));
		Template::set('toolbar_title', lang('fasa_edit') .' Fasa');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_fasa SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('fasa_success'), 'success');
											redirect(SITE_AREA .'/settings/fasa/deleted');
				}
				else
				{
					
					Template::set_message(lang('fasa_error') . $this->fasa_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_fasa where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('fasa_purged'), 'success');
					redirect(SITE_AREA .'/settings/fasa/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('fasa_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/fasa');
		}

		
		
		
		Template::set('fasa', $this->fasa_model->find($id));
		
		Assets::add_module_js('fasa', 'fasa.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Fasa');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_fasa($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['nama']        = $this->input->post('fasa_nama');

		if ($type == 'insert')
		{
			$id = $this->fasa_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->fasa_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}