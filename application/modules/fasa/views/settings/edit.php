<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($fasa))
{
	$fasa = (array) $fasa;
}
$id = isset($fasa['id']) ? $fasa['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Fasa</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Fasa'. lang('bf_form_label_required'), 'fasa_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='fasa_nama' class='input-sm input-s  form-control' type='text' name='fasa_nama' maxlength="255" value="<?php echo set_value('fasa_nama', isset($fasa['nama']) ? $fasa['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('fasa_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/fasa', lang('fasa_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Fasa.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('fasa_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('fasa_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>