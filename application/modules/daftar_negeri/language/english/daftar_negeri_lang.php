<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_negeri_manage']			= 'Manage';
$lang['daftar_negeri_edit']				= 'Edit';
$lang['daftar_negeri_true']				= 'True';
$lang['daftar_negeri_false']				= 'False';
$lang['daftar_negeri_create']			= 'Save';
$lang['daftar_negeri_list']				= 'List';
$lang['daftar_negeri_new']				= 'New';
$lang['daftar_negeri_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_negeri_no_records']		= 'There aren\'t any daftar_negeri in the system.';
$lang['daftar_negeri_create_new']		= 'Create a new Daftar Negeri.';
$lang['daftar_negeri_create_success']	= 'Daftar Negeri successfully created.';
$lang['daftar_negeri_create_failure']	= 'There was a problem creating the daftar_negeri: ';
$lang['daftar_negeri_create_new_button']	= 'Create New Daftar Negeri';
$lang['daftar_negeri_invalid_id']		= 'Invalid Daftar Negeri ID.';
$lang['daftar_negeri_edit_success']		= 'Daftar Negeri successfully saved.';
$lang['daftar_negeri_edit_failure']		= 'There was a problem saving the daftar_negeri: ';
$lang['daftar_negeri_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_negeri_purged']	= 'record(s) successfully purged.';
$lang['daftar_negeri_success']	= 'record(s) successfully restored.';


$lang['daftar_negeri_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_negeri_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_negeri_actions']			= 'Actions';
$lang['daftar_negeri_cancel']			= 'Cancel';
$lang['daftar_negeri_delete_record']		= 'Delete';
$lang['daftar_negeri_delete_confirm']	= 'Are you sure you want to delete this daftar_negeri?';
$lang['daftar_negeri_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_negeri_action_edit']		= 'Save';
$lang['daftar_negeri_action_create']		= 'Create';

// Activities
$lang['daftar_negeri_act_create_record']	= 'Created record with ID';
$lang['daftar_negeri_act_edit_record']	= 'Updated record with ID';
$lang['daftar_negeri_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_negeri_column_created']	= 'Created';
$lang['daftar_negeri_column_deleted']	= 'Deleted';
$lang['daftar_negeri_column_modified']	= 'Modified';
