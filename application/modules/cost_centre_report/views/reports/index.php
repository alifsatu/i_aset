<?php
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="row-fluid">
    <section class="panel panel-default">
        <div class="panel-body">
            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>

            <div class="row">
                <div class="col-md-1">
                    <h4>SBU Report</h4>
                </div>
                <div class="col-md-2">   
                    <!--<a  class="btn  btn-icon m-l "  title="Excel">Export to Excel<span class="m-l fa  fa-file-excel-o" style="color:#093; font-size:18px"></span></a>-->
                    <a id="excel" href="<?php e(SITE_URL('admin/reports/cost_centre_report/export?from=' . $from . '&to=' . $to)) ?>" onclick="geturlandhours()" class="btn btn-success"><span class="glyphicon glyphicon-save"></span> Export to Excel</a>


                </div>
                <div class="col-md-2">
                    <input type="text" id="search" class="input  form-control" placeholder="Filter by staff name">

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="radio-inline" for="radios-hours">
                            <input name="hours" type="radio" value="hours" id="radios-hours" checked="checked"/> 
                            Hours
                        </label> 
                        <label class="radio-inline" for="radios-percentage">
                            <input name="hours" type="radio" value="percentage" id="radios-percentage" /> 
                            Percentage
                        </label>

                    </div>

                </div>
                <div class="col-md-4">


                    <input type="text" class="form-control datepicker-input  pull-left m-r" style="width:100px" placeholder="From Date" name="fromdate" id="fromdate" value="<?= $this->input->post('fromdate') != "" ? date('d-m-Y', strtotime($this->input->post('fromdate'))) : "" ?>" data-date-format="dd-mm-yyyy" autocomplete="off">
                    <input type="text" class="form-control datepicker-input pull-left" style="width:100px" placeholder="To Date" name="todate" id="todate" value="<?= $this->input->post('todate') != "" ? date('d-m-Y', strtotime($this->input->post('todate'))) : "" ?>" data-date-format="dd-mm-yyyy" autocomplete="off">
                    <span class="input-group-btn pull-left">                                    
                        <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
                        <a href="<?php e(SITE_URL('admin/reports/cost_centre_report')) ?>" class="btn btn-primary btn-icon"  title="Reset"><span class="fa  fa-refresh"></span></a>
                        <?php
                        $from = $this->input->post('fromdate') == "" ? "" : $this->input->post('fromdate');
                        $to = $this->input->post('todate') == "" ? "" : $this->input->post('todate');
                        ?>
                    </span>


                </div>

            </div>
            <?php echo form_close(); ?> 




            <div class="table-scrollable innerWrapper m-t" id="dvData" style="height:400px;">

                <table id="tstable" class="table-hover"  cellpadding="5" cellspacing="2" border="1" style="border-collapse:collapse; border:#D7D7D7 solid 1px">
                    <thead>
                        <tr>
                            <?php $cc = $this->db->get_where('intg_cost_centre', array('deleted' => 0))->result(); 
                        $cc_count = $this->db->get_where('intg_cost_centre', array('deleted' => 0))->num_rows(); ?>

                            <th  rowspan="2">#</th>                   
                            <th  rowspan="2">Name</th>
                            <th colspan="<?=$cc_count+1?>">Cost Centre</th>
                            <th>&nbsp;</th>
                        </tr>
                        <tr >
                            <th>User Cost Centre</th>
                            <?php foreach ($cc as $rec) { ?>
                                <th><?php e($rec->cost_centre); ?></th>

                            <?php } ?>
                            <th>Total Hours</th>
                        </tr>
                    </thead>

                    <tbody id="bodycontent">
                        <?php
                        $filter_string = array();

                        if ($this->input->post('fromdate')) {
                            array_push($filter_string, 'itd.tsd_date >= "' . date('Y-m-d', strtotime($this->input->post('fromdate'))) . '"');
                        }

                        if ($this->input->post('todate')) {
                            array_push($filter_string, 'itd.tsd_date <= "' . date('Y-m-d', strtotime($this->input->post('todate'))) . '"');
                        }

                        $no = 1;
                        foreach ($this->db->get_where('intg_users', array('id !=' => '1'))->result() as $user) {
                            ?>			
                            <tr>					
                                <td><?= $no ?></td>
                                <td><?php e($user->display_name) ?></td>
                                <td align="center"><?php e($this->db->query("select cost_centre from intg_cost_centre where id = " . $user->cost_centre_id . " and deleted=0")->row()->cost_centre) ?></td>
                                <?php
                                $cct = 0;
                                foreach ($cc as $rec) {
                                    ?>
                                    <td align="center"><?php
                                        if ($filter_string) {
                                            $ccv = $this->db->query("select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.final_status = 'Yes' and it.uid = " . $user->id . " and it.deleted = 0 and it.id = itd.tid and itd.pid = p.id and p.cost_centre_id = " . $rec->id . "  AND " . implode(' AND ', $filter_string) . "")->row()->thours;
                                        } else {

                                            $ccv = $this->db->query("select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.final_status = 'Yes' and it.uid = " . $user->id . " and it.deleted = 0 and it.id = itd.tid and itd.pid = p.id and p.cost_centre_id = " . $rec->id . "")->row()->thours;
                                        }
                                        echo empty($ccv) ? "" : "<span class='hours row" . $user->id . "' style='display:block'>" . number_format($ccv, 2, '.', '') . "</span><span class='m-l perc" . $user->id . " percentage' style='display:none'></span>";
                                        $cost_centre_total{$rec->id} = $cost_centre_total{$rec->id} + $ccv;
                                        ?>
                                    </td>

                                    <?php
                                    $cct = $cct + $ccv;
                                }
                                ?>
                                <td align="center"> <span id="row<?= $user->id ?>"><?= number_format($cct, 2, '.', '') ?></span></td>

                            </tr>

                        <script>
                            $(".row<?= $user->id ?>").each(function () {
                                var a = parseFloat($(this).text());
                                var b = parseFloat(<?= $cct ?>);
                                var c = (a / b * 100).toFixed(2);
                                $(this).next().text(c + "%");
                            });
                        </script>
                        <?php
                        $no++;
                        $ccv = 0;
                    }
                    ?>
                    </tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td><strong>Total Hours</strong></td>
                        <td align="center">&nbsp;</td>
                        <?php foreach ($cc as $reco) { ?>
                            <td align="center"><?php e(number_format($cost_centre_total{$reco->id}, 2, '.', '')); ?></td>
                        <?php } ?>
                        <td align="center">&nbsp;</td>
                    </tr>
                </table>

            </div>
        </div>



    </section>
</div>

<script>

    $(document).ready(function () {

        $("input[name='hours']").on("click", function () {
            var a = $(this).val();
            if (a == "hours") {
                $(".hours").hide();
                $(".hours").show();
                $(".percentage").hide();
            }
            if (a == "percentage") {
                $(".percentage").show();
                $(".hours").hide();
            }
        });



        $('#fromdate').datepicker().on('changeDate', function () {
            $('#fromdate').datepicker('hide');

        });
        $('#todate').datepicker().on('changeDate', function () {
            $('#todate').datepicker('hide');
        });


        $('#subNav-Header').remove();


    });

    function geturlandhours()
    {
        event.preventDefault();

        var hour = $('input[name=hours]:checked').val();
        var url = $("#excel").attr('href') + "&hourstype=" + hour;

        window.location = url;
    }


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
    $('#nav').on('click', function () {
        setTimeout(clicked, 500);
    });

    function clicked() {
        LoadDiv(true);
    }

    $(window).resize(function () {
        LoadDiv(true);
    });

    function LoadDiv(IsResize) {
        //ponin den nak kiro size ning wehhhhhhhhhhh
        console.log("bodohhh content H : " + $('#content').height());
        console.log("bodohhh content W : " + $('#content').width());


        console.log("body h : " + $('body').height());
        console.log("header h : " + $('header').height());
        console.log(".navbar-header h : " + $('.navbar-header').height());

        console.log("body w : " + $('body').width());
        console.log("nav w : " + $('#nav').width());

        var height = $('body').height() - $('header').height() - $('.navbar-header').height() - $('header').height();//tolok tinggi header ngan nav-bar header.
//        var width = $('body').width()-$('#nav').width()-30;//tolok size nav menu, dpt width container, tolok 30 utk padding 15kiri 15 kanang, beyehhh
        var width = $('#content').width() - 40;

        console.log("calculated h : " + height);
        console.log("calculated w : " + width);

        $('#dvData').css({'height': height});
        $('#dvData').css({'width': width});

    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

</script>
<script>
    $(window).load(function () {
        $("#search").keyup(function () {
            var value = this.value.toLowerCase().trim();

            $("#tstable tr").each(function (index) {
                if (!index)
                    return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });
    });//]]> 
</script>