<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Cost_Centre_Report.Reports.View');
		$this->lang->load('cost_centre_report');
		
		Template::set_block('sub_nav', 'reports/_sub_nav');

		Assets::add_module_js('cost_centre_report', 'cost_centre_report.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		Assets::add_js(Template::theme_url('js/datepicker/bootstrap-datepicker.js'));
		Assets::add_css(Template::theme_url('js/datepicker/datepicker.css'));

		Template::set('toolbar_title', 'Manage Cost Centre Report');
		Template::render();
	}

	//--------------------------------------------------------------------



public function export()
{
	$this->load->library('session');
	$this->load->library('excel');
	$this->excel->setActiveSheetIndex(0);
	//name the worksheet


$this->excel->getActiveSheet()->setTitle('SBU Report');
$this->excel->getActiveSheet()->setCellValue('A1', '#');
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B1', 'Name');
$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);

$cc = $this->db->get_where('intg_cost_centre',array('deleted'=>0))->result();
$cell = "C";

foreach ( $cc as $rec ) {
	
$this->excel->getActiveSheet()->setCellValue($cell.'1', $rec->cost_centre);
$this->excel->getActiveSheet()->getStyle($cell.'1')->getFont()->setBold(true);	
$cell++;

}
$this->excel->getActiveSheet()->setCellValue($cell++.'1', "Total Hours");

$no =1; 
$row = 2;
$searchbydate ="";
$rangedate = "AllTime";

if(is_null($_GET['from'])){
    $from = "";
}else{
    $from = $_GET['from'];
}

if(is_null($_GET['to'])){
    $to = "";
}else{
    $to = $_GET['to'];
}

if ($from !="" && $to != "" ) {

    $searchbydate = 'AND itd.tsd_date >= "'.date('Y-m-d', strtotime($this->input->get('from'))).'" AND itd.tsd_date <= "'.date('Y-m-d', strtotime($this->input->get('to'))).'"';	
    $rangedate = $this->input->get('from')."~".$this->input->get('to');
	
}else if($from !="" && $to == ""){
    $searchbydate = 'AND itd.tsd_date >= "'.date('Y-m-d', strtotime($this->input->get('from'))).'"';	
    $rangedate = "FROM".$this->input->get('from');
}else if($from =="" && $to != ""){
    $searchbydate = 'AND itd.tsd_date <= "'.date('Y-m-d', strtotime($this->input->get('to'))).'"';	
    $rangedate = "TO".$this->input->get('to');
}
//else {
//	
//$searchbydate = 'AND itd.tsd_date >= "'.date('Y-m-01').'" AND itd.tsd_date <= "'.date('Y-m-t').'"';	
//$rangedate = date('01-m-Y')."~".date('t-m-Y');	
//}


foreach ( $this->db->get_where('intg_users',array('id !=' => '1'))->result() as $user ) {

$this->excel->getActiveSheet()->setCellValue('A'.$row, $no);
$this->excel->getActiveSheet()->setCellValue('B'.$row, $user->display_name);

 $cct=0; 
 $ncell = "C";
 
 if ( $_GET['hourstype'] == "hours" ) {
	 
	  $hrs = "hours";
 
 foreach ( $cc as $rec ) {
	
					 $ccv =  $this->db->query("select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.uid = ".$user->id." and it.deleted = 0 and it.final_status='Yes' and it.id = itd.tid and itd.pid = p.id and p.cost_centre_id = ".$rec->id." $searchbydate")->row()->thours; 
//					 echo "select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.uid = ".$user->id." and it.deleted = 0 and it.final_status='Yes' and it.id = itd.tid and itd.pid = p.id and p.cost_centre_id = ".$rec->id." $searchbydate";
					
					  
				 
				 $this->excel->getActiveSheet()->setCellValue($ncell.$row, $ccv);
				 
				 $ncell++;			 
				$cct = $cct + $ccv; 
				 $cost_centre_total{$rec->id} = $cost_centre_total{$rec->id} + $ccv;  
 }
 }
// echo $this->db->last_query();
// die();
 
 if ( $_GET['hourstype'] == "percentage" ) {
	 
	 $hrs = "perc";
	 
	 $ccvTOTAL =  $this->db->query("select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.uid = ".$user->id." and it.deleted = 0 and it.final_status='Yes' and it.id = itd.tid and itd.pid = p.id  $searchbydate")->row()->thours; 
					 
 
 foreach ( $cc as $rec ) {
	
					 $ccv =  $this->db->query("select sum(tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd, intg_projects p where it.status = 'Submitted' and it.uid = ".$user->id." and it.deleted = 0 and it.final_status='Yes' and it.id = itd.tid and itd.pid = p.id and p.cost_centre_id = ".$rec->id." $searchbydate")->row()->thours; 
					 
										  
				 $this->excel->getActiveSheet()->setCellValue($ncell.$row, $ccv/$ccvTOTAL * 100 == 0 ? "" : number_format($ccv/$ccvTOTAL * 100,2));
				 
				 $ncell++;			 
				$cct = $cct + $ccv; 
				 $cost_centre_total{$rec->id} = $cost_centre_total{$rec->id} + $ccv;  
 }
 }
 
 //totalhours
 $this->excel->getActiveSheet()->setCellValue($ncell.$row, $cct);
 $row++; $no++;
 
}

$this->excel->getActiveSheet()->setCellValue('A'.$row++, "");
$this->excel->getActiveSheet()->setCellValue('B'.$row, "Total Hours");
 $ncell = "C";
 foreach ( $cc as $reco ) {
  $this->excel->getActiveSheet()->setCellValue($ncell.$row, number_format($cost_centre_total{$reco->id},2));
				 $ncell++;	
 
 }

 


$filename='SBUReport_'.$rangedate.'_'.$hrs.'.XLSX'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
 ob_end_clean();
$objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');

	
		
		//Template::render();
	
}



}