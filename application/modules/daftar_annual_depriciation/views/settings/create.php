<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_annual_depriciation))
{
	$daftar_annual_depriciation = (array) $daftar_annual_depriciation;
}
$id = isset($daftar_annual_depriciation['id']) ? $daftar_annual_depriciation['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Annual Depriciation</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_annual_depriciation_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_annual_depriciation_name' class='input-sm input-s  form-control' type='text' name='daftar_annual_depriciation_name' maxlength="255" value="<?php echo set_value('daftar_annual_depriciation_name', isset($daftar_annual_depriciation['name']) ? $daftar_annual_depriciation['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_annual_depriciation_classification_group_id', $options, set_value('daftar_annual_depriciation_classification_group_id', isset($daftar_annual_depriciation['classification_group_id']) ? $daftar_annual_depriciation['classification_group_id'] : ''), 'Classification Group'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_annual_depriciation_classification_id', $options, set_value('daftar_annual_depriciation_classification_id', isset($daftar_annual_depriciation['classification_id']) ? $daftar_annual_depriciation['classification_id'] : ''), 'Classification Id'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('value') ? 'error' : ''; ?>">
				<?php echo form_label('Value'. lang('bf_form_label_required'), 'daftar_annual_depriciation_value', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_annual_depriciation_value' class='input-sm input-s  form-control' type='text' name='daftar_annual_depriciation_value' maxlength="30" value="<?php echo set_value('daftar_annual_depriciation_value', isset($daftar_annual_depriciation['value']) ? $daftar_annual_depriciation['value'] : '');?>" />
					<span class='help-inline'><?php echo form_error('value'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_annual_depriciation_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/daftar_annual_depriciation', lang('daftar_annual_depriciation_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>