<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_annual_depriciation_manage']			= 'Manage';
$lang['daftar_annual_depriciation_edit']				= 'Edit';
$lang['daftar_annual_depriciation_true']				= 'True';
$lang['daftar_annual_depriciation_false']				= 'False';
$lang['daftar_annual_depriciation_create']			= 'Save';
$lang['daftar_annual_depriciation_list']				= 'List';
$lang['daftar_annual_depriciation_new']				= 'New';
$lang['daftar_annual_depriciation_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_annual_depriciation_no_records']		= 'There aren\'t any daftar_annual_depriciation in the system.';
$lang['daftar_annual_depriciation_create_new']		= 'Create a new Daftar Annual Depriciation.';
$lang['daftar_annual_depriciation_create_success']	= 'Daftar Annual Depriciation successfully created.';
$lang['daftar_annual_depriciation_create_failure']	= 'There was a problem creating the daftar_annual_depriciation: ';
$lang['daftar_annual_depriciation_create_new_button']	= 'Create New Daftar Annual Depriciation';
$lang['daftar_annual_depriciation_invalid_id']		= 'Invalid Daftar Annual Depriciation ID.';
$lang['daftar_annual_depriciation_edit_success']		= 'Daftar Annual Depriciation successfully saved.';
$lang['daftar_annual_depriciation_edit_failure']		= 'There was a problem saving the daftar_annual_depriciation: ';
$lang['daftar_annual_depriciation_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_annual_depriciation_purged']	= 'record(s) successfully purged.';
$lang['daftar_annual_depriciation_success']	= 'record(s) successfully restored.';


$lang['daftar_annual_depriciation_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_annual_depriciation_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_annual_depriciation_actions']			= 'Actions';
$lang['daftar_annual_depriciation_cancel']			= 'Cancel';
$lang['daftar_annual_depriciation_delete_record']		= 'Delete';
$lang['daftar_annual_depriciation_delete_confirm']	= 'Are you sure you want to delete this daftar_annual_depriciation?';
$lang['daftar_annual_depriciation_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_annual_depriciation_action_edit']		= 'Save';
$lang['daftar_annual_depriciation_action_create']		= 'Create';

// Activities
$lang['daftar_annual_depriciation_act_create_record']	= 'Created record with ID';
$lang['daftar_annual_depriciation_act_edit_record']	= 'Updated record with ID';
$lang['daftar_annual_depriciation_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_annual_depriciation_column_created']	= 'Created';
$lang['daftar_annual_depriciation_column_deleted']	= 'Deleted';
$lang['daftar_annual_depriciation_column_modified']	= 'Modified';
