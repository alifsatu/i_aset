<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($sbu))
{
	$sbu = (array) $sbu;
}
$id = isset($sbu['id']) ? $sbu['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">SBU</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('sbu_company_id', $options, set_value('sbu_company_id', isset($sbu['company_id']) ? $sbu['company_id'] : ''), 'Company'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('SBU Name'. lang('bf_form_label_required'), 'sbu_sbu_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='sbu_sbu_name' class='input-sm input-s  form-control' type='text' name='sbu_sbu_name' maxlength="255" value="<?php echo set_value('sbu_sbu_name', isset($sbu['sbu_name']) ? $sbu['sbu_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('program_id') ? 'error' : ''; ?>">
				<?php echo form_label('Program_name'. lang('bf_form_label_required'), 'sbu_program_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'sbu_program_id', 'id' => 'sbu_program_id', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('sbu_program_id', isset($sbu['program_id']) ? $sbu['program_id'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('program_id'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('descrition') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'sbu_descrition', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'sbu_descrition', 'id' => 'sbu_descrition', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('sbu_descrition', isset($sbu['descrition']) ? $sbu['descrition'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('descrition'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created by', 'sbu_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='sbu_created_by' class='input-sm input-s  form-control' type='text' name='sbu_created_by' maxlength="255" value="<?php echo set_value('sbu_created_by', isset($sbu['created_by']) ? $sbu['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id_ori') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name', 'sbu_company_id_ori', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='sbu_company_id_ori' class='input-sm input-s  form-control' type='text' name='sbu_company_id_ori' maxlength="255" value="<?php echo set_value('sbu_company_id_ori', isset($sbu['company_id_ori']) ? $sbu['company_id_ori'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id_ori'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/sbu', lang('sbu_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('SBU.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('sbu_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>