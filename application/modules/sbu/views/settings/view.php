<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($sbu))
{
	$sbu = (array) $sbu;
}
$id = isset($sbu['id']) ? $sbu['id'] : '';
 
?>
 <div class="row">
 <div class="col-sm-12">
     
     <?php echo send_email_button($this); ?>   
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">SBU</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
			/* 	$options = array(
					255 => 255,
				);

				echo form_dropdown('sbu_company_id', $options, set_value('sbu_company_id', isset($sbu['company_id']) ? $sbu['company_id'] : ''), 'Company'. lang('bf_form_label_required'));
			 */
			 $companies = $this->db->query('SELECT * FROM intg_companies WHERE id = '.$sbu['company_id'].' ORDER BY name ')->result();
			 foreach($companies as $c) {
				 $company = $c->name;
			 }
			?>
			 <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('Company', 'sbu_company_id', array('class' => 'control-label') ); ?> : <? echo $company; ?>
				<div class='controls'>
					 <!--<select name="sbu_company_id" id="sbu_company_id" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
             <option></option>
            <? foreach($companies as $c) { ?>
                    
            
             <option value=<?=$c->id?> <? if($sbu['company_id']==$c->id) { echo "selected"; }?>><b><?=$c->name?></b></option><? }  ?>
            </select>-->
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('SBU Name', 'sbu_sbu_name', array('class' => 'control-label') ); ?> : <? echo $sbu['sbu_name']; ?>
				<div class='controls'>
					<!--<input id='sbu_sbu_name' class='input-sm input-s  form-control' type='text' name='sbu_sbu_name' maxlength="255" value="<?php echo set_value('sbu_sbu_name', isset($sbu['sbu_name']) ? $sbu['sbu_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>-->
				</div>
			</div>

			<!--<div class="form-group <?php echo form_error('program_id') ? 'error' : ''; ?>">
				<?php echo form_label('Program_name', 'sbu_program_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'sbu_program_id', 'id' => 'sbu_program_id', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('sbu_program_id', isset($sbu['program_id']) ? $sbu['program_id'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('program_id'); ?></span>
				</div>
			</div>-->
			<?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_programs WHERE id = '.$sbu['program_id'].' ')->result();
foreach($prgm as $pn) 
{
	$p_name = $pn->program_name;
}	
	?>
			 <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
				<?php echo form_label('Program name', 'sbu_program_id', array('class' => 'control-label') ); ?> :  <? echo $p_name; ?>
				<div class='controls'>
					 <!---<select name="sbu_program_id" id="sbu_program_id" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
             <option></option>
            <? foreach($prgm as $pn) { ?>
                    
            
             <option value=<?=$pn->id?> <? if($sbu['program_id']==$pn->id) { echo "selected"; }?>><b><?=$pn->program_name?></b></option><? }  ?>
            </select>-->
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('descrition') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'sbu_descrition', array('class' => 'control-label') ); ?> :  <? echo $sbu['descrition']; ?>
				<div class='controls'>
					<?php //echo form_textarea( array( 'name' => 'sbu_descrition', 'id' => 'sbu_descrition', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('sbu_descrition', isset($sbu['descrition']) ? $sbu['descrition'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('descrition'); ?></span>
				</div>
			</div>

			

			
		</fieldset>
    <?php echo form_close(); ?>
</div>