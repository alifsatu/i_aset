<?php

$num_columns	= 9;
$can_delete	= $this->auth->has_permission('SBU.Settings.Delete');
$can_edit		= $this->auth->has_permission('SBU.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>SBU</h4></div>
  <div class="col-md-3">
	
<select name="select_field" id="select_field" class="selecta form-control m-b" onchange="setfname()">
<option></option>
<option <?=$this->session->userdata('sbufield')=='All Fields' ? 'selected' : ''?>>All Fields</option>
<option value="subsidiary_name" <?=$this->session->userdata('sbufield')=='subsidiary_name' ? 'selected' : ''?>>Subsidiary Name</option>
<option value="sbu_name" <?=$this->session->userdata('sbufield')=='sbu_name' ? 'selected' : ''?>>SBU Name</option>
<option value="program_name" <?=$this->session->userdata('sbufield')=='program_name' ? 'selected' : ''?>>Program Name</option>
<option value="description" <?=$this->session->userdata('sbufield')=='description' ? 'selected' : ''?>>Description</option>

</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('sbufvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('sbufname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('sbufield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive m-t">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th> 
                    
                   <th<?php if ($this->input->get('sort_by') == '_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                    Subsidiary Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_sbu_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=sbu_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sbu_name') ? 'desc' : 'asc'); ?>'>
                    SBU Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_program_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=program_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'program_name') ? 'desc' : 'asc'); ?>'>
                    Program Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_descrition') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=descrition&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'descrition') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
                    
                  <!-- <th<?php if ($this->input->get('sort_by') == '_created_by') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=created_by&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'created_by') ? 'desc' : 'asc'); ?>'>
                    Created by</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_company_id_ori') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/sbu?sort_by=company_id_ori&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'company_id_ori') ? 'desc' : 'asc'); ?>'>
                    Company Name</a></th>-->
					<th>View</th>
					<th><?php echo lang("sbu_column_created"); ?></th>
					<th><?php echo lang("sbu_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('sbu_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->sbu_id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/sbu/edit/' . $record->sbu_id, $record->subsidiary_name.'<i class="m-l fa fa-pencil"></i>' ); ?></td>
				<?php else : ?>
					<td><?php e($record->subsidiary_name); ?></td>
				<?php endif; ?>
					<td><?php e($record->sbu_name) ?></td>
					<td><?php e($record->program_name) ?></td>
					<td><?php e($record->descrition) ?></td>
					<!--<td><?php e($record->created_by) ?></td>
					<td><?php e($record->company_id_ori) ?></td>-->
					<td><a href="sbu/view/<?=$record->sbu_id;?>" class="btn" title="view"><i class="fa fa-eye" style="color:#000"></i></a></td>
					<td><?php echo date("d/m/Y h:i:s",strtotime($record->sbu_created)); ?></td>
			<td><?php echo $record->sbu_modified=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->sbu_modified)); ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
