<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['sbu_manage']			= 'Manage';
$lang['sbu_edit']				= 'Edit';
$lang['sbu_true']				= 'True';
$lang['sbu_false']				= 'False';
$lang['sbu_create']			= 'Save';
$lang['sbu_list']				= 'List';
$lang['sbu_new']				= 'New';
$lang['sbu_edit_text']			= 'Edit this to suit your needs';
$lang['sbu_no_records']		= 'There aren\'t any sbu in the system.';
$lang['sbu_create_new']		= 'Create a new SBU.';
$lang['sbu_create_success']	= 'SBU successfully created.';
$lang['sbu_create_failure']	= 'There was a problem creating the sbu: ';
$lang['sbu_create_new_button']	= 'Create New SBU';
$lang['sbu_invalid_id']		= 'Invalid SBU ID.';
$lang['sbu_edit_success']		= 'SBU successfully saved.';
$lang['sbu_edit_failure']		= 'There was a problem saving the sbu: ';
$lang['sbu_delete_success']	= 'record(s) successfully deleted.';

$lang['sbu_purged']	= 'record(s) successfully purged.';
$lang['sbu_success']	= 'record(s) successfully restored.';


$lang['sbu_delete_failure']	= 'We could not delete the record: ';
$lang['sbu_delete_error']		= 'You have not selected any records to delete.';
$lang['sbu_actions']			= 'Actions';
$lang['sbu_cancel']			= 'Cancel';
$lang['sbu_delete_record']		= 'Delete';
$lang['sbu_delete_confirm']	= 'Are you sure you want to delete this sbu?';
$lang['sbu_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['sbu_action_edit']		= 'Save';
$lang['sbu_action_create']		= 'Create';

// Activities
$lang['sbu_act_create_record']	= 'Created record with ID';
$lang['sbu_act_edit_record']	= 'Updated record with ID';
$lang['sbu_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['sbu_column_created']	= 'Created';
$lang['sbu_column_deleted']	= 'Deleted';
$lang['sbu_column_modified']	= 'Modified';
