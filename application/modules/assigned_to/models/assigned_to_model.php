<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Assigned_to_model extends BF_Model {

	protected $table_name	= "assigned_to";
	protected $key			= "id";
	protected $soft_deletes	= false;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= false;
	protected $set_modified = false;

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array('notify');
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "item_id",
			"label"		=> "Item ID",
			"rules"		=> "max_length[11]|required"
		),
		array(
			"field"		=> "user_id",
			"label"		=> "User ID",
			"rules"		=> "max_length[11]|required"
		),
		array(
			"field"		=> "item_type",
			"label"		=> "Item Type",
			"rules"		=> "max_length[30]|required"
		),
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

	public function assign_user($item_id, $item_type, $user_ids)
	{
		if(!$user_ids)
			return false;

		$assigned_data = $this->select('user_id')->where(array('item_id' => $item_id, 'item_type' => $item_type))->find_all();
		$remove_users = array();
		if($assigned_data) {

			$assigned_users = array_map(function($data) {
				return $data->user_id;
			}, $assigned_data);

			$existing_users = array_values(array_intersect($assigned_users, $user_ids));
			
			$remove_users = array_filter($assigned_users, function($user_id) use($existing_users) {
				return !in_array($user_id, $existing_users);
			});

			$user_ids = array_filter($user_ids, function($user_id) use($existing_users) {
				return !in_array($user_id, $existing_users);
			});
		}

		if($remove_users)
			$this->where_in('user_id', $remove_users)->delete_where(array(
				'item_id' => $item_id,
				'item_type' => $item_type,
			));
		
		if(is_array($user_ids)) {
			foreach($user_ids as $user_id) {
				if(!$this->count_by(array(
					'item_id' => $item_id,
					'item_type' => $item_type,
					'user_id' => $user_id)))
				$this->insert(array(
					'item_id' => $item_id,
					'item_type' => $item_type,
					'user_id' => $user_id));
			}
		} else if (is_string($user_ids)) {
			$this->insert(array(
					'item_id' => $item_id,
					'item_type' => $item_type,
					'user_id' => $user_ids));
		} else {
			return false;
		}
		return true;
	}
	
	public function get_users($item_id, $item_type) 
	{
		$this->load->model('users/user_model',null,true);
		$assigned = $this->where(array('item_id' => $item_id, 'item_type' => $item_type))->find_all();
		if(!$assigned)
			return array();
		$user_model = $this->user_model;
		return array_map(function($ass_user)use($user_model){
			return $user_model->find($ass_user->user_id);
		}, $assigned);
		
	}
	
	public function notify($id)
	{
		$title = 'You have been assigned';
		$content = 'You\'ve been assigned to the following item';
		$link = '';
		$class = 'fa-envelope-o';
		$module = '';
		
		$item_data = $this->find($id);
		
		$user = $this->user_model->find($item_data->user_id);
		
		switch($item_data->item_type) {
			case 'project':
				$this->load->model('projects/projects_model',null,true);
				$item = $this->projects_model->get_projects($item_data->item_id);
				if($item)
					$item = $item[0];
				$title .= ' to a project';
				$content = 'You\'ve been assigned to the following project, '.$item->project_name .', by '.$item->creator->display_name;
				$link = site_url('admin/projectmgmt/projects');
				$class = 'fa-briefcase';
				$module = 'Project Management';
				break;
			case 'task':
				$this->load->model('tasks/tasks_model',null,true);
				$item = $this->tasks_model->get_tasks($item_data->item_id);
				if($item)
					$item = $item[0];
				$title .= ' to a task';
				$content = 'You\'ve been assigned to the following task, '.$item->task_name .', by '.$item->creator->display_name;
				$link = site_url('admin/projectmgmt/tasks');
				$class = 'fa-tasks';
				$module = 'Project Management';
				break;
			case 'rm_project':
				$this->load->model('rm__project/rm__project_model',null,true);
				$item = $this->rm__project_model->get_projects($item_data->item_id);
				if($item)
					$item = $item[0];
				$title .= ' to a roster project';
				$content = 'You\'ve been assigned to the following roster project, '.$item->project_name .', by '.$item->creator->display_name;
				$link = site_url('admin/rostermanagment/rm__project');
				$class = 'fa-calendar-o';
				$module = 'Roster Management';
				break;
			case 'rm_task':
				$this->load->model('rm_tasks/rm_tasks_model',null,true);
				$item = $this->rm_tasks_model->get_tasks($item_data->item_id);
				if($item)
					$item = $item[0];
				$title .= ' to a roster task';
				$content = 'You\'ve been assigned to the following roster task, '.$item->task_name .', by '.$item->creator->display_name;
				$link = site_url('admin/rostermanagment/rm_tasks');
				$class = 'fa-calendar';
				$module = 'Roster Management';
				break;
		}
		
		$this->load->model('notifications/notifications_model',null,true);
		
		$this->notifications_model->insert(array(
			
			'user_id' => $user->id,
			
			'title' => $title,
			
			'content' => $content,
			
			'link' => $link,
			
			'class' => $class,
			
			'module' => $module,
		));
	}
}
