<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['designation_manage']			= 'Manage';
$lang['designation_edit']				= 'Edit';
$lang['designation_true']				= 'True';
$lang['designation_false']				= 'False';
$lang['designation_create']			= 'Save';
$lang['designation_list']				= 'List';
$lang['designation_new']				= 'New';
$lang['designation_edit_text']			= 'Edit this to suit your needs';
$lang['designation_no_records']		= 'There aren\'t any designation in the system.';
$lang['designation_create_new']		= 'Create a new designation.';
$lang['designation_create_success']	= 'Designation successfully created.';
$lang['designation_create_failure']	= 'There was a problem creating the designation: ';
$lang['designation_create_new_button']	= 'Create New designation';
$lang['designation_invalid_id']		= 'Invalid designation ID.';
$lang['designation_edit_success']		= 'designation successfully saved.';
$lang['designation_edit_failure']		= 'There was a problem saving the designation: ';
$lang['designation_delete_success']	= 'record(s) successfully deleted.';

$lang['designation_purged']	= 'record(s) successfully purged.';
$lang['designation_success']	= 'record(s) successfully restored.';


$lang['designation_delete_failure']	= 'We could not delete the record: ';
$lang['designation_delete_error']		= 'You have not selected any records to delete.';
$lang['designation_actions']			= 'Actions';
$lang['designation_cancel']			= 'Cancel';
$lang['designation_delete_record']		= 'Delete';
$lang['designation_delete_confirm']	= 'Are you sure you want to delete this designation?';
$lang['designation_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['designation_action_edit']		= 'Save';
$lang['designation_action_create']		= 'Create';

// Activities
$lang['designation_act_create_record']	= 'Created record with ID';
$lang['designation_act_edit_record']	= 'Updated record with ID';
$lang['designation_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['designation_column_created']	= 'Created';
$lang['designation_column_deleted']	= 'Deleted';
$lang['designation_column_modified']	= 'Modified';
