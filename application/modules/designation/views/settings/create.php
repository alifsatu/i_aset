<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($designation))
{
	$designation = (array) $designation;
}
$id = isset($designation['id']) ? $designation['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Designation</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('designation') ? 'error' : ''; ?>">
				<?php echo form_label('Designation Name'. lang('bf_form_label_required'), 'designation_designation', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='designation_designation' class='input-sm input-s  form-control' type='text' name='designation_designation' maxlength="255" value="<?php echo set_value('designation_designation', isset($designation['designation']) ? $designation['designation'] : '');?>" />
					<span class='help-inline'><?php echo form_error('designation'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'designation_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='designation_description' class='input-sm input-s  form-control' type='text' name='designation_description' maxlength="255" value="<?php echo set_value('designation_description', isset($designation['description']) ? $designation['description'] : '');?>" />
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('designation_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/designation', lang('designation_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>