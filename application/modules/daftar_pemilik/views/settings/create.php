<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($daftar_pemilik)) {
    $daftar_pemilik = (array) $daftar_pemilik;
}
$id = isset($daftar_pemilik['id']) ? $daftar_pemilik['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Daftar Pemilik</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama' . lang('bf_form_label_required'), 'daftar_pemilik_nama', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='daftar_pemilik_nama' class='input-sm input-s  form-control' type='text' name='daftar_pemilik_nama' maxlength="255" required="" value="<?php echo set_value('daftar_pemilik_nama', isset($daftar_pemilik['nama']) ? $daftar_pemilik['nama'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
                        <div class='controls' aria-labelled-by='daftar_pemilik_status_label'>
                            <label class='radio' for='daftar_pemilik_status_option1'>
                                <input id='daftar_pemilik_status_option1' name='daftar_pemilik_status' type='radio' required=""  value='1' <?php echo set_radio('daftar_pemilik_status', '1', isset($daftar_pemilik['status']) && $daftar_pemilik['status'] == 1 ? TRUE : FALSE); ?> />
                                Aktif
                            </label>
                            <label class='radio' for='daftar_pemilik_status_option2'>
                                <input id='daftar_pemilik_status_option2' name='daftar_pemilik_status' type='radio' required="" value='2' <?php echo set_radio('daftar_pemilik_status','2', isset($daftar_pemilik['status']) && $daftar_pemilik['status'] == 2 ? TRUE : FALSE); ?> />
                                Tidak Aktif
                            </label>
                            <span class='help-inline'><?php echo form_error('status'); ?></span>
                        </div>

                    </div>

                    <div class="form-actions">
                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_pemilik_action_create'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/daftar_pemilik', lang('daftar_pemilik_cancel'), 'class="btn btn-warning"'); ?>

                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>