<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_pemilik))
{
	$daftar_pemilik = (array) $daftar_pemilik;
}
$id = isset($daftar_pemilik['id']) ? $daftar_pemilik['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Pemilik</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama'. lang('bf_form_label_required'), 'daftar_pemilik_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_pemilik_nama' class='input-sm input-s  form-control' type='text' name='daftar_pemilik_nama' maxlength="255" value="<?php echo set_value('daftar_pemilik_nama', isset($daftar_pemilik['nama']) ? $daftar_pemilik['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), 'daftar_pemilik_status', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_pemilik_status' class='input-sm input-s  form-control' type='text' name='daftar_pemilik_status' maxlength="1,2" value="<?php echo set_value('daftar_pemilik_status', isset($daftar_pemilik['status']) ? $daftar_pemilik['status'] : '');?>" />
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/daftar_pemilik', lang('daftar_pemilik_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Daftar_Pemilik.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('daftar_pemilik_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>