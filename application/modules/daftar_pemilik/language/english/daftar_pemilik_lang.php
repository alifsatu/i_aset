<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_pemilik_manage']			= 'Manage';
$lang['daftar_pemilik_edit']				= 'Edit';
$lang['daftar_pemilik_true']				= 'True';
$lang['daftar_pemilik_false']				= 'False';
$lang['daftar_pemilik_create']			= 'Save';
$lang['daftar_pemilik_list']				= 'List';
$lang['daftar_pemilik_new']				= 'New';
$lang['daftar_pemilik_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_pemilik_no_records']		= 'There aren\'t any daftar_pemilik in the system.';
$lang['daftar_pemilik_create_new']		= 'Create a new Daftar Pemilik.';
$lang['daftar_pemilik_create_success']	= 'Daftar Pemilik successfully created.';
$lang['daftar_pemilik_create_failure']	= 'There was a problem creating the daftar_pemilik: ';
$lang['daftar_pemilik_create_new_button']	= 'Create New Daftar Pemilik';
$lang['daftar_pemilik_invalid_id']		= 'Invalid Daftar Pemilik ID.';
$lang['daftar_pemilik_edit_success']		= 'Daftar Pemilik successfully saved.';
$lang['daftar_pemilik_edit_failure']		= 'There was a problem saving the daftar_pemilik: ';
$lang['daftar_pemilik_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_pemilik_purged']	= 'record(s) successfully purged.';
$lang['daftar_pemilik_success']	= 'record(s) successfully restored.';


$lang['daftar_pemilik_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_pemilik_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_pemilik_actions']			= 'Actions';
$lang['daftar_pemilik_cancel']			= 'Cancel';
$lang['daftar_pemilik_delete_record']		= 'Delete';
$lang['daftar_pemilik_delete_confirm']	= 'Are you sure you want to delete this daftar_pemilik?';
$lang['daftar_pemilik_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_pemilik_action_edit']		= 'Save';
$lang['daftar_pemilik_action_create']		= 'Create';

// Activities
$lang['daftar_pemilik_act_create_record']	= 'Created record with ID';
$lang['daftar_pemilik_act_edit_record']	= 'Updated record with ID';
$lang['daftar_pemilik_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_pemilik_column_created']	= 'Created';
$lang['daftar_pemilik_column_deleted']	= 'Deleted';
$lang['daftar_pemilik_column_modified']	= 'Modified';
