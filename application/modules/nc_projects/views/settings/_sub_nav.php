
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/nc_projects') ?>"style="border-radius:0"  id="list"><?php echo lang('nc_projects_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('NC_Projects.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/nc_projects/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('nc_projects_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('NC_Projects.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/nc_projects/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
