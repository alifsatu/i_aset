<?php

$num_columns	= 5;
$can_delete	= $this->auth->has_permission('NC_Projects.Settings.Delete');
$can_edit		= $this->auth->has_permission('NC_Projects.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>NC Projects</h4></div>
  <div class="col-md-3">

<select name="select_field" id="select_field" class="selecta form-control m-b" onchange="setfname()">
<option></option>
<option value="All Fields"  <?=$this->session->userdata('nc_projectsfield')=='All Fields' ? 'selected' : ''?>>All Fields</option>		
<option value="project_name" <?=$this->session->userdata('nc_projectsfield')=='project_name' ? 'selected' : ''?>>Project name</option>
<option value="company_id" <?=$this->session->userdata('nc_projectsfield')=='company_id' ? 'selected' : ''?>>Company id</option>
</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('nc_projectsfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('nc_projectsfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('nc_projectsfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive m-t">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_project_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/nc_projects?sort_by=project_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_name') ? 'desc' : 'asc'); ?>'>
                    Project Name</a></th>
                    
                  <!-- <th<?php if ($this->input->get('sort_by') == '_company_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/nc_projects?sort_by=company_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'company_id') ? 'desc' : 'asc'); ?>'>
                    company name</a></th>-->
					<th><?php echo lang("nc_projects_column_created"); ?></th>
					<th><?php echo lang("nc_projects_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('nc_projects_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/nc_projects/edit/' . $record->id, $record->project_name ); ?>&nbsp;<i class="fa fa-pencil"></i> </td>
				<?php else : ?>
					<td><?php e($record->project_name); ?></td>
				<?php endif; ?>
					<!--<td><?php e($record->company_id) ?></td>-->
					<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
					<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
