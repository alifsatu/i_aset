<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['nc_projects_manage']			= 'Manage';
$lang['nc_projects_edit']				= 'Edit';
$lang['nc_projects_true']				= 'True';
$lang['nc_projects_false']				= 'False';
$lang['nc_projects_create']			= 'Save';
$lang['nc_projects_list']				= 'List';
$lang['nc_projects_new']				= 'New';
$lang['nc_projects_edit_text']			= 'Edit this to suit your needs';
$lang['nc_projects_no_records']		= 'There aren\'t any nc_projects in the system.';
$lang['nc_projects_create_new']		= 'Create a new NC Projects.';
$lang['nc_projects_create_success']	= 'NC Projects successfully created.';
$lang['nc_projects_create_failure']	= 'There was a problem creating the nc_projects: ';
$lang['nc_projects_create_new_button']	= 'Create New NC Projects';
$lang['nc_projects_invalid_id']		= 'Invalid NC Projects ID.';
$lang['nc_projects_edit_success']		= 'NC Projects successfully saved.';
$lang['nc_projects_edit_failure']		= 'There was a problem saving the nc_projects: ';
$lang['nc_projects_delete_success']	= 'record(s) successfully deleted.';

$lang['nc_projects_purged']	= 'record(s) successfully purged.';
$lang['nc_projects_success']	= 'record(s) successfully restored.';


$lang['nc_projects_delete_failure']	= 'We could not delete the record: ';
$lang['nc_projects_delete_error']		= 'You have not selected any records to delete.';
$lang['nc_projects_actions']			= 'Actions';
$lang['nc_projects_cancel']			= 'Cancel';
$lang['nc_projects_delete_record']		= 'Delete';
$lang['nc_projects_delete_confirm']	= 'Are you sure you want to delete this nc_projects?';
$lang['nc_projects_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['nc_projects_action_edit']		= 'Save';
$lang['nc_projects_action_create']		= 'Create';

// Activities
$lang['nc_projects_act_create_record']	= 'Created record with ID';
$lang['nc_projects_act_edit_record']	= 'Updated record with ID';
$lang['nc_projects_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['nc_projects_column_created']	= 'Created';
$lang['nc_projects_column_deleted']	= 'Deleted';
$lang['nc_projects_column_modified']	= 'Modified';
