<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_classification_group_manage']			= 'Manage';
$lang['daftar_classification_group_edit']				= 'Edit';
$lang['daftar_classification_group_true']				= 'True';
$lang['daftar_classification_group_false']				= 'False';
$lang['daftar_classification_group_create']			= 'Save';
$lang['daftar_classification_group_list']				= 'List';
$lang['daftar_classification_group_new']				= 'New';
$lang['daftar_classification_group_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_classification_group_no_records']		= 'There aren\'t any daftar_classification_group in the system.';
$lang['daftar_classification_group_create_new']		= 'Create a new Daftar Classification Group.';
$lang['daftar_classification_group_create_success']	= 'Daftar Classification Group successfully created.';
$lang['daftar_classification_group_create_failure']	= 'There was a problem creating the daftar_classification_group: ';
$lang['daftar_classification_group_create_new_button']	= 'Create New Daftar Classification Group';
$lang['daftar_classification_group_invalid_id']		= 'Invalid Daftar Classification Group ID.';
$lang['daftar_classification_group_edit_success']		= 'Daftar Classification Group successfully saved.';
$lang['daftar_classification_group_edit_failure']		= 'There was a problem saving the daftar_classification_group: ';
$lang['daftar_classification_group_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_classification_group_purged']	= 'record(s) successfully purged.';
$lang['daftar_classification_group_success']	= 'record(s) successfully restored.';


$lang['daftar_classification_group_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_classification_group_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_classification_group_actions']			= 'Actions';
$lang['daftar_classification_group_cancel']			= 'Cancel';
$lang['daftar_classification_group_delete_record']		= 'Delete';
$lang['daftar_classification_group_delete_confirm']	= 'Are you sure you want to delete this daftar_classification_group?';
$lang['daftar_classification_group_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_classification_group_action_edit']		= 'Save';
$lang['daftar_classification_group_action_create']		= 'Create';

// Activities
$lang['daftar_classification_group_act_create_record']	= 'Created record with ID';
$lang['daftar_classification_group_act_edit_record']	= 'Updated record with ID';
$lang['daftar_classification_group_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_classification_group_column_created']	= 'Created';
$lang['daftar_classification_group_column_deleted']	= 'Deleted';
$lang['daftar_classification_group_column_modified']	= 'Modified';
