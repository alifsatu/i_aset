
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/daftar_classification_group') ?>"style="border-radius:0"  id="list"><?php echo lang('daftar_classification_group_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Daftar_Classification_Group.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/daftar_classification_group/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('daftar_classification_group_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Daftar_Classification_Group.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/daftar_classification_group/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
