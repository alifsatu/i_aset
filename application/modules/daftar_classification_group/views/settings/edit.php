<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_classification_group))
{
	$daftar_classification_group = (array) $daftar_classification_group;
}
$id = isset($daftar_classification_group['id']) ? $daftar_classification_group['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Classification Group</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_classification_group_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_classification_group_name' class='input-sm input-s  form-control' type='text' name='daftar_classification_group_name' maxlength="255" value="<?php echo set_value('daftar_classification_group_name', isset($daftar_classification_group['name']) ? $daftar_classification_group['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'daftar_classification_group_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_classification_group_description' class='input-sm input-s  form-control' type='text' name='daftar_classification_group_description' maxlength="255" value="<?php echo set_value('daftar_classification_group_description', isset($daftar_classification_group['description']) ? $daftar_classification_group['description'] : '');?>" />
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_classification_group_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/daftar_classification_group', lang('daftar_classification_group_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Daftar_Classification_Group.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('daftar_classification_group_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('daftar_classification_group_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>