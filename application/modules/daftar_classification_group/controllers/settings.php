<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Daftar_Classification_Group.Settings.View');
        $this->load->model('daftar_classification_group_model', null, true);
        $this->lang->load('daftar_classification_group');

        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('daftar_classification_group', 'daftar_classification_group.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');


        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->daftar_classification_group_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('daftar_classification_group_delete_success'), 'success');
                } else {
                    Template::set_message(lang('daftar_classification_group_delete_failure') . $this->daftar_classification_group_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('daftar_classification_groupfield', $this->input->post('select_field'));
                $this->session->set_userdata('daftar_classification_groupfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('daftar_classification_groupfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('daftar_classification_groupfield');
                $this->session->unset_userdata('daftar_classification_groupfvalue');
                $this->session->unset_userdata('daftar_classification_groupfname');
                break;
        }

        if ($this->session->userdata('daftar_classification_groupfield') != '') {
            $field = $this->session->userdata('daftar_classification_groupfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('daftar_classification_groupfield') == 'All Fields') {

            $total = $this->daftar_classification_group_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->daftar_classification_group_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->count_all();
        }

        //$records = $this->daftar_classification_group_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('daftar_classification_groupfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '0')
                    ->likes('personal_particulars_name', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $records = $this->daftar_classification_group_model
                    ->where('deleted', '0')
                    ->likes($field, $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }
//$records = $this->daftar_classification_group_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);

        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('daftar_classification_group', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Daftar Classification Group');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_daftar_classification_group SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('daftar_classification_group_success'), 'success');
                } else {
                    Template::set_message(lang('daftar_classification_group_restored_error') . $this->daftar_classification_group_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_daftar_classification_group where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('daftar_classification_group_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('daftar_classification_groupfield', $this->input->post('select_field'));
                $this->session->set_userdata('daftar_classification_groupfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('daftar_classification_groupfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('daftar_classification_groupfield');
                $this->session->unset_userdata('daftar_classification_groupfvalue');
                $this->session->unset_userdata('daftar_classification_groupfname');
                break;
        }


        if ($this->session->userdata('daftar_classification_groupfield') != '') {
            $field = $this->session->userdata('daftar_classification_groupfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('daftar_classification_groupfield') == 'All Fields') {

            $total = $this->daftar_classification_group_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->daftar_classification_group_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-daftar_classification_group_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('daftar_classification_groupfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('personal_particulars_name', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_nric', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_htel', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_email', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->likes('personal_particulars_citizenship', $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->daftar_classification_group_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('daftar_classification_groupfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->daftar_classification_group_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('daftar_classification_group', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Daftar Classification Group');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Daftar Classification Group object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Daftar_Classification_Group.Settings.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_daftar_classification_group()) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_classification_group_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'daftar_classification_group');

                Template::set_message(lang('daftar_classification_group_create_success'), 'success');
                redirect(SITE_AREA . '/settings/daftar_classification_group');
            } else {
                Template::set_message(lang('daftar_classification_group_create_failure') . $this->daftar_classification_group_model->error, 'error');
            }
        }
        Assets::add_module_js('daftar_classification_group', 'daftar_classification_group.js');

        Template::set('toolbar_title', lang('daftar_classification_group_create') . ' Daftar Classification Group');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Daftar Classification Group data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('daftar_classification_group_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/daftar_classification_group');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Daftar_Classification_Group.Settings.Edit');

            if ($this->save_daftar_classification_group('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_classification_group_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_classification_group');

                Template::set_message(lang('daftar_classification_group_edit_success'), 'success');
            } else {
                Template::set_message(lang('daftar_classification_group_edit_failure') . $this->daftar_classification_group_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Daftar_Classification_Group.Settings.Delete');

            if ($this->daftar_classification_group_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('daftar_classification_group_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'daftar_classification_group');

                Template::set_message(lang('daftar_classification_group_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/daftar_classification_group');
            } else {
                Template::set_message(lang('daftar_classification_group_delete_failure') . $this->daftar_classification_group_model->error, 'error');
            }
        }
        Template::set('daftar_classification_group', $this->daftar_classification_group_model->find($id));
        Template::set('toolbar_title', lang('daftar_classification_group_edit') . ' Daftar Classification Group');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_daftar_classification_group SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('daftar_classification_group_success'), 'success');
                redirect(SITE_AREA . '/settings/daftar_classification_group/deleted');
            } else {

                Template::set_message(lang('daftar_classification_group_error') . $this->daftar_classification_group_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_daftar_classification_group where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('daftar_classification_group_purged'), 'success');
                redirect(SITE_AREA . '/settings/daftar_classification_group/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('daftar_classification_group_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/daftar_classification_group');
        }




        Template::set('daftar_classification_group', $this->daftar_classification_group_model->find($id));

        Assets::add_module_js('daftar_classification_group', 'daftar_classification_group.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Daftar_classification_group');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_daftar_classification_group($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want

        $data = array();
        $data['name'] = $this->input->post('daftar_classification_group_name');
        $data['description'] = $this->input->post('daftar_classification_group_description');

        if ($type == 'insert') {
            $id = $this->daftar_classification_group_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->daftar_classification_group_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
