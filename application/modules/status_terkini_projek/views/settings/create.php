<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($status_terkini_projek)) {
    $status_terkini_projek = (array) $status_terkini_projek;
}
$id = isset($status_terkini_projek['id']) ? $status_terkini_projek['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Status Terkini Projek</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Status' . lang('bf_form_label_required'), 'status_terkini_projek_nama', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='status_terkini_projek_nama' class='input-sm input-s  form-control' type='text' name='status_terkini_projek_nama' maxlength="255" required="" value="<?php echo set_value('status_terkini_projek_nama', isset($status_terkini_projek['nama']) ? $status_terkini_projek['nama'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
                        <div class='controls' aria-labelled-by='status_terkini_projek_status_label'>
                            <label class='radio' for='status_terkini_projek_status_option1'>
                                <input id='status_terkini_projek_status_option1' name='status_terkini_projek_status' type='radio' required=""  value='1' <?php echo set_radio('status_terkini_projek_status', '1', isset($status_terkini_projek['status']) && $status_terkini_projek['status'] == 1 ? TRUE : FALSE); ?> />
                                Aktif
                            </label>
                            <label class='radio' for='status_terkini_projek_status_option2'>
                                <input id='status_terkini_projek_status_option2' name='status_terkini_projek_status' type='radio' required="" value='2' <?php echo set_radio('status_terkini_projek_status', '2', isset($status_terkini_projek['status']) && $status_terkini_projek['status'] == 2 ? TRUE : FALSE); ?> />
                                Tidak Aktif
                            </label>
                            <span class='help-inline'><?php echo form_error('status'); ?></span>
                        </div>

                    </div>

                    <div class="form-actions">
                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('status_terkini_projek_action_create'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/status_terkini_projek', lang('status_terkini_projek_cancel'), 'class="btn btn-warning"'); ?>

                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>