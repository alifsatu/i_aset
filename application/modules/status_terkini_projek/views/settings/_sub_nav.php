
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/status_terkini_projek') ?>"style="border-radius:0"  id="list"><?php echo lang('status_terkini_projek_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Status_Terkini_Projek.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/status_terkini_projek/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('status_terkini_projek_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Status_Terkini_Projek.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/status_terkini_projek/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
