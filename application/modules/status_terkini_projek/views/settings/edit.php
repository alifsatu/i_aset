<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($status_terkini_projek))
{
	$status_terkini_projek = (array) $status_terkini_projek;
}
$id = isset($status_terkini_projek['id']) ? $status_terkini_projek['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Status Terkini Projek</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Status'. lang('bf_form_label_required'), 'status_terkini_projek_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='status_terkini_projek_nama' class='input-sm input-s  form-control' type='text' name='status_terkini_projek_nama' maxlength="255" value="<?php echo set_value('status_terkini_projek_nama', isset($status_terkini_projek['nama']) ? $status_terkini_projek['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
				<?php echo form_label('Status'. lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'status_terkini_projek_status_label') ); ?>
				<div class='controls' aria-labelled-by='status_terkini_projek_status_label'>
					<label class='radio' for='status_terkini_projek_status_option1'>
						<input id='status_terkini_projek_status_option1' name='status_terkini_projek_status' type='radio'  value='option1' <?php echo set_radio('status_terkini_projek_status', 'option1', TRUE); ?> />
						Radio option 1
					</label>
					<label class='radio' for='status_terkini_projek_status_option2'>
						<input id='status_terkini_projek_status_option2' name='status_terkini_projek_status' type='radio' value='option2' <?php echo set_radio('status_terkini_projek_status', 'option2'); ?> />
						Radio option 2
					</label>
					<span class='help-inline'><?php echo form_error('status'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('status_terkini_projek_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/status_terkini_projek', lang('status_terkini_projek_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Status_Terkini_Projek.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('status_terkini_projek_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('status_terkini_projek_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>