<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['status_terkini_projek_manage']			= 'Manage';
$lang['status_terkini_projek_edit']				= 'Edit';
$lang['status_terkini_projek_true']				= 'True';
$lang['status_terkini_projek_false']				= 'False';
$lang['status_terkini_projek_create']			= 'Save';
$lang['status_terkini_projek_list']				= 'List';
$lang['status_terkini_projek_new']				= 'New';
$lang['status_terkini_projek_edit_text']			= 'Edit this to suit your needs';
$lang['status_terkini_projek_no_records']		= 'There aren\'t any status_terkini_projek in the system.';
$lang['status_terkini_projek_create_new']		= 'Create a new Status Terkini Projek.';
$lang['status_terkini_projek_create_success']	= 'Status Terkini Projek successfully created.';
$lang['status_terkini_projek_create_failure']	= 'There was a problem creating the status_terkini_projek: ';
$lang['status_terkini_projek_create_new_button']	= 'Create New Status Terkini Projek';
$lang['status_terkini_projek_invalid_id']		= 'Invalid Status Terkini Projek ID.';
$lang['status_terkini_projek_edit_success']		= 'Status Terkini Projek successfully saved.';
$lang['status_terkini_projek_edit_failure']		= 'There was a problem saving the status_terkini_projek: ';
$lang['status_terkini_projek_delete_success']	= 'record(s) successfully deleted.';

$lang['status_terkini_projek_purged']	= 'record(s) successfully purged.';
$lang['status_terkini_projek_success']	= 'record(s) successfully restored.';


$lang['status_terkini_projek_delete_failure']	= 'We could not delete the record: ';
$lang['status_terkini_projek_delete_error']		= 'You have not selected any records to delete.';
$lang['status_terkini_projek_actions']			= 'Actions';
$lang['status_terkini_projek_cancel']			= 'Cancel';
$lang['status_terkini_projek_delete_record']		= 'Delete';
$lang['status_terkini_projek_delete_confirm']	= 'Are you sure you want to delete this status_terkini_projek?';
$lang['status_terkini_projek_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['status_terkini_projek_action_edit']		= 'Save';
$lang['status_terkini_projek_action_create']		= 'Create';

// Activities
$lang['status_terkini_projek_act_create_record']	= 'Created record with ID';
$lang['status_terkini_projek_act_edit_record']	= 'Updated record with ID';
$lang['status_terkini_projek_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['status_terkini_projek_column_created']	= 'Created';
$lang['status_terkini_projek_column_deleted']	= 'Deleted';
$lang['status_terkini_projek_column_modified']	= 'Modified';
