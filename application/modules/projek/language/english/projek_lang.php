<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['projek_manage']			= 'Manage';
$lang['projek_edit']				= 'Edit';
$lang['projek_true']				= 'True';
$lang['projek_false']				= 'False';
$lang['projek_create']			= 'Save';
$lang['projek_list']				= 'List';
$lang['projek_new']				= 'New';
$lang['projek_edit_text']			= 'Edit this to suit your needs';
$lang['projek_no_records']		= 'There aren\'t any projek in the system.';
$lang['projek_create_new']		= 'Create a new Projek.';
$lang['projek_create_success']	= 'Projek successfully created.';
$lang['projek_create_failure']	= 'There was a problem creating the projek: ';
$lang['projek_create_new_button']	= 'Create New Projek';
$lang['projek_invalid_id']		= 'Invalid Projek ID.';
$lang['projek_edit_success']		= 'Projek successfully saved.';
$lang['projek_edit_failure']		= 'There was a problem saving the projek: ';
$lang['projek_delete_success']	= 'record(s) successfully deleted.';

$lang['projek_purged']	= 'record(s) successfully purged.';
$lang['projek_success']	= 'record(s) successfully restored.';


$lang['projek_delete_failure']	= 'We could not delete the record: ';
$lang['projek_delete_error']		= 'You have not selected any records to delete.';
$lang['projek_actions']			= 'Actions';
$lang['projek_cancel']			= 'Cancel';
$lang['projek_delete_record']		= 'Delete';
$lang['projek_delete_confirm']	= 'Are you sure you want to delete this projek?';
$lang['projek_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['projek_action_edit']		= 'Save';
$lang['projek_action_create']		= 'Create';

// Activities
$lang['projek_act_create_record']	= 'Created record with ID';
$lang['projek_act_edit_record']	= 'Updated record with ID';
$lang['projek_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['projek_column_created']	= 'Created';
$lang['projek_column_deleted']	= 'Deleted';
$lang['projek_column_modified']	= 'Modified';



$lang['kebenaran_merancang']	= 'KM';
$lang['projek_act_bayar_record']	= 'Updated bayaran record with ID';
$lang['projek_bayaran_success']		= 'Projek successfully saved.';
$lang['projek_bayaran_success']		= 'Projek successfully saved.';
$lang['projek_bayaran_failure']		= 'There was a problem saving the bayaran: ';
