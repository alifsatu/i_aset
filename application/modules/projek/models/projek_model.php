<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projek_model extends BF_Model {

    protected $table_name = "projek";
    protected $key = "id";
    protected $soft_deletes = true;
    protected $date_format = "datetime";
    protected $log_user = TRUE;
    protected $set_created = true;
    protected $set_modified = true;
    protected $created_field = "created_on";
    protected $modified_field = "modified_on";

    /*
      Customize the operations of the model without recreating the insert, update,
      etc methods by adding the method names to act as callbacks here.
     */
    protected $before_insert = array();
    protected $after_insert = array();
    protected $before_update = array();
    protected $after_update = array();
    protected $before_find = array();
    protected $after_find = array();
    protected $before_delete = array();
    protected $after_delete = array();

    /*
      For performance reasons, you may require your model to NOT return the
      id of the last inserted row as it is a bit of a slow method. This is
      primarily helpful when running big loops over data.
     */
    protected $return_insert_id = TRUE;
    // The default type of element data is returned as.
    protected $return_type = "object";
    // Items that are always removed from data arrays prior to
    // any inserts or updates.
    protected $protected_attributes = array();

    /*
      You may need to move certain rules (like required) into the
      $insert_validation_rules array and out of the standard validation array.
      That way it is only required during inserts, not updates which may only
      be updating a portion of the data.
     */
    protected $validation_rules = array(
        array(
            "field" => "projek_tarikh_terima_permohonan",
            "label" => "Tarikh Terima Permohonan",
            "rules" => "required"
        ),
        array(
            "field" => "projek_pegawai_ulasan",
            "label" => "Pegawai Ulasan",
            "rules" => "required|max_length[11]"
        ),
        array(
            "field" => "projek_nama_projek",
            "label" => "Nama Projek",
            "rules" => "required|max_length[1000]"
        ),
        array(
            "field" => "projek_daerah",
            "label" => "Daerah",
            "rules" => "required|max_length[11]"
        ),
        array(
            "field" => "projek_no_rujukan",
            "label" => "No Rujukan",
            "rules" => "required|max_length[255]"
        ),
        array(
            "field" => "projek_jumlah_bayaran_fi_ukp",
            "label" => "Jumlah",
            "rules" => "is_decimal|max_length[255]"
        ),
        array(
            "field" => "projek_tarikh_bayaran_fi_ukp",
            "label" => "Tarikh",
            "rules" => ""
        ),
        array(
            "field" => "projek_no_resit_bayaran_fi_ukp",
            "label" => "No. Resit",
            "rules" => "max_length[255]"
        ),
        array(
            "field" => "projek_jumlah_bayaran_fi_pmutk",
            "label" => "Jumlah",
            "rules" => "is_decimal|max_length[255]"
        ),
        array(
            "field" => "projek_tarikh_bayaran_fi_pmutk",
            "label" => "Tarikh",
            "rules" => "is_decimal"
        ),
        array(
            "field" => "projek_no_resit_bayaran_fi_pmutk",
            "label" => "No. Resit",
            "rules" => "max_length[255]"
        ),
        array(
            "field" => "projek_jumlah_bayaran_fi_pspp",
            "label" => "Jumlah",
            "rules" => "is_decimal|max_length[255]"
        ),
        array(
            "field" => "projek_tarikh_bayaran_fi_pspp",
            "label" => "Tarikh",
            "rules" => ""
        ),
        array(
            "field" => "projek_no_resit_bayaran_fi_pspp",
            "label" => "No. Resit",
            "rules" => "max_length[255]"
        ),
        array(
            "field" => "projek_jumlah_bayaran_fi_ume",
            "label" => "Jumlah",
            "rules" => "is_decimal|max_length[255]"
        ),
        array(
            "field" => "projek_tarikh_bayaran_fi_ume",
            "label" => "Tarikh",
            "rules" => ""
        ),
        array(
            "field" => "projek_no_resit_bayaran_fi_ume",
            "label" => "No. Resit",
            "rules" => "max_length[255]"
        ),
        array(
            "field" => "projek_nama_jurutera_perunding",
            "label" => "Nama Jurutera Perunding",
            "rules" => ""
        ),
        array(
            "field" => "projek_nama_pemilik",
            "label" => "Nama Pemilik",
            "rules" => ""
        ),
        array(
            "field" => "projek_fasa",
            "label" => "Fasa",
            "rules" => "max_length[11]"
        ),
        array(
            "field" => "projek_status_permohonan",
            "label" => "Status Permohonan",
            "rules" => "max_length[11]"
        ),
        array(
            "field" => "projek_status_terkini",
            "label" => "Status Terkini Projek",
            "rules" => "max_length[11]"
        ),
    );
    protected $insert_validation_rules = array();
    protected $skip_validation = FALSE;

    //--------------------------------------------------------------------
}
