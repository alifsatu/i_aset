<?php
$num_columns = 23;
$can_delete = $this->auth->has_permission('Projek.Projectmgmt.Delete');
$can_edit = $this->auth->has_permission('Projek.Projectmgmt.Edit');
$Admin_Can_Edit = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Edit');
$Admin_Can_Delete = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Delete');
$has_records = isset($records) && is_array($records) && count($records);
$user_list = get_user_name_list();
$perunding_list = get_perunding_name_list();
$pemilik_list = get_pemilik_name_list();
$daerah_list = get_daerah_name_list();
$status_permohonan_list = get_status_permohonan_list();
$status_terkini_projek_list = get_intg_status_terkini_projek_list();
?>
<style>
    #reset {
        -webkit-animation: flash 6s infinite linear;
        animation: flash 6s infinite linear;
    }
    div.dataTables_wrapper {
        width: 100vw;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
    /* $(function () {
     
     var $table = $('table').tablesorter({
     theme: 'blue',
     widgets: ["filter"],
     widgetOptions: {
     // filter_anyMatch replaced! Instead use the filter_external option
     // Set to use a jQuery selector (or jQuery object) pointing to the
     // external filter (column specific or any match)
     filter_external: '.search',
     // add a default type search to the first name column
     filter_defaultFilter: {1: '~{query}'},
     // include column filters
     filter_columnFilters: true,
     filter_placeholder: {search: 'Search...'},
     filter_saveFilters: true,
     filter_reset: '.reset'
     }
     });
     
     // make demo search buttons work
     $('button[data-column]').on('click', function () {
     var $this = $(this),
     totalColumns = $table[0].config.columns,
     col = $this.data('column'), // zero-based index or "all"
     filter = [];
     
     // text to add to filter
     filter[ col === 'all' ? totalColumns : col ] = $this.text();
     $table.trigger('search', [filter]);
     return false;
     });
     
     });
     
     */
</script>
<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">

            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>
            <div class="row filter_area" >
                <div class="col-md-2">
                    <h4>Projek</h4>
                </div>
                <div class="col-md-3">
                    <select name="select_field" id="select_field" class="form-control">
                        <option value=''>--Pilih Jenis Saringan--</option>
                        <option <?php echo $this->session->userdata('projekfield') == "All Fields" ? "selected" : "" ?> value="All Fields">All Fields</option>
                        <option <?php echo $this->session->userdata('projekfield') == "intg_projek.id" ? "selected" : "" ?> value="intg_projek.id">ID Projek</option>
                        <option <?php echo $this->session->userdata('projekfield') == "intg_projek.daerah" ? "selected" : "" ?> value="intg_projek.daerah">Daerah</option>
                        <option <?php echo $this->session->userdata('projekfield') == "intg_projek.pegawai_ulasan" ? "selected" : "" ?> value="intg_projek.pegawai_ulasan">Pegawai Ulasan</option>
                        <option <?php echo $this->session->userdata('projekfield') == "intg_projek.nama_jurutera_perunding" ? "selected" : "" ?> value="intg_projek.nama_jurutera_perunding">Jurutera Perunding</option>
                    </select>


                </div>
                <div class="col-md-6">
                    <div class="input-group">

                        <input type="hidden" name="field_name" id="field_name" value="<?= $this->session->userdata('projekfname') ?>"/>
                        <select name="field_value" class="selecta form-control field_value_select" id="projects" >
                            <option ></option><?php
                            $proj = '';
                            switch ($this->session->userdata('projekfield')) {

//                                case "intg_projek.id" :
//                                    $proj = $this->db->query("select id as rowid,nama_projek as selectedname from intg_projek where deleted =0")->result();
//                                    break;

                                case "intg_projek.pegawai_ulasan" :
                                    $proj = $this->db->query("select id as rowid,display_name as selectedname from intg_users where deleted = 0")->result();
                                    break;

                                case "intg_projek.daerah" :
                                    $proj = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_daerah where deleted = 0")->result();
                                    break;

                                case "intg_projek.nama_jurutera_perunding" :
                                    $proj = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_jurutera_perunding where status = 1")->result();
                                    break;

                                default :
//                                    $proj = $this->db->query("select id as rowid,nama_projek as selectedname from intg_projek where deleted =0")->result();
                                    break;
                            }
                            if ($proj != '') {
                                foreach ($proj as $p) {
                                    if ($this->session->userdata('projekfvalue') == $p->rowid) {
                                        ?>
                                        <option  selected="selected" value="<?= $p->rowid ?>"><?= $p->rowid ?> - <?= $p->selectedname ?></option>
                                    <?php } else { ?><option   value="<?= $p->rowid ?>"><?= $p->rowid ?> - <?= $p->selectedname ?></option><?php
                                    }
                                }
                            }
                            ?>
                        </select>

                        <input id='search_input' class='input-sm input-s  form-control field_value_input' type='number' min="0" step="1" name='field_value' value="<?= $this->session->userdata('projekfvalue') ?>" placeholder="Sila Masukkan Kunci Carian" />


                        <span class="input-group-btn">
                            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info">
                                <span class="fa  fa-search"></span>
                            </button>
                            <button type="submit" name="submit"   class="btn btn-primary btn-icon" <?= $this->session->userdata('projekfield') ? 'id="reset"' : '' ?> value="Reset" title="Reset">
                                <span class="fa  fa-refresh"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <?php echo form_close(); ?>
            <hr>
            <?php echo form_open($this->uri->uri_string()); ?>
            <div id="dvData">
                <div class="table-responsive">   
                    <table class="table table-striped table-hover table-condensed text-sm dt-nowrap table-bordered" style="width:100%" id='indexTable'>
                        <thead>
                            <tr>
                                <?php if ($can_delete && $has_records) { ?>
                                    <th class="column-check">Bil <input class="check-all" type="checkbox" /></th>
                                <?php } else { ?>
                                    <th>Bil </th>
                                <?php } ?>
                                <th>Tindakan</th>
                                <th>Id Projek</th>
                                <th>Nama Projek</th>
                                <th>Tarikh Terima Permohonan</th>
                                <th>Pegawai Ulasan</th>
                                <th>Daerah</th>
                                <th>No Rujukan</th>
                                <th>Nama Jurutera Perunding</th>
                                <th>Nama Pemilik</th>
                                <th>Didaftar Oleh</th>
                                <th>Status Permohonan</th>
                                <th>Status Terkini Projek</th>
                                <?php foreach ($jenis_bayaran as $jb) { ?>

                                    <th><?= $jb->nama ?></th>

                                <?php } ?>
                                <th>Jumlah Sumbangan Modal</th>
                                <th>Jumlah Telah Bayar</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            if ($has_records) :
                                $no = 1;

                                foreach ($records as $record) :

                                    $nama_proj_pjg = $record->id . " - " . $record->nama_projek;
                                    $nama_proj_pendek = mb_strimwidth($record->nama_projek, 0, 50, "...");
                                    ?>
                                    <tr>
                                        <?php if (($can_delete && $this->auth->user_id() == $record->created_by) || $Admin_Can_Delete) { ?>
                                            <td class="column-check"><?= $no ?>&nbsp;&nbsp;
                                                <input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" />
                                            </td>

                                        <?php } else { ?>
                                            <td><?= $no ?></td>
                                        <?php } ?>
                                        <td>
                                            <?php if (($can_edit && ($this->auth->user_id() == $record->created_by || $this->auth->user_id() == $record->pegawai_ulasan )) || $Admin_Can_Edit) { ?>
                                                <?php echo anchor(SITE_AREA . '/projectmgmt/projek/edit/' . $record->id, "<i class='fa fa-pencil'> Edit </i>&nbsp;&nbsp", " data-toggle='tooltip' data-placement='top'  title='Kemaskini Projek' class='btn bg-info' "); ?> 
                                            <?php } ?>

                                            <?php if ($this->auth->user()->daerah != '') { ?>
                                                <?php echo anchor(SITE_AREA . '/projectmgmt/projek/bayar/' . $record->id, "<i class='fa fa-pencil'> Pay </i>&nbsp;&nbsp", " data-toggle='tooltip' data-placement='top'  title='Kemaskini Projek' class='btn bg-info' "); ?> 
                                            <?php } ?>

                                            &nbsp;&nbsp;
                                            <?php echo anchor(SITE_AREA . '/projectmgmt/projek/view/' . $record->id, "<i class='fa fa-eye'> View</i>", " data-toggle='tooltip' data-placement='top'  title='Lihat Projek' class='btn bg-success'  "); ?>
                                        </td>
                                        <td>
                                            <b><?= $record->id ?></b><br>
                                        </td>
                                        <td width="20%"><?= $record->nama_projek ?></td>
                                        <td><?php e($record->tarikh_terima_permohonan) ?></td>
                                        <td><?php echo $record->pegawai_ulasan != '' ? $user_list[$record->pegawai_ulasan] : '' ?></td>
                                        <td><?php echo $record->daerah != '' ? $daerah_list[$record->daerah] : '' ?></td>
                                        <td><?php e($record->no_rujukan) ?></td>

                                        <td>
                                            <?php
                                            if ($record->nama_jurutera_perunding != '') {
                                                $arr_jp = explode(",", $record->nama_jurutera_perunding);
                                                $cnt = 1;
                                                foreach ($arr_jp as $jp) {
                                                    echo $cnt . ') ' . $perunding_list[$jp];
                                                    if (sizeof($arr_jp) > 1 and $cnt < sizeof($arr_jp)) {
                                                        echo '<br>';
                                                    }
                                                    $cnt++;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $record->nama_pemilik; ?></td>
                                        <td><?php echo $record->created_by != '' ? $user_list[$record->created_by] : '' ?></td>
                                        <td><?php echo $status_permohonan_list[$record->status_permohonan]; ?></td>
                                        <td><?php echo $status_terkini_projek_list[$record->status_terkini_projek]; ?></td>
                                        <?php
                                        $sum_sumbangan_modal_tlhbyr = 0;
                                        foreach ($jenis_bayaran as $jb) {
                                            ?>

                                            <td>

                                                <?php
                                                $preg = $record->id . '_' . $jb->id . '_';

                                                $inner_count = 1;
                                                foreach ($rec_bayaran as $rec) {
                                                    $pieces = explode('_', $rec->id_projek_bayaran);

                                                    if ($pieces[0] . "_" . $pieces[1] . "_" == $preg) {


                                                        $jumlah_bayaran = '<b>J</b> = N/A';
                                                        if ($rec->jumlah_bayaran != '') {
                                                            $jumlah_bayaran = '<b>J</b> = ' . number_format($rec->jumlah_bayaran, 2);

                                                            if ($jb->id == '1' || $jb->id == '2' || $jb->id == '3') {
                                                                $sum_sumbangan_modal_tlhbyr += number_format($rec->jumlah_bayaran, 2, '.', '');
                                                            }
                                                        }

                                                        $tarikh_bayaran = '<b>T</b> = N/A';
                                                        if ($rec->tarikh_bayaran == '' || $rec->tarikh_bayaran == '0000-00-00') {
                                                            
                                                        } else {
                                                            $tarikh_bayaran = '<b>T</b> = ' . $rec->tarikh_bayaran;
                                                        }

                                                        $resit_bayaran = '<b>R</b> = N/A';
                                                        if ($rec->no_resit_bayaran != '') {
                                                            $resit_bayaran = '<b>R</b> = ' . $rec->no_resit_bayaran;
                                                        }

                                                        $string = '(<b>' . $inner_count . ' : </b>' . $jumlah_bayaran . ', ' . $tarikh_bayaran . ', ' . $resit_bayaran . ')<br>';

                                                        echo $string;
                                                        $inner_count++;
                                                    }
                                                }
//                                                
                                                ?>
                                            </td>

                                        <?php } ?>
                                        <td><?php echo number_format($record->bsm_jumlah_perlu_dijelaskan, 2); ?></td>
                                        <td><?php echo number_format($sum_sumbangan_modal_tlhbyr, 2); ?></td>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<td><?php echo date("d/m/Y h:i:s", strtotime($record->created_on)); ?></td>-->
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<td><?php echo $record->modified_on == "0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s", strtotime($record->modified_on)); ?></td>-->
                                    </tr>
                                    <?php
                                    $no++;
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>

                        <?php if ($has_records) : ?>
                                                                                                                                                                                                                                                                                                                                                                                                <!--                        <tfoot>
                            <?php if ($can_delete) : ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td colspan="<?php echo $num_columns; ?>">
                                <?php echo lang('bf_with_selected'); ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('projek_delete_confirm'))); ?>')" />
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </tr>
                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                        </tfoot>-->
                        <?php endif; ?>


                    </table>
                </div>

            </div>
            <?php echo form_close(); ?>

            <footer class="panel-footer">
                <!-- <div class="row">
                     <div class="col-sm-4 hidden-xs">
 
 
                     </div>
                     <div class="col-sm-4 text-center">
                         <small class="text-muted inline m-t-sm m-b-sm">Showing <?= $offset + 1 ?> - <?php echo ($rowcount + $offset) > $total ? $total : $rowcount + $offset; ?> Of  <?php echo $total; ?></small>
                     </div>
                     <div class="col-sm-4 text-right text-center-xs">                
 
                <?php // echo $this->pagination->create_links();       ?>
                     </div>
                 </div>-->
            </footer>                



        </div>
    </section>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var x = '<?= $this->session->userdata('projekfield') ?>';
        if (x == 'All Fields' || x == 'intg_projek.id') {
            $("#search_input").attr('name', 'field_value');
            $("#projects").attr('name', 'field_value_x');

            if (x == 'intg_projek.id') {
                $("#search_input").attr('type', 'number');
            } else if (x == 'All Fields') {
                $("#search_input").attr('type', 'text');
            }


            $('.field_value_select').hide();
            $('.field_value_input').show();
        } else {

            $("#search_input").attr('name', 'field_value_x');
            $("#projects").attr('name', 'field_value');
            $('.field_value_input').hide();
            $('.field_value_select').show();
        }

<?php if ($has_records) { ?>

            // Setup - add a text input to each header cell
            var count = 1;
            $('#indexTable thead th').each(function () {
                if (count > 2) {
                    var title = $('#indexTable thead th').eq($(this).index()).text();
                    $(this).html(title + '<br><input type="text" >');
                }
                count++;
            });

            var example = $('#indexTable').DataTable({
                "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                //            dom: 'Bflrpitip',
                dom: "<'row'<'col-md-4'l><'col-md-4'B><'col-md-4'f>>" +
                        "<'row'<'col-md-4'i><'col-md-4'p>>" +
                        "<'row'<'col-md-12'tr>>" +
                        "<'row'<'col-md-4'i><'col-md-4'p>>",
                columnDefs: [
                    {targets: 3, width: "500px"}
                ],
                buttons: [
                    {
                        extend: 'copyHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'excelHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'csvHtml5',
                        footer: true,
                        //                    exportOptions: {
                        //                        columns: [2, 3, 4, 5, 6, 7, 8, 9, 10]
                        //                    }
                    },
                    {
                        extend: 'print',
                        footer: true,
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        footer: true,
                        orientation: 'landscape',
                        pageSize: 'A4',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        }
                    }


                ],
                oLanguage: {
                    sLengthMenu: "Record to show : _MENU_",
                },
                order: [[2, "desc"]]
            });


            // Apply the search
            example.columns().eq(0).each(function (colIdx) {
                $('input', example.column(colIdx).header()).on('keyup change', function () {
                    example
                            .column(colIdx)
                            .search(this.value)
                            .draw();
                });

                $('input', example.column(colIdx).header()).on('click', function (e) {
                    e.stopPropagation();
                });
            });


<?php } ?>
    }
    );
    $("#select_field").change(function () {
        waitingDialog.show('Loading...');
        if ($(this).val() == 'All Fields' || $(this).val() == 'intg_projek.id') {
            $("#search_input").attr('name', 'field_value');
            $("#projects").attr('name', 'field_value_x');

            if ($(this).val() == 'intg_projek.id') {
                $("#search_input").attr('type', 'number');
            } else if ($(this).val() == 'All Fields') {
                $("#search_input").attr('type', 'text');
            }

            $('.field_value_select').hide();
            $('.field_value_input').show();
            waitingDialog.hide();
        } else {
            $("#search_input").attr('name', 'field_value_x');
            $("#projects").attr('name', 'field_value');
            $('.field_value_input').hide();
            $('.field_value_select').show();
            $.getJSON("<?php echo site_url(SITE_AREA . '/projectmgmt/projek/search') ?>",
                    {
                        type: $(this).val(),
                        ajax: 'true'
                    }, function (j) {

                $("#projects").select2("val", "");
                console.log(j);
                var options = '<option selected></option>';
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].rowid + '"> ' + j[i].selectedname + '</option>';
                }
                $("#projects").html(options);
                waitingDialog.hide();
            });
        }

    });



    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//

    $(document).ready(function () {
        LoadDiv();
        $('#subNav-Header').remove();
    });
    $('#nav').on('click', function () {
        setTimeout(clicked, 500);
    });

    function clicked() {
        LoadDiv();
    }


    $(window).resize(function () {
        LoadDiv();
    });

    function LoadDiv() {
        var height = $('body').height() - $('header').height() - $('.navbar-header').height() - $('.header').height() - $('#footer').height() - $('.filter_area').height() - $('.filter_area').height();//tolok tinggi header ngan nav-bar header.
        var width = $('#content').width() - 40;

        $('#dvData').css({'height': height});
        $('#dvData').css({'width': width});
        $('#dvData').css({'max-width': width});
        $('#dvData').css({'overflow-x': 'auto'});

    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Function To Auto set width and height of div<<<<<<<<<<<<<<<<<<<

</script>

