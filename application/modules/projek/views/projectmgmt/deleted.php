<style>
    #reset {
        -webkit-animation: flash 6s infinite linear;
        animation: flash 6s infinite linear;
    }
</style>
<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">

            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>
            <div class="row">
                <div class="col-md-6">
                    <h4>Projek</h4></div>
                <div class="col-md-3">

                    <select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

                        <? if ( $this->session->userdata('projekfield') !== NULL ) { ?>
                        <option  selected="selected" value="<?= $this->session->userdata('projekfield') ?>"><?= $this->session->userdata('projekfield') ?></option>
                        <? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
                        <option>All Fields</option>		
                        <?
                        $fquery = $this->db->query('SHOW COLUMNS FROM intg_projek')->result();
                        foreach  ( $fquery as $frow  ) {
                        ?>
                        <option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
                        <? } ?>
                    </select>
                </div><div class="col-md-3"><div class="input-group">
                        <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?= $this->session->userdata('projekfvalue') ?>">
                        <input type="hidden" name="field_name" id="field_name" value="<?= $this->session->userdata('projekfname') ?>"/>


                        <span class="input-group-btn">
                            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
                            <button type="submit" name="submit"   class="btn btn-primary btn-icon" <?= $this->session->userdata('projekfield') ? 'id="reset"' : '' ?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
                    </div></div></div>

            <?php echo form_close(); ?>
            <div class="table-responsive">   



                <?php echo form_open($this->uri->uri_string()); ?>
                <table class="table table-striped datagrid m-b-sm">
                    <thead>
                        <tr>
                            <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete') && isset($records) && is_array($records) && count($records)) : ?>
                                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                            <?php endif; ?>
                            <th>#</th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_tarikh_terima_permohonan') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_tarikh_terima_permohonan&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_tarikh_terima_permohonan') ? 'desc' : 'asc'); ?>'>
                                    Tarikh Terima Permohonan</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_pegawai_ulasan') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_pegawai_ulasan&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_pegawai_ulasan') ? 'desc' : 'asc'); ?>'>
                                    Pegawai Ulasan</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_nama_projek') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_nama_projek&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_nama_projek') ? 'desc' : 'asc'); ?>'>
                                    Nama Projek</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_daerah') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_daerah&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_daerah') ? 'desc' : 'asc'); ?>'>
                                    Daerah</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_no_rujukan') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_no_rujukan&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_no_rujukan') ? 'desc' : 'asc'); ?>'>
                                    No Rujukan</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_ukp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_jumlah_bayaran_fi_ukp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_ukp') ? 'desc' : 'asc'); ?>'>
                                    Jumlah</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_ukp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_tarikh_bayaran_fi_ukp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_ukp') ? 'desc' : 'asc'); ?>'>
                                    Tarikh</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_ukp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_no_resit_bayaran_fi_ukp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_ukp') ? 'desc' : 'asc'); ?>'>
                                    No. Resit</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_pmutk') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_jumlah_bayaran_fi_pmutk&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_pmutk') ? 'desc' : 'asc'); ?>'>
                                    Jumlah</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_pmutk') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_tarikh_bayaran_fi_pmutk&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_pmutk') ? 'desc' : 'asc'); ?>'>
                                    Tarikh</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_pmutk') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_no_resit_bayaran_fi_pmutk&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_pmutk') ? 'desc' : 'asc'); ?>'>
                                    No. Resit</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_pspp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_jumlah_bayaran_fi_pspp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_pspp') ? 'desc' : 'asc'); ?>'>
                                    Jumlah</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_pspp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_tarikh_bayaran_fi_pspp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_pspp') ? 'desc' : 'asc'); ?>'>
                                    Tarikh</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_pspp') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_no_resit_bayaran_fi_pspp&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_pspp') ? 'desc' : 'asc'); ?>'>
                                    No. Resit</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_ume') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_jumlah_bayaran_fi_ume&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_jumlah_bayaran_fi_ume') ? 'desc' : 'asc'); ?>'>
                                    Jumlah</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_ume') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_tarikh_bayaran_fi_ume&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_tarikh_bayaran_fi_ume') ? 'desc' : 'asc'); ?>'>
                                    Tarikh</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_ume') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_no_resit_bayaran_fi_ume&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_no_resit_bayaran_fi_ume') ? 'desc' : 'asc'); ?>'>
                                    No. Resit</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_nama_jurutera_perunding') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_nama_jurutera_perunding&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_nama_jurutera_perunding') ? 'desc' : 'asc'); ?>'>
                                    Nama Jurutera Perunding</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_nama_pemilik') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_nama_pemilik&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_nama_pemilik') ? 'desc' : 'asc'); ?>'>
                                    Nama Pemilik</a></th>
                            <th <?php
                            if ($this->input->get('sort_by') == 'projek' . '_fasa') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/projectmgmt/projek?sort_by=projek_fasa&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'projek' . '_fasa') ? 'desc' : 'asc'); ?>'>
                                    Fasa</a></th>
                            <th>Created</th>
                            <th>Modified</th>
                        </tr>
                    </thead>
                    <?php if (isset($records) && is_array($records) && count($records)) : ?>
                        <tfoot>
                            <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>
                                <tr>
                                    <td colspan="23">
                                        <?php echo lang('bf_with_selected') ?>
                                        <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">

                                                                 <!--	<input type="submit" name="purge" class="btn btn-danger" value="<?php //echo lang('bf_action_purge')        ?>" onclick="return confirm('<?php //echo lang('us_purge_del_confirm');        ?>')">-->

                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tfoot>
                    <?php endif; ?>
                    <tbody>
                        <?php if (isset($records) && is_array($records) && count($records)) : $no = $this->input->get('per_page') + 1; ?>
                            <?php foreach ($records as $record) : ?>
                                <tr>
                                    <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>
                                        <td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td><td><?= $no; ?></td>
                                    <?php endif; ?>

                                    <?php if ($this->auth->has_permission('Projek.Projectmgmt.Edit')) : ?>
                                        <td><?php echo anchor(SITE_AREA . '/projectmgmt/projek/restore_purge/' . $record->id, '<i class="icon-pencil">&nbsp;</i>' . $record->tarikh_terima_permohonan) ?></td>
                                    <?php else: ?>
                                        <td><?php echo $record->tarikh_terima_permohonan ?></td>
                                    <?php endif; ?>

                                    <td><?php echo $record->pegawai_ulasan ?></td>
                                    <td><?php echo $record->nama_projek ?></td>
                                    <td><?php echo $record->daerah ?></td>
                                    <td><?php echo $record->no_rujukan ?></td>
                                    <td><?php echo $record->jumlah_bayaran_fi_ukp ?></td>
                                    <td><?php echo $record->tarikh_bayaran_fi_ukp == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_bayaran_fi_ukp)); ?></td>
                                    <td><?php echo $record->no_resit_bayaran_fi_ukp ?></td>
                                    <td><?php echo $record->jumlah_bayaran_fi_pmutk ?></td>
                                    <td><?php echo $record->tarikh_bayaran_fi_pmutk == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_bayaran_fi_pmutk)); ?></td>
                                    <td><?php echo $record->no_resit_bayaran_fi_pmutk ?></td>
                                    <td><?php echo $record->jumlah_bayaran_fi_pspp ?></td>
                                    <td><?php echo $record->tarikh_bayaran_fi_pspp == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_bayaran_fi_pspp)); ?></td>
                                    <td><?php echo $record->no_resit_bayaran_fi_pspp ?></td>
                                    <td><?php echo $record->jumlah_bayaran_fi_ume ?></td>
                                    <td><?php echo $record->tarikh_bayaran_fi_ume == "0000-00-00" ? "" : date("d/m/Y", strtotime($record->tarikh_bayaran_fi_ume)); ?></td>
                                    <td><?php echo $record->no_resit_bayaran_fi_ume ?></td>
                                    <td><?php echo $record->nama_jurutera_perunding ?></td>
                                    <td><?php echo $record->nama_pemilik ?></td>
                                    <td><?php echo $record->fasa ?></td>
                                    <td><?php echo date("d/m/Y h:i:s", strtotime($record->created_on)); ?></td>
                                    <td><?php echo $record->modified_on == "0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s", strtotime($record->modified_on)); ?></td>
                                </tr>
                                <?php
                                $no++;
                            endforeach;
                            ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="23">No records found that match your selection.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>

                <?php echo form_close(); ?>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">


                        </div>
                        <div class="col-sm-4 text-center">
                            <small class="text-muted inline m-t-sm m-b-sm">Showing <?= $offset + 1 ?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                        </div>
                        <div class="col-sm-4 text-right text-center-xs">                

                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </footer>                
            </div>


        </div>
    </section>
</div>