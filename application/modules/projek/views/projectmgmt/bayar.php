<?php
$projek_status_terkini_projek_his = array();

if (isset($activities) && is_array($activities)) {
    foreach ($activities as $history) {
        $projek_status_terkini_projek_his['record'][] = json_decode($history->formdata, True)['projek_status_terkini_projek'];
        $projek_status_terkini_projek_his['date_changed'][] = $history->created_on;
    }
}


$stp_list = array();
foreach ($status_terkini_projek as $stp) {
    $stp_list[$stp->id] = $stp->nama;
}

$Admin_Can_Edit = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Edit');
$Admin_Can_Delete = $this->auth->has_permission('Projek.Projectmgmt.Admin_Can_Delete');

$vo = isset($view_only) ? $view_only : false;
$eo = isset($edit_only) ? $edit_only : false;


$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projek)) {
    $projek = (array) $projek;
}
$id = isset($projek['id']) ? $projek['id'] : '';
$created_by = isset($projek['created_by']) ? $projek['created_by'] : '';
?>

<style type="text/css">

    textarea{
        text-transform: uppercase;
    }

</style>
<script>

    function check_lat_range() //to ensure user enters latitude value between -90 and 90 only
    {
        var lat = document.getElementById('project_latitude').value;
        if (lat < -90 || lat > 90)
        {
            alert('Allowed Latitude range is between -90 and 90 only');
            $('#projek_latitude').val(5.328746510076078);
            //return false;
        }
    }

    function check_lng_range() //to ensure user enters longitude value between -180 and 180 only
    {
        var lng = document.getElementById('projek_longitude').value;
        if (lng < -180 || lng > 180)
        {
            alert('Allowed Longitude range is between -180 and 180 only');
            $('#projek_longitude').val(103.13797193202981);
            return false;
        }
    }

</script>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold"> Projek 
                <span class="pull-right">
                    <?php if ($id) { ?>

                        ID Projek = <?= $id ?>

                    <?php } ?>
                </span> 
            </header>
            <div class="panel-body">

                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group <?php echo form_error('nama_projek') ? 'error' : ''; ?>">
                            <?php echo form_label('Nama Projek' . lang('bf_form_label_required'), 'projek_nama_projek', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                echo isset($projek['nama_projek']) ? $projek['nama_projek'] : 'N/A';
                                ?>

                                <span class='help-inline'><?php echo form_error('nama_projek'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Tarikh Terima Permohonan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <?php
                                echo isset($projek['tarikh_terima_permohonan']) && $projek['tarikh_terima_permohonan'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_terima_permohonan'])) : 'N/A';
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Pegawai Ulasan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>

                                <?php
                                foreach ($users as $user) {
                                    if (in_array($user->id, explode(",", $projek['pegawai_ulasan']))) {
                                        echo $user->display_name . ' <br>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Status Permohonan' ?></label>
                            <div class='controls'>
                                <?php
                                foreach ($status_permohonan as $sp) {
                                    if ($sp->id == $projek['status_permohonan']) {
                                        echo $sp->nama;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Daerah' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>

                                <?php
                                foreach ($daerah as $n) {
                                    if ($n->id == $projek['daerah']) {
                                        echo $n->nama;
                                    }
                                }
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('no_rujukan') ? 'error' : ''; ?>">
                            <label><?= 'No Rujukan Fail' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <?php
                                echo isset($projek['no_rujukan']) ? $projek['no_rujukan'] : 'N/A';
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Status Terkini Projek' ?></label>
                            <div class='controls'>


                                <?php
                                foreach ($status_terkini_projek as $stp) {
                                    if ($stp->id == $projek['status_terkini_projek']) {
                                        echo $stp->nama;
                                    }
                                }
                                ?>


                                <?php
                                if (isset($projek_status_terkini_projek_his['record'])) {
                                    $len = count($projek_status_terkini_projek_his['record']);
                                    ?>

                                    <a id="popover_pstp" class="btn" rel="popover" data-content="" title="History"><i class="fa fa-history" ></i></a>

                                    <script>
                                        $(document).ready(function () {
                                            var html = '<?php
                                echo '<br>';


                                echo '<table class="table table-condensed table-border table-hover">';
                                echo '<thead>';
                                echo '<tr>';
                                echo '<th>Status</th>';
                                echo '<th>Tarikh Kemaskini</th>';
                                echo '</tr>';
                                echo '</thead>';


                                for ($i = 0; $i < $len; $i++) {
                                    echo '<tr><td>';
                                    echo $stp_list[$projek_status_terkini_projek_his['record'][$i]] . '</td><td>' . $projek_status_terkini_projek_his['date_changed'][$i] . '<br>';
                                    echo '</td></tr>';
                                }

                                echo '</table>';
                                ?>';
                                            $('#popover_pstp').popover({placement: 'bottom', content: html, html: true});
                                        });

                                    </script>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Nama Jurutera Perunding' ?></label>
                            <div class='controls'>

                                <?php
                                if ($vo) {
                                    foreach ($jurutera_perunding as $user) {
                                        if (in_array($user->id, explode(",", $projek['nama_jurutera_perunding']))) {
                                            echo $user->nama . ' <br>';
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('nama_pemilik') ? 'error' : ''; ?>">
                            <label><?= 'Nama Pemilik' . lang('bf_form_label_required') ?></label><div class='controls'>
                                <?php
                                echo isset($projek['nama_pemilik']) ? $projek['nama_pemilik'] : 'N/A';
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                            <?php echo form_label('Jumlah S. Modal Dikenakan', 'projek_bsm_jumlah_perlu_dijelaskan', array('class' => 'control-label')); ?>
                            <div class='controls'>
                                <?php
                                echo isset($projek['bsm_jumlah_perlu_dijelaskan']) ? 'RM' . number_format($projek['bsm_jumlah_perlu_dijelaskan'], 2) : 'N/A';
                                ?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><?= 'Tarikh Kelulusan Permohonan' ?></label>
                            <div class='controls'>
                                <?php
                                echo isset($projek['tarikh_kelulusan_permohonan']) && $projek['tarikh_kelulusan_permohonan'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_kelulusan_permohonan'])) : 'N/A';
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <!--                        <div class="form-group">
                                                    <label>Tarikh Tamat Tempoh</label>
                                                    <div class='controls' id="tarikh_tamat_tempoh">
                        
                                                    </div>
                                                </div>-->

                        <div class="form-group">
                            <label><?= 'Tarikh Tamat Tempoh' ?></label>
                            <div class='controls'>
                                <?php
                                echo isset($projek['tarikh_tamat_tempoh']) && $projek['tarikh_tamat_tempoh'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_tamat_tempoh'])) : 'N/A';
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4" id='div_projek_tarikh_tamat_lanjutan_tempoh_masa'>
                        <div class="form-group">
                            <label><?= 'Tarikh Tamat Lanjutan Tempoh Masa' ?></label>
                            <div class='controls'>
                                <?php
                                echo isset($projek['tarikh_tamat_lanjutan_tempoh_masa']) && $projek['tarikh_tamat_lanjutan_tempoh_masa'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_tamat_lanjutan_tempoh_masa'])) : 'N/A';
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group <?php echo form_error('komen_ulasan') ? 'error' : ''; ?>">
                            <hr>
                            <?php echo form_label('Komen/Ulasan', 'projek_komen_ulasan', array('class' => 'control-label')); ?>
                            <div class='controls  bg-white-only'>


                                <?php
                                echo isset($projek['komen_ulasan']) ? $projek['komen_ulasan'] : 'N/A';
                                ?>

                                <hr>
                                <span class='help-inline'><?php echo form_error('komen_ulasan'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <?php /* Start Another section */ ?>
                <br>

                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#bayaran" aria-controls="bayaran" role="tab" data-toggle="tab">Bayaran</a></li>
                    <li role="presentation"><a href="#lokasi" aria-controls="lokasi" role="tab" data-toggle="tab">Lokasi</a></li>
                    <!--<li role="presentation"><a href="#lokasi2" aria-controls="lokasi2" role="tab" data-toggle="tab">lokasi2</a></li>-->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="bayaran">


                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">

                                    <?php
                                    foreach ($jenis_bayaran as $jns_byrn) {

                                        $bsm_jumlah_perlu_dijelaskan = isset($projek['bsm_jumlah_perlu_dijelaskan']) && $projek['bsm_jumlah_perlu_dijelaskan'] != '' ? $projek['bsm_jumlah_perlu_dijelaskan'] : '0';
                                        $patut_bayar = ($bsm_jumlah_perlu_dijelaskan / 100) * $jns_byrn->formula;

                                        $class = 'default';
                                        if ($jns_byrn->formula == "") {
                                            $class = 'default';
                                            $arr = check_payment_amount($jns_byrn->id, isset($projek['id']) ? $projek['id'] : '');
                                            $paid_amount = $arr['paid_amount'];
                                        } else {

                                            $arr = check_payment_amount($jns_byrn->id, isset($projek['id']) ? $projek['id'] : '', $patut_bayar);
                                            $class = $arr['class'];
                                            $paid_amount = $arr['paid_amount'];
                                        }
                                        ?>
                                        <div formula="<?= $jns_byrn->formula ?>" class="panel  panel-<?= $class ?>">
                                            <div class="panel-heading" >
                                                <div class="panel-title" style="cursor: pointer">
                                                    <a data-toggle="collapse" href="#collapse<?= $jns_byrn->id ?>" style="cursor: pointer">
                                                        <label style="cursor: pointer"><?= $jns_byrn->nama ?></label>
                                                    </a>



                                                </div>
                                            </div>
                                            <div id="collapse<?= $jns_byrn->id ?>" class="panel-collapse collapse in">
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Jumlah Bayaran </label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Tarikh Bayaran </label>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label> No. Resit Bayaran </label>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $id_projek = $this->uri->segment(5);
                                                    $bayaran = $this->db->order_by('id', 'ASC')->get_where($this->db->dbprefix('bayaran'), array("jenis_bayaran" => $jns_byrn->id, "deleted" => "0", "id_projek" => $id_projek))->result();
                                                    $atno = -1;
//        echo $this->db->last_query();

                                                    foreach ($bayaran as $rec) {
                                                        $atno++;
                                                        ?>
                                                        <div class="row row-fluid checkRow " id="row_<?php e($atno) ?>_<?= $jns_byrn->id ?>">
                                                            <!--                <div class="col-md-1">
                                                            <?= $atno ?>
                                                                            </div>-->
                                                            <input type="hidden" name="rowid_bayaran_<?= $jns_byrn->id ?>[]" value="<?= $rec->id ?>"/>
                                                            <div class="col-md-3">
                                                                <?php
                                                                if ($vo) {
                                                                    echo 'RM' . number_format($rec->jumlah_bayaran, 2);
                                                                } else {
                                                                    ?>
                                                                    <input type="number" step=".01" placeholder="Jumlah Bayaran" class="input-sm  form-control" id="jumlah_bayaran_<?= e($atno) ?>_<?= $jns_byrn->id ?>" name="jumlah_bayaran_<?= $jns_byrn->id ?>[]"  value="<?= number_format($rec->jumlah_bayaran, 2, '.', '') ?>" required="required" min="0"  />
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-3 ">
                                                                <?php
                                                                if ($vo) {
                                                                    echo $rec->tarikh_bayaran;
                                                                } else {
                                                                    ?>
                                                                    <input type="text"  class="input-sm  form-control date_picker" placeholder="Tarikh Bayaran" name="tarikh_bayaran_<?= $jns_byrn->id ?>[]"  id="tarikh_bayaran_<?php e($atno) ?>_<?= $jns_byrn->id ?>"  value="<?= e($rec->tarikh_bayaran) ?>" required="required" />
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>  

                                                            <div class="col-md-3 ">
                                                                <?php
                                                                if ($vo) {
                                                                    echo $rec->no_resit_bayaran;
                                                                } else {
                                                                    ?>
                                                                    <input type="text"  class="input-sm  form-control" placeholder="No. Resit Bayaran" name="no_resit_bayaran_<?= $jns_byrn->id ?>[]"  id="no_resit_bayaran_<?php e($atno) ?>_<?= $jns_byrn->id ?>"  value="<?= e($rec->no_resit_bayaran) ?>" required="required"/>
                                                                    <?php
                                                                    //echo $rec->id;
                                                                }
                                                                ?>
                                                            </div>  

                                                            <div class="col-md-2 ">
                                                                <?php
                                                                if ($vo == false) {
//                                                                    if ($atno >= 1) {
                                                                    ?>
                                                                    <a href="javascript:void(0)" >
                                                                        <i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick="remrow('<?= $jns_byrn->id ?>', '<?= $atno ?>', '<?= $rec->id ?>')"></i>
                                                                    </a>
                                                                    <?php
//                                                                    }
                                                                }
                                                                ?>   
                                                            </div>
                                                            <br>
                                                        </div> 
                                                    <?php } ?>

                                                    <?php if ($vo == false) { ?>
                                                        <div id="lastrow_<?= $jns_byrn->id ?>"  counter="<?= e($atno) ?>"></div>
                                                        <div class="row"> 
                                                            <div class="col-md-2">
                                                                <a href="javascript:void(0)" onclick="addnewrow(<?= $jns_byrn->id ?>)"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Tambah</strong></a>
                                                            </div>
                                                        </div>
                                                    <?php } ?>


                                                </div>


                                                <div class="panel-footer">
                                                    <?php
                                                    echo $jns_byrn->formula == "" ? "" : 'Jumlah Patut Bayar : RM' . number_format($bsm_jumlah_perlu_dijelaskan, 2) . ' x ' . $jns_byrn->formula . '% = RM' . number_format($patut_bayar, 2);

                                                    $lebih_byr = 0;
                                                    if ($paid_amount > $patut_bayar) {
                                                        $lebih_byr = $paid_amount - $patut_bayar;
                                                    }
                                                    ?>
                                                    <br>Telah dibayar: RM<?= number_format($paid_amount, 2) ?> <?= $lebih_byr != 0 && $jns_byrn->formula != "" ? '(Lebih Bayar RM' . number_format($lebih_byr, 2) . ')' : '' ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <br><br>
                                </div>
                            </div>
                        </div>



                    </div>

                    <div role="tabpanel" class="tab-pane" id="lokasi">


                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="panel panel-default">
                                    <div class="panel-heading font-bold">
                                        <a class="accordion-toggle show" data-toggle="collapse" href="#collapseSeven">

                                        </a>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group<?php echo form_error('location') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Lokasi Projek', 'projek_location', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='input-md  form-control' type='text' name='projek_location' id='projek_location' maxlength="255" value="" />
                                                            <span class='help-inline'><?php echo form_error('location'); ?></span>
                                                        </div>
                                                    </div>                                            
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group<?php echo form_error('projek_latitude') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Latitude', 'projek_latitude', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='  form-control'  type='text' name='projek_latitude' id='projek_latitude' maxlength="255"  value="" onblur="check_lat_range()" />
                                                            <span class='help-inline'><?php echo form_error('projek_latitude'); ?></span>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group<?php echo form_error('projek_longitude') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Longitude', 'projek_longitude', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='  form-control'  type='text' name='projek_longitude' id='projek_longitude' maxlength="255"  value="" onblur="check_lng_range()" />
                                                            <span class='help-inline'><?php echo form_error('projek_longitude'); ?></span>
                                                        </div>
                                                    </div> 
                                                </div>                                                   
                                            </div><!-- row -->

                                            <div class="row" style="position:relative;">
                                                <div class="map_overlay">&nbsp;</div>

                                                <div id="us2" style="width: 100%; height: 400px;"></div>

                                                <div class="clearfix"></div>
                                            </div>
                                            <style>
                                                .map_overlay{
                                                    width: 100%;
                                                    height:100%;
                                                    position: absolute;
                                                    left: 0;
                                                    top: 0;
                                                    z-index: 100;
                                                }
                                            </style>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('.map_overlay').on('click', function () {
                                                        $(this).css({zIndex: '0'});
                                                    });
                                                });
                                            </script>

                                            <?php
                                            if (isset($projek['latitude']) and intval($projek['latitude']) != "") {
                                                $latitude = $projek['latitude'];
                                            } else {
                                                $latitude = 5.328746510076078;
                                            }
                                            if (isset($projek['longitude']) and intval($projek['longitude']) != "") {
                                                $longitude = $projek['longitude'];
                                            } else {
                                                $longitude = 103.13797193202981;
                                            }
                                            ?>

                                            <script type="text/javascript" src='<?php echo "https://maps.google.com/maps/api/js?libraries=places&key=" . $this->config->item('googleMapAPIKey'); ?>'></script>
                                            <script type="text/javascript" src='<?php echo Template::theme_url('js/locationpicker.jquery.js'); ?>'></script>

                                            <script>
//                                                map.setOptions({draggable: false});
                                                $('#us2').locationpicker({
                                                    location: {
                                                        latitude: <?php echo $latitude; ?>,
                                                        longitude: <?php echo $longitude; ?>
                                                    },
                                                    radius: 10,
                                                    inputBinding: {
                                                        latitudeInput: $('#projek_latitude'),
                                                        longitudeInput: $('#projek_longitude'),
                                                        locationNameInput: $('#projek_location')
                                                    },
                                                    enableAutocomplete: true,

                                                    scrollwheel: <?= $vo ? 'false' : 'true' ?>,
                                                    draggable: <?= $vo ? 'false' : 'true' ?>,
                                                    mapTypeId: google.maps.MapTypeId.SATELLITE,

                                                });
                                            </script>
                                        </div>
                                    </div><!-- panel-body -->
                                </div>


                            </div>
                        </div>
                    </div>






                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <?php
                        $create = '';
                        if (!$vo) {
                            if ($this->uri->segment(4) == 'create') {
                                $create = true;
                                ?>
                                <div class="form-actions">
                                    <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projek_action_create'); ?>"  />
                                    &nbsp;&nbsp;
                                    <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>

                                </div>
                                <?php
                            } else {
                                $create = false;
                                ?>
                                <div class="form-actions">
                                    <input type="submit" name="save" class="btn btn-primary cannot_edit" value="<?php echo lang('projek_action_edit'); ?>"  />
                                    &nbsp;&nbsp;
                                    <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning cannot_edit"'); ?>

                                    <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>
                                        &nbsp;&nbsp;
                                        <button type="submit" name="delete" class="btn btn-danger cannot_edit" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('projek_delete_confirm'))); ?>');">
                                            <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('projek_delete_record'); ?>
                                        </button>
                                    <?php endif; ?>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>

                </div>

                <?php echo form_close(); ?>
            </div>
        </section>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $('#div_projek_tarikh_tamat_lanjutan_tempoh_masa').hide();
        initDatePicker();

<?php
if ($vo) {
    
} else {
    ?>
            CKEDITOR.replace('projek_komen_ulasan');
<?php } ?>

        $("input[type=text]").keyup(function () {
            $(this).val($(this).val().toUpperCase());
        });

<?php
if ($Admin_Can_Edit) {
    
} else if ($this->auth->user_id() != $created_by && $create != true) {
    ?>
            $('#forma input').attr('readonly', 'readonly');
            $("#forma :input").attr("disabled", true);
            $(".cannot_edit").remove();
<?php }
?>
        $('#projek_tarikh_kelulusan_permohonan').datepicker()
                .on('changeDate', function (ev) {
                    var dt = getDateAfter(moment(ev.date).format('YYYY-MM-DD'), 24);
                    $('#projek_tarikh_tamat_tempoh').datepicker('setValue', dt);
                });

<?php if (isset($projek['status_terkini_projek']) && $projek['status_terkini_projek'] == '1') { ?>
            $('#div_projek_tarikh_tamat_lanjutan_tempoh_masa').show();
<?php } ?>


    });




    function addnewrow(type)
    {
        var theId = parseInt($("#lastrow_" + type).attr("counter")) + 1;


        $("#lastrow_" + type).attr("counter", parseInt(theId));

        var row = '<div class="row fadeInUp animated checkRow" id="row_' + theId + '_' + type + '" >' +
                '<input type="hidden" name="rowid_bayaran_' + type + '[]"/>'
                + '<div class="col-md-3 "><input type="number" step=".01" class="input-sm  form-control" placeholder="Jumlah Bayaran" name="jumlah_bayaran_' + type + '[]"  id="jumlah_bayaran_' + theId + '_' + type + '" required="required"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="date_picker form-control input-sm" placeholder="Tarikh Bayaran" name="tarikh_bayaran_' + type + '[]"  id="tarikh_bayaran_' + theId + '_' + type + '" required="required"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="input-sm  form-control" placeholder="No Resit Bayaran" name="no_resit_bayaran_' + type + '[]"  id="no_resit_bayaran_' + theId + '_' + type + '" ></div>'
                + '<div class="col-md-3 "><a href="javascript:void(0)" ><i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick=remTemp("#row_' + theId + '_' + type + '","' + type + '")></i></a><br></div>';

        $('#lastrow_' + type).before(row);
        initDatePicker();
    }

    function remrow(jenis_byr, count, rec_id) //val, id,type
    {
        var result = confirm("Confirm delete?");
        waitingDialog.show("Please wait...");
        if (result) {
            //Logic to delete the item
//            var theId = parseInt($("#lastrow_" + jenis_byr).attr("counter")) - 1;
            $("#row_" + count + '_' + jenis_byr).slideUp('slow', function () {

//                $("#lastrow_" + jenis_byr).attr("counter", theId);

                $.post("", {action: "deleteBayaran", id: rec_id},
                        function (data) {
                            var dt = JSON.parse(data);
                            waitingDialog.show(dt.status_msg);
                            waitingDialog.hide();
                        });

                $("#row_" + count + '_' + jenis_byr).remove();

            });
            setTimeout(function () {

            }, 500);
        } else {
            waitingDialog.hide();
        }

    }

    function remTemp(val, jns_bayaran) {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
//            var theId = parseInt($("#lastrow_" + jns_bayaran).attr("counter")) - 1;
            $(val).slideUp('slow', function () {
                $(val).remove();
//                $("#lastrow_" + jns_bayaran).attr("counter", theId);
            });
            setTimeout(function () {
            }, 500);
        }
    }



    function initDatePicker() {

        $('.date_picker').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            startDate: new Date()
        });
    }

    function getDateAfter(whichdate, monthcount) {
        console.log(whichdate);

        var futureDate = moment(new Date(whichdate)).add(monthcount, 'months');

        return futureDate.format('DD-MM-YYYY');

    }


    $("#projek_status_terkini_projek").change(function () {
        if ($(this).val() == '1') {


            if ($('#projek_tarikh_tamat_tempoh').val() == '') {
                alert('Sila tetapkan Tarikh Kelulusan Pelan & Tarikh Tamat Tempoh dahulu!');

                $('#projek_tarikh_kelulusan_permohonan').focus();
                $('#projek_status_terkini_projek').select2("val", "");
                $('#projek_tarikh_tamat_lanjutan_tempoh_masa').val('').datepicker('update');
            } else {
                waitingDialog.show('Loading...');
                $('#div_projek_tarikh_tamat_lanjutan_tempoh_masa').show();
                var dt = getDateAfter(moment($('#projek_tarikh_tamat_tempoh').val(), 'DD-MM-YYYY').format('YYYY-MM-DD'), 12);
                $('#projek_tarikh_tamat_lanjutan_tempoh_masa').datepicker('setValue', dt);
                waitingDialog.hide();
            }

        } else {
            $('#projek_tarikh_tamat_lanjutan_tempoh_masa').val('').datepicker('update');
            $('#div_projek_tarikh_tamat_lanjutan_tempoh_masa').hide();

        }

    });

</script>