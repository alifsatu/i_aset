<?php
$puli = '7';
$vo = isset($view_only) ? $view_only : false;
$eo = isset($edit_only) ? $edit_only : false;

$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projek)) {
    $projek = (array) $projek;
}
$id = isset($projek['id']) ? $projek['id'] : '';
?>
<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Projek</header>
            <div class="panel-body">

                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>

                <fieldset>
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Tarikh Terima Permohonan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_terima_permohonan' type='text' name='projek_tarikh_terima_permohonan'  value="<?php echo isset($projek['tarikh_terima_permohonan']) ? date("d-m-Y", strtotime($projek['tarikh_terima_permohonan'])) : ''; ?>"  data-required="true" readonly="readonly" required="true"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label><?= 'Pegawai Ulasan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <select name="projek_pegawai_ulasan" id="projek_pegawai_ulasan" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" required="true">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($users as $user) {
                                        if (in_array($user->id, explode(",", $projek['pegawai_ulasan']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->display_name; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $user->id ?>"><?= $user->display_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('nama_projek') ? 'error' : ''; ?>">
                                <?php echo form_label('Nama Projek' . lang('bf_form_label_required'), 'projek_nama_projek', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_nama_projek' class='input-sm input-s  form-control' type='text' name='projek_nama_projek' maxlength="1000" value="<?php echo set_value('projek_nama_projek', isset($projek['nama_projek']) ? $projek['nama_projek'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('nama_projek'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Daerah' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <select name="projek_daerah" id="projek_daerah" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($daerah as $n) {
                                        if ($n->id == $projek['id']) {
                                            ?>
                                            <option selected="selected" value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('no_rujukan') ? 'error' : ''; ?>">
                                <?php echo form_label('No Rujukan' . lang('bf_form_label_required'), 'projek_no_rujukan', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_no_rujukan' class='input-sm input-s  form-control' type='text' name='projek_no_rujukan' maxlength="255" value="<?php echo set_value('projek_no_rujukan', isset($projek['no_rujukan']) ? $projek['no_rujukan'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('no_rujukan'); ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                                <?php echo form_label('Fasa', 'projek_fasa', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_fasa' class='input-sm input-s  form-control' type='text' name='projek_fasa' maxlength="11" value="<?php echo set_value('projek_fasa', isset($projek['fasa']) ? $projek['fasa'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('fasa'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Nama Jurutera Perunding' ?></label>
                            <div class='controls'>

                                <select multiple name="projek_nama_jurutera_perunding[]" id="projek_nama_jurutera_perunding" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" >
                                    <?php
                                    foreach ($jurutera_perunding as $user):
                                        if (in_array($user->id, explode(",", $projek['nama_jurutera_perunding']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->nama; ?></option><?php } else {
                                            ?>
                                            <option value="<?= $user->id ?>"><?= $user->nama; ?> </option>
                                        <?php }
                                        ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">

                             <!--<label><?= 'Nama Pemilik' ?></label>
                                                       <div class='controls'>
                                                            <select multiple name="projek_nama_pemilik[]" id="projek_nama_pemilik" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
                            <?php
                            foreach ($pemilik as $user) {
                                if (in_array($user->id, explode(",", $projek['nama_pemilik']))) {
                                    ?>
                                                                                        <option selected="selected" value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                <?php } else { ?>
                                                                                        <option value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                    <?php
                                }
                            }
                            ?>
                                                            </select>
                                                        </div>-->

                            <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                                <?php echo form_label('Nama Pemilik*', 'projek_nama_pemilik', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_nama_pemilik' class='input-sm input-s  form-control' type='text' name='projek_nama_pemilik' maxlength="255" value="<?php echo set_value('projek_nama_pemilik', isset($projek['nama_pemilik']) ? $projek['nama_pemilik'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('projek_nama_pemilik'); ?></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                                <?php echo form_label('Jumlah Perlu Dijelaskan(RM)*', 'projek_bsm_jumlah_perlu_dijelaskan', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_bsm_jumlah_perlu_dijelaskan' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_bsm_jumlah_perlu_dijelaskan' maxlength="11" value="<?php echo set_value('projek_bsm_jumlah_perlu_dijelaskan', isset($projek['bsm_jumlah_perlu_dijelaskan']) ? $projek['bsm_jumlah_perlu_dijelaskan'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('bsm_jumlah_perlu_dijelaskan'); ?></span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>
                    <hr>
                    <br>

                    <?php /* Start Another section */ ?>


                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= check_payment_amount($projek['bsm_jumlah_perlu_dijelaskan'] * 0.1, $projek['bsm_jumlah_10']); ?>">
                            <div class="panel-heading font-bold">BAYARAN SUMBANGAN MODAL 10% (lulus pelan)</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_jumlah_10') ? 'error' : ''; ?>">
                                            <?php echo form_label('Jumlah Bayaran', 'projek_bsm_jumlah_10', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_jumlah_10' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_bsm_jumlah_10' maxlength="255" value="<?php echo set_value('projek_bsm_jumlah_10', isset($projek['bsm_jumlah_10']) ? $projek['bsm_jumlah_10'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_jumlah_10'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">                             
                                        <label>Tarikh Bayaran</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_bsm_tarikh_10' type='text' name='projek_bsm_tarikh_10'  value="<?php echo isset($projek['bsm_tarikh_10']) && $projek['bsm_tarikh_10'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['bsm_tarikh_10'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_resit_10') ? 'error' : ''; ?>">
                                            <?php echo form_label('No. Resit Bayaran', 'projek_bsm_resit_10', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_resit_10' class='input-sm input-s  form-control' type='text' name='projek_bsm_resit_10' maxlength="255" value="<?php echo set_value('projek_bsm_resit_10', isset($projek['bsm_resit_10']) ? $projek['bsm_resit_10'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_resit_10'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer">
                                <?php echo isset($projek['bsm_jumlah_perlu_dijelaskan']) ? 'Jumlah Patut Bayar :<br> RM' . $projek['bsm_jumlah_perlu_dijelaskan'] . ' x 10% = RM' . ($projek['bsm_jumlah_perlu_dijelaskan'] * 0.1) : ''; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= check_payment_amount($projek['bsm_jumlah_perlu_dijelaskan'] * 0.3, $projek['bsm_jumlah_30']); ?>">
                            <div class="panel-heading font-bold">BAYARAN SUMBANGAN MODAL 30% (9 bulan)</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_jumlah_30') ? 'error' : ''; ?>">
                                            <?php echo form_label('Jumlah Bayaran', 'projek_bsm_jumlah_30', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_jumlah_30' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_bsm_jumlah_30' maxlength="255" value="<?php echo set_value('projek_bsm_jumlah_30', isset($projek['bsm_jumlah_30']) ? $projek['bsm_jumlah_30'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_jumlah_30'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">                             
                                        <label>Tarikh Bayaran</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_bsm_tarikh_30' type='text' name='projek_bsm_tarikh_30'  value="<?php echo isset($projek['bsm_tarikh_30']) && $projek['bsm_tarikh_30'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['bsm_tarikh_30'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_resit_30') ? 'error' : ''; ?>">
                                            <?php echo form_label('No. Resit Bayaran', 'projek_bsm_resit_30', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_resit_30' class='input-sm input-s  form-control' type='text' name='projek_bsm_resit_30' maxlength="255" value="<?php echo set_value('projek_bsm_resit_30', isset($projek['bsm_resit_30']) ? $projek['bsm_resit_30'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_resit_30'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer">
                                <?php echo isset($projek['bsm_jumlah_perlu_dijelaskan']) ? 'Jumlah Patut Bayar :<br> RM' . $projek['bsm_jumlah_perlu_dijelaskan'] . ' x 30% = RM' . ($projek['bsm_jumlah_perlu_dijelaskan'] * 0.3) : ''; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= check_payment_amount($projek['bsm_jumlah_perlu_dijelaskan'] * 0.6, $projek['bsm_jumlah_60']); ?>">
                            <div class="panel-heading font-bold">BAYARAN SUMBANGAN MODAL 60% (sebelum kelulusan CCC)</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_jumlah_60') ? 'error' : ''; ?>">
                                            <?php echo form_label('Jumlah Bayaran', 'projek_bsm_jumlah_60', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_jumlah_60' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_bsm_jumlah_60' maxlength="255" value="<?php echo set_value('projek_bsm_jumlah_60', isset($projek['bsm_jumlah_60']) ? $projek['bsm_jumlah_60'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_jumlah_60'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">                             
                                        <label>Tarikh Bayaran</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_bsm_tarikh_60' type='text' name='projek_bsm_tarikh_60'  value="<?php echo isset($projek['bsm_tarikh_60']) && $projek['bsm_tarikh_60'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['bsm_tarikh_60'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('bsm_resit_60') ? 'error' : ''; ?>">
                                            <?php echo form_label('No. Resit Bayaran', 'projek_bsm_resit_60', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_bsm_resit_60' class='input-sm input-s  form-control' type='text' name='projek_bsm_resit_60' maxlength="255" value="<?php echo set_value('projek_bsm_resit_60', isset($projek['bsm_resit_60']) ? $projek['bsm_resit_60'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('bsm_resit_10'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer">
                                <?php echo isset($projek['bsm_jumlah_perlu_dijelaskan']) ? 'Jumlah Patut Bayar :<br> RM' . $projek['bsm_jumlah_perlu_dijelaskan'] . ' x 60% = RM' . ($projek['bsm_jumlah_perlu_dijelaskan'] * 0.6) : ''; ?>
                            </div>
                        </div>
                    </div> 





                </fieldset>

                <br>
                <hr>
                <br>
                <fieldset>


                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= isset($projek['jumlah_bayaran_fi_ukp']) && $projek['jumlah_bayaran_fi_ukp'] == '' ? 'danger' : 'primary' ?>">
                            <div class="panel-heading font-bold">BAYARAN FI ULASAN  KELULUSAN PELAN</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('jumlah_bayaran_fi_ukp') ? 'error' : ''; ?>">
                                            <?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_ukp', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_jumlah_bayaran_fi_ukp' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_jumlah_bayaran_fi_ukp' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_ukp', isset($projek['jumlah_bayaran_fi_ukp']) ? $projek['jumlah_bayaran_fi_ukp'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_ukp'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">                             
                                        <label>Tarikh</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_bayaran_fi_ukp' type='text' name='projek_tarikh_bayaran_fi_ukp'  value="<?php echo isset($projek['tarikh_bayaran_fi_ukp']) && $projek['tarikh_bayaran_fi_ukp'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_bayaran_fi_ukp'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('no_resit_bayaran_fi_ukp') ? 'error' : ''; ?>">
                                            <?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_ukp', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_no_resit_bayaran_fi_ukp' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_ukp' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_ukp', isset($projek['no_resit_bayaran_fi_ukp']) ? $projek['no_resit_bayaran_fi_ukp'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_ukp'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $atno = 1;
                                if ($vo == false) {
                                    ?>
                                    <div id="lastrow_<?= $puli ?>_ukp"  counter="<?= e($atno) ?>"></div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <a href="javascript:void(0)" onclick="addnewrow('ukp')"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Tambah Baris</strong></a>
                                        </div>
                                    </div>
<?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= isset($projek['jumlah_bayaran_fi_pmutk']) && $projek['jumlah_bayaran_fi_pmutk'] == '' ? 'danger' : 'primary' ?>">
                            <div class="panel-heading font-bold">JUMLAH BAYARAN FI PEMERIKSAAN MATERIAL, UJIAN TEKANAN & KEBOCORAN</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('jumlah_bayaran_fi_pmutk') ? 'error' : ''; ?>">
<?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_pmutk', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_jumlah_bayaran_fi_pmutk' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_jumlah_bayaran_fi_pmutk' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_pmutk', isset($projek['jumlah_bayaran_fi_pmutk']) ? $projek['jumlah_bayaran_fi_pmutk'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_pmutk'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Tarikh</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_bayaran_fi_pmutk' type='text' name='projek_tarikh_bayaran_fi_pmutk'  value="<?php echo isset($projek['tarikh_bayaran_fi_pmutk']) && $projek['tarikh_bayaran_fi_pmutk'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_bayaran_fi_pmutk'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('no_resit_bayaran_fi_pmutk') ? 'error' : ''; ?>">
<?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_pmutk', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_no_resit_bayaran_fi_pmutk' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_pmutk' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_pmutk', isset($projek['no_resit_bayaran_fi_pmutk']) ? $projek['no_resit_bayaran_fi_pmutk'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_pmutk'); ?></span>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <?php
                                $atno = 1;
                                if ($vo == false) {
                                    ?>
                                    <div id="lastrow_<?= $puli ?>_pmutk"  counter="<?= e($atno) ?>"></div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <a href="javascript:void(0)" onclick="addnewrow('pmutk')"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Tambah Baris</strong></a>
                                        </div>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= isset($projek['jumlah_bayaran_fi_pspp']) && $projek['jumlah_bayaran_fi_pspp'] == '' ? 'danger' : 'primary' ?>">
                            <div class="panel-heading font-bold">JUMLAH BAYARAN FI PENYELIAAN SAMBUNGAN, PENSTERILAN & PENYAHAIRAN</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('jumlah_bayaran_fi_pspp') ? 'error' : ''; ?>">
<?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_pspp', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_jumlah_bayaran_fi_pspp' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_jumlah_bayaran_fi_pspp' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_pspp', isset($projek['jumlah_bayaran_fi_pspp']) ? $projek['jumlah_bayaran_fi_pspp'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_pspp'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Tarikh</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_bayaran_fi_pspp' type='text' name='projek_tarikh_bayaran_fi_pspp'  value="<?php echo isset($projek['tarikh_bayaran_fi_pspp']) && $projek['tarikh_bayaran_fi_pspp'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_bayaran_fi_pspp'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('no_resit_bayaran_fi_pspp') ? 'error' : ''; ?>">
<?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_pspp', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_no_resit_bayaran_fi_pspp' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_pspp' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_pspp', isset($projek['no_resit_bayaran_fi_pspp']) ? $projek['no_resit_bayaran_fi_pspp'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_pspp'); ?></span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <?php
                                $atno = 1;
                                if ($vo == false) {
                                    ?>
                                    <div id="lastrow_<?= $puli ?>_pspp"  counter="<?= e($atno) ?>"></div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <a href="javascript:void(0)" onclick="addnewrow('pspp')"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Tambah Baris</strong></a>
                                        </div>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="panel panel-<?= isset($projek['jumlah_bayaran_fi_ume']) && $projek['jumlah_bayaran_fi_ume'] == '' ? 'danger' : 'primary' ?>">
                            <div class="panel-heading font-bold">JUMLAH BAYARAN FI UJIAN MEKANIKAL DAN  ELEKTRIKAL</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('jumlah_bayaran_fi_ume') ? 'error' : ''; ?>">
<?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_ume', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_jumlah_bayaran_fi_ume' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_jumlah_bayaran_fi_ume' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_ume', isset($projek['jumlah_bayaran_fi_ume']) ? $projek['jumlah_bayaran_fi_ume'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_ume'); ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Tarikh</label>
                                        <div class='controls'>
                                            <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_bayaran_fi_ume' type='text' name='projek_tarikh_bayaran_fi_ume'  value="<?php echo isset($projek['tarikh_bayaran_fi_ume']) && $projek['tarikh_bayaran_fi_ume'] != '0000-00-00' ? date("d-m-Y", strtotime($projek['tarikh_bayaran_fi_ume'])) : ''; ?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group <?php echo form_error('no_resit_bayaran_fi_ume') ? 'error' : ''; ?>">
<?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_ume', array('class' => 'control-label')); ?>
                                            <div class='controls'>
                                                <input id='projek_no_resit_bayaran_fi_ume' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_ume' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_ume', isset($projek['no_resit_bayaran_fi_ume']) ? $projek['no_resit_bayaran_fi_ume'] : ''); ?>" />
                                                <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_ume'); ?></span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
<?php
$atno = 1;
if ($vo == false) {
    ?>
                                    <div id="lastrow_<?= $puli ?>_ume"  counter="<?= e($atno) ?>"></div>
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <a href="javascript:void(0)" onclick="addnewrow('ume')"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Tambah Baris</strong></a>
                                        </div>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>



                </fieldset>


                <div class="row">
                    <div class="col-sm-3">
                            <?php if ($this->uri->segment(4) == 'create') { ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projek_action_create'); ?>"  />
                                &nbsp;&nbsp;
    <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>

                            </div>
                            <?php } else { ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projek_action_edit'); ?>"  />
                                &nbsp;&nbsp;
    <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>

                                <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>
                                    &nbsp;&nbsp;
                                    <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('projek_delete_confirm'))); ?>');">
                                        <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('projek_delete_record'); ?>
                                    </button>
    <?php endif; ?>
                            </div>
<?php } ?>

                    </div>

                </div>

<?php echo form_close(); ?>
            </div>
    </div>
</div>


<script type="text/javascript">
    function addnewrow(type)
    {
        var theId = parseInt($("#lastrow_<?= $puli ?>_" + type).attr("counter")) + 1;


        $("#lastrow_<?= $puli ?>_" + type).attr("counter", parseInt(theId));

        var row = '<div class="row fadeInUp animated checkRow" id="row_' + type + '_<?= $puli ?>_' + theId + '" >' +
                '<input type="hidden" name="rowid_' + type + '_<?= $puli ?>[]"/>'
                + '<div class="col-md-3 "><input type="text" class="input-sm  form-control" placeholder="" name="projek_no_resit_bayaran_fi_' + type + '_<?= $puli ?>[]"  id="projek_no_resit_bayaran_fi_' + type + '_<?= $puli ?>_' + theId + '"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy" placeholder="" name="jumlah_bayaran_fi_' + type + '_<?= $puli ?>[]"  id="jumlah_bayaran_fi_' + type + '_<?= $puli ?>_' + theId + '"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="input-sm  form-control" placeholder="" name="projek_no_resit_bayaran_fi_' + type + '_<?= $puli ?>[]"  id="projek_no_resit_bayaran_fi_' + type + '_<?= $puli ?>_' + theId + '" ></div>'
                + '<div class="col-md-3 "><a href="javascript:void(0)" ><i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick=remTemp("#row_<?= $puli ?>_' + theId + '")></i></a><br></div>';

        $('#lastrow_<?= $puli ?>_' + type).before(row);
    }

    function remrow(val, id)
    {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
            var theId = parseInt($("#lastrow_<?= $puli ?>").attr("counter")) - 1;
            $("#row_<?= $puli ?>_" + val).slideUp('slow', function () {
                $("#row_<?= $puli ?>_" + val).remove();
                $("#lastrow_<?= $puli ?>").attr("counter", theId);

                $.post("", {action: "deleteFTE", id: id},
                        function (data) {
                            var dt = JSON.parse(data);
                            waitingDialog.show(dt.status_msg);
                            waitingDialog.hide();
                        });
            });
            setTimeout(function () {

            }, 500);
        }
    }

    function remTemp(val)
    {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
            var theId = parseInt($("#lastrow_<?= $puli ?>").attr("counter")) - 1;
            $(val).slideUp('slow', function () {
                $(val).remove();
                $("#lastrow_<?= $puli ?>").attr("counter", theId);
            });
            setTimeout(function () {
            }, 500);
        }
    }

</script>