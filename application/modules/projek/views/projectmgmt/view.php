<?php
$vo = true;

$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projek)) {
    $projek = (array) $projek;
}
$id = isset($projek['id']) ? $projek['id'] : '';
?>
<script>

    function check_lat_range() //to ensure user enters latitude value between -90 and 90 only
    {
        var lat = document.getElementById('project_latitude').value;
        if (lat < -90 || lat > 90)
        {
            alert('Allowed Latitude range is between -90 and 90 only');
            $('#projek_latitude').val(3.1579);
            //return false;
        }
    }

    function check_lng_range() //to ensure user enters longitude value between -180 and 180 only
    {
        var lng = document.getElementById('projek_longitude').value;
        if (lng < -180 || lng > 180)
        {
            alert('Allowed Longitude range is between -180 and 180 only');
            $('#projek_longitude').val(101.71159999999998);
            return false;
        }
    }

</script>

<div class="row">
    <div class="col-sm-12">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Projek</header>
            <div class="panel-body">

                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>

                <fieldset>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group <?php echo form_error('nama_projek') ? 'error' : ''; ?>">
                                <?php echo form_label('Nama Projek' . lang('bf_form_label_required'), 'projek_nama_projek', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_nama_projek' class='  form-control' type='text' name='projek_nama_projek' maxlength="1000" value="<?php echo set_value('projek_nama_projek', isset($projek['nama_projek']) ? $projek['nama_projek'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('nama_projek'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Tarikh Terima Permohonan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <input    class="datepicker-input form-control input-sm input-s" data-date-format="dd-mm-yyyy"  id='projek_tarikh_terima_permohonan' type='text' name='projek_tarikh_terima_permohonan'  value="<?php echo isset($projek['tarikh_terima_permohonan']) ? date("d-m-Y", strtotime($projek['tarikh_terima_permohonan'])) : ''; ?>"  data-required="true" readonly="readonly" required="true"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label><?= 'Pegawai Ulasan' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <select name="projek_pegawai_ulasan" id="projek_pegawai_ulasan" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" required="true">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($users as $user) {
                                        if (in_array($user->id, explode(",", $projek['pegawai_ulasan']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->display_name; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $user->id ?>"><?= $user->display_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label><?= 'Status Permohonan' ?></label>
                            <div class='controls'>
                                <select name="projek_status_permohonan" id="projek_status_permohonan" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" required="true">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($status_permohonan as $user) {
                                        if (in_array($user->id, explode(",", $projek['status_permohonan']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Daerah' . lang('bf_form_label_required') ?></label>
                            <div class='controls'>
                                <select name="projek_daerah" id="projek_daerah" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($daerah as $n) {
                                        if ($n->id == $projek['id']) {
                                            ?>
                                            <option selected="selected" value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('no_rujukan') ? 'error' : ''; ?>">
                                <?php echo form_label('No Rujukan' . lang('bf_form_label_required'), 'projek_no_rujukan', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_no_rujukan' class='input-sm input-s  form-control' type='text' name='projek_no_rujukan' maxlength="255" value="<?php echo set_value('projek_no_rujukan', isset($projek['no_rujukan']) ? $projek['no_rujukan'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('no_rujukan'); ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label><?= 'Status Terkini Projek' ?></label>
                            <div class='controls'>
                                <select name="projek_status_terkini_projek" id="projek_pegawai_ulasan" class="form-control selecta"  style="height:auto; font-size:12px; width:200px" required="true">
                                    <option value="">--Sila Pilih--</option>
                                    <?php
                                    foreach ($status_terkini_projek as $user) {
                                        if (in_array($user->id, explode(",", $projek['status_terkini_projek']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                        <?php } else { ?>
                                            <option value="<?= $user->id ?>"><?= $user->nama; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!--                        <div class="col-sm-4">
                                                    <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                        <?php echo form_label('Fasa', 'projek_fasa', array('class' => 'control-label')); ?>
                                                        <div class='controls'>
                                                            <input id='projek_fasa' class='input-sm input-s  form-control' type='text' name='projek_fasa' maxlength="11" value="<?php echo set_value('projek_fasa', isset($projek['fasa']) ? $projek['fasa'] : ''); ?>" />
                                                            <span class='help-inline'><?php echo form_error('fasa'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>-->
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label><?= 'Nama Jurutera Perunding' ?></label>
                            <div class='controls'>

                                <select multiple name="projek_nama_jurutera_perunding[]" id="projek_nama_jurutera_perunding" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px" >
                                    <?php
                                    foreach ($jurutera_perunding as $user):
                                        if (in_array($user->id, explode(",", $projek['nama_jurutera_perunding']))) {
                                            ?>
                                            <option selected="selected" value="<?= $user->id ?>"><?= $user->nama; ?></option><?php } else {
                                            ?>
                                            <option value="<?= $user->id ?>"><?= $user->nama; ?> </option>
                                        <?php }
                                        ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                                <?php echo form_label('Nama Pemilik*', 'projek_nama_pemilik', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_nama_pemilik' class='input-sm input-s  form-control' type='text' name='projek_nama_pemilik' maxlength="255" value="<?php echo set_value('projek_nama_pemilik', isset($projek['nama_pemilik']) ? $projek['nama_pemilik'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('projek_nama_pemilik'); ?></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                                <?php echo form_label('Jumlah Perlu Dijelaskan(RM)*', 'projek_bsm_jumlah_perlu_dijelaskan', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='projek_bsm_jumlah_perlu_dijelaskan' class='input-sm input-s  form-control' type='number' step="0.01" name='projek_bsm_jumlah_perlu_dijelaskan' maxlength="11" value="<?php echo set_value('projek_bsm_jumlah_perlu_dijelaskan', isset($projek['bsm_jumlah_perlu_dijelaskan']) ? $projek['bsm_jumlah_perlu_dijelaskan'] : ''); ?>" required="true" />
                                    <span class='help-inline'><?php echo form_error('bsm_jumlah_perlu_dijelaskan'); ?></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <?php /* Start Another section */ ?>
                </fieldset>
                <br>

                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#bayaran" aria-controls="bayaran" role="tab" data-toggle="tab">Bayaran</a></li>
                    <li role="presentation"><a href="#lokasi" aria-controls="lokasi" role="tab" data-toggle="tab">Lokasi</a></li>
                    <!--<li role="presentation"><a href="#lokasi2" aria-controls="lokasi2" role="tab" data-toggle="tab">lokasi2</a></li>-->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="bayaran">

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">

                                    <?php foreach ($jenis_bayaran as $jns_byrn) {
                                        ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" >
                                                <h4 class="panel-title" style="cursor: pointer">
                                                    <a data-toggle="collapse" href="#collapse<?= $jns_byrn->id ?>" style="cursor: pointer">
                                                        <label style="cursor: pointer"><?= $jns_byrn->nama ?></label>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?= $jns_byrn->id ?>" class="panel-collapse collapse in">
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Jumlah Bayaran </label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>Tarikh Bayaran </label>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label> No. Resit Bayaran </label>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $this->db->order_by('id', 'ASC');
                                                    $bayaran = $this->db->get_where($this->db->dbprefix('bayaran'), array("jenis_bayaran" => $jns_byrn->id, "deleted" => "0"))->result();
                                                    $atno = -1;
//        echo $this->db->last_query();

                                                    foreach ($bayaran as $rec) {
                                                        $atno++;
                                                        ?>
                                                        <div class="row row-fluid checkRow " id="row_<?php e($atno) ?>_<?= $jns_byrn->id ?>">
                                                            <!--                <div class="col-md-1">
                                                            <?= $atno ?>
                                                                            </div>-->
                                                            <input type="hidden" name="rowid_<?= $jns_byrn->id ?>[]" value="<?= $rec->id ?>"/>
                                                            <div class="col-md-3">
                                                                <?php
                                                                if ($vo) {
                                                                    echo $rec->jumlah_bayaran;
                                                                } else {
                                                                    ?>
                                                                    <input type="number" placeholder="Jumlah Bayaran" class="input-sm  form-control" id="jumlah_bayaran_<?= e($atno) ?>_<?= $jns_byrn->id ?>" name="jumlah_bayaran_<?= $jns_byrn->id ?>[]"  value="<?= e($rec->jumlah_bayaran) ?>" required="required" min="0"  />
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-3 ">
                                                                <?php
                                                                if ($vo) {
                                                                    echo $rec->tarikh_bayaran;
                                                                } else {
                                                                    ?>
                                                                    <input type="text"  class="input-sm  form-control date_picker" placeholder="Tarikh Bayaran" name="tarikh_bayaran_<?= $jns_byrn->id ?>[]"  id="tarikh_bayaran_<?php e($atno) ?>_<?= $jns_byrn->id ?>"  value="<?= e($rec->tarikh_bayaran) ?>" required="required" />
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>  

                                                            <div class="col-md-3 ">
                                                                <?php
                                                                if ($vo) {
                                                                    echo $rec->no_resit_bayaran;
                                                                } else {
                                                                    ?>
                                                                    <input type="text"  class="input-sm  form-control" placeholder="No. Resit Bayaran" name="no_resit_bayaran_<?= $jns_byrn->id ?>[]"  id="no_resit_bayaran_<?php e($atno) ?>_<?= $jns_byrn->id ?>"  value="<?= e($rec->no_resit_bayaran) ?>" required="required"/>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>  

                                                            <div class="col-md-2 ">
                                                                <?php
                                                                if ($vo == false) {
                                                                    if ($atno >= 1) {
                                                                        ?>
                                                                        <a href="javascript:void(0)" >
                                                                            <i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick="remrow('<?= $jns_byrn->id ?>',<?= e($atno) ?>', '<?= e($rec->id) ?>');"></i>
                                                                        </a>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>   
                                                            </div>
                                                            <br>
                                                        </div> 
                                                    <?php } ?>

                                                    <?php if ($vo == false) { ?>
                                                        <div id="lastrow_<?= $jns_byrn->id ?>"  counter="<?= e($atno) ?>"></div>
                                                        <div class="row"> 
                                                            <div class="col-md-2">
                                                                <a href="javascript:void(0)" onclick="addnewrow(<?= $jns_byrn->id ?>)"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong>Add Line</strong></a>
                                                            </div>
                                                        </div>
                                                    <?php } ?>


                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <br><br>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!--                    <div role="tabpanel" class="tab-pane" id="lokasi">
                   
                                          <div class="panel panel-default">
                                               <div class="panel-body">
                   
                                                   <div class="panel panel-default">
                                                       <div class="panel-heading font-bold">
                                                           <a class="accordion-toggle show" data-toggle="collapse" data-parent="#accordion5" href="#collapseEight">
                                                               Lokasi Projek
                                                           </a>
                                                       </div>
                                                       <div id="collapseEight" class="panel-collapse in">
                                                           <div class="panel-body">
                                                               <div class="row"> 
                   
                                                                   <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                                                       <div class="form-group <?php echo form_error('latitude') ? 'error' : ''; ?>">
                    <?php echo form_label('Latitude', 'projek_latitude', array('class' => 'font-bold control-label')); ?>  
                                                                           <div class='controls'>
                    <?php echo set_value('projek_latitude', isset($projek['latitude']) ? $projek['latitude'] : ''); ?>
                                                                           </div>
                                                                       </div> 
                                                                   </div> col-md-4 col-lg-4 col-sm-4 col-xs-4 
                                                                   <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                                                       <div class="form-group <?php echo form_error('projek_longitude') ? 'error' : ''; ?>">
                    <?php echo form_label('Longitude', 'projek_longitude', array('class' => 'font-bold control-label')); ?>  
                                                                           <div class='controls'>
                    <?php echo set_value('projek_longitude', isset($projek['longitude']) ? $projek['longitude'] : ''); ?>
                                                                           </div>
                                                                       </div> 
                                                                   </div> col-md-4 col-lg-4 col-sm-4 col-xs-4                                                     
                                                               </div> row 
                   
                                                               <style>
                                                                   .map_overlay{
                                                                       width: 100%;
                                                                       height:100%;
                                                                       position: absolute;
                                                                       left: 0;
                                                                       top: 0;
                                                                       z-index: 100;
                                                                   }
                                                               </style>
                                                               <script type="text/javascript">
                                                                   $(function () {
                                                                       $('.map_overlay').on('click', function () {
                                                                           $(this).css({zIndex: '0'});
                                                                       });
                                                                   });
                                                               </script>
                                                               <div class="row" style="position:relative;">
                                                                   <div class="map_overlay">&nbsp;</div>
                                                                   <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                                                       <div class="form-group">
                                                                           <div class='controls'>
                                                                               <div id="us2" style="width: 80%; height: 200px;"></div>
                                                                               <div class="clearfix"></div>
                                                                           </div> controls 
                                                                       </div> form-group 
                                                                   </div> col-md-12 
                                                               </div> row 
                    <?php
                    if (isset($projek['latitude']) and intval($projek['latitude']) != "") {
                        $latitude = $projek['latitude'];
                    } else {
                        $latitude = 3.1579;
                    }
                    if (isset($projek['longitude']) and intval($projek['longitude']) != "") {
                        $longitude = $projek['longitude'];
                    } else {
                        $longitude = 101.71159999999998;
                    }
                    ?>
                   
                                                               <script type="text/javascript" src='<?php echo "http://maps.google.com/maps/api/js?libraries=places&key=" . $this->config->item('googleMapAPIKey'); ?>'></script>
                                                               <script type="text/javascript" src='<?php echo Template::theme_url('js/locationpicker.jquery.js'); ?>'></script>
                   
                                                               <script>
                                                                   $('#us2').locationpicker({
                                                                       location: {
                                                                           latitude: <?php echo $latitude; ?>,
                                                                           longitude: <?php echo $longitude; ?>
                                                                       },
                                                                       radius: 10
                                                                   });
                                                               </script>
                                                           </div>  panel-body 
                   
                                                       </div> panel-collapse in 
                                                   </div>
                                               </div>
                                           </div>
                   
                   
                                       </div>-->

                    <div role="tabpanel" class="tab-pane" id="lokasi">


                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="panel panel-default">
                                    <div class="panel-heading font-bold">
                                        <a class="accordion-toggle show" data-toggle="collapse" href="#collapseSeven">

                                        </a>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                    <div class="form-group<?php echo form_error('location') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Lokasi Projek', 'projek_location', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='input-md  form-control' type='text' name='projek_location' id='projek_location' maxlength="255" value="" />
                                                            <span class='help-inline'><?php echo form_error('location'); ?></span>
                                                        </div>
                                                    </div>                                            
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                                    <div class="form-group<?php echo form_error('projek_latitude') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Latitude', 'projek_latitude', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='input-sm input-s  form-control'  type='text' name='projek_latitude' id='projek_latitude' maxlength="255"  value="" onblur="check_lat_range()" />
                                                            <span class='help-inline'><?php echo form_error('projek_latitude'); ?></span>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                                    <div class="form-group<?php echo form_error('projek_longitude') ? 'error' : ''; ?>">
                                                        <?php echo form_label('Longitude', 'projek_longitude', array('class' => 'font-bold control-label')); ?>  
                                                        <div class='controls'>
                                                            <input  class='input-sm input-s  form-control'  type='text' name='projek_longitude' id='projek_longitude' maxlength="255"  value="" onblur="check_lng_range()" />
                                                            <span class='help-inline'><?php echo form_error('projek_longitude'); ?></span>
                                                        </div>
                                                    </div> 
                                                </div>                                                   
                                            </div><!-- row -->

                                            <div class="row" style="position:relative;">
                                                <div class="map_overlay">&nbsp;</div>
                                                <div id="us2" style="width: 100%; height: 400px;"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <style>
                                                .map_overlay{
                                                    width: 100%;
                                                    height:100%;
                                                    position: absolute;
                                                    left: 0;
                                                    top: 0;
                                                    z-index: 100;
                                                }
                                            </style>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $('.map_overlay').on('click', function () {
                                                        $(this).css({zIndex: '0'});
                                                    });
                                                });
                                            </script>

                                            <?php
                                            if (isset($projek['latitude']) and intval($projek['latitude']) != "") {
                                                $latitude = $projek['latitude'];
                                            } else {
                                                $latitude = 3.1579;
                                            }
                                            if (isset($projek['longitude']) and intval($projek['longitude']) != "") {
                                                $longitude = $projek['longitude'];
                                            } else {
                                                $longitude = 101.71159999999998;
                                            }
                                            ?>

                                            <script type="text/javascript" src='<?php echo "http://maps.google.com/maps/api/js?libraries=places&key=" . $this->config->item('googleMapAPIKey'); ?>'></script>
                                            <script type="text/javascript" src='<?php echo Template::theme_url('js/locationpicker.jquery.js'); ?>'></script>

                                            <script>
                                                $('#us2').locationpicker({
                                                    location: {
                                                        latitude: <?php echo $latitude; ?>,
                                                        longitude: <?php echo $longitude; ?>
                                                    },
                                                    radius: 10,
                                                    inputBinding: {
                                                        latitudeInput: $('#projek_latitude'),
                                                        longitudeInput: $('#projek_longitude'),
                                                        locationNameInput: $('#projek_location')
                                                    },
                                                    enableAutocomplete: true
                                                });
                                            </script>
                                        </div>
                                    </div><!-- panel-body -->
                                </div>


                            </div>
                        </div>
                    </div>






                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <?php if ($this->uri->segment(4) == 'create') { ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projek_action_create'); ?>"  />
                                &nbsp;&nbsp;
                                <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>

                            </div>
                        <?php } else { ?>
                            <div class="form-actions">
                                <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('projek_action_edit'); ?>"  />
                                &nbsp;&nbsp;
                                <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>

                                <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>
                                    &nbsp;&nbsp;
                                    <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('projek_delete_confirm'))); ?>');">
                                        <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('projek_delete_record'); ?>
                                    </button>
                                <?php endif; ?>
                            </div>
                        <?php } ?>

                    </div>

                </div>

                <?php echo form_close(); ?>
            </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        initDatePicker();
<?php if ($this->auth->user_id() != $projek['created_by']) { ?>
            $('#forma input').attr('readonly', 'readonly');
            $("#forma :input").attr("disabled", true);
<?php } ?>
    });




    function addnewrow(type)
    {
        var theId = parseInt($("#lastrow_" + type).attr("counter")) + 1;


        $("#lastrow_" + type).attr("counter", parseInt(theId));

        var row = '<div class="row fadeInUp animated checkRow" id="row_' + theId + '_' + type + '" >' +
                '<input type="hidden" name="rowid_' + type + '[]"/>'
                + '<div class="col-md-3 "><input type="text" class="input-sm  form-control" placeholder="Jumlah Bayaran" name="jumlah_bayaran_' + type + '[]"  id="jumlah_bayaran_' + theId + '_' + type + '"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="date_picker form-control input-sm" placeholder="Tarikh Bayaran" name="tarikh_bayaran_' + type + '[]"  id="tarikh_bayaran_' + theId + '_' + type + '"  ></div>'
                + '<div class="col-md-3 "><input type="text" class="input-sm  form-control" placeholder="" name="no_resit_bayaran_' + type + '[]"  id="no_resit_bayaran_' + theId + '_' + type + '" ></div>'
                + '<div class="col-md-3 "><a href="javascript:void(0)" ><i class="fa fa-times-circle fa-1.5x  m-l m-b" style="color: #F00; font-size:14px"  onclick=remTemp("#row_' + theId + '_' + type + '","' + type + '")></i></a><br></div>';

        $('#lastrow_' + type).before(row);
        initDatePicker();
    }

    function remrow(jenis_byr, count, rec_id) //val, id,type
    {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
//            var theId = parseInt($("#lastrow_" + jenis_byr).attr("counter")) - 1;
            $("#row_" + count + '_' + jenis_byr).slideUp('slow', function () {
                $("#row_" + count + '_' + jenis_byr).remove();
//                $("#lastrow_" + jenis_byr).attr("counter", theId);

                $.post("", {action: "deleteBayaran", id: rec_id},
                        function (data) {
                            var dt = JSON.parse(data);
                            waitingDialog.show(dt.status_msg);
                            waitingDialog.hide();
                        });
            });
            setTimeout(function () {

            }, 500);
        }
    }

    function remTemp(val, jns_bayaran) {
        var result = confirm("Confirm delete?");
        if (result) {
            //Logic to delete the item
//            var theId = parseInt($("#lastrow_" + jns_bayaran).attr("counter")) - 1;
            $(val).slideUp('slow', function () {
                $(val).remove();
//                $("#lastrow_" + jns_bayaran).attr("counter", theId);
            });
            setTimeout(function () {
            }, 500);
        }
    }



    function initDatePicker() {

        $('.date_picker').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            calendarWeeks: true,
            autoclose: true,
            todayHighlight: true,
            startDate: new Date()
        });
    }

</script>