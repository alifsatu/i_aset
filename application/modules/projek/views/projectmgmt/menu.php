<?php
if ($this->auth->user()->daerah == '' || $this->auth->user()->daerah == '0') { //Bukan Pengguna Daerah
    ?>

    <ul>
        <li><a href="<?php echo site_url(SITE_AREA . '/projectmgmt/projek') ?>"><i class="fa fa-angle-right"></i>Senarai Projek</a></li>
        <li><a href="<?php echo site_url(SITE_AREA . '/projectmgmt/projek/my_projects') ?>"><i class="fa fa-angle-right"></i>Projek Anda</a></li>
        <li><a href="<?php echo site_url(SITE_AREA . '/projectmgmt/projek/create') ?>"><i class="fa fa-angle-right"></i>Daftar Projek</a></li>
        <li><a href="<?php echo site_url(SITE_AREA . '/projectmgmt/projek/reports_home') ?>"><i class="fa fa-angle-right"></i>Laporan</a></li>

    </ul>

<?php } else { // Pengguna Daerah
    ?>

    <ul>
        <li><a href="<?php echo site_url(SITE_AREA . '/projectmgmt/projek/my_projects') ?>"><i class="fa fa-angle-right"></i>Projek Anda</a></li>
    </ul>

<?php } ?>


