<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($projek)) {
    $projek = (array) $projek;
}
$id = isset($projek['id']) ? $projek['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Projek</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                    );

                    echo form_dropdown('projek_tarikh_terima_permohonan', $options, set_value('projek_tarikh_terima_permohonan', isset($projek['tarikh_terima_permohonan']) ? $projek['tarikh_terima_permohonan'] : ''), 'Tarikh Terima Permohonan' . lang('bf_form_label_required'));
                    ?>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                        11 => 11,
                    );

                    echo form_dropdown('projek_pegawai_ulasan', $options, set_value('projek_pegawai_ulasan', isset($projek['pegawai_ulasan']) ? $projek['pegawai_ulasan'] : ''), 'Pegawai Ulasan' . lang('bf_form_label_required'));
                    ?>

                    <div class="form-group <?php echo form_error('nama_projek') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Projek' . lang('bf_form_label_required'), 'projek_nama_projek', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_nama_projek' class='input-sm input-s  form-control' type='text' name='projek_nama_projek' maxlength="1000" value="<?php echo set_value('projek_nama_projek', isset($projek['nama_projek']) ? $projek['nama_projek'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama_projek'); ?></span>
                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                        11 => 11,
                    );

                    echo form_dropdown('projek_daerah', $options, set_value('projek_daerah', isset($projek['daerah']) ? $projek['daerah'] : ''), 'Daerah' . lang('bf_form_label_required'));
                    ?>

                    <div class="form-group <?php echo form_error('no_rujukan') ? 'error' : ''; ?>">
                        <?php echo form_label('No Rujukan' . lang('bf_form_label_required'), 'projek_no_rujukan', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_no_rujukan' class='input-sm input-s  form-control' type='text' name='projek_no_rujukan' maxlength="255" value="<?php echo set_value('projek_no_rujukan', isset($projek['no_rujukan']) ? $projek['no_rujukan'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('no_rujukan'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('jumlah_bayaran_fi_ukp') ? 'error' : ''; ?>">
                        <?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_ukp', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_jumlah_bayaran_fi_ukp' class='input-sm input-s  form-control' type='text' name='projek_jumlah_bayaran_fi_ukp' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_ukp', isset($projek['jumlah_bayaran_fi_ukp']) ? $projek['jumlah_bayaran_fi_ukp'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_ukp'); ?></span>
                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                    );

                    echo form_dropdown('projek_tarikh_bayaran_fi_ukp', $options, set_value('projek_tarikh_bayaran_fi_ukp', isset($projek['tarikh_bayaran_fi_ukp']) ? $projek['tarikh_bayaran_fi_ukp'] : ''), 'Tarikh');
                    ?>

                    <div class="form-group <?php echo form_error('no_resit_bayaran_fi_ukp') ? 'error' : ''; ?>">
                        <?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_ukp', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_no_resit_bayaran_fi_ukp' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_ukp' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_ukp', isset($projek['no_resit_bayaran_fi_ukp']) ? $projek['no_resit_bayaran_fi_ukp'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_ukp'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('jumlah_bayaran_fi_pmutk') ? 'error' : ''; ?>">
                        <?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_pmutk', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_jumlah_bayaran_fi_pmutk' class='input-sm input-s  form-control' type='text' name='projek_jumlah_bayaran_fi_pmutk' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_pmutk', isset($projek['jumlah_bayaran_fi_pmutk']) ? $projek['jumlah_bayaran_fi_pmutk'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_pmutk'); ?></span>
                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                    );

                    echo form_dropdown('projek_tarikh_bayaran_fi_pmutk', $options, set_value('projek_tarikh_bayaran_fi_pmutk', isset($projek['tarikh_bayaran_fi_pmutk']) ? $projek['tarikh_bayaran_fi_pmutk'] : ''), 'Tarikh');
                    ?>

                    <div class="form-group <?php echo form_error('no_resit_bayaran_fi_pmutk') ? 'error' : ''; ?>">
                        <?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_pmutk', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_no_resit_bayaran_fi_pmutk' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_pmutk' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_pmutk', isset($projek['no_resit_bayaran_fi_pmutk']) ? $projek['no_resit_bayaran_fi_pmutk'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_pmutk'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('jumlah_bayaran_fi_pspp') ? 'error' : ''; ?>">
                        <?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_pspp', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_jumlah_bayaran_fi_pspp' class='input-sm input-s  form-control' type='text' name='projek_jumlah_bayaran_fi_pspp' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_pspp', isset($projek['jumlah_bayaran_fi_pspp']) ? $projek['jumlah_bayaran_fi_pspp'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_pspp'); ?></span>
                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                    );

                    echo form_dropdown('projek_tarikh_bayaran_fi_pspp', $options, set_value('projek_tarikh_bayaran_fi_pspp', isset($projek['tarikh_bayaran_fi_pspp']) ? $projek['tarikh_bayaran_fi_pspp'] : ''), 'Tarikh');
                    ?>

                    <div class="form-group <?php echo form_error('no_resit_bayaran_fi_pspp') ? 'error' : ''; ?>">
                        <?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_pspp', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_no_resit_bayaran_fi_pspp' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_pspp' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_pspp', isset($projek['no_resit_bayaran_fi_pspp']) ? $projek['no_resit_bayaran_fi_pspp'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_pspp'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('jumlah_bayaran_fi_ume') ? 'error' : ''; ?>">
                        <?php echo form_label('Jumlah', 'projek_jumlah_bayaran_fi_ume', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_jumlah_bayaran_fi_ume' class='input-sm input-s  form-control' type='text' name='projek_jumlah_bayaran_fi_ume' maxlength="255" value="<?php echo set_value('projek_jumlah_bayaran_fi_ume', isset($projek['jumlah_bayaran_fi_ume']) ? $projek['jumlah_bayaran_fi_ume'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('jumlah_bayaran_fi_ume'); ?></span>
                        </div>
                    </div>

                    <?php
                    // Change the values in this array to populate your dropdown as required
                    $options = array(
                    );

                    echo form_dropdown('projek_tarikh_bayaran_fi_ume', $options, set_value('projek_tarikh_bayaran_fi_ume', isset($projek['tarikh_bayaran_fi_ume']) ? $projek['tarikh_bayaran_fi_ume'] : ''), 'Tarikh');
                    ?>

                    <div class="form-group <?php echo form_error('no_resit_bayaran_fi_ume') ? 'error' : ''; ?>">
                        <?php echo form_label('No. Resit', 'projek_no_resit_bayaran_fi_ume', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_no_resit_bayaran_fi_ume' class='input-sm input-s  form-control' type='text' name='projek_no_resit_bayaran_fi_ume' maxlength="255" value="<?php echo set_value('projek_no_resit_bayaran_fi_ume', isset($projek['no_resit_bayaran_fi_ume']) ? $projek['no_resit_bayaran_fi_ume'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('no_resit_bayaran_fi_ume'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('nama_jurutera_perunding') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Jurutera Perunding', 'projek_nama_jurutera_perunding', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_nama_jurutera_perunding' class='input-sm input-s  form-control' type='text' name='projek_nama_jurutera_perunding' maxlength="255" value="<?php echo set_value('projek_nama_jurutera_perunding', isset($projek['nama_jurutera_perunding']) ? $projek['nama_jurutera_perunding'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama_jurutera_perunding'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('nama_pemilik') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Pemilik', 'projek_nama_pemilik', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_nama_pemilik' class='input-sm input-s  form-control' type='text' name='projek_nama_pemilik' maxlength="255" value="<?php echo set_value('projek_nama_pemilik', isset($projek['nama_pemilik']) ? $projek['nama_pemilik'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama_pemilik'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('fasa') ? 'error' : ''; ?>">
                        <?php echo form_label('Fasa', 'projek_fasa', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='projek_fasa' class='input-sm input-s  form-control' type='text' name='projek_fasa' maxlength="11" value="<?php echo set_value('projek_fasa', isset($projek['fasa']) ? $projek['fasa'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('fasa'); ?></span>
                        </div>
                    </div>


                    <div class="form-actions">
                        <br/>

                        <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
                        &nbsp;&nbsp; <?php echo anchor(SITE_AREA . '/projectmgmt/projek', lang('projek_cancel'), 'class="btn btn-warning"'); ?>


                        <?php if ($this->auth->has_permission('Projek.Projectmgmt.Delete')) : ?>

                            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('projek_delete_confirm'); ?>')">
                                <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
                            </button>

                        <?php endif; ?>




                    </div>
                </fieldset>
                <?php echo form_close(); ?>


            </div>