<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * projectmgmt controller
 */
class projectmgmt extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Projek.Projectmgmt.View');
        $this->load->model('projek_model', null, true);
        $this->load->model('projek_komen/projek_komen_model', null, true);
        $this->lang->load('projek');

        $this->load->model('users/user_model', null, true);
        $this->load->model('daftar_daerah/daftar_daerah_model', null, true);
        $this->load->model('daftar_pemilik/daftar_pemilik_model', null, true);
        $this->load->model('daftar_jurutera_perunding/daftar_jurutera_perunding_model', null, true);
        $this->load->model('status_permohonan/status_permohonan_model', null, true);
        $this->load->model('status_terkini_projek/status_terkini_projek_model', null, true);
        $this->load->model('jenis_bayaran/jenis_bayaran_model', null, true);
        $this->load->model('bayaran/bayaran_model', null, true);
        $this->load->model('activities/activity_model');
        $this->load->model('ccc/ccc_model');

        Assets::add_css("js/datepicker/datepicker.css");
        Assets::add_css("js/datatables/datatables.min.css");

        Assets::add_js("js/datepicker/bootstrap-datepicker.js");
        Assets::add_js("js/libs/moment.min.js");

        //alif

        Assets::add_js(array(Template::theme_url('js/datatables/datatables.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.flash.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/JSZip-2.5.0/jszip.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/pdfmake.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/pdfmake-0.1.36/vfs_fonts.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.html5.min.js')));
        Assets::add_js(array(Template::theme_url('js/datatables/Buttons-1.5.4/js/buttons.print.min.js')));

        //ckeditor
        Assets::add_js(Template::theme_url('js/ckeditor/ckeditor'));

        Assets::add_module_js('projek', 'projek.js');
        Assets::add_module_css('projek', 'style.css');

        Template::set_block('sub_nav', 'projectmgmt/_sub_nav');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {

        if ($this->auth->user()->daerah != '' || $this->auth->user()->daerah != '0') {
            redirect(SITE_AREA . '/projectmgmt/projek/my_projects');
        }

        $this->load->library('session');
//        $this->load->library('pagination');
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->projek_model->delete($pid);
                    //delete data bayaran
                    $this->db->query('UPDATE intg_bayaran SET deleted = 1,deleted_by=' . $this->auth->user_id() . ' where id_projek = ' . $pid);
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('projek_delete_success'), 'success');
                } else {
                    Template::set_message(lang('projek_delete_failure') . $this->projek_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('projekfield', $this->input->post('select_field'));
                $this->session->set_userdata('projekfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('projekfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('projekfield');
                $this->session->unset_userdata('projekfvalue');
                $this->session->unset_userdata('projekfname');
                break;
        }

        $field = '';
        $ord1 = '';
        if ($this->session->userdata('projekfield') != '') {
            $field = $this->session->userdata('projekfield');
        }


        if ($this->session->userdata('projekfield') == 'All Fields') {


            $this->projek_model->where('deleted', '0');
            $this->projek_model->likes('nama_projek', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('nama_pemilik', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('no_rujukan', $this->session->userdata('projekfvalue'), 'both');
            $total = $this->projek_model->count_all();
        } else if ($this->session->userdata('projekfield') == 'intg_projek.id') {

            $this->projek_model->where('deleted', '0');
            $this->projek_model->where('id', $this->session->userdata('projekfvalue'));
            $total = $this->projek_model->count_all();
        } else {


            $this->projek_model->where('deleted', '0');
            if ($field != '') {
                $this->projek_model->where($field, $this->session->userdata('projekfvalue'));
            }
            $total = $this->projek_model->count_all();
        }

        if ($this->session->userdata('projekfield') == 'All Fields') {

            $likes = str_replace(" ", '%', $this->session->userdata('projekfvalue'));
            if ($this->session->userdata('projekfvalue') != '') {
                if ($likes[0] != '%') {
                    $likes = '%' . $likes;
                }

                if (substr($this->session->userdata('projekfvalue'), -1) != '%') {
                    $likes = $likes . '%';
                }
            }

            $this->projek_model->where('deleted', '0');
            $this->projek_model->where("nama_projek like '" . $likes . "'");
            $this->projek_model->or_where("nama_pemilik like '" . $likes . "'");
            $this->projek_model->or_where("no_rujukan like '" . $likes . "'");
        } else if ($this->session->userdata('projekfield') == 'intg_projek.id') {

            $this->projek_model->where('deleted', '0');
            $this->projek_model->where('id', $this->session->userdata('projekfvalue'));
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $this->projek_model->where('deleted', '0');
            if ($field != '') {
                $this->projek_model->where($field, $this->session->userdata('projekfvalue'));
            }
        }

        if ($ord1 != '') {
            $this->projek_model->order_by($ord1);
        }

        $records = $this->projek_model->find_all();

        $rec_bayaran = '';
        if ($records) {
            //GET BAYARAN DATA
            $id_projects = array();
            foreach ($records as $rec) {
                $id_projects[] = $rec->id;
            }

            $this->bayaran_model->select(" *,CONCAT(id_projek,'_',jenis_bayaran,'_',id) as id_projek_bayaran ", FALSE);
            $this->bayaran_model->where('deleted', '0');
            $this->bayaran_model->where_in('id_projek', $id_projects);
            $rec_bayaran = $this->bayaran_model->find_all();

            Template::set('rec_bayaran', $rec_bayaran);
        }

//        $rec_bayaran = array();
//        foreach ($raw_rec_bayaran as $rec){
//            $rec_bayaran[$rec->id_projek_bayaran.'_'.$rec->id] = $rec;
//        }
//        echo $this->db->last_query();
//        print_r_pre($rec_bayaran);die;
//$records = $this->projek_model->find_all();


        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('records', $records);
        Template::set('toolbar_title', 'Urus Projek');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_projek SET deleted = 0 where id = ' . $pid . '');
                    $this->db->query('UPDATE intg_bayaran SET deleted = 0 where id_projek = ' . $pid);
                }



                if ($result) {
                    Template::set_message(lang('projek_success'), 'success');
                } else {
                    Template::set_message(lang('projek_restored_error') . $this->projek_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_projek where id = ' . $pid . '');
                    $this->db->query('delete from intg_bayaran where id_projek = ' . $pid);
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('projek_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('projekfield', $this->input->post('select_field'));
                $this->session->set_userdata('projekfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('projekfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('projekfield');
                $this->session->unset_userdata('projekfvalue');
                $this->session->unset_userdata('projekfname');
                break;
        }


        if ($this->session->userdata('projekfield') != '') {
            $field = $this->session->userdata('projekfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('projekfield') == 'All Fields') {

            $this->projek_model->where('deleted', '1');
            $this->projek_model->likes('nama_projek', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('nama_pemilik', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('no_rujukan', $this->session->userdata('projekfvalue'), 'both');
            $total = $this->projek_model->count_all();
        } else {

            $total = $this->projek_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('projekfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-projek_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }


        if ($this->session->userdata('projekfield') == 'All Fields') {
            $this->projek_model->where('deleted', '1');
            $this->projek_model->likes('nama_projek', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('nama_pemilik', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('no_rujukan', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->limit($this->pager['per_page'], $offset);
            $this->projek_model->order_by($this->input->get('sort_by'), $this->input->get('sort_order'));
            $records = $this->projek_model->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->projek_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('projekfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->projek_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);

        Assets::add_module_css('projek', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Urus Projek');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Projek object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Projek.Projectmgmt.Create');

        if (isset($_POST['action']) and $_POST['action'] == "deleteBayaran") {
            $this->deleteBayaran();
        }

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_projek()) {
                // Log the activity
                log_activity($this->current_user->id, lang('projek_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'projek', $_POST, $insert_id);

                Template::set_message(lang('projek_create_success'), 'success');
                redirect(SITE_AREA . '/projectmgmt/projek');
            } else {
                Template::set_message(lang('projek_create_failure') . $this->projek_model->error, 'error');
            }
        }
        Assets::add_module_js('projek', 'projek.js');
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('status_permohonan', $this->status_permohonan_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_terkini_projek', $this->status_terkini_projek_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('jurutera_perunding', $this->daftar_jurutera_perunding_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('pemilik', $this->daftar_pemilik_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('daerah', $this->daftar_daerah_model->find_all_by(array('deleted' => '0')));
        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        Template::set('toolbar_title', lang('projek_create') . ' Projek');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Projek data.
     *
     * @return void
     */
    public function edit() {

        if (isset($_POST['action']) and $_POST['action'] == "deleteBayaran") {
            $this->deleteBayaran();
        }

//        print_r_pre($_POST);die;
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('projek_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/projek');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Projek.Projectmgmt.Edit');

            if ($this->save_projek('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('projek_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'projek', $_POST, $id);

                Template::set_message(lang('projek_edit_success'), 'success');
            } else {
                Template::set_message(lang('projek_edit_failure') . $this->projek_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Projek.Projectmgmt.Delete');

            if ($this->projek_model->delete($id)) {
                // Log the activity
                //soft delete data bayaran
                $this->db->query('UPDATE intg_bayaran SET deleted = 1,deleted_by =' . $this->auth->user_id() . ' where id_projek = ' . $id);

                log_activity($this->current_user->id, lang('projek_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'projek', $_POST, $id);
                Template::set_message(lang('projek_delete_success'), 'success');

                redirect(SITE_AREA . '/projectmgmt/projek');
            } else {
                Template::set_message(lang('projek_delete_failure') . $this->projek_model->error, 'error');
            }
        }

        $ccc_exist = $this->ccc_model->find_by(array('intg_ccc.projek_id' => $id, 'intg_ccc.deleted' => '0'));
        Template::set('ccc_exist', $ccc_exist);

        Template::set('projek', $this->projek_model->find($id));

        $this->projek_komen_model->select(' * ');
        $this->projek_komen_model->select(' DATE_FORMAT(created_on, "%d-%m-%Y %h:%i:%s %p") as tarikh_masuk ', FALSE);
        $this->projek_komen_model->order_by('id', 'DESC');
        $projek_komen = $this->projek_komen_model->find_all_by(array('deleted' => '0', 'id_projek' => $id));

        Template::set('projek_komen', $projek_komen);


        Template::set('toolbar_title', lang('projek_edit') . ' Projek');
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('status_permohonan', $this->status_permohonan_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_terkini_projek', $this->status_terkini_projek_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('jurutera_perunding', $this->daftar_jurutera_perunding_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('pemilik', $this->daftar_pemilik_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('daerah', $this->daftar_daerah_model->find_all_by(array('deleted' => '0')));
        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        $activities = $this->activity_model->order_by('created_on', 'desc')->find_by_module('projek', $id);
        Template::set('activities', $activities);


        Template::set_view('create');
        Template::render();
    }

    public function bayar() {

        if (isset($_POST['action']) and $_POST['action'] == "deleteBayaran") {
            $this->deleteBayaran();
        }

//        print_r_pre($_POST);die;
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('projek_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/projek');
        }

        if (isset($_POST['save'])) {

            if ($this->saveBayaran($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('projek_act_bayar_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'bayaran', $_POST, $id);

                Template::set_message(lang('projek_bayaran_success'), 'success');
            } else {
                Template::set_message(lang('projek_bayaran_failure') . $this->bayaran_model->error, 'error');
            }
        }
        Template::set('projek', $this->projek_model->find($id));
        Template::set('toolbar_title', lang('projek_edit') . ' Projek');
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('status_permohonan', $this->status_permohonan_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_terkini_projek', $this->status_terkini_projek_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('jurutera_perunding', $this->daftar_jurutera_perunding_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('pemilik', $this->daftar_pemilik_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('daerah', $this->daftar_daerah_model->find_all_by(array('deleted' => '0')));
        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1')));

        $activities = $this->activity_model->order_by('created_on', 'desc')->find_by_module('projek', $id);
        Template::set('activities', $activities);


        Template::render();
    }

    public function view() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('projek_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/projek');
        }

        $this->projek_komen_model->select(' * ');
        $this->projek_komen_model->select(' DATE_FORMAT(created_on, "%d-%m-%Y") as tarikh_masuk ', FALSE);
        $this->projek_komen_model->order_by('id', 'DESC');
        $projek_komen = $this->projek_komen_model->find_all_by(array('deleted' => '0', 'id_projek' => $id));

        Template::set('projek_komen', $projek_komen);



        Template::set('projek', $this->projek_model->find($id));
        Template::set('toolbar_title', lang('projek_edit') . ' Projek');
        Template::set('users', $this->user_model->find_all_by(array('intg_users.deleted' => '0')));
        Template::set('status_permohonan', $this->status_permohonan_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('status_terkini_projek', $this->status_terkini_projek_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('jurutera_perunding', $this->daftar_jurutera_perunding_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('pemilik', $this->daftar_pemilik_model->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('daerah', $this->daftar_daerah_model->find_all_by(array('deleted' => '0')));
        Template::set('jenis_bayaran', $this->jenis_bayaran_model->order_by('sorting_number', 'ASC')->find_all_by(array('deleted' => '0', 'status' => '1')));
        Template::set('view_only', true);

        $activities = $this->activity_model->order_by('created_on', 'desc')->find_by_module('projek', $id);
        Template::set('activities', $activities);

        $ccc_exist = $this->ccc_model->find_by(array('intg_ccc.projek_id' => $id, 'intg_ccc.deleted' => '0'));
        Template::set('ccc_exist', $ccc_exist);

        Template::set_view('create');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_projek SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('projek_success'), 'success');
                redirect(SITE_AREA . '/projectmgmt/projek/deleted');
            } else {

                Template::set_message(lang('projek_error') . $this->projek_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_projek where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('projek_purged'), 'success');
                redirect(SITE_AREA . '/projectmgmt/projek/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('projek_invalid_id'), 'error');
            redirect(SITE_AREA . '/projectmgmt/projek');
        }




        Template::set('projek', $this->projek_model->find($id));

        Assets::add_module_js('projek', 'projek.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Projek');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_projek($type = 'insert', $id = 0) {
        $data = array();
        if ($type == 'update') {
            $_POST['id'] = $id;
//            $data['modified_by'] = $this->auth->user_id();
        } else {

//            $data['created_by'] = $this->auth->user_id();
        }

        // make sure we only pass in the fields we want
//        print_r_pre($_POST);
//        die;

        $data['tarikh_terima_permohonan'] = $this->input->post('projek_tarikh_terima_permohonan') ? date_format(date_create($this->input->post('projek_tarikh_terima_permohonan')), "Y-m-d") : '0000-00-00';
        $data['tarikh_kelulusan_permohonan'] = $this->input->post('projek_tarikh_kelulusan_permohonan') ? date_format(date_create($this->input->post('projek_tarikh_kelulusan_permohonan')), "Y-m-d") : '0000-00-00';
        $data['pegawai_ulasan'] = $this->input->post('projek_pegawai_ulasan');
        $data['nama_projek'] = strtoupper($this->input->post('projek_nama_projek'));
        $data['daerah'] = $this->input->post('projek_daerah');
        $data['no_rujukan'] = strtoupper($this->input->post('projek_no_rujukan'));
        $data['nama_jurutera_perunding'] = $this->input->post('projek_nama_jurutera_perunding') ? implode(",", $this->input->post('projek_nama_jurutera_perunding')) : '';
        $data['nama_pemilik'] = strtoupper($this->input->post('projek_nama_pemilik'));
        $data['fasa'] = $this->input->post('projek_fasa');
        $data['status_permohonan'] = $this->input->post('projek_status_permohonan') == '' ? '0' : $this->input->post('projek_status_permohonan');
        $data['status_terkini_projek'] = $this->input->post('projek_status_terkini_projek') == '' ? '0' : $this->input->post('projek_status_terkini_projek');
        $data['bsm_jumlah_perlu_dijelaskan'] = $this->input->post('projek_bsm_jumlah_perlu_dijelaskan');
        $data['latitude'] = $this->input->post('projek_latitude');
        $data['longitude'] = $this->input->post('projek_longitude');
        $data['location'] = $this->input->post('projek_location');
        $data['jumlah_penggunaan_air'] = $this->input->post('projek_jumlah_penggunaan_air');
        $komen_ulasan = strtoupper($this->input->post('projek_komen_ulasan')); //changed to add in projek_komen table
        $data['tarikh_tamat_tempoh'] = $this->input->post('projek_tarikh_tamat_tempoh') ? date_format(date_create($this->input->post('projek_tarikh_tamat_tempoh')), "Y-m-d") : '0000-00-00';
        $data['tarikh_tamat_lanjutan_tempoh_masa'] = $this->input->post('projek_tarikh_tamat_lanjutan_tempoh_masa') ? date_format(date_create($this->input->post('projek_tarikh_tamat_lanjutan_tempoh_masa')), "Y-m-d") : '0000-00-00';


        if ($type == 'insert') {
            $id = $this->projek_model->insert($data);


            if (is_numeric($id)) {

                $this->saveBayaran($id);
                $this->saveKomen($id);

                $return = $id;
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->projek_model->update($id, $data);
            $this->saveBayaran($id);
            $this->saveKomen($id);
        }

        return $return;
    }

    public function saveBayaran($projek_id) {

        $jenis_bayaran = $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1'));
//        print_r_pre($_POST);
//            die;
        foreach ($jenis_bayaran as $jns_byrn) {


            $data_bayaran = array();
            $jumlah_bayaran = $this->input->post('jumlah_bayaran_' . $jns_byrn->id);
            $tarikh_bayaran = $this->input->post('tarikh_bayaran_' . $jns_byrn->id);
            $no_resit_bayaran = $this->input->post('no_resit_bayaran_' . $jns_byrn->id);


            $rowid = $this->input->post('rowid_bayaran_' . $jns_byrn->id);

            $return = true;
            if (is_array($rowid)) {

//                $to_update_data = array();
//                $to_insert_data = array();

                $count = 0;
                foreach ($rowid as $rkey => $rval) {

                    $data_bayaran[$rkey] = array(
                        'id_projek' => $projek_id,
                        'jenis_bayaran' => $jns_byrn->id,
                        'jumlah_bayaran' => $jumlah_bayaran[$rkey],
                        'tarikh_bayaran' => $tarikh_bayaran[$rkey],
                        'no_resit_bayaran' => $no_resit_bayaran[$rkey]
                    );

//                    echo "<pre>";
//                    print_r($data_bayaran);
//                    echo "</pre>";

                    if ($rowid[$rkey] == "") {

//                        $to_insert_data[] = array(
//                            'id_projek' => $projek_id,
//                            'jenis_bayaran' => $jns_byrn->id,
//                            'jumlah_bayaran' => $jumlah_bayaran[$rkey],
//                            'tarikh_bayaran' => $tarikh_bayaran[$rkey],
//                            'no_resit_bayaran' => $no_resit_bayaran[$rkey]
//                        );


                        $res = $this->bayaran_model->insert($data_bayaran[$rkey]);
                        if ($res == false) {
                            $return = false;
                        }
//                        echo $this->db->last_query();
                    } else {
//                        $to_update_data[] = array(
//                            'id' => $rowid[$rkey],
//                            'id_projek' => $projek_id,
//                            'jenis_bayaran' => $jns_byrn->id,
//                            'jumlah_bayaran' => $jumlah_bayaran[$rkey],
//                            'tarikh_bayaran' => $tarikh_bayaran[$rkey],
//                            'no_resit_bayaran' => $no_resit_bayaran[$rkey]
//                        );
                        $res = $this->bayaran_model->update($rowid[$rkey], $data_bayaran[$rkey]);
//                        echo $this->db->last_query();
                        if ($res == false) {
                            $return = false;
                        }
                    }
                    $count++;
                }
//                echo '<pre>';
//                print_r($to_update_data);
//                print_r($to_insert_data);
//                echo '</pre>';
//                die;
//                if (count($to_update_data) == 0) {
////                    echo "Update Array is empty";
//                } else {
////                    echo "Update Array is non- empty";
//                    $update = $this->bayaran_model->update_batch('intg_bayaran', $to_update_data, 'id');
////                    echo $this->db->last_query();
//                    if ($update == false) {
//                        return false;
//                    }
//                }
//                if (count($to_insert_data) == 0) {
////                    echo "Insert Array is empty";
//                } else {
////                    echo "Insert Array is non- empty";
//                    $insert = $this->bayaran_model->insert_batch('intg_bayaran',$to_insert_data);
////                    echo $this->db->last_query();
//                    if ($insert == false) {
//                        return false;
//                    }
//                }
//                echo $this->db->last_query();
//                die;
            }
        }
        if ($return == false) {
            return $return;
        } else {
            return true;
        }
    }

    public function deleteBayaran() {
        $this->load->model('bayaran/bayaran_model', null, true);

        $del = $this->bayaran_model->delete($this->input->post('id'));
        $data = array();
        $data['sql'] = $this->db->last_query();
        if ($del) {
            log_activity($this->current_user->id, lang('projek_act_delete_record') . ': ' . $this->input->post('id') . ' : ' . $this->input->ip_address(), 'bayaran', $_POST, $this->input->post('id'));
            $data['status'] = 'success';
            $data['status_msg'] = 'Record successfully deleted';
        } else {
            $data['status'] = 'success';
            $data['status_msg'] = $this->user_level_details_model->error . '. Please contact system administrator';
        }
        echo json_encode($data);
        die();
    }

    public function search() {
        $r = 0;
        switch ($this->input->get('type')) {
            case "intg_projek.id" :
                $r = $this->db->query("select id as rowid,nama_projek as selectedname from intg_projek where deleted =0")->result();

                break;
            case "intg_projek.pegawai_ulasan" :
                $r = $this->db->query("select id as rowid,display_name as selectedname from intg_users")->result();
                break;

            case "intg_projek.daerah" :
                $r = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_daerah")->result();
                break;

            case "intg_projek.nama_jurutera_perunding" :
                $r = $this->db->query("select id as rowid,nama as selectedname from intg_daftar_jurutera_perunding where status = 1")->result();
                break;
        }
        echo json_encode($r);
    }

    public function my_projects($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {


        $this->load->library('session');
        $this->load->library('pagination');

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->projek_model->delete($pid);
                    //soft delete data bayaran
                    $this->db->query('UPDATE intg_bayaran SET deleted = 1,deleted_by=' . $this->auth->user_id() . ' where id_projek = ' . $pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('projek_delete_success'), 'success');
                } else {
                    Template::set_message(lang('projek_delete_failure') . $this->projek_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('projekfield', $this->input->post('select_field'));
                $this->session->set_userdata('projekfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('projekfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('projekfield');
                $this->session->unset_userdata('projekfvalue');
                $this->session->unset_userdata('projekfname');
                break;
        }

        $field = '';
        if ($this->session->userdata('projekfield') != '') {
            $field = $this->session->userdata('projekfield');
        }



        //set untuk current users only
        if ($this->auth->user()->daerah != '' || $this->auth->user()->daerah != '0') {
            $this->projek_model->where('daerah', $this->auth->user()->daerah);
        } else {
            $this->projek_model->where('pegawai_ulasan', $this->auth->user_id());
        }

        if ($this->session->userdata('projekfield') == 'All Fields') {


            $this->projek_model->where('deleted', '0');
            $this->projek_model->likes('nama_projek', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('nama_pemilik', $this->session->userdata('projekfvalue'), 'both');
            $this->projek_model->likes('no_rujukan', $this->session->userdata('projekfvalue'), 'both');
            $total = $this->projek_model->count_all();
        } else if ($this->session->userdata('projekfield') == 'intg_projek.id') {

            $this->projek_model->where('deleted', '0');
            $this->projek_model->where('id', $this->session->userdata('projekfvalue'));
            $total = $this->projek_model->count_all();
        } else {


            $this->projek_model->where('deleted', '0');
            if ($field != '') {
                $this->projek_model->where($field, $this->session->userdata('projekfvalue'));
            }
            $total = $this->projek_model->count_all();
        }
//        print_r_pre($this->session->all_userdata());
//        echo $this->db->last_query();
        //$records = $this->projek_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 100;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case 'admin' : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case 'todo' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case 'notebook' : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        //set untuk current users only
        if ($this->auth->user()->daerah != '' || $this->auth->user()->daerah != '0') {
            $this->projek_model->where('daerah', $this->auth->user()->daerah);
        } else {
            $this->projek_model->where('pegawai_ulasan', $this->auth->user_id());
        }

        if ($this->session->userdata('projekfield') == 'All Fields') {

            $likes = str_replace(" ", '%', $this->session->userdata('projekfvalue'));
            if ($this->session->userdata('projekfvalue') != '') {
                if ($likes[0] != '%') {
                    $likes = '%' . $likes;
                }

                if (substr($this->session->userdata('projekfvalue'), -1) != '%') {
                    $likes = $likes . '%';
                }
            }



            $this->projek_model->where('deleted', '0');
            $this->projek_model->where("nama_projek like '" . $likes . "'");
            $this->projek_model->or_where("nama_pemilik like '" . $likes . "'");
            $this->projek_model->or_where("no_rujukan like '" . $likes . "'");
            $this->projek_model->limit($this->pager['per_page'], $offset);
            $this->projek_model->order_by($this->input->get('sort_by'), $this->input->get('sort_order'));
            $records = $this->projek_model->find_all();
        } else if ($this->session->userdata('projekfield') == 'intg_projek.id') {

            $this->projek_model->where('deleted', '0');
            $this->projek_model->where('id', $this->session->userdata('projekfvalue'));
            $records = $this->projek_model->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $this->projek_model->where('deleted', '0');
            if ($field != '') {
                $this->projek_model->where($field, $this->session->userdata('projekfvalue'));
            }

            $this->projek_model->limit($this->pager['per_page'], $offset);
            $this->projek_model->order_by($ord1);
            $records = $this->projek_model->find_all();
        }

        $rec_bayaran = '';
        if ($records) {
            //GET BAYARAN DATA
            $id_projects = array();
            foreach ($records as $rec) {
                $id_projects[] = $rec->id;
            }

            $this->bayaran_model->select(" *,CONCAT(id_projek,'_',jenis_bayaran,'_',id) as id_projek_bayaran ", FALSE);
            $this->bayaran_model->where_in('id_projek', $id_projects);
            $rec_bayaran = $this->bayaran_model->find_all();

            Template::set('rec_bayaran', $rec_bayaran);
        }


        Template::set('jenis_bayaran', $this->jenis_bayaran_model->find_all_by(array('deleted' => '0', 'status' => '1')));



//        echo $this->db->last_query();
//$records = $this->projek_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('projek', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Urus Projek');
        Template::set_view('projectmgmt/index');
        Template::render();
    }

    public function reports_home() {
        Template::render();
    }

    public function saveKomen($projek_id, $ajax = false) {


        $komen = strtoupper($this->input->post('projek_komen_ulasan'));
        $current_komen_ulasan = htmlspecialchars_decode(stripslashes(strtoupper($this->input->post('current_komen_ulasan'))));

        if ($komen == $current_komen_ulasan) {
            echo 'ULASAN SAMA : Ulasan @ Komen tidak dikemaskini';
        } else {
//            echo 'TAK SAMA';


//            echo $komen;
//            echo "<br>";
//            echo $current_komen_ulasan;
//            die;
            
            $data['komen'] = $komen;
            $data['id_projek'] = $projek_id;

            $return = $this->projek_komen_model->insert($data);
            $_POST['projek_komen_ulasan'] = $komen;
            if ($ajax) {
                if ($return) {
                    $returned = array(
                        'status' => true,
                        'message' => '',
                    );
                }

                header("Content-type: application/json");
                echo json_encode($returned);
            } else {
                if ($return == false) {
                    return $return;
                } else {
                    return true;
                }
            }
        }
    }

    public function deleteKomen($komenid) {
        
    }

    //--------------------------------------------------------------------
}
