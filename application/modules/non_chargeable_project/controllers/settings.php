<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Non_Chargeable_Project.Settings.View');
		$this->load->model('non_chargeable_project_model', null, true);
		$this->lang->load('non_chargeable_project');
		
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('non_chargeable_project', 'non_chargeable_project.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->non_chargeable_project_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('non_chargeable_project_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('non_chargeable_project_delete_failure') . $this->non_chargeable_project_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('non_chargeable_projectfield',$this->input->post('select_field'));
$this->session->set_userdata('non_chargeable_projectfvalue',$this->input->post('field_value'));
$this->session->set_userdata('non_chargeable_projectfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('non_chargeable_projectfield');
$this->session->unset_userdata('non_chargeable_projectfvalue');
$this->session->unset_userdata('non_chargeable_projectfname');
break;
}

if ( $this->session->userdata('non_chargeable_projectfield')!='') { $field=$this->session->userdata('non_chargeable_projectfield'); }
else { $field = 'intg_non_chargeable_project.id'; }

if ( $this->session->userdata('non_chargeable_projectfield') =='All Fields') 
{ 
 	 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');			
$this->db->join('intg_nc_milestone','intg_nc_milestone.id=intg_non_chargeable_project.milestone');
$this->db->join('intg_users','intg_users.id=intg_non_chargeable_project.created_by'); 	
  $total = $this->non_chargeable_project_model
   ->where('intg_non_chargeable_project.deleted','0')
  ->likes('project_name',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('milestone',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('task_name',$this->session->userdata('non_chargeable_projectfvalue'),'both')
   
  ->count_all();
  
}
else
{
		 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');			
$this->db->join('intg_nc_milestone','intg_nc_milestone.id=intg_non_chargeable_project.milestone');	
$this->db->join('intg_users','intg_users.id=intg_non_chargeable_project.created_by'); 
  $total = $this->non_chargeable_project_model
    ->where('intg_non_chargeable_project.deleted','0')
  ->likes($field,$this->session->userdata('non_chargeable_projectfvalue'),'both')  
  ->count_all();
}

		//$records = $this->non_chargeable_project_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('non_chargeable_projectfield') =='All Fields') 
{
		 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');		
$this->db->join('intg_nc_milestone','intg_nc_milestone.id=intg_non_chargeable_project.milestone');	
$this->db->join('intg_users','intg_users.id=intg_non_chargeable_project.created_by'); 
$records = $this->non_chargeable_project_model
 
    ->where('intg_non_chargeable_project.deleted','0')
   ->likes('project_name',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('milestone',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('task_name',$this->session->userdata('non_chargeable_projectfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_non_chargeable_project.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_non_chargeable_project.id'=>'desc',
	 );	}
	 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');			
$this->db->join('intg_nc_milestone','intg_nc_milestone.id=intg_non_chargeable_project.milestone');			
$this->db->join('intg_users','intg_users.id=intg_non_chargeable_project.created_by'); 	 	
$records = $this->non_chargeable_project_model
  ->where('intg_non_chargeable_project.deleted','0')
->likes($field,$this->session->userdata('non_chargeable_projectfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->non_chargeable_project_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
Assets::add_module_css('non_chargeable_project', 'style.css');
Template::set('records', $records);
Template::set('toolbar_title', 'Manage Non-Chargeable Task');
Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_non_chargeable_project SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('non_chargeable_project_success'), 'success');
				}
				else
				{
					Template::set_message(lang('non_chargeable_project_restored_error'). $this->non_chargeable_project_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_non_chargeable_project where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('non_chargeable_project_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('non_chargeable_projectfield',$this->input->post('select_field'));
$this->session->set_userdata('non_chargeable_projectfvalue',$this->input->post('field_value'));
$this->session->set_userdata('non_chargeable_projectfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('non_chargeable_projectfield');
$this->session->unset_userdata('non_chargeable_projectfvalue');
$this->session->unset_userdata('non_chargeable_projectfname');
break;
}


if ( $this->session->userdata('non_chargeable_projectfield')!='') { $field=$this->session->userdata('non_chargeable_projectfield'); }
else { $field = 'intg_non_chargeable_project.id'; }

if ( $this->session->userdata('non_chargeable_projectfield') =='All Fields') 
{ 
 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');			
  $total = $this->non_chargeable_project_model
  ->where('intg_non_chargeable_project.deleted','1')
  ->likes('project_name',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('milestone',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('task_name',$this->session->userdata('non_chargeable_projectfvalue'),'both')
  ->count_all();


}
else
{
	$this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');	
  $total = $this->non_chargeable_project_model
   ->where('intg_non_chargeable_project.deleted','1')
  ->likes($field,$this->session->userdata('non_chargeable_projectfvalue'),'both')  
  ->count_all();
}

//$records = $this-non_chargeable_project_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


if ( $this->session->userdata('non_chargeable_projectfield') =='All Fields') 
{
	$this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');	
$records = $this->non_chargeable_project_model
 
  ->where('intg_non_chargeable_project.deleted','1')
   ->likes('project_name',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('milestone',$this->session->userdata('non_chargeable_projectfvalue'),'both') 
  ->likes('task_name',$this->session->userdata('non_chargeable_projectfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_non_chargeable_project.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_non_chargeable_project.id'=>'desc',
	 );	}
	 
	 $this->db->select('*,intg_non_chargeable_project.id as non_chargeable_id,intg_non_chargeable_project.created_on as non_chargeable_created,intg_non_chargeable_project.modified_on as non_chargeable_modified ');
$this->db->join('intg_projects','intg_projects.id=intg_non_chargeable_project.project_name');		
$records = $this->non_chargeable_project_model
->where('intg_non_chargeable_project.deleted','1')
->likes($field,$this->session->userdata('non_chargeable_projectfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->non_chargeable_project_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);

 Assets::add_module_css('non_chargeable_project', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Non-Chargeable Task');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Non-Chargeable Task object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Non_Chargeable_Project.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_non_chargeable_project())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('non_chargeable_project_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'non_chargeable_project');

				Template::set_message(lang('non_chargeable_project_create_success'), 'success');
				redirect(SITE_AREA .'/settings/non_chargeable_project');
			}
			else
			{
				Template::set_message(lang('non_chargeable_project_create_failure') . $this->non_chargeable_project_model->error, 'error');
			}
		}
		Assets::add_module_js('non_chargeable_project', 'non_chargeable_project.js');

		Template::set('toolbar_title', lang('non_chargeable_project_create') . ' Non-Chargeable Task');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Non-Chargeable Task data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('non_chargeable_project_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/non_chargeable_project');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Non_Chargeable_Project.Settings.Edit');

			if ($this->save_non_chargeable_project('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('non_chargeable_project_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'non_chargeable_project');

				Template::set_message(lang('non_chargeable_project_edit_success'), 'success');
                                redirect(SITE_AREA .'/settings/non_chargeable_project');
			}
			else
			{
				Template::set_message(lang('non_chargeable_project_edit_failure') . $this->non_chargeable_project_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Non_Chargeable_Project.Settings.Delete');

			if ($this->non_chargeable_project_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('non_chargeable_project_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'non_chargeable_project');

				Template::set_message(lang('non_chargeable_project_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/non_chargeable_project');
			}
			else
			{
				Template::set_message(lang('non_chargeable_project_delete_failure') . $this->non_chargeable_project_model->error, 'error');
			}
		}
		Template::set('non_chargeable_project', $this->non_chargeable_project_model->find($id));
		Template::set('toolbar_title', lang('non_chargeable_project_edit') .' Non-Chargeable Task');
		Template::render();
	}

	//--------------------------------------------------------------------

	
	public function view()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('non_chargeable_project_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/non_chargeable_project');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Non_Chargeable_Project.Settings.Edit');

			if ($this->save_non_chargeable_project('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('non_chargeable_project_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'non_chargeable_project');

				Template::set_message(lang('non_chargeable_project_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('non_chargeable_project_edit_failure') . $this->non_chargeable_project_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Non_Chargeable_Project.Settings.Delete');

			if ($this->non_chargeable_project_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('non_chargeable_project_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'non_chargeable_project');

				Template::set_message(lang('non_chargeable_project_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/non_chargeable_project');
			}
			else
			{
				Template::set_message(lang('non_chargeable_project_delete_failure') . $this->non_chargeable_project_model->error, 'error');
			}
		}
		Template::set('non_chargeable_project', $this->non_chargeable_project_model->find($id));
		Template::set('toolbar_title', lang('non_chargeable_project_edit') .' Non-Chargeable Task');
		Template::render();
	}

public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_non_chargeable_project SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('non_chargeable_project_success'), 'success');
											redirect(SITE_AREA .'/settings/non_chargeable_project/deleted');
				}
				else
				{
					
					Template::set_message(lang('non_chargeable_project_error') . $this->non_chargeable_project_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_non_chargeable_project where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('non_chargeable_project_purged'), 'success');
					redirect(SITE_AREA .'/settings/non_chargeable_project/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('non_chargeable_project_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/non_chargeable_project');
		}

		
		
		
		Template::set('non_chargeable_project', $this->non_chargeable_project_model->find($id));
		
		Assets::add_module_js('non_chargeable_project', 'non_chargeable_project.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Non_chargeable_project');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_non_chargeable_project($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['project_name']        = $this->input->post('non_chargeable_project_project_name');
		$data['milestone']        = $this->input->post('non_chargeable_project_milestone');
		$data['task_name']        = $this->input->post('non_chargeable_project_task_name');
		$data['created_by']        = $this->auth->user_id();
		$data['company_id']        = $this->auth->company_id();

		if ($type == 'insert')
		{
			$id = $this->non_chargeable_project_model->insert($data);
			$this->load->model('task_pool/task_pool_model',null,true);
		$data2 = array();
		$data2['task_name']        = $this->input->post('non_chargeable_project_task_name');
		$data2['description']        = $this->input->post('non_chargeable_project_task_name');
		$data2['created_by']        = $this->auth->user_id();
		$data2['company_id']        = $this->auth->company_id();
		$data2['status_nc']        = "yes";
		//print_r($data2);
		$task_pool_id =$this->task_pool_model->skip_validation(true)->insert($data2);
		
		/* $this->load->model('projects/projects_model',null,true);
		$data3 = array();
		$data3['project_name']        = $this->input->post('non_chargeable_project_project_name');
		$data3['created_by']        = $this->auth->user_id();
		$data3['initiator']        = $this->auth->user_id();
		$data3['projects_color']        = '#2e3e4e';
		
		$data3['status_nc']        = "yes";
		//print_r($data2);
		$project_id = $this->projects_model->skip_validation(true)->insert($data3); */
		//echo $this->db->last_query();
		
		$mile = $this->db->query("select milestone_name from  intg_nc_milestone where id = ".$this->input->post('non_chargeable_project_milestone')."")->row();
		$milestonename = $this->db->query("select * from  intg_milestones where milestone_name = '".$mile->milestone_name."'")->row();
		
	$this->load->model('milestone/milestone_model',null,true);
		$data4 = array();
		$data4['milestone_name']        = $mile->milestone_name;
		$data4['project_id']        = $this->input->post('non_chargeable_project_project_name');
		$data4['created_by']        = $this->auth->user_id();
		
		$data4['status']        = "yes";
		//print_r($data2);
		 
                if($milestonename->milestone_id==null || $milestonename->milestone_id == '' ){
                    $milestone_id = $this->milestone_model->skip_validation(true)->insert($data4);
                }else{
                    $milestone_id = $milestonename->milestone_id;
                }
		  $this->load->model('milestone_tasks/milestone_tasks_model',null,true);
		$data3 = array();
		$data3['project_id']        = $this->input->post('non_chargeable_project_project_name');
		$data3['milestone_id']        = $milestone_id;
		$data3['task_pool_id']        = $task_pool_id;
		
		
		
		//print_r($data2);
		$this->milestone_tasks_model->skip_validation(true)->insert($data3); 
		 
		 
		//exit;
			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->non_chargeable_project_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}