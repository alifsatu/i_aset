
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/non_chargeable_project') ?>"style="border-radius:0"  id="list"><?php echo lang('non_chargeable_project_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Non_Chargeable_Project.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/non_chargeable_project/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('non_chargeable_project_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Non_Chargeable_Project.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/non_chargeable_project/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
