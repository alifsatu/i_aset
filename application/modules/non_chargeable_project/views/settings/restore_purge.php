<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($non_chargeable_project))
{
	$non_chargeable_project = (array) $non_chargeable_project;
}
$id = isset($non_chargeable_project['id']) ? $non_chargeable_project['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Non-Chargeable Task</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('non_chargeable_project_project_name', $options, set_value('non_chargeable_project_project_name', isset($non_chargeable_project['project_name']) ? $non_chargeable_project['project_name'] : ''), 'Project Name'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('non_chargeable_project_milestone', $options, set_value('non_chargeable_project_milestone', isset($non_chargeable_project['milestone']) ? $non_chargeable_project['milestone'] : ''), 'Milestone'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('task_name') ? 'error' : ''; ?>">
				<?php echo form_label('Task Name'. lang('bf_form_label_required'), 'non_chargeable_project_task_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'non_chargeable_project_task_name', 'id' => 'non_chargeable_project_task_name', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('non_chargeable_project_task_name', isset($non_chargeable_project['task_name']) ? $non_chargeable_project['task_name'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('task_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created By', 'non_chargeable_project_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='non_chargeable_project_created_by' class='input-sm input-s  form-control' type='text' name='non_chargeable_project_created_by' maxlength="25" value="<?php echo set_value('non_chargeable_project_created_by', isset($non_chargeable_project['created_by']) ? $non_chargeable_project['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company', 'non_chargeable_project_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='non_chargeable_project_company_id' class='input-sm input-s  form-control' type='text' name='non_chargeable_project_company_id' maxlength="255" value="<?php echo set_value('non_chargeable_project_company_id', isset($non_chargeable_project['company_id']) ? $non_chargeable_project['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/non_chargeable_project', lang('non_chargeable_project_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Non_Chargeable_Project.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('non_chargeable_project_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>