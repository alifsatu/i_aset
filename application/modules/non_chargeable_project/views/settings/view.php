<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($non_chargeable_project))
{
	$non_chargeable_project = (array) $non_chargeable_project;
}
$id = isset($non_chargeable_project['id']) ? $non_chargeable_project['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Non-Chargeable Task</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
			/* 	$options = array(
					255 => 255,
				);

				echo form_dropdown('non_chargeable_project_project_name', $options, set_value('non_chargeable_project_project_name', isset($non_chargeable_project['project_name']) ? $non_chargeable_project['project_name'] : ''), 'Project Name'. lang('bf_form_label_required'));
			 */
			?>
						<?php // Change the values in this array to populate your dropdown as required
				
$prgm = $this->db->query('SELECT * FROM  intg_projects WHERE id = '.$non_chargeable_project['project_name'].' ')->result();
$milestonename = $this->db->query('SELECT milestone_name FROM  intg_nc_milestone WHERE id = '.$non_chargeable_project['milestone'].' ')->row()->milestone_name;
//echo 'SELECT * FROM  intg_projects WHERE id = '.$non_chargeable_project['project_name'].' '; 
foreach($prgm as $pn) 
{
	$ppp = $pn->project_name;
	//echo $pn->project_name;
}	
//echo "ppp".$ppp;
	?>
			 <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>"> 
				<?php echo form_label('Project Name', 'non_chargeable_project_project_name', array('class' => 'control-label') ); ?> : <? echo $ppp; ?>
				<div class='controls'>
					<span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
				</div>
			</div>


			<div class="form-group <?php echo form_error('milestone_name') ? 'error' : ''; ?>">
				<?php echo form_label('Milestone Name', 'non_chargeable_project_task_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo $milestonename; ?>
					<span class='help-inline'><?php echo form_error('milestone_name'); ?></span>
				</div>
			</div>
                    
			<div class="form-group <?php echo form_error('task_name') ? 'error' : ''; ?>">
				<?php echo form_label('Task Name', 'non_chargeable_project_task_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?= isset($non_chargeable_project['task_name']) ? $non_chargeable_project['task_name'] : ''; ?>
					<span class='help-inline'><?php echo form_error('task_name'); ?></span>
				</div>
			</div>

			<!--<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created By', 'non_chargeable_project_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='non_chargeable_project_created_by' class='input-sm input-s  form-control' type='text' name='non_chargeable_project_created_by' maxlength="25" value="<?php echo set_value('non_chargeable_project_created_by', isset($non_chargeable_project['created_by']) ? $non_chargeable_project['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company', 'non_chargeable_project_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='non_chargeable_project_company_id' class='input-sm input-s  form-control' type='text' name='non_chargeable_project_company_id' maxlength="255" value="<?php echo set_value('non_chargeable_project_company_id', isset($non_chargeable_project['company_id']) ? $non_chargeable_project['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>-->

			
		</fieldset>
    <?php echo form_close(); ?>
</div>