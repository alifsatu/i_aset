<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['non_chargeable_project_manage']			= 'Manage';
$lang['non_chargeable_project_edit']				= 'Edit';
$lang['non_chargeable_project_true']				= 'True';
$lang['non_chargeable_project_false']				= 'False';
$lang['non_chargeable_project_create']			= 'Save';
$lang['non_chargeable_project_list']				= 'List';
$lang['non_chargeable_project_new']				= 'New';
$lang['non_chargeable_project_edit_text']			= 'Edit this to suit your needs';
$lang['non_chargeable_project_no_records']		= 'There aren\'t any non_chargeable_project in the system.';
$lang['non_chargeable_project_create_new']		= 'Create a new Non-Chargeable Task.';
$lang['non_chargeable_project_create_success']	= 'Non-Chargeable Task successfully created.';
$lang['non_chargeable_project_create_failure']	= 'There was a problem creating the non_chargeable_project: ';
$lang['non_chargeable_project_create_new_button']	= 'Create New Non-Chargeable Task';
$lang['non_chargeable_project_invalid_id']		= 'Invalid Non-Chargeable Task ID.';
$lang['non_chargeable_project_edit_success']		= 'Non-Chargeable Task successfully saved.';
$lang['non_chargeable_project_edit_failure']		= 'There was a problem saving the non_chargeable_project: ';
$lang['non_chargeable_project_delete_success']	= 'record(s) successfully deleted.';

$lang['non_chargeable_project_purged']	= 'record(s) successfully purged.';
$lang['non_chargeable_project_success']	= 'record(s) successfully restored.';


$lang['non_chargeable_project_delete_failure']	= 'We could not delete the record: ';
$lang['non_chargeable_project_delete_error']		= 'You have not selected any records to delete.';
$lang['non_chargeable_project_actions']			= 'Actions';
$lang['non_chargeable_project_cancel']			= 'Cancel';
$lang['non_chargeable_project_delete_record']		= 'Delete';
$lang['non_chargeable_project_delete_confirm']	= 'Are you sure you want to delete this non_chargeable_project?';
$lang['non_chargeable_project_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['non_chargeable_project_action_edit']		= 'Save';
$lang['non_chargeable_project_action_create']		= 'Create';

// Activities
$lang['non_chargeable_project_act_create_record']	= 'Created record with ID';
$lang['non_chargeable_project_act_edit_record']	= 'Updated record with ID';
$lang['non_chargeable_project_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['non_chargeable_project_column_created']	= 'Created';
$lang['non_chargeable_project_column_deleted']	= 'Deleted';
$lang['non_chargeable_project_column_modified']	= 'Modified';
