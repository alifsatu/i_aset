<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Install_tasks_permissions extends Migration
{

	/**
	 * Permissions to Migrate
	 *
	 * @var Array
	 */
	private $permission_values = array(
		array(
			'name' => 'Tasks.Quotes.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Quotes.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Quotes.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Quotes.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Quotes.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Quotes.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Invoices.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Client.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Content.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Reports.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Settings.Delete',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.View',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.Deleted',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.Create',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.Edit',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.Restore_purge',
			'description' => '',
			'status' => 'active',
		),
		array(
			'name' => 'Tasks.Developer.Delete',
			'description' => '',
			'status' => 'active',
		),
	);

	/**
	 * The name of the permissions table
	 *
	 * @var String
	 */
	private $table_name = 'permissions';

	/**
	 * The name of the role/permissions ref table
	 *
	 * @var String
	 */
	private $roles_table = 'role_permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this migration
	 *
	 * @return void
	 */
	public function up()
	{
		$role_permissions_data = array();
		foreach ($this->permission_values as $permission_value)
		{
			$this->db->insert($this->table_name, $permission_value);

			$role_permissions_data[] = array(
				'role_id' => '1',
				'permission_id' => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->roles_table, $role_permissions_data);
	}

	//--------------------------------------------------------------------

	/**
	 * Uninstall this migration
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->permission_values as $permission_value)
		{
			$query = $this->db->select('permission_id')
				->get_where($this->table_name, array('name' => $permission_value['name'],));

			foreach ($query->result() as $row)
			{
				$this->db->delete($this->roles_table, array('permission_id' => $row->permission_id));
			}

			$this->db->delete($this->table_name, array('name' => $permission_value['name']));
		}
	}

	//--------------------------------------------------------------------

}