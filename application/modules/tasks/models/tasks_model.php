<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tasks_model extends BF_Model {

	protected $table_name	= "intg_tasks";
	protected $key			= "id";
	protected $soft_deletes	= true;
	protected $date_format	= "datetime";

	protected $log_user 	= FALSE;

	protected $set_created	= false;
	protected $set_modified = false;

	/*
		Customize the operations of the model without recreating the insert, update,
		etc methods by adding the method names to act as callbacks here.
	 */
	protected $before_insert 	= array();
	protected $after_insert 	= array();
	protected $before_update 	= array();
	protected $after_update 	= array();
	protected $before_find 		= array();
	protected $after_find 		= array();
	protected $before_delete 	= array();
	protected $after_delete 	= array();

	/*
		For performance reasons, you may require your model to NOT return the
		id of the last inserted row as it is a bit of a slow method. This is
		primarily helpful when running big loops over data.
	 */
	protected $return_insert_id 	= TRUE;

	// The default type of element data is returned as.
	protected $return_type 			= "object";

	// Items that are always removed from data arrays prior to
	// any inserts or updates.
	protected $protected_attributes = array();

	/*
		You may need to move certain rules (like required) into the
		$insert_validation_rules array and out of the standard validation array.
		That way it is only required during inserts, not updates which may only
		be updating a portion of the data.
	 */
	protected $validation_rules 		= array(
		array(
			"field"		=> "tasks_project_id",
			"label"		=> "Project Id",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_task_name",
			"label"		=> "Task Name",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_description",
			"label"		=> "Description",
			"rules"		=> "max_length[1000]"
		),
		array(
			"field"		=> "tasks_parent_task_id",
			"label"		=> "Parent Task Id",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_progress_percentage",
			"label"		=> "Progress Percentage",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_weightage_percentage",
			"label"		=> "Weightage Percentage",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_color",
			"label"		=> "Color",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_send_notification",
			"label"		=> "Send Notification",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_estimated_hours",
			"label"		=> "Estimated Hours",
			"rules"		=> "max_length[45]"
		),
		array(
			"field"		=> "tasks_notes",
			"label"		=> "Notes",
			"rules"		=> "max_length[45]"
		),/*
		array(
			"field"		=> "tasks_task_tag",
			"label"		=> "Task Tag",
			"rules"		=> "max_length[45]"
		),*/
	);
	protected $insert_validation_rules 	= array();
	protected $skip_validation 			= FALSE;

	//--------------------------------------------------------------------

	public function disable_add_new_ts()
	{
		 
		 $period =  $this->uri->segment(6) =='3' ? 1 : 2; 
		 
		  $uid = $this->auth->user_id();
		 
		  $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . date("n") . "' and yr ='" . date("Y") . "' and period = '$period'");
		  
		
			if ( $r->num_rows() > 0 ) {
				
			
				
				 if ($r->row()->status == "Created" && $r->row()->period == 1 || $r->row()->status == "Created" && $r->row()->period == 2) { $disable_add_new_ts = 'true';	}			
				 
				 if ($r->row()->status == "Submitted" && $r->row()->period == 1 ) {
				  if ( $this->uri->segment(6)=='3' ) {  $disable_add_new_ts = 'false'; }  
				 if ( $this->uri->segment(6)=='4' ) { $disable_add_new_ts = 'true'; } 		
				 }
				
				 if ($r->row()->status == "Submitted" && $r->row()->period == 2 ) { $disable_add_new_ts = 'false';}
				 
				
			}
			else{
				
			 $disable_add_new_ts = 'true';	
			}
			
			return $disable_add_new_ts;
           
		
	}
	
	public function get_other_approvers()
	{
	//timesheet	
	$all_approvers = $this->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = '.$this->auth->user_id().' and work_flow_module_id = "41" ')->result();
	
		
		$i=0;
		foreach($all_approvers as $b)
		{
		$options[$i] = $b->work_flow_approvers;
		$i++;
		}
		return implode(",",$options);	
		
	}
	
}
