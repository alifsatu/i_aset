 $(".moreless").click(function () {
	 if ($(this).hasClass("active")) { 
            $("#moreless").slideDown("slow", function () {});
		
        } else {
            $("#moreless").slideUp("slow", function () {});
        }
   
	});
	
	
	$(".massupd").on("focus", function () {
		$(this).popover('destroy');
		$(".popover").hide();
	
       $(this).popover({placement: function (context, source) {
        var position = $(source).position();

        if (position.left > 515) {
            return "left";
        }

        if (position.left < 100) {
            return "right";
        }

                if (position.top < 110) {
            return "bottom";
        }

        return "top";
    },
            html: true, title: "Add Comments<a href='#' class='btn btn-xs btn-warning pull-right' onclick=$('.popover').hide()><i class='fa fa-times' data-date'close'></i></a><button  class='btn btn-xs btn-success pull-right'><i class='fa fa-check'></i></button>", content: '<textarea name="comments"  cols="30" rows="2" class="form-control expand">' + $(this).attr("data-comments") + '</textarea>'});
		
	});
	
	
    $(".hour").on("click", function () {
		$(this).popover('destroy');
		$(".popover").hide();
	
       $(this).popover({placement: function (context, source) {
        var position = $(source).position();

        if (position.left > 515) {
            return "left";
        }

        if (position.left < 100) {
            return "right";
        }

                if (position.top < 110) {
            return "bottom";
        }

        return "top";
    },
            html: true, title: "Add Comments<a href='#' class='btn btn-xs btn-warning pull-right' onclick=$('.popover').hide()><i class='fa fa-times' data-date'close'></i></a><button  class='btn btn-xs btn-success pull-right'><i class='fa fa-check'></i></button>", content: '<textarea name="comments"  cols="30" rows="2" class="form-control expand">' + $(this).attr("data-comments") + '</textarea>'});
		
	});
	
	
	
 /*var leftPos = $('.innerWrapper').scrollLeft();
  $(".innerWrapper").animate({scrollLeft: leftPos + 1200}, 800);
    */
   
  /*
  $(window).resize(function () {
     $(".table-scrollable").css('width', $(document).width() - 100 + "px");  
     $(".table-scrollable").css('overflow-x', 'scroll');
     $(".table-scrollable").css('white-space', 'nowrap'); 
	
});
$(window).resize();*/

  function validateMaxVal(maxval, val, id) {
	
        if (val <= maxval) {
//            alert("Maximum is "+maxval+" Hours"+val+" ---"+id);
            return true;
        } else {
            alert("Maximum is " + maxval + " Hours" + val + " ---" + id);
            $("#" + id).val("");
            $("#" + id).focus();
            return false;
        }
    }
	

    $(function(){

        var table = $("#tstable").stupidtable();
        table.on("beforetablesort", function (event, data) {
          // Apply a "disabled" look to the table while sorting.
          // Using addClass for "testing" as it takes slightly longer to render.
          $("#msg").text("Sorting...");
         table.addClass("disabled");
        });
        table.on("aftertablesort", function (event, data) {
          // Reset loading message.
          $("#msg").html("&nbsp;");
          table.removeClass("disabled");
          var th = $(this).find("th");
          th.find(".arrow").remove();
          var dir = $.fn.stupidtable.dir;
//          var arrow = data.direction === dir.ASC ? "&uarr;" : "&darr;";
          var arrow = data.direction === dir.ASC ? "sort-asc" : "sort-desc";
//          th.eq(data.column).prepend('<span class="arrow center">' + arrow +'</span>');
          th.eq(data.column).prepend('<i class="arrow fa fa-'+arrow+'"></i>');
        });
    });
