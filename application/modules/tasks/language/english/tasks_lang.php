<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['tasks_manage']			= 'Manage';
$lang['tasks_edit']				= 'Edit';
$lang['tasks_true']				= 'True';
$lang['tasks_false']				= 'False';
$lang['tasks_create']			= 'Save';
$lang['tasks_list']				= 'List';
$lang['tasks_new']				= 'New';
$lang['tasks_edit_text']			= 'Edit this to suit your needs';
$lang['tasks_no_records']		= 'There aren\'t any tasks in the system.';
$lang['tasks_create_new']		= 'Create a new Tasks.';
$lang['tasks_create_success']	= 'Tasks successfully created.';
$lang['tasks_create_failure']	= 'There was a problem creating the tasks: ';
$lang['tasks_create_new_button']	= 'Create New Tasks';
$lang['tasks_invalid_id']		= 'Invalid Tasks ID.';
$lang['tasks_edit_success']		= 'Tasks successfully saved.';
$lang['tasks_edit_failure']		= 'There was a problem saving the tasks: ';
$lang['tasks_delete_success']	= 'record(s) successfully deleted.';

$lang['tasks_purged']	= 'record(s) successfully purged.';
$lang['tasks_success']	= 'record(s) successfully restored.';


$lang['tasks_delete_failure']	= 'We could not delete the record: ';
$lang['tasks_delete_error']		= 'You have not selected any records to delete.';
$lang['tasks_actions']			= 'Actions';
$lang['tasks_cancel']			= 'Cancel';
$lang['tasks_delete_record']		= 'Delete';
$lang['tasks_delete_confirm']	= 'Are you sure you want to delete this tasks?';
$lang['timesheet_submit_confirm']	= 'Are you sure you want to submit this timesheet?';
$lang['tasks_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['tasks_action_edit']		= 'Save';
$lang['tasks_action_create']		= 'Create';
$lang['projects_calendar']		= 'Calendar';
$lang['tasks_create_error']	= 'Please fix the following errors:';
$lang['tasks']	= 'Tasks';
$lang['projects_name']	= 'Project';
$lang['task_name']	= 'Task Name';
$lang['tasks_desc']	= 'Description';
$lang['tasks_start_date']	= 'Start Date';
$lang['tasks_end_date']	= 'End Date';
$lang['tasks_assigned_to']	= 'Assigned To';
$lang['tasks_assignees']	= 'assignees';
$lang['progress_percentage']	= 'How much is done?';
$lang['weightage_percentage']	= 'How important is this task?';
$lang['tasks_color']	= 'Color';
$lang['tasks_notes']	= 'Notes';
$lang['Tags']	= 'Tags';
$lang['projects_client']	= 'Client';
$lang['created_by']	= 'Created by';
$lang['Weightage']	= 'Weightage';
$lang['created_on']	= 'Created On';
$lang['projects_status']	= 'Project Status';
$lang['projects_cost']	= 'Project Cost';
$lang['overall_progress']	= 'Overall Progress';

// Activities
$lang['tasks_act_create_record']	= 'Created record with ID';
$lang['tasks_act_edit_record']	= 'Updated record with ID';
$lang['tasks_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['tasks_column_created']	= 'Created';
$lang['tasks_column_deleted']	= 'Deleted';
$lang['tasks_column_modified']	= 'Modified';

// list data

$lang['tasks_allfields']				= 'All Fields';
$lang['tasks_showing']	= 'Showing';
$lang['tasks_progress']	= 'Progress';
$lang['tasks_of']	= 'of';
$lang['tasks_records']	= 'No records found that match your selection.';


$lang['quote_approvers_hierarchy']	= 'Approvers Hierarchy';
$lang['quote_approved_by']	= 'Approved By';
$lang['quote_role']	= 'Role';
$lang['quote_approver_status']	= 'Status';
$lang['quote_approver_remarks']	= 'Remarks';
$lang['quote_approver_datetime']	= 'Date & Time';
$lang['quote_approver_action']	= 'Approver Action';
$lang['quote_approver_action_status']	= 'Approval Status';
$lang['quote_approver_action_yes']	= 'YES';
$lang['quote_approver_action_no']	= 'NO';
$lang['projects_create_success']	= 'Timesheet successfully submitted for approval.';
$lang['task_submit_success_auto']	= 'Timesheet has been automatically approved.';
$lang['task_delete_success']		= 'Task(s) deleted from the timesheet.';
$lang['task_delete_fail']			= 'There was a problem deleting the task for the timesheet: ';
