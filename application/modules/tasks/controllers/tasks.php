<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * tasks controller
 */
class tasks extends Front_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('tasks_model', null, true);
        $this->lang->load('tasks');


        Assets::add_module_js('tasks', 'tasks.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void
     */
    public function index() {

        $records = $this->tasks_model->find_all();

        Template::set('records', $records);
        Template::render();
    }

    /**
     * MUST BE RUN ON 1st DAY ON EACH MONTH
     * ·   timesheet_duplicate()
     * https://timesheet.acgt.com/index.php/tasks/timesheet_duplicate
     * It checks for last months  timesheets and try reinserts them with the 
     * month = current month, 
     * year = current year and status = ‘Created’ which enables the timesheet entry
     * If the duplicate is redundant, then it skip inserting

      // timesheet table must have unique key set on uid	,	mth	,	period	,	yr to prevent duplicate entry of the same timesheet over and over
     */
    public function timesheet_duplicate() {

        // set year current year if month is prev year month
        $year = date('m', strtotime('last month')) == "12" ? date('Y', strtotime('last year')) : date('Y');


        $tm = $this->db->query("select * from intg_timesheet where mth = '" . date('m', strtotime('last month')) . "' and yr = '" . $year . "' AND uid NOT IN (SELECT id FROM intg_users WHERE banned='1') ")->result();

        foreach ($tm as $tmnew) {


            $this->db->query("INSERT INTO intg_timesheet ( `uid`, `mth`,`period`, `yr`, `status`, `parent_Rid`,  `final_status`,  `tsother_approvers`,`final_approvers`,`approved_viewers`,`submission_date`,`deleted`) VALUES ( '" . $tmnew->uid . "', '" . date('m') . "', '" . $tmnew->period . "','" . date('Y') . "', 'Created', NULL,  'No', '" . $tmnew->tsother_approvers . "','" . $tmnew->final_approvers . "','" . $tmnew->approved_viewers . "','" . date("Y-m-d") . "','0')");

            $id = mysql_insert_id();
            $tms = $this->db->query("select  itd.pid, itd.mid, itd.ptskid, itd.status,day(itd.tsd_date) as tsdday, DATEDIFF(im.end_date,now()) as msenddate from intg_timesheet_details itd, intg_milestones im where itd.mid = im.milestone_id and itd.tid = '" . $tmnew->id . "'")->result();

            if ($id != "") {

                foreach ($tms as $tmsn) {

                    if ($tmsn->msenddate >= 0) {

                        $this->db->query("INSERT INTO intg_timesheet_details ( `tid`, `pid`, `mid`, `ptskid`, `tsd_date`, `tsd_hours`, `comments`, `status`) VALUES ( '" . $id . "', '" . $tmsn->pid . "', '" . $tmsn->mid . "', '" . $tmsn->ptskid . "', '" . date("Y-m") . "-" . $tmsn->tsdday . "', '0.00', NULL, 'enabled')");
                    }
                }
            }
            $id = "";
        }
    }

    public function timesheet_duplicate_alif($SelectPeriod = "", $copyToPeriod = "", $userid = "", $from_mth = "", $from_year = "", $to_mth = "", $to_year = "") {

        if ($this->session->userdata('role_id') != '1') {
            echo '<h1>Only Super Admin can run this function!!!</h1>';
            //sample http://localhost/acgt/index.php/tasks/timesheet_duplicate_alif/2/2/325/8/2017/9/2017
            die("");
        }
        echo '<br>timesheet_duplicate_alif/SelectPeriod/copyToPeriod/userid/from_mth/from_year/$to_mth/$to_year';
        if ($SelectPeriod == "") {
            echo '<h3>SelectPeriod has no value</h3>';

            die("");
        }
        if ($copyToPeriod == "") {
            echo '<h3>copyToPeriod  has no value</h3>';
            die("");
        }
        if ($userid == "") {
            echo '<h3>This Function affected all users timesheet records</h3><br>If you want to update for single user, please enter the user id after CopyToPeriod segment';
            $AllorNot = "All";
        } else {
            echo '<h3>This Function affected user id =' . $userid . ' timesheet records</h3>';
            $AllorNot = $userid;
        }
        if ($from_mth == "") {
            echo '<h3>from_mth has no value</h3>';
            die("");
        }
        if ($from_year == "") {
            echo '<h3>from_year has no value</h3>';
            die("");
        }
        if ($to_mth == "") {
            echo '<h3>to_mth has no value</h3>';
            die("");
        }
        if ($to_year == "") {
            echo '<h3>to_year has no value</h3>';
            die("");
        }

//        echo "SelectPeriod = ".$SelectPeriod." <br>copyToPeriod=".$copyToPeriod." <br>userid=" .$userid." <br>from_mth=".$from_mth." <br>from_year=".$from_year." <br>to_mth= ".$to_mth." <br>to_year= ".$to_year;die();

        $success = 0;
        $failed = 0;

        //from
        if ($from_mth == NULL) {
            $from_mth = date('m', strtotime('last month'));
        }
        if ($from_year == NULL) {
            // set year current year if month is prev year month
            $from_year = date('m', strtotime('last month')) == "12" ? date('Y', strtotime('last year')) : date('Y');
        }

        //to
        if ($to_mth == NULL) {
            $to_mth = date('m');
        }
        if ($to_year == NULL) {
            // set year current year if month is prev year month
            $to_year = date('Y');
        }


        echo "--Copy From  <br>Period " . $SelectPeriod . " of month " . $from_mth . " of year" . $from_year . '-->> ';
        echo "To Period " . $copyToPeriod . " of month " . $to_mth . " of " . $to_year . '--';
        echo "<br>For User : " . $AllorNot;


        $tm_sql = "SELECT * FROM intg_timesheet WHERE mth = '" . $from_mth . "' and yr = '" . $from_year . "'";

        if ($userid != "" || $userid != 0 || $userid != NULL) {
            $tm_sql .= " AND uid = " . $userid;
        }

        if ($SelectPeriod != "" || $SelectPeriod != "0" || $SelectPeriod != NULL) {
            $tm_sql .= " AND period = " . $SelectPeriod;
            if ($copyToPeriod == "") {
                echo "Please provide the period to copy to";
                die();
            }
        } else {
            echo "Please provide the period to copy to";
            die();
        }
        echo $tm_sql;
        $tm = $this->db->query($tm_sql)->result();


        if ($tm) {

            foreach ($tm as $tmnew) {
//        print_r($tmnew);

                $newTsPeriod = '1';
                if ($tmnew->period == '1') {
                    $newTsPeriod = '2';
                }

                if ($copyToPeriod != "" || $copyToPeriod != "0" || $copyToPeriod != NULL) {
                    $newTsPeriod = $copyToPeriod;
                }

                //check if the timesheet exist
                $ifExist_sql = "SELECT * FROM intg_timesheet WHERE uid = '" . $tmnew->uid . "' AND mth = '" . $to_mth . "' AND period = '" . $newTsPeriod . "' AND yr = '" . $to_year . "' ORDER BY id ASC ";
                echo "<br><br>";
                $ifExist = $this->db->query($ifExist_sql)->row();

                $id = "";
                if ($ifExist) {
                    $id = $ifExist->id;
                } else {
                    //insert timesheet if not exist
                    echo $insert_ts = "INSERT INTO intg_timesheet ( `uid`, `mth`,`period`, `yr`, `status`, `parent_Rid`,  `final_status`,  `tsother_approvers`,`final_approvers`,`approved_viewers`,`deleted`) VALUES ( '" . $tmnew->uid . "', '" . $to_mth . "', '" . $newTsPeriod . "','" . $to_year . "', 'Created', NULL,  'No', '" . $tmnew->tsother_approvers . "','" . $tmnew->final_approvers . "','" . $tmnew->approved_viewers . "','0')";
                    echo "<br><br>";

                    $this->db->query($insert_ts);
                    $id = mysql_insert_id();
                }

                echo "<br><br><<<<<< Main Timesheet ID : " . $id . ">>>> <br><br>";
                $tms_sql = "select  itd.pid, itd.mid, itd.ptskid, itd.status,day(itd.tsd_date) as tsdday, DATEDIFF(im.end_date,now()) as msenddate from intg_timesheet_details itd, intg_milestones im where itd.mid = im.milestone_id and itd.tid = '" . $tmnew->id . "'";
                echo "<br><br>";

                $tms = $this->db->query($tms_sql)->result();
//            print_r($tms);

                if ($id != "") {
                    echo " Have Main Timesheet ID <br><br>";
                    foreach ($tms as $tmsn) {

                        if ($tmsn->msenddate >= 0) {

                            if ($copyToPeriod == '1') {
                                if ($tmsn->tsdday == "16")
                                    $tsdday = "01";
                                if ($tmsn->tsdday == "17")
                                    $tsdday = "02";
                                if ($tmsn->tsdday == "18")
                                    $tsdday = "03";
                                if ($tmsn->tsdday == "19")
                                    $tsdday = "04";
                                if ($tmsn->tsdday == "20")
                                    $tsdday = "05";
                                if ($tmsn->tsdday == "21")
                                    $tsdday = "06";
                                if ($tmsn->tsdday == "22")
                                    $tsdday = "07";
                                if ($tmsn->tsdday == "23")
                                    $tsdday = "08";
                                if ($tmsn->tsdday == "24")
                                    $tsdday = "09";
                                if ($tmsn->tsdday == "25")
                                    $tsdday = "10";
                                if ($tmsn->tsdday == "26")
                                    $tsdday = "11";
                                if ($tmsn->tsdday == "27")
                                    $tsdday = "12";
                                if ($tmsn->tsdday == "28")
                                    $tsdday = "13";
                                if ($tmsn->tsdday == "29")
                                    $tsdday = "14";
                                if ($tmsn->tsdday == "30")
                                    $tsdday = "15";
                                if ($tmsn->tsdday < "16")
                                    $tsdday = $tmsn->tsdday;
                            }else {
                                $tsdday = $tmsn->tsdday;
                            }

                            echo $insert_sql = "INSERT INTO intg_timesheet_details ( `tid`, `pid`, `mid`, `ptskid`, `tsd_date`, `tsd_hours`, `comments`, `status`) VALUES ( '" . $id . "', '" . $tmsn->pid . "', '" . $tmsn->mid . "', '" . $tmsn->ptskid . "', '" . $to_year . "-" . $to_mth . "-" . $tsdday . "', '0.00', NULL, 'enabled')";
                            $insert = $this->db->query($insert_sql);
                            echo "<br><br>";


                            if ($insert) {
                                $success++;
                            } else {
                                $failed++;
                            }
                        }
                    }
                } else {
                    echo " Takde id <br>";
                }
                echo "Success :" . $success . ", Failed :" . $failed;
                $id = "";
            }
        } else {
            echo "<br>No Timesheet To Copy From Period " . $SelectPeriod . " of month " . $from_mth . "/" . $from_year;
        }
    }

    /**
     * * MUST BE RUN ON 11th DAY ON EACH MONTH
     * @desc On the 11th it updates all the timesheet status to ‘Delayed’ – this disables the timesheet entry
     * @desc On the 12th it updates all the timesheet status to ‘Delayed’ – this disables the timesheet entry
     */
    //cron to check on submission within 24 hours, 
    public function submission_check() {
        // check if current month is december
        $year = date('m', strtotime('last month')) == "12" ? date('Y', strtotime('last year')) : date('Y');

        // if current day is 11th
        if (date("d-m-Y") == date("11-m-Y")) {

            //  change status to delayed where last month timesheet are not subitted and whose sttus is created
            $this->db->query("update intg_timesheet set status = 'Delayed' where mth = '" . date('m', strtotime('last month')) . "' and yr = '" . $year . "' and status = 'Created'");
        }

        if (date("d-m-Y") == date("12-m-Y")) {

            $this->db->query("update intg_timesheet set status = 'Delayed' where mth = '" . date('m', strtotime('last month')) . "' and yr = '" . $year . "' and status = 'Created'");
        }
    }

    /**
     *  cron job query to update daily record for cso in timesheet details
     * ·         Updates CSO’s daily timesheet hours if they are 0 or null
     */
    public function daily_cso_update_timesheet_cron() {
        /* $ts = $this->db->query("SELECT * FROM intg_timesheet it,intg_timesheet_details itd where itd.tid = it.id and it.uid = ".$this->config->item('csouid')." and itd.status='enabled'")->result();
          foreach ( $ts as $t )
          {
          $this->db->query("INSERT IGNORE INTO `intg_timesheet_details` ( `tid`, `pid`, `mid`, `ptskid`, `tsd_date`) VALUES ('".$t->tid."', '".$t->pid."', '".$t->mid."', '".$t->ptskid."', '".date("Y-m-d")."')");

          } */

        foreach ($this->config->item('csouid') as $uid) {

            $ts = $this->db->query("SELECT it.status as itstatus,it.final_status,itd.status as itdstatus,itd.id as id,itd.tsd_hours,itd.mid,itd.tsd_date FROM intg_timesheet it,intg_timesheet_details itd where itd.tid = it.id and it.uid = " . $uid . " and itd.status='enabled' and tsd_date = '" . date("Y-m-d") . "'")->result();

            //echo $this->db->last_query();



            $tssum = $this->db->query("select round(sum(itd.tsd_hours),2) as rsum from  intg_timesheet it,intg_timesheet_details itd where itd.tid = it.id and it.uid = " . $uid . " and itd.status='enabled' and tsd_date = '" . date("Y-m-d") . "'")->row()->rsum;



            $tscount = $this->db->query("select count(*) as cnt from  intg_timesheet it,intg_timesheet_details itd where itd.tid = it.id and it.uid = " . $uid . " and itd.status='enabled' and itd.tsd_date = '" . date("Y-m-d") . "' and ( itd.tsd_hours = 0 or itd.tsd_hours is null)")->row()->cnt;

            //CSO is hq so monday to thursday 8.5 Friday is 8 Sat and sun 8.5

            if (date('D') == "Fri") {
                $hours = 8;
            } else {
                $hours = 8.5;
            }

            //$hourdist = number_format(( $hours - $tssum ) / $tscount, 2);
            $hourdist = ( $hours - $tssum ) / $tscount;

            echo 'hourdist = ' . $hourdist . "<br>";
            $enabledCount = 0;
            $disabledCount = 0;
            foreach ($ts as $t) {
                if ($t->tsd_hours == 0 || $t->tsd_hours == NULL) {
                    $disable = false;
                    $day_time_stamp = strtotime($t->tsd_date);
                    $milestone_date = $this->db->query("SELECT start_date,end_date FROM intg_milestones WHERE milestone_id =" . $t->mid)->row();

                    if ($t->final_status == 'Reject') {
//                    echo " Reject : ";
                        if (strtotime($milestone_date->start_date) > $day_time_stamp || strtotime($milestone_date->end_date) < $day_time_stamp) {
                            if ($t->itdstatus == 'disabled' || $t->itstatus == 'Created' || $t->itstatus == 'Submitted' || $t->itstatus == 'Delayed') {
                                $disable = true;
                            } else {
                                //$disable = false; //FOR CSO if REJECTED WE DO NOT UPDATE AUTO
                            }
                        } else {
                            //$disable = false; //FOR CSO if REJECTED WE DO NOT UPDATE AUTO
                        }
                    } else if ($t->final_status == 'Yes') { //Alif, Quick fix
//                    echo " Approved : ";
                        $disable = true;
                    } else {
//                    echo " No : ";
                        if (strtotime($milestone_date->start_date) > $day_time_stamp || strtotime($milestone_date->end_date) < $day_time_stamp) {
                            $disable = true;
//                        echo "No - 1 , ";
                        } else {
//                        echo "No - 2 , ";
                            if ($t->itdstatus == 'disabled' || $t->itstatus == 'Submitted' || $t->itstatus == 'Delayed') {
                                $disable = true;
//                            echo "No - 2.1 , ";
                            } else {
//                            echo "No - 2.2 , ";
                                $disable = false;
                            }
                        }
                    }

                    if ($disable) {
                        //$disable = true, means the input is disabled, no need to update
                        $disabledCount++;
                    } else {
                        $enabledCount++;
                        //$disable = false, means the input is enabled, need to update
                        $this->db->query("update intg_timesheet_details set tsd_hours = '" . $hourdist . "' where id = '" . $t->id . "'");
                    }
                }
            }

            echo "Enabled Row =" . $enabledCount . "<br>";
            echo "Disabled Row = " . $disabledCount . "<br>";
        }
    }

    //--------------------------------------------------------------------

    /**
     * @desc - Can be used at any day ( not necessarily 3rd or 6th ) to check last month’s timesheet if submitted for approval , 
     * if not it sends email to that timesheet work flow approvers
     * @desc On the 7th of the current month, if the timesheet are not submitted for approval, it locks the timesheet – status ‘Delayed’
     */
    public function timesheet_non_submitted_367() {

        $filepath = APPPATH . 'logs/timesheet_367-log-' . date('Y-m-d') . '.php'; // Creating Email Log file with today's date in application/logs folder
        $handle = fopen($filepath, "a+");                 // Opening file with pointer at the end of the file
        $text = "Execution Time:" . date("D j/n/Y H:i", time()) . '\n\n';

        //Get email list to send
        //get which role will get the email
        $this->load->model('roles/role_model', null, true);
        $this->role_model->select("role_id");
        $this->role_model->where('received_367_email', '1');
        $role = $this->role_model->find_all();

        foreach ($role as $val) {
            $role_list[] = $val->role_id;
        }

        $this->db->select($this->db->dbprefix('users') . ".id");
        $this->db->select($this->db->dbprefix('users') . ".email");
        $this->db->select($this->db->dbprefix('roles') . ".role_name");
        $this->db->from($this->db->dbprefix('users'));
        $this->db->join($this->db->dbprefix('roles'), $this->db->dbprefix('roles') . ".role_id =" . $this->db->dbprefix('users') . ".role_id", "LEFT");
        $this->db->where_in($this->db->dbprefix('users') . '.role_id', $role_list, FALSE);
        $this->db->where($this->db->dbprefix('users') . '.deleted', "0");
        $this->db->where($this->db->dbprefix('users') . '.banned', "0");
        $personWillReceive = $this->db->get()->result_array();

        $year = date('m', strtotime('last month')) == "12" ? date('Y', strtotime('last year')) : date('Y');
//$days = array ( 3,6,7);		

        if (date("d-m-Y") == date("07-m-Y")) {
            // update timesheet set status to delayed where prev month is not submitted
            $this->db->query("update intg_timesheet set status = 'Delayed' where mth = '" . date('n', strtotime('last month')) . "' and yr = '" . $year . "' and status = 'Created'");
        }

        //select delayed timesheet
        $message = PHP_EOL . '<style>
                                table {
                                    border-collapse: collapse;
                                    width: 100%;
                                }

                                th, td {
                                    text-align: left;
                                    padding: 8px;
                                }

                                tr:nth-child(even){background-color: #f2f2f2}

                                th {
                                    background-color: #4CAF50;
                                    color: white;
                                }
                                </style>
                                <p><b>Please find the list of users that failed to submit their timesheet for the period of ' . date('F', strtotime('last month')) . ', ' . $year . '</b></p>
								<table width="80%" border="1">
									  <tr>
									  <th>No</th>
									  <th>Timesheet Owner</th>
									  <th>Cost Center</th>
									  <th>Period(1st or 2nd half)</th>
									</tr>
									';


        $delayed = $this->db->query("select * from intg_timesheet where status = 'Created' and mth = '" . date('n', strtotime('last month')) . "' and yr = '" . $year . "' AND uid NOT IN (SELECT id FROM intg_users WHERE banned='1')  ");

        $no = 1;
        if ($delayed->num_rows() < 1) {
            echo "All timesheets seems to be submitted on time or already set as Delayed";
            exit;
        }
        foreach ($delayed->result() as $d) {

            $period = $d->period == 1 ? "1st Half" : "2nd Half";
            // select join user,cost centre table get cost centre name
            $uc = $this->db->query("select u.display_name,c.cost_centre from intg_users u,intg_cost_centre c where u.cost_centre_id = c.id and u.id = " . $d->uid . "")->row();
            $message .= PHP_EOL . ' <tr>
                                    <td>' . $no . '</td>
                                    <td>' . $uc->display_name . '</td>
                                    <td>' . $uc->cost_centre . '</td>
                                    <td>' . $period . '</td>
                              </tr>
									';
            $no++;
            $users[] = $d->uid;
        }

        $message .= PHP_EOL . '</table>'; //end table 

        $notApprovedYet = $this->db->query("select * from intg_timesheet where status = 'Submitted' and mth = '" . date('n', strtotime('last month')) . "' and yr = '" . $year . "' AND final_status = 'No' AND uid NOT IN (SELECT id FROM intg_users WHERE banned='1') ");

        $message .= PHP_EOL . '<br><br><br><p><b>Please find the list of users that submitted their timesheet but not yet approved for the period of ' . date('F', strtotime('last month')) . ', ' . $year . '</b></p>
								<table width="80%" border="1">
									  <tr>
									  <th>No</th>
									  <th>Timesheet Owner</th>
									  <th>Cost Center</th>
									  <th>Period(1st or 2nd half)</th>
									</tr>
									';

        $no2 = 1;
        foreach ($notApprovedYet->result() as $nay) {
            $period = $nay->period == 1 ? "1st Half" : "2nd Half";
            // select join user,cost centre table get cost centre name
            $uc = $this->db->query("select u.display_name,c.cost_centre from intg_users u,intg_cost_centre c where u.cost_centre_id = c.id and u.id = " . $nay->uid . "")->row();
            $message .= PHP_EOL . ' <tr>
										<td>' . $no2 . '</td>
										<td>' . $uc->display_name . '</td>
										<td>' . $uc->cost_centre . '</td>
										<td>' . $period . '</td>
									  </tr>
									';
            $no2++;
        }

        $message .= PHP_EOL . '</table>';

        // select work flow hierarchy approvers and send notification
        $wf = $this->db->query("select distinct rolename,work_flow_approvers  from intg_work_flow where work_flow_module_id = 41 and work_flow_doc_generator IN (" . implode(",", $users) . ")")->result();
        $b = "";
        $sent = array();
        $approvers = array();
        echo "Approvers List: <br>";

        //Prepare the approvers list
        foreach ($wf as $w) {
            //contain comma or not, if got, do looping
            if (strpos($w->work_flow_approvers, ',') !== false) {
                foreach (explode(",", $w->work_flow_approvers) as $key => $val) {

                    $emailid = $this->db->query("select email from intg_users where id = " . $val . "")->row()->email;

                    $approvers[] = array(
                        'id' => $val,
                        'role_name' => $w->rolename,
                        'email' => $emailid
                    );
                }
            } else {


                $approvers[] = array(
                    'id' => $w->work_flow_approvers,
                    'role_name' => $w->rolename,
                    'email' => $this->db->query("select email from intg_users where id = " . $w->work_flow_approvers . "")->row()->email
                );
            }
        }

        //Send email to not in heirarchy approver. CSO CEO FINANCE, and Allan 
        $personWillReceive[] = array(
            'id' => '0',
            'email' => 'allan.lee@genting.com',
            'role_name' => 'Allan'
        );

        $merged = array_merge($approvers, $personWillReceive);
        $to_email = array();
        foreach ($merged as $key => $val) {
            if (!in_array($merged[$key]['id'], $to_email)) {
                $to_email[$merged[$key]['id']] = $merged[$key];
            }
        }
//        var_dump($to_email);
//        echo '<pre>';
//        print_r($to_email);
//        echo '</pre>';
//        die();
        //send email to all in array
        foreach ($to_email as $val) {
            echo '<br> SENT TO : ' . $val['email'] . ': ROLE NAME : ' . $val['role_name'];
            $this->sendmail($val['id'], $val['email'], $val['role_name'], $message);
            $text .= PHP_EOL . ' SENT TO : ' . $val['email'] . ': ROLE NAME : ' . $val['role_name'];
        }

        fwrite($handle, $text . "\n\n");              // Writing it in the log file

        fwrite($handle, print_r($approvers, TRUE));
        fwrite($handle, print_r($personWillReceive, TRUE));
        fwrite($handle, print_r($to_email, TRUE));
        fwrite($handle, $message);

        fclose($handle);      // Close the file


        echo $message;
    }

    public function sendmail($userid, $emailid, $rolename, $message) {

        $this->load->library('emailer/emailer');

        $this->emailer->AddAddress($emailid, $rolename);

        $this->emailer->Subject = 'Timesheets pending for submission';
        $this->emailer->Body = $message;

        $this->emailer->mail();

        $this->load->model('notifications/notifications_model', null, true);
        $notification_data = array();
        $this->notifications_model->insert(array(
            'user_id' => $userid, // recipient user id
            'content' => ' Timesheet is pending submission',
            'title' => ' Timesheet from previous month pending for submission',
            'link' => site_url('admin/projectmgmt/tasks/timesheets/0/3'),
            'class' => 'fa-briefcase',
            'module' => 'Timesheet',
            'type' => 'info'
        ));
    }

    public function initiate_new_cso($uid) {

        //Select project where new cso is not inside co worker
        $prjToAdd = $this->db->query("SELECT * FROM intg_projects WHERE NOT  FIND_IN_SET(" . $uid . ",coworker)")->result();
        $cntSuccess = 0;
        $cntFailed = 0;
        foreach ($prjToAdd as $val) {
            $update = $this->db->query("UPDATE intg_projects SET coworker = CONCAT(coworker, '," . $uid . "') WHERE id = " . $val->id);
            echo "UPDATE intg_projects SET coworker = CONCAT(coworker, '," . $uid . "') WHERE id = " . $val->id . "<br>";

            if ($update) {
                echo "Update Success";
                $cntSuccess++;
            } else {
                echo "Update Failed";
                $cntFailed++;
            }
        }
//        1)Tambah Timesheet details untuk semua project list
//        2) check untuk non chargable project kot2 system masukkan holiday.
        $taskpool = $this->db->query("select "
                        . "imt.task_pool_id,imt.milestone_id,im.start_date,im.end_date,DATEDIFF(end_date,start_date) as days_diff  "
                        . "from intg_milestones im,intg_milestones_tasks imt,intg_milestones_users imu "
                        . "where im.project_id = " . $projectid . " and im.milestone_id = imu.milestones_id and im.milestone_id = imt.milestone_id and imu.userlevel_id = " . get_from_user_table('user_level_id', 'id', $uid))->result();


        foreach ($taskpool as $t) {

            $a = strtotime($t->start_date);
            $b = strtotime($t->end_date);
            $a1 = date("Y-m", strtotime($t->start_date));
            $b1 = date("Y-m", strtotime($t->end_date));
            $c = strtotime($a1);
            $d = strtotime($b1);

            $day = date("d", strtotime($t->start_date));
            $period = "1";
            if ($day < 16) {
                $period = "1";
                //$daydiff = 16;
            } else {
                $period = "2";
                //$daydiff = date("t",strtotime($t->start_date)) - 15;
            }

            $period_arr = array("1", "2");
            $c = strtotime(date("Y-m-d", $a));

            foreach ($period_arr as $pa) {
                if ($pa == "1") {
                    $daydiff = 16;
                }
                if ($pa == "2") {
                    $daydiff = date("t", $c) + 1;
                }


                foreach ($this->config->item('csouid') as $uid) {

                    $r = $this->db->query("select * from intg_timesheet where uid = " . $uid . " and mth = '" . date("n", $c) . "' and yr ='" . date("Y", $c) . "'  and period = '" . $pa . "' order by id desc");
                    $rec = $r->row();

                    $id = NULL;
                    if ($r->num_rows() == 0) {  // submitted means sent for approval via workflow
                        $datats = array();

                        $datats['uid'] = $uid;
                        $datats['mth'] = date("n", $c);
                        $datats['period'] = $pa;
                        $datats['yr'] = date("Y", $c);
                        $datats['status'] = 'Created';
                        $datats['initiator'] = $uid;
                        $datats['final_approvers'] = "none";

                        $id = $this->timesheet_model->insert($datats); // insert if existing first row is submitted or if there is no row
                    }

                    $tid = $id == NULL ? $rec->id : $id;
                    $i = $pa == "1" ? date("d", $c) : 16;
                    $month = date("m", $c);
                    $year = date("Y", $c);
                    $sd = $pa == "1" ? date("Y-m-d", $c) : $year . "-" . $month . "-16";

                    $checkFTE = get_dailyfte_bydate(get_from_user_table('user_level_id', 'id', $uid), $sd);


                    $q = $this->db->query("SELECT * FROM intg_timesheet_details where tid='" . $tid . "' AND pid= '" . $projectid . "' AND mid='" . $t->milestone_id . "' AND ptskid='" . $t->task_pool_id . "' AND tsd_date = '" . $sd . "' ")->num_rows();
                    while ($i < $daydiff) {
                        if ($q < 1) {
                            //kena dptkan fte rate dan masukkan dlm table
                            $this->db->query("INSERT DELAYED INTO intg_timesheet_details (tid,pid,mid,ptskid,tsd_date,tsd_hours,comments,user_level_details_id,user_level_details_fte_rate) VALUES ('" . $tid . "','" . $projectid . "','" . $t->milestone_id . "','" . $t->task_pool_id . "','" . $sd . "','0',NULL, '" . $checkFTE['user_level_details_id'] . "', '" . $checkFTE['daily_fte'] . "' )");
                        }
                        $sd = date("Y-m-d", strtotime("+1 day", strtotime($sd)));
                        $i++;
                    }
                }
            }

            $c = strtotime(date("Y-m-d", $a) . " +1 month");
            $a = $c;
        }

        echo $cntSuccess . "<br>";
        echo $cntFailed . "<br>";

        die();

//        
    }

}
