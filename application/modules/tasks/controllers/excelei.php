<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * projects controller
*/

class excelei extends Admin_Controller
{

	//--------------------------------------------------------------------

public $dota = array();	
	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		Assets::add_css('js/datepicker/datepicker.css');
		Assets::add_js("js/datepicker/bootstrap-datepicker.js");
		Assets::add_css('js/datetimepicker/bootstrap-datetimepicker.min.css');
		Assets::add_js("js/datetimepicker/bootstrap-datetimepicker.min.js");
		Assets::add_js("js/datetimepicker/moment.min.js");
                Assets::add_js("js/parsley/parsley.min.js");
                $this->lang->load('tasks');
                
		Template::set_block('sub_nav', 'projectmgmt/_sub_nav');
		
	}
	

	
	function saveexcel() {		
		
		$p = array();
		$this->load->model('timesheet/timesheet_model',null,true);
		$this->load->model('timesheet_details/timesheet_details_model',null,true);		
		

if (($handle = fopen("./uploads/user".$this->auth->user_id()."timesheet1.csv", "r")) !== FALSE) {
	$r= 1;
	$s = 1;
	$handle = fopen("./uploads/user".$this->auth->user_id()."timesheet1.csv", "r");
	
	 while (($cdata = fgetcsv($handle, 0, ",")) !== FALSE) {
			
		
		 if ( $s==2 ) {
			 for ($cc=0; $cc < count($cdata); $cc++) {
                             $cdota[] = $cdata[$cc]; 
                         }	
                }else {
            
                    if ( $s > 3 ) {
                        foreach ( $cdota as $cd=>$ckey ) {


                            if ($cd > 7) {
                                    if ($cdota[$cd] != "C" . $cd) {

                                        if ($cdata[$cd] != "") {
                                            if (isset($p[$ckey])) {
                                                $p[$ckey]+= $cdata[$cd];
                                            } else {
                                                $p[$ckey] = $cdata[$cd];
                                            }
                                        } 
                                    } 

                                }
                                
                        }  
                    } 
	}
	
	$s++;
        
        } 
                        
        foreach ( $p as $pp=>$pk ) { 
            if ( $pk < 8.5 ) {  
            Template::set_message( "The value for ".date("M jS D",strtotime($pp))." is less than 8.5", 'error'); 
            
            } 
            
        } 
	
        $handle2 = fopen("./uploads/user".$this->auth->user_id()."timesheet1.csv", "r");
	
	 while (($data = fgetcsv($handle2, 0, ",")) !== FALSE) {
			
		if ( $r == 1 ) {			
		$rs = $this->db->query("select * from intg_timesheet where uid = '".$data[3]."' and mth = '".$data[5]."' and yr ='".$data[7]."' and period ='".$data[9]."' order by id desc");			
		$s = $rs->row();		
		$id= NULL;		
		if (  $rs->num_rows() == 0  || $s->status == "Submitted")  {		
		$datats = array();
		$datats['uid']       = $data[3];
		$datats['mth']       = $data[5];
		$datats['period']    = $data[9];
		$datats['yr']        = $data[7];
		$datats['status']    = 'Created';	
		
		$tsid = $this->timesheet_model->insert($datats);	
			
		} else if ( $s->status == "Created" ) { 
		
		$this->db->query("delete from intg_timesheet where id = '".$s->id."'");
		$this->db->query("delete from intg_timesheet_details where tid = '".$s->id."'");
			
		$datats = array();
		$datats['uid']       = $data[3];
		$datats['mth']       = $data[5];
		$datats['period']    = $data[9];
		$datats['yr']        = $data[7];
		$datats['status']    = 'Created';			
			
			if($tsid = $this->timesheet_model->insert($datats)){
                            $return = true;
                        }
			
		}

}
		 if ( $r==2 ) {			 
			 for ($c=0; $c < count($data); $c++) {$dota[] = $data[$c]; }	
	
	} 	
	else {
	if ( $r > 3 ) {			
	
	if ( $tsid != NULL ) { 	
	
	foreach ( $dota as $d=>$key ) {		
		
		if ( $d > 7  ) { 
			if (  $dota[$d] != "C".$d  ) {
			
			if (  $data[$d] !="" ) { 
			
			$date = str_replace('/', '-', $dota[$d]);
		$id = $this->timesheet_details_model->insert( array( "tid"=>$tsid,"pid"=>$data[4],"mid"=>$data[5],"ptskid"=>$data[6],"tsd_date"=>date("Y-m-d",strtotime($date)),"tsd_hours"=>$data[$d]) ); 
		
			} } 
			
			if (  $key == "C".$d ) { 
			
			if (  $data[$d] !="" ) {
			
                            if($this->timesheet_details_model->update($id,array("comments"=>$data[$d]))){
                                $return = true;
                            }
			
			}
			
			}
			
		}
		
	}  
	} 
	
	} 
	
	}
	$r++;}
        
        
	
	
    fclose($handle);
    fclose($handle2);
}else{
    $return = false;
}

return $return;

	}
	

	
	public function export_import_excel()
	{
		if ( isset($_POST['insertcsv']) ) {
		
                    if($this->saveexcel()){
                        Template::set_message('Timesheet successfully saved', 'success');
                    }else{
                        Template::set_message(lang('tasks_create_error'), 'error');
                    }
			
		}
		$this->load->model('timesheet_details/timesheet_details_model',null,true);
$config = array(
			'upload_path' => './uploads/', 
			'allowed_types' => 'xlsx',
			'max_size' => '',
			'max_width' => '',
			'max_heigth' => '',
		);

$this->load->library('upload');
	if ( $_FILES['files']['error'][0] !=4 ) {
	
$this->upload->do_multi_upload('files', $config);
		
		// upload errors
		$this->dota['upload_errors'] = $this->upload->display_multi_errors();
		
		if ( $this->upload->display_multi_errors()) { 
		
		echo $_FILES['files']['error'];
		Template::set_message("Upload Error".$this->upload->display_multi_errors(), 'error'); 
		
		
	//return false; 
	} 	
		
foreach ($this->upload->multi_data() as $item => $value):
//$filename[] = $value['file_name'].":".$value['orig_name'];

 //$this->saveexcel($value['orig_name']);
 /////////////////
 
 //Various excel formats supported by PHPExcel library
$excel_readers = array(
    'Excel5' , 
    'Excel2003XML' , 
    'Excel2007'
);
 
$this->load->library('excel');

$reader = PHPExcel_IOFactory::createReader('Excel2007');
$reader->setReadDataOnly(true);
$path = './uploads/'.$value['orig_name'];
$excel = $reader->load($path);
 
$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
$writer->save('./uploads/user'.$this->auth->user_id().'timesheet1.csv');

unlink($path);

 ////////////////
    Template::set_message('File saved to csv format', 'success');
 endforeach; 
	

	}
	
	
	if ( $this->upload->display_multi_errors()) {				
				Template::set_message("Upload Error".$this->upload->display_multi_errors(), 'error'); 
				 }  
		
		Template::render();
		
	}
	
	
		public function export_timesheet()
	{		
                $this->load->library('upload');		
		Template::render();		
	}
	
	
	public function index()
	{
		//print_r($_POST); exit;
	 
		$name = $this->input->post('username');
		$dm = explode("/",$this->input->post('datemth'));

		
		$this->load->model('milestone/milestone_model', null, true);
		$this->load->model('milestone_users/milestone_users_model', null, true);
		$this->load->model('milestone_tasks/milestone_tasks_model', null, true);
		
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
//name the worksheet
$this->excel->getActiveSheet()->setTitle('Time Sheet');
$this->excel->getActiveSheet()->setCellValue('A1', 'User:');
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B1', $name);
$this->excel->getActiveSheet()->setCellValue('C1', 'ID:');
$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('D1', $this->input->post('users'));
$this->excel->getActiveSheet()->setCellValue('E1', 'Month:');
$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('F1', $dm[0]);
$this->excel->getActiveSheet()->setCellValue('G1', 'Year:');
$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('H1', $dm[1]);
$this->excel->getActiveSheet()->setCellValue('I1', 'Period:');
$this->excel->getActiveSheet()->setCellValue('J1', $this->input->post('period'));

$this->excel->getActiveSheet()->protectCells('I4:AL100', 'PHP');
$this->excel->getActiveSheet()->getStyle('I4:AL100')->getProtection()
->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
$this->excel->getActiveSheet()->getProtection()->setPassword('123123123');
$this->excel->getActiveSheet()
    ->getProtection()->setSheet(true);
/*$this->excel->getActiveSheet()->getStyle('I4:AL100')
    ->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);*/

$this->excel->getActiveSheet()->setCellValue('A2', 'No');
$this->excel->getActiveSheet()->setCellValue('B2', 'Project');
$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('C2', 'Milestone');
$this->excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('D2', 'Task');
$this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('E2', 'pid');
$this->excel->getActiveSheet()->setCellValue('F2', 'mid');
$this->excel->getActiveSheet()->setCellValue('G2', 'ptskid');
$this->excel->getActiveSheet()->getColumnDimension('E')->setVisible(false);
$this->excel->getActiveSheet()->getColumnDimension('F')->setVisible(false);
$this->excel->getActiveSheet()->getColumnDimension('G')->setVisible(false);

$this->excel->getActiveSheet()->getRowDimension('2')->setVisible(false);





$this->excel->getActiveSheet()->setCellValue('A3', 'No');
$this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B3', 'Project');
$this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('C3', 'Milestone');
$this->excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('D3', 'Task');
$this->excel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);

$current_week = 0; $monthly = 0; $monthly_chunk_th = 1;


							$days_time_stamp = array();
							$total_monthly_chunk = 0;
							
								$current_month = $dm[0];
								//echo "CURRENT MONTH:".$current_month;
								
								$current_year = $dm[1];
								$days_number = cal_days_in_month(CAL_GREGORIAN, $dm[0], $dm[1]);
								
								
								if ( $this->input->post('period') == 1 )
								{
								for($i = 1; $i <= 15; $i++)
								{
									$days_time_stamp[] = date("M jS D",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
									$nicedate[] = date("Y-m-d",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
								}
								}
								if ( $this->input->post('period') == 2 )
									{
									for($i = 16; $i <= $days_number; $i++)
									{
										$days_time_stamp[] = date("M jS D",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
										$nicedate[] = date("Y-m-d",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
									}
								}
								
							
								
$alphabet = "I";
$dateno = 8;
$colno = 3;
foreach ( $days_time_stamp as $val => $spitit ) {

$this->excel->getActiveSheet()->setCellValue($alphabet."2", $nicedate[$val]);
$this->excel->getActiveSheet()->setCellValue($alphabet."3", $spitit);
$this->excel->getActiveSheet()->getStyle($alphabet."3")->getFont()->setBold(true);


$alphabet ++;
$dateno ++;


$this->excel->getActiveSheet()->setCellValue($alphabet."2", "C".$dateno);

$this->excel->getActiveSheet()->setCellValue($alphabet."3", "Comments");
$this->excel->getActiveSheet()->getStyle($alphabet."3")->getFont()->setBold(true);



$alphabet ++;
$dateno ++;

}


//$timesheet = $this->db->query("SELECT *,itd.id as itid FROM intg_timesheet it, intg_timesheet_details itd,intg_milestones m where it.id = itd.tid and m.completed_status is null and itd.mid=m.milestone_id and  itd.pid IN ( ".implode(",",$_POST['projects']).") group by tid,pid,mid,ptskid ORDER BY `itid` desc")->result();

$timesheet = $this->db->query("SELECT * FROM intg_milestones_tasks where project_id IN ( ".implode(",",$_POST['projects']).")  ORDER BY task_id desc")->result();



//echo $this->db->last_query(); exit;

 $no=4;
 
$alphabet2 = "I";
$colno = 4;
 foreach($timesheet as $record) 
 {
$this->excel->getActiveSheet()->setCellValue("A".$no, $no-3);	 
$this->excel->getActiveSheet()->setCellValue("B".$no, $this->db->query("select id,project_name from intg_projects where id = ".$record->project_id."")->row()->project_name);
$this->excel->getActiveSheet()->setCellValue("C".$no, $this->db->query("select milestone_id,milestone_name from intg_milestones where milestone_id = ".$record->milestone_id."")->row()->milestone_name);						
$this->excel->getActiveSheet()->setCellValue("D".$no, $this->db->query("select id,task_name from intg_task_pool where id = ".$record->task_pool_id."")->row()->task_name); 
$this->excel->getActiveSheet()->setCellValue("E".$no,$record->project_id);	 
$this->excel->getActiveSheet()->setCellValue("F".$no,$record->milestone_id);	 
$this->excel->getActiveSheet()->setCellValue("G".$no,$record->task_pool_id);


foreach ( $days_time_stamp as $val2 => $spitit2 ) {

//$this->excel->getActiveSheet()->setCellValue($alphabet2.$colno, $alphabet2.$colno);	
//$objValidation = $this->excel->getActiveSheet()->getStyle($alphabet2.$colno)->getNumberFormat()->setFormatCode('0.00');

$objValidation = $this->excel->getActiveSheet()->getCell($alphabet2.$colno)->getDataValidation();

$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL  );
$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
$objValidation->setAllowBlank(true);
$objValidation->setShowInputMessage(true);
$objValidation->setShowErrorMessage(true);
$objValidation->setErrorTitle('Input error');
$objValidation->setError('Only Decimals between 0.25 & 24!');
$objValidation->setPromptTitle('Allowed input');
$objValidation->setPrompt('Only numbers between 0.25 and 24 are allowed.');
$objValidation->setFormula1(0.25);
$objValidation->setFormula2(24);
$this->excel->getActiveSheet()->getCell($alphabet2.$colno)->setDataValidation($objValidation);
	
$alphabet2++;
$alphabet2++;
}
$alphabet2 = "I";
$colno++;


 $no++;
 }
 $period = "";
 if($this->input->post('period') != '' || $this->input->post('period') != null){
     $period = $this->input->post('period')."H";
 }
 if($this->input->post('username') != '' || $this->input->post('username') != null){
     $username = $this->input->post('username')."_";
 }
 
 

$name = $username."timesheet_".$dm[1].$dm[0]."_".$period.'.XLSX';
$fname = preg_replace('/\s+/', '', $name);
$filename=$fname; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
 ob_clean();
$objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');

	
		
		//Template::render();
	}
	
	
public function timesheet_export()
	{
	 
		$name = $this->input->post('username');
		$dm = explode("/",$this->input->post('datemth'));
		
		$this->load->model('milestone/milestone_model', null, true);
		$this->load->model('milestone_users/milestone_users_model', null, true);
		$this->load->model('milestone_tasks/milestone_tasks_model', null, true);		
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		
                                //name the worksheet
                                $this->excel->getActiveSheet()->setTitle('Time Sheet');
                                $this->excel->getActiveSheet()->setCellValue('A1', 'User:');
                                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('B1', $name);
                                $this->excel->getActiveSheet()->setCellValue('C1', 'ID:');
                                $this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('D1', $this->input->post('users'));
                                $this->excel->getActiveSheet()->setCellValue('E1', 'Month:');
                                $this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('F1', $dm[0]);
                                $this->excel->getActiveSheet()->setCellValue('G1', 'Year:');
                                $this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('H1', $dm[1]);

                                $this->excel->getActiveSheet()->setCellValue('A2', 'No');
                                $this->excel->getActiveSheet()->setCellValue('B2', 'Project');
                                $this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('C2', 'Milestone');
                                $this->excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('D2', 'Task');
                                $this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('E2', 'pid');
                                $this->excel->getActiveSheet()->setCellValue('F2', 'mid');
                                $this->excel->getActiveSheet()->setCellValue('G2', 'ptskid');
                                $this->excel->getActiveSheet()->getColumnDimension('E')->setVisible(false);
                                $this->excel->getActiveSheet()->getColumnDimension('F')->setVisible(false);
                                $this->excel->getActiveSheet()->getColumnDimension('G')->setVisible(false);

                                $this->excel->getActiveSheet()->getRowDimension('2')->setVisible(false);

                                $this->excel->getActiveSheet()->setCellValue('A3', 'No');
                                $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('B3', 'Project');
                                $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('C3', 'Milestone');
                                $this->excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
                                $this->excel->getActiveSheet()->setCellValue('D3', 'Task');
                                $this->excel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);

                                $current_week = 0; $monthly = 0; $monthly_chunk_th = 1; 


                                $days_time_stamp = array();
                                $total_monthly_chunk = 0;

                                        $current_month = $dm[0];
                                        //echo "CURRENT MONTH:".$current_month;

                                        $current_year = $dm[1];
                                        $days_number = cal_days_in_month(CAL_GREGORIAN, $dm[0], $dm[1]);								

                                                for($i = 1; $i <= $days_number; $i++)
                                                {
                                                        $days_time_stamp[] = date("M jS D",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
                                                        $nicedate[] = date("Y-m-d",strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'));
                                                }
								
                $alphabet = "I";
                $dateno = 8;
                $colno = 3;
                foreach ( $days_time_stamp as $val => $spitit ) {

                $this->excel->getActiveSheet()->setCellValue($alphabet."2", $nicedate[$val]);
                $this->excel->getActiveSheet()->setCellValue($alphabet."3", $spitit);
                $this->excel->getActiveSheet()->getStyle($alphabet."3")->getFont()->setBold(true);


                $alphabet ++;
                $dateno ++;


                $this->excel->getActiveSheet()->setCellValue($alphabet."2", "C".$dateno);

                $this->excel->getActiveSheet()->setCellValue($alphabet."3", "Comments");
                $this->excel->getActiveSheet()->getStyle($alphabet."3")->getFont()->setBold(true);



                $alphabet ++;
                $dateno ++;

                }


            //$timesheet = $this->db->query("SELECT *,itd.id as itid FROM intg_timesheet it, intg_timesheet_details itd,intg_milestones m where it.id = itd.tid and m.completed_status is null and itd.mid=m.milestone_id and  itd.pid IN ( ".implode(",",$_POST['projects']).") group by tid,pid,mid,ptskid ORDER BY `itid` desc")->result();

            $timesheet = $this->db->query("SELECT * FROM intg_milestones_tasks   ORDER BY task_id desc")->result();



//echo $this->db->last_query(); exit;

 $no=4;
 
$alphabet2 = "I";
$colno = 4;
 foreach($timesheet as $record) 
 {
$this->excel->getActiveSheet()->setCellValue("A".$no, $no-3);	 
$this->excel->getActiveSheet()->setCellValue("B".$no, $this->db->query("select id,project_name from intg_projects where id = ".$record->project_id."")->row()->project_name);
$this->excel->getActiveSheet()->setCellValue("C".$no, $this->db->query("select milestone_id,milestone_name from intg_milestones where milestone_id = ".$record->milestone_id."")->row()->milestone_name);						
$this->excel->getActiveSheet()->setCellValue("D".$no, $this->db->query("select id,task_name from intg_task_pool where id = ".$record->task_pool_id."")->row()->task_name); 
$this->excel->getActiveSheet()->setCellValue("E".$no,$record->project_id);	 
$this->excel->getActiveSheet()->setCellValue("F".$no,$record->milestone_id);	 
$this->excel->getActiveSheet()->setCellValue("G".$no,$record->task_pool_id);


$tmhours = $this->db->query("select itd.tsd_date,itd.tsd_hours,itd.comments from intg_timesheet it,intg_timesheet_details itd where it.id = itd.tid and itd.pid = '".$record->project_id."' and itd.mid='".$record->milestone_id."' and itd.ptskid='".$record->task_pool_id."' and it.uid = '".$this->input->post('users')."'")->result();

 
foreach ( $tmhours  as $t ) { $tsddate[] =$t->tsd_date ; $tsdhours[$t->tsd_date] = $t->tsd_hours; $comments[$t->tsd_date]=$t->comments; }




foreach ( $days_time_stamp as $val2 => $spitit2 ) {
	
	$d1 = date("Y-m-d",strtotime($spitit2));	
	$this->excel->getActiveSheet()->setCellValue($alphabet2.$colno, in_array($d1,$tsddate ) ? $tsdhours[$d1] : "" );
	
/*$objValidation = $this->excel->getActiveSheet()->getCell($alphabet2.$colno)->getDataValidation();

$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DECIMAL  );
$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
$objValidation->setAllowBlank(true);
$objValidation->setShowInputMessage(true);
$objValidation->setShowErrorMessage(true);
$objValidation->setErrorTitle('Input error');
$objValidation->setError('Only Decimals between 0.25 & 24!');
$objValidation->setPromptTitle('Allowed input');
$objValidation->setPrompt('Only numbers between 0.25 and 24 are allowed.');
$objValidation->setFormula1(0.25);
$objValidation->setFormula2(24);
$this->excel->getActiveSheet()->getCell($alphabet2.$colno)->setDataValidation($objValidation);*/
	
	$alphabet2++;
	$this->excel->getActiveSheet()->setCellValue($alphabet2.$colno, in_array($d1,$tsddate ) ? $comments[$d1] : "" );



$alphabet2++;
}


$alphabet2 = "I";
$colno++;
$no++;

 $tsddate =array(); $tsdhours = array(); $comments=array();
 }

$name = $name."timesheet_".$dm[1].$dm[0].'.XLSX';
$fname = preg_replace('/\s+/', '', $name);
$filename=$fname; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
 ob_clean();
$objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');
		//Template::render();
	}

}