<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class projectmgmt extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->auth->restrict('Tasks.Projectmgmt.View');
        /* Assets::add_css('js/datepicker/datepicker.css');
          Assets::add_js("js/datepicker/bootstrap-datepicker.js");
          Assets::add_css('js/datetimepicker/bootstrap-datetimepicker.min.css');
          Assets::add_js("js/datetimepicker/bootstrap-datetimepicker.min.js"); */
        Assets::add_css(Template::theme_url('js/datepicker2/bootstrap-datepicker.css'));
        Assets::add_js(Template::theme_url("js/datepicker2/bootstrap-datepicker.js"));
        Assets::add_js(Template::theme_url("js/jquery.sortElements.js"));
        Assets::add_js(Template::theme_url("js/stupidtable.js"));
        Assets::add_js("js/datetimepicker/moment.min.js");
        $this->load->model('tasks_model', null, true);
        $this->lang->load('tasks');
        Template::set_block('sub_nav', 'projectmgmt/_sub_nav');
        Assets::add_js("js/parsley/parsley.min.js");
        Assets::add_module_js('tasks', 'tasks.js');
    }

    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {

        $period = 3;
        $today = date('d');

        if ($today < 16) {
            $period = 3;
        } else {
            $period = 4;
        }

        $uid = $this->auth->user_id();
        if (in_array($uid, $this->config->item('csouid'))) { //settle vo
            redirect(SITE_AREA . '/projectmgmt/tasks/timesheets_cso/0/' . $period);
        } else {
            redirect(SITE_AREA . '/projectmgmt/tasks/timesheets/0/' . $period);
        }
    }

    public function populateoptions() {

        $proj = $this->db->query("select project_start_date,status_nc FROM intg_projects WHERE id = '" . $_GET['projid'] . "'")->row();

        if ($proj->status_nc == "yes") {

            if ($_GET['type'] == "milestone") {
                $result = $this->db->query("select  milestone_name,milestone_id  from intg_milestones where project_id = '" . $_GET['projid'] . "' ")->result();
                foreach ($result as $r) {
                    $res = $this->db->query("select milestone_name,milestone_id from intg_milestones where parent_Rid = '" . $r->milestone_id . "' order by milestone_id desc");
                    if ($res->num_rows() < 1) {
                        $results[] = array("milestone_name" => $r->milestone_name, "milestone_id" => $r->milestone_id);
                    } else {
                        $results[] = array("milestone_name" => $res->row()->milestone_name, "milestone_id" => $res->row()->milestone_id);
                    }
                }
            }

            if ($_GET['type'] == "task") {
                $this->load->model('task_pool/task_pool_model', null, true);
                $this->load->model('milestone_tasks/milestone_tasks_model', null, true);
                $m_start_date = $this->db->query("select start_date FROM intg_milestones WHERE milestone_id = '" . $_GET['milestone_id'] . "'")->row();
                $taskdata = $this->db->query("select distinct tp.task_name,m.task_pool_id from intg_milestones_tasks m,intg_task_pool tp  where tp.id = m.task_pool_id and milestone_id = '" . $_GET['milestone_id'] . "'")->result();

                if ($taskdata) {
                    $results = $taskdata;
                } else {
                    $results = false; //no tasks
                }
            }
        } else {

            if (strtotime($proj->project_start_date) > time()) {
                $results = false; //project not started
            } else {
                if ($_GET['type'] == "milestone") {
                    $startdate = format_date($this->input->get('tsdate'), "Y-m-d");
                    $result = $this->db->query("select  milestone_name,milestone_id  from intg_milestones where project_id = '" . $_GET['projid'] . "' and parent_Rid = 0 and start_date <= '" . $startdate . "'")->result();
//                    $results['sql'] = $this->db->last_query();
                    foreach ($result as $r) {

                        $res = $this->db->query("select milestone_name,milestone_id from intg_milestones where parent_Rid = '" . $r->milestone_id . "' and start_date <='" . $startdate . "' order by milestone_id desc");

                        if ($res->num_rows() < 1) {
                            $results[] = array("milestone_name" => $r->milestone_name, "milestone_id" => $r->milestone_id);
                        } else {
                            $results[] = array("milestone_name" => $res->row()->milestone_name, "milestone_id" => $res->row()->milestone_id);
                        }
                    }
                    // not working in current php 5.3 version
                    /* $results = array_map(function($distinctrow) {

                      $a = $this->db->query("select milestone_name,milestone_id from intg_milestones where parent_Rid = '" . $distinctrow->milestone_id . "' order by milestone_id desc");
                      echo $this->db->last_query();

                      if ( $a->num_rows() < 1 ) {
                      return array ("milestone_name" =>$distinctrow->milestone_name,"milestone_id" =>$distinctrow->milestone_id);
                      } else {
                      return  array("milestone_name" =>$a->row()->milestone_name,"milestone_id" =>$a->row()->milestone_id);
                      }
                      }, $result); */
                }
                if ($_GET['type'] == "task") {
                    $this->load->model('task_pool/task_pool_model', null, true);
                    $this->load->model('milestone_tasks/milestone_tasks_model', null, true);
                    $m_start_date = $this->db->query("select start_date FROM intg_milestones WHERE milestone_id = '" . $_GET['milestone_id'] . "'")->row();

                    if (strtotime($m_start_date->start_date) > time()) {
                        $results = false; //milestone not started
                    } else {
                        $results = $this->db->query("select distinct tp.task_name,m.task_pool_id from intg_milestones_tasks m,intg_task_pool tp  where tp.id = m.task_pool_id and milestone_id = '" . $_GET['milestone_id'] . "'")->result();
                    }
                }
            }
        }

        echo json_encode($results);
    }

    function quickapprove() {
        Template::render();
    }

    function updateApprovalstatus() {
        Template::render();
    }

    public function approval($current_week = 0, $monthly = 0, $monthly_chunk_th = 1) {
        $this->load->model('timesheet/timesheet_model', null, true);
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('timesheet/timesheet_model', null, true);

        $user_id = $this->auth->user_id();
        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');
            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->timesheet_model->delete($pid);
                }
                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('tasks_delete_success'), 'success');
                } else {
                    Template::set_message(lang('tasks_delete_failure') . $this->tasks_model->error, 'error');
                }
            }
        }
        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('sapfield', $this->input->post('select_field'));
                $this->session->set_userdata('sapfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('sapfname', $this->input->post('field_name'));
                break;
            case 'Reset':
                $this->session->unset_userdata('sapfield');
                $this->session->unset_userdata('sapfvalue');
                $this->session->unset_userdata('sapfname');
                break;
        }
        if ($this->session->userdata('sapfield') != '') {
            $field = $this->session->userdata('sapfield');
        } else {
            $field = 'id';
        }
        if ($this->session->userdata('sapfield') == 'All Fields') {
            $total = $this->timesheet_model
                    ->join("intg_users", "intg_users.id = intg_timesheet.uid")
                    ->where('intg_timesheet.parent_Rid', '0')
                    ->where('intg_timesheet.deleted', '0')
                    ->where('intg_timesheet.status', 'Submitted')->where("(
   `initiator` = " . $this->auth->user_id() . " OR   
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',tsother_approvers)    
   )")
                    // ->where('intg_projects.status','final')
                    //->where('intg_projects.company_id',$this->auth->company_id())
                    ->where("(
   intg_users.display_name like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `mth` like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `yr` like ('%" . $this->session->userdata('sapfvalue') . "%')
   )", NULL, FALSE)
                    ->count_all();
        } else {
            $total = $this->timesheet_model
                    ->where('parent_Rid', '0')
                    ->where('deleted', '0')
                    ->where('status', 'Submitted')->where("(
   `initiator` = " . $this->auth->user_id() . " OR   
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',tsother_approvers)    
   )")
                    // ->where('intg_projects.status','final')
                    // ->where('intg_projects.company_id',$this->auth->company_id())
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->count_all();
        }
        //$records = $this->quote_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 5;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;
        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }
        if ($this->session->userdata('sapfield') == 'All Fields') {
            $records = $this->timesheet_model
                    ->join("intg_users", "intg_users.id = intg_timesheet.uid")
                    ->where('intg_timesheet.parent_Rid', '0')
                    ->where('intg_timesheet.deleted', '0')
                    ->where('intg_timesheet.status', 'Submitted')->where("(
   `initiator` = " . $this->auth->user_id() . " OR  
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',tsother_approvers)    
   )")
                    ->where("(
   intg_users.display_name like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `mth` like ('%" . $this->session->userdata('sapfvalue') . "%') OR
   `yr` like ('%" . $this->session->userdata('sapfvalue') . "%') 
   )", NULL, FALSE)
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'submission_date' => 'desc',
                );
            } else {
                $ord1 = array(
                    'version_no' => 'desc',
                    'submission_date' => 'desc'
                );
            }
            $records = $this->timesheet_model
                    ->where('parent_Rid', '0')
                    ->where('deleted', '0')
                    ->where('status', 'Submitted')
                    ->where("(
   `initiator` = " . $this->auth->user_id() . " OR  
   FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR   
    FIND_IN_SET( '" . $this->auth->user_id() . "',tsother_approvers)    
   )")
                    // ->where('intg_projects.company_id',$this->auth->company_id())
                    ->likes($field, $this->session->userdata('sapfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        $this->load->model('users/user_model', null, true);
        Template::set('users', $this->user_model->find_all());
        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Template::set('records', $records);
        Template::set('monthly', $monthly);
        Template::set('current_week', $current_week);
        Template::set('monthly_chunk_th', $monthly_chunk_th);
        Template::render();
    }

    function delrow() {

        $result = $this->db->query("delete from intg_timesheet_details where mid = '" . $this->input->post('mid') . "' AND  tid = '" . $this->input->post('tid') . "' and ptskid = '" . $this->input->post('ptskid') . "'");
        if ($result) {
            Template::set_message(lang('task_delete_success'), 'success');
        } else {
            Template::set_message(lang('task_delete_fail') . $this->db->_error_message(), 'error');
        }
    }

    function lock_unlock_action() {
        $mthyr = explode("-", $this->input->post('lockmonth'));
        $status = $this->input->post('status') == "lock" ? "unlock" : "lock";
        $timesheetstatus = $this->input->post('status') == "lock" ? "Created" : "Delayed";
        $this->db->query("update intg_timesheet set status='$timesheetstatus' where uid = '" . $this->input->post('uid') . "' and mth = '" . $this->input->post('mth_lock') . "' and yr = '" . $mthyr[1] . "' and period = '" . $this->input->post('period') . "'");

        $this->db->query("update intg_timesheet_lock_history set status='$status' where id = '" . $this->input->post('lurowid') . "'");
    }

    function unlock() {
        $this->db->query("update intg_timesheet set status='Created' where id = '" . $this->input->post('tid') . "'");
    }

    function lock_unlock() {
        if (!has_permission('Tasks.Projectmgmt.LockUnlockView')) {
            Template::set_message("No Access", 'error');
            redirect(SITE_AREA . '/projectmgmt/tasks/timesheets');
        }
        if (isset($_POST['massupd'])) {

            $checked = $this->input->post('rowselect');
            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid => $key) {

                    $result = $this->db->query("update intg_timesheet_lock_history set status = 'unlock' where id = '" . $key . "'");
                    $this->db->query("update intg_timesheet set status = 'Created' where uid = " . $_POST['name'][$pid] . " and mth = " . $_POST['mth_lock'][$pid] . " and period = " . $_POST['period'][$pid] . "");

                    //echo $this->db->last_query()."<br>";
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . 'success');
                } else {
                    Template::set_message("Failure" . $this->timesheet_lock_history_model->error, 'error');
                }
            }
        }
        $this->load->model('timesheet_lock_history/timesheet_lock_history_model', null, true);
        $this->load->library('session');
        $this->load->library('pagination');
        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('lufield', $this->input->post('select_field'));
                $this->session->set_userdata('lufvalue', $this->input->post('field_value'));
                $this->session->set_userdata('lupfname', $this->input->post('field_name'));
                break;
            case 'Reset':
                $this->session->unset_userdata('lufield');
                $this->session->unset_userdata('lufvalue');
                $this->session->unset_userdata('lufname');
                break;
        }
        if ($this->session->userdata('lufield') != '') {
            $field = $this->session->userdata('lufield');
        } else {
            $field = 'id';
        }
        if ($this->session->userdata('lufield') == 'All Fields') {
            $total = $this->timesheet_lock_history_model
                    ->where("(
     `name`   like ('%" . $this->session->userdata('lufvalue') . "%') OR 
   `department` like ('%" . $this->session->userdata('lufvalue') . "%') OR
   `lockmonth` like ('%" . $this->session->userdata('lufvalue') . "%')
 
   )", NULL, FALSE)
                    ->count_all();
            // echo $this->db->last_query();
        } else {
            $total = $this->timesheet_lock_history_model
                    ->likes($field, $this->session->userdata('lufvalue'), 'both')
                    ->count_all();
            // echo $this->db->last_query();
        }
        $offset = $this->input->get('per_page') ? $this->input->get('per_page') : 0;
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = isset($total->numrows) ? $total->numrows : 0;
        $this->pager['per_page'] = 10;
        $this->pager['num_links'] = 5;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;
        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }
        if ($this->session->userdata('lufield') == 'All Fields') {
            $records = $this->timesheet_lock_history_model
                    ->where("(
     `name`   like ('%" . $this->session->userdata('lufvalue') . "%') OR 
   `department` like ('%" . $this->session->userdata('lufvalue') . "%') OR
   `lockmonth` like ('%" . $this->session->userdata('lufvalue') . "%')
 
   )", NULL, FALSE)
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'intg_tasks.id' => 'desc',
                );
            }
            $records = $this->db->query("SELECT * FROM intg_timesheet_lock_history  ORDER BY `id` desc")->result();
        }
        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('records', $records);
        Template::set('total', isset($total->numrows) ? $total->numrows : 0);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Template::render();
    }

    public function timesheets($current_week = 0, $monthly = 0, $monthly_chunk_th = 1) {

        if ($monthly == 0) {
            $today = date('d');
            if ($today < 16)
                $monthly = 3;
            else
                $monthly = 4;
        }

        //echo $monthly;

        $uid = $this->auth->user_id();
        $this->load->library('workflow');
        list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();

        //check if user@role have workflow or not
        if ($typeworkflow == "none") {
            Template::set_message("No Timesheet Workflow Available, Please Contact Administrator", 'error');
            redirect_back();
        }

        //check if user@role have user_level_id or not
        if ($this->current_user->user_level_id == "") {
            Template::set_message("You did not have User Level/FTE Level assigned, Please Contact Administrator", 'error');
            redirect_back();
        }



        if ($monthly == 3) {
            $first_sechalf = " < 16 ";
            $period = "1";
        }
        if ($monthly == 4) {
            $first_sechalf = " > 15 ";
            $period = "2";
        }

        if (isset($_POST['rtounlock'])) {
            $this->load->model('timesheet_lock_history/timesheet_lock_history_model', null, true);
            $data = array();
            $data['name'] = $uid;
            $data['department'] = 0;
            $data['lockmonth'] = $this->input->post('cmonth');
            $data['mth_lock'] = $this->input->post('mth_lock');
            $data['requested_on'] = date("Y-m-d");
            $data['status'] = "lock";
            $data['period'] = $period;
            $this->timesheet_lock_history_model->insert($data);

            $this->load->library('emailer/emailer');
            $u = $this->db->query("select email,display_name from intg_users u,intg_roles r where u.role_id = r.role_id and r.role_name = 'Finance Team'")->result();

            foreach ($u as $us) {
                $this->emailer->addAddress($us->email, $us->display_name);
                $dis = $us->display_name;
                $this->emailer->Subject = '[Timesheet ACGT] Request to unlock';
                $this->emailer->Body = $this->load->view('_emails/timesheet_unlock', array('item_type' => 'timesheet', 'period' => $period == 1 ? "1st Half," : "2nd Half," . $this->input->post('cmonth'), 'finance_name' => $us->display_name, 'creator_name' => $this->current_user->display_name), TRUE);
            }
            $this->emailer->mail();
        }


        if (isset($_POST['save'])) {

            $m = $this->input->post('month');
            $y = $this->input->post('year');
            $p = $this->input->post('period');

            $this->load->model('timesheet/timesheet_model', null, true);
            $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . $m . "' and yr ='" . $y . "' and period='" . $p . "'  and status ='Created' order by id desc");
            $s = $r->result();
            $id = NULL;

            foreach ($s as $dd) {
//                echo $dd->id . "<br>";
                list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();
                // make sure we only pass in the fields we want
                $data = array();
                $data['parent_Rid'] = $this->input->post('parent_Rid');
                $typeworkflow == "none" ? $data['final_status'] = "none" : "";
                $data['final_approvers'] = $work_flow_approvers;
                $data['initiator'] = $dataroleuser;
                $data['tsother_approvers'] = $this->tasks_model->get_other_approvers();
                $data['uid'] = $uid;
//                $data['mth'] = date("m");
                $data['mth'] = $m;
//                $data['yr'] = date("Y");
                $data['yr'] = $y;
                $data['status'] = 'Submitted';

                if ($dd->final_status == "Reject") {
                    $data['final_status'] = 'No';
                }

                $data['submission_date'] = date("Y-m-d");
                $this->timesheet_model->update($dd->id, $data);
                $this->workflow->create_approvers($dd->id);
                $this->sendmail($data['initiator'], $dd->id, $url);
                if ($s) {
                    Template::set_message(lang('projects_create_success'), 'success');
                    //	redirect(SITE_AREA .'/projectmgmt/tasks/approval');
                }
            }
        }
        $this->load->model('users/user_model', null, true);
        Template::set('users', $this->db->query("select * from intg_users where  user_level_id > " . $this->current_user->user_level_id . "")->result());

//        $this->load->model('projects/projects_model', null, true);
//        Template::set('projects', $this->db->query("select * from intg_projects where FIND_IN_SET (subsidiary_id,'" . $this->current_user->subsidiary_id . "')")->result());

        $this->load->model('timesheet/timesheet_model', null, true);
        Template::set('monthly', $monthly);
        Template::set('current_week', $current_week);
        $timesheets = array();
        $filter_string = array();
        if ($this->input->get('users')) {
            array_push($filter_string, 'it.uid =' . $this->input->get('users') . '');
        }
        if ($this->input->get('projects')) {
            array_push($filter_string, 'itd.pid =' . $this->input->get('projects') . '');
        }


        if ($filter_string) {
            $timesheets = $this->db->query("SELECT *,it.id as itid,it.status as itstatus FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid AND " . implode(' AND ', $filter_string) . " and m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%') and day(itd.tsd_date)  $first_sechalf  group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result();
            $allow_user = $this->input->get('users');
        } else {
            $timesheets = $this->db->query("SELECT *,it.id as itid,it.status as itstatus FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid and it.uid ='" . $this->auth->user_id() . "'  and m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%') and day(itd.tsd_date)  $first_sechalf group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result();
            $allow_user = $this->auth->user_id();
        }
//        echo $this->db->last_query();die();
        $allowed_user_ids[] = "";
        $timesheets2 = $this->db->query("SELECT *,it.id as itid,it.status as itstatus FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid and it.uid ='" . $this->auth->user_id() . "'  and m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%')  group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result();
        foreach ($timesheets2 as $tuids) {
            $allowed_user_ids[] = $tuids->initiator;
            $allowed_user_ids[] = $tuids->final_approvers;
        }
        Template::set('timesheet', $timesheets);
        Template::set('allowed_user_ids', $allowed_user_ids);


        $getM = $this->input->get('m');
        $getY = $this->input->get('y');
        $getP = $this->input->get('p');

//        if($getM==""){
        $submissionDate = "select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . date("m") . "' and yr ='" . date("Y") . "' and period = '" . $period . "' order by id asc";
//        }else{
//              $submissionDate = "select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . $getM . "' and yr ='" . $getY . "' and period = '".$getP."' order by id asc";
//        }
        Template::set('subdate', $this->db->query($submissionDate)->result());
//        Template::set('subdate', $this->db->query("select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . date("m") . "' and yr ='" . date("Y") . "' and period='".$period."' order by id desc")->result());


        if ($getM == "") {
            $apprdate_sql = "select * from intg_timesheet it,intg_approval_status ias where ias.approval_status_module_id = 41 and ias.approval_status_mrowid = it.id and ias.approval_status_action_by=it.final_approvers and   it.uid =  '" . $this->auth->user_id() . "' and it.mth = '" . date("n") . "' and it.yr ='" . date("Y") . "'  and it.period='" . $period . "' order by it.id desc";
        } else {
            $apprdate_sql = "select * from intg_timesheet it,intg_approval_status ias where ias.approval_status_module_id = 41 and ias.approval_status_mrowid = it.id and ias.approval_status_action_by=it.final_approvers and   it.uid =  '" . $this->auth->user_id() . "' and it.mth = '" . $getM . "' and it.yr ='" . $getY . "'  and it.period='" . $period . "' order by it.id desc";
        }

        Template::set('apprdate', $this->db->query($apprdate_sql)->result());



        $month = date('n', strtotime(date('Y') . '-' . date('m') . '-1 ' . $current_week . ' month'));
        $year = date('Y', strtotime(date('Y') . '-' . date('m') . '-1 ' . $current_week . ' month'));

        $preventSub_sql = "select sum(itd.tsd_hours) as thours from intg_timesheet_details itd,intg_timesheet it where it.id = itd.tid and it.status='Created'  and it.mth = '" . $month . "' and it.yr ='" . $year . "' and it.uid='" . $this->auth->user_id() . "' and day(tsd_date) {$first_sechalf}  group by itd.tsd_date,it.uid";
        $preventsub = $this->db->query($preventSub_sql);




        //echo $this->db->last_query();
        if ($preventsub->num_rows() < 1) {
            $submitdisabled = "disabled='disabled'";
            Template::set('emptyresults', $preventsub->num_rows());
        }




        foreach ($preventsub->result() as $ps) {
            //loop throud all rows of recrods, if found thours less than 8.5 then set submit for approval button disabled
            if ($ps->thours < 8.5) {
////                if($ps->thours == 0){
//                    $submitdisabled = "";
//                }else{              
                $submitdisabled = "disabled='disabled'";
//                }
            }
        }

        $unlockdisabled = $this->db->query("select status from intg_timesheet_lock_history where name = '" . $uid . "' and lockmonth = '" . date('m-Y', strtotime('first day of this month ' . $current_week . ' month')) . "' and period = $period")->row();
        //echo $this->db->last_query();
        Template::set('disable_add_new_ts', $this->tasks_model->disable_add_new_ts());
        Template::set('allow_user', $allow_user);
        Template::set('submitdisabled', $submitdisabled);
        Template::set('unlockdisabled', $unlockdisabled);
        Template::set('monthly_chunk_th', $monthly_chunk_th);
        Template::set('typeworkflow', $typeworkflow);
        Template::render();
    }

    public function sendmail($generator, $id, $url) {


        //echo $generator."/".$id."/".$url; exit;

        $this->load->library('emailer/emailer');


        //echo $row_id;
        //exit;

        $sql = 'SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="41" and approval_status_mrowid = "' . $id . '"  ORDER BY id DESC';
        $queryapprover = $this->db->query($sql);
        $rowapp = $queryapprover->row();
        $user_iids = (explode(",", $rowapp->approval_status_action_by));
        //var_dump($user_iids);exit();
        $size = sizeof($user_iids);
        $pro = $this->db->query('select * from intg_timesheet where id="' . $id . '"')->row();
        for ($i = 0; $i < $size; $i++) {
            //echo $rowapp11->email."--".$rowapp->rolename;
            $apdet = $this->db->query("SELECT display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
            $rowapp11 = $apdet->row();
            $this->emailer->AddAddress("" . $rowapp11->email . "", "" . $rowapp->rolename . "");
            //$mail->AddCC('rudravalambi.saikishore@gmail.com',$rowmail->rolename.$i);
            //$mail->Subject
            $creator = $this->db->query("SELECT display_name FROM intg_users WHERE id = '" . $pro->initiator . "'")->row();
            $this->emailer->Subject = 'Timesheet for ' . $creator->display_name . ' is ready for approval';
            $this->emailer->Body = $this->load->view('_emails/timesheet_app', array('item_type' => 'timesheet', 'item_name' => $pro->mth . '/' . $pro->yr, 'assignee_name' => $rowapp11->display_name, 'start_date' => $pro->project_start_date, 'end_date' => $pro->project_end_date, 'creator_name' => $creator->display_name), TRUE);

//                                        echo " project_name :".$pro->project_name;
//                                        echo ' assignee_name :'.$rowapp11->display_name;
//                                        echo ' start_date :'.$pro->project_start_date;
//                                        echo ' end_date :'.$pro->project_end_date;
//                                        echo ' creator_name :'.$creator->display_name;
//                                        exit();

            $this->emailer->mail();

            $this->load->model('notifications/notifications_model', null, true);
            $this->notifications_model->insert(array(
                'user_id' => $user_iids[$i],
                'content' => ' Timesheet for ' . $creator->display_name . ' is ready for approval',
                'title' => ' Timesheet for ' . $creator->display_name . ' is ready for approval',
                'link' => site_url('admin/projectmgmt/tasks/approval'),
                'class' => 'fa-briefcase',
                'module' => 'ProjectMgmt',
                'type' => 'info'
            ));
            //echo $this->db->last_query();
            //exit();
        }
    }

    public function timesheets_viewapprove($current_week = 0, $monthly = 0, $monthly_chunk_th = 1) {
        if (isset($_POST['save'])) {
            $this->load->model('timesheet/timesheet_model', null, true);
//            $uid = $this->auth->user_id();//alif commented
            $uid = $_POST['timesheetowner']; //
            $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . date("m") . "' and yr ='" . date("Y") . "' and status ='created' order by id desc");
            $s = $r->result();
            //echo "select * from intg_timesheet where uid = '".$uid."' and mth = '".date("m")."' and yr ='".date("Y")."' and status ='created' order by id desc";
            //exit();
            $id = NULL;
            //	if ( $s[0]->status=="Submitted" || $r->num_rows() ==0  )  {
            $this->load->library('workflow');
            foreach ($s as $dd) {
//                echo $dd->id . "<br>";
                list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();
                // make sure we only pass in the fields we want
                $data = array();
                $data['parent_Rid'] = $this->input->post('parent_Rid');
                $typeworkflow == "none" ? $data['final_status'] = "none" : "";
                $data['final_approvers'] = $work_flow_approvers;
                $data['initiator'] = $dataroleuser;
                //$data['status']        = 'Created';
                $this->timesheet_model->update($dd->id, $data);
                //$id = $this->timesheet_model->insert($data);
                $this->workflow->create_approvers($dd->id);
            }
            if ($s) {
                Template::set_message(lang('projects_create_success'), 'success');
                redirect(SITE_AREA . '/projectmgmt/tasks/approval');
            }
            //}
        }
        $this->load->model('users/user_model', null, true);
        Template::set('users', $this->user_model->find_all());
        //$this->load->model('projects/projects_model', null, true);
        //Template::set('projects', $this->projects_model->get_projects());
        $this->load->model('timesheet/timesheet_model', null, true);

        $a = $this->db->query("select * from intg_timesheet where id=" . $this->uri->segment(5) . "")->row();
        Template::set('qq', $a);

        if ($a->period == "1") {
            $first_sechalf = " < 16 ";
        }
        if ($a->period == "2") {
            $first_sechalf = " > 15 ";
        }

        if ($a->mth < 10) {
            $m = "0" . $a->mth;
        } else {
            $m = $a->mth;
        }
        $timesheets = $this->db->query("SELECT *,it.id as itid,it.status as itstatus FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid and it.uid ='" . $a->uid . "'  and m.completed_status is null and itd.mid=m.milestone_id and itd.tsd_date like '" . $a->yr . '-' . $m . "%' and day(itd.tsd_date)  $first_sechalf group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result();
        Template::set('timesheet', $timesheets);
//                    Template::set('timesheet', $this->db->query("SELECT *,itd.id as itid FROM intg_timesheet it,intg_timesheet_details itd where it.id=itd.tid and it.uid ='" . $a->uid . "' and itd.tsd_date like ('" . date($a->yr.'-'.$m) . "%') and itd.status ='enabled' and day(itd.tsd_date)  $first_sechalf group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result());
//                    Template::set('timesheet', $this->db->query("SELECT *,itd.id as itid FROM intg_timesheet it,intg_timesheet_details itd where it.id=itd.tid and it.uid ='" . $a->uid . "' and itd.tsd_date like ('" . date($a->yr.'-'.$m) . "%') and itd.status ='enabled' and it.period = ".$a->period." group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result());
//		echo $this->db->last_query(); exit;



        Template::set('subdate', $this->db->query("select * from intg_timesheet where deleted = 0 and id = '" . $this->uri->segment(5) . "' order by id desc")->result());
        Template::set('apprdate', $this->db->query("select * from intg_timesheet it,intg_approval_status ias where it.deleted = 0 and ias.approval_status_module_id = 41 and ias.approval_status_mrowid = it.id and   ias.approval_status_mrowid = '" . $this->uri->segment(5) . "' order by it.id desc")->result());

        Template::set('monthly', $monthly);
        Template::set('current_week', $current_week);
        Template::set('monthly_chunk_th', $monthly_chunk_th);
        Template::render();
    }

    public function timesheets_cso($current_week = 0, $monthly = 0, $monthly_chunk_th = 1) {

        if ($this->uri->segment(6) == 3) {
            $first_sechalf = " < 16 ";
            $period = "1";
        }
        if ($this->uri->segment(6) == 4) {
            $first_sechalf = " > 15 ";
            $period = "2";
        }

        /* $this->load->model('timesheet_details/timesheet_details_model', null, true);
          $this->timesheet_details_model->cso_timesheet_from_project($projectid=23); */

        $this->load->library('workflow');
        list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();

        //check if user@role have workflow or not
        $NoApprovalNeeded = "0";
        if ($typeworkflow == "none") {
            $NoApprovalNeeded = "1";
            Template::set('NoApprovalNeeded', $NoApprovalNeeded);
            Template::set_message("No Timesheet Workflow Available. Timesheet will be automatically approved after clicking Submit for Approval button", 'info');
            // redirect(SITE_AREA .'/projectmgmt/projects');
        }

        if ($this->current_user->user_level_id == "") {
            Template::set_message("You did not have User Level/FTE Level assigned, Please Contact Administrator", 'error');
            redirect_back();
        }



        if (isset($_POST['purge'])) {
            $itid = $this->input->post('itid');
            $ptskid = $this->input->post('ptskid');
            $mid = $this->input->post('mid');


//                        print_r($itid);
//                        print_r($ptskid);
//                        print_r($mid);
//                        

            if (is_array($itid) && count($itid)) {

                $result = FALSE;
                foreach ($itid as $key => $value) {
                    $result = $this->db->query("delete from intg_timesheet_details where "
                            . "mid = '" . $mid[$key] . "' "
                            . "AND  tid = '" . $value . "' "
                            . "AND ptskid = '" . $ptskid[$key] . "'");
                }
                $countSuccess = 1;
                $countFailed = 1;

                if ($result) {
                    $countSuccess++;
                    Template::set_message($count . " " . lang('task_delete_success'), 'success');
                } else {
                    $countFailed++;
                    Template::set_message($countFailed . " " . lang('task_delete_fail'), 'error');
                }
            }
        }


        //	print_r($_GET);
        //$this->cso_insert_timesheet_cron();
        $uid = $this->auth->user_id();

        if (!in_array($uid, $this->config->item('csouid'))) { //settle VO
            redirect_back();
        }


        if (isset($_POST['rtounlock'])) {
            $this->load->model('timesheet_lock_history/timesheet_lock_history_model', null, true);
            $data = array();
            $data['name'] = $uid;
            $data['department'] = 0;
            $data['lockmonth'] = $this->input->post('cmonth');
            $data['mth_lock'] = $this->input->post('mth_lock');
            $data['requested_on'] = date("Y-m-d");
            $data['status'] = "lock";
            $data['period'] = $period;
            $this->timesheet_lock_history_model->insert($data);

            $this->load->library('emailer/emailer');
            $u = $this->db->query("select email,display_name from intg_users u,intg_roles r where u.role_id = r.role_id and r.role_name = 'Finance Team'")->result();

            foreach ($u as $us) {
                $this->emailer->addAddress($us->email, $us->display_name);
                $dis = $us->display_name;
                $this->emailer->Subject = '[Timesheet ACGT] Request to unlock';
                $this->emailer->Body = $this->load->view('_emails/timesheet_unlock', array('item_type' => 'timesheet', 'period' => $period == 1 ? "1st Half," : "2nd Half," . $this->input->post('cmonth'), 'finance_name' => $us->display_name, 'creator_name' => $this->current_user->display_name), TRUE);
            }
            $this->emailer->mail();
        }




        // workfflow submit for approval
        if (isset($_POST['save'])) {

            $m = $this->input->post('month');
            $y = $this->input->post('year');
            $p = $this->input->post('period');

//                print_r($_POST);die();
            $this->load->model('timesheet/timesheet_model', null, true);
//            $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . date("n") . "' and yr ='" . date("Y") . "'  and status ='Created' order by id desc");
            $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . $m . "' and yr ='" . $y . "' and period='" . $p . "'  and status ='Created' order by id desc");
            $s = $r->result();
            $id = NULL;
            $this->load->library('workflow');
            foreach ($s as $dd) {
//                echo $dd->id . "<br>";
                list ($dataroleuser, $typeworkflow, $work_flow_approvers ) = $this->workflow->checkflow();
                // make sure we only pass in the fields we want
                $data = array();
                $data['parent_Rid'] = $this->input->post('parent_Rid');
//                $typeworkflow == "none" ? $data['final_status'] = "none" : "";
                $typeworkflow == "none" ? $data['final_status'] = "Yes" : ""; //Alif 19/09/2016. Changed to Yes for auto approved cso. Need to set Yes, because FTE Report only check for final_status = Yes.
                $data['final_approvers'] = $work_flow_approvers;
                $data['initiator'] = $dataroleuser;
                $data['tsother_approvers'] = $this->tasks_model->get_other_approvers();
                $data['uid'] = $uid;
                $data['mth'] = $m;
                $data['yr'] = $y;
                $data['status'] = 'Submitted';

                if ($dd->final_status == "Reject") {
                    $data['final_status'] = 'No';
                }

                $data['submission_date'] = date("Y-m-d");
                $this->timesheet_model->update($dd->id, $data);
                $this->workflow->create_approvers($dd->id);
                $this->sendmail($data['initiator'], $dd->id, $url);
                if ($s) {
                    $typeworkflow == "none" ? Template::set_message(lang('task_submit_success_auto'), 'success') : Template::set_message(lang('projects_create_success'), 'success');
                    //	redirect(SITE_AREA .'/projectmgmt/tasks/approval');
                }
            }
        }
        $this->load->library('session');
        $this->load->model('users/user_model', null, true);
        Template::set('users', $this->user_model->find_all());
        $this->load->model('projects/projects_model', null, true);
        Template::set('projects', $this->projects_model->find_all());
        $this->load->model('timesheet/timesheet_model', null, true);
        $this->load->model('timesheet_details/timesheet_details_model', null, true);

        if ($this->input->post('submit')) {
            $this->session->set_userdata('tsusers', $this->input->post('users'));
            $tsusers = $this->session->userdata('tsusers');
            $this->session->set_userdata('tsprojects', $this->input->post('projects'));
            $tsprojects = $this->session->userdata('tsprojects');
        }



        Template::set('monthly', $monthly);
        Template::set('current_week', $current_week);



        $timesheets = array();
        $filter_string = array();
        if ($this->input->get('users')) {
            array_push($filter_string, 'it.uid =' . $this->input->get('users') . '');
        }
        if ($this->input->get('projects')) {
            array_push($filter_string, 'itd.pid =' . $this->input->get('projects') . '');
        }


        if ($this->uri->segment(6) == '3') {
            $first_sechalf = " < 16 ";
            $period = "1";
        }
        if ($this->uri->segment(6) == '4') {
            $first_sechalf = " > 15 ";
            $period = "2";
        }

        if ($filter_string) {

            Template::set('timesheet', $this->db->query("SELECT *,it.id as itid,it.status as itstatus,itd.status as itdstatus, itd.id as itd_id FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid AND " . implode(' AND ', $filter_string) . " and m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%') and day(itd.tsd_date)  $first_sechalf  group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result());
            $allow_user = $this->input->get('users');
        } else {

            Template::set('timesheet', $this->db->query("SELECT *,it.id as itid,it.status as itstatus,it.uid as ituid,itd.status as itdstatus, itd.id as itd_id FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m  where it.id=itd.tid  and  m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%') and  it.uid = " . $this->auth->user_id() . " and day(itd.tsd_date)  $first_sechalf group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result());
        }
        //	echo $this->db->last_query();

        $getM = $this->input->get('m');
        $getY = $this->input->get('y');
        $getP = $this->input->get('p');

        if ($getM == "") {
            $submissionDate = "select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . date("m") . "' and yr ='" . date("Y") . "' and period = '" . $period . "' order by id asc";
        } else {
            $submissionDate = "select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . $getM . "' and yr ='" . $getY . "' and period = '" . $getP . "' order by id asc";
        }

        Template::set('subdate', $this->db->query($submissionDate)->result());
        Template::set('apprdate', $this->db->query("select * from intg_timesheet it,intg_approval_status ias where ias.approval_status_module_id = 41 and ias.approval_status_mrowid = it.id and   it.uid =  '" . $this->auth->user_id() . "' and it.mth = '" . date("m") . "' and it.yr ='" . date("Y") . "' order by it.id desc")->result());


        $month = date('n', strtotime(date('Y') . '-' . date('m') . '-1 ' . $current_week . ' month'));
        $year = date('Y', strtotime(date('Y') . '-' . date('m') . '-1 ' . $current_week . ' month'));

        $preventsub = $this->db->query("select round(sum(itd.tsd_hours),2) as thours from intg_timesheet_details itd,intg_timesheet it where it.id = itd.tid and it.status='Created'  and it.mth = '" . $month . "' and it.yr ='" . $year . "' and it.uid='" . $this->auth->user_id() . "' and month(itd.tsd_date) = $month and day(tsd_date) {$first_sechalf}  group by itd.tsd_date,it.uid")->result();




        $submitdisabled = "";
        if (sizeof($preventsub) > 0) {
            foreach ($preventsub as $ps) {
                if ($ps->thours < 8.5) {
                    $submitdisabled = "disabled='disabled'";
                }
            }
        } else {
            $submitdisabled = "disabled='disabled'";
        }
        $unlockdisabled = $this->db->query("select * from intg_timesheet_lock_history where name = '" . $uid . "' and lockmonth = '" . date('m-Y', strtotime('first day of this month ' . $current_week . ' month')) . "'")->row();
//        echo $this->db->last_query();
        Template::set('submitdisabled', $submitdisabled);
        Template::set('unlockdisabled', $unlockdisabled);
        Template::set('monthly_chunk_th', $monthly_chunk_th);
        Template::set('disable_add_new_ts', $this->tasks_model->disable_add_new_ts());
        Template::render();
    }

    public function popover() {
        Template::render();
    }

    public function create_ts() {
        if (isset($_POST['save'])) {
            $this->load->model('timesheet/timesheet_model', null, true);
            $this->load->model('timesheet_details/timesheet_details_model', null, true);
            $uid = $this->auth->user_id();

            $tsddate = format_date($this->input->post('ts_date'), "Y-m-d");

            $day = date("d", strtotime($tsddate));

            $period = "1";
            if ($day < 16) {
                $period = "1";
            } else {
                $period = "2";
            }

            $r = $this->db->query("select * from intg_timesheet where uid = '" . $uid . "' and mth = '" . date("n", strtotime($tsddate)) . "' and yr ='" . date("Y", strtotime($tsddate)) . "' and period = '" . $period . "' order by id desc");
            $s = $r->result();
            $id = NULL;

//            if ($s[0]->status == "Submitted" || $r->num_rows() == 0) {
            if ($r->num_rows() == 0) {
                $data = array();
                $data['uid'] = $uid;
//                $data['mth'] = date("m");
                $data['mth'] = date("m", strtotime($tsddate));
                $data['period'] = date("d", strtotime($tsddate)) < 16 ? 1 : 2;
//                $data['yr'] = date("Y");
                $data['yr'] = date("Y", strtotime($tsddate));
                $data['status'] = 'Created';
                $id = $this->timesheet_model->insert($data);
            }
            $tid = $id == NULL ? $s[0]->id : $id;

            $td = $this->db->query("select * from intg_timesheet_details where tid = '" . $tid . "' and pid='" . $this->input->post('project_name') . "' and mid='" . $this->input->post('milestone') . "' and ptskid='" . $this->input->post('task_pool') . "' and tsd_date ='" . $tsddate . "'");

            if ($td->num_rows == 0) {

                $data2['tid'] = $tid;
                $data2['pid'] = $this->input->post('project_name');
                $data2['mid'] = $this->input->post('milestone');
                $data2['ptskid'] = $this->input->post('task_pool');
                $data2['tsd_date'] = $tsddate;
                $data2['tsd_hours'] = $this->input->post('hours');
                $data2['comments'] = $this->input->post('description');

                $checkFTE = get_dailyfte_bydate($this->current_user->user_level_id, $tsddate);
                if ($checkFTE) {
                    $data2['user_level_details_id'] = $checkFTE['user_level_details_id'];
                    $data2['user_level_details_fte_rate'] = $checkFTE['daily_fte'];
                } else {
                    $return['status'] = "fte_error";
                    echo json_encode($return);
                    exit;
                }


                $this->timesheet_details_model->insert($data2);
                $return['status'] = "success";
                echo json_encode($return);
                exit;
            } else {
                $return['status'] = "duplicate";
                echo json_encode($return);
                exit;
            }
        }
        Template::render();
    }

    public function update_hr_com() {
        $this->load->model('timesheet_details/timesheet_details_model', null, true);

        if (isset($_POST['cso'])) {
            $hours = $this->input->post('hours') == "" ? "8.5" : $this->input->post('hours');
            $tsempty_hours = $this->db->query("select *,itd.id as itid from intg_timesheet it,intg_timesheet_details itd where it.id = itd.tid  and it.uid = " . $this->auth->user_id() . " and  itd.tsd_date = '" . $this->input->post('clicked_date') . "' and ( tsd_hours is null or tsd_hours = 0 )");
            //echo $this->db->last_query();
            $tshours = $this->db->query("select sum(itd.tsd_hours) as thours from intg_timesheet it,intg_timesheet_details itd where it.id = itd.tid and it.uid = " . $this->auth->user_id() . " and itd.tsd_date='" . $this->input->post('clicked_date') . "' and  tsd_hours  > 0 ")->row()->thours;
            //	echo $this->db->last_query();             
            $hval = abs(($hours - $tshours) / $tsempty_hours->num_rows());

            //CODE TO ENTER THE ENTIRE COLUMN ROWS IF THEY DONT EXIST
            /* if (  $tsempty_hours->num_rows() == 0 ) {
              $new_tsempty_hours = $this->db->query("select *,itd.id as itid from intg_timesheet it,intg_timesheet_details itd where it.id = itd.tid  and it.uid = ".$this->config->item('csouid')."  group by tid,pid,mid,ptskid");

              $nthval = abs(($hours - $tshours)   / $new_tsempty_hours->num_rows());
              echo $hours."<br>";
              echo $new_tsempty_hours->num_rows();

              foreach ($new_tsempty_hours->result() as $nt) {

              $proj_start_date = $this->db->query("select start_date from intg_milestones where milestone_id = ".$nt->mid."")->row()->start_date;
              //echo $this->db->last_query();
              if( $this->input->post('clicked_date') >= $proj_start_date ){
              $datanewts = array();

              $datanewts['tid'] = $nt->tid;
              $datanewts['pid'] = $nt->pid;
              $datanewts['mid'] = $nt->mid;
              $datanewts['ptskid'] = $nt->ptskid;
              $datanewts['tsd_date'] = $this->input->post('clicked_date');
              $datanewts['tsd_hours'] = $nthval;
              $this->timesheet_details_model->insert($datanewts);
              }
              }
              } */
            foreach ($tsempty_hours->result() as $t) {
                echo "before proj date";
                echo "after project date";
                $this->db->query("update intg_timesheet_details set tsd_hours = '" . $hval . "' where id = '" . $t->itid . "'");
            }
        } else {

            if ($_POST['rowno'] == "") {
                $data_insert = array();
                $data = array();
                $_POST['taskid'] == "undefined" ? "" : $data['tid'] = $_POST['taskid'];
                $_POST['projectid'] == "undefined" ? "" : $data['pid'] = $_POST['projectid'];
                $_POST['milestoneid'] == "undefined" ? "" : $data['mid'] = $_POST['milestoneid'];
                $_POST['ptaskid'] == "undefined" ? "" : $data['ptskid'] = $_POST['ptaskid'];
                $data['tsd_hours'] = $_POST['hours'] == "" ? 0 : $_POST['hours'];
                $data['tsd_date'] = $_POST['timedate'];
                $data['comments'] = $_POST['comments'];

                $checkFTE = get_dailyfte_bydate($this->current_user->user_level_id, $_POST['timedate']);

                if ($checkFTE) {
                    $data['user_level_details_id'] = $checkFTE['user_level_details_id'];
                    $data['user_level_details_fte_rate'] = $checkFTE['daily_fte'];
                } else {
                    $data_insert['result'] = "failed";
                    $data_update['fail_msg'] = "Retrieve FTE in insert failed";
                    echo json_encode($data);
                    die;
                }

                $id = $this->timesheet_details_model->insert($data);

                if (is_int($id)) {
                    echo $id;
                } else {
                    $data_insert['result'] = "failed";
                    echo json_encode($data_insert);
                }
//                die("rowno xder");
            } else {
                $data_update = array();
                $data = array();
                if ($_POST['hours'] == "") {
                    $_POST['hours'] = 0;
                }

                $checkFTE = get_dailyfte_bydate($this->current_user->user_level_id, $_POST['timedate']);

                if ($checkFTE) {
                    $data['user_level_details_id'] = $checkFTE['user_level_details_id'];
                    $data['user_level_details_fte_rate'] = $checkFTE['daily_fte'];
                } else {
                    $data_update['result'] = "failed";
                    $data_update['fail_msg'] = "Retrieve FTE in update failed";
                    echo json_encode($data_update);
                    die;
                }

                $data["tsd_hours"] = $_POST['hours'];
                $data["comments"] = $_POST['comments'];

                $result = $this->timesheet_details_model->update($_POST['rowno'], $data);

                if ($result) {
                    $data_update['result'] = "success";
                    $data_update['comment'] = $_POST['comments'];
                } else {
                    $data_update['result'] = "failed";
                    $data_update['fail_msg'] = "update failed";
                }

                echo json_encode($data_update);
                die("rowno ade");
            }
        }
    }

    public function get_tsd_hours_left() {
        $sql = "SELECT SUM(itd.`tsd_hours`) AS totaltsdhours "
                . "FROM `intg_timesheet` it "
                . "JOIN `intg_timesheet_details` itd ON itd.`tid` = it.`id` "
                . "WHERE uid = " . $_GET['userid'] . " "
                . "AND tsd_date = '" . format_date($_GET['date'], "Y-m-d") . "'"
//                . "AND itd.pid = ".$_GET['projid']
        ;
        $totaltsdhours = $this->db->query($sql)->row()->totaltsdhours;
        $totalhoursleft = 24 - $totaltsdhours;
        echo json_encode($totalhoursleft);
    }

    public function export($current_week = 0, $monthly = 0, $monthly_chunk_th = 1) {
        $name = $this->db->query("select display_name from intg_users where id = " . $this->auth->user_id() . "")->row()->display_name;

        if ($this->uri->segment(6) == 3) {
            $first_sechalf = " < 16 ";
            $period = "1";
        }
        if ($this->uri->segment(6) == 4) {
            $first_sechalf = " > 15 ";
            $period = "2";
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $styleArray = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'f1c40f')
            ),
            'font' => array(
                'bold' => true,
//                'color' => array('rgb' => 'FF0000'),
                'size' => 11,
                'name' => 'Verdana'
        ));

        //$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        // $this->excel->getActiveSheet()->getStyle($columnID . '1')->applyFromArray($styleArray);

        $this->excel->getActiveSheet()->setTitle('Timesheet Report');

        $this->excel->getActiveSheet()->setCellValue('A1', 'Name');
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('A2', 'Employee Id');
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('A3', 'Period');
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArray);

        $this->excel->getActiveSheet()->setCellValue('B1', $name);
        $this->excel->getActiveSheet()->setCellValue('B2', $this->auth->user_id());
        $this->excel->getActiveSheet()->setCellValue('B3', $this->input->get("periodName"));





        $this->excel->getActiveSheet()->setCellValue('C1', 'Latest Timesheet Submission Date');
        $this->excel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('C2', 'Latest Timesheet Approval Date');
        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($styleArray);


        $subdate = $this->db->query("select * from intg_timesheet where uid = '" . $this->auth->user_id() . "' and mth = '" . date("m") . "' and yr ='" . date("Y") . "' and period='" . $period . "' order by id desc")->result();
        foreach ($subdate as $sd) {

            if ($sd->submission_date != '' || $sd->submission_date != null) {

                $whateverdate[] = date("d-m-Y", strtotime($sd->submission_date));
            }
        }

        $this->excel->getActiveSheet()->setCellValue('D1', implode(",", $whateverdate));

        $apprdate = $this->db->query("select * from intg_timesheet it,intg_approval_status ias where ias.approval_status_module_id = 41 and ias.approval_status_mrowid = it.id and ias.approval_status_action_by=it.final_approvers and   it.uid =  '" . $this->auth->user_id() . "' and it.mth = '" . date("n") . "' and it.yr ='" . date("Y") . "'  and it.period='" . $period . "' order by it.id desc")->result();
        $n = 1;
        foreach ($apprdate as $ad) {

            switch ($ad->approval_status_status) {
                case "No" :
                    $status = "Pending Approval";
                    break;
                case "Yes" :
                    $status = "Approved";
                    break;
                case "Reject" :
                    $status = "Rejected";
                    break;
            }

            if ($ad->approval_status_action_date != '' || $ad->approval_status_action_date != null) {

                $anotherwhateverdate[] = $n . ') ' . date("d-m-Y", strtotime($ad->approval_status_action_date)) . "/" . $status;
            }
            $n++;
        }

        $this->excel->getActiveSheet()->setCellValue('D2', implode(",", $anotherwhateverdate));


        $this->excel->getActiveSheet()->setCellValue('A5', 'No');
        $this->excel->getActiveSheet()->getStyle('A5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('B5', 'Project');
        $this->excel->getActiveSheet()->getStyle('B5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('C5', 'Milestone');
        $this->excel->getActiveSheet()->getStyle('C5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->setCellValue('D5', 'Task');
        $this->excel->getActiveSheet()->getStyle('D5')->applyFromArray($styleArray);



        $cc = $this->db->query("SELECT *,it.id as itid,it.status as itstatus FROM intg_timesheet it,intg_timesheet_details itd,intg_milestones m where it.id=itd.tid and it.uid ='" . $this->auth->user_id() . "'  and m.completed_status is null and itd.mid=m.milestone_id  and itd.tsd_date like ('" . date('Y-m', strtotime('first day of this month ' . $current_week . ' month')) . "%') and day(itd.tsd_date)  $first_sechalf group by tid,pid,mid,ptskid ORDER BY itd.id desc")->result();

//echo $this->db->last_query(); exit;

        $cell = "E";

        $today = date('d');





        $days_time_stamp = array();
        $total_monthly_chunk = 0;


        if ($monthly == 3) { //1st Half
            $current_month = date('m');
            $current_year = date('Y');
            $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));

            for ($i = 1; $i <= $days_number; $i++) {

                $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');
            }

            $days_time_stamp_chunks = array_chunk($days_time_stamp, 15);
            $total_monthly_chunk = count($days_time_stamp_chunks);
            $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];
        }
        if ($monthly == 4) { //2nd Half
            $current_month = date('m');
            $current_month_daycount = date('t');
            $current_year = date('Y');
            $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
            $sechalf = $days_number - 15;
            $plusday = 1;
            for ($i = 16; $i <= $days_number; $i++) {
                if ($i > $current_month_daycount) {
                    $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $current_month_daycount . ' ' . $current_week . ' month +' . $plusday . " day");
                    $plusday++;
                } else {
                    $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');
                }
            }

            $days_time_stamp_chunks = array_chunk($days_time_stamp, $sechalf);
            $total_monthly_chunk = count($days_time_stamp_chunks);
            $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];
        }


        $date_format = 'd D';

        foreach ($days_time_stamp as $day_time_stamp) {
            $this->excel->getActiveSheet()->setCellValue($cell . '5', date($date_format, $day_time_stamp));
            $this->excel->getActiveSheet()->getStyle($cell . '5')->applyFromArray($styleArray);

            $cell++;
        }

        $no = 1;
        $row = 6;
        foreach ($cc as $record) {
            $rcprojects = $this->db->query("select project_name,status_nc from intg_projects where id = " . $record->pid . "")->row()->project_name;
            $rcmilestone = $this->db->query("select * from intg_milestones where milestone_id = " . $record->mid . "")->row()->milestone_name;
            $rctaskpool = $this->db->query("select task_name from intg_task_pool where id = " . $record->ptskid . "")->row()->task_name;

            $this->excel->getActiveSheet()->setCellValue('A' . $row, $no);
            $this->excel->getActiveSheet()->setCellValue('B' . $row, $rcprojects);
            $this->excel->getActiveSheet()->setCellValue('C' . $row, $rcmilestone);
            $this->excel->getActiveSheet()->setCellValue('D' . $row, $rctaskpool);

            $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "'")->result();

            $ncell = "E";

            foreach ($days_time_stamp as $day_time_stamp) {

                $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "' and tsd_date = '" . date('Y-m-d', $day_time_stamp) . "'")->row();

                $hrs{$day_time_stamp} = $hrs{$day_time_stamp} + $tms->tsd_hours;
                $this->excel->getActiveSheet()->setCellValue($ncell . $row, empty($tms->tsd_hours) ? 0 : $tms->tsd_hours);
                if (!empty($tms->comments)) {
                    $this->excel->getActiveSheet()->getComment($ncell . $row)->getText()->createTextRun($tms->comments);
                }
                $ncell++;
            }



            $no++;
            $row++;
        }

        $this->excel->getActiveSheet()->setCellValue("D" . $row++, "Total Hours");
        $this->excel->getActiveSheet()->setCellValue("D" . $row++, "OT");
        $ncell = "E";
        $row = $row - 2;
        foreach ($days_time_stamp as $day_time_stamp):
            if ($this->current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                $OTvalid = 8.0;
            } else {
                $OTvalid = 8.5;
            };
            if ($this->current_user->origin == "Estate") {
                $OTvalid = 8.0;
            };

            if ($hrs{$day_time_stamp} < $OTvalid)
                $color = "#F00";

            if ($hrs{$day_time_stamp} == $OTvalid)
                $color = "#339933";

            if ($hrs{$day_time_stamp} > $OTvalid)
                $color = "#000";


            $this->excel->getActiveSheet()->setCellValue($ncell . $row, number_format($hrs{$day_time_stamp}, 2));

            $color = "";
            $ncell++;
        endforeach;

        $ncell = "E";
        $row++;
        foreach ($days_time_stamp as $day_time_stamp):
            if ($this->current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                $OTvalid = 8.0;
            } else {
                $OTvalid = 8.5;
            };
            if ($this->current_user->origin == "Estate") {
                $OTvalid = 8.0;
            };


            if ($hrs{$day_time_stamp} != 0) {
                if ($hrs{$day_time_stamp} - $OTvalid > 0) {

                    $this->excel->getActiveSheet()->setCellValue($ncell . $row, number_format($hrs{$day_time_stamp} - $OTvalid, 2));
                } else {
                    $this->excel->getActiveSheet()->setCellValue($ncell . $row, "0");
                }
            }

            $ncell++;

        endforeach;


        $filename = 'Timesheet ' . $name . ' - ' . $this->input->get("periodName") . '.XLSX'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
        ob_end_clean();
        $objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');
        //Template::render();
    }

    public function timesheet_report() {

        $selectedUser = $this->db->query("SELECT * FROM intg_users WHERE id IN (301,303,307,311,341,339,319,317,318)")->result();
//        print_r($selectedUser);




        Template::set('selectedUser', $selectedUser);
        Template::render();
    }

}
