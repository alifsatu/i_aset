<div>
	<h1 class="page-header">Tasks</h1>
</div>

<?php if (isset($records) && is_array($records) && count($records)) : ?>
				
	<table class="table table-striped table-bordered">
		<thead>
			<tr>				
		<th>Project Id</th>
		<th>Task Name</th>
		<th>Description</th>
		<th>Parent Task Id</th>
		<th>Progress Percentage</th>
		<th>Color</th>
		<th>Send Notification</th>
		<th>Estimated Hours</th>
		<th>Notes</th>
		<th>Task Tag</th>
			</tr>
		</thead>
		<tbody>
		
		<?php foreach ($records as $record) : ?>
			<?php $record = (array)$record;?>
			<tr>
			<?php foreach($record as $field => $value) : ?>
				
				<?php if ($field != 'id') : ?>
					<td>
						<?php if ($field == 'deleted'): ?>
							<?php e(($value > 0) ? lang('tasks_true') : lang('tasks_false')); ?>
						<?php else: ?>
							<?php e($value); ?>
						<?php endif ?>
					</td>
				<?php endif; ?>
				
			<?php endforeach; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php endif; ?>