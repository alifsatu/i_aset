
<table class="table table-bordered">
    <?php
    $month = array('6', '7', '8', '9', '10', '11', '12');


    foreach ($selectedUser as $value) {
        $total_cost = 0;
        echo '<tr>';
        echo '<td>';
        echo "<b>Staff Name</b><br> " . $value->display_name;
        echo '</td>';
        echo '<td>';

        echo '<table class="table table-bordered">';
        echo '<tr>';
        foreach ($month as $mth) {
            $timesheet = $this->db->query("SELECT * FROM intg_timesheet where mth = " . $mth . " and yr = 2017 and uid =  " . $value->id)->result();


            echo '<td>';
            $dateObj = DateTime::createFromFormat('!m', $mth);
            $monthName = $dateObj->format('F');
            echo '<b>' . $monthName . '</b>';

            echo '<table class="table table-bordered">';
            foreach ($timesheet as $v) {
                $approval = $this->db->query("SELECT * FROM intg_approval_status WHERE  approval_status_module_id = 41 AND approval_status_mrowid = " . $v->id)->row();

                $timehsheet_details = $this->db->query("SELECT SUM(tsd_hours) as hr FROM intg_timesheet_details WHERE tid = " . $v->id)->row();

                $cost = get_actual_cost_by_user_hours($value->id, $timehsheet_details->hr);


                echo '<tr><td>';
                echo " Period " . $v->period;
                echo " <b>Status :</b> " . $v->status;
                echo '<br><b>Submission Date: </b><br>' . $v->submission_date;
                echo '<br><b>Approval Date: </b><br>' . $approval->approval_status_action_date;
                echo '<br><b>Cost : </b><br>' . $cost;
                echo '</tr></td>';
                $total_cost += $cost;
            }
            echo '</table>';

            echo '</td>';
        }

        echo '<td>';
        echo '<b>Total:</b>RM' . $total_cost;
        echo '</td>';

        echo '</tr>';
        echo '</table>';

        echo '</td>';
        echo '</tr>';
    }
    ?>
</table>
