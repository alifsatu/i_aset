<?php

$num_columns	= 8;
$can_delete	= $this->auth->has_permission('Cost_Centre.Settings.Delete');
$can_edit		= $this->auth->has_permission('Cost_Centre.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>Timesheet</h4></div>
  <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">


<? if ( $this->session->userdata('cost_centrefield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('cost_centrefield')?>"><?=$this->session->userdata('cost_centrefield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<option value="cost_centre">Cost Centre</option>
<option value="sbu_name">SBU</option>
<option value="description">Description</option>


</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('cost_centrefvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('cost_centrefname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('cost_centrefield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<!--<th class="column-check"><input class="check-all" type="checkbox" /></th>-->
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_cost_centre') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/cost_centre?sort_by=cost_centre&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'cost_centre') ? 'desc' : 'asc'); ?>'>
                    User</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_sbu') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/cost_centre?sort_by=sbu&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sbu') ? 'desc' : 'asc'); ?>'>
                    Month& Year</a></th>
                    
                   <th>Period</th>
                    
                   <th>Submission Date</th>
                   <th>Status</th>
				   <th></th>
				   <th></th>
				  
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<!--<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('tasks_delete_confirm'))); ?>')" />
					</td>-->
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php $numbering = 1;
				if ($has_records) : $no = $this->input->get('per_page')+1;
				$j=0;
					foreach ($records as $record) : 
					$querychild = $this->db->query('SELECT * FROM intg_timesheet WHERE  parent_Rid = "'.$record->id.'" ORDER BY id desc');

if ( $querychild->num_rows() == 0 ) { 
			
			include "getparent.php";
                        $numbering++;
                 } 
				 else
				 {
					 $nochild = 1; $ver = $querychild->num_rows();
					foreach ( $querychild->result() as $rowchild ) {
				//include "getchild.php";
				$nochild++; $ver--;
				$parentid = $rowchild->parent_Rid;
			} 
			//include "getparent2.php";
				 }			
					
				?>
				
				<?php $no++;
					endforeach;
					$this->session->set_userdata('t_pending',$j);
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php   echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
