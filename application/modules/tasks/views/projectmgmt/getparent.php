                       <?php // print_r($record); ?>
<script>

    function callpop(val)
    {
        //alert('quick approve');
        $('[rel=popover]').popover('destroy');

        if ($("#mypop" + val).hasClass('pop')) {
            $("#mypop" + val).removeClass('pop')
        } else {

            waitingDialog.show('...Starting quick approval dialog');

            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/quickapprove') ?>", {docid: $("#mypop" + val).attr("qid"), initiator: $("#mypop" + val).attr("initiator"), finalapprover: $("#mypop" + val).attr("fapprovers")}, function (data) {
//	$('#mypop'+val).popover();	
                $("#mypop" + val).attr('data-content', data).popover("show").addClass('pop');
                waitingDialog.hide();

            });

        }
    }



    function updateApprovalstatus(val, val2, val3, val4, val5, val6, val8, val7)
    {
        console.log("updateApprovalstatus in getParent.php");
        var a = $("input[name='approval_status_status']:checked").val();
        var b = $('#asr').val();
        
        $('#appr2' + val, '#appr1' + val).html("<img src='<?php echo Template::theme_url('images/loadingnew.gif') ?>' />");
        waitingDialog.show('...Approval in process, please wait');
        $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/updateApprovalstatus') ?>", {
            status: a, 
            remarks: b, 
            rowid: val2, 
            rolename: val3, 
            docid: val, 
            mrowid: val4, 
            act: val5, 
            userid: val6, 
            finalapprover: val8, 
            initiator: val7
        }, function (resp) {
            var inn1 = $('#inner1', resp).html();
            var inn2 = $('#inner2', resp).html();
            $("#appr2" + val).html(inn1);
            $("#appr1" + val).html(inn2);
            waitingDialog.hide();
        });



    }


    function attache(val)
    {
        var a = $("#fileField").val();
    }


</script>

<style>

</style>
<?php $apprstat = NULL; ?>
<?php if ($has_records) : ?>

<?php endif; ?>
<tbody>
    <?php
    if ($has_records) : $no = $this->input->get('per_page') + 1;
        ?>
        <tr>
            <?php if ($can_delete) : ?>
                                                                                    <!--<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td>-->
                <td><?= $record->id; ?></td>


            <?php endif; ?>
            <td><?= $numbering; ?></td>
            <?php /* if ($can_edit) : ?>
              <td><?php echo anchor(SITE_AREA . '/quotes/quote/edit/'.$record->id, '<span class="fa fa-pencil"></span>&nbsp;' .  $record->client_name); ?></td>
              <?php else : ?>
              <td><?php e($record->client_name); ?></td>
              <?php endif; */ ?>
            <td><?= $this->auth->display_name_by_id($record->uid) ?></td>
            <td><?php e($record->mth) ?>/<?php e($record->yr) ?> </td>
			 <td><?php e($record->period=="1" ? "1st Half" : "2nd Half") ?></td>
            <td><?php echo format_date($record->submission_date, "d/m/Y") ?></td>
            <td><?php e($record->status) ?></td>

                                                                            <!--<td><?php echo date('d-m-Y h:i:s ', strtotime($record->quote_created)) ?></td>-->


            <td>
                <div id="appr1<?= $record->id ?>">
                   <?php //echo $record->id;?>
                    <input type="hidden" name="timesheetowner" value="<?= $record->uid; ?>" />

                    <?php
                    if ($record->final_status == "Reject" || $record->final_status == "none" || $record->final_status == "Yes") {

                        // check if logged in user is the generator of the quote		
                        if ($this->auth->user_id() == $record->user_id) {
                            echo anchor(SITE_AREA . '/quotes/quote/edit/' . $record->id . '/' . $record->user_id . '/' . $_GET['per_page'], '<i class=" fa fa-pencil">&nbsp;</i>', 'title="Create Version"');
                        }
                    } else if ($record->final_status == "No") {

                        $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "41" and approval_status_mrowid = "' . $record->id . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by) order by id desc');
                        //echo 'SELECT * from intg_approval_status WHERE  approval_status_module_id = "41" and approval_status_mrowid = "'.$record->id.'"  and  FIND_IN_SET('.$this->auth->user_id().',approval_status_action_by)';
                        //echo $this->db->last_query();
                        $rowm = $querym->row();


                        if ($querym->num_rows() > 0) {
                            $apprstat = $rowm->approval_status_status;
                            $querym2 = $this->db->query("SELECT approval_status_status,hierarchy_status,rolename from intg_approval_status WHERE approval_status_module_id = '41'  AND approval_status_mrowid = '" . $record->id . "' AND hierarchy_status = '" . $rowm->hierarchy_status . "' order by id desc");
                            $rowm2 = $querym2->row();
                            //echo "<br>".$this->db->last_query();

                            if ($rowm2->approval_status_status == "No" && $rowm->hierarchy_status == "0") {
                                $i = $i + 1;

                                echo anchor(SITE_AREA . '/projectmgmt/tasks/timesheets_viewapprove/' . $record->id . '/perpage/' . $_GET['per_page'], '<i class="fa fa-pencil">&nbsp;</i>', 'title="View and Approve"')
                                ?></a>&nbsp;&nbsp;<a href="#"  rel="popover"  data-html="true"  data-original-title="Approvers Action" data-placement="left"  data-trigger="click" id="mypop<?= $record->id ?>" onclick="callpop('<?= $record->id ?>')" title="Quick Approve"   qid="<?= $record->id ?>"  initiator="<?= $record->initiator ?>" fapprovers="<?= $record->final_approvers ?>" ><i class="fa  fa-check"></i></a><div style="position:relative"><div id="load<?= $record->id ?>"   style="display:none; position:absolute; top:0; right:100px;"></div></div>
                                <?php
                            }

                            if ($rowm2->approval_status_status == "No" && $rowm->hierarchy_status != "0") {
                                $querym3 = $this->db->query("SELECT approval_status_status,hierarchy_status,rolename from intg_approval_status WHERE  approval_status_module_id = '41' and  approval_status_mrowid = '" . $record->id . "' AND rolename = '" . $rowm2->hierarchy_status . "'  order by id desc");
                                // echo "<br>".$this->db->last_query(); 				
                                $rowm3 = $querym3->row();


                                if ($rowm3->approval_status_status == "Yes") {

                                    echo anchor(SITE_AREA . '/projectmgmt/tasks/timesheets_viewapprove/' . $record->id . '/perpage/' . $_GET['per_page'], '<i class="fa fa-pencil">&nbsp;</i>', 'title="View and Approve"')
                                    ?></a>&nbsp;&nbsp;<a href="#"  rel="popover"  data-html="true"  data-original-title="Approvers Action" data-placement="left"  data-trigger="click" id="mypop<?= $record->id ?>" onclick="callpop('<?= $record->id ?>')"   qid="<?= $record->id ?>"  initiator="<?= $record->initiator ?>" fapprovers="<?= $record->final_approvers ?>" ><i class="fa fa-check"></i></a>
                                            <?php
                                        }
                                    }
                                }
                            }else{
                                echo "No Final Status";
                            }
                            ?>
        </div>
            </td>

    <td>
        <div id="appr2<?= $record->id ?>"  class="swing animated">
            <?php
            $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="41" and bas.approval_status_mrowid = "' . $record->id . '" ORDER BY bas.id asc');
            //echo 'SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="41" and bas.approval_status_mrowid = "'.$record->id.'" ORDER BY bas.id asc'; 
            //echo $this->db->last_query();
            ?>



            <a href="#" data-toggle="popover" data-html="true" data-placement="left" data-trigger="hover"  data-original-title="Approvers" data-content="<div class='scrollable' style='height:auto;width:500px'><table width='100%' class='table table-condensed'>
               <tr>   
               <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
               <td><strong><?php echo lang('quote_role') ?></strong></td>
               <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
               <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
               <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
               </tr><?php
               foreach ($queryappr->result() as $rowappr) {

                   switch ($rowappr->approvers_status) {
                       case "No" : $status = "<span class='badge btn-warning'>Pending</span>";
                           break;
                       case "Yes" : $status = "<span class='badge btn-primary'>Approved</span>";
                           break;
                       case "Reject" : $status = "<span class='badge btn-danger'>Rejected</span>";
                           break;
                       case "": $status = "<span class='badge btn-danger'>NULL</span>";
                           break;
                   }
                   ?>

                   <tr>   
                   <td><?= $rowappr->display_name ?></td>
                   <td><?= $rowappr->role_name ?></td>
                   <td><?= $status ?></td>
                   <td><?= wordwrap($rowappr->approvers_remarks, 100, "<br />\n"); ?></td>
                   <td><?php
                   if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                       echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                   }
                   ?></td>

                   </tr><?php } 
                   
                   if ($record->parent_Rid != 0) { ?><?php } ?></table></div>" <?php
               if ($record->final_status == "No") {
                   if ($apprstat == "Yes") {
                       ?>class="btn btn-success"><i class=" fa fa-check"></i><?php
                   } else {?>
                        class="btn btn-warning" ><i class="fa fa-cogs"></i><?php
                    }
                } else if ($record->final_status == "Reject") { ?>
                        class="btn btn-danger"><i class="fa fa-times"></i><?php 
                } else if ($record->final_status == "Yes") {?>
                        class="btn btn-success"><i class=" fa fa-check">
                <?php }else if($record->final_status == ""){ ?>
                class="btn btn-danger"><i class=" fa fa-exclamation-triangle"></i>
                    <?php } ?></i></a>
        </div>
    </td>




    </tr><?php endif; ?>
                
