<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Add Timesheet<button type="button" class="close" data-dismiss="modal">×</button></header>
            <div class="panel-body">
                <?php echo form_open($this->uri->uri_string(), 'id="formts"'); ?>
                <div class="form-group clearfix">
                    <div class="col-sm-5">
                   
                        <label>Date</label>
                        <div class='controls'>
                        <?php if ( $this->uri->segment(6)==0 ) { ?>
                            <input id='ts_date' class='input-sm input-s  form-control' readonly="readonly" type='text' name='ts_date' value="<?php 
                            if ( $this->uri->segment(5)==3 ) {  
                                if ( date('d') > 15 ) {  
                                    echo '15/'.date('m/Y');
                                } else {  
                                    echo  date("d/m/Y");
                                }
                            } 
                            if ( $this->uri->segment(5)==4 ) {
                                if ( date('d') < 16 ) {  
                                    echo '16/'.date('m/Y');
                                } else {  
                                    echo  date("d/m/Y");
                                }
                            }  ?>
                                   "/><?php } else { ?>
                                   
                            <input id='ts_date' class='input-sm input-s  datepickers form-control' readonly="readonly" type='text' name='ts_date' value="" required=""/>
									 <script>
									 var today = new Date();
var lastDate = new Date(today.getFullYear(), today.getMonth(0)-1, 31);
		$('#ts_date').datepicker({
			format:"dd/mm/yyyy",
	   startDate: '-1m',
    endDate: lastDate
	});
	</script>
									 <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
                // Change the values in this array to populate your dropdown as required

                $prgm = $this->db->query("SELECT * FROM  intg_projects WHERE deleted = 0 AND  final_status = 'Yes'  and `row_status` = 'final' AND status='initiated' "
//                        . "AND FIND_IN_SET(subsidiary_id ,'" . $current_user->subsidiary_id . "' ) "
                        . "AND ( `initiator` = " . $this->auth->user_id() . " OR FIND_IN_SET( '" . $this->auth->user_id() . "',coworker) OR  FIND_IN_SET( '" . $this->auth->user_id() . "',final_approvers) OR  FIND_IN_SET( '" . $this->auth->user_id() . "',other_approvers))  ORDER BY project_name")->result();

//echo $this->db->last_query();
                $prgm2 = $this->db->query("select * from intg_projects where status_nc = 'yes'")->result();

//echo $this->db->last_query();
                ?>
                <div class="form-group clearfix">
                    <div class="col-sm-5">
                        <label>Project</label>
                        <div class='controls'>
                            <select name="project_name" id="project_name" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" data-required="true">
                                <option></option>
                                <?php foreach ($prgm as $pn) { ?>
                                    <option value="<?= $pn->id ?>"><?= $pn->project_name ?></option>
                                <?php } ?>
                                <?php  foreach ($prgm2 as $pn2) { ?>
                                    <option value="<?= $pn2->id ?>"><?= $pn2->project_name."-".$pn2->prefix_io_number ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                      
                        <label>Milestone</label>
                        <div class='controls'>
                            <select name="milestone" id="milestone" class="form-control selecta"  style="height:30px;width:200px; font-size:12px" data-required="true">
                                <option></option>


                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-5">

                        <label>Task</label>
                        <div class='controls'>
                            <select name="task_pool" id="task_pool" class="form-control selecta"  style="height:30px;width:200px; font-size:12px" data-required="true">
                                <option></option>

                            </select>
                             <input id='hours' class='input-sm  form-control'  type='hidden' name='hours' data-required="true" value="0"  autocomplete="off"/>
                        </div>
                    </div>
                    <!--<div class="col-sm-5">
                        <label>Hours</label>
                        <div class='controls'>
                            <input id='hours' class='input-sm  form-control'  type='hidden' name='hours' data-required="true" value="0"  autocomplete="off"/>

                        </div>
                    </div>-->
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-10"> <?php echo form_label('Description', 'description', array('class' => 'control-label')); ?>
                        <div class='controls'> <?php echo form_textarea(array('name' => 'description', 'id' => 'description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40')); ?> </div>
                    </div>
                </div>
                <div class="form-actions  clearfix">
                    <div class="col-sm-10">
                        <div class="controls">

                            <input type="button" id="submitform" name="save" class="btn btn-primary "   value="Save" />   <input type="button" name="save" class="btn btn-warning"  data-dismiss="modal" value="Close" />&nbsp;&nbsp;<span id="spin"></span>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?> </div>
        </section></div>
    <div class="col-sm-3"></div>

</div>
<script>
    $("#submitform").click(function () {

        if ($("#formts").parsley('validate')) {
            $("#spin").html("<i class='fa fa-spin fa-spinner'></i>");
            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/create_ts') ?>", $("#formts").serialize() + '&save=', function (data) {

//                console.log(JSON.parse(data));
                var dt = JSON.parse(data)
                if (dt.status == "duplicate") {
                    $("#spin").html('<p><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i>Duplicate record exists for the same selections and date</div></p>');
                }else if(dt.status == "fte_error"){
                       $("#spin").html('<p><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i>Error in retrieving Daily FTE Rate.<br>\n\
                   Please check your Daily FTE Rate for current date or date range is exist, <br>or contact system administrator for further assistants.</div></p>');
                }else if(dt.status == 'success') {
                    $("#spin").html("<i class='fa fa-check' style='color:rgb(142, 193, 101)'></i>");
                    location.reload();
                }else{
                    $("#spin").html('<p><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><i class="fa fa-ban-circle"></i>Unable to create data, please refresh page and try again. </div></p>');
                }
                

            });



        }

    });
    $(document).ready(function () {
		
		
		$(".datepickers").datepicker();
		
        $('#milestone,#project_name,#task_pool').select2(
                {placeholder: "Please Select", allowClear: false, dropdownAutoWidth: true}


        );



        $("#project_name").change(function () {
            $('#milestone').select2('val', '');
            $('#task_pool').select2('val', '');
            $.getJSON("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/populateoptions') ?>", {projid: $(this).val(), ajax: 'true', type: "milestone" , tsdate:$("#ts_date").val()}, function (j) {
				console.log("milestone>>>> ",j);
                if (j == false || j == null) {
                    alert("Project Not Started");
                    $("#milestone").attr('disabled', 'disabled');
                    $("#task_pool").attr('disabled', 'disabled');
                    $("#hours").attr('disabled', 'disabled');
                    $("#submitform").attr('disabled', 'disabled');
                } else {
//                    alert("Project Started");
                    var options = '<option></option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].milestone_id + '">' + j[i].milestone_name + '</option>';
                    }
                    $("#milestone").removeAttr('disabled');
                    $("#task_pool").removeAttr('disabled');
                    $("#hours").removeAttr('disabled');
                    $("#submitform").removeAttr('disabled');
                    $("#milestone").html(options);
                }
            });



            $.getJSON("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/get_tsd_hours_left') ?>", {projid: $(this).val(), ajax: 'true', userid: "<?= $this->auth->user_id(); ?>", date : $("#ts_date").val() }, function (totaltsdhours) {
//                alert(totaltsdhours);
                $("#hours").attr('placeholder',"Max "+totaltsdhours+" Hours Left");
                $("#hours").attr('max',totaltsdhours);
            });

        });


        $("#milestone").change(function () {
             $('#task_pool').select2('val', '');
            $.getJSON("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/populateoptions') ?>", {milestone_id: $(this).val(), ajax: 'true', type: "task"}, function (j) {
                console.log(j.length);
                if (j == false) {
                    if(j.length < 1){
                        alert("Milestone do not have task");
                    }else{
                        alert("Milestone Not Started");
                    }
                    $("#task_pool").attr('disabled', 'disabled');
                    $("#hours").attr('disabled', 'disabled');
                    $("#submitform").attr('disabled', 'disabled');
                } else {
//                    alert("Milestone Started");
                    var options = '<option></option>';
                    for (var i = 0; i < j.length; i++) {
                        options += '<option value="' + j[i].task_pool_id + '">' + j[i].task_name + '</option>';
                    }
                    $("#task_pool").removeAttr('disabled');
                    $("#hours").removeAttr('disabled');
                    $("#submitform").removeAttr('disabled');
                    $("#task_pool").html(options);
                }
                
                
                
            })
        })



    });


//    $(function () {
//        $('#hours').keypress(function (event) {
//            event.preventDefault();
//            return false;
//        });
//    });

</script>
<script src="<?php echo Template::theme_url('js/parsleyold/parsley.min.js'); ?>" cache="false"></script> 
