<?php
	
	$monthly = $monthly == 1;
?>
<div>
<section class="panel panel-default"  > 
<header class="panel-heading"><strong>Timesheet Details </strong>  <button href="#" class="moreless btn btn-xs btn-default pull-right" data-toggle="class:hide">
		                  <i class="fa fa-compress text"></i>		                  
		                  <i class="fa fa-expand text-active"></i>		                 
		                </button></header>

<div class="row text-sm wrapper" id="moreless">
<div class="col-sm-5">
<table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     
                    
                        <tr>                    
                          <td><strong>Name:</strong></td>
                          <td><?=$this->auth->display_name_by_id($this->auth->user_id())?></td>
                        </tr>
                        <tr>                    
                          <td><strong>Employee Id:</strong></td>
                          <td>546456</td>
                        </tr>
                        <tr>                    
                          <td><strong>Period:</strong></td>
                          <td>?</td>
                        </tr>                     
                    </table>

      </div>

                
                  <div class="col-sm-5">
                  <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     
                    
                        <tr>                    
                          <td><strong>Submission Date:</strong></td>
                          <td><? foreach ( $subdate as $sd ) { echo date("d-m-Y", strtotime($sd->submission_date))."<br>"; }?></td>
                        </tr>
                        <tr>                    
                          <td><strong>Approval Date:</strong></td>
                           <td><? foreach ( $apprdate as $ad ) { echo date("d-m-Y", strtotime($ad->approval_status_action_date))."<br>"; }?></td>
                        </tr>                        
                     
                    </table>
                </div>
    </div>
                </section>
<section class="panel panel-default">
                <header class="panel-heading">
                <?php 
							$date_format = 'M jS D';
							$days_time_stamp = array();
							$total_monthly_chunk = 0;
							if($monthly) {
								$current_month = date('m');
							//	echo "CURRENT MONTH:".$current_month;
								$current_year = date('Y');
								$days_number = date('t', strtotime($current_year.'-'.$current_month.'-1 '.$current_week.' month'));
								
								for($i = 0; $i <= $days_number; $i++)
								{
									$days_time_stamp[] = strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month');
								}
								$days_time_stamp_chunks = array_chunk($days_time_stamp, 30);
								$total_monthly_chunk = count($days_time_stamp_chunks);
								$days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th-1];
							} else {
								$days_time_stamp = array(
									strtotime('monday this week '.$current_week.' week'),
									strtotime('tuesday this week '.$current_week.' week'),
									strtotime('wednesday this week '.$current_week.' week'),
									strtotime('thursday this week '.$current_week.' week'),
									strtotime('friday this week '.$current_week.' week'),
									strtotime('saturday this week '.$current_week.' week'),
									strtotime('sunday this week '.$current_week.' week')
								);
							}
				
							
				if($monthly)
				
					e('Month: '.date('l F jS', strtotime('first day of this month '.$current_week.' month')). ' - '.date('l F jS', strtotime('last day of this month '.$current_week.' month')));
				else	
				
				
				
					echo '<b>Week: '. date('l F jS', strtotime('monday this week '.$current_week.' week')). ' - '. date('l F jS', strtotime('today '.$current_week.' week')).'</b>';
				?> <span class="btn-xs">(* disabled inputs are due to old version, please click add entry to add new version)</span>
              
                </header>
               
         
      
                
                <div class="row text-sm wrapper">
                
                  <div class="col-sm-2 m-b-xs">
                    <div class="btn-group">
						<a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/'.($current_week-1).'/'.($monthly ? 1: 0)));?>" class="btn btn-default"><i class="fa fa-chevron-left"></i></a>
						<a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets'));?>" class="btn btn-default" <?php if($current_week == 0): e('disabled'); endif;?>>Today</a>
                    <?   if ( $current_week+1 < 1 ) { ?>
						<a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/'.($current_week+1).'/'.($monthly ? 1: 0)));?>" class="btn btn-default"><i class="fa fa-chevron-right"></i></a>
                       <? } ?>
					</div>
                  </div>
                  <div class="col-sm-2 m-b-xs">
                    <div class="btn-group timesheet-view">
                      <label class="btn btn-sm btn-default <?php echo $monthly ? : 'active'; ?>" data-value="weekly">Week</label>
                      <label class="btn btn-sm btn-default <?php echo $monthly ? 'active' : ''; ?>" data-value="monthly">Month</label>
                    </div>
                    
                  </div>
                  <div class="col-sm-3 m-b-xs">
                  <input type="text" class="form-control input input-sm input-xs pull-left" placeholder="Date" id="datepicker"/>
                  <input type="button" class="btn btn-sm btn-primary" id="datesubmit"  value="Get It"/>
                  </div>
                  
				 
                   <div class="col-sm-5">
                   
                   <?php echo form_open($this->uri->uri_string()); ?>
           
                    <select class="input-sm form-control input-s-sm  selecta " name="users">
                    
						<option value="0">All Users</option>
					<?php foreach($users as $user): if ( $this->session->userdata('tsusers')==$user->id ) { ?>
                      <option selected="selected" value="<?php e($user->id);?>"><?php e($user->display_name);?></option><? } else { ?>
                      <option value="<?php e($user->id);?>"><?php e($user->display_name);?></option><? } ?>
					<?php endforeach;?>
                    </select> 
                    
                    <select class="input-sm form-control input-s-sm  selecta " name="projects">
						<option value="0">All Projects</option>
					<?php foreach($projects as $project): if ( $this->session->userdata('tsprojects')==$project->id ) { ?>
                      <option selected="selected" value="<?php e($project->id);?>"><?php e($project->project_name);?></option><? } else { ?>
                      <option value="<?php e($project->id);?>"><?php e($project->project_name);?></option><? } ?>
					<?php endforeach;?>
                    </select> 
                    
                    <input type="submit" name="submit" class="btn btn-sm btn-primary" />
					 <a href="<?=base_url()?>index.php/admin/projectmgmt/tasks/create_ts" class="btn btn-sm btn-success" style=" color: #FFF"  data-toggle="ajaxModal"><i class="fa fa-plus"></i></a><?php echo form_close(); ?>
				
                  </div>
                   
                     
                   </div>
				
               
                
                 <?php echo form_open($this->uri->uri_string()); ?>
                <div class="table-responsive">
                <div style="width:1087px; overflow:auto; margin:15px" >
                  <table class="table table-striped b-t b-light text-sm timesheet-table" width="1700">
                    <thead>
                      <tr align="center">
                        <th></th>
                        <th>No</th>
                        <th>Project</th>
                        <th>Milestone</th>
                        <th>Task</th>
						<?php 
							
						foreach($days_time_stamp as $day_time_stamp):
						?>
                        <th><?php e(date($date_format, $day_time_stamp));?></th>
						<?php endforeach;?><th></th>
                      </tr>
                    </thead>
                    <tbody>
					<?php $task_ids = array();?>
					<?php $no=1; foreach($timesheet as $record):?>
					<?php $task_ids[] = (int) $record->id;?>
                      <tr>
                       <th> 
						<? //echo $record->uid."<br>".$record->itstatus; 
						 if ( $record->itstatus=="Created" && $record->uid == $this->auth->user_id() ) { ?>
                         <a href="javascript:void()" onclick="commfunc('<?=$record->itid?>','<?=$record->ptskid?>','delrow')"><i class="fa fa-times" style="color:#F00"></i></a>
						 <? } if ( $record->itstatus=="Delayed" && $this->auth->role_id()== 1 ) { ?>
                        <a href="javascript:void()" onclick="commfunc('<?=$record->itid?>','<?=$record->ptskid?>','unlock')"><i class="fa fa-unlock"></i></a><? } ?>                         
                         </th>
                        <th><?=$no?></th>
                        <th><?php e($this->db->query("select project_name from intg_projects where id = ".$record->pid."")->row()->project_name);?></th>
                        <th><?php e($this->db->query("select milestone_name from intg_milestones where milestone_id = ".$record->mid."")->row()->milestone_name);?></th>
                        <th><?php e($this->db->query("select task_name from intg_task_pool where id = ".$record->ptskid."")->row()->task_name);?></th>
                        
						<?php 
						
					$tms = $this->db->query("select * from intg_timesheet_details where tid = '".$record->tid."' and  pid =  '".$record->pid."'and  mid = '".$record->mid."' and  ptskid = '".$record->ptskid."'")->result();
						
						foreach($days_time_stamp as $day_time_stamp): 
						
						$tms = $this->db->query("select * from intg_timesheet_details where tid = '".$record->tid."' and  pid =  '".$record->pid."'and  mid = '".$record->mid."' and  ptskid = '".$record->ptskid."' and tsd_date = '".date('Y-m-d', $day_time_stamp)."'")->row();						
						
						 $hrs{$day_time_stamp} =  $hrs{$day_time_stamp} + $tms->tsd_hours;						 
						
						if ( date('Y-m-d', $day_time_stamp) >   date('Y-m-d') ) 
							{ break;}
						
						?>
                        <td <? if (  date('Y-m-d', $day_time_stamp) ==   date('Y-m-d')) { ?>style="background-color: rgba(95,199,200, 0.2);"<? } ?>><input <?=sizeof($tms)==0 ? "tid='".$record->tid."' pid='".$record->pid."' mid='".$record->mid."' ptskid='".$record->ptskid."' " : "";?> type="text" style="width:62px;display:inline" data-comments="<?=$tms->comments?>"   class="hour input-sm form-control text-left"  data-date="<?php e(date('Y-m-d', $day_time_stamp));?>" data-week="<?php e($current_week);?>" placeholder="hrs" rowid="<?php e($tms->id);?>" value="<? echo $tms->tsd_hours; ?>" <?=$record->status=='disabled' || $record->itstatus=='Submitted' || $record->itstatus=='Delayed'? 'disabled="disabled"' : ''?> />  				
						</td>
                        <?php  
						
						
						endforeach;?>
                        
                       
					  </tr>
					<?php $no++; endforeach;?>
					  <tr id="lastrow">
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
                        <td><strong>Total (hours)</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
						<?php foreach($days_time_stamp as $day_time_stamp):
						if ( $hrs{$day_time_stamp} < 8.5 ) $color =  "#F00" ;
						if ( $hrs{$day_time_stamp} == 8.5 ) $color =  "#339933" ;
						if ( $hrs{$day_time_stamp} > 8.5 ) $color =  "#000" ;
						
						?>
                        <td><span data-date="<?php e(date('Y-m-d', $day_time_stamp));?>" data-week=<?php e($current_week);?> class="total"><strong style="color:<?=$color?>"><? echo number_format($hrs{$day_time_stamp},2);?></strong></span></td>
						<?php $color=""; endforeach;?>
                        <td>&nbsp;</td>
					  </tr>
                    </tbody>
                  </table>
                  </div>
</div>
                
				</section> 
                <input type="submit" name="save" class="btn btn-primary" value="Submit For Approval"  <?=$submitdisabled?> />
                <input type="hidden" name="cmonth" value="<?=date('m-Y', strtotime('first day of this month '.$current_week.' month'))?>" />
               <? if  ($unlockdisabled->status=="lock") { ?>
                <input type="submit" name="rtounlock" class="btn btn-primary" value="Request To Unlock" <? echo $unlockdisabled->status=="lock" ? "disabled='disabled'" : ""?> /><? } ?>
             
               
    
    
			
				<?php echo form_close(); ?>
                
				</div>
                
                
				</div><br /> <? if  ($unlockdisabled->status=="lock") { ?>
                 <div class="alert  alert-error  notification col-md-6">
		<a data-dismiss="alert" class="close" href="#">×</a>
		<div>You have requested to unlock. Please wait for response from finance Team</div>
	</div><? } ?>
                 
	<script>
	
	function commfunc(val,val1,val2){
		
		if ( val2 =="delrow" ) {
	var r = confirm("Confirm Delete ?");
		}
		if ( val2 =="unlock" ){			
		var r = confirm("Confirm Unlock ?");	
		}
if (r == true) {
	
	$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/') ?>/"+val2, { ptskid:val1, tid:val}, function(data){	console.log(data); location.reload();	
	})	
		
}}


	

	
	$(".moreless").click ( function() {
	 if ($(this).hasClass("active")) { 
        $("#moreless").slideDown("slow",function(){});
		
	 } else {  $("#moreless").slideUp("slow",function(){}); }
   
	});

	$(".hour").on("focus",function() {
		$(this).popover('destroy');
		$(".popover").hide();
	
		$(this).popover( { placement: function (context, source) {
        var position = $(source).position();

        if (position.left > 515) {
            return "left";
        }

        if (position.left < 100) {
            return "right";
        }

        if (position.top < 110){
            return "bottom";
        }

        return "top";
    },
	
	 html: true,title:"Add Comments<a href='#' class='btn btn-xs btn-warning pull-right' onclick=$('.popover').hide()><i class='fa fa-times' data-date'close'></i></a><button  class='btn btn-xs btn-success pull-right'><i class='fa fa-check'></i></button>" , content:'<textarea name="comments"  cols="30" rows="2" class="form-control expand">'+$(this).attr("data-comments")+'</textarea>'});
		
	});
	
	
	$('body').on('click', '.popover button', function () {


	var inp = $(this).parent().parent().prev('input');
	var ta = $(this).parent().parent().find("textarea").val();
	var val3 = inp.val();		
	
	if ( val3 < 0.25 ) { alert("hours must be above 0.25") } else {
	
	
	var rowid = inp.attr("rowid");	
	var tid = inp.attr("tid");	
	var pid = inp.attr("pid");
	var mid = inp.attr("mid");
	var ptskid = inp.attr("ptskid");
	var tdate = inp.attr("data-date");
	
	if ( ta !="" ) {
	
	$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/update_hr_com') ?>", { rowno:rowid,hours:val3,taskid:tid,projectid:pid,milestoneid:mid,ptaskid:ptskid,timedate:tdate,comments:ta}, function(data){ $(".popover").hide();
	$(".popover >textarea").val(ta);
	
	inp.attr("data-comments",ta);
	
	console.log("hrs"+val3,"row:"+rowid,"tid:"+tid,"pid:"+pid,"mid:"+mid,"ptskid:"+ptskid,"tsdate:"+tdate,"comments:"+ta); 

	})
	}}

});
	
	
	$(".text-left").on("change" , function() { if ( $(this).val() > 0.25) { 
	
	var rowid = $(this).attr("rowid");	
	var row = $(this);
	var tid = $(this).attr("tid");	
	var pid = $(this).attr("pid");
	var mid = $(this).attr("mid");
	var ptskid = $(this).attr("ptskid");
	var tdate = $(this).attr("data-date");
	var ta = $(this).attr("data-comments");
	
	$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/update_hr_com') ?>", { rowno:rowid,hours:$(this).val(),taskid:tid,projectid:pid,milestoneid:mid,ptaskid:ptskid,timedate:tdate,comments:ta}, function(data){ row.attr("rowid",data); row.attr("data-comments",ta);} )
	
	} 
	});
	
	// pagination
		$('.monthly-chunk label').on('click', function(e){
			
			location.href='<?php e(site_url('/admin/projectmgmt/tasks/timesheets').'/'.$current_week.'/1/');?>'+$(this).data('value');
		}).filter('[data-value=<?php e($monthly_chunk_th);?>]').addClass('active');
		
	//month,week button
		$('div.timesheet-view label').on('click',function(e){
			//console.log(<?=$current_week?>);
			e.preventDefault();
			monthly = $(this).data('value') == 'monthly' ? 1 : 0;
			if(monthly != '<?php e($monthly);?>') {
				location.href='<?php e(site_url('/admin/projectmgmt/tasks/timesheets').'/'.$current_week.'/');?>'+monthly;
			}
		});
		


		
		 $(document).ready(function() {
   // put all your jQuery goodness in here.
    var diff;
  
    $('#datepicker').datepicker().on( "changeDate",function(){ 
	 $('#datepicker').datepicker('hide');
	  console.log( $('#datepicker').val());
	
	});
	
	$("#datesubmit").on("click", function() {
if (  $('#datepicker').val() != "" ) {
		var d = new Date();
	var n = d.getMonth()+1;
	var arr =$('#datepicker').val().split("/");
 diff = arr[1]- n;
	location.href='<?php e(site_url('/admin/projectmgmt/tasks/timesheets').'/');?>'+diff+'/1';
	
	}});
	
		 });
		
		
	
	
	</script>
 