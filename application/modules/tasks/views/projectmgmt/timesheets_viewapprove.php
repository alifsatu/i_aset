<style>
.borderprimary { border:solid 2px rgb(72, 167, 91); padding:5px; }
  .panel .table-striped > thead th.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > thead th.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > tbody > tr > td.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > tbody > tr > td.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > thead th  { padding:2px 2px; text-align:center}
    .panel .table-striped > tbody > tr > td  { padding:2px 2px; text-align:center; vertical-align:middle}
    .panel .table td, .panel .table th { padding:0px;vertical-align:middle }
    .input-xs {padding-left:4px; padding-right:0}
    .goo .table td, .panel .table th { padding:5px;vertical-align:middle }
</style>
<div>
    <section class="panel panel-default">


        <header class="panel-heading"><strong>Timesheet Details </strong>  <button href="#" class="moreless btn btn-xs btn-default pull-right" data-toggle="class:hide">
                <i class="fa fa-compress text"></i>		                  
                <i class="fa fa-expand text-active"></i>		                 
            </button></header>

        <div class="panel-body">

            <div class="row text-sm wrapper" id="moreless">
                <div class="col-md-6 goo">
                    <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     

                        <tr>                    
                            <td><strong>Name:</strong></td>

                            <td><?= $this->auth->display_name_by_id($qq->uid) ?></td>

                        </tr>
                        <tr>                    
                            <td><strong>Employee Id:</strong></td>
                            <td><?php echo $qq->uid ?></td>
                        </tr>
                        <tr>                    
                            <td><strong>Period:</strong></td>
                            <td>
                                <?php
                                if ($qq->period == 1) {
                                    echo "1st Half";
                                }

                                if ($qq->period == 2) {
                                    echo "2nd Half";
                                }
                                ?>
                            </td>
                        </tr>                     
                    </table>

                </div>


                <div class="col-sm-5 goo">
                    <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     

                        <tr>                    
                            <td><strong>Latest Timesheet Submission Date:</strong></td>
                            <td><?php
                                foreach ($subdate as $sd) {

                                    if ($sd->submission_date != '' || $sd->submission_date != null) {
                                        echo date("d-m-Y", strtotime($sd->submission_date)) . "<br>";
                                    }
                                }
                                ?></td>
                        </tr>
                        <tr>                    
                            <td><strong>Latest Timesheet Approval Date:</strong></td>
                            <td><?php
                                foreach ($apprdate as $ad) {
                                    if ($ad->approval_status_action_date != '' || $ad->approval_status_action_date != null) {
                                        echo date("d-m-Y", strtotime($ad->approval_status_action_date)) . " - " . $ad->approval_status_status . "<br>";
                                    }
                                }
                                ?></td>
                        </tr>                        

                    </table>
                </div>
            </div>



            <div class="table-scrollable innerWrapper" style="width:1080px; overflow:auto">  
                <table class="table  table-striped b-t b-light text-sm timesheet-table m-b">
                    <thead>
                    <tr align="center">
                        <th width="10px"></th>
                        <th width="10px">No</th>
                        <th width="10px">Project</th>
                        <th width="150px">Milestone</th>
                        <th width="150px">Task</th>
                            <?php
                            $tms = $this->db->query("select tsd_date as tdate,tsd_date from intg_timesheet_details where tid = '" . $this->uri->segment(5) . "' and status ='enabled' group by tsd_date")->result();
                            $tdate = array();
                            $no = 1;
							
                            foreach ($tms as $t) {
                                $tdate[] = $t->tsd_date;
                                ?>
                                
                                
                                
                                
                                <th><?php e(date('d', strtotime($t->tdate))); ?><br /><?php e(date('D', strtotime($t->tdate))); ?></th>
								
								
								<?php } ?>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $rownum = 1;
                            //SELECT *,itd.id as itid FROM intg_timesheet it,intg_timesheet_details itd where it.deleted = 0 and it.id=itd.tid and it.uid ='295' and itd.tsd_date like ('2016-05%') and itd.status ='enabled' group by tid,pid,mid,ptskid ORDER BY itd.id desc
                            foreach ($timesheet as $record) {

                                $uid = $record->uid;
                                ?>

                                <tr>
                                    <th></th>
                                    <th><?= $no ?></th>
                                    <th width="10px"><?php
                                        $rcprojects = $this->db->query("select project_name from intg_projects where id = " . $record->pid . "")->row();
                                        $rcmilestone = $this->db->query("select * from intg_milestones where milestone_id = " . $record->mid . "")->row();
                                        $rctaskpool = $this->db->query("select task_name from intg_task_pool where id = " . $record->ptskid . "")->row();
                                        ?>
                                         <?= wordwrap($rcprojects->project_name, 20, "<br>\n") ?>
                                    </th>
                                    <th ><span data-toggle="tooltip" title = "<?= $milestonedesc; ?>" ><?= wordwrap($rcmilestone->milestone_name, 30, "<br>\n") ?></span></th>
                                    <th><?= wordwrap($rctaskpool->task_name, 30, "<br>\n") ?></th>

                                    <?php
                                    $rn = 1;
                                    foreach ($tdate as $td) {
                                        
                                        $ts = $this->db->query("select tsd_hours,comments from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "' and tsd_date = '" . $td . "'")->row();
//                                        $ts = $this->db->query("select tsd_hours from intg_timesheet_details where tid = '" . $record->tid . "' and ptskid = '" . $record->ptskid . "' and tsd_date = '" . $td . "' and status ='enabled'")->row();

                                        $hrs{strtotime($td)} = $hrs{strtotime($td)} + $ts->tsd_hours;
                                        ?>
                                        <td valign="middle"  data-container="body" data-toggle="tooltip" data-placement="top" title="<?=$ts->comments?>">
										<span <?= $ts->comments !="" ? 'class="borderprimary"' : ''?>><?php $v{$rn} = $v{$rn} + $ts->tsd_hours; e(empty($ts->tsd_hours) ? 0  : $ts->tsd_hours ); ?></span>
                                        </td>
                                        <?php
                                        $rn++;
                                    }
                                    ?>


                                </tr>
                                <?php 
                                $rn = 0;
                                $no++; 
                                $rownum++;
                            }
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><strong>Total (hours)</strong></td>
                                <td>&nbsp;</td> 
                                <td>&nbsp;</td>  
                                <?php
                                $rn = 1;
                                foreach ($tdate as $td) {
                                    ?>
                                    <td><?= $v{$rn} ?></td>  <?php $rn++;
                                } $rn = 0;
                                ?>



                            </tr>
                            <tr >
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><strong>OT</strong></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <?php
                                $date_format = 'd';
                                $day_format = 'D';
                                $origin = $this->db->query("select origin from intg_users where id = $uid")->row();
                                foreach ($tdate as $td):
                                    if ($origin == "HQ" && date($day_format, strtotime($td)) == "Fri") {
                                        $OTvalid = 8.0;
                                    } else {
                                        $OTvalid = 8.5;
                                    };
                                    if ($origin->origin == "Estate") {
                                        $OTvalid = 8.0;
                                    };
                                    ?>
                                    <td><span><?php
                                            if ($hrs{ strtotime($td)} != 0) {
                                                if ($hrs{strtotime($td)} - $OTvalid > 0) {
                                                    echo "<span style='color:rgb(56, 161, 162)'>" . number_format($hrs{strtotime($td)} - $OTvalid, 2) . "</span>";
                                                } else {
                                                    echo '0.00';
                                                }
                                            }
                                            ?></span></td>
        <?php
    endforeach;
    ?>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>



                <?php
                $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,ba.modified_on,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="41" and bas.approval_status_mrowid = "' . $this->uri->segment(5) . '" ORDER BY bas.id asc');

                $rowreject = $queryappr->row();


                if ($qq->uid != $this->auth->user_id()) {
                    ?>
                    <div class="col-sm-6 goo">
                        <h4><?php echo lang('quote_approvers_hierarchy') ?></h4>
                        <table width='80%' class='table table-bordered' >
                            <tr>
                                <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                                <td><strong><?php echo lang('quote_role') ?></strong></td>
                                <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                                <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                                <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                            </tr>
                            <?php
                            foreach ($queryappr->result() as $rowappr) {

                                switch ($rowappr->approvers_status) {
                                    case "No" : $status = "<span class='badge bg-warning'>&nbsp;Pending&nbsp;</span>";
                                        break;
                                    case "Yes" : $status = "<span class='badge bg-success'>Approved</span>";
                                        break;
                                    case "Reject" : $status = "<span class='badge bg-important'>Rejected&nbsp;</span>";
                                        break;
                                }
                                ?>
                                <tr>
                                    <td><?= $rowappr->display_name ?></td>
                                    <td><?= $rowappr->role_name ?></td>
                                    <td><?php if(($rowappr->approval_status_status == "Yes" && $rowappr->approvers_status == "No") || ($rowappr->approval_status_status == "Reject" && $rowappr->approvers_status == "No"))

                                        {

                                        echo "<span class='badge badge-disabled'>&nbsp;&nbsp;*ANR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                                        } 
                                        else 
                                        { 
                                        echo $status;
                                        }  ?></td>
                                    <td><?= $rowappr->approvers_remarks ?></td>
                                    <td><?php
                                if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                    echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                                }
                                ?></td>
                                </tr>
        <?php }
        ?>
                            <tr>   
                                <td colspan='5' ><font size='2' color='blue'>*ANR : Action Not Required</font></td>

                            </tr>
                        </table>
                    </div>
                    <?php
                    $querym = $this->db->query('SELECT * from intg_approval_status WHERE  approval_status_module_id = "41" and approval_status_mrowid = "' . $this->uri->segment(5) . '"  and  FIND_IN_SET(' . $this->auth->user_id() . ',approval_status_action_by) order by id desc');
                    //echo $this->db->last_query();			
                    $rowm = $querym->row();


                    if ($querym->num_rows() > 0) {
                        ?>
                        <div id="load" class="col-sm-3 goo">
                            <h4><?php echo lang('quote_approver_action') ?></h4>
                            <form id="quickapprove">
                                <table width="80%" class="table table-bordered">
                                    <tr>
                                        <td><?php echo lang('quote_approver_action_status') ?></td>
                                    </tr>
                                    <tr>
                                        <td><div class="btn-group" data-toggle="buttons" style="width:70%">

                                                <label class="btn btn-sm btn-info active" id="label_yes">
                                                    <input type="radio" name="approval_status_status" checked="checked" id="option1"  value="Yes">
                                                    <i class="fa fa-check text-active"></i> <?php echo lang('quote_approver_action_yes') ?> </label>
                                                    <label class="btn btn-sm btn-danger" id="label_no">
                                                    <input type="radio" name="approval_status_status" id="option2" value="Reject">
                                                    <i class="fa fa-check text-active"></i><?php echo lang('quote_approver_action_no') ?> </label>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo lang('quote_approver_remarks') ?></td>
                                    </tr>
                                    <tr>
                                        <td><textarea name="approval_status_remarks" style="border: #ccc solid 1px" cols="50" rows="4" id="asr"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><input name="updateappr" type="button" value="Submit" class="btn btn-success" onclick="updateApprovalstatus('<?= $this->uri->segment(5) ?>', '<?= $rowm->id ?>', '<?= $rowm->rolename ?>', '<?= $rowm->approval_status_mrowid ?>', 'edit', '<?= $this->auth->user_id() ?>', '<?= $qq->final_approvers ?>', '<?= $qq->uid ?>')" data-loading-text="Updating..." /></td>
                                    </tr>
                                </table>

                            </form>
                        </div>
        <?php } ?>

    <?php } ?>
            </div></section>
    </div>
    <script>
        function updateApprovalstatus(val, val2, val3, val4, val5, val6, val8, val7)
        {
            console.log("timesheets_viewapprove");
            var a = $('input[name=approval_status_status]:checked').val();
            var b = $('#asr').val();


            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/updateApprovalstatus') ?>", {status: a, remarks: b, rowid: val2, rolename: val3, docid: val, mrowid: val4, act: val5, userid: val6, finalapprover: val8, initiator: val7}, function (data) { });


            $(document).ajaxStop(function () {
                window.location = '<?php echo base_url('index.php/admin/projectmgmt/tasks/approval'); ?>';
            });

        }
    </script>
    <script>

        $(document).ready(function () {
            $(window).resize(function () {
                $(".table-scrollable").css('width', $(document).width() - 300 + "px");  /* 350 is the width taken by left menu */
                $(".table-scrollable").css('overflow-x', 'scroll');
                $(".table-scrollable").css('white-space', 'nowrap');
            });
            $(window).resize();
        });


        function commfunc(val, val1, val2) {

            if (val2 == "delrow") {
                var r = confirm("Confirm Delete ?");
            }
            if (val2 == "unlock") {
                var r = confirm("Confirm Unlock ?");
            }
            if (r == true) {

                $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/') ?>/" + val2, {ptskid: val1, tid: val}, function (data) {
                    console.log(data);
                    location.reload();
                })

            }
        }





        $(".moreless").click(function () {
            if ($(this).hasClass("active")) {
                $("#moreless").slideDown("slow", function () {});

            } else {
                $("#moreless").slideUp("slow", function () {});
            }

        });

        $(".hour").on("focus", function () {
            $(this).popover('destroy');
            $(".popover").hide();

            $(this).popover({placement: function (context, source) {
                    var position = $(source).position();

                    if (position.left > 515) {
                        return "left";
                    }

                    if (position.left < 100) {
                        return "right";
                    }

                    if (position.top < 110) {
                        return "bottom";
                    }

                    return "top";
                },
                html: true, title: "Add Comments<a href='#' class='btn btn-xs btn-warning pull-right' onclick=$('.popover').hide()><i class='fa fa-times' data-date'close'></i></a><button  class='btn btn-xs btn-success pull-right'><i class='fa fa-check'></i></button>", content: '<textarea name="comments"  cols="30" rows="2" class="form-control expand">' + $(this).attr("data-comments") + '</textarea>'});

        });


        $('body').on('click', '.popover button', function () {
    //        alert("masukk");

            var inp = $(this).parent().parent().prev('input');
            var ta = $(this).parent().parent().find("textarea").val();
            var val3 = inp.val();

            if (val3 < 0.25) {
                alert("hours must be above 0.25")
            } else {


                var rowid = inp.attr("rowid");
                var tid = inp.attr("tid");
                var pid = inp.attr("pid");
                var mid = inp.attr("mid");
                var ptskid = inp.attr("ptskid");
                var tdate = inp.attr("data-date");

                if (ta != "") {

                    $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {rowno: rowid, hours: val3, taskid: tid, projectid: pid, milestoneid: mid, ptaskid: ptskid, timedate: tdate, comments: ta}, function (data) {
                        $(".popover").hide();
                        $(".popover >textarea").val(ta);

                        inp.attr("data-comments", ta);

                        console.log("hrs" + val3, "row:" + rowid, "tid:" + tid, "pid:" + pid, "mid:" + mid, "ptskid:" + ptskid, "tsdate:" + tdate, "comments:" + ta);

                    })
                }
            }

        });


        $(".text-left").on("change", function () {
            if ($(this).val() > 0.25) {

                var rowid = $(this).attr("rowid");
                var row = $(this);
                var tid = $(this).attr("tid");
                var pid = $(this).attr("pid");
                var mid = $(this).attr("mid");
                var ptskid = $(this).attr("ptskid");
                var tdate = $(this).attr("data-date");
                var ta = $(this).attr("data-comments");

                $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {rowno: rowid, hours: $(this).val(), taskid: tid, projectid: pid, milestoneid: mid, ptaskid: ptskid, timedate: tdate, comments: ta}, function (data) {
                    row.attr("rowid", data);
                    row.attr("data-comments", ta);
                })

            } else {
                alert("hours must be above 0.25");
                $(this).val("");
            }
        });

    // pagination
        $('.monthly-chunk label').on('click', function (e) {

            location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/' . $current_week . '/1/'); ?>' + $(this).data('value');
        }).filter('[data-value=<?php e($monthly_chunk_th); ?>]').addClass('active');

    //month,week button //Alif edited
        $('div.timesheet-view label').on('click', function (e) {
            console.log("current week : <?= $current_week ?>");
            e.preventDefault();
    //			monthly = $(this).data('value') == 'monthly' ? 1 : 0;

            if ($(this).data('value') == 'monthly')
                options = 1;
            else if ($(this).data('value') == 'weekly')
                options = 2;

            else if ($(this).data('value') == '1st')
                options = 3;

            else if ($(this).data('value') == '2nd')
                options = 4;
            else
                options = 3

    //			if(monthly != '<?php e($monthly); ?>') {
            if (options != '<?php e($monthly); ?>') {
    //				location.href='<?php // e(site_url('/admin/projectmgmt/tasks/timesheets').'/'.$current_week.'/');   ?>'+monthly;
                location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/' . $current_week . '/'); ?>' + options;
            }
        });




        $(document).ready(function () {
    
	
	
	
            var diff;

            $('#datepicker').datepicker().on("changeDate", function () {
                $('#datepicker').datepicker('hide');
                console.log($('#datepicker').val());

            });

            $("#datesubmit").on("click", function () {
                if ($('#datepicker').val() != "") {
                    var d = new Date();
                    var n = d.getMonth() + 1;
                    var arr = $('#datepicker').val().split("/");
                    diff = arr[1] - n;
                    location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/'); ?>' + diff + '/1';

                }
            });

            $(".total").each(function () {
                var totalrow = <?= $no ?>;
                var dInputId = this.id;
                var putonchecking = false;

                var columnId = dInputId.split("-");

                //do looping through all hrs for the respected column@days
                for (loop = 1; loop < totalrow; loop++) {
    //                        alert(columnId[1] + "-" + loop+" - "+totalrow);
                    var isDisabled = $("#" + columnId[1] + "-" + loop).is(':disabled');
                    if (!isDisabled) {
                        //if hrs not disabled, set putonchecking = true
                        putonchecking = true;
                    } else {
                        //if hrs is disabled
    //                               alert("masuk disabled"+putonchecking);
                    }
                }

                if (putonchecking) {
                    if (parseFloat($(this).text()) < 1) {
                        console.log($(this).text());
                        $("#sfa").attr("disabled", "disabled");
                    }
                }

            });

        });

        function validateMaxVal(maxval, val, id) {

            if (val <= maxval) {
    //            alert("Maximum is "+maxval+" Hours"+val+" ---"+id);
                return true;
            } else {
                alert("Maximum is " + maxval + " Hours" + val + " ---" + id);
                $("#" + id).val("");
                $("#" + id).focus();
                return false;
            }
        }

        $('.checkval').bind('keyup change focusout', function () {
            var dInput = this.value;
            var dInputId = this.id;
            var totalrow = <?= $no ?>;
//    totalrow = totalrow-1;

        var columnId = dInputId.split("-");

        var totalhours = 0;
        for (loop = 1; loop < totalrow; loop++) {
            var v = parseFloat($("#" + columnId[0] + "-" + loop).val());
            if (!isNaN(v))
                totalhours += v;
            console.log(columnId[0] + "-" + loop);

        }

        if (totalhours > 24) {
            alert("Maximum Total Hours 24 Reached");
            $("#" + dInputId).val("");
            $("#" + dInputId).focus();
        }

        console.log(dInput + " , id:" + dInputId + ", totalrow : " + totalrow + ", id : " + columnId[0] + "-" + loop + " totalhours: " + totalhours);

//    alert(dInput+" , id:"+dInputId+", totalrow : "+totalrow);
//    $(".dDimension:contains('" + dInput + "')").css("display","block");
    });


 $('#label_yes, #label_no').click(function(){
   if($(this).hasClass('active')){
       return false;
   }     
});
</script>
