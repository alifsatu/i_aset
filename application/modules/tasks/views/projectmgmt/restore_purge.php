<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($tasks))
{
	$tasks = (array) $tasks;
}
$id = isset($tasks['id']) ? $tasks['id'] : '';

?>
<div class="row">
	<h3>Tasks</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('project_id') ? 'error' : ''; ?>">
				<?php echo form_label('Project Id', 'tasks_project_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_project_id' type='text' name='tasks_project_id' maxlength="45" value="<?php echo set_value('tasks_project_id', isset($tasks['project_id']) ? $tasks['project_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('project_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('task_name') ? 'error' : ''; ?>">
				<?php echo form_label('Task Name', 'tasks_task_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_task_name' type='text' name='tasks_task_name' maxlength="45" value="<?php echo set_value('tasks_task_name', isset($tasks['task_name']) ? $tasks['task_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('task_name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'tasks_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'tasks_description', 'id' => 'tasks_description', 'rows' => '5', 'cols' => '80', 'value' => set_value('tasks_description', isset($tasks['description']) ? $tasks['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('parent_task_id') ? 'error' : ''; ?>">
				<?php echo form_label('Parent Task Id', 'tasks_parent_task_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_parent_task_id' type='text' name='tasks_parent_task_id' maxlength="45" value="<?php echo set_value('tasks_parent_task_id', isset($tasks['parent_task_id']) ? $tasks['parent_task_id'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('parent_task_id'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('progress_percentage') ? 'error' : ''; ?>">
				<?php echo form_label('Progress Percentage', 'tasks_progress_percentage', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_progress_percentage' type='text' name='tasks_progress_percentage' maxlength="45" value="<?php echo set_value('tasks_progress_percentage', isset($tasks['progress_percentage']) ? $tasks['progress_percentage'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('progress_percentage'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('color') ? 'error' : ''; ?>">
				<?php echo form_label('Color', 'tasks_color', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_color' type='text' name='tasks_color' maxlength="45" value="<?php echo set_value('tasks_color', isset($tasks['color']) ? $tasks['color'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('color'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('send_notification') ? 'error' : ''; ?>">
				<?php echo form_label('Send Notification', 'tasks_send_notification', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_send_notification' type='text' name='tasks_send_notification' maxlength="45" value="<?php echo set_value('tasks_send_notification', isset($tasks['send_notification']) ? $tasks['send_notification'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('send_notification'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('estimated_hours') ? 'error' : ''; ?>">
				<?php echo form_label('Estimated Hours', 'tasks_estimated_hours', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_estimated_hours' type='text' name='tasks_estimated_hours' maxlength="45" value="<?php echo set_value('tasks_estimated_hours', isset($tasks['estimated_hours']) ? $tasks['estimated_hours'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('estimated_hours'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('notes') ? 'error' : ''; ?>">
				<?php echo form_label('Notes', 'tasks_notes', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'tasks_notes', 'id' => 'tasks_notes', 'rows' => '5', 'cols' => '80', 'value' => set_value('tasks_notes', isset($tasks['notes']) ? $tasks['notes'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('notes'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('task_tag') ? 'error' : ''; ?>">
				<?php echo form_label('Task Tag', 'tasks_task_tag', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='tasks_task_tag' type='text' name='tasks_task_tag' maxlength="45" value="<?php echo set_value('tasks_task_tag', isset($tasks['task_tag']) ? $tasks['task_tag'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('task_tag'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/content/tasks', lang('tasks_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Tasks.Content.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('tasks_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>