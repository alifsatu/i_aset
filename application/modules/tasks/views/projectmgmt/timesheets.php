<?php
//echo "Monthly : ".$monthly;
//	$monthly = $monthly == 1; 
?>
<style>

    .panel .table-striped > thead th.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > thead th.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > tbody > tr > td.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > tbody > tr > td.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > thead th  { padding:2px 2px; text-align:center}
    .panel .table-striped > tbody > tr > td  { padding:2px 2px; text-align:center; vertical-align:middle}
    .panel .table td, .panel .table th { padding:0px;vertical-align:middle }
    .input-xs {padding-left:4px; padding-right:0}
    .goo .table td, .panel .table th { padding:5px;vertical-align:middle }
	.borderprimary { border:solid 2px rgb(72, 167, 91);}

</style>
<script>
    $(window).load(function () {
        $("#search").keyup(function () {
            var value = this.value.toLowerCase().trim();
            console.log(value);
            $("#tstable tr").each(function (index) {
                if (!index)
                    return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    console.log(not_found);
                    return not_found;
                });
            });
        });
    });//]]> 
</script>
<div>
    <section class="panel panel-default"  >
        <header class="panel-heading"><strong>Timesheet Details </strong>
            <button href="#" class="moreless btn btn-xs btn-default pull-right" data-toggle="class:hide"> <i class="fa fa-compress text"></i> <i class="fa fa-expand text-active"></i> </button>
        </header>
        <div class="row text-sm wrapper" id="moreless">
            <div class="col-md-6 goo">
                <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">
                    <tr>
                        <td><strong>Name:</strong></td>
                        <td><?= $this->auth->display_name_by_id($this->auth->user_id()) ?></td>
                    </tr>
                    <tr>
                        <td><strong>Employee Id:</strong></td>
                        <td><?php echo $this->auth->user_id() ?></td>
                    </tr>
                    <tr>
                        <td><strong>Period:</strong></td>
                        <td><?php
                            $today = date('d');

                            if (!isset($monthly) || $monthly == '' || $monthly == null) {

                                if ($today < 16)
                                    $monthly = 3;
                                else
                                    $monthly = 4;
                            }

                            if ($monthly == 1) {

                                echo "Monthly";
                            }

                            if ($monthly == 2) {

                                echo "Weekly";
                            }

                            if ($monthly == 3) {

                                echo "1st Half";
                                $period = "1";
                            }

                            if ($monthly == 4) {

                                echo "2nd Half";
                                $period = "2";
                            }
                            ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6 goo">
                <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">
                    <tr>
                        <td><strong>Latest Timesheet Submission Date:</strong></td>
                        <td><?php
                            $no=1;
                            foreach ($subdate as $sd) {
                                
                                if ($sd->submission_date != '' || $sd->submission_date != null) {

                                    echo $no.") ".date("d-m-Y", strtotime($sd->submission_date)) . "<br>";
                                }
                                $no++;
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td><strong>Latest Timesheet Approval Date:</strong></td>
                        <td><?php
                            $n = 1;
                            foreach ($apprdate as $ad) {

                                switch ($ad->approval_status_status) {
                                    case "No" :
                                        $status = "<span class='badge btn-warning m-l text-xs'>Pending Approval</span>";
                                        break;
                                    case "Yes" :
                                        $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                                        break;
                                    case "Reject" :
                                        $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                                        break;
                                }


                                if ($ad->approval_status_action_date != '' || $ad->approval_status_action_date != null) {

                                    echo $n . ') ' . date("d-m-Y", strtotime($ad->approval_status_action_date)) . " " . $status . "<br>";
                                }
                                $n++;
                            }
                            ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
    <section class="panel panel-default">
        <header class="panel-heading">
            <div class="col-md-8">
            <?php
            $date_format = 'M jS D';

            $days_time_stamp = array();

            $total_monthly_chunk = 0;

            $todaybutton = 0;

            if ($monthly == 0) {

                if ($today < 16)
                    $monthly = 3;
                else
                    $monthly = 4;
            }

            if ($monthly == 1) { //Monthly
                $current_month = date('m');

                $current_year = date('Y');

                $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));

//                for($i = 1;$i <= $days_number;$i++)
//                {
//                    $days_time_stamp[] = strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month');
//                }

                $current_month_daycount = date('t');

                $plusday = 1;

                for ($i = 1; $i <= $days_number; $i++) {

                    if ($i > $current_month_daycount) {

                        $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $current_month_daycount . ' ' . $current_week . ' month +' . $plusday . " day");

                        //          echo $current_year.'-'.$current_month.'-'.$current_month_daycount.' '.$current_week.' month +'.$plusday." day || ". date($date_format,strtotime($current_year.'-'.$current_month.'-'.$current_month_daycount.' '.$current_week.' month +'.$plusday." day"))."<br>";

                        $plusday++;
                    } else {

                        $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');

                        //          echo $current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month || '. date($date_format,strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'))."<br>";
                    }
                }

                $days_time_stamp_chunks = array_chunk($days_time_stamp, 31);

                $total_monthly_chunk = count($days_time_stamp_chunks);

                $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];

                $todaybutton = 1;
            } else if ($monthly == 2) { //weekly
                $days_time_stamp = array(
                    strtotime('monday this week ' . $current_week . ' week'),
                    strtotime('tuesday this week ' . $current_week . ' week'),
                    strtotime('wednesday this week ' . $current_week . ' week'),
                    strtotime('thursday this week ' . $current_week . ' week'),
                    strtotime('friday this week ' . $current_week . ' week'),
                    strtotime('saturday this week ' . $current_week . ' week'),
                    strtotime('sunday this week ' . $current_week . ' week')
                );

                $todaybutton = 2;
            } else if ($monthly == 3) { //1st Half
                $current_month = date('m');

                $current_year = date('Y');

                $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));

                //		echo "days_number ".$days_number."<br>";

                for ($i = 1; $i <= $days_number; $i++) {

                    $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');

//              echo $current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month || '. date($date_format,strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'))."<br>";
                }

                $days_time_stamp_chunks = array_chunk($days_time_stamp, 15);

                $total_monthly_chunk = count($days_time_stamp_chunks);

                $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];

                if ($today < 16)
                    $todaybutton = 3;
                else
                    $todaybutton = 4;
            }else if ($monthly == 4) { //2nd Half
                $current_month = date('m');

                $current_month_daycount = date('t');

                $current_year = date('Y');

                $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));

                $sechalf = $days_number - 15;

                //		echo "selected month days_number ".$days_number;
                //		echo "current month days_number ".date('t');
                //          echo "second half ".$sechalf;
                //          echo "<br>";

                $plusday = 1;

                for ($i = 16; $i <= $days_number; $i++) {

                    if ($i > $current_month_daycount) {

                        $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $current_month_daycount . ' ' . $current_week . ' month +' . $plusday . " day");

                        //          echo $current_year.'-'.$current_month.'-'.$current_month_daycount.' '.$current_week.' month +'.$plusday." day || ". date($date_format,strtotime($current_year.'-'.$current_month.'-'.$current_month_daycount.' '.$current_week.' month +'.$plusday." day"))."<br>";

                        $plusday++;
                    } else {

                        $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');

                        //          echo $current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month || '. date($date_format,strtotime($current_year.'-'.$current_month.'-'.$i.' '.$current_week.' month'))."<br>";
                    }
                }

                $days_time_stamp_chunks = array_chunk($days_time_stamp, $sechalf);

                $total_monthly_chunk = count($days_time_stamp_chunks);

                $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];

                if ($today > 15)
                    $todaybutton = 4;
                else
                    $todaybutton = 3;
            }

            echo '<b>';

//            if($monthly == 1){
//                                echo 'Full Timesheet of '.date('F', strtotime($current_year.'-'.$current_month.'-1 '.$current_week.' month'));
//                            }
//
//                            if($monthly == 2){
//                                echo 'Current Week of '.date('F', strtotime($current_year.'-'.$current_month.'-1 '.$current_week.' month'));
//                            }
//

            if ($monthly == 3) {

                
                $month = date('n', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                $year = date('Y', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                echo $periodName = '1st Half of ' . date('F', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'))." ".$year;
            }

            if ($monthly == 4) {

                
                $month = date('n', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                $year = date('Y', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                echo $periodName = '2nd Half of ' . date('F', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'))." ".$year;
            }

//            if($monthly)
//					e('Month: '.date('l F jS', strtotime('first day of this month '.$current_week.' month')). ' - '.date('l F jS', strtotime('last day of this month '.$current_week.' month')));s
//            else

            echo '</b>';

//					echo '<b>Week: '. date('l F jS', strtotime('monday this week '.$current_week.' week')). ' - '. date('l F jS', strtotime('today '.$current_week.' week')).'</b>';
            ?>
            <span class="btn-xs">(* disabled inputs are due to old version, please click add entry to add new version)
                <?php $sechalf; ?>
            </span>
            </div>
<!--            <div class="col-md-4">
                <input type="text" id="search" class="input-sm input-xl  form-control" placeholder="search task">
            </div>-->
            <div class="clearfix"></div>
            <!--<a id="excel" href="<?= base_url() ?>index.php/admin/projectmgmt/tasks/export/<?= $this->uri->segment(5) ?>/<?= $this->uri->segment(6) ?>" class="pull-right btn  btn-icon btn-xs"   title="Excel"><span class="fa  fa-file-excel-o" style="color:#093; font-size:18px;"></span></a>-->
        </header>
        <div class="row text-sm wrapper">
            <div class="col-sm-2 m-b-xs">
                
                <div class="btn-group">
                    <?php $this->input->get('users') ? $tusers = "?users=" . $this->input->get('users') : $tusers = "?u="; ?>
                    <?php $tmonth = "&m=" . date('m', strtotime('first day of this month ' . $current_week . ' month')); ?>
                    <?php $tmonthPrev = "&m=" . date('m', strtotime('first day of this month ' . $current_week -1 . ' month')); ?>
                    <?php $tmonthNext = "&m=" . date('m', strtotime('first day of this month ' . $current_week + 1 . ' month')); ?>
                    <?php $tyear = "&y=" . date('Y', strtotime('first day of this month ' . $current_week . ' month')); ?>
                    <?php $tperiod = "&p=" . $period; ?>

                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/' . ($current_week - 1) . '/' . $monthly . $tusers.$tmonthPrev.$tyear.$tperiod)); ?>" class="btn btn-sm btn-default"><i class="fa fa-chevron-left"></i></a>

                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/') . "/0/" . $todaybutton . $tusers.$tmonth.$tyear.$tperiod); ?>" class="btn btn-sm btn-default" <?php // if($todaybutton == $monthly): e('disabled'); endif;                       ?> >Today</a>
                    <?php if ($current_week + 1 < 1) { ?>
                        <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/' . ($current_week + 1) . '/' . $monthly . $tusers.$tmonthNext.$tyear.$tperiod)); ?>" class="btn btn-sm btn-default"><i class="fa fa-chevron-right"></i></a>
                    <?php } ?>
                </div>
                
<!--                <div class="btn-group">
                    <?  $this->input->get('users') ? $tusers = "?users=".$this->input->get('users') : ""; ?>
                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/' . ($current_week - 1) . '/' . $monthly . $tusers)); ?>" class="btn btn-sm btn-default"><i class="fa fa-chevron-left"></i></a> 

<a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/') . "/0/" . $todaybutton . $tusers); ?>" class="btn  btn-sm btn-default" <?php // if($current_week == 0): e('disabled');                                 ?> > 

                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/') . "/0/" . $todaybutton . $tusers); ?>" class="btn  btn-sm btn-default" <?php // if($todaybutton == $monthly): e('disabled'); endif;                                 ?> >Today</a>
                    <?php if ($current_week + 1 < 1) { ?>
                        <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets/' . ($current_week + 1) . '/' . $monthly . $tusers)); ?>" class="btn  btn-sm btn-default"><i class="fa fa-chevron-right"></i></a>
                    <?php } ?>
                </div>-->
            </div>
            <div class="col-sm-2 m-b-xs">
                <div class="btn-group timesheet-view"> 


                    <label class="btn btn-sm btn-default <?php echo $monthly == 3 ? 'active' : ''; ?>" data-value="1st">1st Half</label>
                    <label class="btn btn-sm btn-default <?php echo $monthly == 4 ? 'active' : ''; ?>" data-value="2nd">2nd Half</label>
                </div>
            </div>
            <div class="col-sm-3 m-b-xs">
                <input type="text" class="form-control input input-sm input-xs pull-left" placeholder="Date" id="datepicker"/>
                <input type="button" class="btn btn-sm btn-primary" id="datesubmit"  value="Get It"/>
                <p id="msg">&nbsp;</p>
            </div>
            <div class="col-sm-5">
                <?php //echo implode(",",$allowed_user_ids);  ?>
                <form class="form-inline">
                    <select class="input-sm form-control input-s-sm  selecta " name="users">
                        <option value="0">All Users</option>
                        <?php foreach ($users as $user): if ($this->input->get('users') == $user->id) { ?>
                                <option selected="selected" value="<?php e($user->id); ?>">
                                    <?php e($user->display_name); ?>
                                </option>
                                <?php
                            } else {

                                if (in_array($this->auth->user_id(), $allowed_user_ids)) {

                                    if (in_array($user->id, $allowed_user_ids)) {
                                        ?>
                                        <option value="<?php e($user->id); ?>">
                                            <?php e($user->display_name); ?>
                                        </option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="<?php e($user->id); ?>">
                                        <?php e($user->display_name); ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        <?php endforeach; ?>
                    </select>



                    <input type="submit" name="submit" class="btn btn-sm btn-primary" />
                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/0/4'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></a>
                    <?php if ($disable_add_new_ts == 'true' && $typeworkflow != "none") { ?>
                        <a href="<?= base_url() ?>index.php/admin/projectmgmt/tasks/create_ts/<?= $this->uri->segment(6) ?>/<?= $this->uri->segment(5) ?>" class="btn btn-sm btn-success" style=" color: #FFF"  data-toggle="ajaxModal" ><i class="fa fa-plus"></i></a>
                    <?php } ?>
                    <a id="excel" href="<?= base_url() ?>index.php/admin/projectmgmt/tasks/export/<?= $this->uri->segment(5) ?>/<?= $this->uri->segment(6) ?>/?periodName=<?=$periodName?>" class="btn btn-success pull-right btn-sm"><span class="glyphicon glyphicon-save"></span> Export to Excel</a>
                </form>
            </div>
        </div>
        <?php echo form_open($this->uri->uri_string()); ?>
        <div class="table-scrollable innerWrapper">
            <table class="table table-striped b-t b-light text-sm timesheet-table m-b" id="tstable">
                <thead>
                    <tr align="center">
                        <th width="10px"></th>
                        <th width="10px" data-sort="int" style="cursor: pointer" title="Click to sort by this column"><br>No</th>
                        <th width="150px" id="project_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Project</th>
                        <th width="150px" id="milestone_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Milestone</th>
                        <th width="150px" id="task_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Task</th>
                        <?php
                        $date_format = 'd';
                        $day_format = 'D';

                        foreach ($days_time_stamp as $day_time_stamp):
                            ?>
                            <th width="12px" style="cursor: pointer" title="Click to sort by this column" class="<?php e(in_array(date($day_format, $day_time_stamp), array("Sat", "Sun")) ? date($day_format, $day_time_stamp) : "" ) ?>"
                                data-sort="float"  style="cursor: pointer"><br>
                                <?php e(date($date_format, $day_time_stamp)); ?><br />
                                <?php e(date($day_format, $day_time_stamp)); ?></th>
                            <?php
                            $dateym = $day_time_stamp;
                        endforeach;
                        ?>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $task_ids = array(); ?>
                    <?php
                    $no = 1;

                    $colnum = 1;

                    $rownum = 1;

                    foreach ($timesheet as $record):

                        $task_ids[] = (int) $record->id;
                        $rcprojects = $this->db->query("select project_name,status_nc from intg_projects where id = " . $record->pid . "")->row();
                        $rcmilestone = $this->db->query("select * from intg_milestones where milestone_id = " . $record->mid . "")->row();
                        $rctaskpool = $this->db->query("select task_name from intg_task_pool where id = " . $record->ptskid . "")->row();
                        //check for start date // ALIF EDIT : 18022016  /  Alif modified = change form select project start date to milestone start date
                        $milestone_start_date = $this->db->query("select start_date from intg_milestones where milestone_id = " . $record->mid . "")->row()->start_date;
                    
                    
                        ?>
                    <tr>
                            <td> <?php if (($record->itstatus == "Created" && $record->uid == $this->auth->user_id()) || ($record->final_status == "Reject" && $record->uid == $this->auth->user_id())) { ?>
                                    <a href="javascript:void()" onclick="commfunc('<?= $record->itid ?>', '<?= $record->ptskid ?>', 'delrow', '<?= $record->mid ?>')"><i class="fa fa-times" style="color:#F00; padding:0px 6px 0px 6px"></i></a>
                                    <?php
                                }

                                if ($record->itstatus == "Delayed" && $this->auth->role_id() == 1) {
                                    ?>
                                    <a href="javascript:void()" onclick="commfunc('<?= $record->itid ?>', '<?= $record->ptskid ?>', 'unlock', '')"><i class="fa fa-unlock"></i></a>
                                <?php } ?>
                            </td>
                            <td><?= $no ?></td>
                            <td>
                                <?= wordwrap($rcprojects->project_name, 20, "<br>\n") ?>
                            </td>
                            <td >
                                <?php
                                $milestonedesc = " Start Date- " . format_date($rcmilestone->start_date, "d/m/Y")
                                        . ", End Date- " . format_date($rcmilestone->end_date, "d/m/Y")
                                        . ", Created By- " . get_from_user_table("display_name", "id", $rcmilestone->created_by)
                                        . ", Current Status- " . $rcmilestone->status;
                                
                                echo $rcmilestone->action == 'deactivated' ? "<span class='badge btn-danger m-l text-xs'>Deactivated</span><br>":""; ?>
                                <span data-container="body" data-toggle="tooltip" title = "<?= $milestonedesc; ?>" >
                                    <?= wordwrap($rcmilestone->milestone_name, 20, "<br>\n") ?>
                                </span></td>
                            <td>
                                
                                    <?= wordwrap($rctaskpool->task_name, 30, "<br>\n") ?>
                            </td>
                            <?php
                            $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "'")->result();

                            foreach ($days_time_stamp as $day_time_stamp):

                                $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "' and tsd_date = '" . date('Y-m-d', $day_time_stamp) . "'")->row();

                                $hrs{$day_time_stamp} = $hrs{$day_time_stamp} + $tms->tsd_hours;

                                //Removed by Alif , 1 march 2016, Client want to open all hrs input fields
//                            if ( date('Y-m-d', $day_time_stamp) > date('Y-m-d') )
//                            { 
//                                break;
//                            }
                                ?>
                                <td  class="<?php e(in_array(date($day_format, $day_time_stamp), array("Sat", "Sun")) ? date($day_format, $day_time_stamp) : "" ) ?>" <?php if (  date('Y-m-d', $day_time_stamp) ==   date('Y-m-d')) { ?>style="background-color: rgba(95,199,200, 0.2);"<?php } ?> ><?php
                                // ALIF EDIT : 18022016
                                // var_dump($tms); 
                                ?>
                                <?php // echo 'Row : '.$rownum."-".$day_time_stamp;  ?>
                                <?php // echo 'Project Start Date : '.strtotime($milestone_start_date)." > ".$day_time_stamp;     ?>
                                    <font style="display: none;"><?php echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours; ?></font>
                                     <input 
                                        <?= sizeof($tms) == 0 ? "tid='" . $record->tid . "' pid='" . $record->pid . "' mid='" . $record->mid . "' ptskid='" . $record->ptskid . "' " : ""; ?> 

                                        type="<?php echo $rcmilestone->action == 'deactivated' ? 'hidden':'text'; ?>" 
                                        data-parsley-type="digits"
                                        max="24" min="1" 
                                        uniqueid = <?= $day_time_stamp ?>
                                        id="<?= $day_time_stamp . "-" . $rownum ?>" 
                                        style="width:34px;display:inline;" 
                                        data-comments="<?= $tms->comments ?>"   
                                        class="hour checkval input-xs input-sm form-control text-left totalup<?= $day_time_stamp ?> <?= $tms->comments !="" ? 'borderprimary' : ''?> "
                                        data-date="<?php e(date('Y-m-d', $day_time_stamp)); ?>" 
                                        data-week="<?php e($current_week); ?>" 
                                        rowid="<?php e($tms->id); ?>" 
                                        value="<?php echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours; ?>"
                                        

                                        <?php
                                        $echo = "debug: ";
                                        if ($record->final_status == 'Reject') {



                                            $echo .= "Reject";

//                                   if (strtotime($milestone_start_date) > $day_time_stamp || date("Y-m",strtotime($milestone_start_date)) != date("Y-m",$day_time_stamp)) {
                                            if (strtotime($milestone_start_date) > $day_time_stamp) {

//                                       
                                                if ($rcprojects->status_nc != 'yes') {
                                                    if ($record->itstatus == 'disabled' || $record->itstatus == 'Created' || $record->itstatus == 'Submitted' || $record->itstatus == 'Delayed' || $record->uid != $this->auth->user_id()) {

                                                        echo 'disabled="disabled" title="'.$tms->comments.'"';
                                                    }
                                                }
                                            }
                                        } else {

                                            $echo .= " 1";
//                                    if (strtotime($milestone_start_date) > $day_time_stamp || date("Y-m",strtotime($milestone_start_date)) != date("Y-m",$day_time_stamp)) {
                                            if (strtotime($milestone_start_date) > $day_time_stamp) {
                                                $echo .= " 2";
                                                if ($rcprojects->status_nc != 'yes') {
                                                    $echo .= " 3";
                                                  echo 'disabled="disabled" title="'.$tms->comments.'"';
                                                }
                                            } else {
                                                $echo .= " 4";
//                                        if($rcprojects->status_nc != 'yes'){ //alif commented 12052016
                                                if ($record->status == 'disabled' || $record->itstatus == 'Submitted' || $record->itstatus == 'Delayed' || $record->uid != $this->auth->user_id()) {
                                                    $echo .= " 5";
                                                   echo 'disabled="disabled" title="'.$tms->comments.'"'; //removed as requested by allan on 1 march 2016
                                                }
//                                        }//alif commented 12052016
                                            }
                                        }
                                        ?> /> <?php
                                        if($rcmilestone->action == 'deactivated'){
                                            echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours;
                                        }
                                            //echo "status : ".$echo; ?>
                                
                                
                                </td>
                                <?php endforeach; ?>
                        </tr>
                        <?php
                        $no++;

                        $rownum++;

                    endforeach;
                    ?>
                    <tfoot>
                    <tr id="lastrow">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>Total (hours)</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php
                        foreach ($days_time_stamp as $day_time_stamp):

                            if ($current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                                $OTvalid = 8.0;
                            } else {
                                $OTvalid = 8.5;
                            };
                            if ($current_user->origin == "Estate") {
                                $OTvalid = 8.0;
                            };

                            if ($hrs{$day_time_stamp} < $OTvalid)
                                $color = "#F00";

                            if ($hrs{$day_time_stamp} == $OTvalid)
                                $color = "#339933";

                            if ($hrs{$day_time_stamp} > $OTvalid)
                                $color = "#000";
                            ?>
                            <td><span data-date="<?php e(date('Y-m-d', $day_time_stamp)); ?>" data-week=<?php e($current_week); ?>> <strong style="color:<?= $color ?>" min-hours="<?=number_format($OTvalid, 1)?>" id="tot<?= $day_time_stamp ?>"><?php echo number_format($hrs{$day_time_stamp}, 1); ?> </strong> </span></td>
                            <?php
                            $color = "";

                        endforeach;
                        ?>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>OT</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php
                        foreach ($days_time_stamp as $day_time_stamp):
                            if ($current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                                $OTvalid = 8.0;
                            } else {
                                $OTvalid = 8.5;
                            };
                            if ($current_user->origin == "Estate") {
                                $OTvalid = 8.0;
                            };
                            ?>
                            <td><span><?php
                                    if ($hrs{$day_time_stamp} != 0) {
                                        if ($hrs{$day_time_stamp} - $OTvalid > 0) {
                                            echo "<span style='color:rgb(56, 161, 162)'>" . number_format($hrs{$day_time_stamp} - $OTvalid, 1) . "</span>";
                                        } else {
                                            echo '0.00';
                                        }
                                    }
                                    ?></span>
                                
                                   <?php // echo "<br>OTValid: ".$OTvalid; ?>
                            <?php // echo "<br>Current User: ".$current_user->origin.", hari: ".date($day_format, $day_time_stamp)."<br>"; ?>
                            </td>
                            <?php
                        endforeach;
                        ?>
                        <td>&nbsp;</td>
                    </tr>
                    </tfoot>
                </tbody>
            </table>

        </div>

    </section>
    <input type="hidden" name="period" value="<?= $period ?>" />
    <input type="hidden" name="month"  value="<?= $month ?>" />
    <input type="hidden" name="year"   value="<?= $year ?>" />
    <?php if ( $allow_user == $this->auth->user_id() 
    //  &&   date("Y-m", $dateym) == date("Y-m")
    )  
    { ?>
    <a href="javascript:location.reload(true)" class="btn  btn-info">Calculate Total Hours</a>
    <input type="submit" name="save" class="btn btn-primary" id="sfa" value="Submit For Approval"  <?= $submitdisabled ?> onclick="return confirm('<?php e(js_escape(lang('timesheet_submit_confirm'))); ?>')" />
    <?php } ?>
    <input type="hidden" name="cmonth" value="<?= date('m-Y', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
    <input type="hidden" name="mth_lock" value="<?= date('n', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
    <?php 
    if ( $record->itstatus=="Delayed" ) {  ?>
    <input type="submit" name="rtounlock" class="btn btn-primary" value="Request To Unlock" <? echo $unlockdisabled->status=="lock" ? "disabled='disabled'" : ""?> />
           <?php  }?>
           <?php echo form_close(); ?> </div>
</div>
<br />
<?php if  ($unlockdisabled->status=="lock") { ?>
<div class="alert  alert-error  notification col-md-6"> <a data-dismiss="alert" class="close" href="#">x</a>
    <div>You have requested to unlock. Please wait for response from Finance Team</div>
</div>
<?php } ?>
<script>

    function commfunc(val, val1, val2, mid) {

        if (val2 == "delrow") {
            var r = confirm("Confirm Delete ?");
        }

        if (val2 == "unlock") {
            var r = confirm("Confirm Unlock ?");
        }

        if (r == true) {
            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/') ?>/" + val2, {mid: mid, ptskid: val1, tid: val}, function (data) {
                console.log(data);
                location.reload();
            });
        }
    }

    $(".moreless").click(function () {
        if ($(this).hasClass("active")) {
            $("#moreless").slideDown("slow", function () {});
        } else {
            $("#moreless").slideUp("slow", function () {});
        }
    });

    $(".hour").on("focus", function () {
                $(this).popover('destroy');
        $(".popover").hide();
        $(this).popover({
            placement: function (context, source) {
                var position = $(source).position();
                if (position.left > 515) {
                    return "left";
                }
                if (position.left < 100) {
                    return "right";
                }
                if (position.top < 110) {
                    return "bottom";
                }
                return "top";
            },
            html: true,
            title: "Add Comments<a href='#' class='btn btn-xs btn-warning pull-right' onclick=$('.popover').hide()><i class='fa fa-times' data-date'close'></i></a><button  class='btn btn-xs btn-success pull-right'><i class='fa fa-check'></i></button>",
            content: '<textarea name="comments"  cols="30" rows="2" class="form-control expand">' + $(this).attr("data-comments") + '</textarea><div id="error_popover"></div>'
        });
        
    });

    $('body').on('click', '.popover button', function () {

        var inp = $(this).parent().parent().prev('input');
        var ta = $(this).parent().parent().find("textarea").val();
        var val3 = inp.val();
//        if (val3 > 0.25 || val3 == 0) {
        var rowid = inp.attr("rowid");
        var tid = inp.attr("tid");
        var pid = inp.attr("pid");
        var mid = inp.attr("mid");
        var ptskid = inp.attr("ptskid");
        var tdate = inp.attr("data-date");
//        console.log("hrs" + val3, "row:" + rowid, "tid:" + tid, "pid:" + pid, "mid:" + mid, "ptskid:" + ptskid, "tsdate:" + tdate, "comments:" + ta);

        if (!$.isNumeric(val3)) {
                waitingDialog.show('The hour value must be numeric');
                inp.val("0");
                inp.focus();
                setTimeout(function () {waitingDialog.hide();}, 5000);
                
        }else{
            waitingDialog.show('...updating, wait for a moment please');
            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {
                rowno: rowid,
                hours: val3,
                taskid: tid,
                projectid: pid,
                milestoneid: mid,
                ptaskid: ptskid,
                timedate: tdate,
                comments: ta
            }, function (data) {
                console.log(data);
                if(data.result=="success"){
                    
                    $(".popover >textarea").val(data.comment);
                    inp.attr("data-comments", data.comment);
                    waitingDialog.show("Comment successfully saved.");
                    $(".popover").hide();
                    setTimeout(function () {waitingDialog.hide();}, 1000);
                }else if(data.result=="failed"){
                     $(".popover >textarea").val(data.comment);
                    inp.attr("data-comments", data.comment);
                    waitingDialog.show("Comment did not saved.Please try again, otherwise do contact System Administrator for help");
                    setTimeout(function () {waitingDialog.hide();}, 5000);
                }else{
                    $(".popover >textarea").val(ta);
                    inp.attr("data-comments", ta);
                    inp.attr("rowid",data);
                    waitingDialog.show("Comment successfully saved.");
                    $(".popover").hide();
                    setTimeout(function () {waitingDialog.hide();}, 1000);
                }
            
//            waitingDialog.hide();
                
            }, 'json');
        }
//        } else {
//            alert("hours must be above 0.25");
//        }
    return false;//prevent refresh page
    });

    $('.monthly-chunk label').on('click', function (e) {
        location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/' . $current_week . '/1/'); ?>' + $(this).data('value');
    }).filter('[data-value=<?php e($monthly_chunk_th); ?>]').addClass('active');

//month,week button //Alif edited

    $('div.timesheet-view label').on('click', function (e) {

        console.log("current week : <?= $current_week ?>");
        e.preventDefault();

//			monthly = $(this).data('value') == 'monthly' ? 1 : 0;

        if ($(this).data('value') == 'monthly'){
            options = 1;
        }else if ($(this).data('value') == 'weekly'){
            options = 2;

        }else if ($(this).data('value') == '1st'){
            options = 3;
            period = 1;

        }else if ($(this).data('value') == '2nd'){
            options = 4;
            period = 2;
        }else{
            options = 3
        }
//			if(monthly != '<?php e($monthly); ?>') {
        if (options != '<?php e($monthly); ?>') {

//				location.href='<?php // e(site_url('/admin/projectmgmt/tasks/timesheets').'/'.$current_week.'/');                                  ?>'+monthly;
            location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/' . $current_week . '/'); ?>' + options+"<?=$tusers.$tmonth.$tyear ?>&p="+period;
        }

    });

    $(document).ready(function () {

        $('#datepicker').datepicker({format: "dd/mm/yyyy"})
        $(".hour").each(function () {
            if ($(this).is(':disabled')) {
            } else {
                $("#tot" + $(this).attr("uniqueid")).addClass("total");
            }
        });

        var cnt = 0;
        $(".total").each(function () {
//            console.log("doc ready HrsValue= "+parseFloat($(this).text())+"- OTVALID= <?=$OTvalid?> --> "+$(this).attr('min-hours'));
            if (parseFloat($(this).text()) < $(this).attr('min-hours')) {
                $("#sfa").attr("disabled", "disabled");
                cnt = 1;
            }
        });

        console.log("any zeros:" + cnt);

        if (cnt != 1) {
            $("#sfa").removeAttr("disabled", "disabled");
        }
        cnt = 0;

        var emptyres = "<?= $emptyresults ?>";
        if (emptyres == "0") {
            $("#sfa").attr("disabled", "disabled");
        }
        console.log(emptyres);

        var diff;

        $('#datepicker').datepicker().on("changeDate", function () {
            $('#datepicker').datepicker('hide');
            console.log($('#datepicker').val());
        });

        $("#datesubmit").on("click", function () {

            if ($('#datepicker').val() != "") {
                var date = new Date();
                var currMonth = date.getMonth() + 1;
                var currYear = date.getFullYear();
                var arrSelected = $('#datepicker').val().split("/");
                
                var selectedDay = arrSelected[0];
                var selectedMonth = arrSelected[1];
                var selectedYear = arrSelected[2];
                
                if(selectedYear == currYear){
                    diff = selectedMonth - currMonth;
                }
                
                if(selectedYear < currYear){
                    var yearDif = currYear - selectedYear;
                    diff = (selectedMonth - currMonth) - (12*yearDif);
                }
                
//                console.log("selectedDay "+selectedDay);
//                console.log("selectedMonth "+selectedMonth);
//                console.log("selectedYear "+selectedYear);
//                console.log("currMonth "+currMonth);
//                console.log("currYear "+currYear);
//                console.log("diff "+diff);
                var period = 0;
                if (selectedDay < 16) {
                    period = 3;
                } else {
                    period = 4;
                }
                location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets') . '/'); ?>' + diff + '/' + period;
            }

        });


        $('.checkval').bind(' change ', function () {
            var dInput = this.value;
            var dInputId = this.id;
            var totalrow = <?= $no ?>;
            var columnId = dInputId.split("-");

            var totalhours = 0;
            for (loop = 1; loop < totalrow; loop++) {
                var v = parseFloat($("#" + columnId[0] + "-" + loop).val());
                if (!isNaN(v)) {
                    totalhours += v;
                }
            }
            //set total hours
            $("#tot" + $(this).attr("uniqueid")).text(parseFloat(totalhours).toFixed(2));

            //change color
            
            if (parseFloat(totalhours).toFixed(1) < $("#tot" + $(this).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(this).attr("uniqueid")).css('color', '#F00');
                console.log(parseFloat(totalhours).toFixed(1) +" < "+ $("#tot" + $(this).attr("uniqueid")).attr('min-hours')+" Color: #F00 red");
            }
            if (parseFloat(totalhours).toFixed(1) == $("#tot" + $(this).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(this).attr("uniqueid")).css('color', '#339933');
                console.log(parseFloat(totalhours).toFixed(1) +" == "+ $("#tot" + $(this).attr("uniqueid")).attr('min-hours')+" Color: #339933 green");
            }
            if (parseFloat(totalhours).toFixed(1) > $("#tot" + $(this).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(this).attr("uniqueid")).css('color', '#000');
                console.log(parseFloat(totalhours).toFixed(1) +" > "+ $("#tot" + $(this).attr("uniqueid")).attr('min-hours')+" Color: #000 black");
            }
            
            if (!$.isNumeric($(this).val())) {
                waitingDialog.show('The hour value must be numeric');
                $(this).val("0");
                $(this).focus();
                setTimeout(function () {waitingDialog.hide();}, 1000);
                
        }else{

                if (totalhours > 24) {
                    alert("Maximum Total Hours 24 Reached");
                    $("#" + dInputId).val("");
                    // $("#" + dInputId).focus();
                } else {

                    var rowid = $(this).attr("rowid");
                    var row = $(this);
                    var tid = $(this).attr("tid");
                    var pid = $(this).attr("pid");
                    var mid = $(this).attr("mid");
                    var ptskid = $(this).attr("ptskid");
                    var tdate = $(this).attr("data-date");
                    var ta = $(this).attr("data-comments");
                    var cnt = 0;

                    waitingDialog.show('...updating, wait for a moment please');
                    $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {
                        rowno: rowid,
                        hours: $(this).val(),
                        taskid: tid,
                        projectid: pid,
                        milestoneid: mid,
                        ptaskid: ptskid,
                        timedate: tdate,
                        comments: ta},
                            function (data) {
                                console.log(data);
                                if(data.result=="success"){
                                    waitingDialog.show("Data successfully saved 1.");
                                    setTimeout(function () {waitingDialog.hide();}, 1000);
                                }else if(data.result=="failed"){
                                    waitingDialog.show("The hour did not saved.Please try again, otherwise do contact System Administrator for help");
                                    setTimeout(function () {waitingDialog.hide();}, 5000);
                                }else{
                                    waitingDialog.show("Data successfully saved 2.");
                                    console.log("hadehh  "+data);
                                    $("#" + dInputId).attr("rowid",data);
                                    setTimeout(function () {waitingDialog.hide();}, 1000);
                                }


                            }, 'json');
                }
            }

            var cnt = 0;
            $(".total").each(function () {
//            console.log("checkval HrsValue= "+parseFloat($(this).text())+"- OTVALID= <?=$OTvalid?>");
                if (parseFloat($(this).text()) < $(this).attr('min-hours')) {
                    $("#sfa").attr("disabled", "disabled");
                    cnt = 1;
                }
            });

            console.log("any zeros:" + cnt);
            if (cnt != 1) {
                $("#sfa").removeAttr("disabled", "disabled");
            }
        });
        
    });


    function validateMaxVal(maxval, val, id) {

        if (val <= maxval) {
            return true;
        } else {
            alert("Maximum is " + maxval + " Hours" + val + " ---" + id);
            $("#" + id).val("");
            $("#" + id).focus();
            return false;
        }
    }


    function checkTotal() {

        $(".total").each(function () {

            var totalrow = <?= $rownum ?>;
            var dInputId = this.id;
            var putonchecking = false;
            var columnId = dInputId.split("-");

            for (loop = 1; loop < totalrow; loop++) {
                var isDisabled = $("#" + columnId[1] + "-" + loop).is(':disabled');
                if (!isDisabled) {
                    //if hrs not disabled, set putonchecking = true
                    putonchecking = true;

                } else {
                    //if hrs is disabled
                    alert("masuk disabled" + putonchecking);
                }

            }

            if (putonchecking) {
                $(".total").each(function () {
//                    console.log("checkTotal HrsValue= "+parseFloat($(this).text())+"- OTVALID= <?=$OTvalid?>");
                    if (parseFloat($(this).text()) < $(this).attr('min-hours')) {
                        $("#sfa").attr("disabled", "disabled");
                        cnt = 1;
                    }
                });

                console.log("any zeros:" + cnt);

                if (cnt != 1) {
                    $("#sfa").removeAttr("disabled", "disabled");
                }
            }
        });
    }


</script> 
