<?php
$statusText = "No Status";
if ($_POST['status'] == "No") {
    $statusText = "Pending";
}
if ($_POST['status'] == "Yes") {
    $statusText = "Approved";
}
if ($_POST['status'] == "Reject") {
    $statusText = "Rejected";
}
?>
<div>
    <div id="inner1"><?php
//print_r($_POST);

        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $queryapprover = $this->db->query('SELECT approval_status_action_by,rolename FROM intg_approval_status WHERE approval_status_module_id ="41" and approval_status_mrowid = "' . $_POST['mrowid'] . '"  ORDER BY id asc')->result();


        $val = NULL;


        foreach ($queryapprover as $v) {
            if ($val == 1) {
                $nextapprover = $v->rolename;
                $nextapprover_id = $v->work_flow_approvers;
                break;
            }


            if ($v->rolename == $role_name) {
                $val = 1;
            }
        }

        $last = end($queryapprover);


        $queryinitiator = $this->db->query('SELECT id,role_id,email,display_name FROM intg_users  WHERE id = "' . $_POST['initiator'] . '"');
        $rowinitiator = $queryinitiator->row();


        switch ($_POST['status']) {
            case "Yes" : $status = "Approved";
                break;
            case "No" : $status = "Pending";
                break;
            case "Reject" : $status = "Rejected";
                break;
        }

        $data = array('approval_status_status' => $_POST['status'], 'approval_status_action_date' => date('Y-m-d h:i:s'),);


        if ($_POST['status'] == "Reject") {
            $this->db->where('approval_status_mrowid', $_POST['mrowid']);
            $this->db->update('intg_approval_status', $data);
            $this->db->query('UPDATE intg_timesheet SET final_status = "Reject", status="Created"  WHERE id = "' . $_POST['docid'] . '"');
        } else {
            $this->db->where('id', $_POST['rowid']);
            $this->db->update('intg_approval_status', $data);
        }

        $data2 = array(
            'approvers_status' => $_POST['status'],
            'approvers_remarks' => $_POST['remarks'],
            'approvers_approve_date' => date('Y-m-d h:i:s')
        );

        $this->db->where('approvers_appstatrowid', $_POST['rowid']);
        $this->db->where('approvers_approver', $this->auth->user_id());
        $this->db->update('intg_approvers', $data2);

        if ($this->auth->user_id() == $_POST['finalapprover']) {

            $this->db->query('UPDATE intg_timesheet SET final_status = "' . $_POST['status'] . '" WHERE id = "' . $_POST['docid'] . '" AND FIND_IN_SET ("' . $this->auth->user_id() . '",final_approvers)');
        }

        $queryappr = $this->db->query('SELECT bas.approval_status_status,ba.approvers_status,ba.approvers_approve_date,ba.approvers_remarks,bu.username,bu.display_name,br.role_name FROM  intg_approvers ba,intg_approval_status bas, intg_users bu , intg_roles br WHERE  ba.approvers_approver = bu.id  AND  ba.approvers_appstatrowid	= bas.id AND br.role_id=bu.role_id and bas.approval_status_module_id="41" and bas.approval_status_mrowid = "' . $_POST['mrowid'] . '" ORDER BY bas.id asc')->result();

        if ($_POST['act'] != "edit") {

            if ($_POST['status'] == "Reject") {
                $btntype = "btn-danger";
                $iconclass = "times";
            }


            if ($_POST['status'] == "Yes") {
                $btntype = "btn-success";
                $iconclass = "check";
            }
            ?>
            <span id="animationSandbox<?= $_POST['mrowid'] ?>" style="display: block;" class="flipInX animated"> <a href="#" id="p<?= $_POST['mrowid'] ?>" data-toggle="popover" data-html="true" data-placement="left" data-trigger="hover"  data-original-title="Approvers Hierarchy" 
                                                                                                                    data-content="<div class='scrollable' style='height:auto;width:500px'><table width='100%' class='table table-condensed'>
                                                                                                                    <tr>   
                                                                                                                    <td><strong><?php echo lang('quote_approved_by') ?></strong></td>
                                                                                                                    <td><strong><?php echo lang('quote_role') ?></strong></td>
                                                                                                                    <td><strong><?php echo lang('quote_approver_status') ?></strong></td>
                                                                                                                    <td><strong><?php echo lang('quote_approver_remarks') ?></strong></td>
                                                                                                                    <td><strong><?php echo lang('quote_approver_datetime') ?></strong></td>
                                                                                                                    </tr><?php
                                                                                                                    foreach ($queryappr as $rowappr) {

                                                                                                                        switch ($rowappr->approvers_status) {
                                                                                                                            case 'No' : $status = "<span class='badge btn-warning'>Pending</span>";
                                                                                                                                break;
                                                                                                                            case "Yes" : $status = "<span class='badge btn-primary'>Approved</span>";
                                                                                                                                break;
                                                                                                                            case "Reject" : $status = "<span class='badge btn-important'>Rejected</span>";
                                                                                                                                break;
                                                                                                                        }
                                                                                                                        ?>

                                                                                                                        <tr>   
                                                                                                                        <td><?= $rowappr->display_name ?></td>
                                                                                                                        <td><?= $rowappr->role_name ?></td>
                                                                                                                        <td><?= $status ?></td>
                                                                                                                        <td><?= $rowappr->approvers_remarks ?></td>
                                                                                                                        <td><?php
                                                                                                                        if ($rowappr->approvers_approve_date != "0000-00-00 00:00:00") {
                                                                                                                            echo date('d-m-y h:i:s', strtotime($rowappr->approvers_approve_date));
                                                                                                                        }
                                                                                                                        ?></td>

                                                                                                                        </tr><?php } ?></table>" class="btn <?= $btntype ?>"><i class="fa fa-<?= $iconclass ?>"></i> </a>
            </span>





            <script>
                $('#p' +<?= $_POST['mrowid'] ?>).popover('destroy');

                $('#p' +<?= $_POST['mrowid'] ?>).hover(function () {
                    $('#p' +<?= $_POST['mrowid'] ?>).popover('show');
                });

            </script><?php
        }
        ?>

        <!----------------Email code----->
        <?php
        if ($_POST['status'] == "Yes") {
            $fapp = $_POST['finalapprover'];
            $f_app = explode(",", $fapp);
            if ($this->auth->user_id() == $_POST['finalapprover'] || in_array($this->auth->user_id(), $f_app)) {
//if ( $role_name == $last->rolename )

                $this->load->library('emailer/emailer');


                $ver = $this->db->query('SELECT parent_Rid,version_no,initiator,mth,yr FROM intg_timesheet WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
                $ver_i = $ver->row();
                if ($ver_i->parent_Rid == "0") {
                    $row_id = $_POST['docid'];
                } else {
                    $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                }


                //COMMENTED THE CODE BELOW TO PREVENT SENDING MAIL
                $j = $ver_i->mth . '/' . $ver_i->yr;
                $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);
                $this->emailer->Subject = 'Timesheet ' . $j . ' has been reviewed';
                $this->emailer->Body = $this->load->view('_emails/timesheet_status', array(
                    'item_type' => 'Timesheet',
                    'item_name' => $j,
                    'assignee_name' => $rowinitiator->display_name,
                    'creator_name' => $creator->display_name,
                    'status' => $_POST['status']), TRUE);

                $this->emailer->mail();
                $this->load->model('notifications/notifications_model', null, true);
                $this->notifications_model->insert(array(
                    'user_id' => $rowinitiator->id,
                    'content' => ' Timesheet ' . $j . ' has been reviewed, ' . $statusText,
                    'title' => ' Timesheet ' . $j . ' was ' . $statusText,
                    'link' => site_url('admin/projectmgmt/tasks/approval'),
                    'class' => 'fa-briefcase',
                    'module' => 'ProjectMgmt',
                    'type' => 'info'
                ));
            } else {
                $approver = $nextapprover;
                $user_iids = (explode(",", $nextapprover_id));
                $size = sizeof($user_iids);
                for ($i = 0; $i < $size; $i++) {
                    $apdet = $this->db->query("SELECT id,display_name,email FROM intg_users WHERE id = '" . $user_iids[$i] . "'");
                    $rowapp = $apdet->row();

                    /* echo $nextapprover_id;
                      echo $this->db->last_query();
                      echo "approver:".$approver."</br>";
                      echo "email:".$rowapp->email."</br>";
                      exit; */

                    $this->load->library('emailer/emailer');
                    $ver = $this->db->query('SELECT parent_Rid,version_no,initiator,mth,yr FROM intg_timesheet WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
                    $ver_i = $ver->row();
                    $j = $ver_i->mth . "/" . $ver_i->yr;
                    $creator = $this->db->query("SELECT display_name FROM intg_users WHERE id = '" . $ver_i->initiator . "'")->row();
                    if ($ver_i->parent_Rid == "0") {
                        $row_id = $_POST['docid'];
                    } else {
                        $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
                    }
                    $this->emailer->addAddress($rowapp->email, $rowapp->display_name);
                    $this->emailer->Subject = 'Timesheet ' . $j . ' is ready for approval';
                    $this->emailer->Body = $this->load->view('_emails/project_app', array('item_type' => 'timesheet', 'item_name' => $j, 'assignee_name' => $rowapp->display_name, 'creator_name' => $creator->display_name), TRUE);


                    $this->emailer->mail();
                    $this->load->model('notifications/notifications_model', null, true);
                    $this->notifications_model->insert(array(
                        'user_id' => $rowapp->id,
                        'content' => ' Timesheet ' . $j . ' is ready for approval',
                        'title' => ' Timesheet ' . $j . '  is ready for approval ',
                        'link' => site_url('admin/projectmgmt/tasks/approval'),
                        'class' => 'fa-briefcase',
                        'module' => 'ProjectMgmt',
                        'type' => 'info'
                    ));
                }
            }
        }

        if ($_POST['status'] == "Reject") {


            $this->load->library('emailer/emailer');
            $ver = $this->db->query('SELECT parent_Rid,version_no,initiator,mth,yr FROM intg_timesheet WHERE  id = "' . $_POST['docid'] . '"  ORDER BY id asc');
            $ver_i = $ver->row();
            if ($ver_i->parent_Rid == "0") {
                $row_id = $_POST['docid'];
            } else {
                $row_id = $ver_i->parent_Rid . "." . $ver_i->version_no;
            }
            $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);

            //COMMENTED THE CODE BELOW TO PREVENT SENDING MAIL
            $j = $ver_i->mth . '/' . $ver_i->yr;
            $this->emailer->addAddress($rowinitiator->email, $rowinitiator->display_name);
            $this->emailer->Subject = 'Timesheet ' . $j . ' has been reviewed';
            $this->emailer->Body = $this->load->view('_emails/timesheet_status', array(
                'item_type' => 'Timesheet',
                'item_name' => $j,
                'assignee_name' => $rowinitiator->display_name,
                'creator_name' => $creator->display_name,
                'status' => $_POST['status']), TRUE);

            $this->emailer->mail();

            $this->load->model('notifications/notifications_model', null, true);
            $this->notifications_model->insert(array(
            'user_id' => $rowinitiator->id,
            'content' => ' Timesheet '.$j .' has been reviewed, ' . $statusText,
            'title' => ' Timesheet '.$j .' was ' . $statusText,
            'link' => site_url('admin/projectmgmt/tasks/approval'),
            'class' => 'fa-briefcase',
            'module' => 'ProjectMgmt',
            'type' => 'info'
            ));
        }
        ?>
        <!----------------Email code----->

    </div>
    <div id="inner2">&nbsp;</div>