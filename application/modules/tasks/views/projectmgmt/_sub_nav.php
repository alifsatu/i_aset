<ul class="nav nav-pills">

	
   
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Timesheet<b class="caret"></b></a>
    <ul class="dropdown-menu text-left">
    <?php
    
    $today = date('d');
    if ($today < 16)
        $period = 3;
    else
        $period = 4;
    
    
    if ( $this->auth->role_id() == 17 ) { ?>
    <li <?php echo $this->uri->segment(4) == 'timesheets_cso' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/timesheets_cso/0')."/".$period ?>" id="list"><i class="fa fa-clock-o m-r"></i>TimeSheet For CSO</a></li>
    <? }else{ ?>
    <li <?php echo $this->uri->segment(4) == 'timesheets' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/timesheets/0')."/".$period ?>" id="list"><i class="fa fa-clock-o m-r"></i>TimeSheet</a></li>
    <? } ?>
    
    <li <?php echo $this->uri->segment(4) == 'export_import_excel' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/excelei/tasks/export_import_excel') ?>" id="list"><i class="fa fa-file-excel-o m-r"></i>Export/ Import Excel</a></li>   
     <!--<li <?php // echo $this->uri->segment(4) == 'export_timesheet' ? 'class="active"' : '' ?>><a href="<?php // echo site_url(SITE_AREA .'/excelei/tasks/export_timesheet') ?>" id="list"><i class="fa fa-file-excel-o m-r"></i>Export Timesheet</a></li>-->   
    </ul></li>
    
     <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Approvals<b class="caret"></b></a>
    <ul class="dropdown-menu text-left">
	<li <?php echo $this->uri->segment(4) == 'approval' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/approval') ?>" id="list">Approval</a></li>   
    
        <?php if(has_permission("Tasks.Projectmgmt.LockUnlockView")){ ?>
    <li <?php echo $this->uri->segment(4) == 'lock_unlock' ? 'class="active"' : '' ?>><a href="<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/lock_unlock') ?>"   id="list">Lock/ Unlock</a></li>
        <?php } ?>
    </ul></li>

</ul>



