
<?php
$num_columns = 8;

$can_delete	= $this->auth->has_permission('Tasks.Projectmgmt.Delete');

$can_edit		= $this->auth->has_permission('Tasks.Projectmgmt.Edit');

$has_records	= isset($records) && is_array($records) && count($records);
?>
<div class="row">
	<section class="panel panel-default">
		<div class="panel-body">
			<?php echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
			<div class="row">
				<div class="col-md-6"> 
					<h4><? echo lang('tasks')?></h4>
				</div>
				<div class="col-md-3">
					<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">
						<option><? echo lang('tasks_allfields')?></option>
<option value="task_name"><? echo lang('task_name')?></option>
<option value="project_name"><? echo lang('projects_name')?></option>
<!--
<option value="task_start_date">Start Date (DD-MM-YYYY) >=</option>
<option value="task_end_date">End Date (DD-MM-YYYY) <=</option>
-->
<option value="progress_percentage"><? echo lang('tasks_progress')?> %</option>								
					</select>
				</div>
				<div class="col-md-3">
					<div class="input-group">
						<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('sapfvalue')?>">
						<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('sapfield')?>"/>
						<span class="input-group-btn">
							<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
							<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
						</span>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>       

    <div class="table-responsive">
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					
					<th><a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks?sort_by=task_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'task_name') ? 'desc' : 'asc'); ?>'>
                   <? echo lang('task_name')?></a></th>
					
					<th><a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks?sort_by=project_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_name') ? 'desc' : 'asc'); ?>'>
                   <? echo lang('projects_name')?></a></th>
					
					<th><a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks?sort_by=task_start_date&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'task_start_date') ? 'desc' : 'asc'); ?>'>
                   <? echo lang('tasks_start_date')?></a></th>

                   <th><a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks?sort_by=task_end_date&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'task_end_date') ? 'desc' : 'asc'); ?>'>
                  <? echo lang('tasks_end_date')?></a></th>
				   
				   <th><? echo lang('tasks_assignees')?></th>

                   <th><a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks?sort_by=progress_percentage&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'progress_percentage') ? 'desc' : 'asc'); ?>'>
                   <? echo lang('tasks_progress')?> %</a></th>
				   
				   <th></th>

				</tr>

			</thead>

			<?php if ($has_records) : ?>

			<tfoot>

				<?php if ($can_delete) : ?>

				<tr>

					<td colspan="<?php echo $num_columns; ?>">

						<?php echo lang('bf_with_selected'); ?>

						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('tasks_delete_confirm'))); ?>')" />

					</td>

				</tr>

				<?php endif; ?>

			</tfoot>

			<?php endif; ?>

			<tbody>

				<?php

				if ($has_records) : $no = 1;

					foreach ($records as $record) :
//print_r($records);
				?>

				<tr>

				<?php if ($can_delete) : ?>

					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>

				<?php endif;?>

				<?php if ($can_edit) : ?>

					<td><?php echo anchor(SITE_AREA . '/projectmgmt/tasks/edit/' . $record->id, '<i class="fa fa-edit icon"></i>' .  $record->task_name); ?></td>

				<?php else : ?>

					<td><?php e($record->task_name); ?></td>

				<?php endif; ?>

					<td><a href="#" class="projectDetail" data-toggle="modal" data-id="<?php e($record->project_id)?>" data-project_name="<?php e($record->project_name) ?>"><u><?php e($record->project_name) ?></u></a></td>

					<td><?php e(date('d-m-Y H:i', strtotime($record->task_start_date))) ?></td>

					<td><?php e(date('d-m-Y H:i', strtotime($record->task_end_date))) ?></td>
					<td valign="centre">
					<?php 
						$assign = $this->db->query('SELECT * FROM intg_assigned_to WHERE item_id='.$record->id.' AND  item_type="task" ');
$num = $assign->num_rows();
foreach($assign->result() as $h)
{


$dis_name = $this->db->query('SELECT display_name FROM intg_users WHERE id='.$h->user_id.'  ')->result();
foreach($dis_name as $g)
{
if($num > 1)
{

echo $g->display_name.",";
}
else
{
echo $g->display_name;
}

}
}
					?>
					<?php //$record->assigned_to ? e(implode(', ', array_map(function($assignee){ return $assignee->display_name;}, $record->assigned_to))) : ''?></td>

					<td align="centre">
					
					<?php
					//$a = $record->progress_percentage."%";
					?><span align="centre">
					<!--<div class="progress progress-xs m-t-sm">
                          <div class="progress-bar progress-bar-info" data-toggle="tooltip" data-original-title="<?=$a?>" style="width: 100%"></div>
						  
                        </div>-->
						
						
                
                      <div class="inline">
                        <div class="easypiechart" data-percent="<?=$record->progress_percentage."%"?>" data-animate="2000" data-line-width="3" data-loop="false" data-size="40">
                          <span ><b><font size="1"><?=$record->progress_percentage ? $record->progress_percentage : '0'?>%</font></b></span>
                         
                        </div>
                      </div>
					 
                  <!--  </div>-->
						
						
						<?php //e($record->progress_percentage) ?>
					</span>
						</td>
						
					
					<td><a href="#" class="taskDetail" data-toggle="modal" data-task_name="<?=$record->task_name?>" data-id="<?=$record->id?>" data-project_name="<?=$record->project_name?>"><i class="fa fa-eye icon"></i></a></td>

				</tr>

				<?php $no++;

					endforeach;

				else:

				?>

				<tr>

					<td colspan="<?php echo $num_columns; ?>"><? echo lang('tasks_records')?></td>

				</tr>

				<?php endif; ?>

			</tbody>

		</table>

	<?php echo form_close(); ?>
	</section>
	<footer class="panel-footer">
		<div class="row">
			<div class="col-sm-4 hidden-xs"></div>
			<div class="col-sm-4 text-center">
				<small class="text-muted inline m-t-sm m-b-sm"><? echo lang('tasks_showing')?> <?=$offset+1?> - <? echo $rowcount+$offset?> <? echo lang('tasks_of')?> <?  echo $total; ?></small>
			</div>
			<div class="col-sm-4 text-right text-center-xs"><?php  echo $this->pagination->create_links(); ?></div>
		</div>
	</footer>
    </div>

</div>
<script type="text/javascript">
	var modalHtml = '<div class="modal fade"><div class="modal-dialog"><div class="modal-content">'
		+'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title"></h4></div>'
		+'<div class="modal-body"></div><div class="modal-footer"></div>'
		+'</div></div></div>';
		
	$('.taskDetail').on('click', function(e){
		$modal = $(modalHtml).modal();
		$('body').append($modal);
		$modal.find('.modal-title').text('Task Details');
		id = $(this).data('id');
		$.get("<?=base_url('index.php/admin/projectmgmt/tasks/all')?>/"+id, function(data){
			data = data[0];console.log(data);
			var htmlBody = '<table class="table">';
					htmlBody += '<thead><tr><td><strong><? echo lang('projects_name')?></strong></td><td>'+data.project.project_name+'</td></tr></thead><tbody>';
					htmlBody += '<tr><td><strong><? echo lang('created_by')?></td><td>'+data.project.creator.display_name+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('task_name')?></td><td>'+data.task_name+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_desc')?></td><td>'+data.description+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('Weightage')?></td><td>'+data.weightage_percentage+' %</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_start_date')?></td><td>'+data.task_start_date+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_end_date')?></td><td>'+data.task_end_date+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('created_on')?></strong></td><td>'+data.created_on+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_assigned_to')?></td><td>'+ $.map(data.assigned_to, function(e){return e.display_name;}).join(', ') +'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('Tags')?></td><td>'+data.tags+'</td></tr>';
					htmlBody += '</tbody></table>';
			$modal.find('.modal-body').html(htmlBody);
			},'json');
	});
	
	$('.projectDetail').on('click', function(e){
		$modal = $(modalHtml).modal();
		$('body').append($modal);
		$modal.find('.modal-title').text('Project Details');
		id = $(this).data('id');
		$.get("<?=base_url('index.php/admin/projectmgmt/projects/all')?>/"+id, function(data){
			data = data[0];
			var htmlBody = '<table class="table">';
		htmlBody += '<thead><tr><td><strong><? echo lang('projects_name')?></strong></td><td>'+data.project_name+'</td></tr></thead><tbody>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_desc')?></strong></td><td>'+data.description+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('projects_client')?></strong></td><td>'+data.client.client_name+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('projects_status')?></strong></td><td>'+data.status+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('created_by')?></strong></td><td>'+data.creator.display_name+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_assigned_to')?></strong></td><td>'+ $.map(data.assigned_to, function(e){return e.display_name;}).join(', ') +'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_start_date')?></strong></td><td>'+ data.project_start_date+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('tasks_end_date')?></strong></td><td>'+ data.project_end_date +'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('created_on')?></strong></td><td>'+data.created_on+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('projects_cost')?></strong></td><td>'+data.cost_currency+' '+data.project_cost+'</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('overall_progress')?></strong></td><td>'+ data.overall_progress * 100 +'%</td></tr>';
					htmlBody += '<tr><td><strong><? echo lang('Tags')?></strong></td><td>'+data.tags+'</td></tr>';
					htmlBody += '</tbody></table>';
			$modal.find('.modal-body').html(htmlBody);
		}, 'json');
	});
</script>