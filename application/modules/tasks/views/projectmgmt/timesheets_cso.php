<style>
    .panel .table-striped > thead th.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > thead th.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > tbody > tr > td.Sat  { background-color: rgba(253, 221, 218, 0.7)}
    .panel .table-striped > tbody > tr > td.Sun  { background-color: rgb(253, 221, 218 )}
    .panel .table-striped > thead th  { padding:2px 2px; text-align:center}
    .panel .table-striped > tbody > tr > td  { padding:2px 2px; text-align:center; vertical-align:middle}
    .panel .table td, .panel .table th { padding:0px;vertical-align:middle }
    .input-xs {padding-left:4px; padding-right:0}
    .goo .table td, .panel .table th { padding:5px;vertical-align:middle }
    .massupd { padding:2px; width:30px }
.borderprimary { border:solid 2px rgb(72, 167, 91);}

</style>
<script>
    $(window).load(function () {
        $("#search").keyup(function () {
            var value = this.value.toLowerCase().trim();

            $("#tstable tr").each(function (index) {
                if (!index)
                    return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });
    });//]]> 
</script>
<div>
    <section class="panel panel-default"  > 
        <header class="panel-heading"><strong>CSO Timesheet Details </strong>  <button href="#" class="moreless btn btn-xs btn-default pull-right" data-toggle="class:hide">
                <i class="fa fa-compress text"></i>		                  
                <i class="fa fa-expand text-active"></i>		                 
            </button></header>

        <div class="row text-sm wrapper" id="moreless">
            <div class="col-sm-6 goo">
                <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     

                    <tr>                    
                        <td><strong>Name:</strong></td>
                        <td><?= $this->auth->display_name_by_id($this->auth->user_id()) ?></td>
                    </tr>
                    <tr>                    
                        <td><strong>Employee Id:</strong></td>
                        <td><?php echo $this->auth->user_id() ?></td>
                    </tr>
                    <tr>                    
                        <td><strong>Period:</strong></td>
                        <td>
                            <?php
                            $today = date('d');
                            
                            if (!isset($monthly) || $monthly == '' || $monthly == null) {
                                if ($today < 16)
                                    $monthly = 3;
                                else
                                    $monthly = 4;
                            }


                            if ($monthly == 1) {
                                echo "Monthly";
                            }

                            if ($monthly == 2) {
                                echo "Weekly";
                            }

                            if ($monthly == 3) {
                                echo "1st Half";
                                $period = "1";
                            }

                            if ($monthly == 4) {
                                echo "2nd Half";
                                $period = "2";
                            }
                            ?>
                        </td>
                    </tr>                     
                </table>

            </div>


            <div class="col-sm-6 goo">

                <table class="table" border="1" style=" border: #F0F0F0;  border-collapse:collapse">                     

                    <tr>                    
                        <td><strong>Latest Timesheet Submission Date:</strong></td>
                        <td><?php
                        $no = 1;
                            foreach ($subdate as $sd) {

                                if ($sd->submission_date != '' || $sd->submission_date != null) {
                                    echo $no.") ".date("d-m-Y", strtotime($sd->submission_date)) . "<br>";
                                }
                                $no++;
                            }
                            ?></td>
                    </tr>
                    <tr>                    
                        <td><strong>Latest Timesheet Approval Date:</strong></td>
                        <td><?php
                        if($NoApprovalNeeded=="1"){
                            echo "<span class='badge btn-primary m-l text-xs'>No Approval Needed</span>";
                        }else{
                        
                            $n = 1;
                            foreach ($apprdate as $ad) {

                                switch ($ad->approval_status_status) {
                                    case "No" :
                                        $status = "<span class='badge btn-warning m-l text-xs'>Pending Approval</span>";
                                        break;
                                    case "Yes" :
                                        $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                                        break;
                                    case "Reject" :
                                        $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                                        break;
                                }

                                if ($ad->approval_status_action_date != '' || $ad->approval_status_action_date != null) {
                                    echo $n.") ".date("d-m-Y", strtotime($ad->approval_status_action_date)) . " - " . $status . "<br>";
                                }
                                $n++;
                            }
                        }
                            ?></td>
                    </tr>                        

                </table>
            </div>
        </div>
    </section>
    <section class="panel panel-default">
        <header class="panel-heading">
            <div class="col-md-8">
                <?php
                $date_format = 'M jS D';
                $days_time_stamp = array();
                $total_monthly_chunk = 0;

                $todaybutton = 0;

                if ($monthly == 0) {
                    if ($today < 16)
                        $monthly = 3;
                    else
                        $monthly = 4;
                }

                if ($monthly == 1) { //Monthly
                    $current_month = date('m');
                    $current_year = date('Y');
                    $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));


                    $current_month_daycount = date('t');
                    $plusday = 1;
                    for ($i = 1; $i <= $days_number; $i++) {
                        if ($i > $current_month_daycount) {

                            $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $current_month_daycount . ' ' . $current_week . ' month +' . $plusday . " day");

                            $plusday++;
                        } else {
                            $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');
                        }
                    }

                    $days_time_stamp_chunks = array_chunk($days_time_stamp, 31);
                    $total_monthly_chunk = count($days_time_stamp_chunks);
                    $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];
                    $todaybutton = 1;
                } else if ($monthly == 2) { //weekly
                    $days_time_stamp = array(
                        strtotime('monday this week ' . $current_week . ' week'),
                        strtotime('tuesday this week ' . $current_week . ' week'),
                        strtotime('wednesday this week ' . $current_week . ' week'),
                        strtotime('thursday this week ' . $current_week . ' week'),
                        strtotime('friday this week ' . $current_week . ' week'),
                        strtotime('saturday this week ' . $current_week . ' week'),
                        strtotime('sunday this week ' . $current_week . ' week')
                    );
                    $todaybutton = 2;
                } else if ($monthly == 3) { //1st Half
                    $current_month = date('m');
                    $current_year = date('Y');
                    $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));

                    for ($i = 1; $i <= $days_number; $i++) {
                        $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');
                    }
                    $days_time_stamp_chunks = array_chunk($days_time_stamp, 15);
                    $total_monthly_chunk = count($days_time_stamp_chunks);
                    $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];

                    if ($today < 16)
                        $todaybutton = 3;
                    else
                        $todaybutton = 4;
                }else if ($monthly == 4) { //2nd Half
                    $current_month = date('m');
                    $current_month_daycount = date('t');
                    $current_year = date('Y');
                    $days_number = date('t', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                    $sechalf = $days_number - 15;

                    $plusday = 1;
                    for ($i = 16; $i <= $days_number; $i++) {
                        if ($i > $current_month_daycount) {

                            $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $current_month_daycount . ' ' . $current_week . ' month +' . $plusday . " day");

                            $plusday++;
                        } else {
                            $days_time_stamp[] = strtotime($current_year . '-' . $current_month . '-' . $i . ' ' . $current_week . ' month');
                        }
                    }

                    $days_time_stamp_chunks = array_chunk($days_time_stamp, $sechalf);
                    $total_monthly_chunk = count($days_time_stamp_chunks);
                    $days_time_stamp = $days_time_stamp_chunks[$monthly_chunk_th - 1];

                    if ($today > 15)
                        $todaybutton = 4;
                    else
                        $todaybutton = 3;
                }

                echo '<b>';
                if ($monthly == 3) {
                     $year = date('Y', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                    echo '1st Half of ' . date('F', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'))." ".$year; 
                }

                if ($monthly == 4) {
                     $year = date('Y', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'));
                    echo '2nd Half of ' . date('F', strtotime($current_year . '-' . $current_month . '-1 ' . $current_week . ' month'))." ".$year; 
                }



                echo '</b>';
                ?> <span class="btn-xs">(* disabled inputs are due to old version, please click add entry to add new version)<?php $sechalf; ?>
                </span></div>
            <div class="col-md-4">
                <input type="text" id="search" class="input-sm input-xl  form-control" placeholder="search task">
            </div>
            <div class="clearfix"></div>

        </header>

        <div class="row text-sm wrapper">

            <div class="col-sm-2 m-b-xs">
                <div class="btn-group">
                    <?php $this->input->get('users') ? $tusers = "?users=" . $this->input->get('users') : $tusers = "?u="; ?>
                    <?php $tmonth = "&m=" . date('m', strtotime('first day of this month ' . $current_week . ' month')); ?>
                    <?php $tyear = "&y=" . date('Y', strtotime('first day of this month ' . $current_week . ' month')); ?>
                    <?php $tperiod = "&p=" . $period; ?>

                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso/' . ($current_week - 1) . '/' . $monthly . $tusers.$tmonth.$tyear.$tperiod)); ?>" class="btn btn-sm btn-default"><i class="fa fa-chevron-left"></i></a>

                    <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso/') . "/0/" . $todaybutton . $tusers.$tmonth.$tyear.$tperiod); ?>" class="btn btn-sm btn-default" <?php // if($todaybutton == $monthly): e('disabled'); endif;                       ?> >Today</a>
                    <?php if ($current_week + 1 < 1) { ?>
                        <a href="<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso/' . ($current_week + 1) . '/' . $monthly . $tusers.$tmonth.$tyear.$tperiod)); ?>" class="btn btn-sm btn-default"><i class="fa fa-chevron-right"></i></a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-2 m-b-xs">
                <div class="btn-group timesheet-view">

                    <label class="btn btn-sm btn-default <?php echo $monthly == 3 ? 'active' : ''; ?>" data-value="1st">1st Half</label>
                    <label class="btn btn-sm btn-default <?php echo $monthly == 4 ? 'active' : ''; ?>" data-value="2nd">2nd Half</label>
                </div>

            </div>
            <div class="col-sm-3 m-b-xs">
                <input type="text" class="form-control input input-sm input-xs pull-left" placeholder="Date" id="datepicker"/>
                <input type="button" class="btn btn-sm btn-primary" id="datesubmit"  value="Get It"/>
                 <p id="msg">&nbsp;</p>
            </div>


            <div class="col-sm-5">



                <form class="form-inline">

                    <select class="input-sm form-control input-s-sm  selecta " name="users">

                        <option value="0">All Users</option>
                        <?php foreach ($users as $user): if ($this->input->get('users') == $user->id) { ?>
                                <option selected="selected" value="<?php e($user->id); ?>"><?php e($user->display_name); ?></option><?php
                            } else {
                                ?><option value="<?php e($user->id); ?>"><?php e($user->display_name); ?></option><?php }
                            ?>
                        <?php endforeach; ?>
                    </select> 

<!--<select class="input-sm form-control input-s-sm  selecta " name="projects">
                            <option value="0">All Projects</option>
                    <?php /* foreach($projects as $project): if ( $this->input->get('projects')==$project->id ) { ?>
                      <option selected="selected" value="<?php e($project->id); ?>"><?php e($project->project_name); ?></option><?php
                      } else {
                      if ( in_array($this->auth->user_id(), $allowed_user_ids))
                      {
                      if ( in_array($project->initiator, $allowed_user_ids)) {
                      ?>

                      <option value="<?php e($project->id); ?>"><?php e($project->project_name); ?></option><?php } } else {  ?><option value="<?php e($project->id); ?>"><?php e($project->project_name); ?></option><?php }} ?>
                      <?php endforeach; */ ?>
</select>--> 

                    <input type="submit" name="submit" class="btn btn-sm btn-primary" />
                    <a href="<?= base_url() ?>index.php/admin/projectmgmt/tasks/timesheets_cso/0/3" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></a>
                    <?php if ($disable_add_new_ts == 'true') { ?>

                        <a href="<?= base_url() ?>index.php/admin/projectmgmt/tasks/create_ts/<?= $this->uri->segment(6) ?>/<?= $this->uri->segment(5) ?>" class="btn btn-sm btn-success" style=" color: #FFF"  data-toggle="ajaxModal" ><i class="fa fa-plus"></i></a><?php } ?>
                </form>
            </div>


        </div>

        <?php echo form_open($this->uri->uri_string()); ?>  



        <div class="table-scrollable innerWrapper">               
            <table class="table table-striped b-t b-light text-sm timesheet-table m-b" id="tstable">
                <thead>
                    <tr align="center">
                        <th class="headcol" style></th>
                        <th class="headcol" data-sort="int" style="cursor: pointer" title="Click to sort by this column"><br>No</th>
                        <th class="headcol" id="project_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Project</th>
                        <th class="headcol" id="milestone_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Milestone</th>
                        <th class="headcol" id="task_header" data-sort="string"  style="cursor: pointer" title="Click to sort by this column"><br>Task</th>
                        <?php
                        $date_format = 'd';
                        $day_format = 'D';
                        foreach ($days_time_stamp as $day_time_stamp):
                            ?>
                            <th class="hide<?php e($day_time_stamp); ?> <?php e(in_array(date($day_format, $day_time_stamp), array("Sat", "Sun")) ? date($day_format, $day_time_stamp) : "" ) ?>">
                                <div align="center">
                                    <a href="#" class="btn btn-sm btn-info massupd" data-toggle="popover" data-html="true" data-placement="left" data-content='<input name="clicked_date"  class="cd" type="hidden" value="<?php e(date('Y-m-d', $day_time_stamp)); ?>"><input name="hours" class="hr" autocomplete="off" placeholder="hours" value="8.5" type="text"> <input type="button" class="csoupd" name="cso" value="Update" identity="<?php e($day_time_stamp); ?>" /> <input type="button" class="reset" name="reset" value="Reset" identity="<?php e($day_time_stamp); ?>" />'   title="" data-original-title="<a href='#'  class='btn btn-xs  close pull-right' data-dismiss='popover'>×</a>CSO hours distribution"><?php e(date($date_format, $day_time_stamp)); ?>
                                        <br /><?php e(date($day_format, $day_time_stamp)); ?>
                                    </a>
                                </div>
                            </th>
                            <?php
                            $dateym = $day_time_stamp;
                        endforeach;
                        ?><th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $task_ids = array(); ?>
                    <?php
                    $no = 1;
                    $colnum = 1;
                    $rownum = 1;
                    foreach ($timesheet as $record) {

                        $proj_start_date = $this->db->query("select start_date from intg_milestones where milestone_id = " . $record->mid . "")->row()->start_date;
                        $rcprojects = $this->db->query("select project_name,status_nc from intg_projects where id = " . $record->pid . "")->row();
                        $rcmilestone = $this->db->query("select * from intg_milestones where milestone_id = " . $record->mid . "")->row();
                        $rctaskpool = $this->db->query("select task_name from intg_task_pool where id = " . $record->ptskid . "")->row();
                        //check for start date // ALIF EDIT : 18022016  /  Alif modified = change form select project start date to milestone start date
                        $milestone_date = $this->db->query("select start_date,end_date from intg_milestones where milestone_id = " . $record->mid . "")->row();
                        
                        ?>
                        <?php $task_ids[] = (int) $record->id; ?>
                        <tr>
                            <td class="headcol"> 
                                <?php if ($record->itstatus == "Created" && $record->ituid == $this->auth->user_id() && $record->itdstatus == "enabled") { ?>

                                                <!--           FOR MULTIPLE DELETE, GOT ISSUE, ALIF DISABLED                     <input type="checkbox" name="itid[]" value="<?= $record->itid ?>" title="Multiple Delete" >
                                                 <input type="hidden" name="ptskid[]" value="<?= $record->ptskid ?>" >
                                                 <input type="hidden" name="mid[]" value="<?= $record->mid ?>" >-->

                                    <a href="javascript:void()" onclick="commfunc('<?= $record->itid ?>', '<?= $record->ptskid ?>', 'delrow', 'delrow', '<?= $record->mid ?>')" title="Single Delete"><i class="fa fa-times" style="color:#F00"></i></a>
                                <?php }
                                if ($record->itstatus == "Delayed" && $this->auth->role_id() == 1) { ?>
                                    <a href="javascript:void()" onclick="commfunc('<?= $record->itid ?>', '<?= $record->ptskid ?>', 'unlock', '', '')"><i class="fa fa-unlock"></i></a><?php } ?>                         
                            </td>
                            <td class="headcol"><?= $no ?></td>
                            <td class="headcol"><span class="text-info">
                                    <?= wordwrap($rcprojects->project_name, 20, "<br>\n") ?></span>
                            </td>
                            <td  class="headcol">
                                <?php
                                $milestonedesc = " Start Date - " . date("d/m/Y", strtotime($rcmilestone->start_date))
                                        . ", End Date - " . date("d/m/Y", strtotime($rcmilestone->end_date))
                                        . ", Created By - " . get_from_user_table("display_name", "id", $rcmilestone->created_by)
                                        . ", Current Status - " . $rcmilestone->status;
                                
                                echo $rcmilestone->action == 'deactivated' ? "<span class='badge btn-danger m-l text-xs'>Deactivated</span><br>":""; ?>
                                <span class="text-info" data-container="body" data-toggle="tooltip" title = "<?= $milestonedesc; ?>" ><?= wordwrap($rcmilestone->milestone_name, 20, "<br>\n") ?></span></td>
                            <td class="headcol" title="<?php e($rctaskpool->task_name); ?>"><span class="text-info"><?= wordwrap($rctaskpool->task_name, 30, "<br>\n") ?></span></td>

                            <?php
                            $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "'")->result();
                            
                            foreach ($days_time_stamp as $day_time_stamp) {

                                $tms = $this->db->query("select * from intg_timesheet_details where tid = '" . $record->tid . "' and  pid =  '" . $record->pid . "'and  mid = '" . $record->mid . "' and  ptskid = '" . $record->ptskid . "' and tsd_date = '" . date('Y-m-d', $day_time_stamp) . "'")->row();

                                //					

                                $hrs{$day_time_stamp} = $hrs{$day_time_stamp} + $tms->tsd_hours;

                                $hourclass = $day_time_stamp;

                                if ($record->final_status == 'Reject') {
                                    $echo = "Reject";
                                    if (strtotime($milestone_date->start_date) > $day_time_stamp || strtotime($milestone_date->end_date) < $day_time_stamp || date("Y-m", strtotime($proj_start_date)) != date("Y-m", $day_time_stamp)) {

                                        if ($rcprojects->status_nc != 'yes') {
                                            if ($record->itdstatus == 'disabled' || $record->itstatus == 'Created' || $record->itstatus == 'Submitted' || $record->itstatus == 'Delayed' || $record->uid != $this->auth->user_id()) {

                                                 $disenable = 'disabled=disabled';
												 $title = 'title='.$tms->comments;
												
                                                $hourclass = "";
                                            }
                                        }
                                    }
                                } else if ($record->final_status == 'Yes') { //Alif, Quick fix
                                 $disenable = 'disabled=disabled';
								 $title = 'title='.$tms->comments;							
                                    $hourclass = "";
                                } else {

                                    $echo = "Else ";
                                    if (strtotime($milestone_date->start_date) > $day_time_stamp || strtotime($milestone_date->end_date) < $day_time_stamp) {
                                        $echo .= " 1 ";
                                        if ($rcprojects->status_nc != 'yes') {
                                            $echo .= " 1.1 ";
                                          $disenable = 'disabled=disabled';
											 $title = 'title='.$tms->comments;			
                                            $hourclass = "";
                                        }
                                    } else {
                                        $echo .= " 2 ";
//                                        if ($rcprojects->status_nc != 'yes') {
                                        $echo .= " 2.1 ";
                                        if ($record->itdstatus == 'disabled' || $record->itstatus == 'Submitted' || $record->itstatus == 'Delayed' || $record->ituid != $this->auth->user_id()) {
                                            $echo .= " 2.2 ";
                                          $disenable = 'disabled=disabled';
										   $title = 'title='.$tms->comments;
                                            $hourclass = "";
                                        }
//                                        }
                                    }
                                }
//                                echo "<br>".$echo."<br>";
                                ?>
                                <td  class = <?php e(in_array(date($day_format, $day_time_stamp), array("Sat", "Sun")) ? date($day_format, $day_time_stamp) : "" ) ?> "<?php e($hideclass) ?>" <?php if (date('Y-m-d', $day_time_stamp) == date('Y-m-d')) { ?>style="background-color: rgba(95,199,200, 0.2);"<?php } ?> >
                                    <font style="display: none;"><?php echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours; ?></font>
                                    <input 
                                    <?= sizeof($tms) == 0 ? "tid='" . $record->tid . "' pid='" . $record->pid . "' mid='" . $record->mid . "' ptskid='" . $record->ptskid . "' " : ""; ?> 
                                        type="<?php echo $rcmilestone->action == 'deactivated' ? 'hidden':'text'; ?>" 
                                        uniqueid = <?= $day_time_stamp ?>
                                        data-parsley-type="digits" 
                                        max="24" min="1" 
                                        id="<?= $day_time_stamp . "-" . $rownum ?>" 
                                        style="width:40px;display:inline;text-align:center;" 
                                        data-comments="<?= $tms->comments ?>"   
                                        class="hour checkval input-xs input-sm form-control text-left hr<?= $hourclass ?> <?= $tms->comments !="" ? 'borderprimary' : ''?>"  
                                        data-date="<?php e(date('Y-m-d', $day_time_stamp)); ?>" 
                                        data-week="<?php e($current_week); ?>"  
                                        rowid="<?php e($tms->id); ?>" 
                                        value="<?php echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours; ?>"  autocomplete="off"

                                        <?php e($disenable); if ( $disenable !="") { ?>
                                        
                                      title="<?php e($tms->comments); ?>"
                            <?php } ?>
                                        />  		
                                   <?php if($rcmilestone->action == 'deactivated'){
                                            echo empty($tms->tsd_hours) ? 0 : $tms->tsd_hours;
                                        } ?>

                                </td>
                                <?php
                                $hideclass = "";
                                $disenable = "";
                            }
                            ?>


                        </tr>
                        <?php
                        $no++;
                        $rownum++;
                    }
                    ?>
                <tfoot>
                    <tr id="lastrow">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><strong>Total (hours)</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php
                        foreach ($days_time_stamp as $day_time_stamp):

                            if ($current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                                $OTvalid = 8.0;
                            } else {
                                $OTvalid = 8.5;
                            };

                            if (number_format($hrs{$day_time_stamp}, 1) < $OTvalid) {
                                $color = "#F00";
                            }
                            if (number_format($hrs{$day_time_stamp}, 1) == $OTvalid) {
                                $color = "#339933";
                            }
                            if (number_format($hrs{$day_time_stamp}, 1) > $OTvalid) {
                                $color = "#000";
                            }
                            ?>
                            <td class ="hide<?php e($day_time_stamp) ?>" >
                                <span data-date="<?php e(date('Y-m-d', $day_time_stamp)); ?>" data-week=<?php e($current_week); ?>>
                                    <strong style="color:<?= $color ?>" min-hours="<?=number_format($OTvalid, 1)?>"  id="tot<?= $day_time_stamp ?>"><?php echo number_format($hrs{$day_time_stamp}, 1); ?></strong>
                                </span>
                            </td>
                            <?php
                            $color = "";

                        endforeach;
                        ?>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <!--FOR MULTIPLE DELETE, GOT ISSUE, ALIF DISABLED  <input type="submit" name="purge" class="btn btn-danger" value="Delete Task(s)" onclick="return confirm('<?php echo lang('tasks_delete_confirm'); ?>')">-->
                        </td>
                        <td>&nbsp;</td>
                        <td><strong>OT</strong></td>
                        <td>&nbsp;</td>

                        <td> <?php
                            foreach ($days_time_stamp as $day_time_stamp):
                                if ($current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
                                    $OTvalid = 8.0;
                                } else {
                                    $OTvalid = 8.5;
                                };
                                ?>
                            <td><span><?php
                                    if ($hrs{$day_time_stamp} != 0) {
                                        if ($hrs{$day_time_stamp} - $OTvalid > 0) {
                                            echo "<span style='color:rgb(56, 161, 162)'>" . number_format($hrs{$day_time_stamp} - $OTvalid, 1) . "</span>";
                                        } else {
                                            echo number_format($hrs{$day_time_stamp} - $OTvalid, 1);
                                        }
                                    }
                                    ?></span></td>
                            <?php
                        endforeach;
                        ?></td>
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
                </tbody>
            </table>
        </div>


    </section> 

    <?php //echo date("d-m-Y",$dateym); if (date("Y-m", $dateym) == date("Y-m") ) {   ?>
    <a href="javascript:location.reload(true)" class="btn  btn-info">Calculate Total Hours</a>
    <input type="submit" name="save" class="btn btn-primary" id="sfa" value="Submit For Approval"  <?= $submitdisabled ?> onclick="return confirm('<?php e(js_escape(lang('timesheet_submit_confirm'))); ?>')" /><?php //}                       ?>
    <input type="hidden" name="cmonth" value="<?= date('m-Y', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
    <input type="hidden" name="month" value="<?= date('m', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
    <input type="hidden" name="year" value="<?= date('Y', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
    <input type="hidden" name="period" value="<?=$period?>" />
    <input type="hidden" name="mth_lock" value="<?= date('n', strtotime('first day of this month ' . $current_week . ' month')) ?>" />
 
<?php 
    if ( $record->itstatus=="Delayed" ) {  ?>
    <input type="submit" name="rtounlock" class="btn btn-primary" value="Request To Unlock" <?php echo $unlockdisabled->status=="lock" ? "disabled='disabled'" : ""?> />
           <?php  }?>



    <?php echo form_close(); ?>

</div>


</div><br /> <?php if ($unlockdisabled->status == "lock") { ?>
    <div class="alert  alert-error  notification col-md-6">
        <a data-dismiss="alert" class="close" href="#">×</a>
        <div>You have requested to unlock. Please wait for response from finance Team</div>
    </div><?php } ?>

<script>




    function commfunc(val, val1, val2, val3, mid) {

        if (val2 == "delrow") {

            var r = confirm("Confirm Delete ?");

        }

        if (val2 == "unlock") {

            var r = confirm("Confirm Unlock ?");

        }

        if (r == true) {

            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/') ?>/" + val2, {mid: mid, ptskid: val1, tid: val}, function (data) {

                console.log(data);

                location.reload();

            })

        }

    }

    $('body').on('click', '.popover button', function () {

        var inp = $(this).parent().parent().prev('input');
        var ta = $(this).parent().parent().find("textarea").val();
        var val3 = inp.val();
//console.log("val3"+val3);

        var rowid = inp.attr("rowid");
        var tid = inp.attr("tid");
        var pid = inp.attr("pid");
        var mid = inp.attr("mid");
        var ptskid = inp.attr("ptskid");
        var tdate = inp.attr("data-date");

        if (ta != "") {

            $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {rowno: rowid, hours: val3, taskid: tid, projectid: pid, milestoneid: mid, ptaskid: ptskid, timedate: tdate, comments: ta}, function (data) {
                $(".popover").hide();
                $(".popover >textarea").val(ta);

                inp.attr("data-comments", ta);

                console.log("WOOOYhrs" + val3, "row:" + rowid, "tid:" + tid, "pid:" + pid, "mid:" + mid, "ptskid:" + ptskid, "tsdate:" + tdate, "comments:" + ta);

            })
        }


    });

     $( document ).ajaxStart(function() {
                waitingDialog.show('...updating, wait for a moment please');
            });
                
    $(document).ajaxStop(function () {
         waitingDialog.hide();
     });
           
    $(document).on("click", ".popover .csoupd", function () {
    
    
        var a = $(this), bal, zero, distr;

        var tstamp = a.attr("identity");
        var tothr = 0;
        $(".hr" + tstamp).each(function () {

            var st = parseFloat($(this).val());
            if (!isNaN(st)) {
                tothr += st;
            }

        });
        bal = Math.abs(a.parent().find('.hr').val() - tothr);
        zero = $(".hr" + tstamp).filter(function () {
            return +this.value == 0;
        }).length;
        distr = bal / zero;
        console.log("bal" + bal, "zero" + zero);

        $(".hr" + tstamp).each(function () {
            if ($(this).val() == 0) {
                $(this).trigger("keypress").val(function (i, val)
                {
                    return parseFloat(distr).toFixed(4);
                });
//                $(this).trigger("change");
                saveHours(this);
            }
        });

        $(this).popover('destroy');
        $(".popover").hide();
    });

    $(document).on("click", ".popover .reset", function () {
        var a = $(this);

        var tstamp = a.attr("identity");
        var tothr = 0;
        $(".hr" + tstamp).each(function () {
            $(this).trigger("keypress") // you can trigger keypress like this if you need to..
                    .val(function (i, val) {
                        return 0
                    });
//            $(this).trigger("change");
                saveHours(this);
        });

        $(this).popover('destroy');
        $(".popover").hide();
    });





// pagination
    $('.monthly-chunk label').on('click', function (e) {

        location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso') . '/' . $current_week . '/1/'); ?>' + $(this).data('value');
    }).filter('[data-value=<?php e($monthly_chunk_th); ?>]').addClass('active');

//month,week button //Alif edited
    $('div.timesheet-view label').on('click', function (e) {
//        alert(<?php // echo $todaybutton;                       ?>);

        e.preventDefault();
//			monthly = $(this).data('value') == 'monthly' ? 1 : 0;

        if ($(this).data('value') == 'monthly'){
            options = 1;
        }else if ($(this).data('value') == 'weekly'){
            options = 2;

        }else if ($(this).data('value') == '1st'){
            options = 3;
            period = 1;

        }else if ($(this).data('value') == '2nd'){
            options = 4;
            period = 2;
        }else{
            options = 3
        }

//			if(monthly != '<?php e($monthly); ?>') {
        if (options != '<?php e($monthly); ?>') {
//				location.href='<?php // e(site_url('/admin/projectmgmt/tasks/timesheets').'/'.$current_week.'/');                       ?>'+monthly;
            location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso') . '/' . $current_week . '/'); ?>' + options+"<?=$tusers.$tmonth.$tyear ?>&p="+period;
        }
    });

    $(document).ready(function () {

        $(".hour").each(function () {

            if ($(this).is(':disabled')) {
            } else {
                $("#tot" + $(this).attr("uniqueid")).addClass("total");
            }

        });

        var diff;

        $('#datepicker').datepicker({format: "dd/mm/yyyy"}).on("changeDate", function () {
            $('#datepicker').datepicker('hide');


        });

        $("#datesubmit").on("click", function () {

            if ($('#datepicker').val() != "") {
                var date = new Date();
                var currMonth = date.getMonth() + 1;
                var currYear = date.getFullYear();
                var arrSelected = $('#datepicker').val().split("/");
                
                var selectedDay = arrSelected[0];
                var selectedMonth = arrSelected[1];
                var selectedYear = arrSelected[2];
                
                if(selectedYear == currYear){
                    diff = selectedMonth - currMonth;
                }
                
                if(selectedYear < currYear){
                    var yearDif = currYear - selectedYear;
                    diff = (selectedMonth - currMonth) - (12*yearDif);
                }
                
//                console.log("selectedDay "+selectedDay);
//                console.log("selectedMonth "+selectedMonth);
//                console.log("selectedYear "+selectedYear);
//                console.log("currMonth "+currMonth);
//                console.log("currYear "+currYear);
//                console.log("diff "+diff);
                var period = 0;
                if (selectedDay < 16) {
                    period = 3;
                } else {
                    period = 4;
                }
                location.href = '<?php e(site_url('/admin/projectmgmt/tasks/timesheets_cso') . '/'); ?>' + diff + '/' + period;
            }

        });

        $('.checkval').bind(' change ', function () {
            waitingDialog.show('...updating, wait for a moment please');
            saveHours(this);
            waitingDialog.hide();
        });


    });


function saveHours(data){

            var dInput = data.value;
            var dInputId = data.id;
            var totalrow = <?= $no ?>;
            var columnId = dInputId.split("-");

            var totalhours = 0;
            for (loop = 1; loop < totalrow; loop++) {
                var v = parseFloat($("#" + columnId[0] + "-" + loop).val());
                if (!isNaN(v)) {
                    totalhours += v;
                }


            }
//
//            $("#tot" + $(this).attr("uniqueid")).text(parseFloat(totalhours).toFixed(1));
//
//            //change color
//            console.log(parseFloat(totalhours).toFixed(1));
//            if (parseFloat(totalhours).toFixed(1) < 8.5) {
//                $("#tot" + $(this).attr("uniqueid")).css('color', '#F00');
//            }
//            if (parseFloat(totalhours).toFixed(1) == 8.50) {
//                $("#tot" + $(this).attr("uniqueid")).css('color', '#339933');
//            }
//            if (parseFloat(totalhours).toFixed(1) > 8.50) {
//                $("#tot" + $(this).attr("uniqueid")).css('color', '#000');
//            }
////	if ( totalhours > 8.5 )  {  $("#sfa").removeAttr("disabled");  } 



            if (totalhours > 24) {
                alert("Maximum Total Hours 24 Reached");
                $("#" + dInputId).val("");
                // $("#" + dInputId).focus();
            } else {

                var rowid = $(data).attr("rowid");
                var row = $(data);
                var tid = $(data).attr("tid");
                var pid = $(data).attr("pid");
                var mid = $(data).attr("mid");
                var ptskid = $(data).attr("ptskid");
                var tdate = $(data).attr("data-date");
                var ta = $(data).attr("data-comments");
                var cnt = 0;

//                waitingDialog.show('...updating, wait for a moment please');
                $.post("<?php echo site_url(SITE_AREA . '/projectmgmt/tasks/update_hr_com') ?>", {rowno: rowid, hours: $(data).val(), taskid: tid, projectid: pid, milestoneid: mid, ptaskid: ptskid, timedate: tdate, comments: ta}, function (data) {

//                    waitingDialog.hide();

// console.log(data);

                    /*$(".total").each( function() {
                     
                     if ( parseFloat($(this).text()) < 8.5 ) { $("#sfa").attr("disabled","disabled"); cnt = 1; }
                     
                     });
                     
                     console.log("any zeros:"+cnt);
                     
                     if ( cnt != 1 ) { $("#sfa").removeAttr("disabled","disabled");}*/

                });

            }
            var cnt = 0;
            $(".total").each(function () {

                if (parseFloat($(data).text()) < $(data).attr('min-hours')) {
                    $("#sfa").attr("disabled", "disabled");
                    cnt = 1;
                }

            });

            console.log("any zeros:" + cnt);

            if (cnt != 1) {
                $("#sfa").removeAttr("disabled", "disabled");
            }



            $("#tot" + $(data).attr("uniqueid")).text(parseFloat(totalhours).toFixed(1));

            //change color
            if (parseFloat(totalhours).toFixed(1) < $("#tot" + $(data).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(data).attr("uniqueid")).css('color', '#F00');
                console.log(parseFloat(totalhours).toFixed(1) +" < "+ $("#tot" + $(data).attr("uniqueid")).attr('min-hours')+" Color: #F00 red");
            }
            if (parseFloat(totalhours).toFixed(1) == $("#tot" + $(data).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(data).attr("uniqueid")).css('color', '#339933');
                console.log(parseFloat(totalhours).toFixed(1) +" == "+ $("#tot" + $(data).attr("uniqueid")).attr('min-hours')+" Color: #339933 green");
            }
            if (parseFloat(totalhours).toFixed(1) > $("#tot" + $(data).attr("uniqueid")).attr('min-hours')) {
                $("#tot" + $(data).attr("uniqueid")).css('color', '#000');
                console.log(parseFloat(totalhours).toFixed(1) +" > "+ $("#tot" + $(data).attr("uniqueid")).attr('min-hours')+" Color: #000 black");
            }
            


}



</script>
