<?php

$num_columns	= 8;
$can_delete	= $this->auth->has_permission('Tasks.Projectmgmt.LockUnlockDeleted');
$can_edit		= $this->auth->has_permission('Tasks.Projectmgmt.LockUnlockEdit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4><strong>Locked Timesheets</strong></h4>
  </div>
  <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b selecta" onchange="setfname()">
<? if ( $this->session->userdata('lufield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('lufield')?>"><?=$this->session->userdata('lufield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<option value="name">Name</option>
<option value="department">Department</option>
<option value="month">Month</option>
</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('lufvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('lufname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('lufield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all"  type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks/lock_unlock?sort_by=name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                    Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_department') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/projectmgmt/tasks/lock_unlock?sort_by=department&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'department') ? 'desc' : 'asc'); ?>'>
                    Department</a></th>
                    
                   
                    
                   <th>Month</th>
				   <th>Requested On</th>
				   <th>Action</th>
				  
                    
                
				</tr>
			</thead>
		
			
		
			<tbody>
				<?php
				
					foreach ($records as $record) : ?>
<tr>
<th class="column-check"><input class="check-all" name="rowselect[]" type="checkbox" value="<? e($record->id)?>" /><input type="hidden" name="mth_lock[]" value="<?=$record->mth_lock?>" /><input type="hidden" name="period[]" value="<? e($record->period)?>" /><input type="hidden" name="name[]" value="<? e($record->name)?>" /></th>				
<td><? e($record->id)?></td>
<td><? e($this->auth->display_name_by_id($record->name))?></td>
<td><? e($record->department)?></td>
<td><? e($record->lockmonth)?></td>
<td><? e(date("d-m-Y",strtotime($record->requested_on)))?></td>
<td><a href="javascript:void()" onclick="lock_unlock('<? e($record->id)?>','<? e($record->name)?>','<? e($record->lockmonth)?>','<? e($record->status)?>','<? e($record->period)?>','<? e($record->mth_lock)?>')"><? echo $record->status=="lock" ? "<i class='fa fa-lock'></i>" : "<i class='fa fa-unlock'></i>"?></a></td>
</tr>
				 
				<?	endforeach;
				
				?>
				<tfoot>
								<tr>
					<td colspan="8">
						With selected						<input type="submit" name="massupd" id="delete-me" class="btn btn-danger" value="Unlock" onclick="return confirm('Confirm Unlock ?')">
					</td>
				</tr>
							</tfoot>
			</tbody>
		</table>
        
        
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php // echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
<script>
function lock_unlock(val,val2,val3,val4,val5,val6){		
		var r = confirm("Confirm "+val4+" ?");	
if (r == true) {
	$.post("<?php echo site_url(SITE_AREA .'/projectmgmt/tasks/lock_unlock_action') ?>", { lurowid:val,uid:val2,lockmonth:val3,status:val4,period:val5,mth_lock:val6}, function(data){	
	console.log(data); 
	window.location.href = "<?=base_url()?>index.php/admin/projectmgmt/tasks/lock_unlock";	
	})	
}
}
</script>