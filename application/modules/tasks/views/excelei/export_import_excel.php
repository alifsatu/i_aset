<div class="row">
    <section class="panel panel-default">
        <header class="panel-heading"> <strong>Export Timesheet Template by User / Month/ Project(s) </strong> </header>
        <div class="panel-body">
            <div class="row text-sm wrapper">
                <form method="post" action="<?php echo site_url(SITE_AREA . '/excelei/tasks/index') ?>" data-parsley-validate id="formexcel">
                    <div class="col-sm-3"><label>User</label>
                        <div class="controls">
                            <select name="users" id="users" class="selecta pull-left form-control input-sm" style="width:200px" onchange="getname()" required="" >
                                <option></option>
                                <?php
                                
                                $curruserlevel = $this->auth->user()->user_level_id;
                                $u = $this->db->query("select * from intg_users where user_level_id >= ".$curruserlevel." and subsidiary_id = ".$current_user->subsidiary_id)->result();
                                foreach ($u as $users) {
                                    ?>
                                    <option value="<?= $users->id ?>">
                                        <?= $users->display_name ?>
                                    </option>
                                    <?php } ?>
                                </select></div>
                            <input type="hidden" name="username" id="username" />
                        </div>

                        <div class="col-sm-3"> <label>Period</label>
                            <div class="controls">
                                <p>
                                <input type="radio" name="period" value="1" required=""/> 1st Half&nbsp;&nbsp;
                                <input type="radio" name="period" value="2" /> 2nd Half
                                </p>
                            </div>
                        </div>

                        <div class="col-sm-3"><label>Month</label>
                            <div class="controls">
                                <input type="text" name="datemth" id="datemth"  class="input-s input-sm  datepicker-input form-control" placeholder="Select Month" required=""/>
                            </div> 
                        </div>

                        <div class="col-sm-3">
                            <label>Project(s)</label>
                            <div class="controls">
                                <select name="projects[]" id="projects" multiple="multiple" class="selecta pull-left form-control input-sm" style="width:200px" required="">
                                    <option></option>
                                    <?php
                                    $p = $this->db->query("select * from intg_projects where final_status = 'Yes'")->result();
                                    foreach ( $p as $proj ) {

                                    ?>
                                    <option value="<?= $proj->id ?>">
                                        <?= $proj->project_name ?>
                                    </option>
                                    <?php } ?>
                                </select>

                            </div> 
                        </div>
                        <div class="col-sm-3"> 
                            <input class="btn btn-sm btn-primary" id="excel" type="submit" value="Export">
                        </div>
                    </form>
                </div>

            </div>
            <header class="panel-heading" style="border-top:rgb(232, 232, 232) solid 1px"> <strong>Import timesheet from excel</strong> </header>
            <div class="panel-body">
                <div class="row text-sm wrapper"> <?php echo form_open_multipart($this->uri->uri_string(), 'id="forma" data-parsley-validate'); ?>
                    <div class="form-group  clearfix">
                        <div class="form-actions  col-sm-5">
                            <div class='controls'>
                                <input type="file" name="files[]"   class="pull-left"/>
                                <span><strong>Incorrect uploaded file will be previewed but not inserted</strong></span></div>
                        </div>
                    </div>
                    <div class="form-group  clearfix">
                        <div class="form-actions  col-sm-5">
                            <div class='controls'>
                                <input type="submit" id="submit2"  name="save" class="btn btn-sm btn-primary" value="Import"  />
                                <?php  
                                if ( !$this->upload->display_multi_errors() && $_FILES['files']['name']!="") {  ?>
                                <input type="submit" id="submit3"  name="insertcsv" class="btn btn-sm btn-primary" value="Submit"  />
                                 <div class="table-scrollable innerWrapper">
            <table class="table table-striped b-t b-light text-sm timesheet-table m-b">
                                        <?php

                                        if (($handle = fopen("./uploads/user".$this->auth->user_id()."timesheet1.csv", "r")) !== FALSE) {
                                        $r= 1;
                                        $handle = fopen("./uploads/user".$this->auth->user_id()."timesheet1.csv", "r");

                                        while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {

                                        if ( $r == 1 ) { 
                                            echo "<tr>";
//                                            echo "<td></td>";
                                            //for ($c=0; $c < count($data); $c++) {$dota[] = $data[$c]; }	
                                            foreach ( $data  as $key1) {		
                                                echo "<td>".$key1."</td>";
                                            }
                                        }
                                        if ( $r==3 ) {
                                            echo "<tr>";
//                                            echo "<td></td>";
                                            for ($c=0; $c < count($data); $c++) {
                                                $dota[] = $data[$c]; 
                                            }
                                            
                                            foreach ( $dota  as $d1=>$key1) {		

                                                if ( $d1 > -1  && $d1 < 4  || $d1 > 7 ) {
                                                echo "<td>".$key1." </td>";
                                                }
                                            }
                                        } 

                                        else {
                                            if ( $r > 3 ) {
                                                
                                                $rowcount = $r - 3;
                                                
                                                echo "<tr>";
                                                echo "<td>$rowcount</td>";

                                                foreach ( $dota as $d=>$key ) {

                                                    if ( $d > 0  && $d < 4  || $d > 7 ) { 
                                                        if (  $dota[$d] != "C".$d  ) {
                                                            echo "<td>".$data[$d]." </td>";
                                                        } 

                                                        if (  $key == "C".$d ) { 
                                                            echo "<td>".$data[$d]." </td>";
                                                        }

                                                    }

                                                }

                                            } 

                                        }
                                        echo "</tr>";$r++;
                                        
                                        }


                                        fclose($handle);


                                        }
                                        ?>
                                    </table>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?> </div>
        </section>
    </div>


<script type="text/javascript">
        $(document).ready(function () {
			
			$(window).resize(function () {
     $(".table-scrollable").css('width', $(document).width() - 300 + "px");  /* 350 is the width taken by left menu */
     $(".table-scrollable").css('overflow-x', 'scroll');
     $(".table-scrollable").css('white-space', 'nowrap'); 
});
$(window).resize();
            // put all your jQuery goodness in here.
            $('#datemth').datepicker({
                format: 'm/yyyy',
                minViewMode: 'months',
                viewMode: 'months',
                pickTime: false
            });


            $('#datemth').datepicker().on('changeDate', function () {
                $('.datepicker').hide();

            });

        });
        /*
         $("#excel").click( function() { 
         if ( $("#datemth").val() !="" ) {
         
         
         location.href='<?php echo site_url(SITE_AREA .'/excelei/tasks/index') ?>/'+$("#datemth").val();
     
     }
     
     
     
     });*/

    function getname()
    {

        var a = $('#users  option:selected').text();
        $("#username").val(a);
    }

</script>