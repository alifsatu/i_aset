<div class="row">
  <section class="panel panel-default">
    <header class="panel-heading"> <strong>Export Timesheet by User / Month</strong> </header>
    <div class="panel-body">
      <div class="row text-sm wrapper">
      <form method="post" action="<?php echo site_url(SITE_AREA .'/excelei/tasks/timesheet_export') ?>">
        <div class="col-sm-3">
          <select name="users" id="users" class="selecta pull-left form-control input-sm" style="width:200px" onchange="getname()">
            <option></option>
            <?php
                        $u = $this->db->query("select * from intg_users")->result();
                        foreach ( $u as $users ) {
                            
                        ?>
            <option value="<?=$users->id?>"><?=$users->display_name?> </option>
            <?php } ?>
          </select>
          <input type="hidden" name="username" id="username" />
        </div>
         
        <div class="col-sm-3">
          <input type="text" name="datemth" id="datemth"  class="input-s input-sm  datepicker-input form-control" placeholder="Select mth"/>
        </div>
       
         <div class="col-sm-3"> <input class="btn btn-sm btn-primary" id="excel" type="submit" value="Export"></div>
          </form>
        </div>
    
    </div>
    <header class="panel-heading" style="border-top:rgb(232, 232, 232) solid 1px"></header>
  </section>
</div>
<script>
						$(document).ready(function() {
   // put all your jQuery goodness in here.
   $('#datemth').datepicker({
    format: 'm/yyyy',
	 minViewMode: 'months',
    viewMode: 'months',
    pickTime: false
 });
 
 
  $('#datemth').datepicker().on('changeDate', function(){
          $('.datepicker').hide();
		 
        });
	
})
/*
$("#excel").click( function() { 
if ( $("#datemth").val() !="" ) {
	
	
location.href='<?php echo site_url(SITE_AREA .'/excelei/tasks/index') ?>/'+$("#datemth").val();
	
}



});*/

function getname()
{
	
var a = $('#users  option:selected').text();
$("#username").val(a); 	
}
	
	</script>