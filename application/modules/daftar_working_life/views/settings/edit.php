<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_working_life))
{
	$daftar_working_life = (array) $daftar_working_life;
}
$id = isset($daftar_working_life['id']) ? $daftar_working_life['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Working Life</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_working_life_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_working_life_name' class='input-sm input-s  form-control' type='text' name='daftar_working_life_name' maxlength="255" value="<?php echo set_value('daftar_working_life_name', isset($daftar_working_life['name']) ? $daftar_working_life['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_working_life_classification_group_id', $options, set_value('daftar_working_life_classification_group_id', isset($daftar_working_life['classification_group_id']) ? $daftar_working_life['classification_group_id'] : ''), 'Classification Group Id'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_working_life_classification_id', $options, set_value('daftar_working_life_classification_id', isset($daftar_working_life['classification_id']) ? $daftar_working_life['classification_id'] : ''), 'Classification Id'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('value') ? 'error' : ''; ?>">
				<?php echo form_label('Value'. lang('bf_form_label_required'), 'daftar_working_life_value', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_working_life_value' class='input-sm input-s  form-control' type='text' name='daftar_working_life_value' maxlength="30" value="<?php echo set_value('daftar_working_life_value', isset($daftar_working_life['value']) ? $daftar_working_life['value'] : '');?>" />
					<span class='help-inline'><?php echo form_error('value'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_working_life_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/daftar_working_life', lang('daftar_working_life_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Daftar_Working_Life.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('daftar_working_life_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('daftar_working_life_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>