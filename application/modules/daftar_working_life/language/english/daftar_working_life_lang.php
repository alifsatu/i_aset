<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_working_life_manage']			= 'Manage';
$lang['daftar_working_life_edit']				= 'Edit';
$lang['daftar_working_life_true']				= 'True';
$lang['daftar_working_life_false']				= 'False';
$lang['daftar_working_life_create']			= 'Save';
$lang['daftar_working_life_list']				= 'List';
$lang['daftar_working_life_new']				= 'New';
$lang['daftar_working_life_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_working_life_no_records']		= 'There aren\'t any daftar_working_life in the system.';
$lang['daftar_working_life_create_new']		= 'Create a new Daftar Working Life.';
$lang['daftar_working_life_create_success']	= 'Daftar Working Life successfully created.';
$lang['daftar_working_life_create_failure']	= 'There was a problem creating the daftar_working_life: ';
$lang['daftar_working_life_create_new_button']	= 'Create New Daftar Working Life';
$lang['daftar_working_life_invalid_id']		= 'Invalid Daftar Working Life ID.';
$lang['daftar_working_life_edit_success']		= 'Daftar Working Life successfully saved.';
$lang['daftar_working_life_edit_failure']		= 'There was a problem saving the daftar_working_life: ';
$lang['daftar_working_life_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_working_life_purged']	= 'record(s) successfully purged.';
$lang['daftar_working_life_success']	= 'record(s) successfully restored.';


$lang['daftar_working_life_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_working_life_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_working_life_actions']			= 'Actions';
$lang['daftar_working_life_cancel']			= 'Cancel';
$lang['daftar_working_life_delete_record']		= 'Delete';
$lang['daftar_working_life_delete_confirm']	= 'Are you sure you want to delete this daftar_working_life?';
$lang['daftar_working_life_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_working_life_action_edit']		= 'Save';
$lang['daftar_working_life_action_create']		= 'Create';

// Activities
$lang['daftar_working_life_act_create_record']	= 'Created record with ID';
$lang['daftar_working_life_act_edit_record']	= 'Updated record with ID';
$lang['daftar_working_life_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_working_life_column_created']	= 'Created';
$lang['daftar_working_life_column_deleted']	= 'Deleted';
$lang['daftar_working_life_column_modified']	= 'Modified';
