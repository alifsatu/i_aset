<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * reports controller
 */
class reports extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Reporta.Reports.View');
		$this->load->model('reporta_model', null, true);
		$this->load->model('projects/projects_model', null, true);
		$this->lang->load('reporta');
		$this->lang->load('projects/projects');
		Assets::add_css('js/datepicker/datepicker.css');
		Assets::add_js("js/datepicker/bootstrap-datepicker.js");
		
		Template::set_block('sub_nav', 'reports/_sub_nav');

		
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		

		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('reportafromfield',date("Y-m-d",strtotime($this->input->post('fromdate'))));
$this->session->set_userdata('reportatofield',date("Y-m-d",strtotime($this->input->post('todate'))));
break;
case 'Reset':
$this->session->unset_userdata('reportafromfield');
$this->session->unset_userdata('reportatofield');
break;
}

$fromdate = $this->session->userdata('reportafromfield')=="" ? "1970-01-01" : $this->session->userdata('reportafromfield');
$todate = $this->session->userdata('reportatofield')=="" ? "2050-01-01" : $this->session->userdata('reportatofield');
	
$this->db->select('*,intg_projects.id as project_id');
$this->db->join('intg_users','intg_users.id=intg_projects.initiator');
$this->db->join('intg_cost_centre','intg_users.cost_centre_id=intg_cost_centre.id');
$this->db->join('intg_user_level','intg_users.user_level_id=intg_user_level.id');

$this->db->join('intg_sbu','intg_sbu.id=intg_projects.sbu_id');
//$this->db->join('intg_companies','intg_companies.id=intg_sbu.company_id');
$total = $this->projects_model
  ->where('intg_projects.project_start_date >=', $fromdate)
   ->where('intg_projects.project_end_date <=', $todate)
->where('final_status','Yes') 
  ->where('row_status','final')  
  ->where('FIND_IN_SET( intg_projects.subsidiary_id,"'.$this->current_user->subsidiary_id.'")')
	 ->where ("(
   `initiator` = ".$this->auth->user_id()." OR 
   FIND_IN_SET( '".$this->auth->user_id()."',coworker) OR
   FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR   
    FIND_IN_SET( '".$this->auth->user_id()."',other_approvers)    
   )")
->count_all();	


		//$records = $this->reporta_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 10;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case 'admin' :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case 'todo' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case 'notebook' :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}


	 

	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_projects.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_projects.id'=>'desc',
	 );	}
	 	 	

				 
				 		
 $this->db->select('*,intg_projects.id as project_id');
$this->db->join('intg_users','intg_users.id=intg_projects.initiator');
$this->db->join('intg_cost_centre','intg_users.cost_centre_id=intg_cost_centre.id');
$this->db->join('intg_user_level','intg_users.user_level_id=intg_user_level.id');


$this->db->join('intg_sbu','intg_sbu.id=intg_projects.sbu_id');
//$this->db->join('intg_companies','intg_companies.id=intg_sbu.company_id');
$records = $this->projects_model
->where('intg_projects.project_start_date >=', $fromdate)
   ->where('intg_projects.project_end_date <=', $todate)
->where('final_status','Yes') 
  ->where('row_status','final')  
  ->where('FIND_IN_SET( intg_projects.subsidiary_id,"'.$this->current_user->subsidiary_id.'")')
	 ->where ("(
   `initiator` = ".$this->auth->user_id()." OR 
   FIND_IN_SET( '".$this->auth->user_id()."',coworker) OR
   FIND_IN_SET( '".$this->auth->user_id()."',final_approvers) OR   
    FIND_IN_SET( '".$this->auth->user_id()."',other_approvers)    
   )")
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		


// echo $this->db->last_query();
//$records = $this->reporta_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('reporta', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage reporta');
		Template::render();
	}

	//--------------------------------------------------------------------

public function export()
{
	$this->load->library('session');
	$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
//name the worksheet
$this->excel->getActiveSheet()->setTitle('project');
$this->excel->getActiveSheet()->setCellValue('A1', '#');
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('B1', 'Individual');
$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('C1', 'Project Name');
$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('D1', 'CC');
$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('E1', 'SBU');
$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('F1', 'Company');
$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('G1', 'Hours');
$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('H1', 'RM');
$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('I1', 'Staff Level');
$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('J1', 'Designation');
$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('K1', 'Race');
$this->excel->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('L1', 'Bumi/Non-Bumi');
$this->excel->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('M1', 'Email');
$this->excel->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('N1', 'DOB');
$this->excel->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('O1', 'Highest Qualification');
$this->excel->getActiveSheet()->getStyle('O1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->setCellValue('P1', 'Joining Date');
$this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);



$fromdate = $this->session->userdata('reportafromfield')=="" ? "1970-01-01" : $this->session->userdata('reportafromfield');
$todate = $this->session->userdata('reportatofield')=="" ? "2050-01-01" : $this->session->userdata('reportatofield');

if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_projects.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_projects.id'=>'desc',
	 );	}
	 	 	
$this->load->model('projects/projects_model', null, true);
				 
				 		
 $this->db->select('*,intg_projects.id as project_id');
$this->db->join('intg_users','intg_users.id=intg_projects.initiator');
$this->db->join('intg_cost_centre','intg_users.cost_centre_id=intg_cost_centre.id');
$this->db->join('intg_user_level','intg_users.user_level_id=intg_user_level.id');

$this->db->join('intg_sbu','intg_sbu.id=intg_projects.sbu_id');
$this->db->join('intg_companies','intg_companies.id=intg_sbu.company_id');
$records = $this->projects_model
->where('intg_projects.project_start_date >=', $fromdate)
   ->where('intg_projects.project_end_date <=', $todate)

                 ->find_all();	

 $no=2;
 

 foreach($records as $record) 
 {
$this->excel->getActiveSheet()->setCellValue("A".$no, $no-1);	 
$this->excel->getActiveSheet()->setCellValue("B".$no, $record->display_name);
$this->excel->getActiveSheet()->setCellValue("C".$no, $record->project_name);						
$this->excel->getActiveSheet()->setCellValue("D".$no, $record->cost_centre); 
$this->excel->getActiveSheet()->setCellValue("E".$no,$record->sbu_name); 
$this->excel->getActiveSheet()->setCellValue("F".$no,$record->name);	 
$this->excel->getActiveSheet()->setCellValue("G".$no,number_format($record->total_days/24,2));	 
$this->excel->getActiveSheet()->setCellValue("H".$no,number_format($record->project_cost));
$this->excel->getActiveSheet()->setCellValue('I'.$no, $record->user_level);
$this->excel->getActiveSheet()->setCellValue('J'.$no, $record->position);
$this->excel->getActiveSheet()->setCellValue('K'.$no, $record->race);
$this->excel->getActiveSheet()->setCellValue('L'.$no, $record->bumi);
$this->excel->getActiveSheet()->setCellValue('M'.$no, $record->email);
$this->excel->getActiveSheet()->setCellValue('N'.$no, $record->dob);
$this->excel->getActiveSheet()->setCellValue('O'.$no, $record->highest_qualification);
$this->excel->getActiveSheet()->setCellValue('P'.$no, date("d-m-Y",strtotime($record->joining_date)));


                   


 $no++;
 }


$filename='project.XLSX'; //save our workbook as this file name
header('Content-Type: application/vnd.ms-excel');
//header('Content-type: text/csv'); //mime type
header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
            
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
//$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
//force user to download the Excel file without writing it to server's HD
$objWriter->save('php://output');
//$objWriter->save('testExportFile.csv');

	
		
		//Template::render();
	
}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_reporta($type='insert', $id=0)
	{}

	//--------------------------------------------------------------------


}