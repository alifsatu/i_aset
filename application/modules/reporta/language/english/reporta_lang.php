<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['reporta_manage']			= 'Manage';
$lang['reporta_edit']				= 'Edit';
$lang['reporta_true']				= 'True';
$lang['reporta_false']				= 'False';
$lang['reporta_create']			= 'Save';
$lang['reporta_list']				= 'List';
$lang['reporta_new']				= 'New';
$lang['reporta_edit_text']			= 'Edit this to suit your needs';
$lang['reporta_no_records']		= 'There aren\'t any reporta in the system.';
$lang['reporta_create_new']		= 'Create a new reporta.';
$lang['reporta_create_success']	= 'reporta successfully created.';
$lang['reporta_create_failure']	= 'There was a problem creating the reporta: ';
$lang['reporta_create_new_button']	= 'Create New reporta';
$lang['reporta_invalid_id']		= 'Invalid reporta ID.';
$lang['reporta_edit_success']		= 'reporta successfully saved.';
$lang['reporta_edit_failure']		= 'There was a problem saving the reporta: ';
$lang['reporta_delete_success']	= 'record(s) successfully deleted.';

$lang['reporta_purged']	= 'record(s) successfully purged.';
$lang['reporta_success']	= 'record(s) successfully restored.';


$lang['reporta_delete_failure']	= 'We could not delete the record: ';
$lang['reporta_delete_error']		= 'You have not selected any records to delete.';
$lang['reporta_actions']			= 'Actions';
$lang['reporta_cancel']			= 'Cancel';
$lang['reporta_delete_record']		= 'Delete';
$lang['reporta_delete_confirm']	= 'Are you sure you want to delete this reporta?';
$lang['reporta_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['reporta_action_edit']		= 'Save';
$lang['reporta_action_create']		= 'Create';

// Activities
$lang['reporta_act_create_record']	= 'Created record with ID';
$lang['reporta_act_edit_record']	= 'Updated record with ID';
$lang['reporta_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['reporta_column_created']	= 'Created';
$lang['reporta_column_deleted']	= 'Deleted';
$lang['reporta_column_modified']	= 'Modified';
