<?php

$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row-fluid">
<section class="panel panel-default">
 <div class="panel-body">
<? echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
				<div class="row">
				 <div class="column col-sm-2 col-xs-2 col-md-6"> <h4>Export To Excel</h4></div>
                 <div class="column col-sm-2 col-xs-2 col-md-2">
				<input type="text" class="form-control datepicker-input" placeholder="From Date" name="fromdate" id="from-date" value="<?=$this->session->userdata('reportafromfield') != "" ? date("d-m-Y",strtotime($this->session->userdata('reportafromfield'))) : ""?>" data-date-format="dd-mm-yyyy">
						</div>
						<div class="column col-sm-2 col-xs-2 col-md-2"><div class="input-group">
					<input type="text" class="form-control datepicker-input" placeholder="To Date" name="todate" id="to-date" value="<?=$this->session->userdata('reportatofield') != "" ? date("d-m-Y",strtotime($this->session->userdata('reportatofield'))): ""?>" data-date-format="dd-mm-yyyy">
						<span class="input-group-btn">                                    
                        <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
						<button type="submit" name="submit" class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
						</span>
							</div>
						</div>
					</div>
                  <?php echo form_close(); ?> 
                  <div class="row">
      <div class="table-responsive m-t">  
     <div style="width:75%; overflow:auto; margin:15px" >
               
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					
					<th>#</th>
                   
                    <th<?php if ($this->input->get('sort_by') == '_display_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=display_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'display_name') ? 'desc' : 'asc'); ?>'>
                    Individual</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_project_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=project_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_name') ? 'desc' : 'asc'); ?>'>
                    Project Name</a></th>
                   <th<?php if ($this->input->get('sort_by') == '_email') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=email&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'email') ? 'desc' : 'asc'); ?>'>
                   CC</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_sbu_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=sbu_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'sbu_name') ? 'desc' : 'asc'); ?>'>
                   SBU</a></th>
                     <th<?php if ($this->input->get('sort_by') == '_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                   Company</a></th>
                    <th<?php if ($this->input->get('sort_by') == '_total_days') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=total_days&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'total_days') ? 'desc' : 'asc'); ?>'>
                   Hours</a></th>
					 <th<?php if ($this->input->get('sort_by') == '_project_cost') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/reports/reporta?sort_by=project_cost&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'project_cost') ? 'desc' : 'asc'); ?>'>
                   RM</a></th>
					 <th>Staff Level</th>
					 <th>Designation</th>
					 <th>Race</th>
					 <th>Bumi/Non-Bumi</th>
					 <th>Email</th>
					 <th>DOB</th>
					 <th>Highest Qualification</th>
					 <th>Joining Date</th>
				</tr>
			</thead>
			
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					
					<td><?=$no?></td>
                    <td><?=$record->display_name?></td>
                    <td><?=$record->project_name?></td>
                    <td><?=$record->cost_centre?></td>
                    <td><?=$record->sbu_name?></td>
                    <td><?=$record->name?></td>
                    <td><?=number_format($record->total_days/24,2)?></td>
                    <td><?=number_format($record->project_cost)?></td>
                    <td><?=$record->user_level?></td>
                   <td><?=$record->position?></td>
                   <td><?=$record->race?></td>
                   <td><?=$record->bumi?></td>
                    <td><?=$record->email?></td>
                    <td><?=$record->dob?></td>
                   <td><?=$record->highest_qualification?></td>
                  <td><?=date("d-m-Y",strtotime($record->joining_date))?></td>
					
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?></div>
    
<footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
