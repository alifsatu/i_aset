<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($reporta))
{
	$reporta = (array) $reporta;
}
$id = isset($reporta['id']) ? $reporta['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">reporta</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('reporta_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/reports/reporta', lang('reporta_cancel'), 'class="btn btn-warning"'); ?>
				
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>