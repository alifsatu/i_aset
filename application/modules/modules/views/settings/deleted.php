<div class="row">
	<h3>Modules <div style="float:right">
	<?php 


echo form_open($this->uri->uri_string(),'class="form-search"'); ?>
	<label style="color:#fff">Search by fields</label>
<select name="select_field" id="select_field" onchange="setfname()">
<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>

<option>All Fields</option>		
<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_modules')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>



<input type="text" name="field_value" class="search-query input-xlarge" value="<?=$this->session->userdata('capfvalue')?>">
<button class="btn" type="submit" name="submit" value="Search" title="Search" ><i class="icon-search icon-black"></i></button>
<button type="submit" name="submit" class="btn" value="Reset" title="Reset"><i class="icon-refresh icon-black"></i></button>




	</h3>
	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Modules.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_modules_module_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_modules_module_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_modules_module_name') ? 'desc' : 'asc'); ?>'>
                    Modules Module Name</a></th>
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_nickname') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_nickname&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_nickname') ? 'desc' : 'asc'); ?>'>
                    Nickname</a></th>
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_modules_is_active') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_modules_is_active&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_modules_is_active') ? 'desc' : 'asc'); ?>'>
                    Modules Is Active</a></th>
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_deleted') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_deleted&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_deleted') ? 'desc' : 'asc'); ?>'>
                    Deleted</a></th>
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_created_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_created_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_created_on') ? 'desc' : 'asc'); ?>'>
                    Created On</a></th>
					<th <?php if ($this->input->get('sort_by') == 'modules'.'_modified_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_modified_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules'.'_modified_on') ? 'desc' : 'asc'); ?>'>
                    Modified On</a></th>
					<th>Deleted</th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Modules.Settings.Delete')) : ?>
				<tr>
					<td colspan="10">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Modules.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Modules.Settings.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/settings/modules/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->modules_module_name) ?></td>
				<?php else: ?>
				<td><?php echo $record->modules_module_name ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->nickname?></td>
				<td><?php echo $record->modules_is_active?></td>
				<td><?php echo $record->deleted?></td>
				<td><?php echo $record->created_on?></td>
				<td><?php echo $record->modified_on?></td>
				<td><?php echo $record->deleted > 0 ? lang('modules_true') : lang('modules_false')?></td>
				<td><?php echo $record->?></td>
				<td><?php echo $record->?></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="10">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
          <?php  echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
</div>