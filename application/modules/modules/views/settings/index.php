<?php

$num_columns	= 7;
$can_delete	= $this->auth->has_permission('Modules.Settings.Delete');
$can_edit		= $this->auth->has_permission('Modules.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
 <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">
  <div class="col-md-6">  <h4>Modules</h4></div>
	
     <div class="col-md-3">
<select name="select_field" id="select_field" class="form-control m-b"  onchange="setfname()">
<option value="<?=$this->session->userdata('capfield')?>"><?=$this->session->userdata('capfname')?></option>

<option>All Fields</option>		
<option value="personal_particulars_name">Name</option>
<option value="personal_particulars_nric">NRIC</option>
<option value="personal_particulars_htel">Handphone</option>
<option value="personal_particulars_email">Email</option>
<option value="personal_particulars_citizenship">Citizenship</option>
</select>

</div><div class="col-md-3"><div class="input-group">
                          
                          <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">

<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
   </span>
                        </div></div>
      </div>
	  <?php echo form_close(); ?>    

    
     <div class="table-responsive"> 
      
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
		<table class="table table-striped">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_modules_module_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_module_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules_module_name') ? 'desc' : 'asc'); ?>'>
                    Modules Module Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_nickname') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=nickname&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'nickname') ? 'desc' : 'asc'); ?>'>
                    Nickname</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_modules_is_active') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modules_is_active&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modules_is_active') ? 'desc' : 'asc'); ?>'>
                    Modules Is Active</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_deleted') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=deleted&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'deleted') ? 'desc' : 'asc'); ?>'>
                    Deleted</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_created_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=created_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'created_on') ? 'desc' : 'asc'); ?>'>
                    Created On</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_modified_on') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/modules?sort_by=modified_on&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'modified_on') ? 'desc' : 'asc'); ?>'>
                    Modified On</a></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('modules_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/modules/edit/' . $record->id, '<span class="icon-pencil"></span>' .  $record->modules_module_name); ?></td>
				<?php else : ?>
					<td><?php e($record->modules_module_name); ?></td>
				<?php endif; ?>
					<td><?php e($record->nickname) ?></td>
					<td><?php e($record->modules_is_active) ?></td>
					<td><?php echo $record->deleted > 0 ? lang('modules_true') : lang('modules_false')?></td>
					<td><?php e($record->created_on) ?></td>
					<td><?php e($record->modified_on) ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
        
  <?php echo form_close(); ?> 
  
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div> </section>
