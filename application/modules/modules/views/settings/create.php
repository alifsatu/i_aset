<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($modules))
{
	$modules = (array) $modules;
}
$id = isset($modules['id']) ? $modules['id'] : '';

?>
<div class="row">
                <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading clearfix">Modules</header>
                    <div class="panel-body">
                          

	<?php echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>
    	<fieldset>
	
			<div class="form-group <?php echo form_error('modules_module_name') ? 'error' : ''; ?>">
				<?php echo form_label('Modules Module Name', 'modules_modules_module_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='modules_modules_module_name'  class="input-sm input-s  form-control" type='text' name='modules_modules_module_name' maxlength="100" value="<?php echo set_value('modules_modules_module_name', isset($modules['modules_module_name']) ? $modules['modules_module_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modules_module_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('nickname') ? 'error' : ''; ?>">
				<?php echo form_label('Nickname', 'modules_nickname', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='modules_nickname'  class="input-sm input-s  form-control" type='text' name='modules_nickname' maxlength="100" value="<?php echo set_value('modules_nickname', isset($modules['nickname']) ? $modules['nickname'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('nickname'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('modules_is_active') ? 'error' : ''; ?>">
				<?php echo form_label('Modules Is Active', 'modules_modules_is_active', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='modules_modules_is_active'  class="input-sm input-s  form-control" type='text' name='modules_modules_is_active' maxlength="3" value="<?php echo set_value('modules_modules_is_active', isset($modules['modules_is_active']) ? $modules['modules_is_active'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modules_is_active'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('deleted') ? 'error' : ''; ?>">
				<?php echo form_label('Deleted', 'modules_deleted', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<label class='checkbox' for='modules_deleted'>
						<input type='checkbox' id='modules_deleted' name='modules_deleted' value='1' <?php echo (isset($modules['deleted']) && $modules['deleted'] == 1) ? 'checked="checked"' : set_checkbox('modules_deleted', 1); ?>>
						<span class='help-inline'><?php echo form_error('deleted'); ?></span>
					</label>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_on') ? 'error' : ''; ?>">
				<?php echo form_label('Created On', 'modules_created_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='modules_created_on'  class="input-sm input-s  form-control" type='text' name='modules_created_on' maxlength="1" value="<?php echo set_value('modules_created_on', isset($modules['created_on']) ? $modules['created_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('created_on'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('modified_on') ? 'error' : ''; ?>">
				<?php echo form_label('Modified On', 'modules_modified_on', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='modules_modified_on'  class="input-sm input-s  form-control" type='text' name='modules_modified_on' maxlength="1" value="<?php echo set_value('modules_modified_on', isset($modules['modified_on']) ? $modules['modified_on'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('modified_on'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('modules_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/modules', lang('modules_cancel'), 'class="btn btn-warning"'); ?>
				
			 </div>
					
					</fieldset>
					<?php echo form_close(); ?>
                    
                    
                    </div>
                    </section>
			</div><!--end of col6-->
		
    
</div> <!--end of row-->