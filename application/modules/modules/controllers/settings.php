<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Modules.Settings.View');
		$this->load->model('modules_model', null, true);
		$this->lang->load('modules');
		
			Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
			Assets::add_js('jquery-ui-1.8.13.min.js');
			Assets::add_css('jquery-ui-timepicker.css');
			Assets::add_js('jquery-ui-timepicker-addon.js');
		Template::set_block('sub_nav', 'settings/_sub_nav');

		Assets::add_module_js('modules', 'modules.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		
		$user_id =  $this->auth->user_id();

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->modules_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('modules_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('modules_delete_failure') . $this->modules_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}

if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->modules_model
   ->where('deleted','0')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();
  
}
else
{
	
  $total = $this->modules_model
    ->where('deleted','0')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

		//$records = $this->modules_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;



if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
    ->where('deleted','0')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 	 	
$records = $this->modules_model
  ->where('deleted','0')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}
//$records = $this->modules_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('modules', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Modules');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_modules SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('modules_success'), 'success');
				}
				else
				{
					Template::set_message(lang('modules_restored_error'). $this->modules_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_modules where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('modules_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}


if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 		
  $total = $this->modules_model
  ->where('deleted','1')
  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 
  ->count_all();


}
else
{
	
  $total = $this->modules_model
   ->where('deleted','1')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

//$records = $this-modules_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;


if ( $this->session->userdata('sapfield') =='All Fields') 
{
$records = $this->personal_particulars_model
 
  ->where('deleted','1')
   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 
  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')
  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')
 
 

->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'id'=>'desc',
	 );	}
	 
	 	
$records = $this->modules_model
->where('deleted','1')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->modules_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
 Assets::add_module_css('modules', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Modules');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Modules object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Modules.Settings.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_modules())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('modules_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'modules');

				Template::set_message(lang('modules_create_success'), 'success');
				redirect(SITE_AREA .'/settings/modules');
			}
			else
			{
				Template::set_message(lang('modules_create_failure') . $this->modules_model->error, 'error');
			}
		}
		Assets::add_module_js('modules', 'modules.js');

		Template::set('toolbar_title', lang('modules_create') . ' Modules');
		Template::render();
	}

	//--------------------------------------------------------------------


	/**
	 * Allows editing of Modules data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('modules_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/modules');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Modules.Settings.Edit');

			if ($this->save_modules('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('modules_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'modules');

				Template::set_message(lang('modules_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('modules_edit_failure') . $this->modules_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Modules.Settings.Delete');

			if ($this->modules_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('modules_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'modules');

				Template::set_message(lang('modules_delete_success'), 'success');

				redirect(SITE_AREA .'/settings/modules');
			}
			else
			{
				Template::set_message(lang('modules_delete_failure') . $this->modules_model->error, 'error');
			}
		}
		Template::set('modules', $this->modules_model->find($id));
		Template::set('toolbar_title', lang('modules_edit') .' Modules');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_modules SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('modules_success'), 'success');
											redirect(SITE_AREA .'/settings/modules/deleted');
				}
				else
				{
					
					Template::set_message(lang('modules_error') . $this->modules_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_modules where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('modules_purged'), 'success');
					redirect(SITE_AREA .'/settings/modules/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('modules_invalid_id'), 'error');
			redirect(SITE_AREA .'/settings/modules');
		}

		
		
		
		Template::set('modules', $this->modules_model->find($id));
		
		Assets::add_module_js('modules', 'modules.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Modules');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_modules($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['modules_module_name']        = $this->input->post('modules_modules_module_name');
		$data['nickname']        = $this->input->post('modules_nickname');
		$data['modules_is_active']        = $this->input->post('modules_modules_is_active');
		$data['deleted']        = $this->input->post('modules_deleted');
		$data['created_on']        = $this->input->post('modules_created_on') ? $this->input->post('modules_created_on') : '0000-00-00 00:00:00';
		$data['modified_on']        = $this->input->post('modules_modified_on') ? $this->input->post('modules_modified_on') : '0000-00-00 00:00:00';

		if ($type == 'insert')
		{
			$id = $this->modules_model->insert($data);

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->modules_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}