<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['modules_manage']			= 'Manage';
$lang['modules_edit']				= 'Edit';
$lang['modules_true']				= 'True';
$lang['modules_false']				= 'False';
$lang['modules_create']			= 'Save';
$lang['modules_list']				= 'List';
$lang['modules_new']				= 'New';
$lang['modules_edit_text']			= 'Edit this to suit your needs';
$lang['modules_no_records']		= 'There aren\'t any modules in the system.';
$lang['modules_create_new']		= 'Create a new Modules.';
$lang['modules_create_success']	= 'Modules successfully created.';
$lang['modules_create_failure']	= 'There was a problem creating the modules: ';
$lang['modules_create_new_button']	= 'Create New Modules';
$lang['modules_invalid_id']		= 'Invalid Modules ID.';
$lang['modules_edit_success']		= 'Modules successfully saved.';
$lang['modules_edit_failure']		= 'There was a problem saving the modules: ';
$lang['modules_delete_success']	= 'record(s) successfully deleted.';

$lang['modules_purged']	= 'record(s) successfully purged.';
$lang['modules_success']	= 'record(s) successfully restored.';


$lang['modules_delete_failure']	= 'We could not delete the record: ';
$lang['modules_delete_error']		= 'You have not selected any records to delete.';
$lang['modules_actions']			= 'Actions';
$lang['modules_cancel']			= 'Cancel';
$lang['modules_delete_record']		= 'Delete';
$lang['modules_delete_confirm']	= 'Are you sure you want to delete this modules?';
$lang['modules_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['modules_action_edit']		= 'Save';
$lang['modules_action_create']		= 'Create';

// Activities
$lang['modules_act_create_record']	= 'Created record with ID';
$lang['modules_act_edit_record']	= 'Updated record with ID';
$lang['modules_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['modules_column_created']	= 'Created';
$lang['modules_column_deleted']	= 'Deleted';
$lang['modules_column_modified']	= 'Modified';
