<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($daftar_status_jaminan_ccc)) {
    $daftar_status_jaminan_ccc = (array) $daftar_status_jaminan_ccc;
}
$id = isset($daftar_status_jaminan_ccc['id']) ? $daftar_status_jaminan_ccc['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Daftar Status Jaminan CCC</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Status' . lang('bf_form_label_required'), 'daftar_status_jaminan_ccc_nama', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='daftar_status_jaminan_ccc_nama' class='input-sm input-s  form-control' type='text' name='daftar_status_jaminan_ccc_nama' maxlength="255" value="<?php echo set_value('daftar_status_jaminan_ccc_nama', isset($daftar_status_jaminan_ccc['nama']) ? $daftar_status_jaminan_ccc['nama'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('status') ? 'error' : ''; ?>">
                        <?php echo form_label('Status' . lang('bf_form_label_required'), '', array('class' => 'control-label', 'id' => 'daftar_status_jaminan_ccc_status_label')); ?>
                        <div class='controls' aria-labelled-by='daftar_status_jaminan_ccc_status_label'>
                            <label class='radio' for='daftar_status_jaminan_ccc_status_option1'>
                                <input id='daftar_status_jaminan_ccc_status_option1' name='daftar_status_jaminan_ccc_status' type='radio'  value='1' <?php echo set_radio('daftar_status_jaminan_ccc_status', '1', TRUE); ?> />
                                Aktif
                            </label>
                            <label class='radio' for='daftar_status_jaminan_ccc_status_option2'>
                                <input id='daftar_status_jaminan_ccc_status_option2' name='daftar_status_jaminan_ccc_status' type='radio' value='2' <?php echo set_radio('daftar_status_jaminan_ccc_status', '2'); ?> />
                                Tak Aktif
                            </label>
                            <span class='help-inline'><?php echo form_error('status'); ?></span>
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_status_jaminan_ccc_action_edit'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/daftar_status_jaminan_ccc', lang('daftar_status_jaminan_ccc_cancel'), 'class="btn btn-warning"'); ?>

                        <?php if ($this->auth->has_permission('Daftar_Status_Jaminan_CCC.Settings.Delete')) : ?>
                            &nbsp;&nbsp;
                            <button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('daftar_status_jaminan_ccc_delete_confirm'))); ?>');">
                                <span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('daftar_status_jaminan_ccc_delete_record'); ?>
                            </button>
                        <?php endif; ?>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>