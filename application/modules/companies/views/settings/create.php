<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($companies)) {
    $companies = (array) $companies;
}
$id = isset($companies['id']) ? $companies['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Companies</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
                        <?php echo form_label('Company Name' . lang('bf_form_label_required'), 'companies_name', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='companies_name' class='input-sm input-s  form-control' type='text' name='companies_name' maxlength="255" value="<?php echo set_value('companies_name', isset($companies['name']) ? $companies['name'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('name'); ?></span>
                        </div>
                    </div>
                    <div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
                        <?php echo form_label('Description', 'description', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <?php echo form_textarea(array('name' => 'description', 'id' => 'description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('clients_description', isset($companies['description']) ? $companies['description'] : ''))); ?>
                            <span class='help-inline'><?php echo form_error('description'); ?></span>
                        </div>
                    </div>


                     <div class="control-group <?php echo form_error('companies_license_type') ? 'error' : ''; ?>">
                        <?php echo form_label('Package' . lang('bf_form_label_required'), 'companies_license_type', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='companies_license_type' type='text' name='companies_license_type' maxlength="255" value="<?php echo set_value('companies_license_type', isset($companies['companies_license_type']) ? $companies['companies_license_type'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('name'); ?></span>
                        </div>
                    </div>

                    <?php
// Change the values in this array to populate your dropdown as required
                    $query = $this->db->query("SELECT role_id, role_name FROM intg_roles WHERE role_name <> 'Administrator'");
                    $role_options[''] = "- - -Please Select- - -";
                    foreach ($query->result() as $row) {
                        $role_options[$row->role_id] = $row->role_name;
                        //echo $row->role_name;
                    }


                    echo form_multiselect('companies_roles[]', $role_options, set_value('companies_roles', isset($companies['roles']) ? $companies['roles'] : ''), 'Roles' . lang('bf_form_label_required'));
                    ?>

                    <!--// Modules which a client needs to subscribe-->

                    <?php
                    $module_options = array(
                        '' => '---- Ctrl + Select-----',
                        'Quotes' => 'Quotes Module',
                        'Invoices' => 'Invoices Module',
                        'Client' => 'Clients Module',
                        'Inventory' => 'Inventory Module',
                        'Reports' => 'Reports Module',
                        'Settings' => 'Settings Module',
                    );
                    echo form_multiselect('modules[]', $module_options, set_value('modules', isset($companies['modules']) ? $companies['modules'] : ''), 'Modules' . lang('bf_form_label_required'));
                    ?>


                    <div class="form-group <?php echo form_error('validity_from') ? 'error' : ''; ?>">
                        <?php echo form_label('Validity From' . lang('bf_form_label_required'), 'companies_validity_from', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='companies_validity_from' type='text' name='companies_validity_from'  value="<?php echo set_value('companies_validity_from', isset($companies['validity_from']) ? $companies['validity_from'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('validity_from'); ?></span>
                        </div>
                    </div>

                    <div class="form-group <?php echo form_error('validity_to') ? 'error' : ''; ?>">
                        <?php echo form_label('Validity To' . lang('bf_form_label_required'), 'companies_validity_to', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='companies_validity_to' type='text' name='companies_validity_to'  value="<?php echo set_value('companies_validity_to', isset($companies['validity_to']) ? $companies['validity_to'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('validity_to'); ?></span>
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('companies_action_create'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/companies', lang('companies_cancel'), 'class="btn btn-warning"'); ?>

                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>