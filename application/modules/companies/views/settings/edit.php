<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($companies))
{
	$companies = (array) $companies;
}
$id = isset($companies['id']) ? $companies['id'] : '';

?>
<div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Companies</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name'. lang('bf_form_label_required'), 'companies_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_name' type='text' name='companies_name' maxlength="255" value="<?php echo set_value('companies_name', isset($companies['name']) ? $companies['name'] : ''); ?>" class="input-s form-control" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>
            <div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'description', 'id' => 'description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('description', isset($companies['description']) ? $companies['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			
		<!--<div class="control-group <?php echo form_error('companies_license_type') ? 'error' : ''; ?>">
				<?php echo form_label('Package', 'companies_license_type', array('class' => 'control-label') ); ?> :
				
				<?
				$qp = $this->db->query("select p.package_name from intg_packages p,intg_license l where p.id=l.package_id and l.company_id = '".$companies['id']."'")->row();
				echo $qp->package_name;
				?>
				
			</div>-->

			<?php // Change the values in this array to populate your dropdown as required
				/* $options = array(
					255 => 255,
				);

				echo form_dropdown('companies_roles', $options, set_value('companies_roles', isset($companies['roles']) ? $companies['roles'] : ''), 'Roles'. lang('bf_form_label_required'));
		 */	?>

			<!--<div class="control-group <?php echo form_error('validity_from') ? 'error' : ''; ?>">
				<?php echo form_label('Validity From'. lang('bf_form_label_required'), 'companies_validity_from', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_validity_from' type='text' name='companies_validity_from'  value="<?php echo set_value('companies_validity_from', isset($companies['validity_from']) ? $companies['validity_from'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('validity_from'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('validity_to') ? 'error' : ''; ?>">
				<?php echo form_label('Validity To'. lang('bf_form_label_required'), 'companies_validity_to', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_validity_to' type='text' name='companies_validity_to'  value="<?php echo set_value('companies_validity_to', isset($companies['validity_to']) ? $companies['validity_to'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('validity_to'); ?></span>
				</div>
			</div>-->

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('companies_action_edit'); ?>"  />
				
				<?php echo anchor(SITE_AREA .'/settings/companies', lang('companies_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Companies.Settings.Delete')) : ?>
				
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('companies_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('companies_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>