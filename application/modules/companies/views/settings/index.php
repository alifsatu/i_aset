<?php
$num_columns = 10;
$can_delete = $this->auth->has_permission('Companies.Settings.Delete');
$can_edit = $this->auth->has_permission('Companies.Settings.Edit');
$has_records = isset($records) && is_array($records) && count($records);
?>
<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">
            <?php echo form_open($this->uri->uri_string(), 'class="form-inline"'); ?>

            <div class="row">
                <div class="col-md-6">
                    <h4>Companies</h4></div>

                <div class="col-md-3">
                    <select name="select_field" id="select_field" class="selecta form-control m-b"  onchange="setfname()">
                        <option></option>
                        <option value="All Fields" <?= $this->session->userdata('capfield') == 'All Fields' ? 'selected' : '' ?>>All Fields</option>		
                        <option value="name" <?= $this->session->userdata('capfield') == 'name' ? 'selected' : '' ?>>Company Name</option>
                        <option value="description" <?= $this->session->userdata('capfield') == 'description' ? 'selected' : '' ?>>Description</option>
                    </select>

                </div><div class="col-md-3"><div class="input-group">

                        <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?= $this->session->userdata('capfvalue') ?>">
                        <input type="hidden" name="field_name" id="field_name" value="<?= $this->session->userdata('capfname') ?>"/>

                        <span class="input-group-btn">

                            <button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
                            <button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
                        </span>
                    </div></div>
            </div>
            <?php echo form_close(); ?>    


            <div class="table-responsive m-t"> 

                <?php echo form_open($this->uri->uri_string()); ?>
                <table class="table table-striped datagrid m-b-sm">
                    <thead>
                        <tr>
                            <?php if ($can_delete && $has_records) : ?>
                                <th class="column-check"><input class="check-all" type="checkbox" /></th>
                            <?php endif; ?>
                            <th>#</th>

                            <th<?php
                            if ($this->input->get('sort_by') == '_name') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                                <a href='<?php echo base_url() . 'index.php/admin/settings/companies?sort_by=name&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'name') ? 'desc' : 'asc'); ?>'>
                                    Company Name</a></th>
                            <th>Description</th>
                          <!-- <th<?php
                            if ($this->input->get('sort_by') == '_no_users') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                           <a href='<?php echo base_url() . 'index.php/admin/settings/companies?sort_by=no_users&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'no_users') ? 'desc' : 'asc'); ?>'>
                            No. Of. Users</a></th>
                            
                           <th>Package Type</th>
                            
                           <th<?php
                            if ($this->input->get('sort_by') == '_roles') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                           <a href='<?php echo base_url() . 'index.php/admin/settings/companies?sort_by=roles&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'roles') ? 'desc' : 'asc'); ?>'>
                            Roles</a></th>
                            
                           <th<?php
                            if ($this->input->get('sort_by') == '_validity_from') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                           <a href='<?php echo base_url() . 'index.php/admin/settings/companies?sort_by=validity_from&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'validity_from') ? 'desc' : 'asc'); ?>'>
                            Validity From</a></th>
                            
                           <th<?php
                            if ($this->input->get('sort_by') == '_validity_to') {
                                echo 'class=\'sort_' . $this->input->get('sort_order') . '\'';
                            }
                            ?>>
                           <a href='<?php echo base_url() . 'index.php/admin/settings/companies?sort_by=validity_to&sort_order=' . (($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'validity_to') ? 'desc' : 'asc'); ?>'>
                            Validity To</a></th>
                                                <th><?php echo lang("companies_column_deleted"); ?></th>-->
                            <th><?php echo lang("companies_column_created"); ?></th>
                            <th><?php echo lang("companies_column_modified"); ?></th>
                        </tr>
                    </thead>
                    <?php if ($has_records) : ?>
                        <tfoot>
                            <?php if ($can_delete) : ?>
                                <tr>
                                    <td colspan="<?php echo $num_columns; ?>">
                                        <?php echo lang('bf_with_selected'); ?>
                                        <input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('companies_delete_confirm'))); ?>')" />
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tfoot>
                    <?php endif; ?>
                    <tbody>
                        <?php

                        if ($has_records) :
                            $no = $this->input->get('per_page') + 1;
                            foreach ($records as $record) :
                                ?>
                                <tr>
                                    <?php if ($can_delete) : ?>
                                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?= $no; ?></td>
                                    <?php endif; ?>

                                    <?php if ($can_edit) : ?>
                                        <td><?php echo anchor(SITE_AREA . '/settings/companies/edit/' . $record->id, $record->name . '<i class="fa fa-pencil m-l"></i>'); ?></td>
                                    <?php else : ?>
                                        <td><?php e($record->name); ?></td>
                                    <?php endif; ?>
                                    <td><?php e($record->description); ?>
                                            <!--<td><?php e($record->no_users) ?></td>
                                            <td><?php
                                        $qp = $this->db->query("select p.package_name from intg_packages p,intg_license l where p.id=l.package_id and l.company_id = '" . $record->id . "'")->row();
                                        echo $qp->package_name;
                                        ?></td>
                                            <td><?php e($record->roles) ?></td>
                                            <td><?php e($record->validity_from) ?></td>
                                            <td><?php e($record->validity_to) ?></td>
                                            <td><?php echo $record->deleted > 0 ? lang('companies_true') : lang('companies_false') ?></td>-->
                                    <td><?php echo date("d/m/Y h:i:s", strtotime($record->created_on)); ?></td>
                                    <td><?php echo $record->modified_on == "0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s", strtotime($record->modified_on)); ?></td>

                                </tr>
                                <?php
                                $no++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>

                <?php echo form_close(); ?> 

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">


                        </div>
                        <div class="col-sm-4 text-center">
                            <small class="text-muted inline m-t-sm m-b-sm">Showing <?= $offset + 1 ?> - <?php echo $rowcount + $offset ?> of <?php echo $total; ?></small>
                        </div>
                        <div class="col-sm-4 text-right text-center-xs">                

                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </footer>

            </div>


        </div> </section>
