<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($companies))
{
	$companies = (array) $companies;
}
$id = isset($companies['id']) ? $companies['id'] : '';

?>
<div class="row">
	<h3>Companies</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name'. lang('bf_form_label_required'), 'companies_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_name' type='text' name='companies_name' maxlength="255" value="<?php echo set_value('companies_name', isset($companies['name']) ? $companies['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('companies_no_users', $options, set_value('companies_no_users', isset($companies['no_users']) ? $companies['no_users'] : ''), 'No. Of. Users'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('companies_license_type', $options, set_value('companies_license_type', isset($companies['license_type']) ? $companies['license_type'] : ''), 'License Type'. lang('bf_form_label_required'));
			?>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					255 => 255,
				);

				echo form_dropdown('companies_roles', $options, set_value('companies_roles', isset($companies['roles']) ? $companies['roles'] : ''), 'Roles'. lang('bf_form_label_required'));
			?>

			<div class="control-group <?php echo form_error('validity_from') ? 'error' : ''; ?>">
				<?php echo form_label('Validity From'. lang('bf_form_label_required'), 'companies_validity_from', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_validity_from' type='text' name='companies_validity_from'  value="<?php echo set_value('companies_validity_from', isset($companies['validity_from']) ? $companies['validity_from'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('validity_from'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('validity_to') ? 'error' : ''; ?>">
				<?php echo form_label('Validity To'. lang('bf_form_label_required'), 'companies_validity_to', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='companies_validity_to' type='text' name='companies_validity_to'  value="<?php echo set_value('companies_validity_to', isset($companies['validity_to']) ? $companies['validity_to'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('validity_to'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/companies', lang('companies_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Companies.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('companies_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>