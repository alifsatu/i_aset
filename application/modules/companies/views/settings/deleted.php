<?php

$num_columns	= 10;
$can_delete	= $this->auth->has_permission('Companies.Settings.Delete');
$can_edit		= $this->auth->has_permission('Companies.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
 <?php 
  
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>

<div class="row">
  <div class="col-md-6">  <h4>Companies</h4></div>
	
     <div class="col-md-3">
<select name="select_field" id="select_field" class="selecta form-control m-b"  onchange="setfname()">
<option></option>
<option value="All Fields" <?=$this->session->userdata('capfield')=='All Fields' ? 'selected' : ''?>>All Fields</option>		
<option value="name" <?=$this->session->userdata('capfield')=='name' ? 'selected' : ''?>>Company Name</option>
<option value="description" <?=$this->session->userdata('capfield')=='description' ? 'selected' : ''?>>Description</option>
</select>

</div><div class="col-md-3"><div class="input-group">
                          
                          <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">

<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button>
   </span>
                        </div></div>
      </div>
	  <?php echo form_close(); ?>    

    
     <div class="table-responsive m-t"> 
	<?php  echo form_open($this->uri->uri_string()); ?>
	<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Companies.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					<th <?php if ($this->input->get('sort_by') == 'companies'.'_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_name') ? 'desc' : 'asc'); ?>'>
                    Company Name</a></th>
                     <th>Description</th>
					<!--<th <?php if ($this->input->get('sort_by') == 'companies'.'_no_users') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_no_users&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_no_users') ? 'desc' : 'asc'); ?>'>
                    No. Of. Users</a></th>
					<th <?php if ($this->input->get('sort_by') == 'companies'.'_license_type') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_license_type&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_license_type') ? 'desc' : 'asc'); ?>'>
                    License Type</a></th>
					<th <?php if ($this->input->get('sort_by') == 'companies'.'_roles') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_roles&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_roles') ? 'desc' : 'asc'); ?>'>
                    Roles</a></th>
					<th <?php if ($this->input->get('sort_by') == 'companies'.'_validity_from') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_validity_from&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_validity_from') ? 'desc' : 'asc'); ?>'>
                    Validity From</a></th>
					<th <?php if ($this->input->get('sort_by') == 'companies'.'_validity_to') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/companies?sort_by=companies_validity_to&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'companies'.'_validity_to') ? 'desc' : 'asc'); ?>'>
                    Validity To</a></th>-->
					<th>Deleted</th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Companies.Settings.Delete')) : ?>
				<tr>
					<td colspan="10">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
					<!--<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">-->
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Companies.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Companies.Settings.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/settings/companies/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->name) ?></td>
				<?php else: ?>
				<td><?php echo $record->name ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->description?></td>
				
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
					<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="10">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
          <?php  //echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div> </section>
