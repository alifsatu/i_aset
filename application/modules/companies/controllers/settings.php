<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * settings controller
 */
class settings extends Admin_Controller {
    //--------------------------------------------------------------------

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();

        $this->auth->restrict('Companies.Settings.View');
        $this->load->model('companies_model', null, true);
        $this->lang->load('companies');

        Assets::add_css('flick/jquery-ui-1.8.13.custom.css');
        Assets::add_js('jquery-ui-1.8.13.min.js');
        Template::set_block('sub_nav', 'settings/_sub_nav');

        Assets::add_module_js('companies', 'companies.js');
    }

    //--------------------------------------------------------------------

    /**
     * Displays a list of form data.
     *
     * @return void

      public function index() */
    public function index($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());
        $user_id = $this->auth->user_id();

        // Deleting anything?
        if (isset($_POST['delete'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->companies_model->delete($pid);
                }

                if ($result) {
                    Template::set_message(count($checked) . ' ' . lang('companies_delete_success'), 'success');
                } else {
                    Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
                }
            }
        }


        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('capfield', $this->input->post('select_field'));
                $this->session->set_userdata('capfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('capfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('capfield');
                $this->session->unset_userdata('capfvalue');
                $this->session->unset_userdata('capfname');
                break;
        }
        $field = '';
        if ($this->session->userdata('capfield') != '') {
            $field = $this->session->userdata('capfield');
        }

        if ($this->session->userdata('capfield') == 'All Fields') {

            $total = $this->companies_model
                    ->where('deleted', '0')
                    ->likes('name', $this->session->userdata('capfvalue'), 'both')
                    ->likes('description', $this->session->userdata('capfvalue'), 'both')
                    ->count_all();
        } else {

            $this->companies_model->where('deleted', '0');
            if ($field) {
                $this->companies_model->likes($field, $this->session->userdata('capfvalue'), 'both');
            }
            $total = $this->companies_model->count_all();
        }
//        echo $this->db->last_query();
        //$records = $this->companies_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 5;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;

        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('capfield') == 'All Fields') {
            $records = $this->companies_model
                    ->where('deleted', '0')
                    ->likes('name', $this->session->userdata('capfvalue'), 'both')
                    ->likes('description', $this->session->userdata('capfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }

            $this->companies_model->where('deleted', '0');
            if ($field) {
                $this->companies_model->likes($field, $this->session->userdata('capfvalue'), 'both');
            }
            $this->companies_model->limit($this->pager['per_page'], $offset);
            $this->companies_model->order_by($ord1);
            $records = $this->companies_model->find_all();
        }

//        echo $this->db->last_query();
//$records = $this->companies_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('companies', 'style.css');
        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Companies');
        Template::render();
    }

    //--------------------------------------------------------------------





    public function deleted($limit = 0, $filter = NULL, $per_page = NULL, $sort_by = 'id', $sort_order = 'asc') {
        $this->load->library('session');
        $this->load->library('pagination');
        $role_name = $this->auth->role_name_by_id($role_id = $this->auth->role_id());

        $user_id = $this->auth->user_id();



        if (isset($_POST['restore'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {

                    $result = $this->db->query('UPDATE intg_companies SET deleted = 0 where id = ' . $pid . '');
                }



                if ($result) {
                    Template::set_message(lang('companies_success'), 'success');
                } else {
                    Template::set_message(lang('companies_restored_error') . $this->companies_model->error, 'error');
                }
            }
        }

        if (isset($_POST['purge'])) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                foreach ($checked as $pid) {
                    $result = $this->db->query('delete from intg_companies where id = ' . $pid . '');
                }

                if ($result) {

                    Template::set_message(count($checked) . ' ' . lang('companies_purged'), 'success');
                }
            }
        }

        switch ($this->input->post('submit')) {
            case 'Search':
                $this->session->set_userdata('capfield', $this->input->post('select_field'));
                $this->session->set_userdata('capfvalue', $this->input->post('field_value'));
                $this->session->set_userdata('capfname', $this->input->post('field_name'));


                break;
            case 'Reset':
                $this->session->unset_userdata('capfield');
                $this->session->unset_userdata('capfvalue');
                $this->session->unset_userdata('capfname');
                break;
        }


        if ($this->session->userdata('capfield') != '') {
            $field = $this->session->userdata('capfield');
        } else {
            $field = 'id';
        }

        if ($this->session->userdata('capfield') == 'All Fields') {

            $total = $this->companies_model
                    ->where('deleted', '1')
                    ->likes('name', $this->session->userdata('capfvalue'), 'both')
                    ->likes('description', $this->session->userdata('capfvalue'), 'both')
                    ->count_all();
        } else {

            $total = $this->companies_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('capfvalue'), 'both')
                    ->count_all();
        }

//$records = $this-companies_model->find_all();
        /*         * ************************Pagination******************************* */
        $offset = $this->input->get('per_page');
        $this->pager['base_url'] = current_url() . '?';
        $this->pager['base_url'] = current_url() . '?sort_by=' . $this->input->get('sort_by') . '&sort_order=' . $this->input->get('sort_order');
        $this->pager['total_rows'] = $total;
        $this->pager['per_page'] = 5;
        $this->pager['num_links'] = 20;
        $this->pager['page_query_string'] = TRUE;
        $this->pager['uri_segment'] = 5;
        switch ($this->config->item('template.admin_theme')) {
            case "admin" : $this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>';
                break;
            case "todo" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
            case "notebook" : $this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">';
                break;
        }

        if ($this->session->userdata('capfield') == 'All Fields') {
            $records = $this->personal_particulars_model
                    ->where('deleted', '1')
                    ->likes('name', $this->session->userdata('capfvalue'), 'both')
                    ->likes('description', $this->session->userdata('capfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($this->input->get('sort_by'), $this->input->get('sort_order'))
                    ->find_all();
        } else {
            if ($this->input->get('sort_by') != '') {
                $ord1 = array(
                    $this->input->get('sort_by') => $this->input->get('sort_order'),
                    'id' => 'desc',
                );
            } else {
                $ord1 = array(
                    'id' => 'desc',
                );
            }


            $records = $this->companies_model
                    ->where('deleted', '1')
                    ->likes($field, $this->session->userdata('capfvalue'), 'both')
                    ->limit($this->pager['per_page'], $offset)
                    ->order_by($ord1)
                    ->find_all();
        }

        //$records = $this->companies_model->find_all();

        $this->pagination->initialize($this->pager);
        Template::set('current_url', current_url());
        Template::set('sort_by', $sort_by);
        Template::set('role_name', $role_name);
        Template::set('user_id', $user_id);
        Template::set('sort_order', $sort_order);
        Template::set('filter', $filter);
        Template::set('total', $total);
        Template::set('offset', $offset);
        Template::set('rowcount', $this->pager['per_page']);
        Assets::add_module_css('companies', 'style.css');

        Template::set('records', $records);
        Template::set('toolbar_title', 'Manage Companies');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Creates a Companies object.
     *
     * @return void
     */
    public function create() {
        $this->auth->restrict('Companies.Settings.Create');

        if (isset($_POST['save'])) {
            if ($insert_id = $this->save_companies()) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_create_record') . ': ' . $insert_id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_create_success'), 'success');
                redirect(SITE_AREA . '/settings/companies');
            } else {
                Template::set_message(lang('companies_create_failure') . $this->companies_model->error, 'error');
            }
        }
        Assets::add_module_js('companies', 'companies.js');

        Template::set('toolbar_title', lang('companies_create') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------

    /**
     * Allows editing of Companies data.
     *
     * @return void
     */
    public function edit() {
        $id = $this->uri->segment(5);

        if (empty($id)) {
            Template::set_message(lang('companies_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/companies');
        }

        if (isset($_POST['save'])) {
            $this->auth->restrict('Companies.Settings.Edit');

            if ($this->save_companies('update', $id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_edit_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_edit_success'), 'success');
            } else {
                Template::set_message(lang('companies_edit_failure') . $this->companies_model->error, 'error');
            }
        } else if (isset($_POST['delete'])) {
            $this->auth->restrict('Companies.Settings.Delete');

            if ($this->companies_model->delete($id)) {
                // Log the activity
                log_activity($this->current_user->id, lang('companies_act_delete_record') . ': ' . $id . ' : ' . $this->input->ip_address(), 'companies');

                Template::set_message(lang('companies_delete_success'), 'success');

                redirect(SITE_AREA . '/settings/companies');
            } else {
                Template::set_message(lang('companies_delete_failure') . $this->companies_model->error, 'error');
            }
        }
        Template::set('companies', $this->companies_model->find($id));
        Template::set('toolbar_title', lang('companies_edit') . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------



    public function restore_purge() {
        $id = $this->uri->segment(5);
        if (isset($_POST['restore'])) {

            $result = $this->db->query('UPDATE intg_companies SET deleted = 0 where id = ' . $id . '');

            if ($result) {
                Template::set_message(lang('companies_success'), 'success');
                redirect(SITE_AREA . '/settings/companies/deleted');
            } else {

                Template::set_message(lang('companies_error') . $this->companies_model->error, 'error');
            }
        }


        if (isset($_POST['purge'])) {

            $result = $this->db->query('delete from intg_companies where id = ' . $id . '');

            if ($result) {

                Template::set_message(lang('companies_purged'), 'success');
                redirect(SITE_AREA . '/settings/companies/deleted');
            }
        }


        if (empty($id)) {
            Template::set_message(lang('companies_invalid_id'), 'error');
            redirect(SITE_AREA . '/settings/companies');
        }




        Template::set('companies', $this->companies_model->find($id));

        Assets::add_module_js('companies', 'companies.js');

        Template::set('toolbar_title', 'Restore / Purge' . ' Companies');
        Template::render();
    }

    //--------------------------------------------------------------------
    // !PRIVATE METHODS
    //--------------------------------------------------------------------

    /**
     * Summary
     *
     * @param String $type Either "insert" or "update"
     * @param Int	 $id	The ID of the record to update, ignored on inserts
     *
     * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
     */
    private function save_companies($type = 'insert', $id = 0) {
        if ($type == 'update') {
            $_POST['id'] = $id;
        }

        // make sure we only pass in the fields we want
        $role_id = $this->input->post('companies_roles');
        $idr = implode(',', $role_id);
        $data = array();
        $data['name'] = $this->input->post('companies_name');
        $data['no_users'] = $this->input->post('companies_no_users');
        $data['license_type'] = $this->input->post('companies_license_type');
        $data['roles'] = $this->input->post('companies_license_type');
        $data['description'] = $this->input->post('description');
        $data['validity_from'] = $this->input->post('companies_validity_from') ? $this->input->post('companies_validity_from') : '0000-00-00';
        $data['validity_to'] = $this->input->post('companies_validity_to') ? $this->input->post('companies_validity_to') : '0000-00-00';

        if ($type == 'insert') {
            $id = $this->companies_model->insert($data);

            if (is_numeric($id)) {
                $return = $id;
                //$client_permission = $this->db->query("insert into intg_role_permissions_client (role_id, permission_id, company_id) select role_id, permission_id, $id from intg_role_permissions");
                //select all The permissions_id related to the modules selected
                $per_id = $this->input->post('modules');
                $pidr = implode('|', $per_id);
                $pidr = $pidr . '|' . 'Users' . '|' . 'Roles' . '|' . 'Site.Settings.View' . '|' . 'Bonfire.Permissions.View' . '|' . 'Bonfire.Permissions.Manage' . '|' . 'Permissions.Editor.Manage' . '|' . 'Permissions.User.Manage' . '|' . 'Permissions.Developer.Manage';
                $permission_query = $this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT 7, permission_id, $id FROM `intg_permissions` WHERE name RLIKE '$pidr'");
                //$user_creation = $this->db->query("INSERT INTO intg_users (role_id, email, username, company_id, password_hash, display_name, language, active, password_iterations) Values('7', 'pankaj@wipro.com', 'wadmin', '2', 'some_password', 'Wipro', 'english', '1', '8')");				
            } else {
                $return = FALSE;
            }
        } elseif ($type == 'update') {
            $return = $this->companies_model->update($id, $data);
        }

        return $return;
    }

    //--------------------------------------------------------------------
}
