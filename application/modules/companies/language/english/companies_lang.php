<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['companies_manage']			= 'Manage';
$lang['companies_edit']				= 'Edit';
$lang['companies_true']				= 'True';
$lang['companies_false']				= 'False';
$lang['companies_create']			= 'Save';
$lang['companies_list']				= 'List';
$lang['companies_new']				= 'New';
$lang['companies_edit_text']			= 'Edit this to suit your needs';
$lang['companies_no_records']		= 'There aren\'t any companies in the system.';
$lang['companies_create_new']		= 'Create a new Companies.';
$lang['companies_create_success']	= 'Companies successfully created.';
$lang['companies_create_failure']	= 'There was a problem creating the companies: ';
$lang['companies_create_new_button']	= 'Create New Companies';
$lang['companies_invalid_id']		= 'Invalid Companies ID.';
$lang['companies_edit_success']		= 'Companies successfully saved.';
$lang['companies_edit_failure']		= 'There was a problem saving the companies: ';
$lang['companies_delete_success']	= 'record(s) successfully deleted.';

$lang['companies_purged']	= 'record(s) successfully purged.';
$lang['companies_success']	= 'record(s) successfully restored.';


$lang['companies_delete_failure']	= 'We could not delete the record: ';
$lang['companies_delete_error']		= 'You have not selected any records to delete.';
$lang['companies_actions']			= 'Actions';
$lang['companies_cancel']			= 'Cancel';
$lang['companies_delete_record']		= 'Delete';
$lang['companies_delete_confirm']	= 'Are you sure you want to delete this companies?';
$lang['companies_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['companies_action_edit']		= 'Save';
$lang['companies_action_create']		= 'Create';

// Activities
$lang['companies_act_create_record']	= 'Created record with ID';
$lang['companies_act_edit_record']	= 'Updated record with ID';
$lang['companies_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['companies_column_created']	= 'Created';
$lang['companies_column_deleted']	= 'Deleted';
$lang['companies_column_modified']	= 'Modified';
