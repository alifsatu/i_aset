<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * client controller
 */
class client extends Admin_Controller
{

	//--------------------------------------------------------------------


	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth->restrict('Clients.Client.View');
		$this->load->model('clients_model', null, true);
		$this->lang->load('clients');
		
		Template::set_block('sub_nav', 'client/_sub_nav');
		

		Assets::add_module_js('clients', 'clients.js');
	}

	//--------------------------------------------------------------------


	/**
	 * Displays a list of form data.
	 *
	 * @return void
	
	public function index() */
	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		
		$user_id =  $this->auth->user_id();

		// Deleting anything?
		if (isset($_POST['delete']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = $this->clients_model->delete($pid);
				}

				if ($result)
				{
					Template::set_message(count($checked) .' '. lang('clients_delete_success'), 'success');
				}
				else
				{
					Template::set_message(lang('clients_delete_failure') . $this->clients_model->error, 'error');
				}
			}
		}
		
				
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}

if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'intg_clients.id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
 $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
 	//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	 		
  $total = $this->clients_model
   ->where('intg_clients.deleted','0')
//->where('intg_clients.company_id',$this->session->userdata('company_id')) 
   ->likes('client_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address_2',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_city',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_state',$this->session->userdata('sapfvalue'),'both')
     ->likes('client_zip',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_country',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_phone_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_fax_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_mobile_number',$this->session->userdata('sapfvalue'),'both')
    ->likes('client_email',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_web_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_notes',$this->session->userdata('sapfvalue'),'both')
  ->likes('tax_rate_name',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_active',$this->session->userdata('sapfvalue'),'both')
  ->count_all();
  
}
else
{
  $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	 		
  $total = $this->clients_model
    ->where('intg_clients.deleted','0')
	//->where('intg_clients.company_id',$this->session->userdata('company_id')) 
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

		//$records = $this->clients_model->find_all();
		/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case "admin" :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case "todo" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case "notebook" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}

if ( $this->session->userdata('sapfield') =='All Fields') 
{
  $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	 	
$records = $this->clients_model
 
    ->where('intg_clients.deleted','0')
	//->where('intg_clients.company_id',$this->session->userdata('company_id')) 
   ->likes('client_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address_2',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_city',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_state',$this->session->userdata('sapfvalue'),'both')
     ->likes('client_zip',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_country',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_phone_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_fax_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_mobile_number',$this->session->userdata('sapfvalue'),'both')
    ->likes('client_email',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_web_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_notes',$this->session->userdata('sapfvalue'),'both')
  ->likes('tax_rate_name',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_active',$this->session->userdata('sapfvalue'),'both')
  ->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_clients.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_clients.id'=>'desc',
	 );	}
 $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	 	
$records = $this->clients_model
  ->where('intg_clients.deleted','0')
 // ->where('intg_clients.company_id',$this->session->userdata('company_id')) 
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
				// echo $this->db->last_query();
		
}

//echo $this->db->last_query();

//$records = $this->clients_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('clients', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Clients');
		Template::render();
	}

	//--------------------------------------------------------------------


	
	
	
	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')
	{
		$this->load->library('session');
		$this->load->library('pagination');
		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());
		
		$user_id =  $this->auth->user_id();
		
				
				
if (isset($_POST['restore']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					
					$result = $this->db->query('UPDATE intg_clients SET deleted = 0 where id = '.$pid.'');			
				}
				
											

				if ($result)
				{
					Template::set_message(lang('clients_success'), 'success');
				}
				else
				{
					Template::set_message(lang('clients_restored_error'). $this->clients_model->error, 'error');
				}
			}
		}
		
		if (isset($_POST['purge']))
		{
			$checked = $this->input->post('checked');

			if (is_array($checked) && count($checked))
			{
				$result = FALSE;
				foreach ($checked as $pid)
				{
					$result = 	$this->db->query('delete from intg_clients where id = '.$pid.'');
		
				}

				if ($result)
				{
					
					Template::set_message(count($checked) .' '. lang('clients_purged'), 'success');
				}
				
			}
		}
		
switch ( $this->input->post('submit') ) 
{
case 'Search':
$this->session->set_userdata('sapfield',$this->input->post('select_field'));
$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));
$this->session->set_userdata('sapfname',$this->input->post('field_name'));


break;
case 'Reset':
$this->session->unset_userdata('sapfield');
$this->session->unset_userdata('sapfvalue');
$this->session->unset_userdata('sapfname');
break;
}


if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }
else { $field = 'intg_clients.id'; }

if ( $this->session->userdata('sapfield') =='All Fields') 
{ 
  $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
 	//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 			
  $total = $this->clients_model
  ->where('intg_clients.deleted','1')
 // ->where('intg_clients.company_id',$this->session->userdata('company_id')) 
  ->likes('client_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address_2',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_city',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_state',$this->session->userdata('sapfvalue'),'both')
     ->likes('client_zip',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_country',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_phone_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_fax_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_mobile_number',$this->session->userdata('sapfvalue'),'both')
    ->likes('client_email',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_web_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_notes',$this->session->userdata('sapfvalue'),'both')
  ->likes('tax_rate_name',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_active',$this->session->userdata('sapfvalue'),'both')
  ->count_all();


}
else
{
	 $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
 	//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	
  $total = $this->clients_model
   ->where('intg_clients.deleted','1')
  ->likes($field,$this->session->userdata('sapfvalue'),'both')  
  ->count_all();
}

//$records = $this-clients_model->find_all();
/**************************Pagination********************************/
$offset = $this->input->get('per_page');
$this->pager['base_url'] = current_url() .'?';
$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');
$this->pager['total_rows'] = $total;
$this->pager['per_page'] = 5;
$this->pager['num_links'] = 20;
$this->pager['page_query_string']= TRUE;
$this->pager['uri_segment'] = 5;

switch ($this->config->item('template.admin_theme') )
{
case "admin" :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case "todo" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case "notebook" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;

}
if ( $this->session->userdata('sapfield') =='All Fields') 
{
$this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
 ///	$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	
$records = $this->clients_model
 
  ->where('intg_clients.deleted','1')
 // ->where('intg_clients.company_id',$this->session->userdata('company_id')) 
   ->likes('client_name',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_address_2',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_city',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_state',$this->session->userdata('sapfvalue'),'both')
     ->likes('client_zip',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_country',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_phone_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_fax_number',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_mobile_number',$this->session->userdata('sapfvalue'),'both')
    ->likes('client_email',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_web_address',$this->session->userdata('sapfvalue'),'both') 
  ->likes('client_notes',$this->session->userdata('sapfvalue'),'both')
  ->likes('tax_rate_name',$this->session->userdata('sapfvalue'),'both')
  ->likes('client_active',$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 
                 ->find_all();
	 
}
else
{
	if ( $this->input->get('sort_by') !='' )
	{
		$ord1 = array (	
	$this->input->get('sort_by') => $this->input->get('sort_order'),
	'intg_clients.id'=>'desc',
	 );		
	}
	else { $ord1 = array (	
		'intg_clients.id'=>'desc',
	 );	}
 $this->db->select('*,intg_clients.id as clientid,intg_clients.created_on as clients_created,intg_clients.modified_on as clients_modified');
//$this->db->join('intg_tax_rates','intg_tax_rates.id=intg_clients.client_tax_id');	 	
$records = $this->clients_model
->where('intg_clients.deleted','1')
->likes($field,$this->session->userdata('sapfvalue'),'both')
->limit($this->pager['per_page'], $offset)
	->order_by( $ord1 ) 
                 ->find_all();	
		
}

		//$records = $this->clients_model->find_all();
		
$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('clients', 'style.css');

		Template::set('records', $records);
		Template::set('toolbar_title', 'Manage Clients');
		Template::render();
	}

	//--------------------------------------------------------------------



	/**
	 * Creates a Clients object.
	 *
	 * @return void
	 */
	public function create()
	{
		$this->auth->restrict('Clients.Client.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_clients())
			{
				// Log the activity
				log_activity($this->current_user->id, lang('clients_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'clients');

				Template::set_message(lang('clients_create_success'), 'success');
				redirect(SITE_AREA .'/client/clients');
			}
			else
			{
				Template::set_message(lang('clients_create_failure') . $this->clients_model->error, 'error');
			}
		}
		Assets::add_module_js('clients', 'clients.js');

		Template::set('toolbar_title', lang('clients_create') . ' Clients');
		Template::render();
	}


	public function ajax_create() {
		$this->auth->restrict('Clients.Client.Create');

		if (isset($_POST['save']))
		{
			if ($insert_id = $this->save_clients())
			{
				echo $insert_id;exit;
				// Log the activity
				//log_activity($this->current_user->id, lang('clients_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'clients');

				//Template::set_message(lang('clients_create_success'), 'success');
				//redirect(SITE_AREA .'/client/clients');
			}
			else
			{
				//Template::set_message(lang('clients_create_failure') . $this->clients_model->error, 'error');
			}
		}
		Assets::add_module_js('clients', 'clients.js');

		Template::set('toolbar_title', lang('clients_create') . ' Clients');
		$this->load->view('client/create');
		}
	//--------------------------------------------------------------------


	/**
	 * Allows editing of Clients data.
	 *
	 * @return void
	 */
	public function edit()
	{
		$id = $this->uri->segment(5);

		if (empty($id))
		{
			Template::set_message(lang('clients_invalid_id'), 'error');
			redirect(SITE_AREA .'/client/clients');
		}

		if (isset($_POST['save']))
		{
			$this->auth->restrict('Clients.Client.Edit');

			if ($this->save_clients('update', $id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('clients_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'clients');

				Template::set_message(lang('clients_edit_success'), 'success');
			}
			else
			{
				Template::set_message(lang('clients_edit_failure') . $this->clients_model->error, 'error');
			}
		}
		else if (isset($_POST['delete']))
		{
			$this->auth->restrict('Clients.Client.Delete');

			if ($this->clients_model->delete($id))
			{
				// Log the activity
				log_activity($this->current_user->id, lang('clients_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'clients');

				Template::set_message(lang('clients_delete_success'), 'success');

				redirect(SITE_AREA .'/client/clients');
			}
			else
			{
				Template::set_message(lang('clients_delete_failure') . $this->clients_model->error, 'error');
			}
		}
		Template::set('clients', $this->clients_model->find($id));
		Template::set('toolbar_title', lang('clients_edit') .' Clients');
		Template::render();
	}

	//--------------------------------------------------------------------



public function restore_purge()
	{
		$id = $this->uri->segment(5);
		if (isset($_POST['restore']))
		{
				
					$result = $this->db->query('UPDATE intg_clients SET deleted = 0 where id = '.$id.'');			

				if ($result)
				{
						Template::set_message(lang('clients_success'), 'success');
											redirect(SITE_AREA .'/client/clients/deleted');
				}
				else
				{
					
					Template::set_message(lang('clients_error') . $this->clients_model->error, 'error');
				}
		}
		
		
		if (isset($_POST['purge']))
		{
					
					$result = 	$this->db->query('delete from intg_clients where id = '.$id.'');		

				if ($result)
				{
					
						Template::set_message(lang('clients_purged'), 'success');
					redirect(SITE_AREA .'/client/clients/deleted');
				}
				
			
		}
		

		if (empty($id))
		{
				Template::set_message(lang('clients_invalid_id'), 'error');
			redirect(SITE_AREA .'/client/clients');
		}

		
		
		
		Template::set('clients', $this->clients_model->find($id));
		
		Assets::add_module_js('clients', 'clients.js');

		Template::set('toolbar_title', 'Restore / Purge' . ' Clients');
		Template::render();
	}


	//--------------------------------------------------------------------
	// !PRIVATE METHODS
	//--------------------------------------------------------------------

	/**
	 * Summary
	 *
	 * @param String $type Either "insert" or "update"
	 * @param Int	 $id	The ID of the record to update, ignored on inserts
	 *
	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE
	 */
	private function save_clients($type='insert', $id=0)
	{
		if ($type == 'update')
		{
			$_POST['id'] = $id;
		}

		// make sure we only pass in the fields we want
		
		$data = array();
		$data['client_name']        = $this->input->post('clients_client_name');
		$data['client_address']        = $this->input->post('clients_client_address');
		$data['client_address_2']        = $this->input->post('clients_client_address_2');
		$data['client_city']        = $this->input->post('clients_client_city');
		$data['client_state']        = $this->input->post('clients_client_state');
		$data['client_zip']        = $this->input->post('clients_client_zip');
		$data['client_country']        = $this->input->post('clients_client_country');
		$data['client_phone_number']        = $this->input->post('clients_client_phone_number');
		$data['client_fax_number']        = $this->input->post('clients_client_fax_number');
		$data['client_mobile_number']        = $this->input->post('clients_client_mobile_number');
		$data['client_email']        = $this->input->post('clients_client_email');
		$data['client_web_address']        = $this->input->post('clients_client_web_address');
		$data['client_notes']        = $this->input->post('clients_client_notes');
		$data['client_tax_id']        = $this->input->post('clients_client_tax_id');
		$data['client_active']        = $this->input->post('clients_client_active');
		//$data['company_id']      = $this->session->userdata('company_id');
		/* creating user for clients */
		$this->load->model('companies/companies_model', null, true);
		$this->load->model('users/user_model', null, true);
		$com_data = array(
					'name' => $this->input->post('clients_client_name'),
					'package_id' => '1',
					'no_users' => '15'
				);
				//print_r($com_data);
				//exit;
			 $this->db->insert('intg_companies', $com_data);
				$company_id = $this->db->insert_id();
				
		
		//	print_r($us_data);
		//echo $a[0];
	//		print_r($a);	
			//	EXIT;
			$pidr = 'Users'.'|'.'Roles'.'|'.'Site.Settings.View'.'|'.'Bonfire.Permissions.View'.'|'.'Bonfire.Permissions.Manage'.'|'.'Permissions.Manager.Manage'.'|'.'Permissions.HOD.Manage'.'|'.'Permissions.Employee.Manage'.'|'.'Permissions.CEO.Manage'.'|'.'Dashboard'.'|'.'Site.Invoices.View'.'|'.'Invoice_for_you.Invoices.Delete'.'|'.'Invoice_for_you.Invoices.Restore_purge'.'|'.'Invoice_for_you.Invoices.Deleted'.'|'.'Invoice_for_you.Invoices.View';
					$pidr = $pidr;
					//echo $pidr;
					//exit;
					//$pidr = $pidr.'|'.'Users'.'|'.'Roles'.'|'.'Site.Settings.View'.'|'.'Bonfire.Permissions.View'.'|'.'Bonfire.Permissions.Manage';
					$permission_query = $this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT 7, permission_id, $company_id FROM `intg_permissions` WHERE name RLIKE '$pidr'");
					//echo $this->db->last_query();
					 $records = $this->db->query("SELECT role_id FROM `intg_roles` WHERE role_name != 'cadmin' and role_name != 'administrator'")->result();
					//echo $records;
					foreach($records as $record)
					{
					$role = $record->role_id;
					$this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT $role, permission_id, $company_id FROM `intg_permissions` WHERE name RLIKE '$pidr'");
					//echo $this->db->last_query();
					//exit;
					} 
				$abc = $this->input->post('clients_client_email');
		$a = (explode("@",$abc));
		$us_data = array(
					'email' => $this->input->post('clients_client_email'),
					'password' => 'izeberg123',
					'company_id' => $company_id,
					'display_name' => $a[0],
					'username' => $a[0],
					'role_id' => '12'
				);
		
		//$return =	$this->db->insert('intg_users',$us_data); 
		$return = $this->user_model->insert($us_data);		
				
					
					
					
/* creating user for clients */
		if ($type == 'insert')
		{
			
			
			
			$id = $this->clients_model->insert($data);
			
			//echo $this->db->last_query();

			if (is_numeric($id))
			{
				$return = $id;
			}
			else
			{
				$return = FALSE;
			}
		}
		elseif ($type == 'update')
		{
			$return = $this->clients_model->update($id, $data);
		}

		return $return;
	}

	//--------------------------------------------------------------------


}