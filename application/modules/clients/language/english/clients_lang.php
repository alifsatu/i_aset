<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['clients_manage']			= 'Manage';
$lang['clients_edit']				= 'Edit';
$lang['clients_true']				= 'True';
$lang['clients_false']				= 'False';
$lang['clients_create']			= 'Save';
$lang['clients_list']				= 'List';
$lang['clients_new']				= 'New';
$lang['clients_edit_text']			= 'Edit this to suit your needs';
$lang['clients_no_records']		= 'There aren\'t any clients in the system.';
$lang['clients_create_new']		= 'Create a new Clients.';
$lang['clients_create_success']	= 'Clients successfully created.';
$lang['clients_create_failure']	= 'There was a problem creating the clients: ';
$lang['clients_create_new_button']	= 'Create New Clients';
$lang['clients_invalid_id']		= 'Invalid Clients ID.';
$lang['clients_edit_success']		= 'Clients successfully saved.';
$lang['clients_edit_failure']		= 'There was a problem saving the clients: ';
$lang['clients_delete_success']	= 'record(s) successfully deleted.';

$lang['clients_purged']	= 'record(s) successfully purged.';
$lang['clients_success']	= 'record(s) successfully restored.';


$lang['clients_delete_failure']	= 'We could not delete the record: ';
$lang['clients_delete_error']		= 'You have not selected any records to delete.';
$lang['clients_actions']			= 'Actions';
$lang['clients_cancel']			= 'Cancel';
$lang['clients_delete_record']		= 'Delete';
$lang['clients_delete_confirm']	= 'Are you sure you want to delete this clients?';
$lang['clients_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['clients_action_edit']		= 'Save';
$lang['clients_action_create']		= 'Create';

// Activities
$lang['clients_act_create_record']	= 'Created record with ID';
$lang['clients_act_edit_record']	= 'Updated record with ID';
$lang['clients_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['clients_column_created']	= 'Created';
$lang['clients_column_deleted']	= 'Deleted';
$lang['clients_column_modified']	= 'Modified';
