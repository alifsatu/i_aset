<div class="row">
<section class="panel panel-default">
<div class="panel-body">
	<?php 
echo form_open($this->uri->uri_string(),'class="form-search"'); ?>

<div class="row">
        <div class="col-md-6">  <h4><? echo lang('clients')?></h4></div>
        <div class="col-md-3"><select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

<option><? echo lang('client_allfields')?></option>		
<option value="client_name" <?php if($this->session->userdata('sapfield')== "client_name") echo "Selected";?>><? echo lang('clients_name')?></option>
<option value="client_address" <?php if($this->session->userdata('sapfield')== "client_address") echo "Selected";?>><? echo lang('clients_address')?></option>
<option value="client_address_2" <?php if($this->session->userdata('sapfield')== "client_address_2") echo "Selected";?>><? echo lang('clients_address')?> 2</option>
<option value="client_city" <?php if($this->session->userdata('sapfield')== "client_city") echo "Selected";?>><? echo lang('clients_city')?></option>
<option value="client_state" <?php if($this->session->userdata('sapfield')== "client_state") echo "Selected";?>><? echo lang('clients_state')?></option>
<option value="client_zip" <?php if($this->session->userdata('sapfield')== "client_zip") echo "Selected";?>><? echo lang('clients_zip_code')?></option>
<option value="client_country" <?php if($this->session->userdata('sapfield')== "client_country") echo "Selected";?>><? echo lang('clients_country')?></option>
<option value="client_phone_number" <?php if($this->session->userdata('sapfield')== "client_phone_number") echo "Selected";?>><? echo lang('clients_phone')?></option>
<option value="client_fax_number" <?php if($this->session->userdata('sapfield')== "client_fax_number") echo "Selected";?>><? echo lang('clients_fax')?></option>
<option value="client_mobile_number" <?php if($this->session->userdata('sapfield')== "client_mobile_number") echo "Selected";?>><? echo lang('clients_mobile_number')?></option>
<option value="client_email" <?php if($this->session->userdata('sapfield')== "client_email") echo "Selected";?>><? echo lang('clients_email')?></option>
<option value="client_web_address" <?php if($this->session->userdata('sapfield')== "client_web_address") echo "Selected";?>><? echo lang('clients_web')?></option>
<option value="client_notes" <?php if($this->session->userdata('sapfield')== "client_notes") echo "Selected";?>><? echo lang('clients_notes')?></option>

</select>
</div><div class="col-md-3"><div class="input-group">
 <input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('capfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('capfname')?>"/>

                          <span class="input-group-btn">
                          
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
<button type="submit" name="submit"  class="btn btn-primary btn-icon" value="Reset" title="Reset"><i class="fa fa-refresh"></i></button>

                          </span>
                        </div></div>
      </div>



                <?php echo form_close(); ?>
                
                 <div class="table-responsive">
                 <?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped b-t b-light text-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Clients.Client.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					
					 <th<?php if ($this->input->get('sort_by') == '_client_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/client/clients?sort_by=client_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_name') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('clients_name')?></a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_client_phone_number') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/client/clients?sort_by=client_phone_number&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_phone_number') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('clients_phone')?></a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_client_fax_number') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/client/clients?sort_by=client_fax_number&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_fax_number') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('clients_fax')?></a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_client_mobile_number') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/client/clients?sort_by=client_mobile_number&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_mobile_number') ? 'desc' : 'asc'); ?>'>
                    <? echo lang('clients_mobile_number')?></a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_client_email') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/client/clients?sort_by=client_email&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_email') ? 'desc' : 'asc'); ?>'>
                   <? echo lang('clients_email')?></a></th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Clients.Client.Delete')) : ?>
				<tr>
					<td colspan="19">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       <? if($this->auth->company_id() == 0)  { ?>

					<input type="submit" name="purge" class="btn btn-danger" value="<?php echo lang('bf_action_purge') ?>" onclick="return confirm('<?php echo lang('us_purge_del_confirm'); ?>')">
						
<? } ?>
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Clients.Client.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->clientid ?>" /></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Clients.Client.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/client/clients/restore_purge/'. $record->clientid, '<i class="icon-pencil">&nbsp;</i>' .  $record->client_name) ?></td>
				<?php else: ?>
				<td><?php echo $record->client_name ?></td>
				<?php endif; ?>
			
				
				<td><?php e($record->client_phone_number) ?></td>
					<td><?php e($record->client_fax_number) ?></td>
					<td><?php e($record->client_mobile_number) ?></td>
					<td><?php e($record->client_email) ?></td>
					
				</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="19"><? echo lang('clients_records')?></td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         <?php  //echo $this->pagination->create_links(); ?>
	<?php echo form_close(); ?>
 <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm"><? echo lang('quote_showing')?> <?=$offset+1?> - <? echo $rowcount+$offset?><? echo lang('quote_of')?>  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                     <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>
                
                </div>
              
                              
                     </div>
                     </section>