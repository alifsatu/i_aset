<ul class="nav nav-pills">
	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>
		<a href="<?php echo site_url(SITE_AREA .'/client/clients') ?>" style="border-radius:0" id="list"><?php echo lang('clients_list'); ?></a>
	</li>
	<?php if ($this->auth->has_permission('Clients.Client.Create')) : ?>
	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/client/clients/create') ?>" style="border-radius:0" id="create_new"><?php echo lang('clients_new'); ?></a>
	</li>
	<?php endif; ?>
     <?php if ($this->auth->has_permission('Clients.Client.Deleted')) : ?>
    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >
		<a href="<?php echo site_url(SITE_AREA .'/client/clients/deleted') ?>" style="border-radius:0" id="deleted"><? echo lang('clients_column_deleted')?></a>
	</li>
    <?php endif; ?>
</ul>