<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($clients))
{
	$clients = (array) $clients;
}
$id = isset($clients['id']) ? $clients['id'] : '';

?>
<div class="row">
	<h3>Clients</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('client_name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'clients_client_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_name' type='text' name='clients_client_name' maxlength="255" value="<?php echo set_value('clients_client_name', isset($clients['client_name']) ? $clients['client_name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_name'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_address') ? 'error' : ''; ?>">
				<?php echo form_label('Address'. lang('bf_form_label_required'), 'clients_client_address', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'clients_client_address', 'id' => 'clients_client_address', 'rows' => '5', 'cols' => '80', 'value' => set_value('clients_client_address', isset($clients['client_address']) ? $clients['client_address'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('client_address'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_address_2') ? 'error' : ''; ?>">
				<?php echo form_label('Address 2', 'clients_client_address_2', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'clients_client_address_2', 'id' => 'clients_client_address_2', 'rows' => '5', 'cols' => '80', 'value' => set_value('clients_client_address_2', isset($clients['client_address_2']) ? $clients['client_address_2'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('client_address_2'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_city') ? 'error' : ''; ?>">
				<?php echo form_label('City'. lang('bf_form_label_required'), 'clients_client_city', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_city' type='text' name='clients_client_city' maxlength="45" value="<?php echo set_value('clients_client_city', isset($clients['client_city']) ? $clients['client_city'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_city'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_state') ? 'error' : ''; ?>">
				<?php echo form_label('State'. lang('bf_form_label_required'), 'clients_client_state', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_state' type='text' name='clients_client_state' maxlength="45" value="<?php echo set_value('clients_client_state', isset($clients['client_state']) ? $clients['client_state'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_state'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_zip') ? 'error' : ''; ?>">
				<?php echo form_label('Zip Code'. lang('bf_form_label_required'), 'clients_client_zip', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_zip' type='text' name='clients_client_zip' maxlength="45" value="<?php echo set_value('clients_client_zip', isset($clients['client_zip']) ? $clients['client_zip'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_zip'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_country') ? 'error' : ''; ?>">
				<?php echo form_label('Country'. lang('bf_form_label_required'), 'clients_client_country', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_country' type='text' name='clients_client_country' maxlength="45" value="<?php echo set_value('clients_client_country', isset($clients['client_country']) ? $clients['client_country'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_country'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_phone_number') ? 'error' : ''; ?>">
				<?php echo form_label('Phone Number'. lang('bf_form_label_required'), 'clients_client_phone_number', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_phone_number' type='text' name='clients_client_phone_number' maxlength="45" value="<?php echo set_value('clients_client_phone_number', isset($clients['client_phone_number']) ? $clients['client_phone_number'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_phone_number'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_fax_number') ? 'error' : ''; ?>">
				<?php echo form_label('Fax', 'clients_client_fax_number', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_fax_number' type='text' name='clients_client_fax_number' maxlength="45" value="<?php echo set_value('clients_client_fax_number', isset($clients['client_fax_number']) ? $clients['client_fax_number'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_fax_number'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_mobile_number') ? 'error' : ''; ?>">
				<?php echo form_label('Mobile Number'. lang('bf_form_label_required'), 'clients_client_mobile_number', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_mobile_number' type='text' name='clients_client_mobile_number' maxlength="45" value="<?php echo set_value('clients_client_mobile_number', isset($clients['client_mobile_number']) ? $clients['client_mobile_number'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_mobile_number'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_email') ? 'error' : ''; ?>">
				<?php echo form_label('E-mail'. lang('bf_form_label_required'), 'clients_client_email', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_email' type='text' name='clients_client_email' maxlength="45" value="<?php echo set_value('clients_client_email', isset($clients['client_email']) ? $clients['client_email'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_email'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_web_address') ? 'error' : ''; ?>">
				<?php echo form_label('Web Address', 'clients_client_web_address', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_web_address' type='text' name='clients_client_web_address' maxlength="45" value="<?php echo set_value('clients_client_web_address', isset($clients['client_web_address']) ? $clients['client_web_address'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_web_address'); ?></span>
				</div>
			</div>

			<div class="control-group <?php echo form_error('client_notes') ? 'error' : ''; ?>">
				<?php echo form_label('Notes', 'clients_client_notes', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'clients_client_notes', 'id' => 'clients_client_notes', 'rows' => '5', 'cols' => '80', 'value' => set_value('clients_client_notes', isset($clients['client_notes']) ? $clients['client_notes'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('client_notes'); ?></span>
				</div>
			</div>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('clients_client_tax_id', $options, set_value('clients_client_tax_id', isset($clients['client_tax_id']) ? $clients['client_tax_id'] : ''), 'Client Tax'. lang('bf_form_label_required'));
			?>

			<div class="control-group <?php echo form_error('client_active') ? 'error' : ''; ?>">
				<?php echo form_label('Active', 'clients_client_active', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_active' type='text' name='clients_client_active' maxlength="45" value="<?php echo set_value('clients_client_active', isset($clients['client_active']) ? $clients['client_active'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('client_active'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/client/clients', lang('clients_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Clients.Client.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('clients_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>