<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 

echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>Clients</h4></div>
 <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

<? if ( $this->session->userdata('clientsfield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('clientsfield')?>"><?=$this->session->userdata('clientsfield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_clients')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>
</div><div class="col-md-3"><div class="input-group">
<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('clientsfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('clientsfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('clientsfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

<?php echo form_close(); ?>
     <div class="table-responsive">   
  


	<?php  echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($this->auth->has_permission('Clients.Settings.Delete') && isset($records) && is_array($records) && count($records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_client_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_client_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_client_name') ? 'desc' : 'asc'); ?>'>
                    Client Name</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_email') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_email&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_email') ? 'desc' : 'asc'); ?>'>
                    Email</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_mobile') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_mobile&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_mobile') ? 'desc' : 'asc'); ?>'>
                    Mobile</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_phone') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_phone&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_phone') ? 'desc' : 'asc'); ?>'>
                    Phone</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_fax') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_fax&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_fax') ? 'desc' : 'asc'); ?>'>
                    Fax</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_description') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_created_by') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_created_by&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_created_by') ? 'desc' : 'asc'); ?>'>
                    Created by</a></th>
					<th <?php if ($this->input->get('sort_by') == 'clients'.'_company_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=clients_company_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'clients'.'_company_id') ? 'desc' : 'asc'); ?>'>
                    Company Name</a></th>
					<th>Created</th>
					<th>Modified</th>
				</tr>
			</thead>
			<?php if (isset($records) && is_array($records) && count($records)) : ?>
			<tfoot>
				<?php if ($this->auth->has_permission('Clients.Settings.Delete')) : ?>
				<tr>
					<td colspan="11">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
				<!--	<input type="submit" name="purge" class="btn btn-danger" value="<?php //echo lang('bf_action_purge') ?>" onclick="return confirm('<?php //echo lang('us_purge_del_confirm'); ?>')">-->
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset($records) && is_array($records) && count($records)) :  $no = $this->input->get('per_page')+1;?>
			<?php foreach ($records as $record) : ?>
				<tr>
					<?php if ($this->auth->has_permission('Clients.Settings.Delete')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo $record->id ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($this->auth->has_permission('Clients.Settings.Edit')) : ?>
				<td><?php echo anchor(SITE_AREA .'/settings/clients/restore_purge/'. $record->id, '<i class="icon-pencil">&nbsp;</i>' .  $record->client_name) ?></td>
				<?php else: ?>
				<td><?php echo $record->client_name ?></td>
				<?php endif; ?>
			
				<td><?php echo $record->email?></td>
				<td><?php echo $record->mobile?></td>
				<td><?php echo $record->phone?></td>
				<td><?php echo $record->fax?></td>
				<td><?php echo $record->description?></td>
				<td><?php echo $record->created_by?></td>
				<td><?php echo $record->company_id?></td>
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
			<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
			<?php $no++; endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="11">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         
	<?php echo form_close(); ?>
  <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>