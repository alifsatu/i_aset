<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($clients))
{
	$clients = (array) $clients;
}
$id = isset($clients['id']) ? $clients['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Clients</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('client_name') ? 'error' : ''; ?>">
				<?php echo form_label('Client Name'. lang('bf_form_label_required'), 'clients_client_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_client_name' class='input-sm input-s  form-control' type='text' name='clients_client_name' maxlength="255" value="<?php echo set_value('clients_client_name', isset($clients['client_name']) ? $clients['client_name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('client_name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('email') ? 'error' : ''; ?>">
				<?php echo form_label('Email'. lang('bf_form_label_required'), 'clients_email', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_email' class='input-sm input-s  form-control' type='text' name='clients_email' maxlength="255" value="<?php echo set_value('clients_email', isset($clients['email']) ? $clients['email'] : '');?>" />
					<span class='help-inline'><?php echo form_error('email'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('mobile') ? 'error' : ''; ?>">
				<?php echo form_label('Mobile', 'clients_mobile', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_mobile' class='input-sm input-s  form-control' type='text' name='clients_mobile' maxlength="255" value="<?php echo set_value('clients_mobile', isset($clients['mobile']) ? $clients['mobile'] : '');?>" />
					<span class='help-inline'><?php echo form_error('mobile'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('phone') ? 'error' : ''; ?>">
				<?php echo form_label('Phone', 'clients_phone', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_phone' class='input-sm input-s  form-control' type='text' name='clients_phone' maxlength="255" value="<?php echo set_value('clients_phone', isset($clients['phone']) ? $clients['phone'] : '');?>" />
					<span class='help-inline'><?php echo form_error('phone'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('fax') ? 'error' : ''; ?>">
				<?php echo form_label('Fax', 'clients_fax', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_fax' class='input-sm input-s  form-control' type='text' name='clients_fax' maxlength="255" value="<?php echo set_value('clients_fax', isset($clients['fax']) ? $clients['fax'] : '');?>" />
					<span class='help-inline'><?php echo form_error('fax'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'clients_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<?php echo form_textarea( array( 'name' => 'clients_description', 'id' => 'clients_description', 'class' => 'expand form-control', 'rows' => '2', 'cols' => '40', 'value' => set_value('clients_description', isset($clients['description']) ? $clients['description'] : '') ) ); ?>
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('created_by') ? 'error' : ''; ?>">
				<?php echo form_label('Created by', 'clients_created_by', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_created_by' class='input-sm input-s  form-control' type='text' name='clients_created_by' maxlength="255" value="<?php echo set_value('clients_created_by', isset($clients['created_by']) ? $clients['created_by'] : '');?>" />
					<span class='help-inline'><?php echo form_error('created_by'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('company_id') ? 'error' : ''; ?>">
				<?php echo form_label('Company Name', 'clients_company_id', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='clients_company_id' class='input-sm input-s  form-control' type='text' name='clients_company_id' maxlength="255" value="<?php echo set_value('clients_company_id', isset($clients['company_id']) ? $clients['company_id'] : '');?>" />
					<span class='help-inline'><?php echo form_error('company_id'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('clients_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/clients', lang('clients_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Clients.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('clients_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('clients_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>