<?php

$num_columns	= 11;
$can_delete	= $this->auth->has_permission('Clients.Settings.Delete');
$can_edit		= $this->auth->has_permission('Clients.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
    <?php 
 
echo form_open($this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>Clients</h4></div>
  <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">


<? if ( $this->session->userdata('clientsfield') !== NULL ) { ?>
<option  selected="selected" value="<?=$this->session->userdata('clientsfield')?>"><?=$this->session->userdata('clientsfield')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		<?
$fquery = $this->db->query('SHOW COLUMNS FROM intg_clients')->result();
foreach  ( $fquery as $frow  ) {
?>
<option value="<? echo $frow->Field; ?>"><? echo $frow->Field; ?></option>
<? } ?>
</select>

</div><div class="col-md-3"><div class="input-group">

<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=$this->session->userdata('clientsfvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=$this->session->userdata('clientsfname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=$this->session->userdata('clientsfield') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

   <?php echo form_close(); ?>
     <div class="table-responsive">   
  
	<?php echo form_open($this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if ($can_delete && $has_records) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					<th>#</th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_client_name') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=client_name&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'client_name') ? 'desc' : 'asc'); ?>'>
                    Client Name</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_email') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=email&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'email') ? 'desc' : 'asc'); ?>'>
                    Email</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_mobile') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=mobile&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'mobile') ? 'desc' : 'asc'); ?>'>
                    Mobile</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_phone') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=phone&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'phone') ? 'desc' : 'asc'); ?>'>
                    Phone</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_fax') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=fax&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'fax') ? 'desc' : 'asc'); ?>'>
                    Fax</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_description') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=description&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'description') ? 'desc' : 'asc'); ?>'>
                    Description</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_created_by') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=created_by&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'created_by') ? 'desc' : 'asc'); ?>'>
                    Created by</a></th>
                    
                   <th<?php if ($this->input->get('sort_by') == '_company_id') { echo 'class=\'sort_'.$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/settings/clients?sort_by=company_id&sort_order='.(($this->input->get('sort_order') == 'asc' && $this->input->get('sort_by') == 'company_id') ? 'desc' : 'asc'); ?>'>
                    Company Name</a></th>
					<th><?php echo lang("clients_column_created"); ?></th>
					<th><?php echo lang("clients_column_modified"); ?></th>
				</tr>
			</thead>
			<?php if ($has_records) : ?>
			<tfoot>
				<?php if ($can_delete) : ?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">
						<?php echo lang('bf_with_selected'); ?>
						<input type="submit" name="delete" id="delete-me" class="btn btn-danger" value="<?php echo lang('bf_action_delete'); ?>" onclick="return confirm('<?php e(js_escape(lang('clients_delete_confirm'))); ?>')" />
					</td>
				</tr>
				<?php endif; ?>
			</tfoot>
			<?php endif; ?>
			<tbody>
				<?php
				if ($has_records) : $no = $this->input->get('per_page')+1;
					foreach ($records as $record) : 
				?>
				<tr>
					<?php if ($can_delete) : ?>
					<td class="column-check"><input type="checkbox" name="checked[]" value="<?php echo $record->id; ?>" /></td><td><?=$no;?></td>
					<?php endif;?>
					
				<?php if ($can_edit) : ?>
					<td><?php echo anchor(SITE_AREA . '/settings/clients/edit/' . $record->id, $record->client_name ); ?>&nbsp;<i class="fa fa-pencil"></i> </td>
				<?php else : ?>
					<td><?php e($record->client_name); ?></td>
				<?php endif; ?>
					<td><?php e($record->email) ?></td>
					<td><?php e($record->mobile) ?></td>
					<td><?php e($record->phone) ?></td>
					<td><?php e($record->fax) ?></td>
					<td><?php e($record->description) ?></td>
					<td><?php e($record->created_by) ?></td>
					<td><?php e($record->company_id) ?></td>
					<td><?php echo date("d/m/Y h:i:s",strtotime($record->created_on)); ?></td>
					<td><?php echo $record->modified_on=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->modified_on)); ?></td>
				</tr>
				<?php $no++;
					endforeach;
				else:
				?>
				<tr>
					<td colspan="<?php echo $num_columns; ?>">No records found that match your selection.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
       
	<?php echo form_close(); ?>
    
    <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=$offset+1?> - <? echo $rowcount+$offset?> Of  <?  echo $total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo $this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
    
