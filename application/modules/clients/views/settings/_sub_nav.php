
<ul class="nav nav-pills">

	<li <?php echo $this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/settings/clients') ?>"style="border-radius:0"  id="list"><?php echo lang('clients_list'); ?></a>

	</li>

	<?php if ($this->auth->has_permission('Clients.Settings.Create')) : ?>

	<li <?php echo $this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/clients/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('clients_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if ($this->auth->has_permission('Clients.Settings.Deleted')) : ?>

    	<li <?php echo $this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/settings/clients/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>
