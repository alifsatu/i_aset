<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($main))
{
	$main = (array) $main;
}
$id = isset($main['id']) ? $main['id'] : '';

?>
<div class="row">
	<h3>main</h3>
	<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"'); ?>
		<fieldset>

			<div class="control-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name', 'main_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='main_name' type='text' name='main_name' maxlength="255" value="<?php echo set_value('main_name', isset($main['name']) ? $main['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('main_action_edit'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/main', lang('main_cancel'), 'class="btn btn-warning"'); ?>
				
			<?php if ($this->auth->has_permission('Main.Settings.Delete')) : ?>
				&nbsp;&nbsp;
				<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?php e(js_escape(lang('main_delete_confirm'))); ?>'); ">
					<span class="icon-trash icon-white"></span>&nbsp;<?php echo lang('main_delete_record'); ?>
				</button>
			<?php endif; ?>
			</div>
		</fieldset>
    <?php echo form_close(); ?>
</div>