<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($main))
{
	$main = (array) $main;
}
$id = isset($main['id']) ? $main['id'] : '';

?>
<div class="row">
                <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading clearfix">main</header>
                    <div class="panel-body">
                          

	<?php echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>
    	<fieldset>
	

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name', 'main_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='main_name'  class="input-sm input-s  form-control" type='text' name='main_name' maxlength="255" value="<?php echo set_value('main_name', isset($main['name']) ? $main['name'] : ''); ?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-actions">
				<input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('main_action_create'); ?>"  />
				&nbsp;&nbsp;
				<?php echo anchor(SITE_AREA .'/settings/main', lang('main_cancel'), 'class="btn btn-warning"'); ?>
				
			 </div>
					
					</fieldset>
					<?php echo form_close(); ?>
                    
                    
                    </div>
                    </section>
			</div><!--end of col6-->
		
    
</div> <!--end of row-->