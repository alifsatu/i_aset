<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['main_manage']			= 'Manage';
$lang['main_edit']				= 'Edit';
$lang['main_true']				= 'True';
$lang['main_false']				= 'False';
$lang['main_create']			= 'Save';
$lang['main_list']				= 'List';
$lang['main_new']				= 'New';
$lang['main_edit_text']			= 'Edit this to suit your needs';
$lang['main_no_records']		= 'There aren\'t any main in the system.';
$lang['main_create_new']		= 'Create a new main.';
$lang['main_create_success']	= 'main successfully created.';
$lang['main_create_failure']	= 'There was a problem creating the main: ';
$lang['main_create_new_button']	= 'Create New main';
$lang['main_invalid_id']		= 'Invalid main ID.';
$lang['main_edit_success']		= 'main successfully saved.';
$lang['main_edit_failure']		= 'There was a problem saving the main: ';
$lang['main_delete_success']	= 'record(s) successfully deleted.';

$lang['main_purged']	= 'record(s) successfully purged.';
$lang['main_success']	= 'record(s) successfully restored.';


$lang['main_delete_failure']	= 'We could not delete the record: ';
$lang['main_delete_error']		= 'You have not selected any records to delete.';
$lang['main_actions']			= 'Actions';
$lang['main_cancel']			= 'Cancel';
$lang['main_delete_record']		= 'Delete';
$lang['main_delete_confirm']	= 'Are you sure you want to delete this main?';
$lang['main_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['main_action_edit']		= 'Save';
$lang['main_action_create']		= 'Create';

// Activities
$lang['main_act_create_record']	= 'Created record with ID';
$lang['main_act_edit_record']	= 'Updated record with ID';
$lang['main_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['main_column_created']	= 'Created';
$lang['main_column_deleted']	= 'Deleted';
$lang['main_column_modified']	= 'Modified';
