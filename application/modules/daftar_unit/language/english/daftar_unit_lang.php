<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_unit_manage']			= 'Manage';
$lang['daftar_unit_edit']				= 'Edit';
$lang['daftar_unit_true']				= 'True';
$lang['daftar_unit_false']				= 'False';
$lang['daftar_unit_create']			= 'Save';
$lang['daftar_unit_list']				= 'List';
$lang['daftar_unit_new']				= 'New';
$lang['daftar_unit_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_unit_no_records']		= 'There aren\'t any daftar_unit in the system.';
$lang['daftar_unit_create_new']		= 'Create a new Daftar Unit.';
$lang['daftar_unit_create_success']	= 'Daftar Unit successfully created.';
$lang['daftar_unit_create_failure']	= 'There was a problem creating the daftar_unit: ';
$lang['daftar_unit_create_new_button']	= 'Create New Daftar Unit';
$lang['daftar_unit_invalid_id']		= 'Invalid Daftar Unit ID.';
$lang['daftar_unit_edit_success']		= 'Daftar Unit successfully saved.';
$lang['daftar_unit_edit_failure']		= 'There was a problem saving the daftar_unit: ';
$lang['daftar_unit_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_unit_purged']	= 'record(s) successfully purged.';
$lang['daftar_unit_success']	= 'record(s) successfully restored.';


$lang['daftar_unit_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_unit_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_unit_actions']			= 'Actions';
$lang['daftar_unit_cancel']			= 'Cancel';
$lang['daftar_unit_delete_record']		= 'Delete';
$lang['daftar_unit_delete_confirm']	= 'Are you sure you want to delete this daftar_unit?';
$lang['daftar_unit_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_unit_action_edit']		= 'Save';
$lang['daftar_unit_action_create']		= 'Create';

// Activities
$lang['daftar_unit_act_create_record']	= 'Created record with ID';
$lang['daftar_unit_act_edit_record']	= 'Updated record with ID';
$lang['daftar_unit_act_delete_record']	= 'Deleted record with ID';

// Column Headings
$lang['daftar_unit_column_created']	= 'Created';
$lang['daftar_unit_column_deleted']	= 'Deleted';
$lang['daftar_unit_column_modified']	= 'Modified';
