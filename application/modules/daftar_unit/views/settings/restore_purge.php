<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_unit))
{
	$daftar_unit = (array) $daftar_unit;
}
$id = isset($daftar_unit['id']) ? $daftar_unit['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Unit</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<div class="form-group <?php echo form_error('name') ? 'error' : ''; ?>">
				<?php echo form_label('Name'. lang('bf_form_label_required'), 'daftar_unit_name', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_unit_name' class='input-sm input-s  form-control' type='text' name='daftar_unit_name' maxlength="255" value="<?php echo set_value('daftar_unit_name', isset($daftar_unit['name']) ? $daftar_unit['name'] : '');?>" />
					<span class='help-inline'><?php echo form_error('name'); ?></span>
				</div>
			</div>

			<div class="form-group <?php echo form_error('description') ? 'error' : ''; ?>">
				<?php echo form_label('Description', 'daftar_unit_description', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_unit_description' class='input-sm input-s  form-control' type='text' name='daftar_unit_description' maxlength="255" value="<?php echo set_value('daftar_unit_description', isset($daftar_unit['description']) ? $daftar_unit['description'] : '');?>" />
					<span class='help-inline'><?php echo form_error('description'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/daftar_unit', lang('daftar_unit_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Daftar_Unit.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('daftar_unit_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>