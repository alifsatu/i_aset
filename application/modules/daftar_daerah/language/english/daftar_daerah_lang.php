<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['daftar_daerah_manage']			= 'Manage';
$lang['daftar_daerah_edit']				= 'Edit';
$lang['daftar_daerah_true']				= 'True';
$lang['daftar_daerah_false']				= 'False';
$lang['daftar_daerah_create']			= 'Save';
$lang['daftar_daerah_list']				= 'List';
$lang['daftar_daerah_new']				= 'New';
$lang['daftar_daerah_edit_text']			= 'Edit this to suit your needs';
$lang['daftar_daerah_no_records']		= 'There aren\'t any daftar_daerah in the system.';
$lang['daftar_daerah_create_new']		= 'Create a new Daftar Daerah.';
$lang['daftar_daerah_create_success']	= 'Daftar Daerah successfully created.';
$lang['daftar_daerah_create_failure']	= 'There was a problem creating the daftar_daerah: ';
$lang['daftar_daerah_create_new_button']	= 'Create New Daftar Daerah';
$lang['daftar_daerah_invalid_id']		= 'Invalid Daftar Daerah ID.';
$lang['daftar_daerah_edit_success']		= 'Daftar Daerah successfully saved.';
$lang['daftar_daerah_edit_failure']		= 'There was a problem saving the daftar_daerah: ';
$lang['daftar_daerah_delete_success']	= 'record(s) successfully deleted.';

$lang['daftar_daerah_purged']	= 'record(s) successfully purged.';
$lang['daftar_daerah_success']	= 'record(s) successfully restored.';


$lang['daftar_daerah_delete_failure']	= 'We could not delete the record: ';
$lang['daftar_daerah_delete_error']		= 'You have not selected any records to delete.';
$lang['daftar_daerah_actions']			= 'Actions';
$lang['daftar_daerah_cancel']			= 'Cancel';
$lang['daftar_daerah_delete_record']		= 'Delete';
$lang['daftar_daerah_delete_confirm']	= 'Are you sure you want to delete this daftar_daerah?';
$lang['daftar_daerah_edit_heading']		= 'Edit';

// Create/Edit Buttons
$lang['daftar_daerah_action_edit']		= 'Save';
$lang['daftar_daerah_action_create']		= 'Create';

// Activities
$lang['daftar_daerah_act_create_record']	= 'Created record with ID';
$lang['daftar_daerah_act_edit_record']	= 'Updated record with ID';
$lang['daftar_daerah_act_delete_record']	= 'Deleted record with ID';
$lang['daftar_daerah_restored_success']	= 'Restored record with ID';
$lang['daftar_daerah_restored_error']	= 'Restoration failed with record ID';

// Column Headings
$lang['daftar_daerah_column_created']	= 'Created';
$lang['daftar_daerah_column_deleted']	= 'Deleted';
$lang['daftar_daerah_column_modified']	= 'Modified';
