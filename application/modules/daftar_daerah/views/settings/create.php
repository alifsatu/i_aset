<?php
$validation_errors = validation_errors();

if ($validation_errors) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <h4 class="alert-heading">Please fix the following errors:</h4>
        <?php echo $validation_errors; ?>
    </div>
    <?php
endif;

if (isset($daftar_daerah)) {
    $daftar_daerah = (array) $daftar_daerah;
}
$id = isset($daftar_daerah['id']) ? $daftar_daerah['id'] : '';
?>
<div class="row">
    <div class="col-sm-6">
        <section class="panel panel-default">
            <header class="panel-heading font-bold">Daftar Daerah</header>
            <div class="panel-body">



                <?php echo form_open($this->uri->uri_string(), 'id="forma" data-validate="parsley"'); ?>
                <fieldset>

                    <label><?= 'Negeri' . lang('bf_form_label_required') ?></label>
                    <div class='controls'>
                        <select name="daftar_daerah_id_negeri" id="daftar_daerah_id_negeri" class="form-control selecta"   data-required="true"  style="height:auto; font-size:12px; width:200px">
                            <option value="">--Sila Pilih--</option>
                            <?php
                            foreach ($negeri as $n) {
                                if ($n->id == $daftar_daerah['id_negeri']) {
                                    ?>
                                    <option selected="selected" value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                <?php } else { ?>
                                    <option value="<?= $n->id ?>"><?= $n->nama; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
                        <?php echo form_label('Nama Daerah' . lang('bf_form_label_required'), 'daftar_daerah_nama', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='daftar_daerah_nama' class='input-sm input-s  form-control' type='text' name='daftar_daerah_nama' maxlength="255" value="<?php echo set_value('daftar_daerah_nama', isset($daftar_daerah['nama']) ? $daftar_daerah['nama'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>
                    
                    <div class="form-group <?php echo form_error('id_old') ? 'error' : ''; ?>">
                        <?php echo form_label('ID Sistem Lama' . lang('bf_form_label_required'), 'daftar_daerah_id_old', array('class' => 'control-label')); ?>
                        <div class='controls'>
                            <input id='daftar_daerah_id_old' class='input-sm input-s  form-control' type='text' name='daftar_daerah_id_old' maxlength="255" value="<?php echo set_value('daftar_daerah_id_old', isset($daftar_daerah['id_old']) ? $daftar_daerah['id_old'] : ''); ?>" />
                            <span class='help-inline'><?php echo form_error('id_old'); ?></span>
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('daftar_daerah_action_create'); ?>"  />
                        &nbsp;&nbsp;
                        <?php echo anchor(SITE_AREA . '/settings/daftar_daerah', lang('daftar_daerah_cancel'), 'class="btn btn-warning"'); ?>

                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>