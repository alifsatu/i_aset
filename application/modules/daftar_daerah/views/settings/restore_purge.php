<?php

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<h4 class="alert-heading">Please fix the following errors:</h4>
	<?php echo $validation_errors; ?>
</div>
<?php
endif;

if (isset($daftar_daerah))
{
	$daftar_daerah = (array) $daftar_daerah;
}
$id = isset($daftar_daerah['id']) ? $daftar_daerah['id'] : '';

?>
 <div class="row">
 <div class="col-sm-6">
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">Daftar Daerah</header>
                    <div class="panel-body">
					
						
 
	<?php echo form_open($this->uri->uri_string(),    'id="forma" data-validate="parsley"'); ?>
		<fieldset>

			<?php // Change the values in this array to populate your dropdown as required
				$options = array(
					11 => 11,
				);

				echo form_dropdown('daftar_daerah_id_negeri', $options, set_value('daftar_daerah_id_negeri', isset($daftar_daerah['id_negeri']) ? $daftar_daerah['id_negeri'] : ''), 'Negeri'. lang('bf_form_label_required'));
			?>

			<div class="form-group <?php echo form_error('nama') ? 'error' : ''; ?>">
				<?php echo form_label('Nama Daerah'. lang('bf_form_label_required'), 'daftar_daerah_nama', array('class' => 'control-label') ); ?>
				<div class='controls'>
					<input id='daftar_daerah_nama' class='input-sm input-s  form-control' type='text' name='daftar_daerah_nama' maxlength="255" value="<?php echo set_value('daftar_daerah_nama', isset($daftar_daerah['nama']) ? $daftar_daerah['nama'] : '');?>" />
					<span class='help-inline'><?php echo form_error('nama'); ?></span>
				</div>
			</div>


        <div class="form-actions">
            <br/>
			
            <input type="submit" name="restore" class="btn btn-primary" value="Restore" />
            &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/settings/daftar_daerah', lang('daftar_daerah_cancel'), 'class="btn btn-warning"'); ?>
            

    <?php if ($this->auth->has_permission('Daftar_Daerah.Settings.Delete')) : ?>

            &nbsp;&nbsp; <button type="submit" name="purge" class="btn btn-danger" id="purge-me" onclick="return confirm('<?php echo lang('daftar_daerah_delete_confirm'); ?>')">
            <i class="icon-trash icon-white">&nbsp;</i>&nbsp;Purge
            </button>

    <?php endif; ?>


			
			
        </div>
    </fieldset>
    <?php echo form_close(); ?>


</div>