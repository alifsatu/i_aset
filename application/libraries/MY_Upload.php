<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* class Upload
*
* @package application/libraries
* @subpackage Libraries
* @author Lykourgos Tsirikos
*/
class MY_Upload extends CI_Upload
{	
	/**
	* Hold multiple errors
	* @var array
	*/
	public $multi_errors = array();

	/**
	* Hold multiple upload file data
	* @var array
	*/
	public $multi_upload_data = array();

	/**
	* Upload multiple files.
	* 1st param - The name attribute of the input field
	* 	i.e. <input type="file" name="userfile[]" />
	*
	* 2nd param - The configuration settings for doing the upload
	* 
	* @param string $field
	* @param array 	$config
	*/
	public function do_multi_upload($field = 'files', $config) {
		
		// check if $_FILES[$field] is set
		// if not, no reason to continue
		if (!isset($_FILES[$field])) {
			return false; 
		}

		// check if it's a multiple upload
		// if not then fall back to CI do_upload()
		if (!is_array($_FILES[$field]['name'])) {
			return $this->do_upload($field);
		}

		$files = $_FILES[$field];
		
		// try to upload each file
		foreach ($files['name'] as $key => $value) {
			$_FILES[$field]['name'] = $files['name'][$key];
            		$_FILES[$field]['type'] = $files['type'][$key];
            		$_FILES[$field]['tmp_name'] = $files['tmp_name'][$key];
            		$_FILES[$field]['error'] = $files['error'][$key];
            		$_FILES[$field]['size'] = $files['size'][$key];

            		$this->initialize($config);
		        if (!$this->do_upload($field)) {
		            	// oops, an error occured
				// specify the name of the file and the error that occured
				$this->multi_errors[] = 'File: ' . $_FILES[$field]['name'] . ' - Error: ' . $this->display_errors('', '');
		        } else {
            			// file uploaded successfully
				// lets get the upload data of the file
				$this->multi_upload_data[] = $this->data();
            		}
		}
	}

	/**
	* Display the errors for files that failed to upload
	*
	* @param 	string 	$start
	* @param 	string 	$end
	* @return 	string
	*/
	public function display_multi_errors($start = '<p>', $end = '</p>'){
		$output = '';

		if (sizeof($this->multi_errors) > 0) {
			foreach ($this->multi_errors as $error) {
				// <p> . {the error} . </p>
				$output .= $start . $error . $end . PHP_EOL;
			}
		}
		return $output;
	}

	/**
	* Display the upload data for files that uploaded successfully
	*
	* @return 	mixed 	array/string
	*/
	public function multi_data(){
		if (sizeof($this->multi_upload_data) > 0) {
			return $this->multi_upload_data;
		}
		return '';
	}
}

/* End of file MY_Upload.php */
/* Location: ./application/libraries/MY_Upload.php */
