<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  

use Dompdf\Dompdf;
use Dompdf\Options;
require_once APPPATH."/third_party/dompdf/autoload.inc.php";

class MY_Dompdf extends Dompdf {
    public function __construct() {
        parent::__construct();
    }
}