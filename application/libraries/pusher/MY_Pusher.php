<?php 

class MY_Pusher {

	private $pusher;

	function __construct()
	{
		$ci =& get_instance();
	
		$ci->load->config('pusher');
		
		require_once('Pusher.php');
		$this->pusher = new Pusher($ci->config->item('app_key'), $ci->config->item('app_secret'), $ci->config->item('app_id'));
	}

	function notify($user_id, $data, $event = 'notify') 
	{
		if(is_string($data))
			$this->pusher->trigger($user_id.'_channel', $event, array('title' => 'You\'ve one notification', 'message' => $message));
		else if(is_array($data))
			$this->pusher->trigger($user_id.'_channel', $event, $data);
	}
}