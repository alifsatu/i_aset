<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



/**

 * ideabank controller

 */

class ideabank extends Admin_Controller

{



	//--------------------------------------------------------------------





	/**

	 * Constructor

	 *

	 * @return void

	 */

	public function __construct()

	{

		parent::__construct();



		$this->auth->restrict('Ideas.Ideabank.View');

		$this->load->model('ideas_model', null, true);

		$this->lang->load('ideas');

		
Assets::add_css('js/slider/slider.css');
		Assets::add_css('js/fuelux/fuelux.css');
		
		
		Assets::add_js("js/parsley/parsley.min.js");
		Assets::add_js("js/parsley/parsley.extend.js");
			Assets::add_js('js/wysiwyg/jquery.hotkeys.js');
			Assets::add_js('js/wysiwyg/bootstrap-wysiwyg.js');
			Assets::add_js('js/wysiwyg/demo.js');
			


		Template::set_block('sub_nav', 'ideabank/_sub_nav');

		Assets::add_module_js('ideas', 'ideas.js');
		Assets::add_module_js('ideas', 'like.js');
		Assets::add_module_js('ideas', 'dislike.js');

	}



	//--------------------------------------------------------------------





	/**

	 * Displays a list of form data.

	 *

	 * @return void

	

	public function index() */

	public function index($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')

	{

		$this->load->library('session');

		$this->load->library('pagination');

		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());		

		$user_id =  $this->auth->user_id();



		// Deleting anything?

		if (isset($_POST['delete']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					$result = $this->ideas_model->delete($pid);

				}



				if ($result)

				{

					Template::set_message(count($checked) .' '. lang('ideas_delete_success'), 'success');

				}

				else

				{

					Template::set_message(lang('ideas_delete_failure') . $this->ideas_model->error, 'error');

				}

			}

		}

		

				

switch ( $this->input->post('submit') ) 

{

case 'Search':

$this->session->set_userdata('sapfield',$this->input->post('select_field'));

$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));

$this->session->set_userdata('sapfname',$this->input->post('field_name'));





break;

case 'Reset':

$this->session->unset_userdata('sapfield');

$this->session->unset_userdata('sapfvalue');

$this->session->unset_userdata('sapfname');

break;

}



if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }

else { $field = 'intg_ideas.id'; }



if ( $this->session->userdata('sapfield') =='All Fields') 

{ 

 		
 $this->db->select('*,intg_ideas.id as idea_id');
$this->db->join('intg_idea_categories','intg_idea_categories.id=intg_ideas.idea_category_id');
$this->db->join('intg_idea_themes','intg_idea_themes.id=intg_ideas.theme_id');
  $total = $this->ideas_model

   ->where('deleted','0')

  ->likes('title',$this->session->userdata('sapfvalue'),'both') 

  ->likes('idea_description',$this->session->userdata('sapfvalue'),'both') 

  ->likes('description',$this->session->userdata('sapfvalue'),'both')

 ->likes('category_id',$this->session->userdata('sapfvalue'),'both')

  ->count_all();

  

}

else

{

  		
 $this->db->select('*,intg_ideas.id as idea_id');
$this->db->join('intg_idea_categories','intg_idea_categories.id=intg_ideas.idea_category_id');	
$this->db->join('intg_idea_themes','intg_idea_themes.id=intg_ideas.theme_id');
  $total = $this->ideas_model

    ->where('intg_ideas.deleted','0')

  ->likes($field,$this->session->userdata('sapfvalue'),'both')  

  ->count_all();

}



		//$records = $this->ideas_model->find_all();

		/**************************Pagination********************************/

$offset = $this->input->get('per_page');

$this->pager['base_url'] = current_url() .'?';

$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');

$this->pager['total_rows'] = $total;

$this->pager['per_page'] = 5;

$this->pager['num_links'] = 20;

$this->pager['page_query_string']= TRUE;

$this->pager['uri_segment'] = 5;


switch ($this->config->item('template.admin_theme') )
{
case "admin" :	$this->pager['full_tag_open'] = '<div class="pagination pagination-right"><ul>'; break;
case "todo" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;
case "notebook" :	$this->pager['full_tag_open'] = '<ul class="pagination pagination-sm m-t-sm m-b-none">'; break;


	
	
}




if ( $this->session->userdata('sapfield') =='All Fields') 

{
 		
 $this->db->select('*,intg_ideas.id as idea_id');
$this->db->join('intg_idea_categories','intg_idea_categories.id=intg_ideas.idea_category_id');
$this->db->join('intg_idea_themes','intg_idea_themes.id=intg_ideas.theme_id');
$records = $this->ideas_model

 

    ->where('intg_ideas.deleted','0')

  ->likes('title',$this->session->userdata('sapfvalue'),'both') 

  ->likes('idea_description',$this->session->userdata('sapfvalue'),'both') 

  ->likes('description',$this->session->userdata('sapfvalue'),'both')

 ->likes('category_id',$this->session->userdata('sapfvalue'),'both')

  ->limit($this->pager['per_page'], $offset)

	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 

                 ->find_all();

	 

}

else

{

	if ( $this->input->get('sort_by') !='' )

	{

		$ord1 = array (	

	$this->input->get('sort_by') => $this->input->get('sort_order'),

	'intg_ideas.id'=>'desc',

	 );		

	}

	else { $ord1 = array (	

		'intg_ideas.id'=>'desc',

	 );	}

	 	 	
 		
 $this->db->select('*,intg_ideas.id as idea_id');
$this->db->join('intg_idea_categories','intg_idea_categories.id=intg_ideas.idea_category_id');
$this->db->join('intg_idea_themes','intg_idea_themes.id=intg_ideas.theme_id');
//$this->db->join('intg_clients','intg_clients.id=intg_quote.client_id');
$records = $this->ideas_model

  ->where('intg_ideas.deleted','0')

->likes($field,$this->session->userdata('sapfvalue'),'both')

->limit($this->pager['per_page'], $offset)

	->order_by( $ord1 ) 

                 ->find_all();	

		

}

//$records = $this->ideas_model->find_all();

		echo $this->db->last_query();

$this->pagination->initialize($this->pager);
Template::set('current_url', current_url());
Template::set('sort_by', $sort_by);
Template::set('role_name', $role_name);
Template::set('user_id', $user_id);
Template::set('sort_order', $sort_order);
Template::set('filter', $filter);
Template::set('total',$total);
Template::set('offset',$offset);
Template::set('rowcount',$this->pager['per_page']);
 Assets::add_module_css('quote', 'style.css');



		Template::set('records', $records);

		Template::set('toolbar_title', 'Manage Ideas');

		Template::render();

	}



	//--------------------------------------------------------------------

function view(){
$id = $this->uri->segment(5);

Template::set('ideas', $this->ideas_model->find($id));

		//Template::set('toolbar_title', lang('ideas_edit') .' Ideas');
		//Template::set('quote', $this->quote_model->find($id));
		Template::set('toolbar_title', ' Idea');

	Template::render();}

function quickapprove(){	Template::render();}
	function updateApprovalstatus(){	Template::render();}

public function modal() { Template::render();}
	
public function add_upd_rows() { Template::render();}	

	

public function wall()
{
	$records = $this->ideas_model->find_all();
	Template::set('records', $records);
	Template::render();
}
	

public function comments()
{
Template::render();
}

	

	public function deleted($limit = 0, $filter = NULL, $per_page = NULL,$sort_by = 'id', $sort_order = 'asc')

	{

		$this->load->library('session');

		$this->load->library('pagination');

		 $role_name = $this->auth->role_name_by_id( $role_id = $this->auth->role_id());

		

		$user_id =  $this->auth->user_id();

		

				

				

if (isset($_POST['restore']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					

					$result = $this->db->query('UPDATE intg_ideas SET deleted = 0 where id = '.$pid.'');			

				}

				

											



				if ($result)

				{

					Template::set_message(lang('ideas_success'), 'success');

				}

				else

				{

					Template::set_message(lang('ideas_restored_error'). $this->ideas_model->error, 'error');

				}

			}

		}

		

		if (isset($_POST['purge']))

		{

			$checked = $this->input->post('checked');



			if (is_array($checked) && count($checked))

			{

				$result = FALSE;

				foreach ($checked as $pid)

				{

					$result = 	$this->db->query('delete from intg_ideas where id = '.$pid.'');

		

				}



				if ($result)

				{

					

					Template::set_message(count($checked) .' '. lang('ideas_purged'), 'success');

				}

				

			}

		}

		

switch ( $this->input->post('submit') ) 

{

case 'Search':

$this->session->set_userdata('sapfield',$this->input->post('select_field'));

$this->session->set_userdata('sapfvalue',$this->input->post('field_value'));

$this->session->set_userdata('sapfname',$this->input->post('field_name'));





break;

case 'Reset':

$this->session->unset_userdata('sapfield');

$this->session->unset_userdata('sapfvalue');

$this->session->unset_userdata('sapfname');

break;

}





if ( $this->session->userdata('sapfield')!='') { $field=$this->session->userdata('sapfield'); }

else { $field = 'id'; }



if ( $this->session->userdata('sapfield') =='All Fields') 

{ 

 		

  $total = $this->ideas_model

  ->where('deleted','1')

  ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both') 

  ->count_all();





}

else

{

	

  $total = $this->ideas_model

   ->where('deleted','1')

  ->likes($field,$this->session->userdata('sapfvalue'),'both')  

  ->count_all();

}



//$records = $this-ideas_model->find_all();

/**************************Pagination********************************/

$offset = $this->input->get('per_page');

$this->pager['base_url'] = current_url() .'?';

$this->pager['base_url'] = current_url() .'?sort_by='.$this->input->get('sort_by').'&sort_order='.$this->input->get('sort_order');

$this->pager['total_rows'] = $total;

$this->pager['per_page'] = 5;

$this->pager['num_links'] = 20;

$this->pager['page_query_string']= TRUE;

$this->pager['uri_segment'] = 5;





if ( $this->session->userdata('sapfield') =='All Fields') 

{

$records = $this->personal_particulars_model

 

  ->where('deleted','1')

   ->likes('personal_particulars_name',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_nric',$this->session->userdata('sapfvalue'),'both') 

  ->likes('personal_particulars_htel',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_email',$this->session->userdata('sapfvalue'),'both')

  ->likes('personal_particulars_citizenship',$this->session->userdata('sapfvalue'),'both')

 

 



->limit($this->pager['per_page'], $offset)

	->order_by( $this->input->get('sort_by'),$this->input->get('sort_order')) 

                 ->find_all();

	 

}

else

{

	if ( $this->input->get('sort_by') !='' )

	{

		$ord1 = array (	

	$this->input->get('sort_by') => $this->input->get('sort_order'),

	'id'=>'desc',

	 );		

	}

	else { $ord1 = array (	

		'id'=>'desc',

	 );	}

	 

	 	

$records = $this->ideas_model

->where('deleted','1')

->likes($field,$this->session->userdata('sapfvalue'),'both')

->limit($this->pager['per_page'], $offset)

	->order_by( $ord1 ) 

                 ->find_all();	

		

}



		//$records = $this->ideas_model->find_all();

		

$this->pagination->initialize($this->pager);

Template::set('current_url', current_url());

Template::set('sort_by', $sort_by);

Template::set('role_name', $role_name);

Template::set('user_id', $user_id);

Template::set('sort_order', $sort_order);

Template::set('filter', $filter);

 Assets::add_module_css('ideas', 'style.css');



		Template::set('records', $records);

		Template::set('toolbar_title', 'Manage Ideas');

		Template::render();

	}



	//--------------------------------------------------------------------







	/**

	 * Creates a Ideas object.

	 *

	 * @return void

	 */

	public function create()

	{

		$this->auth->restrict('Ideas.Ideabank.Create');



		if (isset($_POST['save']))

		{

			if ($insert_id = $this->save_ideas())

			{

				// Log the activity

				log_activity($this->current_user->id, lang('ideas_act_create_record') .': '. $insert_id .' : '. $this->input->ip_address(), 'ideas');



				Template::set_message(lang('ideas_create_success'), 'success');

				redirect(SITE_AREA .'/ideabank/ideas');

			}

			else

			{

				Template::set_message(lang('ideas_create_failure') . $this->ideas_model->error, 'error');

			}

		}

		Assets::add_module_js('ideas', 'ideas.js');



		Template::set('toolbar_title', lang('ideas_create') . ' Ideas');

		Template::render();

	}



	//--------------------------------------------------------------------





	/**

	 * Allows editing of Ideas data.

	 *

	 * @return void

	 */

	public function edit()

	{

		$id = $this->uri->segment(5);



		if (empty($id))

		{

			Template::set_message(lang('ideas_invalid_id'), 'error');

			redirect(SITE_AREA .'/ideabank/ideas');

		}



		if (isset($_POST['save']))

		{

			$this->auth->restrict('Ideas.Ideabank.Edit');



			if ($this->save_ideas('update', $id))

			{

				// Log the activity

				log_activity($this->current_user->id, lang('ideas_act_edit_record') .': '. $id .' : '. $this->input->ip_address(), 'ideas');



				Template::set_message(lang('ideas_edit_success'), 'success');

			}

			else

			{

				Template::set_message(lang('ideas_edit_failure') . $this->ideas_model->error, 'error');

			}

		}

		else if (isset($_POST['delete']))

		{

			$this->auth->restrict('Ideas.Ideabank.Delete');



			if ($this->ideas_model->delete($id))

			{

				// Log the activity

				log_activity($this->current_user->id, lang('ideas_act_delete_record') .': '. $id .' : '. $this->input->ip_address(), 'ideas');



				Template::set_message(lang('ideas_delete_success'), 'success');



				redirect(SITE_AREA .'/ideabank/ideas');

			}

			else

			{

				Template::set_message(lang('ideas_delete_failure') . $this->ideas_model->error, 'error');

			}

		}

		Template::set('ideas', $this->ideas_model->find($id));

		Template::set('toolbar_title', lang('ideas_edit') .' Ideas');

		Template::render();

	}



	//--------------------------------------------------------------------







public function restore_purge()

	{

		$id = $this->uri->segment(5);

		if (isset($_POST['restore']))

		{

				

					$result = $this->db->query('UPDATE intg_ideas SET deleted = 0 where id = '.$id.'');			



				if ($result)

				{

						Template::set_message(lang('ideas_success'), 'success');

											redirect(SITE_AREA .'/ideabank/ideas/deleted');

				}

				else

				{

					

					Template::set_message(lang('ideas_error') . $this->ideas_model->error, 'error');

				}

		}

		

		

		if (isset($_POST['purge']))

		{

					

					$result = 	$this->db->query('delete from intg_ideas where id = '.$id.'');		



				if ($result)

				{

					

						Template::set_message(lang('ideas_purged'), 'success');

					redirect(SITE_AREA .'/ideabank/ideas/deleted');

				}

				

			

		}

		



		if (empty($id))

		{

				Template::set_message(lang('ideas_invalid_id'), 'error');

			redirect(SITE_AREA .'/ideabank/ideas');

		}



		

		

		

		Template::set('ideas', $this->ideas_model->find($id));

		

		Assets::add_module_js('ideas', 'ideas.js');



		Template::set('toolbar_title', 'Restore / Purge' . ' Ideas');

		Template::render();

	}





	//--------------------------------------------------------------------

	// !PRIVATE METHODS

	//--------------------------------------------------------------------



	/**

	 * Summary

	 *

	 * @param String $type Either "insert" or "update"

	 * @param Int	 $id	The ID of the record to update, ignored on inserts

	 *

	 * @return Mixed    An INT id for successful inserts, TRUE for successful updates, else FALSE

	 */

	private function save_ideas($type='insert', $id=0)

	{

		if ($type == 'update')

		{

			$_POST['id'] = $id;

		}



		// make sure we only pass in the fields we want

		$config['upload_path'] = '../tests/upload/';
$config['allowed_types'] = 'gif|jpg|png|PNG|JPG|JPEG|jpeg';
$config['max_size'] = '10000000';
$config['max_width'] = '0';
$config['max_height'] = '0';
$config['encrypt_name'] = FALSE;

$this->load->library('upload', $config);
$this->upload->initialize($config);

		$data = array();
			$data['parent_Rid'] = $this->input->post('parent_Rid');
		
		
		$queryfappr = $this->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = '.$this->auth->role_id().' AND rolename = work_flow_fappr  and work_flow_module_id = "37" ORDER BY  id asc');		
		if ( $queryfappr->num_rows() > 0 ) { $rowfappr = $queryfappr->row(); $typeworkflow = "role"; $dataroleuser = $this->auth->role_id(); } else {
		// if final approver is not found based on role, search based on given id	
		$queryfappr = $this->db->query('SELECT work_flow_approvers FROM intg_work_flow WHERE work_flow_doc_generator = '.$this->auth->user_id().' AND rolename = work_flow_fappr  and work_flow_module_id = "37" ORDER BY  id asc');		
		$rowfappr = $queryfappr->row(); $typeworkflow = "user"; $dataroleuser = $this->auth->user_id();
		
		}
		//echo $this->db->last_query(); echo $typeworkflow; exit;
		$data['final_approvers'] = $rowfappr->work_flow_approvers;
		$data['initiator'] = $dataroleuser;
		
		$data['user_id']          = $this->auth->user_id();
		
		
		
		
		//print_r($_POST);
		//exit;
		$data['title']        = $this->input->post('ideas_title');
		$data['idea_description']        = $this->input->post('ideas_description');
		$data['posted_by']        = $this->input->post('ideas_posted_by');
		$data['is_draft']        = $this->input->post('ideas_is_draft');
		$data['is_project']        = $this->input->post('ideas_is_project');
		$data['views']        = $this->input->post('ideas_views');
		$data['likes']        = $this->input->post('ideas_likes');
		$data['dislike']        = $this->input->post('ideas_dislike');
		$data['theme_id']        = $this->input->post('ideas_theme_id');
		$data['report_count']        = $this->input->post('ideas_report_count');
		$data['story_behind']        = $this->input->post('ideas_story_behind');
		$data['story_posted_date']        = $this->input->post('ideas_story_posted_date') ? $this->input->post('ideas_story_posted_date') : '0000-00-00';
		$data['is_story_active']        = $this->input->post('ideas_is_story_active');
		$data['idea_completed_percentage']        = $this->input->post('ideas_idea_completed_percentage');
		$data['attachment']        = $_POST['attachment']['name'];
		$data['idea_category_id']        = $this->input->post('category');
		$data['video_links']        = $this->input->post('ideas_video_links');
		$data['tag_people']        = $this->input->post('ideas_tag_people');
		$data['tag_words']        = $this->input->post('ideas_tag_words');
		$data['attachment_name']        = $this->input->post('attachment_name');
      //  $data['attachment']        = $this->input->post('attachment');
//print_r($data);
//exit;
		if ( $_FILES AND $_FILES['attachment']['name'] ) 
{
//print_r($this->upload->data());
//exit;
    // Upload the file
//echo $this->upload->do_upload('attachment');
//exit;
		
//if ( ! $this->upload->do_upload('attachment'))
//{
   //return FALSE;
          
	 
	  if ($type != 'update')
	  	 { 
		$error = array('error' => $this->upload->display_errors());		
			
			
		 }
//}

}


		if ($type == 'insert' || $type == 'create_version')

		{

			$id = $this->ideas_model->insert($data);
             $iid = mysql_insert_id();
			  $qmax = $this->db->query("select max(version_no) as rmax from intg_ideas where parent_Rid=0");
 $max = $qmax->row();
 if($type == 'insert')
 {
	  $max_insert = $max->rmax + 1;
 }


			if (is_numeric($id))

			{

				$return = $id;
				// work flow 
				
				$querym = $this->db->query('select bfm.id as bfmid,bfwf.work_flow_approvers as approvers,bfwf.rolename as rname,bfwf.escalation from intg_modules bfm,intg_work_flow bfwf where bfm.id = bfwf.work_flow_module_id and bfwf.work_flow_doc_generator ='.$dataroleuser.' and bfm.modules_module_name="intg_ideas" order by bfwf.id asc');
				
				//$rowm = $querym->row();
			
				$hierarchy = 0;
				$esc = NULL;
				foreach ($querym->result() as $rowm)
				{
				
			$esc = $esc + $rowm->escalation;
				$datam = array(
   'approval_status_module_id' => ''.$rowm->bfmid.'' ,   // module id
   'approval_status_mrowid' => ''.$iid.'' , // new intg_quote inserted row id
   'approval_status_action_by' => ''.$rowm->approvers.'', // approver's role id
   'hierarchy_status' => ''.$hierarchy.'', // initially set to 0 to escape the first approver in hierarchy
  'rolename' => ''.$rowm->rname.'',
   'escalation' => ''.$esc.'',
  'approval_status_action_date' => ''.date('Y-m-d').'',
   'created_on' => ''.date('Y-m-d').''
);
$this->db->insert('intg_approval_status', $datam); 
$hierarchy = $rowm->rname; // assign the first approver
	$insertid = mysql_insert_id();
	
	
	
		$approvers = explode(",",$rowm->approvers);	
	foreach ( $approvers as $val ) 
	{
	$queryrole = $this->db->query('SELECT role_id FROM intg_users WHERE id = "'.$val.'"');
	//echo $this->db->last_query(); 
$rowrole = $queryrole->row();
	$datan = array(
   'approvers_appstatrowid' => ''.$insertid.'' ,
   'approvers_approver' => ''.$val.'' ,
   'approvers_status' => 'No',
    'approvers_role' => $rowrole->role_id,
     'created_on' => ''.date('Y-m-d').'',
	'modified_on' =>''.date('Y-m-d').''
);



$this->db->insert('intg_approvers', $datan); 

	
	}
				}

// end of work flow
				
				

			}

			else

			{

				$return = FALSE;

			}

		}

		elseif ($type == 'update')

		{

			$return = $this->ideas_model->update($id, $data);

		}



		return $return;

	}



	//--------------------------------------------------------------------





}