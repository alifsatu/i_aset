<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/highcharts.js"></script>

<div class="container">
<div class="row-fluid">
<div class="span3">

             
 


<div   style="
  float: left;
  min-width: 260px;
  padding:10px;
  margin: 1px 0 0;
  list-style: none;
  background-color: #ffffff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 5px;
     -moz-border-radius: 5px;
          border-radius: 5px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
     -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
          box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding;
          background-clip: padding-box;">
          <h3 style="padding:5px; margin-bottom:3px;">Quick Links</h3>
<div class="column size1of4 media-box " style="width:100%; margin-bottom:10px">		
			<a href="#"><img src="<?php echo Template::theme_url('images/selectproject.png') ?>" border="0">	

		    </a>
		<p><a href="#"><b>Training Module</b><br>
		  <span>View - Training Module</span></a>
	    </p>
	</div>
    <div style="clear:both"></div>
    <div class="column size1of4 media-box" style="width:100%; margin-bottom:10px">	<img src="<?php echo Template::theme_url('images/selectreports.png'); ?>"/>
	<p><a href="#"><strong>Trainings</strong> <span><br />
	  Attend Traning</span></a><br /></p>
      </div>
       <div style="clear:both"></div>
       <div class="column size1of4 media-box" style="width:100%; margin-bottom:10px"><img src="<?php echo Template::theme_url('images/conhazard.png'); ?>"/>
     <p><b><a href="#">Quiz Modules<br />
       </a></b><a href="#"><span>View-Quiz Modules</span></a></p>
      </div>
         <div style="clear:both"></div>
       <div class="column size1of4 media-box" style="width:100%; margin-bottom:10px; vertical-align:middle"><img src="<?php echo Template::theme_url('images/statreport.png'); ?>"/>
     <p><a href="#"><strong>Quiz</strong><br />
    Attend Quiz</a></p>
      </div>
         <div style="clear:both"></div>
       <div class="column size1of4 media-box" style="width:100%; margin-bottom:10px"><img src="<?php echo Template::theme_url('images/visitform.png'); ?>" />
     <p><a href="#"><strong>Exam Modules</strong><br />
       <span>View- Exam Modules</span></a></p>
      </div>
         <div style="clear:both"></div>
       <div class="column size1of4 media-box" style="width:100%; margin-bottom:10px"><img src="<?php echo Template::theme_url('images/unsafe.png'); ?>"/>
     <p><a href="#"><strong>Exam</strong><br />
       <span>Attend Exam</span></a></p>
     </div>
     </div>
     <p>&nbsp;</p>
    
     <p>&nbsp;</p>
</div>
<div class="span9 columns">
<?php if (isset($current_user->email)) : ?>

	

<div class="container-fluid">
<div class="bs-docs">
            <ul id="myTab" class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab">Top 10 Trainings Conducted</a></li>
              <li class=""><a href="#profile" data-toggle="tab">Top 10 Quiz</a></li>
               <li class=""><a href="#dropdown1" data-toggle="tab">Top 10 Exams</a></li>
             
            </ul>
            <div id="myTabContent" class="tab-content">
              <div class="tab-pane fade active in" id="home">
                
				<p style="float:right">
				
				<script type="text/javascript">
				
			
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'column'
            },
            title: {
                text: 'Top 10 Trainings Conducted'
            },
            subtitle: {
                text: 'Source: SPP'
            },
            xAxis: {
                categories: [
                    'APPLICATION SERVER',
                    'OPERATING SYS',
                    'POLICIES & PROCEDURES',
                    'DATABASE',
                    'SYS ADMIN',
					'LOCAL AREA NETWORK',
					'ELECTRICITY'
                  
                ]
				
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'No Of Trainings Per Week'
                }
            },
            legend: {
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                y: 70,
                floating: true,
                shadow: true
            },
            tooltip: {
                formatter: function() {
                    return ''+
                        this.x +': '+ this.y +' risks';
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
					
                }
            },
                series: [{
                name: 'Risks',			
                data: [89.9, 61.5, 46.4, 129.2,104.0,322,244],
				colorByPoint: true
				
				
    
            }, 
			//{
//                name: 'New York',
//                data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
//    
//            }, {
//                name: 'London',
//                data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
//    
//            }, {
//                name: 'Berlin',
//                data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
//    
//            }
			
			]
        });
    });
    
});
		</script>
        <div  id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div></p>
              </div>
              <div class="tab-pane fade" id="profile">
                 <p><script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container1',
                zoomType: 'xy'
            },
            title: {
                text: 'Top 10 Quiz'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: 'Summary',
                    style: {
                        color: '#89A54E'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Threats',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#4572A7'
                    }
                },
                opposite: true
            }],
            tooltip: {
                formatter: function() {
                    return ''+
                        this.x +': '+ this.y +
                        (this.series.name == 'Rainfall' ? ' mm' : ' Cases');
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: '#FFFFFF'
            },
            series: [{
                name: 'Summary',
                color: '#4572A7',
                type: 'column',
                yAxis: 1,
                data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 194, 95, 54]
    
            }, {
                name: 'Threats',
                color: '#89A54E',
                type: 'spline',
                data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
            }]
        });
    });
    
});
		</script>
        <div  id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div></p>
              </div>
              <div align="right"  class="tab-pane fade" id="dropdown1">
                 <p ><script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
    	
    	// Radialize the colors
		Highcharts.getOptions().colors = $.map(Highcharts.getOptions().colors, function(color) {
		    return {
		        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		        stops: [
		            [0, color],
		            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
		        ]
		    };
		});
		
		// Build the chart
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container2',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Top 10 Exams'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
				






                name: 'Vulnerabilities',
                data: [
                    ['Others',   45.0],
                    ['Services',       26.8],
                    {
                        name: 'Data/Information',
                        y: 12.8,
                        sliced: true,
                        selected: true
                    },
                    ['People',    8.5],
                    ['Software',     6.2],
                    ['Hardware',   0.7]
                ]
            }]
        });
    });
    
});
		</script>
        <div id="container2" style="min-width: 400px; height: 400px; margin: 0 auto"></div></p>
              </div>
             
            </div>
          </div>
	

<?php endif;?>




<?php if (!isset($current_user->email)) : ?>
<div class="hero-unit">
		<h1>Welcome to HSE SYS</h1>

		
	</div>


	<p style="text-align: center">
		<?php echo anchor('/login', '<i class="icon-lock icon-white"></i> '. lang('bf_action_login'), ' class="btn btn-primary btn-large" '); ?>
	</p>

<?php endif;?>

</div>
</div>
</div>
</div>
