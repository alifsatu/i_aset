<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @author : Alif
 * 
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Alif
 * @description To get the sbu name by sbu id
 */
if (!function_exists('get_sbu_name')) {

    function get_sbu_name($sbu_id) {

        $CI = & get_instance();

        return $CI->db->query("select sbu_name from intg_sbu where id = '" . $sbu_id . "'")->row()->sbu_name;
    }

}
/**
 * @author Alif
 * @description To get the cc name by cc id
 */
if (!function_exists('get_cc_name')) {

    function get_cc_name($cc_id) {

        $CI = & get_instance();

        return $CI->db->query("select cost_centre from intg_cost_centre where id = '" . $cc_id . "'")->row()->cost_centre;
    }

}


if (!function_exists('get_costcentre_name')) {

    /**
     * @author Alif
     * @param type $costcentre_id
     * @return type string cost center name
     */
    function get_costcentre_name($costcentre_id) {

        $CI = & get_instance();

        return $CI->db->query("select cost_centre from intg_cost_centre where id ='" . $costcentre_id . "'")->row()->cost_centre;
    }

}

if (!function_exists('get_programme_name')) {

    /**
     * 
     * @param type $program_id
     * @return type
     */
    function get_programme_name($program_id) {

        $CI = & get_instance();

        return $CI->db->query("select program_name from intg_programs where id = '" . $program_id . "'")->row()->program_name;
    }

}


if (!function_exists('get_project_hour_spent')) {

    function get_project_hour_spent($project_id) {

        $CI = & get_instance();

        return $CI->db->query("select sum(tsd_hours) as hours from intg_timesheet_details  where pid = '" . $project_id . "'")->row()->hours;
    }

}

if (!function_exists('get_project_hour_spent_by_user')) {

    function get_project_hour_spent_by_user($user, $project_id, $task_id = null, $mid = null) {

        $CI = & get_instance();

        $sql = "SELECT SUM(tsd_hours) AS hours "
                . "FROM intg_timesheet_details "
                . "JOIN intg_timesheet ON intg_timesheet.`id` = intg_timesheet_details.`tid` "
                . "WHERE pid = '" . $project_id . "' AND uid = '" . $user . "'";

        if ($task_id != null || $task_id != '') {
            $sql .= " AND intg_timesheet_details.ptskid='" . $task_id . "'";
        }

        if ($mid != null || $mid != '') {
            $sql .= " AND intg_timesheet_details.mid='" . $mid . "'";
        }

        //check if timesheet is approved
        $sql .= " AND intg_timesheet.final_status = 'Yes' ";

//        echo $sql . "<br>";
        $hours = $CI->db->query($sql)->row()->hours;
//        echo "<br>" . $hours . "<br>";

        if ($hours == 0)
            $hours = '0';

        return $hours;
    }

}

if (!function_exists('get_hour_spent_by_task')) {

    function get_hour_spent_by_task($user_id, $project_id, $task_id = null) {

        $CI = & get_instance();

        $statement = "SELECT SUM(itd.tsd_hours) as thours from intg_timesheet_details itd";

        if ($task_id == null || $task_id == '') {
            $statement .= ", intg_timesheet it ";
        }
        $statement .= " where ";

        if ($task_id != null || $task_id != '') {
            $statement .= " itd.id='$task_id' AND ";
        } else {
            $statement .= " it.uid = " . $user_id . " AND ";
        }

        $statement .= " itd.pid='$project_id' ";
        echo $statement;
        $workhours = $CI->db->query($statement)->row()->thours;

        return $workhours;
    }

}


if (!function_exists('get_budget_cost_by_user')) {

    function get_budget_cost_by_user($user_id, $milestoneid) {

        $CI = & get_instance();

        $userlevel = $CI->db->query("SELECT user_level_id FROM intg_users where id = " . $user_id)->row()->user_level_id;
        $daily_fte = $CI->db->query("SELECT daily_fte FROM intg_user_level where id = " . $userlevel)->row()->daily_fte;
        $mandays = $CI->db->query("SELECT total_man_days FROM intg_milestones_users where"
                        . " milestones_id = " . $milestoneid . ""
                        . " AND userlevel_id=" . $userlevel)->row()->total_man_days;
//        echo $CI->db->last_query();
//echo "SELECT total_man_days FROM intg_milestones_users where"
//                . " milestones_id = " . $milestoneid.""
//                . " AND userlevel_id=".$userlevel;
//        echo "Mileseonte id ".$milestoneid.", userlevel ".$userlevel.", daily_fte ".$daily_fte.", mandays ".$mandays."<br>";;
//        echo "Man Days  :".$mandays;
//        echo "<br>Mandays Total :".
        $mandaystotal = intval($mandays) * 8.5;
//        echo "<br>Daily FTE  :".$daily_fte;
//        echo "<br>FTE Per hour  :".
        $dailyfteperhour = intval($daily_fte) / 8.5;
//        echo "<br>Total Budget :".
        $totalbudget = $mandaystotal * $dailyfteperhour;
//        echo "<br>Budget Cost:  ";

        return number_format($totalbudget, 2, '.', '');
    }

}

/**
 * @author: Alif
 */
if (!function_exists('get_actual_cost_by_task')) {

    function get_actual_cost_by_task($user_id, $project_id, $task_id = null, $mid = null) {
        $CI = & get_instance();

        $userlevel = $CI->db->query("SELECT user_level_id FROM intg_users where id = " . $user_id)->row()->user_level_id;
        $daily_fte = $CI->db->query("SELECT daily_fte FROM intg_user_level where id = " . $userlevel)->row()->daily_fte;


        if ($task_id != null || $task_id != "") {
            $workhours = get_project_hour_spent_by_user($user_id, $project_id, $task_id, $mid);
        } else {
            $workhours = get_project_hour_spent_by_user($user_id, $project_id);
        }

        $rate = $daily_fte / 8.5;

        $actual_cost = $rate * $workhours;

        return number_format($actual_cost, 2, '.', '');
    }

}

/**
 * @author: Alif
 */
if (!function_exists('get_actual_cost_by_user_hours')) {

    function get_actual_cost_by_user_hours($user_id, $workhours) {
        $CI = & get_instance();

        $userlevel = $CI->db->query("SELECT user_level_id FROM intg_users where id = " . $user_id)->row()->user_level_id;
        $daily_fte = $CI->db->query("SELECT daily_fte FROM intg_user_level where id = " . $userlevel)->row()->daily_fte;

        $rate = $daily_fte / 8.5;

        $actual_cost = $rate * $workhours;

        return number_format($actual_cost, 2, '.', '');
    }

}


/**
 * @author: Alif
 */
if (!function_exists('get_task_name')) {

    function get_task_name() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,task_name FROM intg_task_pool")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->task_name;
        }


        return $data;
    }

}

if (!function_exists('get_taskname_by_project')) {

    function get_taskname_by_project($project_id) {
        $CI = & get_instance();

        $ts = $CI->db->query("select DISTINCT(tp.task_name) from intg_task_pool tp,intg_timesheet_details itd  where itd.ptskid = tp.id and itd.pid = '" . $project_id . "'")->result();
//        $ts = $CI->db->query("select DISTINCT(tp.task_name) from intg_task_pool tp,intg_timesheet_details itd  where itd.ptskid = tp.id and itd.pid = 15 ")->result();
        $taskname = '';
        $lfcr = chr(10); //. chr(13);
        foreach ($ts as $t) {
            $taskname .= $t->task_name . ", " . $lfcr;
        }

        return $taskname;
    }

}


if (!function_exists('get_milestone_name')) {

    /**
     * @author : ALif
     * @param type $milestone_id
     * @return type
     */
    function get_milestone_name() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT milestone_id,milestone_name FROM intg_milestones")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->milestone_id] = $val->milestone_name;
        }


        return $data;
    }

}


if (!function_exists('get_task_by_user')) {

    /**
     * @author : ALif
     * @param type $user_id
     * @return type
     */
    function get_task_by_user($user_id) {

        $CI = & get_instance();
        $sql = "SELECT count(*) as numrows "
                . "FROM intg_timesheet_details "
                . "JOIN intg_timesheet ON intg_timesheet.`id` = intg_timesheet_details.`tid` "
                . "WHERE uid = '" . $user_id . "' AND final_status = 'Yes' AND intg_timesheet.status='Submitted'";


        //echo $sql."<br>";
        return $CI->db->query($sql)->row()->numrows;
    }

}


if (!function_exists('countWorkingDays')) {

    /**
     * 
     * @param type $year - current year
     * @param type $month - current month
     * @param type $ignore - array of igonored days (0 = sunday, 6 = saturday)
     * @return int
     */
    function countWorkingDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }

}


if (!function_exists('get_from_user_table')) {

    /**
     * 
     * @param type $selection - which column to select
     * @param type $where - conditons column
     * @param type $value - conditions value
     * @return type
     */
    function get_from_user_table($selection, $where, $value) {
        $CI = & get_instance();
        $sql = "SELECT " . $selection . " FROM intg_users WHERE " . $where . "=" . $value;

        return $CI->db->query($sql)->row()->$selection;
    }

}


if (!function_exists('get_from_level_table')) {

    /**
     * 
     * @param type $selection - which column to select
     * @param type $where - conditons column
     * @param type $value - conditions value
     * @return type
     */
    function get_from_level_table($selection, $where, $value) {
        $CI = & get_instance();
        $sql = "SELECT " . $selection . " FROM intg_user_level WHERE " . $where . "=" . $value;

        return $CI->db->query($sql)->row()->$selection;
    }

}



if (!function_exists('get_from_any_table')) {

    /**
     * 
     * @param type $selection - which table to select
     * @param type $selection - which column to select
     * @param type $where - conditons column
     * @param type $value - conditions value
     * @return type
     */
    function get_from_any_table($tablename, $selection, $where, $value) {
        $CI = & get_instance();
        $sql = "SELECT " . $selection . " FROM " . $tablename . " WHERE " . $where . "=" . $value;

        return $CI->db->query($sql)->row()->$selection;
    }

}


if (!function_exists('format_date')) {

    /**
     * 
     * @param type $value = date with format eg: 2016-01-01
     * @param type $format = to what format
     * @return type date
     */
    function format_date($value, $format) {

        $date = date_create(str_replace('/', '-', $value));
        return date_format($date, $format);
    }

}



if (!function_exists('get_dailyfte_by_id')) {

    function get_dailyfte_by_id($userleveldetailsid) {
        $CI = & get_instance();

        $daily_fte_rate = $CI->db->query("SELECT daily_fte FROM intg_user_level_details where id = " . $userleveldetailsid)->row()->daily_fte;

        return $daily_fte_rate;
    }

}


if (!function_exists('get_project_hour_spent_fte_by_user')) {

    function get_project_hour_cost_fte_by_user($user, $project_id, $task_id = null, $mid = null) {

        $CI = & get_instance();
        $data = array();
        $sql = "SELECT * "
                . "FROM intg_timesheet_details "
                . "JOIN intg_timesheet ON intg_timesheet.`id` = intg_timesheet_details.`tid` "
                . "JOIN intg_user_level_details ON intg_user_level_details.`id` = intg_timesheet_details.`user_level_details_id`"
                . "WHERE pid = '" . $project_id . "' AND uid = '" . $user . "'";

        if ($task_id != null || $task_id != '') {
            $sql .= " AND intg_timesheet_details.ptskid='" . $task_id . "'";
        }

        if ($mid != null || $mid != '') {
            $sql .= " AND intg_timesheet_details.mid='" . $mid . "'";
        }

        //check if timesheet is approved
        $sql .= " AND intg_timesheet.final_status = 'Yes' ";

//        echo $sql . "<br>";
        $res = $CI->db->query($sql)->result();
//        echo "<br>".$hours."<br>";
        //Pasu's code from timesheet
//        if ($current_user->origin == "HQ" && date($day_format, $day_time_stamp) == "Fri") {
//                                $OTvalid = 8.0;
//                            } else {
//                                $OTvalid = 8.5;
//                            };
//                            if ($current_user->origin == "Estate") {
//                                $OTvalid = 8.0;
//                            };

        $fte_cost_total = '0';
        $tsd_hours_total = '0';
        foreach ($res as $k => $val) {
            $data[$k]['daily_fte'] = $val->daily_fte;
            $data[$k]['tsd_hours'] = $val->tsd_hours;

            $rate = $val->daily_fte / 8.5;

            $actual_cost = $rate * $val->tsd_hours;

            $data[$k]['fte_cost'] = $actual_cost;
            $fte_cost_total += $actual_cost;
            $tsd_hours_total += $val->tsd_hours;
        }
        $data['fte_cost_total'] = $fte_cost_total;
        $data['tsd_hours_total'] = $tsd_hours_total;

//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        die();
        return $data;
    }

}

/**
 * @author Alif NOV 2017
 * @description To get the sbu list
 */
if (!function_exists('get_sbu_list')) {

    function get_sbu_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,sbu_name FROM intg_sbu")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->sbu_name;
        }


        return $data;
    }

}

/**
 * @author Alif NOV 2017
 * @description To get the user level list
 */
if (!function_exists('get_user_level_list')) {

    function get_user_level_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,user_level FROM intg_user_level")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->user_level;
        }


        return $data;
    }

}

/**
 * @author Alif
 * @description To get the cc name by cc id
 */
if (!function_exists('get_cc_list')) {

    function get_cc_list() {

        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,cost_centre FROM intg_cost_centre")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->cost_centre;
        }


        return $data;
    }

}


/**
 * @author Alif
 * @description To get the cc name by cc id
 */
if (!function_exists('get_dailyfte_bydate')) {

    function get_dailyfte_bydate($userlevel, $date) {

        $CI = & get_instance();

        $result = $CI->db->query("SELECT iuld.id,iuld.daily_fte,iuld.start_date FROM intg_user_level iul,intg_user_level_details iuld WHERE iul.id = iuld.`parent_user_level_id` AND iul.id = " . $userlevel);

        $dateRangeList = array();
        $row_count = $result->num_rows();

        $res = $result->result_array();

        for ($i = 0; $i < $row_count; $i++) {
            //if only have 1 row of record
            if (($i == 0) && ($row_count == 1)) {
                $dateRangeList[$i]['start_date'] = $res[$i]['start_date'];
                $dateRangeList[$i]['end_date'] = '2037-01-01';
            }

            //for first row if only have more than 1 row of record
            if (($i == 0) && ($row_count > 1)) {
                $dateRangeList[$i]['start_date'] = $res[$i]['start_date'];
            }


            if (($i > 0) && ($row_count > 1)) {
                $dateRangeList[$i]['start_date'] = $res[$i]['start_date'];
                $nextIndex = $i + 1;
                $prevIndex = $i - 1;
                if (isset($res[$nextIndex]['start_date'])) {
                    $dateRangeList[$i]['end_date'] = date('Y-m-d', strtotime('-1 day', strtotime($res[$nextIndex]['start_date'])));
                }
                $dateRangeList[$prevIndex]['end_date'] = date('Y-m-d', strtotime('-1 day', strtotime($res[$i]['start_date'])));
            }

            if (($i == $row_count - 1) && ($row_count > 1)) {
                $dateRangeList[$i]['start_date'] = $res[$i]['start_date'];
                $dateRangeList[$i]['end_date'] = '2037-01-01';
            }

            $dateRangeList[$i]['user_level_details_id'] = $res[$i]['id'];
            $dateRangeList[$i]['daily_fte'] = $res[$i]['daily_fte'];
        }


        $user_level_data = array();
        $user_level_details_id = '';
        $daily_fte = '';
        foreach ($dateRangeList as $val) {
            // Convert to timestamp
            $start_ts = strtotime($val['start_date']);
            $end_ts = strtotime($val['end_date']);

            $user_ts = strtotime($date);

            // Check that user date is between start & end
            $isInrange = (($user_ts >= $start_ts) && ($user_ts <= $end_ts));

            if ($isInrange) {
//                echo "in range" . $user_ts . '>=' . $start_ts . '&& ' . $user_ts . '<=' . $end_ts;
//                echo "<br>in range" . $date . '>=' . $val['start_date'] . '&& ' . $date . '<=' . $val['end_date'];
                $user_level_details_id = $val['user_level_details_id'];
                $daily_fte = $val['daily_fte'];
            }
        }

        if ($user_level_details_id != '') {
            $user_level_data['user_level_details_id'] = $user_level_details_id;
            $user_level_data['daily_fte'] = $daily_fte;
        } else {
            $user_level_data = false;
        }


//        echo '<pre>';
//        print_r($dateRangeList);
//        print_r($user_level_data);
//        echo '</pre>';
        return $user_level_data;
    }

}

/**
 * @author Alif
 * @description 
 */
if (!function_exists('milestone_approval_badges')) {

    function milestone_approval_badges($milestone_data, $milestone_approval_created_by, $project_id) {
        $CI = & get_instance();
        $rc_approval = $CI->db->query("Select * from intg_milestone_approval where final_status = 'No' AND created_by = " . $milestone_approval_created_by . "  AND project_id = '" . $project_id . "' ");
        $status = '';
        switch ($milestone_data->status) {
            case "No" :
                if ($rc_approval->num_rows() == 0)
                    $status = "<span class='badge btn-warning m-l text-xs'>Pending Approval</span>";
                else
                    $status = "<span class='badge btn-success m-l text-xs'>Sent For Approval</span>";
                break;
            case "Yes" :
                $status = "<span class='badge btn-primary m-l text-xs'>Approved</span>";
                break;
            case "Reject" :
                $status = "<span class='badge btn-danger m-l text-xs'>Rejected</span>";
                break;
        }
        echo $status;
        $action = '';
        if ($milestone_data->action != '') {
            switch ($milestone_data->action) {
                case "deactivate" :
                    $action = "<span class='badge btn-warning m-l text-xs'>Sent For Deactivation Approval</span>";
                    break;
                case "activate" :
                    $action = "<span class='badge btn-warning m-l text-xs'>Sent For Activation Approval</span>";
                    break;
                case "deactivated" :
                    $action = "<span class='badge btn-danger m-l text-xs'>Deactivated</span>";
                    break;
                default :
                    break;
            }
        }
        echo $action;
    }

}


/**
 * @author Alif NOV 2017
 * @description To get the user list
 */
if (!function_exists('get_user_name_list')) {

    function get_user_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,display_name FROM intg_users where banned='0' ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->display_name;
        }


        return $data;
    }

}


if (!function_exists('redirect_back')) {

    function redirect_back() {
        if (isset($_SERVER['HTTP_REFERER'])) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        } else {
            header('Location: http://' . $_SERVER['SERVER_NAME']);
        }
        exit;
    }

}

if (!function_exists('print_r_pre')) {

    /**
     * 
     * @param type $array to print out
     */
    function print_r_pre($array) {
        echo '<pre>';
        echo print_r($array);
        echo '</pre>';
    }

}
if (!function_exists('project_info')) {

    /**
     * 
     * @param type $pj
     */
    function project_info($pj) {
        $CI = & get_instance();
        $user_name_list = get_user_name_list();
        ?>
        <div id="project" class="panel-collapse in" style="height: auto;">
            <div class="panel-body">
                <div class="form-group  clearfix">
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Project Code</strong></header>
                        <div class="controls pad">
                            <?= $pj->prefix_io_number ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>SBU</strong></header>
                        <div class="controls pad">
                            <?= get_sbu_name($pj->sbu_id); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Cost Centre</strong></header>
                        <div class="controls pad">
                            <?= get_cc_name($pj->cost_centre_id); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Duration in Days</strong></header>
                        <div class="controls pad">
                            <?= $pj->total_days ?>
                        </div>
                    </div>
                </div>
                <div class="form-group  clearfix">
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Programme</strong></header>
                        <div class="controls pad">
                            <?= get_programme_name($pj->program_id); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Justification of Project</strong></header>
                        <div class="controls pad" style="text-align:justify">
                            <?= $pj->justification ?>
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                        <header class="panel-heading bg-light  no-border"><strong>Co-Worker</strong></header>
                        <div class="controls pad">
                            <?php
                            $user_level_list = get_user_level_list();
                            $us = $CI->db->query("select display_name,user_level_id from intg_users where id IN ( " . $pj->coworker . ")")->result();
                            foreach ($us as $up) {

                                $cw = $up->display_name . ' (';
                                $cw .= $up->user_level_id != '' ? $user_level_list[$up->user_level_id] : 'N/A';
                                $cw .= '), ';
                                $upa[] = $cw;
                            }
                            echo implode("&nbsp;", $upa);
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Attachment</strong></header>
                        <div class="controls pad">
                            <?php
                            if ($pj->attachment != "") {
                                $att = explode(":", $pj->attachment);
                                ?>
                                <a href="<?= base_url() ?>uploads/<?= $att[0] ?>" download><?= $att[0] ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-group  clearfix">
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light  no-border"><strong>Created By</strong></header>

                        <div class="controls pad">

                            <?php
                            if ($CI->auth->has_permission('Change.Project.Ownership') && $pj->created_by != '1') {
                                unset($user_name_list[1]);
                                echo form_dropdown('change_created_by', $user_name_list, $pj->created_by, 'Change Project Owner', 'class="selecta form-control input-sm input-s"');
                            } else {
                                echo $user_name_list[$pj->created_by];
                                echo '<br/>';
                            }

                            if ($pj->created_by_history != '') {
                                ?>

                                <a href="#"   data-toggle="modal" data-target="#myModal2"><i class="fa fa-eye"></i>&nbsp;&nbsp;View</a>
                                <div id="myModal2" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body" align="center" width="400" height="300">
                                                <!------app list-->


                                                <h4>Previous Owner:</h4>
                                                <table width='100%' class='table table-bordered' id="approver_table">
                                                    <tr>
                                                        <td><strong>No</strong></td>
                                                        <td><strong>Name</strong></td>
                                                        <td><strong>Changed On</strong></td>
                                                        <td><strong>Changed By</strong></td>
                                                    </tr>
                                                    <?php
                                                    $nom = 1;
                                                    foreach (json_decode('[' . $pj->created_by_history . ']') as $rec) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $nom ?></td>
                                                            <td><?= $user_name_list[$rec->user_id] ?></td>
                                                            <td><?= $rec->changed_on ?></td>
                                                            <td><?= $user_name_list[$rec->changed_by] ?></td>
                                                        </tr>
                                                        <?php
                                                        $nom++;
                                                    }
                                                    ?>
                                                </table>

                                                <!------app list-->


                                            </div>

                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Start Date</strong> (dd/mm/yyyy)</header>
                        <div class="controls pad">
                            <?= date("d/m/Y", strtotime($pj->project_start_date)) ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light    no-border"><strong>End Date</strong> (dd/mm/yyyy)</header>
                        <div class="controls pad">
                            <?= date("d/m/Y", strtotime($pj->project_end_date)) ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <header class="panel-heading bg-light   no-border"><strong>Project Color</strong></header>
                        <div class="controls pad">  <div style="background-color: <?= $pj->projects_color ?>;  width:80px; text-align:center">&nbsp;</div> </div>
                    </div>
                </div>
                <div class="form-group  clearfix">
                    <div class="col-sm-10">
                        <header class="panel-heading bg-light   no-border"><strong>Introduction</strong></header>
                        <div class="controls pad">
                            <?= $pj->introduction ?>
                        </div>
                    </div>
                </div>

                <div class="form-group  clearfix">
                    <div class="col-sm-10">
                        <header class="panel-heading bg-light   no-border"><strong>Cost Summary (RM) </strong></header>
                        <div class="controls pad">
                            <?php
                            if ($pj->status != "Initiated") {
                                $ms = $CI->db->query("select * from intg_milestones m where m.parent_Rid= 0 and  m.project_id = '" . $CI->uri->segment(5) . "' and status != 'Reject'  order by start_date   ")->result();
                            } else {
                                $ms = $CI->db->query("select * from intg_milestones m where m.parent_Rid= 0 and  m.project_id = '" . $CI->uri->segment(5) . "' and status != 'Reject'  order by start_date  ")->result();
                            }
                            ?>
                            <div class="table table-responsive">
                                <table class="table table-striped m-b-none text-sm">
                                    <tr>
                                        <td width="120">&nbsp;</td>
                                        <?php
                                        $no = 1;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><b>
                                                    Milestone <?= $no ?> :<br><?= $cs->milestone_name ?>
                                                </b></td>

                                            <?php
                                            $no++;
                                        }
                                        ?>
                                        <td><strong>Total (RM)</strong></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Material Cost</strong></td>
                                        <?php
                                        $mstotal = 0;
                                        foreach ($ms as $cs) {
                                            $total{$cs->milestone_id} = $cs->material_cost + $cs->new_capex + $cs->outsourcing + $cs->others;
                                            ?>
                                            <td><?= number_format($cs->material_cost, 2) ?></td>

                                            <?php
                                            $mstotal = $mstotal + $cs->material_cost;
                                        }
                                        ?>
                                        <td><?php echo number_format($mstotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>New CAPEX</strong></td>
                                        <?php
                                        $nptotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?= number_format($cs->new_capex, 2) ?></td>

                                            <?php
                                            $nptotal = $nptotal + $cs->new_capex;
                                        }
                                        ?>
                                        <td><?php echo number_format($nptotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Outsourcing</strong></td>
                                        <?php
                                        $ostotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?= number_format($cs->outsourcing, 2) ?></td>

                                            <?php
                                            $ostotal = $ostotal + $cs->outsourcing;
                                        }
                                        ?>
                                        <td><?php echo number_format($ostotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td width="120"><strong>Others</strong></td>
                                        <?php
                                        $ottotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?php echo number_format($cs->others, 2); ?></td>

                                            <?php
                                            $ottotal = $ottotal + $cs->others;
                                        }
                                        ?>
                                        <td><?php echo number_format($ottotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td  width="120"><strong>Time Cost</strong></td>
                                        <?php
                                        $timetotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><?php
                                                $ts{$cs->milestone_id} = $CI->db->query("select sum(time_cost) as timecost from intg_milestones_users where milestones_id = '" . $cs->milestone_id . "'")->row()->timecost;
                                                echo number_format($ts{$cs->milestone_id}, 2);
                                                ?></td>

                                            <?php
                                            $timetotal = $timetotal + $ts{$cs->milestone_id};
                                        }
                                        ?>
                                        <td><?php echo number_format($timetotal, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td  width="120"><strong>Total (RM)</strong></td>
                                        <?php
                                        $gtotal = 0;
                                        foreach ($ms as $cs) {
                                            ?>
                                            <td><strong><?php
                                                    echo number_format($total{$cs->milestone_id} + $ts{$cs->milestone_id}, 2);
                                                    ?>
                                                </strong></td>

                                            <?php
                                            $gtotal = $gtotal + $total{$cs->milestone_id} + $ts{$cs->milestone_id};
                                        }
                                        ?>
                                        <td><strong><?php echo number_format($gtotal, 2); ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--submit for milestone approval--->

                    <?php echo form_close(); ?>
                    <!--submit for milestone approval--->
                </div>
            </div>
        </div> <?php
    }

}



if (!function_exists('get_project_name_list')) {

    function get_project_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama_projek FROM intg_projek ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama_projek;
        }


        return $data;
    }

}

if (!function_exists('get_perunding_name_list')) {

    function get_perunding_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_daftar_jurutera_perunding ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('get_pemilik_name_list')) {

    function get_pemilik_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_daftar_pemilik ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('get_daerah_name_list')) {

    function get_daerah_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_daftar_daerah ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('check_payment_amount')) {

    function check_payment_amount($jns_byrn, $projek_id, $should_pays = null) {
        if ($projek_id != "") {
            $CI = & get_instance();
            $result = $CI->db->query("SELECT sum(jumlah_bayaran) as jumlah FROM intg_bayaran where jenis_bayaran = " . $jns_byrn . " AND id_projek = " . $projek_id . " AND deleted = 0 ")->row();
//        echo $CI->db->last_query();
            $paids = $result->jumlah;
            $paid = round((float) $paids, 2);


            $class = 'danger';
            if ($should_pays) {
                $should_pay = round((float) $should_pays, 2);
                if ($paids != '' && $paid < $should_pay) {
                    $class = 'warning';
                }

                if ($paids != '' && $paid == $should_pay) {
                    $class = 'primary';
                }

                if ($paids != '' && $paid > $should_pay) {
                    $class = 'info';
                }
            }

            $data = array(
                'class' => $class,
                'paid_amount' => $paid
            );
        } else {
            $data = array(
                'class' => "",
                'paid_amount' => 0);
        }
        return $data;
    }

}
if (!function_exists('status_rekod_aktif_takaktif')) {

    function status_rekod_aktif_takaktif($id) {
        $status = array(
            '1' => '<i class="fa fa-check" style="color: green">Aktif</i>',
            '2' => '<i class="fa fa-times" style="color: red">Tak Aktif</i>'
        );

        return $status[$id];
    }

}
if (!function_exists('search_array_multi')) {

    function search_array_multi($array, $key, $value) {

        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, search_array_multi($subarray, $key, $value));
            }
        }

        return $results;
    }

}



/**
 * Case in-sensitive array_search() with partial matches
 *
 * @param string $needle   The string to search for.
 * @param array  $haystack The array to search in.
 *
 * @author Bran van der Meer <branmovic@gmail.com>
 * @since 29-01-2010
 */
if (!function_exists('array_find')) {

    function array_find($needle, array $haystack) {
        foreach ($haystack as $key => $value) {
            if (false !== stripos($value, $needle)) {
                return $key;
            }
        }
        return false;
    }

}

if (!function_exists('list_bulan_melayu_panjang')) {

    function list_bulan_melayu_panjang() {
        $bulan = array(
            '1' => 'Januari',
            '2' => 'Februari',
            '3' => 'Mac',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Jun',
            '7' => 'Julai',
            '8' => 'Ogos',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Disember'
        );

        return $bulan;
    }

}

if (!function_exists('list_bulan_melayu_pandek')) {

    function list_bulan_melayu_pandek() {
        $bulan = array(
            '1' => 'Jan',
            '2' => 'Feb',
            '3' => 'Mac',
            '4' => 'Apr',
            '5' => 'Mei',
            '6' => 'Jun',
            '7' => 'Jul',
            '8' => 'Ogos',
            '9' => 'Sep',
            '10' => 'Okt',
            '11' => 'Nov',
            '12' => 'Dis'
        );

        return $bulan;
    }

}


if (!function_exists('get_status_permohonan_list')) {

    function get_status_permohonan_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_status_permohonan ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}
if (!function_exists('get_intg_status_terkini_projek_list')) {

    function get_intg_status_terkini_projek_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_status_terkini_projek ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('get_status_permohonan_ccc_sokongan_list')) {

    function get_status_permohonan_ccc_sokongan_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_daftar_status_permohonan_sokongan_ccc ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('get_kaedah_jaminan_ccc_list')) {

    function get_kaedah_jaminan_ccc_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,nama FROM intg_daftar_kaedah_jaminan_ccc ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->nama;
        }


        return $data;
    }

}

if (!function_exists('get_location_name_list')) {

    function get_location_name_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,name FROM intg_daftar_lokasi where deleted = 0 ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->name;
        }


        return $data;
    }

}


if (!function_exists('get_asset_classification_group_list')) {

    function get_asset_classification_group_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,name FROM intg_daftar_classification_group where deleted = 0 ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->name;
        }


        return $data;
    }

}

if (!function_exists('get_asset_classification_list')) {

    function get_asset_classification_list($class_group_id = NULL) {
        $CI = & get_instance();
        $whereclause = '';
        if ($class_group_id != NULL) {
            $whereclause = ' AND classification_group_id = "' . $class_group_id . '" ';
        }

        $result = $CI->db->query("SELECT id,name,classification_group_id FROM intg_daftar_classification where deleted = 0 $whereclause ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = array('name'=>$val->name,'cgi'=>$val->classification_group_id);
        }


        return $data;
    }

}


if (!function_exists('get_aset_unit_id_list')) {

    function get_aset_unit_id_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,name FROM intg_daftar_unit where deleted = 0 ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->name;
        }


        return $data;
    }

}

if (!function_exists('get_asset_list')) {

    function get_asset_list() {
        $CI = & get_instance();

        $result = $CI->db->query("SELECT id,asset_name FROM intg_urus_aset where deleted = 0 ")->result();
        $data = array();
        foreach ($result as $val) {
            $data[$val->id] = $val->asset_name;
        }


        return $data;
    }

}
?>
