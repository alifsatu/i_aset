<style type="text/css">
    .modal-dialog {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }

    .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
    }
</style>


<div class="row">
    <section class="panel panel-default">
        <div class="panel-body">
            <div class="admin-box">
                <h3><?php echo lang('activity_filter_head'); ?></h3>
                <?php
                echo form_open(SITE_AREA . '/reports/activities/' . $vars['which'], 'class=""');

                $form_help = '<span class="help-inline">' . sprintf(lang('activity_filter_note'), ($vars['view_which'] == ucwords(lang('activity_date')) ? 'from before' : 'only for'), strtolower($vars['view_which'])) . '</span>';
                $form_data = array('name' => $vars['which'] . '_select', 'id' => $vars['which'] . '_select', 'class' => 'selecta', 'style' => 'border: #D8D8D8 solid 1px; padding:5px');
                echo form_dropdown($form_data, $select_options, $filter);
                //echo form_dropdown("activity_select", $select_options, $filter,array('id' => 'activity_select', 'class' => 'span4' ) );
                unset($form_data, $form_help);
                ?>
                <div class="form-actions">
                    <?php echo form_submit('filter', lang('activity_filter'), 'class="btn btn-primary"'); ?>
                    <?php if ($vars['which'] == 'activity_own' && has_permission('Activities.Own.Delete')): ?>
                        <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_own"><i class="icon-trash icon-white">&nbsp;</i>&nbsp;<?php echo lang('activity_own_delete'); ?></button>
                    <?php elseif ($vars['which'] == 'activity_user' && has_permission('Activities.User.Delete')): ?>
                        <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_user"><i class="icon-trash icon-white">&nbsp;</i>&nbsp;<?php echo lang('activity_user_delete'); ?></button>
                    <?php elseif ($vars['which'] == 'activity_module' && has_permission('Activities.Module.Delete')): ?>
                        <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_module"><i class="icon-trash icon-white">&nbsp;</i>&nbsp;<?php echo lang('activity_module_delete'); ?></button>
                    <?php elseif ($vars['which'] == 'activity_date' && has_permission('Activities.Date.Delete')): ?>
                        <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_date"><i class="icon-trash icon-white">&nbsp;</i>&nbsp;<?php echo lang('activity_date_delete'); ?></button>
                    <?php endif; ?>
                </div>
                <?php echo form_close(); ?>
                <br/><br/>
                <h3><?php echo sprintf(lang('activity_view'), ($vars['view_which'] == lang('activity_date') ? $vars['view_which'] . ' before' : $vars['view_which']), $vars['name']); ?></h3>
            </div>




            <?php if (!isset($activity_content) || empty($activity_content)) : ?>
                <div class="alert alert-error fade in">
                    <a class="close" data-dismiss="alert">&times;</a>
                    <h4 class="alert-heading"><?php echo lang('activity_not_found'); ?></h4>
                </div>
            <?php else : ?>

                <div id="user_activities">
                    <table class="table table-striped table-bordered" id="flex_table">
                        <thead>
                            <tr>
                                <th><?php echo lang('activity_user'); ?></th>
                                <th><?php echo lang('activity_activity'); ?></th>
                                <th><?php echo lang('activity_module'); ?></th>
                                <th><?php echo lang('activity_when'); ?></th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tfoot></tfoot>

                        <tbody>
                            <?php foreach ($activity_content as $activity) : ?>
                                <tr>
                                    <td><i class="icon-user">&nbsp;</i>&nbsp;<?php e($activity->username); ?></td>
                                    <td><?php echo $activity->activity; ?></td>
                                    <td><?php echo $activity->module; ?></td>
                                    <td><?php echo date('M j, Y g:i A', strtotime($activity->created)); ?></td>
                                    <td>
                                        <?php
                                        if ($activity->formdata == '') {
                                            
                                        } elseif ($activity->formdata == '""') {
                                            
                                        } else {
                                            ?>
                                            <button class="btn btn-primary btn-sm" onclick="showActivitiesDetail(<?php echo htmlspecialchars(json_encode($activity->formdata)) ?>)">
                                                Show Form Data
                                            </button>
                                        <?php } ?>
                                    </td>
                                </tr>











                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>


            <?php endif; ?>


        </div>
</div></section></div>



<footer class="panel-footer">
    <div class="row">
        <div class="col-sm-4 hidden-xs">


        </div>
        <div class="col-sm-4 text-center">
            <small class="text-muted inline m-t-sm m-b-sm">Showing <?= $offset + 1 ?> - <? echo $rowcount+$offset?> of <?  echo $total; ?></small>
        </div>
        <div class="col-sm-4 text-right text-center-xs">                

            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</footer>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Form Data Details</h4>
            </div>
            <div class="modal-body">





            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

//    $('#myModal').on('show.bs.modal', function (event) {
//        var button = $(event.relatedTarget); // Button that triggered the modal
//        var formdata = button.data('whatever'); // Extract info from data-* attributes
//        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//        console.log(formdata);
//        
//         var table='<table>';
//            /* loop over each object in the array to create rows*/
//            $.each( formdata, function( index, item){
//                  /* add to html string started above*/
//               table+='<tr><td>'+index+'</td><td>'+index+'</td></tr>';       
//            });
//            table+='</table>';
//      /* insert the html string*/
//        
//        var modal = $(this);
//        modal.find('.modal-body').html(table);
//    });

    function showActivitiesDetail(formdata) {
        console.log(formdata);
        var obj = JSON.parse(formdata);
        console.log(obj);
        console.log(isEmpty(obj));

        if (isEmpty(obj) == true) {
            $('#myModal').find('.modal-body').html('<h3>No Data Available</h3>');
            console.log('empty');
        } else {
            console.log('not empty');
            var table = '<div class="table table-responsive"><table class="table table-bordered table-striped table-condensed">';
            /* loop over each object in the array to create rows*/
            $.each(obj, function (index, item) {
                /* add to html string started above*/
                table += '<tr><td><b>' + index + '</b></td><td>' + item + '</td></tr>';
            });
            table += '</table></div>';
            /* insert the html string*/

//        var modal = $(this);
            $('#myModal').find('.modal-body').html(table);
        }
        $('#myModal').modal('show');
    }


    function isEmpty(myObject) {
        for (var key in myObject) {
            if (myObject.hasOwnProperty(key)) {
                return false;
            }
        }

        return true;
    }

</script>