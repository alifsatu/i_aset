<script type="text/javascript">
	window.g_permission 	= '<?php e(lang('matrix_permission')); ?>';
	window.g_role			= '<?php e(lang('matrix_role')); ?>';
	window.g_url			= '<?php echo site_url(SITE_AREA .'/settings/roles/matrix_update') ?>';
        
        //alif
//        $(document).ready(function ()
//                {
//                    $("#permission_table").tablesorter();
//                }
//                );

$(function() {

  var $table = $('table').tablesorter({
    theme: 'blue',
    widgets: ["filter"],
    widgetOptions : {
      // filter_anyMatch replaced! Instead use the filter_external option
      // Set to use a jQuery selector (or jQuery object) pointing to the
      // external filter (column specific or any match)
      filter_external : '.search',
      // add a default type search to the first name column
      filter_defaultFilter: { 1 : '~{query}' },
      // include column filters
      filter_columnFilters: true,
      filter_placeholder: { search : 'Search...' },
      filter_saveFilters : true,
      filter_reset: '.reset'
    }
  });

  // make demo search buttons work
  $('button[data-column]').on('click', function(){
    var $this = $(this),
      totalColumns = $table[0].config.columns,
      col = $this.data('column'), // zero-based index or "all"
      filter = [];

    // text to add to filter
    filter[ col === 'all' ? totalColumns : col ] = $this.text();
    $table.trigger('search', [ filter ]);
    return false;
  });

});

</script>

<div id="permission_table_result" class="alert alert-info fade in">
	<a class="close" data-dismiss="alert">&times;</a>
	<?php echo lang('matrix_note');?>
</div>

<div class="admin-box">
    
    
    Search : <input class="search" type="search" data-column="all"> 

<!-- targeted by the "filter_reset" option -->
<button type="button" class="reset">Reset Search</button>&nbsp;(Company ID : <?php echo $this->session->userdata('company_id'); ?> )

	<table class="table table-striped" id="permission_table">
		<thead>
			<tr>
				<th><?php echo lang('matrix_permission');?></th>
				<?php foreach($matrix_roles as $matrix_role ) : ?>
					<?php $matrix_role = (array)$matrix_role; ?>
					<?php if (has_permission('Permissions.'.$matrix_role['role_name'].'.Manage')) : ?>
						<th  class="text-center"><?php echo $matrix_role['role_name']; ?></th>
					<?php endif; ?>
					<?php $cols[] = array('role_id' => $matrix_role['role_id'], 'role_name' => $matrix_role['role_name']); ?>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
		<?php foreach($matrix_permissions as $matrix_perm ) : ?>
			<?php $matrix_perm = (array)$matrix_perm; ?>

			<?php if (has_permission($matrix_perm['name']) || $current_user->role_id == 1): //Admin?>
			<tr title="<?php echo $matrix_perm['name']; ?>">
				<td><?php echo $matrix_perm['name']; ?></td>
				<?php
				for($i=0;$i<count($cols);$i++) :
					if (has_permission('Permissions.'.$cols[$i]['role_name'].'.Manage')) :
						$checkbox_value = $cols[$i]['role_id'].','.$matrix_perm['permission_id'];
						$checked = in_array($checkbox_value, $matrix_role_permissions) ? ' checked="checked"' : '';
				 ?>
					<td class="text-center" title="<?php echo $cols[$i]['role_name']; ?>">
						<input type="checkbox" value="<?php echo $checkbox_value; ?>"<?php echo $checked; ?> title="<?php echo lang('matrix_role');?>: <?php echo $cols[$i]['role_name']; ?>, <?php echo lang('matrix_permission');?>: <?php echo $matrix_perm['name']; ?>" />
					</td>
					<?php endif; ?>
				<?php endfor; ?>
			</tr>
			<?php endif; ?>

		<?php endforeach; ?>
		</tbody>
	</table>

</div>
