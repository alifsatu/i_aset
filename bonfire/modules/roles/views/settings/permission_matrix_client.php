
<script type="text/javascript">
    window.g_permission = '<?php e(lang('matrix_permission')); ?>';
    window.g_role = '<?php e(lang('matrix_role')); ?>';
    window.g_url = '<?php echo site_url(SITE_AREA . '/settings/roles/matrix_update') ?>';

    //alif
//        $(document).ready(function ()
//                {
//                    $("#permission_table").tablesorter();
//                }
//                );

    $(function () {

        var $table = $('table').tablesorter({
            theme: 'blue',
            widgets: ["filter"],
            widgetOptions: {
                // filter_anyMatch replaced! Instead use the filter_external option
                // Set to use a jQuery selector (or jQuery object) pointing to the
                // external filter (column specific or any match)
                filter_external: '.search',
                // add a default type search to the first name column
                filter_defaultFilter: {1: '~{query}'},
                // include column filters
                filter_columnFilters: true,
                filter_placeholder: {search: 'Search...'},
                filter_saveFilters: true,
                filter_reset: '.reset'
            }
        });

        // make demo search buttons work
        $('button[data-column]').on('click', function () {
            var $this = $(this),
                    totalColumns = $table[0].config.columns,
                    col = $this.data('column'), // zero-based index or "all"
                    filter = [];

            // text to add to filter
            filter[ col === 'all' ? totalColumns : col ] = $this.text();
            $table.trigger('search', [filter]);
            return false;
        });

    });

</script>

<div id="permission_table_result" class="alert alert-info fade in">
    <a class="close" data-dismiss="alert">&times;</a>
    <?php echo lang('matrix_note'); ?>
</div>

<div class="admin-box">


    Search : <input class="search" type="search" data-column="all"> 

    <!-- targeted by the "filter_reset" option -->
    <button type="button" class="reset">Reset Search</button>&nbsp;(Company ID : <?php echo $this->session->userdata('company_id'); ?> )

    <table class="table table-striped" id="permission_table">
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo lang('matrix_permission'); ?></th>
                <?php
                foreach ($matrix_companies as $matrix_company) {

                    $matrix_company = (array) $matrix_company;
                    ?>
                    <th  class="text-center"><?php echo $matrix_company['name']; ?></th>
                <?php } ?>
            </tr>
        </thead>

        <tbody>
            <?php
            $count = 1;
            foreach ($matrix_permissions as $matrix_perm) {

                $matrix_perm = (array) $matrix_perm;


                if (has_permission($matrix_perm['name']) || $current_user->role_id == 1) { //Admin
                    ?>
                    <tr title="<?php echo $matrix_perm['name']; ?>">
                        <td> <?= $count ?></td>
                        <td><?php echo $matrix_perm['name']; ?></td>
                        <?php
                        foreach ($matrix_companies as $td) {
                                $td = (array) $td;
                            ?>
                            <td class="text-center">
                                <?php
                                if (array_key_exists($matrix_perm['permission_id'], $matrix_role_permissions_client[$td['id']])) {
                                    echo '<span class="label bg-primary">'.$matrix_perm['name'].'</span>';
                                }else{
                                    echo '<span class="label bg-danger">'.$matrix_perm['name'].'</span>';
                                }
                                ?>
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                    $count++;
                }
                ?>

            <?php } ?>
        </tbody>
    </table>

</div>
