<?php

class Cron extends MX_Controller {

	public function index($id) {
		$this->load->config();
		$this->load->library('MY_PHPMailer');
		$mail = new PHPMailer();
		//$mail->isSendmail();
		$mail->IsSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug  = 2;
		$mail->SMTPAuth   = false;
		$mail->Debugoutput = 'html';
		foreach($this->config->item('mail_config') as $prop => $val) {
			$mail->{$prop} = $val;
		}
		$mail->setFrom('timesheet@genting.com', 'timesheet@genting.com');
		$this->load->model('Emailer_model', 'emailer_model', TRUE);
		
		$e = $this->emailer_model->find($id);
		//echo "Email : ".$e->to_email.", Name: ". $e->to_name.", TO :".$mail->getTo();
		$mail->addAddress($e->to_email, $e->to_name);
		$mail->Subject = $e->subject;
		$mail->Body = $e->message;
		$mail->AltBody = 'This is a plain-text message body';
		$mail->IsHTML(true);
		$sent = false;
		$attempts = $e->attempts;
		while(!$sent && $attempts < $e->max_attempts) {
			$attempts++;
			$this->emailer_model->update($e->id, array('attempts' => $attempts, 'last_attempt' => date('Y-m-d H:i:s')));
			if($sent = $mail->send()) {
				$this->emailer_model->update($e->id, array('success' => true, 'date_sent' => date('Y-m-d H:i:s')));
				echo "Email Sent";
				
			}else{
				 echo "Email Not Sent. Mailer Error: " . $mail->ErrorInfo;
			}
		}
		$mail->ClearAddresses();
	}
	
	public function all() {
		$this->load->config();
		$this->load->library('MY_PHPMailer');
		$mail = new PHPMailer();
		//$mail->isSendmail();
		$mail->IsSMTP();
		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug  = 2;
		$mail->SMTPAuth   = false;
		foreach($this->config->item('mail_config') as $prop => $val) {
			$mail->{$prop} = $val;
		}
		$mail->SetFrom($mail->Username, 'SPP');
		$this->load->model('Emailer_model', 'emailer_model', TRUE);
		$emailer = $this->emailer_model->find_all_by(array('success' => false));
		foreach($emailer as $e) {
			$mail->AddAddress($e->to_email, $e->to_name);
			$mail->Subject = $e->subject;
			$mail->Body = $e->message;
			$mail->AltBody = 'This is a plain-text message body';
			$mail->IsHTML(true);
			$this->emailer_model->update($e->id, array('attempts' => $e->attempts + 1));
			$this->emailer_model->update($e->id, array('last_attempt' => date('Y-m-d H:i:s')));
			if($sent = $mail->send()) {
				$this->emailer_model->update($e->id, array('success' => true, 'date_sent' => date('Y-m-d H:i:s')));
				echo "Email Sent";
				
			}else{
				 echo "Email Not Sent. Mailer Error: " . $mail->ErrorInfo;
			}
			$mail->ClearAddresses();
		}
	}
	
	public function debug() {
                $this->load->config();
		$this->load->library('emailer/emailer');
		$this->emailer->addAddress('budakpoly@gmail.com', 'Alif');
		$this->emailer->Subject = 'Email Debug - '.  rand(1, 999);
                $data= " ";
                foreach($this->config->item('mail_config') as $prop => $val) {
                    if($prop == 'Password')
                        $data .= $prop .'='."********".'<br>';
                    else
			$data .= $prop .'='. $val.'<br>';
		}
                
		$this->emailer->Body = 'Email Sent from '.  base_url().' EMAIL DATA:<br>'.$data;
		$this->emailer->mail();
	}
        
        public function processQueue(){
            $this->load->library('emailer/emailer');
            $this->emailer->process_queue();
            
        }
}