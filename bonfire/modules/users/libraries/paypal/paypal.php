<?php

require_once( 'paypal-digital-goods.class.php' );
require_once( 'paypal-subscription.class.php' );
require_once( 'paypal-purchase.class.php' );

class Paypal
{
        private $environment = 'live';
        private $business_name = 'xxx';
        private $username = 'xxx';
        private $password = 'xxx';
        private $signature = 'xxx';
		public $email = 'xxx';
        
        function __construct()
        {
                $this->CI = get_instance();
        }
        
        function set_credentials()
        {       
                PayPal_Digital_Goods_Configuration::username( $this->username );
                PayPal_Digital_Goods_Configuration::password( $this->password );
                PayPal_Digital_Goods_Configuration::signature( $this->signature );
                PayPal_Digital_Goods_Configuration::business_name( $this->business_name );

				PayPal_Digital_Goods_Configuration::currency('MYR'); 
				PayPal_Digital_Goods_Configuration::incontext_url('no'); 
                PayPal_Digital_Goods_Configuration::return_url(base_url('index.php/users/paypal_return') );
                PayPal_Digital_Goods_Configuration::cancel_url( base_url('index.php/users/paypal_cancel') );
                PayPal_Digital_Goods_Configuration::notify_url( base_url('index.php/users/paypal_notify') );
        
                PayPal_Digital_Goods_Configuration::environment( $this->environment );  
        }
        
        function create_purchase($data = array())
        {
            $this->set_credentials();
                
            $purchase_details = array(
                'name'        => 'Izeberg Package A',
                'description' => 'Izeberg Plan A with Quote and all',
                'amount'      => 3,
                'invoice_number'  => 'P'.time(),
            );
        
            return new PayPal_Purchase( $data + $purchase_details );
        }
        
		function create_subscription($data = array())
		{
			$this->set_credentials();
			
			$subscription_details = array(
				'description'        => 'Izeberg Plan A with Quote and all.',
				'amount'             => 3.00,
			);
			
			return new PayPal_Subscription( $data + $subscription_details );
		}
		
        function purchase_button()
        {        
                $this->purchase = $this->purchase_details();
                $checkout_url = $this->purchase->get_checkout_url();
                $element_id = 'planA_purchase_button';
                $print_script = '<script src ="https://www.paypalobjects.com/js/external/dg.js" type="text/javascript"></script>'
                                                        . '<script>'
                                                        . 'var dg = new PAYPAL.apps.DGFlow({'
                                                        . 'trigger: "' . $element_id . '"' // the ID of the HTML element which calls setExpressCheckout
                                                        . '}); </script>'; //$("#'.$element_id.'").click(function(){$(this).attr("disabled","disabled");});
                
                $print_button = '<a id="'.$element_id.'" class="btn btn-primary" href="'.$checkout_url.'" alt="Paying">Paying</a>';
                return $print_button.$print_script;             
        }
}