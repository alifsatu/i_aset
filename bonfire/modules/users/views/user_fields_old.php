<?php
/* /bonfire/modules/users/views/user_fields.php */

$currentMethod = $this->router->fetch_method();

$errorClass = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span4' : $controlClass;
$registerClass = $currentMethod == 'register' ? ' required' : '';
$editSettings = $currentMethod == 'edit';

$defaultLanguage = isset($user->language) ? $user->language : strtolower(settings_item('language'));
$defaultTimezone = isset($current_user) ? $current_user->timezone : strtoupper(settings_item('site.default_user_timezone'));


//echo "Value for Session = " . $this->session->userdata('company_id');
if ($this->session->userdata('company_id') > 0) {
    $comp_id = $this->session->userdata('company_id');
    $comp_query = $this->db->query('SELECT name from intg_companies where id =' . $comp_id);
    foreach ($comp_query->result() as $row) {
        $company_name = $row->name;
    }
    ?>
    <div class="form-group <?php echo iif(form_error('email'), $errorClass); ?>">
        <label class="control-label required" for="email">Company Name : </label><font size="3"><b>&nbsp;<?= $company_name ?></b></font>
    </div>
    <input type="hidden" name="company_id" value="<?= $comp_id ?>" />
    <?php
} else if ($this->session->userdata('company_id') == 0) {
    $query = $this->db->query("SELECT id,name FROM intg_companies WHERE deleted = 0 order by name");
    $company_options['0'] = "INTERNAL (Own Company)";
    foreach ($query->result() as $row) {
        $company_options[$row->id] = $row->name;
        //echo $row->role_name;
    }
    echo form_dropdown('company_id', $company_options, set_value('company_id', isset($user) ? $user->company_id : ''), 'Company Name' . lang('bf_form_label_required'), "class='selecta form-control'");
}
?>
    <br>
    <br>

<div class="form-group <?php echo iif(form_error('email'), $errorClass); ?>">
    <label class="control-label required" for="email"><?php echo lang('bf_email'); ?></label>
    <div class="controls" style="width:50%">
        <input class="form-control" type="text" id="email" name="email" value="<?php echo set_value('email', isset($user) ? $user->email : ''); ?>" />
        <span class="help-inline"><?php echo form_error('email'); ?></span>
    </div>
</div>
    
<div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
    <label class="control-label" for="display_name"><?php echo lang('bf_display_name'); ?></label>
    <div class="controls" style="width:50%">
        <input class="form-control" type="text" id="display_name" name="display_name" value="<?php echo set_value('display_name', isset($user) ? $user->display_name : ''); ?>" />
        <span class="help-inline"><?php echo form_error('display_name'); ?></span>
    </div>
</div>

<?php 
if (settings_item('auth.login_type') !== 'email' OR settings_item('auth.use_usernames')) { ?>
    <div class="form-group <?php echo iif(form_error('username'), $errorClass); ?>">
        <label class="control-label required" for="username"><?php echo lang('bf_username'); ?></label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="text" id="username" name="username" value="<?php echo set_value('username', isset($user) ? $user->username : ''); ?>" />
            <span class="help-inline"><?php echo form_error('username'); ?></span>
        </div>
    </div>
    <input type="hidden" id="username" name="username" value="<?php echo set_value('username', isset($user) ? $user->username : ''); ?>" />
<?php } ?>

<?php if (isset($user)) { ?>

    <div class="form-group <?php echo iif(form_error('password'), $errorClass); ?>">
        <label class="control-label<?php echo $registerClass; ?>" for="password"><?php echo lang('bf_password'); ?></label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="password" id="password" name="password" value="" />
            <span class="help-inline"><?php echo form_error('password'); ?></span>
            <p class="help-block"><?php
                if (isset($password_hints)) {
                    echo $password_hints;
                }
                ?></p>
        </div>
    </div>
    <div class="form-group <?php echo iif(form_error('pass_confirm'), $errorClass); ?>">
        <label class="control-label<?php echo $registerClass; ?>" for="pass_confirm"><?php echo lang('bf_password_confirm'); ?></label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="password" id="pass_confirm" name="pass_confirm" value="" />
            <span class="help-inline"><?php echo form_error('pass_confirm'); ?></span>
        </div>
    </div>
    <?php
} else {
    $email = "user@satuwater.com.my";
    ?>
    <input type="hidden" id="password" name="password" value="<?php echo substr(md5($email . time()), 0, 8); ?>" />
    <input type="hidden" id="pass_confirm" name="pass_confirm" value="<?php echo substr(md5($email . time()), 0, 8); ?>" />
    <?
    }
    ?>


    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">DOB</label>
        <div class="controls" style="width:50%">
            <input type="text" name="dob" id="dob" class="input-sm input-s datepicker-input form-control" size="16" placeholder="Date" value="<?php echo set_value('dob', isset($user) ? format_date($user->dob, "d/m/Y") : ''); ?>">
            <span class="help-inline"><?php echo form_error('display_name'); ?></span>
        </div>
    </div>
    <?php
// Change the values in this array to populate your dropdown as required

//    $designation = $this->db->query('SELECT * FROM intg_designation WHERE deleted = 0 ORDER BY designation ')->result();
    ?>
    <br>
<!--    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('Designation' . lang('bf_form_label_required'), 'subsidiary_id', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="designation_id" id="designation_id" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" >
                <option></option>
                <?php
                foreach ($designation as $d) {
                    if ($_POST['designation_id'] == $d->id) {
                        ?>
                        <option value=<?= $d->id ?> <?php
                        if ($_POST['designation_id'] == $d->id) {
                            echo "Selected";
                        }
                        ?>><b><?= $d->designation ?></b></option>
                                <?php
                            } else {
                                ?>


                        <option value=<?= $d->id ?> <?php
                        if ($user->designation_id == $d->id) {
                            echo "Selected";
                        }
                        ?>><b><?= $d->designation ?></b></option><?php
                            }
                        }
                        ?>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
        </div>
    </div>-->
    <?php
// Change the values in this array to populate your dropdown as required
    /* $options = array(
      255 => 255,
      );

      echo form_dropdown('sbu_company_id', $options, set_value('sbu_company_id', isset($sbu['company_id']) ? $sbu['company_id'] : ''), 'Company'. lang('bf_form_label_required'));
     */
//$companies = $this->db->query('SELECT * FROM intg_companies WHERE deleted = 0 ORDER BY name ')->result();
    ?>

                <!--<div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
    <?php echo form_label('Company' . lang('bf_form_label_required'), 'sbu_company_id', array('class' => 'control-label')); ?>
                       <div class='controls'>
                                <select name="sbu_company_id" id="sbu_company_id" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" required>
                <option></option>
    <?php foreach ($companies as $c) { ?>


                                <option value=<?= $c->id ?>><b><?= $c->name ?></b></option><?php } ?>
                </select>
                               <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
                       </div>
                </div>-->
    <?php
// Change the values in this array to populate your dropdown as required

    $companies = $this->db->query('SELECT * FROM intg_subsidiary WHERE deleted = 0 ORDER BY subsidiary_name ')->result();
    ?>

<!--    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('Subsidiary Name' . lang('bf_form_label_required'), 'subsidiary_id', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="subsidiary_id" id="subsidiary_id" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" >
                <option></option>
                <?php
                foreach ($companies as $c) {
                    if ($_POST['subsidiary_id'] == $c->id) {
                        ?>
                        <option value=<?= $c->id ?> <?php
                        if ($_POST['subsidiary_id'] == $c->id) {
                            echo "Selected";
                        }
                        ?>><b><?= $c->subsidiary_name ?></b></option><?} else {?>       

                        <option value=<?= $c->id ?> <?php
                        if (isset($user->subsidiary_id) && $user->subsidiary_id == $c->id) {
                            echo "Selected";
                        }
                        ?>><b><?= $c->subsidiary_name ?></b></option><?php
                            }
                        }
                        ?>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
        </div>
    </div>-->

    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">Highest Qualification</label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="text" id="highest_qualification" name="highest_qualification" value="<?php echo set_value('highest_qualification', isset($user) ? $user->highest_qualification : isset($_POST['highest_qualification'])); ?>" />
            <span class="help-inline"><?php echo form_error('display_name'); ?></span>
        </div>
    </div>

<!--    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">Joining Date</label>
        <div class="controls" style="width:50%">
            <input type="text" name="join_date" id="join_date" class="input-sm input-s datepicker-input form-control" size="16" placeholder="Date" value="<?php echo set_value('join_date', isset($user) ? format_date($user->joining_date, "d/m/Y") : isset($_POST['join_date'])); ?>">
            <span class="help-inline"><?php echo form_error('display_name'); ?></span>
        </div>
    </div>
    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">Leaving Date</label>
        <div class="controls" style="width:50%">
            <input type="text" name="leave_date" id="leave_date" class="input-sm input-s datepicker-input form-control" size="16" placeholder="Date" value="<?php
            if (empty($user->leaving_date) || $user->leaving_date == "0000-00-00") {
                echo NULL;
            } else {
                echo format_date($user->leaving_date, "d/m/Y");
            }
            ?>"> 
            <span class="help-inline"><?php echo form_error('display_name'); ?></span>
        </div>
    </div>-->
    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">Gender</label>
        <div class="controls" style="width:50%">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-sm btn-info <?php e(isset($user->gender) && $user->gender == "Male" ? 'active' : '') ?>">
                    <input type="radio" name="Gender" id="option1"  value="Male"  checked="checked" ><i class="fa fa-check text-active"></i> Male 
                </label>
                <label class="btn btn-sm btn-danger <?php e(isset($user->gender) && $user->gender == "Female" ? 'active' : '') ?>">
                    <input type="radio" name="Gender" id="option2" value="Female"><i class="fa fa-check text-active"></i> Female
                </label>		                
            </div>
        </div>
    </div>



    <?php
//    $companies = $this->db->query('SELECT * FROM intg_companies WHERE deleted = 0 ORDER BY name ')->result();
    ?>

    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('Race' . lang('bf_form_label_required'), 'sbu_company_id', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="race" id="race" class="selecta form-control"  style="height:30px;width:200px; font-size:12px">
                <option></option>
                <option value="Malay" <?php
                if (isset($user->race) && $user->race == "Malay") {
                    echo "selected";
                }
                ?>><b>Malay</b></option>
                <option value="Chinese" <?php
                if (isset($user->race) && $user->race == "Chinese") {
                    echo "selected";
                }
                ?>><b>Chinese</b></option>
                <option value="Indian" <?php
                if (isset($user->race) && $user->race == "Indian") {
                    echo "selected";
                }
                ?>><b>Indian</b></option>
                <option value="Bumi" <?php
                if (isset($user->race) && $user->race == "Bumi") {
                    echo "selected";
                }
                ?>><b>Bumi</b></option>
                <option value="Others" <?php
                if (isset($user->race) && $user->race == "Others") {
                    echo "selected";
                }
                ?>><b>Others</b></option>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
        </div>
    </div>

<!--    <div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
        <label class="control-label" for="display_name">Bumi/ Nonbumi</label>
        <div class="controls" style="width:50%">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-sm btn-info <?php e(isset($user->bumi) && $user->bumi == "Yes" ? 'active' : '') ?>">
                    <input type="radio" name="Bumi" id="option1"  value="Yes"  checked="checked" ><i class="fa fa-check text-active"></i> Yes 
                </label>
                <label class="btn btn-sm btn-danger <?php e(isset($user->bumi) && $user->bumi == "No" ? 'active' : '') ?>">
                    <input type="radio" name="Bumi" id="option2" value="No"><i class="fa fa-check text-active"></i> No
                </label>		                
            </div>
        </div>
    </div>-->

    <?php
    // Change the values in this array to populate your dropdown as required

    $prgm = $this->db->query('SELECT * FROM  intg_sbu WHERE deleted = 0 ORDER BY sbu_name ')->result();
    ?>
    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('SBU' . lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="sbu" id="sbu" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" >
                <option></option>
                <?php foreach ($prgm as $pn) { ?>


                    <option value=<?= $pn->id ?> <?php
                    if (isset($user->sbu_id) && $user->sbu_id == $pn->id) {
                        echo "Selected";
                    }
                    ?>><b><?= $pn->sbu_name ?></b></option><?php } ?>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
        </div>
    </div>

    <?php
    // Change the values in this array to populate your dropdown as required

    $cc = $this->db->query('SELECT * FROM  intg_cost_centre WHERE deleted = 0 ORDER BY cost_centre ')->result();
    ?>
    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('Cost Centre' . lang('bf_form_label_required'), 'cost_centre_sbu', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="cost_centre" id="cost_centre" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" >
                <option></option>
                <?php foreach ($cc as $c) { ?>


                    <option value=<?= $c->id ?> <?php
                    if (isset($user->cost_centre_id) && $user->cost_centre_id == $c->id) {
                        echo "Selected";
                    }
                    ?>><b><?= $c->cost_centre ?></b></option><?php } ?>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>
        </div>
    </div>

    <?php
    // Change the values in this array to populate your dropdown as required

    $ul = $this->db->query('SELECT * FROM  intg_user_level WHERE deleted = 0 ORDER BY user_level ')->result();
    ?>
    <div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
        <?php echo form_label('User Level' . lang('bf_form_label_required'), 'user_level', array('class' => 'control-label')); ?>
        <div class='controls'>
            <select name="user_level" id="user_level" class="selecta form-control"  style="height:30px;width:200px; font-size:12px">
                <option></option>
                <?php foreach ($ul as $u) { ?>


                    <option value=<?= $u->id ?> <?php
                    if (isset($user->user_level_id) && $user->user_level_id == $u->id) {
                        echo "Selected";
                    }
                    ?>><b><?= $u->user_level ?></b></option><?php } ?>
            </select>
            <span class='help-inline'><?php echo form_error('sbu_name'); ?></span>


        </div>


    </div>
    <div class="form-group <?php echo iif(form_error('origin'), $errorClass); ?>">
        <label class="control-label" for="display_name">HQ/ Estate *</label>
        <div class="controls" style="width:50%">
            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-sm btn-info <?php e(isset($user->origin) && $user->origin == "HQ" ? 'active' : '') ?>">
                    <input type="radio" name="origin" id="option1"  value="HQ"  checked="checked" ><i class="fa fa-check text-active"></i> HQ 
                </label>
                <label class="btn btn-sm btn-danger <?php e(isset($user->origin) && $user->origin == "Estate" ? 'active' : '') ?>">
                    <input type="radio" name="origin" id="option2" value="Estate"><i class="fa fa-check text-active"></i> Estate
                </label>		                
            </div>
        </div>
    </div>

    <?php if ($editSettings) : ?>
                                                    <!--<div class="form-group <?php echo iif(form_error('force_password_reset'), $errorClass); ?>">
                                                        <div class="controls" style="width:50%">
                                                            <label class="checkbox" for="force_password_reset">
                                                                <input type="checkbox" id="force_password_reset" name="force_password_reset" value="1" <?php echo set_checkbox('force_password_reset', empty($user->force_password_reset)); ?> />
        <?php echo lang('us_force_password_reset'); ?>
                                                            </label>
                                                        </div>
                                                    </div>-->
        <?php
    endif;



    // Change the values in this array to populate your dropdown as required
    /* 			$company = $this->db->get_where('intg_companies', array('deleted'=>'0'))->result();
      $cname[''] = "- - -Please Select- - -";
      foreach($company as $h)
      {
      $cname[$h->id] = $h->name;
      }

      echo form_dropdown('company_id', $cname, set_value('company_id', isset($user->company_id) ? $user->company_id : ''), 'Company'. lang('bf_form_label_required')); */

    if (isset($languages) && is_array($languages) && count($languages)) :
        if (count($languages) == 1):
            ?>
            <input type="hidden" id="language" name="language" value="<?php echo $languages[0]; ?>" />
            <?php
        else :
            ?>
            <div class="form-group <?php echo iif(form_error('language'), $errorClass); ?>">
                <label class="control-label required" for="language"><?php echo lang('bf_language'); ?></label>
                <div class="controls" style="width:50%">
                    <select name="language" id="language" class="chzn-select selecta form-control">
                        <?php foreach ($languages as $language) : ?>
                            <option value="<?php e($language); ?>" <?php echo set_select('language', $language, $defaultLanguage == $language ? true : false); ?>>
                                <?php e(ucfirst($language)); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <span class="help-inline"><?php echo form_error('language'); ?></span>
                </div>
            </div>
        <?php
        endif;
    endif;
    ?>
    <div class="form-group <?php echo iif(form_error('timezone'), $errorClass); ?>">
        <label class="control-label required" for="timezones"><?php echo lang('bf_timezone'); ?></label>
        <div class="controls" style="width:70%">
            <?php echo timezone_menu(set_value('timezones', isset($user) ? $user->timezone : $defaultTimezone), 'selecta form-control'); ?>
            <span class="help-inline"><?php echo form_error('timezones'); ?></span>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
    $('document').ready(function () {

        $("#join_date,#leave_date,#dob").datepicker({format: "dd/mm/yyyy"});
    });

</script>

