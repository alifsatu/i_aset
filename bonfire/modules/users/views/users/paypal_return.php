<section id="content" class="m-t-lg wrapper-md animated fadeInUp">    

	<div class="container aside-xxl">
	
		<div align="center"><img src="<?=base_url('images/logo.png')?>" width="23" align="baseline" ><strong style="font-size:20px;color:#999;">&nbsp;SPP</strong></div>

		<section class="panel panel-default bg-white m-t-lg">
		
			<header class="panel-heading text-center">

			<?php if(isset($response) && $response['ACK'] == 'Success'):?>
				<strong>Aww yeah! You will received your login details shortly after we receive your first payment.</strong>

			</header>
		
			<div class="col-sm-4 animated fadeInRightBig" style="width:100%">
			<section class="panel b-primary text-center m-t">
				<header class="panel-heading bg-primary"><h3 class="m-t-sm"><?= $package->package_name?></h3>
                <p><?=$package->description?></p>
              </header>
              <ul class="list-group">
                <li class="list-group-item text-center bg-light lt">
                  <div class="padder-v">
                    <span class="text-danger font-bold h4">
                     	<?=$package->license_duration?> <?=$package->duration_metric?>, <?=$package->no_of_users?> Users
                    </span> 
                  </div>
                </li>
				<?php $modules = explode(',',$package->modules);?>
				<?php foreach($modules as $module):?>
				<li class="list-group-item">
					<?php
								switch(strtolower($module)) {
									case 'content':
										echo 'Inventory';
										break;
									case 'ideabank':
										echo 'Socialize';
										break;
									default:
										echo $module;
								}
							?>
				</li>
				<?php endforeach;?>
                
              </ul>
			  <footer class="panel-footer">
                <a href="<?=site_url('login')?>" class="btn btn-primary m-t m-b">Login Now</a>
              </footer>
            </section>
          </div>
             
			<?php else:?>
			<strong>Oops! Something went wrong.</strong> <a href="<?=site_url('packages')?>"><u>Go back</u></a>
			
			</header>
			<?php endif;?>
			 
            </section>
          </div>
		 


	

 </section>



</div>

 </section>