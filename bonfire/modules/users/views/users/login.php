<?php

	$site_open = $this->settings_lib->item('auth.allow_register');

?>

<!--<p><br/><a href="<?=base_url('..')?>">&larr; <?php echo lang('us_back_to') . $this->settings_lib->item('site.title'); ?></a></p>-->

<section id="content" class="m-t-lg wrapper-md animated fadeInUp">    

 <div class="container aside-xxl">
     <div align="center"><img src="<?=base_url('images/spp_logo.png')?>" height="80" align="baseline" >
 </div>
     <div align="center"><br><strong style="font-size:30px;color:#999;">&nbsp;Sistem i-Aset SATU</strong> </div>

   <section class="panel panel-default bg-white m-t-lg">
<header class="panel-heading text-center">

         <strong><?php echo lang('us_login'); ?></strong>

        </header>
        

	
<div class="row-fluid">

		<div style="padding:5px">

			<?php //if(Template::message()!="You must be logged in to view that page.") { echo Template::message(); }  ?>
			<?php echo Template::message();  ?>



		</div>

	</div>


	

	<?php

		if (validation_errors()) :

	?>

	<div class="row-fluid">

		<div class="span12">

			<div class="alert alert-error fade in">

			  <a data-dismiss="alert" class="close">&times;</a>

				<?php echo validation_errors(); ?>

			</div>

		</div>

	</div>

	<?php endif; ?>



	<?php echo form_open(LOGIN_URL, array('autocomplete' => 'off' , 'class' => 'panel-body wrapper-lg')); ?>



		 <div class="form-group <?php echo iif( form_error('login') , 'error') ;?>">

			<label class="control-label">Email</label>

				<input  type="text" name="login" id="login_value" value="<?php echo set_value('login'); ?>" tabindex="1" placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') .'/'. lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>" class="form-control input-lg" />

			

		</div>



		 <div class="form-group <?php echo iif( form_error('password') , 'error') ;?>">

			 <label class="control-label">Password</label>

				<input type="password" name="password" id="password" value="" tabindex="2" placeholder="<?php echo lang('bf_password'); ?>"  class="form-control input-lg">

			

		</div>
		
	
               
		<?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
			
			<div class="checkbox">
            <label>
              <input type="checkbox" name="remember_me" id="remember_me" value="1"> <?php echo lang('us_remember_note'); ?>
            </label>
          </div>
				
			<!--
			<label  for="remember_me">
				<input type="checkbox" name="remember_me" id="remember_me" value="1" tabindex="3" />
				<span class="inline-help"><?php echo lang('us_remember_note'); ?></span>
			</label>
			-->	
			
		<?php endif; ?>

		 <div class="form-group">

			

				<input class="btn btn-primary" type="submit" name="log-me-in" id="submit" value="<?php e(lang('us_let_me_in')); ?>" tabindex="5" />

			       <?php echo anchor('/forgot_password', '<small>'.lang('us_forgot_your_password').'</small>', 'class="pull-right m-t-xs"'); ?>
		
		</div>

         <div class="line line-dashed"></div>
		
 

	<?php echo form_close(); ?>



	<?php // show for Email Activation (1) only

		if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>

	<!-- Activation Block -->

			<p style="text-align: left" class="well">

				<?php echo lang('bf_login_activate_title'); ?><br />

				<?php

				$activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]',anchor('/activate', lang('bf_activate')),lang('bf_login_activate_email'));

				$activate_str = str_replace('[ACTIVATE_RESEND_URL]',anchor('/resend_activation', lang('bf_activate_resend')),$activate_str);

				echo $activate_str; ?>

			</p>

	<?php endif; ?>



	<p style="text-align: center">

		<?php // if ( $site_open ) : ?>
<?php //echo anchor(PACKAGES_URL, lang('us_sign_up')); ?>
			<?php //echo anchor(REGISTER_URL, lang('us_sign_up')); ?>

		<?php //endif; ?>

 

	
  </p>
 </section>



</div>


 </section>
 <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <p>
      <div class="container aside-xl">
      </p>
    </div>
  </footer>
 <div id="fb-root"></div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


 
 