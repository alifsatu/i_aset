<style type='text/css'>
.expand {
	height: 1em;
	padding: 3px;
	width:100%;
	font-size:12px;
	overflow:hidden;
}
.hcur {
	float:left;
	font-weight:bold
}




</style>

<script>

function remrow(val)
	{	 	
	  $("#"+val).hide('fast', function(){  $("#"+val).remove(); });
	 setTimeout(function() {
      calcamount()
}, 500);
		}
		
		function calcamount()
		{
			 var total = 0;
$(".amount").each(function() {   
            total += parseFloat($(this).text());
      
 });
$("#totalval").html(numberWithCommas(parseFloat(total).toFixed(2)));
		}
		
		function remfile(value)
		{			

$.post("<?php echo site_url(SITE_AREA .'/quotes/quote/delattach') ?>", { filename:""+value+""} ).done( 
function(data){
	
	$('.itemDelete').on('click', function(){
		
    $(this).html(data);
	
});

	
	
	} ).fail(function() { alert( "failed delete" ); } );


	
}
		
	</script>
<?php

//echo $_SERVER['SCRIPT_FILENAME'];

$validation_errors = validation_errors();

if ($validation_errors) :
?>
<!--<link href="../../../../../themes/notebook/css/bootstrap.css" rel="stylesheet" type="text/css" />
-->
<div class="alert alert-block alert-error fade in"> <a class="close" data-dismiss="alert">&times;</a>
  <h4 class="alert-heading"><? echo lang('quote_create_error'); ?></h4>
  <?php echo $validation_errors; ?> </div>
<?php
endif;

if (isset($quote))
{
	$quote = (array) $quote;
}
$id = isset($quote['id']) ? $quote['id'] : '';

?>
<?php echo form_open_multipart($this->uri->uri_string(), 'id="forma" role="form" data-parsley-validate'); ?>

<div class="row">
  <div class="col-sm-16">
    <section class="panel panel-default">
    <header class="panel-heading">
      <h3 class="m-b-none"><span style="float:left; margin-right:10px"><? echo lang('quote_quote')?> #</span>
 
      <input id='quote_quote_number' class="input-sm input-s  form-control" type='text' name='quote_quote_number' maxlength="45" value="<?php 
	  
	 $inv_grp_def = $this->db->query('SELECT * FROM intg_quote_group WHERE deleted = 0 AND company_id='.$current_user->company_id.' AND quote_group_name ="Default"');
		$invprefix = $inv_grp_def->row();
	  $q1 = $this->db->query("select max(version_no) as rmax from intg_quote where  company_id = '".$current_user->company_id."'");
	  
	  if ( $inv_grp_def->num_rows() > 0  )  { $r1 = $q1->row(); $r2 = $r1->rmax + 1; echo $invprefix->quote_group_prefix."-".$invprefix->quote_group_prefix_month."-".$invprefix->Quote_group_prefix_year."-".$r2;  } else { $r1 = $q1->row(); echo $r1->rmax + 1; }
	  ?>"   qnum="<?=$r1->rmax + 1?>" required></h3>
     
   
    </header>
    <div class="panel-body">
      <div class="col-sm-12"> 
        <div class="form-group pull-in clearfix">
          <div class="col-sm-3">
          <div class="form-group">
          <label><? echo lang('quote_client')?></label>
            <div class="controls">
            <?php // Change the values in this array to populate your dropdown as required
				$category_names = $this->db->query('SELECT * FROM intg_clients WHERE deleted = 0  ORDER BY client_name  ')->result();

			?>
             <select name="quote_client_id" id="quote_client_id" class="form-control client selecta" style="height:30px; font-size:12px"  required >
                    <option></option>
                    <?
                foreach($category_names as $h)
{ ?>
                    <option value="<?=$h->id?>"><?=$h->client_name?></option>
                    <? }
				
				?>
                  </select>
          </div></div></div>
          <div class="col-sm-3">
            <label><? echo lang('quote_date'); ?></label>
            <input type="text" name="quote_date" id="quote_date" class="input-sm input-s datepicker-input form-control" size="16" placeholder="Date" required>
          </div>
          
          <div class="col-sm-3">
            <label><? echo lang('quote_duedate'); ?></label>
            <input id='quote_Quote_due_date' name='quote_Quote_due_date' value="<?php echo set_value('quote_Quote_due_date', isset($quote['Quote_due_date']) ? $quote['Quote_due_date'] : ''); ?>" class="input-sm input-s datepicker-input form-control" size="16" type="text" placeholder="Date"  required>
          </div>
          
          <div class="col-sm-3">
          <div class="form-group">
          <label><? echo lang('quote_currency'); ?></label>
          <div class="controls">
            <?  
			
		$currency = $this->db->query('SELECT * FROM intg_currency WHERE deleted = 0 ORDER BY id ')->result();
		$curr = $this->db->query("select currency_id from intg_companies where id = '".$current_user->company_id."'")->row();
		//echo $this->db->last_query();
		
			?>
            <select name="currency_id" id="currency_id" class="selecta form-control" onchange="$('.ccur').html(($('option:selected', this).attr('homecurr')));alert('Change of currency occurred, please review your price')" style="height:30px; font-size:12px" required>
             <option></option>
            <? foreach($currency as $c) { if ( $curr->currency_id == $c->id ) {?>
            <option value="<?=$c->id?>" selected="selected" homecurr="<?=$c->home_currency?>"><b><?=$c->converted_currency?></b></option>
            
            
            <? } else { ?>
             <option value="<?=$c->id?>" homecurr="<?=$c->home_currency?>"><b><?=$c->converted_currency?></b></option><? } } ?>
            </select></div></div>
          </div>
        </div>
        <div class="form-group pull-in clearfix">
          <div class="col-sm-3">
          <div id="files" class="files"></div>
          <div id="quotefile"><ul id="ul-sample"></ul></div>
            <div style="height:0px;overflow:hidden">
                              <a href="<? echo base_url();?>index.php/admin/settings/fileupload/index/quote" id="fileuploader"   data-toggle="ajaxModal1"></a>
                              <input type="file" id="filesList" multiple="multiple"  accept="image/png"/></div>
                            <ul class="nav nav-pills nav-sm">
                              <li><a href="#" onclick="$('#filesList').click();"><i class="fa fa-tags text-default"></i>&nbsp;<i class="icon-upload"></i>Attach Files</a></li> 
                              
                             
                             
                            </ul>
                        
          
            <input type="hidden" name="filename" id="filename" />
           <input name="filerename" id="filerename" type="hidden" />
               

          </div>
           <div class="col-sm-3"><div class="form-group">
          <label><? echo lang('quote_group')?></label>
          <div class="controls">
            <?  
			
				$quotegrp = $this->db->query('SELECT * FROM intg_quote_group WHERE deleted = 0 and company_id='.$this->session->userdata('company_id').' ORDER BY id ')->result();
		
			?>
            <select name="quote_group_id" id="quote_group_id"  onchange="$('#quote_quote_number').val($('option:selected', this).attr('prefix')+'-'+$('option:selected', this).attr('ym')+'-'+$('#quote_quote_number').attr('qnum'))"  class="selecta form-control"  style="height:30px; font-size:12px" >
             
            <? foreach($quotegrp as $qg) { if ( $qg->quote_group_name=="Default" ) { ?>
            <option selected="selected" value="<?=$qg->id?>" prefix="<?=$qg->quote_group_prefix?>" ym="<?=$qg->quote_group_prefix_month."-".$qg->Quote_group_prefix_year?>"><b><?=$qg->quote_group_name?></b></option>
            
            
            <? } else { ?><option value="<?=$qg->id?>" prefix="<?=$qg->quote_group_prefix?>" ym="<?=$qg->quote_group_prefix_month."-".$qg->Quote_group_prefix_year?>"><b><?=$qg->quote_group_name?></b></option><? } } ?>
            </select></div></div></div>
          <div class="col-sm-6">
           <label><? echo lang('quote_quote_notes'); ?></label> <?php echo form_textarea( array( 'name' => 'quote_quote_notes','placeholder'=>'Type your message', 'class' => 'form-control parsley-validated expand','id' => 'quote_quote_notes',  'rows' => '1', 'cols' => '', 'value' => set_value('quote_quote_notes', isset($quote['quote_notes']) ? $quote['quote_notes'] : '') ) ); ?> </div>
        </div>
      </div>
      <div class="clearfix"></div>
      
      
      
      <div class="col-sm-16" id="lineitem_form">
        <header class="panel-heading font-bold" style="color: #ffffff;  background-color: rgb(83, 181, 103);  "><? echo lang('quote_product'); ?></header>
        <section class="panel panel-default">
        <div class="table-responsive ">
        
          <table class="table table-striped b-t b-light text-sm " id="product_table">
            <thead>
              <tr>
                <th width="58"><? echo lang('quote_product'); ?></th>
                <th width="356"><? echo lang('quote_description'); ?></th>
                <th width="68"><? echo lang('quote_quantity'); ?></th>
                 <th width="152"><? echo lang('quote_price'); ?> (<span class="ccur"><?=$curr->converted_currency?></span>)</th>
                <th width="100"><? echo lang('quote_tax'); ?> %</th>
                <th width="97"><? echo lang('quote_tax'); ?></th>
                 <th width="122"><? echo lang('quote_amount'); ?> (<span class="ccur"><?=$curr->converted_currency?></span>)</th>
                <th width="19">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <tr id="row1">
                <td><?php 
				$inventory = $this->db->query('SELECT inv.id AS invid,inv.inventory_description,inv.inventory_name,inv.inventory_unit_price,inv.inventory_tax_rate_id FROM intg_inventory inv,intg_companies cmp WHERE inv.client_id = cmp.id AND inv.client_id = "'.$this->auth->company_id().'" AND inv.deleted = 0 ORDER BY inv.inventory_name asc ')->result();
				  ?>
                  <select name="select[]" id="select1" class="selecta form-control product req" style="height:30px; font-size:12px; width:140px" onchange="select2onchange('1')"  required><option></option>
                    <?
                foreach($inventory as $inv)
{ ?>
                    <option value="<?=$inv->invid?>" desc="<?=$inv->inventory_description?>"  price="<?=$inv->inventory_unit_price?>" taxid ="<?=$inv->inventory_tax_rate_id?>"  ><?=$inv->inventory_name?></option>                    
                    <? }
					
				
				?>
                  </select><input type="hidden" name="itemname[]" id="itemname1"/></td>
                <td><textarea name="description[]" id="description1" cols="60" rows="1" class="form-control  expand req"></textarea></td>
                <td><input type="text" name="quantity[]" id="quantity1"  class="input-sm  form-control" value="1" onkeyup="calcamount('1')" required/></td>
                <td><input type="text" name="price[]" id="price1" class="input-sm  form-control req"  data-parsley-group="block1" onkeyup="calcamount('1')" required/></td>
                <td><select  multiple name="tax[]" id="tax1"  class="taxes  populate select2-offscreen selecta" style="font-size:12px; width:100px" onchange="calcamount('1')" data-parsley-ui-enabled="false">            
               <?
			   $qtax = $this->db->query("SELECT id,tax_rate_name,tax_rate_percent FROM intg_tax_rates WHERE deleted=0 and company_id='".$this->auth->company_id()."'")->result();
			   
			   foreach ( $qtax as $taxval ) { $tid[] = $taxval->id; $taxname[]=$taxval->tax_rate_name; $taxpercent[] = $taxval->tax_rate_percent; }
			   
			   foreach ( $tid as $idv=>$idk )
			   { ?>
			   <option  value="<?=$tid[$idv]?>" ids="<?=$taxpercent[$idv]?>"><?=$taxname[$idv]?></option>
			   <? }
			   ?>
             
                </select>
                <input type="hidden" name="tax_arr[]" id="tax_arr1" /><input type="hidden" name="tax_perc[]" id="tax_perc1" />
               
                </td>
                <td><input name="amtax1" type="text" class="input-sm  form-control amtax req" id="amtax1"  value="0" size="10" required/></td>
                <td><div id="amount1" class="amount">0</div></td>
                <td><!--<i class="fa fa-times-circle fa-2x" style="color: #F00; font-size:14px" onclick="remrow('row1')"></i>--></td>
              </tr>
            </tbody>
            <tfoot>
              <tr id="lastrow" counter=1>
                <td colspan="5"></td>
                <td align="right"><? echo lang('quote_subtotal'); ?>:</td>
                <td><div id="stotal">0</div></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5">&nbsp;</td>
                <td align="right"><? echo lang('quote_tax'); ?>:</td>
                <td><div id="taxval">0</div></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="4"><a href="javascript:void(0)" onclick="addnewrow()"><i class="fa fa-plus-circle fa-1px" style="color: #090; font-size:14px"></i>&nbsp;&nbsp;<strong><? echo lang('quote_add_line'); ?></strong></a></td>
                <td colspan="2" align="right"><strong><? echo lang('quote_total'); ?> (<span class="ccur"></span>):</strong></td>
                <td><strong><div id="totalval">0</div><input type="hidden" name="total_amount" id="total_amount" value="0"></strong></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
               <td colspan="8"> 
        
        <a href="<? echo base_url();?>index.php/admin/settings/tnc/create/modal" id="btn_quote_to_invoice" data-quote-id="<?=$this->uri->segment(5)?>"  data-toggle="ajaxModal3"><i class="fa fa-tags text-default"></i>&nbsp;<i class="icon-upload"></i>Terms & Conditions</a><br />
        <div id="tncmodal"></div>
        <textarea id="tncmodal2" name="tncmodal2" style="display:none"></textarea>
                     
                      </td></tr>
            </tfoot>
          </table>
         
        </div>
        
      </div>
      <div   class="form-group">
    
       <input type="hidden" name="client_name" id="client_name" />
        <input type="submit"  name="save" class="btn btn-primary"  id="submit"  value="<?php echo lang('quote_action_create'); ?>"  />
        &nbsp;&nbsp; <?php echo anchor(SITE_AREA .'/quotes/quote', lang('quote_cancel'), 'class="btn btn-warning"'); ?> &nbsp;&nbsp;  &nbsp;&nbsp;  <input type="submit" id="draft" name="draft" class="btn btn-info" value="Save As Draft"  />  </div>
      </div>
  </div>
  </section>
</div> <?php echo form_close(); ?>
<!--end of row -->

<script>
function addnewrow()
{
	    var theId = $("#lastrow").attr("counter") + 1;	
		
			$("#lastrow").attr("counter", parseInt(theId)+1) ;	
	
	$('#lastrow').before('<tr id="row'+theId+'"><td><select name="select[]" id="select'+theId+'" class="selecta form-control product" onchange="select2onchange('+theId+')" required  style="height:30px; font-size:12px;  width:140px" ><option></option></select><input type="hidden" name="itemname[]" id="itemname'+theId+'"/></td><td><textarea name="description[]" id="description'+theId+'"  cols="80" rows="1" class="form-control  expand"></textarea></td><td><input type="text" name="quantity[]" id="quantity'+theId+'" class="input-sm  form-control" onkeyup="calcamount('+theId+')" value="1" required/></td><td><input type="text" onkeyup="calcamount('+theId+')" name="price[]" id="price'+theId+'"  class="input-sm  form-control" required/></td><td><select  multiple name="tax[]" id="tax'+theId+'" class=" taxes  populate select2-offscreen selecta" style="font-size:12px; width:100px" onchange="calcamount('+theId+')" data-parsley-ui-enabled="false"><? foreach ( $tid as $idv=>$idk )
			   { ?><option   value="<?=$tid[$idv]?>" ids="<?=$taxpercent[$idv]?>"><?=$taxname[$idv]?></option><? }			    
			   ?></select><input type="hidden" name="tax_arr[]" id="tax_arr'+theId+'" /><input type="hidden" name="tax_perc[]" id="tax_perc'+theId+'" /></td><td><input type="text" id="amtax'+theId+'"   value="0"  class="input-sm  form-control amtax" required/></td><td><div id="amount'+theId+'" class="amount" required>0</div></td><td><i class="fa fa-times-circle fa-1.5x" style="color: #F00; font-size:14px" " onclick="remrow(\'row'+theId+'\')"></i></td></tr>');
			   
			   
	     $('#select1 option').clone().appendTo('#select'+theId);
		$('#itemname'+theId).val($('option:selected', "#select1").text());	   
			   
	
		$("#tax"+theId).select2(		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:false}	);	$("#select"+theId).select2(		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:true}	);
		
		$("#select"+theId).
	// append footer at the end of dropdown list
select2('container').find('div.select2-drop').append('<div class="panel-footer text-sm"><a href="<? echo base_url();?>index.php/admin/content/inventory/ajax_create" data-select="#select'+theId+'"  data-s2option_name="inventory_name"  data-toggle="ajaxModal" data-s2option_desc="inventory_description" data-s2option_rowid="'+theId+'"  data-s2option_price="inventory_unit_price" onclick="add_new_option($(this),\'inventory\')"><img src="<?php echo Template::theme_url('images/add-15.png'); ?>"/>&nbsp;&nbsp;Add New</a></div>') ;

		
		$('textarea.expand').focus(function () {
   // $(this).animate({ height: "4em" }, 500); 
});

$('textarea.expand').blur(function () {
   // $(this).animate({ height: "2em" }, 500); 
});

$( '#forma' ).parsley( 'addItem', '#select'+theId );
	$( '#forma' ).parsley( 'addItem', '#quantity'+theId );
	$( '#forma' ).parsley( 'addItem', '#price'+theId );
	$( '#forma' ).parsley( 'addItem', '#tax'+theId );
	
	
	}
	
	
	function select2onchange(val)
	{		
	
	var descr = $('option:selected', "#select"+val).attr('desc');
	var price =  $('option:selected', "#select"+val).attr('price');
	var qty =  $('option:selected', "#select"+val).attr('qty');	
	var taxid = $('option:selected', "#select"+val).attr('taxid');	
	
	$("#tax"+val+" option:selected").removeAttr("selected");
	
  var valArr = taxid, // array of option values
    i = 0, size = valArr.length; // index and array size declared here to avoid overhead
    $options = $("#tax"+val+" option"); // options cached here to avoid overhead of fetching inside loop

// run the loop only for the given values
for(i; i < size; i++){
    // filter the options with the specific value and select them
    $options.filter('[value="'+valArr[i]+'"]').prop('selected', true);
}
	
	
	var itemnametext = $('option:selected', "#select"+val).text();	
	$("#itemname"+val).val(itemnametext);
	
	$("#quantity"+val).attr("data-parsley-max",qty)
	if ( parseInt($("#quantity"+val).val()) > parseInt(qty) ) 
	{ 
	$("#quantity"+val).attr("data-parsley-max",qty)	
	} 
	else {
$("#description"+val).val(descr);
	$("#price"+val).val(price);
	 $("#price"+val).removeClass('parsley-error');
		   var aid = $("#price"+val).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
	}
	//$("#quantity"+val).val(qty);	
	
	var tax = $("#tax"+val).select2("val");
	
	$("#tax_arr"+val).val(tax);
	
	var taxperc = 0;
    	$('#tax'+val+' > option:selected').each(function() {
    		var values = $(this).attr('ids');

    		taxperc = parseFloat(taxperc) + parseFloat(values);
    		taxperc = taxperc.toFixed(2);
    	});
		$("#tax_perc"+val).val(taxperc);
	
	$("#tax"+val).select2(		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:false}	);
		
		
	calcamount(val);	
	}
	
	
	
	function calcamount(val)
	{
		
		var curr = $('option:selected', "#currency_id").attr('homecurr');
		var concurr = $('option:selected', "#currency_id").text();
		$(".hcur").html(curr);
		$(".ccur").html('<b>'+concurr+'</b>');
		var qty = $("#quantity"+val).val()*1;
		var price = $("#price"+val).val()*1;
		
		var tax = 0;
    	$('#tax'+val+' > option:selected').each(function() {
    		var value = $(this).attr('ids');

    		tax = parseFloat(tax) + parseFloat(value);
    		tax = tax.toFixed(2);
    	});
			$("#tax_perc"+val).val(tax);
		
		$("#amtax"+val).val( isNaN(qty*price) ? 0 : parseFloat(qty*price*(tax/100)).toFixed(2));
		$("#amount"+val).html(isNaN(qty*price) ? 0 : parseFloat(qty*price).toFixed(2));
		 var total = 0;
$(".amount").each(function() {    total += parseFloat($(this).text());});
var taxtotal = 0;
$(".amtax").each(function() {    taxtotal += parseFloat($(this).val());});
$("#stotal").html(isNaN(total) ? 0 : parseFloat(total).toFixed(2));
		$("#taxval").html(isNaN(taxtotal) ? 0 :parseFloat(taxtotal).toFixed(2));
		$("#totalval").html(isNaN(total+taxtotal) ? 0 : numberWithCommas(parseFloat(total+taxtotal).toFixed(2)));
$("#total_amount").val(total+taxtotal);


	}
	
	function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
	
$(document).ready(function(){
	
	
		
window.ParsleyConfig = {
  errorsWrapper: '<div class="pe"></div>',
  errorTemplate: '<div class="parsley-required bounceIn animated"></div>'
};
	
	 $("#currency_id").select2(		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:false}	);
	
	$('#quote_date').datepicker().on('changeDate', function(){
          $('.datepicker').hide();
		   $(this).removeClass('parsley-error');
		   var aid = $(this).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
        });
		$('#quote_Quote_due_date').datepicker().on('changeDate', function(){
          $('.datepicker').hide();
		   $(this).removeClass('parsley-error');
		   var aid = $(this).attr("data-parsley-id");
		   $('#parsley-id-'+aid).remove();
        });
		
	
	
$('#quote_client_id').

select2('container').find('div.select2-drop').append('<div class="panel-footer text-sm"><a href="<? echo base_url();?>index.php/admin/client/clients/ajax_create" data-select="#quote_client_id" data-toggle="ajaxModal" data-s2option_name="clients_client_name"  onclick="add_new_option($(this),\'client\')"><img src="<?php echo Template::theme_url('images/add-15.png'); ?>"/>&nbsp;&nbsp;Add New</a></div>') ;


$('#quote_client_id').change(function() {
	
$("#client_name").val($("#quote_client_id option:selected" ).text());
});

$('#select1').
	// append footer at the end of dropdown list
select2('container').find('div.select2-drop').append('<div class="panel-footer text-sm"><a href="<? echo base_url();?>index.php/admin/content/inventory/ajax_create" data-select="#select1"  data-s2option_name="inventory_name" data-s2option_price="inventory_unit_price"  data-s2option_desc="inventory_description" data-s2option_rowid="1"  data-toggle="ajaxModal" onclick="add_new_option($(this),\'inventory\')"><img src="<?php echo Template::theme_url('images/add-15.png'); ?>"/>&nbsp;&nbsp;Add New</a></div>') ;


$('textarea.expand').focus(function () {
   // $(this).animate({ height: "4em" }, 500); 
});

$('textarea.expand').blur(function () {
   // $(this).animate({ height: "2em" }, 500); 
});



});


function add_new_option($this,val2) {
	
	
		$($this.data('select')).select2('container').select2('close');
		
		$('#ajaxModal').remove();
        //e.preventDefault();
        //var $this = $(this)
        var $remote = $this.data('remote') || $this.attr('href') , 
			$modal = $('<div class="modal fade" id="ajaxModal" style="width:auto;height:auto;max-height:100%"><div class="modal-dialog"><div class="modal-content">'+
			'<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">&nbsp;</h4></div>'+
			'<div class="modal-body"></div>'+
			'<div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Close</a><a href="#" class="btn btn-primary" id="save-modal"  data-loading-text="Updating..." >Save</a></div>'+
			'</div></div></div>') ,
			$return = $this.data('s2option_name'),			
			new_option_name ,
			new_option_val,
			new_option_description,
			new_option_rowid,
			new_option_price;
			
        $('body').append($modal);
        $modal.modal();
        $modal.find('.modal-body').load($remote, function(){
			$($this.data('select')).select2('container').select2('close');
			$('#invcol').attr('class', 'col-sm-12');
			$('.modal-body .btn').remove();
			$('#save-modal').on('click', function(){ 
				$.post($remote, $('form[action="'+$remote+'"]').serialize()+'&save=', function(data){
					
					console.log(data);
				
					if(typeof $(data).filter('.alert').html() == 'undefined') { // if no error message
						new_option_name = $('#'+$return).val();
						new_option_val = data;
						
						new_option_description =  $('#'+$this.data('s2option_desc')).val();
						new_option_price=$('#'+$this.data('s2option_price')).val();
						new_option_rowid=$('#'+$this.data('s2option_rowid')).val();
			
			
						
						} else {
							$modal.find('.modal-body').html(data);
							$('.modal-body .btn').remove();
							}
				if(new_option_name && new_option_val)
				{
					
					
			 		$('#ajaxModal').modal('hide');
					
					
					
				if ( val2 =="inventory") {
					
					console.log(new_option_val,new_option_description,new_option_price,new_option_name);
					//select2onchange($this.data('s2option_rowid'));
					$('.product').each(function() {
    
			 		$(this).append('<option value="'+new_option_val+'" desc="'+new_option_description+'" price="'+new_option_price+'">'+new_option_name+'</option>');
					});
			 		$($this.data('select')).select2('val', new_option_val);
					
					
					$("#itemname"+$this.data('s2option_rowid')).val(new_option_name);
					
					$("#description"+$this.data('s2option_rowid')).val(new_option_description);
					$("#price"+$this.data('s2option_rowid')).val(new_option_price);
					calcamount($this.data('s2option_rowid'));
					
					
					
					}
					
						if ( val2 =="client") {
					//select2onchange($this.data('clients_client_name'));
					select2onchange($this.data('s2option_rowid'));
					$('.client').each(function() {
    
			 		$(this).append('<option value="'+new_option_val+'" desc="'+new_option_description+'" price="'+new_option_price+'">'+new_option_name+'</option>');
					});
					$($this.data('select')).select2('val', new_option_val);
					}
					
				//	alert("asdfasdf")
				/*alert($this.data('select')).select2('val', new_option_val))*/
					
					 
				}
						}, 'html'); // end post
						
			});
		});
		
		//$modal.find('.modal-body').children().eq(2).hide();
					//$('[name=save]').parent().hide();
	}

	
jQuery(document).on('click', '[data-toggle="ajaxModal3"]',
      function(e) {
	

$('#ajaxModal').remove();
        e.preventDefault();
		
        var $this = $(this)
        var $remote = $this.data('remote') || $this.attr('href') , 
			$modal = $('<div class="modal fade" id="ajaxModal" style="width:auto;height:auto;max-height:100%"><div class="modal-dialog"  style="width:70%"><div class="modal-content">'+
			'<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">&nbsp;</h4></div>'+
			'<div class="modal-body"><i class="fa fa-spin fa-spinner fa-3x"></i></div>'+
			'<div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Close</a><a href="#" class="btn btn-primary" id="save-modal">Save</a></div>'+
			'</div></div></div>');
					
			
			 $('body').append($modal);
        $modal.modal();
        $modal.find('.modal-body').load($remote, function(){
			//$('#tnc_type').find('option[text="Quote"]').val();
			$('[name=tnc_type]').val("Quote");
			$('#tnc_type').trigger('change');
			$("#tnc_type option:contains('Invoice')").attr("disabled","disabled");
			$('.modal-body .rembut').remove();
			$('#save-modal').on('click', function(){
				  $("#tnc_content").val($("#editor").html());

				
				
				$('#ajaxModal').modal('hide');
						
		$('#tncmodal').html($('#editor').html());
		$('#tncmodal2').val($('#editor').html());
						
			});
			
				
			
			
	

		});
		
		
		
	}
);
	jQuery(document).on('click', '[data-toggle="ajaxModal1"]',
      function(e) {
	

$('#ajaxModal').remove();
        e.preventDefault();
		
        var $this = $(this)
		
		
        var $remote = $this.data('remote') || $this.attr('href') , 
			$modal = $('<div class="modal fade"  id="ajaxModal"><div class="modal-dialog2">'+
			
			'<div class="modal-body" style="padding:0;margin-top:100px;"><i class="fa fa-spin fa-spinner fa-3x" style="color:#fff"></i><span style="color:#fff">...Loading</span></div>'+
			'</div></div>');
			
				
					
			
			// $('body').append($modal);
        $modal.modal();
        $modal.find('.modal-body').load($remote, function(){
		
		
				
		
			});
			
			

		});
 
 $(document).ready(function(){
$("#filesList").change(function() {
	
$("#fileuploader").trigger("click");
$("#ul-sample").html("");
	
});
});

	</script> 
    
  
