<?php



$validation_errors = validation_errors();

$errorClass = ' error';

$controlClass = 'span6';

$fieldData = array(

    'errorClass'    => $errorClass,

    'controlClass'  => $controlClass,

);


							$currency = isset($geoplugin->countryCode) && $geoplugin->countryCode == 'MY' ? 'RM' : 'USD';
?>

<p align="left" style="margin:20px"><br/><a href="<?=site_url('packages')?>">&larr; <?php echo lang('us_back_to') . 'pricing page'; ?></a></p>

<section id="content" class="m-t-lg wrapper-md animated fadeInUp">    


 <div class="container aside-xxl"><div align="center"><img src="<?=base_url('/images/logo.png')?>" width="23" align="baseline" >  <strong style="font-size:20px;color:#999;">&nbsp;SPP
</strong></div>

   <section class="panel panel-default bg-white m-t-lg">
<header class="panel-heading text-center">

         <strong><?php echo $this->auth->is_logged_in() ? 'Upgrade Account' : lang('us_sign_up'); ?></strong>

        </header>
        

	



	<?php  echo Template::message(); ?>



	<?php

		if (validation_errors()) :

	?>

	
<!--
		
			<div class="alert alert-danger fade in">

			  <a data-dismiss="alert" class="close">&times;</a>

				<?php echo validation_errors(); ?>

			</div>

		
-->
	

	<?php endif; ?>

<?
			$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
		
			?>
           

	<?php echo form_open($actual_link, array('autocomplete' => 'off' , 'class' => 'panel-body wrapper-lg')); ?>

	<div class="form-group <?php echo iif( form_error('period') , 'error') ;?>" id="form-package-period" <?=$selected_package_id == 1 ? 'style="display:none"': ''?>>

			<label class="control-label" for="period">Period</label>

				<div class="controls">
					<div class="btn-group btn-group-justified m-b" data-toggle="buttons">
		                  <label class="btn btn-sm btn-info <?=$this->session->userdata('checkout_package_period') ? ($this->session->userdata('checkout_package_period')==1 ? 'active' : '') : 'active'?>">
		                    <input type="radio" name="period" value=1><i class="fa fa-check text-active"></i> Monthly
		                  </label>
		                  <label class="btn btn-sm btn-success <?=$this->session->userdata('checkout_package_period')==12 ? 'active' : ''?>">
		                    <input type="radio" name="period" value=12><i class="fa fa-check text-active"></i> Yearly
		                  </label>
						  <!--
		                  <label class="btn btn-sm btn-primary">
		                    <input type="radio" name="period" value=0><i class="fa fa-check text-active"></i> One off
		                  </label>
						  -->
						  <input name="package_period" type="hidden" value=<?=$this->session->userdata('checkout_package_period') ? $this->session->userdata('checkout_package_period') : 1?>>
		                </div>
				</div>
			 <span class="help-inline"><?php echo form_error('period'); ?></span>

		</div>

		 <div class="form-group <?php echo iif( form_error('package_id') , 'error') ;?>">

			<label class="control-label" for="package">Package</label>

				<div class="controls">
					<select class="form-control product selecta" name="package_id">

					<?php foreach($packages as $package):?>

						<?php if($this->auth->is_logged_in() === false || ($this->auth->is_logged_in() && $package->id != 1)):?>

							<?php if($package->id == 1):?>

						<option value="<?=$package->id?>" <?=$selected_package_id == $package->id ? 'selected' : ''?>><?=$package->package_name?> (<?=$currency?> <?=number_format($this->session->userdata('checkout_package_period') ? $package->price*$this->session->userdata('checkout_package_period')*$discount[$this->session->userdata('checkout_package_period')] : $package->price,2)?> / <?=$package->license_duration?> <?=strtolower($package->duration_metric)?>)</option>

							<?php else:?>
						<?php
							$package_period = $this->session->userdata('checkout_package_period') ? $this->session->userdata('checkout_package_period') : 1;
							$price_per_user = number_format(($currency == 'RM' ? $package->price_in_myr : $package->price ) * $discount[$package_period] * $package_period, 2);
							$total_price = number_format($price_per_user * $package->no_of_users, 2);
						?>
						<option value="<?=$package->id?>" <?=$selected_package_id == $package->id ? 'selected' : ''?>><?=$package->package_name?> (<?=$currency?> <?=$price_per_user?> / <?=$package_period==12 ? 'year' : 'month'?>) X <?=$package->no_of_users?> = <?=$currency?> <?=$total_price?></option>
							
							<?php endif;?>

						<?php endif;?>

					<?php endforeach;?>

					</select>
				</div>
			 <span class="help-inline"><?php echo form_error('package_id'); ?></span>

		</div>

<?php if($this->auth->is_logged_in() === false):?>

		 <div class="form-group <?php echo iif( form_error('company_name') , 'error') ;?>">

			<label class="control-label" for="company_name">Company Name</label>

				<input  type="text" id="company_name" name="company_name" value="<?php echo set_value('company_name', $this->session->userdata('checkout_company_name') ? $this->session->userdata('checkout_company_name') : ''); ?>" tabindex="1" placeholder="<?php echo "Company Name" ?>" class="form-control input-lg" />

			 <span class="help-inline alert-danger"><?php echo form_error('company_name'); ?></span>

		</div>



		 <div class="form-group <?php echo iif( form_error('email') , 'error') ;?>">

			 <label class="control-label" for="email"><?php echo lang('bf_email'); ?></label>

				<input type="text" id="email" name="email" value="<?php echo set_value('email', $this->session->userdata('checkout_email') ? $this->session->userdata('checkout_email') : ''); ?>" tabindex="2" placeholder="<?php echo lang('bf_email'); ?>"  class="form-control input-lg">

			 <span class="help-inline alert-danger"><?php echo form_error('email'); ?></span>

		</div>
		
		<?php foreach ($meta_fields as $field):?>

			<?php  if($field['form_detail']['type'] == 'country_select' && is_callable('country_select')) : ?>
				<div class="form-group <?php echo iif( form_error('country') , 'error'); ?>">
					<label class="control-label" for="country"><?php echo lang('user_meta_country'); ?></label>
					<div class="controls">
						<?php echo country_select(set_value($field['name'], isset($geoplugin->countryCode) ? $geoplugin->countryCode : 'MY'), 'MY', 'country', 'form-control product selecta'); ?>
					</div>
				</div>
			<?php endif;?>

		<?php endforeach; ?>

<?php endif;?>

	
 <?php

                // Allow modules to render custom fields

               Events::trigger('render_user_form');
  ?>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<div class="form-group">

                    <div class="controls">

                        <!--<input class="btn btn-primary" type="submit" name="register" id="submit" value="<?php echo lang('us_register'); ?>"  />-->
						<input class="btn btn-primary" type="submit" name="register" value="Subscribe"  /> <!-- Change this value and you need to change in users controller as well -->
						<!--<input class="btn btn-success" type="submit" name="register" value="One time off Payment"  />-->
                    </div>

                </div>
    <?php if($this->auth->is_logged_in() === false):?>
        <p class='already-registered'>

                <?php echo lang('us_already_registered'); ?>

                <?php echo anchor(LOGIN_URL, lang('bf_action_login')); ?>

            </p>
    <?php endif;?>
	<?php echo form_close(); ?>



	

 </section>



</div>

 </section>
 <script>
 var currency = '<?=$currency?>',
		draw_package_options = function(value){

			var packages = JSON.parse('<?=json_encode($packages)?>'),
				package_option_html = '',
				$package_option = $("select[name=package_id]"),
				selected_package = $package_option.select2('val'),
				is_logged_in = <?=$this->auth->is_logged_in() ? 1 : 0?>,
				discount = JSON.parse('<?=json_encode($discount)?>');
	
			$('input:hidden[name=package_period]').val(value);

			for(i in packages)
			{
				if(!is_logged_in || (is_logged_in && packages[i].id != 1))
					if(packages[i].id == 1)
						package_option_html += '<option value='+packages[i].id+'>'+packages[i].package_name+' ('+currency+' '+ (parseFloat(packages[i].price*discount[1]).toFixed(2))+' / '+packages[i].license_duration+' '+packages[i].duration_metric.toLowerCase()+')' +'</option>';
					else {
						price_per_user = parseFloat((currency == 'RM' ? packages[i].price_in_myr : packages[i].price) * value * discount[value]).toFixed(2);
						total_price = parseFloat(price_per_user * packages[i].no_of_users).toFixed(2);
						package_option_html += '<option value='+packages[i].id+'>'+packages[i].package_name+' ('+currency+' '+ price_per_user +' / '+ (value==12 ? 'year' : 'month') +') X '+packages[i].no_of_users+' = '+currency+' '+total_price+'</option>';
					}
			}
	
			$package_option.html(package_option_html).select2().select2('val',selected_package);

		};

 $("input:radio[name=period]").closest('label').on("click",function(e) {

    var value = $(this).find('input:radio[name=period]').val();

    draw_package_options(value);

});

 $(document).ready(function(){

 	$('select[name=package_id]').on('select2-selecting',function(e){

 		$('#form-package-period').show();
 		if(e.val == 1) {
 			$('#form-package-period').hide();
 			$("input:radio[name=period][value="+e.val+"]").closest('label').click();
 			draw_package_options(e.val);
 			$("select[name=package_id]").select2('val',e.val);
 		}
 	});

 	$('select[name=country]').on('select2-selecting',function(e){

 		currency = 'USD';
 		if(e.val == 'MY')
 			currency = 'RM';

 		draw_package_options($('input:hidden[name=package_period]').val());

 	});

 });
 </script>