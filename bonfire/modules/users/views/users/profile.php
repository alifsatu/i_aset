<section class="hbox stretch">
  <aside class="aside-lg bg-light lter b-r">
    <section class="vbox">
      <section class="scrollable">
        <div class="wrapper">
           <section class="panel no-border bg-primary lt" style="background-color:rgb(29, 137, 207)">
            <div class="panel-body">
              <div class="row m-t-xl">
                <div class="col-xs-3 text-right padder-v" >  </div>
                <div class="col-xs-6 text-center">
                 <div class="inline" style="margin-left:-150px; margin-right:-148px">
                    <div class="easypiechart easyPieChart" data-percent="75" data-line-width="6" data-bar-color="#fff" data-track-color="#2796de" data-scale-color="false" data-size="140" data-line-cap="butt" data-animate="1000" style="width: 140px; height: 132px; line-height: 140px;">
                     <? 
	  // $this->load->view('../../profileimage/views/settings/index'); 
	    $this->load->view('profileimages'); 
	
	/*if ( @getimagesize(Template::theme_url('images/userimg/'.$current_user->id.'.jpg'))) { ?>
    <img id="profimg" src="<?php echo Template::theme_url('images/userimg/'.$current_user->id.'.jpg') ?>">
    <? } else { ?> <img src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"><? } */
	
	
	?>
                      <canvas width="140" height="140"></canvas>
                    </div>
                
                 </div>
                  <div class="h4 m-t m-b-xs font-bold"><?php echo $current_user->display_name ?></div>
                 <big class="text-muted m-b font-bold"><?php echo $this->auth->company_name_by_id($current_user->company_id) ?></big>
                  
                </div>
                <div class="col-xs-3 padder-v"> </div>
              </div>
              <div class="wrapper m-t-xl m-b">
                <div class="row m-b">
                  <div class="col-xs-6 text-right"> </small>
                    <div class="text font-bold"></div>
                  </div>
                  <div class="col-xs-6"> 
                    <div class="text font-bold"></div>
                  </div>
                </div>
                
              </div>
            </div>
            <footer class="panel-footer dk text-center no-border" style="background-color:rgb(29, 137, 207)">
              <!--<div class="row pull-out">
                <div class="col-xs-4" >
                  <div class="padder-v"> <span class="m-b-xs h3 block text-white" >245</span> <small class="text-muted">Followers</small> </div>
                </div>
                <div class="col-xs-4 dker" style="background-color:rgb(29, 137, 207)">
                  <div class="padder-v"> <span class="m-b-xs h3 block text-white">55</span> <small class="text-muted">Following</small> </div>
                </div>
                <div class="col-xs-4">
                  <div class="padder-v"> <span class="m-b-xs h3 block text-white">2,035</span> <small class="text-muted">Tweets</small> </div>
                </div>
              </div>-->
            </footer>
          </section>
        </div>
      </section>
    </section>
  </aside>
<!--  <aside class="col-lg-4 b-l no-padder">
    <section class="vbox">
      <section class="scrollable">
        <div class="wrapper">
          <section class="panel panel-default">
            <form>
              <textarea class="form-control no-border" rows="3" placeholder="What are you doing..."></textarea>
            </form>
            <footer class="panel-footer bg-light lter">
              <button class="btn btn-info pull-right btn-sm">POST</button>
              <ul class="nav nav-pills nav-sm">
                <li><a href="#"><i class="fa fa-camera text-muted"></i></a></li>
                <li><a href="#"><i class="fa fa-video-camera text-muted"></i></a></li>
              </ul>
            </footer>
          </section>
          <section class="panel panel-default">
            <h4 class="padder">Latest Tweets</h4>
            <ul class="list-group">
              <li class="list-group-item">
                <p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>
                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago</small> </li>
              <li class="list-group-item">
                <p>Morbi nec <a href="#" class="text-info">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>
                <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 hour ago</small> </li>
              <li class="list-group-item">
                <p><a href="#" class="text-info">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. </p>
                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 hours ago</small> </li>
            </ul>
          </section>
          <section class="panel clearfix bg-info dk">
            <div class="panel-body"> <a href="#" class="thumb pull-left m-r"> <img src="images/a0.png" class="img-circle b-a b-3x b-white"> </a>
              <div class="clear"> <a href="#" class="text-info">@Mike Mcalidek <i class="fa fa-twitter"></i></a> <small class="block text-muted">2,415 followers / 225 tweets</small> <a href="#" class="btn btn-xs btn-info m-t-xs">Follow</a> </div>
            </div>
          </section>
        </div>
      </section>
    </section>
  </aside>-->
</section>
