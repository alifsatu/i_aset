<link rel="stylesheet" type="text/css" href="<? echo Template::theme_url('assets/css/main.css')?>"/>
<link rel="stylesheet" type="text/css" href="<? echo Template::theme_url('assets/css/croppic.css')?>"/>
<?php

$num_columns	= 5;
$can_delete	= $this->auth->has_permission('profile.Settings.Delete');
$can_edit		= $this->auth->has_permission('profile.Settings.Edit');
$has_records	= isset($records) && is_array($records) && count($records);

?>
	
	

			
				
		<div class="thumb-lg avatar" style="background-image: url(<?=base_url()?>themes/notebook/images/userimg/<?=$current_user->profileimage?>); height:135px; z-index:-1"> 
				<!--<div id="cropContaineroutput"  ></div>-->
				<div id="cropContainerModal"></div>
			</div>
			
			
	
        
        
	

<!--<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script> 
<script src="<? echo Template::theme_url('js/bootstrap.js')?>"></script>--> 
<script src="<? echo Template::theme_url('croppic.min.js')?>"></script>
<script src="<? echo Template::theme_url('assets/js/main.js')?>"></script>

  <script>
		var croppicHeaderOptions = {
				uploadUrl:'<?=base_url()?>index.php/users/img_save_to_file',
				cropData:{
					"dummyData":1,
					"dummyData2":"asdas"
				},
				cropUrl:'<?=base_url()?>index.php/users/img_crop_to_file',
				customUploadButtonId:'cropContainerHeaderButton',
				modal:false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') }
		}	
		var croppic = new Croppic('croppic', croppicHeaderOptions);
		
		
		var croppicContainerModalOptions = {
				uploadUrl:'<?=base_url()?>index.php/users/img_save_to_file',
				cropUrl:'<?=base_url()?>index.php/users/img_crop_to_file',
				modal:true,
				imgEyecandyOpacity:0.4,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
		}
		var cropContainerModal = new Croppic('cropContainerModal', croppicContainerModalOptions);
		
		
		var croppicContaineroutputOptions = {
				uploadUrl:'<?=base_url()?>index.php/users/img_save_to_file',
				cropUrl:'<?=base_url()?>index.php/users/img_crop_to_file', 
				outputUrlId:'cropOutput',
				modal:false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
		}
		var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);
		
		
		
	</script>
