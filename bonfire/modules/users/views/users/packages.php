    <header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light"  data-spy="affix" data-offset-top="1">
    <div class="container">
      <div class="navbar-header">        
        <a href="#" class="navbar-brand"><img src="<?=base_url('images/logo.png')?>" class="m-r-sm"><span class="text-muted">SPP</span></a>
        <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li>
            <a href="<?=base_url('..')?>">Home</a>
          </li>
          <li>
            <a href="<?=base_url('../features.html')?>">Features</a>
          </li>
           <li>
            <a href="<?=base_url('../modules.html')?>">Modules</a>
          </li>
          <li class="active">
            <a href="<?=site_url('packages')?>">Plans & Pricing</a>
          </li>
          <li>
            <a href="<?=base_url('/blog')?>">Blog</a>
          </li>
          <li>
            <div class="m-t-sm">
            <?php if($this->auth->is_logged_in() === false):?>
              <a href="<?=site_url('login')?>" class="btn btn-link btn-sm">Sign in</a> 
              <a href="<?=site_url('register/1')?>" class="btn btn-sm btn-success m-l"><strong>Sign up</strong></a>
            <?php else:?>
              <a href="<?php echo site_url('logout'); ?>" class="btn btn-sm btn-success m-l"><?php echo lang('bf_action_logout')?></a>
            <?php endif;?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </header>
  <!-- / header -->
	<section id="content" >
	  <div class="bg-dark lt">
      <div class="container">
        <div class="m-b-lg m-t-lg">
          <h3 class="m-b-none">Plans & Pricing</h3>
          <small class="text-muted">Choose the plan that best fits your needs</small>
        </div>
      </div>
    </div>
    <div class="bg-white b-b b-light">
      <div class="container">      
        <ul class="breadcrumb no-border bg-empty m-b-none m-l-n-sm">
          <li><a href="<?=base_url('..')?>">Home</a></li>
          <li class="active">Price</li>
        </ul>
      </div>
    </div>
    <div class="container">
      <div class="m-t-xl m-b-xl text-center">
        <h2>Find the Right Pricing Plan For You</h2>
      </div>

    <?php if($this->auth->is_logged_in()):?>
      <div class="alert alert-success fade in">

        <a data-dismiss="alert" class="close">&times;</a>

        <h4 align="center">!!!<br> We believe you have enjoyed our system. Please select the package to Purchase license.</h4>

      </div>
    <?php endif;?>

	   <div class="clearfix">
        <div class="row m-b-xl">
		<?php foreach($packages as $i => $package):?>
    <?php if($this->auth->is_logged_in() === false || ($this->auth->is_logged_in() && $package->id != 1)):?>
    <?php 
      $fadein_class = 'fadeInUp';
      if(!$i)
        $fadein_class = 'fadeInLeft';
      else if($i == count($packages)-1)
        $fadein_class = 'fadeInRight';
    ?>
			<div class="col-sm-4 animated <?=$fadein_class?>">
				<?php if(isset($package->featured) && $package->featured):?>
				<section class="panel b-primary text-center">
					<header class="panel-heading bg-primary">
				<?php else:?>
				<section class="panel b-light text-center m-t">
					<header class="panel-heading bg-white b-light">
				<?php endif;?>
						<h3 class="m-t-sm"><?= $package->package_name?></h3>
						<p><?=$package->description?></p>
					</header>
					<ul class="list-group">
						<li class="list-group-item text-center bg-light lt">
							<div class="padder-v">
								<span class="text-danger font-bold h1"><?=isset($geoplugin->countryCode) && $geoplugin->countryCode == 'MY' ? 'RM '.$package->price_in_myr : 'USD '.$package->price?></span> / month/ user <br/> (<? if($package->package_name == "Free Trial - 15 Days") { echo "max"; } else { echo "min"; }?> <?=$package->no_of_users?> Users) 
							</div>
						</li>
						<?php 
							$modules = explode(',',$package->modules);
							foreach($modules as $module):
						?>
						<li class="list-group-item">
							<?php
								switch(strtolower($module)) {
									case 'content':
										echo 'Inventory';
										break;
									case 'ideabank':
										echo 'Socialize';
										break;
                  case 'invoices':
                    echo 'Invoices & Payments';
                    break;
                  case 'client':
                    echo 'Clients';
                    break;
                  case 'projectmgmt';
                    echo 'Project Management';
                    break;
                  case 'rostermanagment';
                    echo 'Roster Management';
                    break;
					case 'docmgnt';
                    echo 'Document Management';
                    break;
									default:
										echo $module;
								}
							?>
						</li>
						<?php endforeach;?>
					</ul>
					<footer class="panel-footer">
						<a href="<?php echo REGISTER_URL .'/'. $package->id; ?>" class="btn btn-primary m-t m-b">Get Started</a>
					</footer>
				</section>
			</div>
    <?php endif;?>
		<?php endforeach;?>
        </div>
      </div>      
    </div>
    <div class="bg-white-only">
      <div class="container">
        <div class="m-t-xl m-b-xl">
          <h2 class="font-thin">Why many companies have chosen us</h2>
        </div>
        <div class="row m-b-xl">
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            <span class="fa-stack fa-2x pull-left m-r m-t-xs">
              <i class="fa fa-circle fa-stack-2x text-info"></i>
              <i class="fa fa-stack-1x fa-inverse">1</i>
            </span>
            <div class="clear">
              <h4>Simplified with Lightweight & Rich Modules</h4>
              <p class="text-muted text-sm">We have only the lightweight features, but with rich functions. SPP's main passion is simplicity. You will find features that you really need, and none of those which you don't want. Making the application simple & blazing fast!</p>
            </div>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="600">
            <span class="fa-stack fa-2x pull-left m-r m-t-xs">
              <i class="fa fa-circle fa-stack-2x text-info"></i>
              <i class="fa fa-stack-1x fa-inverse">2</i>
            </span>
            <div class="clear">
              <h4>Free Upgrades forever</h4>
              <p class="text-muted text-sm">You will be always updated with our free updates with new & cool features.</p>
            </div>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="900">
            <span class="fa-stack fa-2x pull-left m-r m-t-xs">
              <i class="fa fa-circle fa-stack-2x text-info"></i>
              <i class="fa fa-stack-1x fa-inverse">3</i>
            </span>
            <div class="clear">
              <h4>Mobile Ready</h4>
              <p class="text-muted text-sm">SPP will work no matter what device you use. Use it anywhere, any time on any device</p>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
  
  <!-- footer -->
  <footer id="footer">
    <div class="bg-primary text-center">
      <div class="container wrapper">
        <div class="m-t-xl m-b">
          For your faster and easier operations.
        
        </div>
      </div>
      <i class="fa fa-caret-down fa-4x text-primary m-b-n-lg block"></i>
    </div>
    <div class="bg-dark dker wrapper">
      <div class="container text-center m-t-lg">
        <div class="row m-t-xl m-b-xl">
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInLeft" data-delay="300">
            <i class="fa fa-map-marker fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">SUPPORT EMAIL</h5>
            <p class="text-sm"><a href="mailto:support@satuwater.com.my">support@satuwater.com.my</a>
               
            
             </p>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInUp" data-delay="600">
            <i class="fa fa-envelope-o fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">Mail Us</h5>
            <p class="text-sm"><a href="mailto:info@satuwater.com.my">info@satuwater.com.my</a></p>
          </div>
          <div class="col-sm-4" data-ride="animated" data-animation="fadeInRight" data-delay="900">
            <i class="fa fa-globe fa-3x icon-muted"></i>
            <h5 class="text-uc m-b m-t-lg">Join Us</h5>
            <p class="text-sm">Send your resume to <br><a href="mailto:recruit@satuwater.com.my">recruit@satuwater.com.my</a></p>
          </div>
        </div>
        <div class="m-t-xl m-b-xl">
          <p>
            <a href="http://www.facebook.com/pages/INTEGRIO/125745161741" class="btn btn-icon btn-rounded btn-facebook bg-empty m-sm"><i class="fa fa-facebook"></i></a>
            <a href="#" class="btn btn-icon btn-rounded btn-twitter bg-empty m-sm"><i class="fa fa-twitter"></i></a>
            <a href="#" class="btn btn-icon btn-rounded btn-gplus bg-empty m-sm"><i class="fa fa-google-plus"></i></a>
          </p>
          <p>
            <a href="#content" data-jump="true" class="btn btn-icon btn-rounded btn-dark b-dark bg-empty m-sm text-muted"><i class="fa fa-angle-up"></i></a>
          </p>
        </div>
      </div>
    </div>
  </footer>
