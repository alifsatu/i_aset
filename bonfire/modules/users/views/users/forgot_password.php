<div class="container bg-white aside-xxl">
<div class="page-header">
	<h1><?php echo lang('us_reset_password'); ?></h1>
</div>

<?php if (validation_errors()) : ?>
	<div class="alert alert-error fade in">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>

<div class="alert alert-info fade in">
	<?php echo lang('us_reset_note'); ?>
</div>


	
<?php echo form_open($this->uri->uri_string(), array('class' => "panel-body wrapper-lg", 'autocomplete' => 'off')); ?>
<?php //echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>

	<div class="form-group <?php echo iif( form_error('email') , 'error'); ?>">
		<label class="control-label required" for="email"><?php echo lang('bf_email'); ?></label>
		<div class="controls">
			<input class="form-control input" type="text" name="email" id="email" value="<?php echo set_value('email') ?>" />
		</div>
	</div>

	<div class="form-group">
		<div class="controls">
			<input class="btn btn-primary" type="submit" name="send" value="<?php e(lang('us_reset_password')); ?>" />
		</div>
	</div>

<?php echo form_close(); ?>

	

</div>
</section>
