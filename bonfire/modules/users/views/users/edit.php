<?php
$validation_errors = validation_errors();
$errorClass = ' error';
$controlClass = 'span6';
$fieldData = array(
    'errorClass' => $errorClass,
    'controlClass' => $controlClass,
);
?>
<div class="span12"> <?php echo form_open($this->uri->uri_string(), array('class' => '', 'autocomplete' => 'off')); ?>
    <div class="row">
        <div class="col-sm-8">
            <section class="panel panel-default">
                <header class="panel-heading font-bold clearfix"> <h3 class="m-b-none"><span style="float:left; margin-right:10px"><?php echo lang('us_edit_profile'); ?></span></h3></header>

                <div class="panel-body">	



                    <?php if ($validation_errors) : ?>
                        <div class="alert alert-error"> <?php echo $validation_errors; ?> </div>
                    <?php endif; ?>
                    <?php if (isset($user) && $user->role_name == 'Banned') : ?>
                        <div data-dismiss="alert" class="alert alert-error"> <?php echo lang('us_banned_admin_note'); ?> </div>
                    <?php endif; ?>
                    <div class="alert alert-info">
                        <h4 class="alert-heading"><?php echo lang('bf_required_note'); ?></h4>
                        <?php
                        if (isset($password_hints)) {
                            echo $password_hints;
                        }
                        ?>
                    </div>


                    <?php Template::block('user_fields', 'user_fields', $fieldData); ?>
                    <?php
                    if ($this->auth->role_id() == 7) {
                        ?>

                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php
                        // echo $this->auth->role_id();
                        $this->load->model('companies/companies_model', null, true);
                        $company = $this->companies_model->find($this->auth->company_id());
                        // echo "array".$company;
                        // $company = $this->db->query("select * from intg_companies where id = '$this->auth->company_id()'")->result();
                        //print_r($company);
                        if (isset($company)) {
                            $company = (array) $company;
                        }
//print_r($company);
                        $id = isset($company['id']) ? $company['id'] : '0';
                        ?>
                      <!-- <h3 class="m-b-none"><span style="float:left; margin-right:10px">Company Details</span></h3>-->
                        <div class="col-lg-6">
                            <h3>Company Details</h3>
                            <div class="control-group <?php echo form_error('company_address') ? 'error' : ''; ?>">
                                <?php echo form_label('Address' . lang('bf_form_label_required'), 'company_add', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <?php echo form_textarea(array('name' => 'company_add', 'class' => 'expand form-control parsley-validated', 'id' => 'company_add', 'rows' => '1', 'cols' => '60', 'value' => set_value('company_add', isset($company['address']) ? $company['address'] : ''))); ?>
                                    <span class='help-inline'><?php echo form_error('client_address'); ?></span>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;	
                            <div class="control-group <?php echo form_error('company_phone_number') ? 'error' : ''; ?>">
                                <?php echo form_label('Phone Number' . lang('bf_form_label_required'), 'company_phone_number', array('class' => 'control-label')); ?>
                                <div class='controls'> 
                                    <input id='company_phone_number' class="input-sm input-s  form-control" type='text' name='company_phone_number' maxlength="45" value="<?php echo set_value('company_phone_number', isset($company['phone_no']) ? $company['phone_no'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('client_phone_number'); ?></span>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="control-group <?php echo form_error('company_fax_number') ? 'error' : ''; ?>">
                                <?php echo form_label('Fax Number', 'company_fax_number', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='company_fax_number'  class="input-sm input-s  form-control" type='text' name='company_fax_number' maxlength="45" value="<?php echo set_value('company_fax_number', isset($company['fax']) ? $company['fax'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('company_fax_number'); ?></span>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="control-group <?php echo form_error('company_web_address') ? 'error' : ''; ?>">
                                <?php echo form_label('Web Site', 'company_web_address', array('class' => 'control-label')); ?>
                                <div class='controls'>
                                    <input id='company_web_address' type='text' class="input-sm input-s  form-control" name='company_web_address'  maxlength="45" value="<?php echo set_value('company_web_address', isset($company['web_address']) ? $company['web_address'] : ''); ?>" />
                                    <span class='help-inline'><?php echo form_error('client_web_address'); ?></span>
                                </div>
                            </div>	
                            &nbsp;&nbsp;&nbsp;&nbsp;	
                            <?php
                        }
                        ?>
                        <?php
                        // Allow modules to render custom fields
                        Events::trigger('render_user_form', $user);
                        ?>
                        <!-- Start User Meta -->
                        <?php $this->load->view('users/user_meta', array('frontend_only' => true));
                        ?>
                        <!-- End of User Meta -->
                        <br /><br />

                    </div>
                    <?php
                    if ($this->auth->role_id() == 7) {
                        $image = $this->db->query("select cmpimage from intg_companies where id = " . $this->auth->company_id() . "")->row();
                        ?>
                        <div class="col-lg-6" align="center"><h4 style="margin:10px 5px">Attach Company Logo</h4> <a href="<?php echo base_url(); ?>index.php/admin/settings/fileupload/company_profile?cmp=1" class="btn btn-rounded btn-sm btn-icon btn-success left"  data-toggle="ajaxModal"><i class="fa fa-camera"></i></a><input type="hidden" name="filename" id="filename" /> <div id="baba" align="center" style="margin-top:10px"><img src="<?= base_url() ?>files/CID<?= $this->auth->company_id() ?>/thumbnail/<?= $image->cmpimage ?>" alt="" data-item="media"></div></div><?php } ?>

                    <div style="clear:both"></div>
                    <div class="form-actions">

                        <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('bf_action_save') . ' ' . lang('bf_user'); ?>" />
                        <?php echo ' ' . anchor('/users/profile', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?> </div>

                </div>


            </section>
        </div>
    </div>
    <?php echo form_close(); ?>

