<div class="container bg-white aside-xxl">
<div class="page-header">
	<h1>Reset Your Password</h1>
</div>

<?php if (validation_errors()) : ?>
	<div class="alert alert-error fade in">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>

<div class="alert alert-info fade in">
	<?php echo lang('us_reset_password_note'); ?>
</div>


	
<?php echo form_open($this->uri->uri_string(), array('class' => "panel-body wrapper-lg", 'autocomplete' => 'off')); ?>
<?php //echo form_open($this->uri->uri_string(), 'id="forma" role="form" data-validate="parsley"'); ?>


<input type="hidden" name="user_id" value="<?php echo $user->id ?>" />

	<div class="form-group <?php echo iif( form_error('password') , 'error') ;?>">
		<label class="control-label" for="password"><?php echo lang('bf_password'); ?></label>
		<div class="controls">
			<input class="form-control input" type="password" name="password" id="password" value="" placeholder="Password...." />
			<p class="help-block"><?php echo lang('us_password_mins'); ?></p>
		</div>
	</div>

	<div class="form-group <?php echo iif( form_error('pass_confirm') , 'error') ;?>">
		<label class="control-label" for="pass_confirm"><?php echo lang('bf_password_confirm'); ?></label>
		<div class="controls">
			<input class="form-control input" type="password" name="pass_confirm" id="pass_confirm" value="" placeholder="<?php echo lang('bf_password_confirm'); ?>" />
		</div>
	</div>

	

	<div class="form-group">
		<div class="controls">
			<input class="btn btn-primary" type="submit" name="set_password" id="submit" value="<?php e(lang('us_set_password')); ?>"  />
		</div>
	</div>
	
	

<?php echo form_close(); ?>

	

</div>
</section>



