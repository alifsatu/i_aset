<?php
/* /bonfire/modules/users/views/user_fields.php */

$currentMethod = $this->router->fetch_method();

$errorClass = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span4' : $controlClass;
$registerClass = $currentMethod == 'register' ? ' required' : '';
$editSettings = $currentMethod == 'edit';

$defaultLanguage = isset($user->language) ? $user->language : strtolower(settings_item('language'));
$defaultTimezone = isset($current_user) ? $current_user->timezone : strtoupper(settings_item('site.default_user_timezone'));


//echo "Value for Session = " . $this->session->userdata('company_id');
if ($this->session->userdata('company_id') > 0) {
    $comp_id = $this->session->userdata('company_id');
    $comp_query = $this->db->query('SELECT name from intg_companies where id =' . $comp_id);
    foreach ($comp_query->result() as $row) {
        $company_name = $row->name;
    }
    ?>
    <div class="form-group <?php echo iif(form_error('email'), $errorClass); ?>">
        <label class="control-label required" for="email">Company Name : </label><font size="3"><b>&nbsp;<?= $company_name ?></b></font>
    </div>
    <input type="hidden" name="company_id" value="<?= $comp_id ?>" />
    <?php
} else if ($this->session->userdata('company_id') == 0) {
    $query = $this->db->query("SELECT id,name FROM intg_companies WHERE deleted = 0 order by name");
    $company_options['0'] = "INTERNAL (Own Company)";
    foreach ($query->result() as $row) {
        $company_options[$row->id] = $row->name;
        //echo $row->role_name;
    }
    echo form_dropdown('company_id', $company_options, set_value('company_id', isset($user) ? $user->company_id : ''), 'Company Name' . lang('bf_form_label_required'), "class='selecta form-control'");
}
?>
<br>
<br>
<div class="form-group <?php echo iif(form_error('email'), $errorClass); ?>">
    <label class="control-label" for="email"><?php echo lang('bf_email'); ?></label>
    <div class="controls" style="width:50%">
        <input class="form-control" type="text" id="email" name="email" value="<?php echo set_value('email', isset($user) ? $user->email : ''); ?>" />
        <span class="help-inline"><?php echo form_error('email'); ?></span>
    </div>
    <?php echo form_error('email'); ?>
</div>

<div class="form-group <?php echo iif(form_error('display_name'), $errorClass); ?>">
    <label class="control-label" for="display_name"><?php echo lang('bf_display_name'); ?></label>
    <div class="controls" style="width:50%">
        <input class="form-control" type="text" id="display_name" name="display_name" value="<?php echo set_value('display_name', isset($user) ? $user->display_name : ''); ?>" />
        <span class="help-inline"><?php echo form_error('display_name'); ?></span>
    </div>
</div>

<?php if (settings_item('auth.login_type') !== 'email' OR settings_item('auth.use_usernames')) { ?>
    <div class="form-group <?php echo iif(form_error('username'), $errorClass); ?>">
        <label class="control-label required" for="username"><?php echo lang('bf_username'); ?>*</label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="text" id="username" name="username" value="<?php echo set_value('username', isset($user) ? $user->username : ''); ?>" />
            <span class="help-inline"><?php echo form_error('username'); ?></span>
        </div>
    </div>
    <!--<input type="hidden" id="username" name="username" value="<?php echo set_value('username', isset($user) ? $user->username : ''); ?>" />-->
<?php } ?>

<?php if (isset($user)) { ?>

    <div class="form-group <?php echo iif(form_error('password'), $errorClass); ?>">
        <label class="control-label<?php echo $registerClass; ?>" for="password"><?php echo lang('bf_password'); ?> (8 Characters Above)</label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="password" id="password" name="password" value="" />
            <span class="help-inline"><?php echo form_error('password'); ?></span>
            <p class="help-block"><?php
                if (isset($password_hints)) {
                    echo $password_hints;
                }
                ?></p>
        </div>
    </div>
    <div class="form-group <?php echo iif(form_error('pass_confirm'), $errorClass); ?>">
        <label class="control-label<?php echo $registerClass; ?>" for="pass_confirm"><?php echo lang('bf_password_confirm'); ?></label>
        <div class="controls" style="width:50%">
            <input class="form-control" type="password" id="pass_confirm" name="pass_confirm" value="" />
            <span class="help-inline"><?php echo form_error('pass_confirm'); ?></span>
        </div>
    </div>
    <?php
} else {
    $email = "user@satuwater.com.my";
    ?>
    <input type="hidden" id="password" name="password" value="<?php echo substr(md5($email . time()), 0, 8); ?>" />
    <input type="hidden" id="pass_confirm" name="pass_confirm" value="<?php echo substr(md5($email . time()), 0, 8); ?>" />
    <?php
}


$prgm = $this->db->query('SELECT * FROM  intg_daftar_daerah WHERE deleted = 0 ORDER BY nama ')->result();
?>
<div class="form-group <?php echo form_error('sbu_name') ? 'error' : ''; ?>">
    <?php echo form_label('Daerah', 'daerah', array('class' => 'control-label')); ?>
    <div class='controls'>
        <select name="daerah" id="daerah" class="selecta form-control"  style="height:30px;width:200px; font-size:12px" >
            <option value="0" >Sila Pilih</option>
            <?php foreach ($prgm as $pn) { ?>


                <option value=<?= $pn->id ?> <?php
                if (isset($user->daerah) && $user->daerah == $pn->id) {
                    echo "Selected";
                }
                ?>><b><?= $pn->nama ?></b></option><?php } ?>
        </select>
        <span class='help-inline'><?php echo form_error('daerah'); ?></span>
    </div>
</div>

<br>
<br>


<?php
if (isset($languages) && is_array($languages) && count($languages)) :
    if (count($languages) == 1):
        ?>
        <input type="hidden" id="language" name="language" value="<?php echo $languages[0]; ?>" />
        <?php
    else :
        ?>
        <div class="form-group <?php echo iif(form_error('language'), $errorClass); ?>">
            <label class="control-label required" for="language"><?php echo lang('bf_language'); ?></label>
            <div class="controls" style="width:50%">
                <select name="language" id="language" class="chzn-select selecta form-control">
                    <?php foreach ($languages as $language) : ?>
                        <option value="<?php e($language); ?>" <?php echo set_select('language', $language, $defaultLanguage == $language ? true : false); ?>>
                            <?php e(ucfirst($language)); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <span class="help-inline"><?php echo form_error('language'); ?></span>
            </div>
        </div>
    <?php
    endif;
endif;
?>
<div class="form-group <?php echo iif(form_error('timezone'), $errorClass); ?>">
    <label class="control-label required" for="timezones"><?php echo lang('bf_timezone'); ?></label>
    <div class="controls" style="width:70%">
        <?php echo timezone_menu(set_value('timezones', isset($user) ? $user->timezone : $defaultTimezone), 'selecta form-control'); ?>
        <span class="help-inline"><?php echo form_error('timezones'); ?></span>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
    $('document').ready(function () {

        $("#join_date,#leave_date,#dob").datepicker({format: "dd/mm/yyyy"});
    });

</script>

