<?php
$errorClass = ' error';
$controlClass = 'span6';
$fieldData = array(
    'errorClass' => $errorClass,
    'controlClass' => $controlClass,
);

if (isset($user) && $user->banned) {
    ?>
    <div class="alert alert-warning fade in">
        <h4 class="alert-heading"><?php echo lang('us_banned_admin_note'); ?></h4>
    </div>
    <?php
}
if (isset($password_hints)) {
    ?>
    <div class="alert alert-info fade in">
        <a data-dismiss="alert" class="close">&times;</a>
        <?php echo $password_hints; ?>
    </div>
    <?php
}
if ($user_full == TRUE) {
    ?> 
    <section class="chat-body">                      
        <div class="panel bg bg-success text-sm m-b-none">
            <div class="panel-body">
                <span class="fa arrow right"></span>
                <h4 align="center"><?= $message ?>!!!<br> To add more users upgrade your Package.</h4>
            </div>
        </div>
    </section>
    <?php
}
if ($user_full == FALSE) {
    echo form_open($this->uri->uri_string(), array('class' => '', 'autocomplete' => 'off'));
    ?>
    <fieldset>
        <div class="row">
            <div class="col-md-12">
                <section class="panel panel-default">
                    <header class="panel-heading font-bold clearfix"> <h3 class="m-b-none"><span style="float:left; margin-right:10px"><?php echo lang('us_account_details'); ?></span></h3></header>

                    <div class="panel-body">
               <!--<legend><?php echo lang('us_account_details') ?></legend>-->
                        <div class="col-md-6">
                            <div class="row">
                                <?php Template::block('user_fields', 'user_fields', $fieldData); ?>
                            </div>
                        </div>

                        <!--</fieldset>-->


                        <div class="col-md-6">
                            <?php
                            /* if (has_permission('Bonfire.Roles.Manage')
                              && ( ! isset($user) || (isset($user) && has_permission('Permissions.' . $user->role_name . '.Manage')))
                              ) : */
                            if ($this->auth->role_id() == 7 || $this->auth->role_id() == 1) {
                                ?>
                                <!--  <fieldset> -->




                                <legend><?php echo lang('us_role'); ?></legend>
                                <div class="form-group">
                                    <label for="role_id" class="control-label"><?php echo lang('us_role'); ?></label>
                                    <div class="controls">
                                        <select name="role_id" id="role_id" class="chzn-select selecta form-control <?php echo $controlClass; ?>">
                                            <?php
                                            if (isset($roles) && is_array($roles) && count($roles)) {
                                                //if ($this->auth->role_id()==7 || $this->auth->role_id()==1) :
                                                foreach ($roles as $role) {
                                                    //if (has_permission('Permissions.' . ucfirst($role->role_name) . '.Manage')) :
                                                    // check if it should be the default
                                                    $default_role = false;
                                                    if ((isset($user) && $user->role_id == $role->role_id) || (!isset($user) && $role->default == 1)) {
                                                        $default_role = true;
                                                    }
                                                    ?>
                                                    <option value="<?php echo $role->role_id; ?>" <?php echo set_select('role_id', $role->role_id, $default_role); ?>>
                                                        <?php e(ucfirst($role->role_name)); ?>
                                                    </option>
                                                    <?php
                                                    // endif;
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- </fieldset>-->

                                <?php
                            }
                            // Allow modules to render custom fields
                            Events::trigger('render_user_form');
                            ?>
                            <!-- Start of User Meta -->
                            <?php $this->load->view('users/user_meta'); ?>
                            <!-- End of User Meta -->


                        </div>

                        <?php
                        if (isset($user) && has_permission('Permissions.' . ucfirst($user->role_name) . '.Manage') && $user->id != $this->auth->user_id() && ($user->banned || $user->deleted)) {
                            ?>
                            <!-- <fieldset>-->
                            <legend><?php echo lang('us_account_status'); ?></legend>
                            <?php
                            $field = 'activate';
                            if ($user->active) {
                                $field = 'de' . $field;
                            }
                            ?>
                            <div class="control-group">
                                <div class="controls">
                                    <label for="<?php echo $field; ?>">
                                        <input type="checkbox" name="<?php echo $field; ?>" id="<?php echo $field; ?>" value="1" />
                                        <?php echo lang('us_' . $field . '_note') ?>
                                    </label>
                                </div>
                            </div>
                            <?php if ($user->deleted) { ?>
                                <div class="control-group">
                                    <div class="controls">
                                        <label for="restore">
                                            <input type="checkbox" name="restore" id="restore" value="1" />
                                            <?php echo lang('us_restore_note'); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php } elseif ($user->banned) { ?>
                                <div class="control-group">
                                    <div class="controls">
                                        <label for="unban">
                                            <input type="checkbox" name="unban" id="unban" value="1" />
                                            <?php echo lang('us_unban_note'); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php } ?>
                            </fieldset>
                        <?php } ?>
                        <div class="form-actions">
                            <input type="submit" name="save" class="btn btn-primary" value="<?php echo lang('bf_action_save') . ' ' . lang('bf_user'); ?>" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <?php echo anchor(SITE_AREA . '/settings/users', lang('bf_action_cancel'), 'class="btn btn-warning"'); ?>
                        </div>
                        <?php
                        echo form_close();
                    }
                    ?>
                </div>


            </section>
        </div>
    </div>
</div>
</div>
