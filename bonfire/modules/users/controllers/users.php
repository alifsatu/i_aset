<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**

 * Bonfire

 *

 * An open source project to allow developers get a jumpstart their development of CodeIgniter applications

 *

 * @package   Bonfire

 * @author    Bonfire Dev Team

 * @copyright Copyright (c) 2011 - 2013, Bonfire Dev Team

 * @license   http://guides.cibonfire.com/license.html

 * @link      http://cibonfire.com

 * @since     Version 1.0

 * @filesource

 */



// ------------------------------------------------------------------------



/**

 * Users Controller

 *

 * Provides front-end functions for users, like login and logout.

 *

 * @package    Bonfire

 * @subpackage Modules_Users

 * @category   Controllers

 * @author     Bonfire Dev Team

 * @link       http://cibonfire.com

 *

 */

class Users extends Front_Controller

{

	public $discount = array(1 => 1, 12 => 0.95);

	//--------------------------------------------------------------------



	/**

	 * Setup the required libraries etc

	 *

	 * @retun void

	 */

	public function __construct()

	{

		parent::__construct();



		$this->load->helper('form');

		$this->load->library('form_validation');



		$this->load->model('users/user_model');



		$this->load->library('users/auth');



		$this->lang->load('users');



	}//end __construct()



	//--------------------------------------------------------------------



	/**

	 * Presents the login function and allows the user to actually login.

	 *

	 * @access public

	 *

	 * @return void

	 */

	public function login()

	{

		// if the user is not logged in continue to show the login page

		if ($this->auth->is_logged_in() === FALSE)

		{

			if (isset($_POST['log-me-in']))

			{

				$remember = $this->input->post('remember_me') == '1' ? TRUE : FALSE;



				// Try to login

				if ($this->auth->login($this->input->post('login'), $this->input->post('password'), $remember) === TRUE)

				{

					$this->session->unset_userdata('checkout_email');

					// Log the Activity

					log_activity($this->auth->user_id(), lang('us_log_logged') . ': ' . $this->input->ip_address(), 'users');



					// Now redirect.  (If this ever changes to render something,

					// note that auth->login() currently doesn't attempt to fix

					// `$this->current_user` for the current page load).



					/*

						In many cases, we will have set a destination for a

						particular user-role to redirect to. This is helpful for

						cases where we are presenting different information to different

						roles that might cause the base destination to be not available.

					*/


					if ($this->settings_lib->item('auth.do_login_redirect') && !empty ($this->auth->login_destination))

					{

						Template::redirect($this->auth->login_destination);

					}

					else

					{

						if (!empty($this->requested_page))

						{

							Template::redirect($this->requested_page);

						}

						else

						{

							Template::redirect('/');

						}

					}

				}//end if

			}//end if



			Template::set_view('users/users/login');

			Template::set('page_title', 'Login');

			Template::render('login');

		}

		else

		{


Template::redirect('/');
			//Template::redirect('/admin/settings/main');

		}//end if



	}//end login()



	//--------------------------------------------------------------------



	/**

	 * Calls the auth->logout method to destroy the session and cleanup,

	 * then redirects to the home page.

	 *

	 * @access public

	 *

	 * @return void

	 */

	public function logout()

	{

		if (isset($this->current_user->id))

		{

			// Login session is valid.  Log the Activity

			log_activity($this->current_user->id, lang('us_log_logged_out') . ': ' . $this->input->ip_address(), 'users');

		}



		// Always clear browser data (don't silently ignore user requests :).

		$this->auth->logout();



		redirect('/');



	}//end  logout()



	//--------------------------------------------------------------------



	/**

	 * Allows a user to start the process of resetting their password.

	 * An email is allowed with a special temporary link that is only valid

	 * for 24 hours. This link takes them to reset_password().

	 *

	 * @access public

	 *

	 * @return void

	 */

	public function forgot_password()

	{



		// if the user is not logged in continue to show the login page

		if ($this->auth->is_logged_in() === FALSE)

		{

			if (isset($_POST['send']))

			{

				$this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email');



				if ($this->form_validation->run() !== FALSE)

				{

					// We validated. Does the user actually exist?

					$user = $this->user_model->find_by('email', $_POST['email']);



					if ($user !== FALSE)

					{

						// User exists, so create a temp password.

						$this->load->helpers(array('string', 'security'));



						$pass_code = random_string('alnum', 40);



						$hash = do_hash($pass_code . $_POST['email']);



						// Save the hash to the db so we can confirm it later.

						$this->user_model->update_where('email', $_POST['email'], array('reset_hash' => $hash, 'reset_by' => strtotime("+24 hours") ));



						// Create the link to reset the password

						$pass_link = site_url('reset_password/'. str_replace('@', ':', $_POST['email']) .'/'. $hash);



						// Now send the email

						$this->load->library('emailer/emailer');



						$data = array(

									'to'	=> $_POST['email'],

									'subject'	=> lang('us_reset_pass_subject'),

									'message'	=> $this->load->view('_emails/forgot_password', array('link' => $pass_link), TRUE)

							 );


						$this->emailer->addAddress($_POST['email'],"");
						$this->emailer->Subject = lang('us_reset_pass_subject');
						$this->emailer->Body = $this->load->view('_emails/forgot_password', array('link' => $pass_link), TRUE);
						if ($this->emailer->mail()) 

						{

							Template::set_message(lang('us_reset_pass_message'), 'success');

						}

						else

						{

							Template::set_message(lang('us_reset_pass_error'). $this->emailer->error, 'error');

						}

					}

					else

					{

						Template::set_message(lang('us_invalid_email'), 'error');

					}//end if

				}//end if

			}//end if



			Template::set_view('users/users/forgot_password');

			Template::set('page_title', 'Password Reset');

			Template::render();

		}

		else

		{



			Template::redirect('/');

		}//end if



	}//end forgot_password()



	//--------------------------------------------------------------------



	/**

	 * Allows a user to edit their own profile information.

	 *

	 * @access public

	 *

	 * @return void

	 */


 public function profileimages() { 
	 
	 
	//$this->load->view('../../profileimage/views/settings/index'); 
	 Template::render();
	 }
	 
  public function companyimage() {	 Template::render();	 }
  
  	 
public function img_save_to_file() { Template::render(); }
public function cmpimg_save_to_file() { Template::render(); }

public function img_crop_to_file() {
	Template::set_view('users/users/img_crop_to_file');
	 Template::render(); }
	 
 public function cmpimg_crop_to_file() {
	Template::set_view('users/users/cmpimg_crop_to_file');
	 Template::render(); }	
	  
	public function profile()

	{
		Template::set_block('sub_nav', 'users/users/_sub_nav');
		 $this->load->library('ui/contexts');

Template::set_theme('notebook');
		// Make sure we're logged in.

		$this->auth->restrict();

		$this->set_current_user();



		$this->load->helper('date');



		$this->load->config('address');

		$this->load->helper('address');



		$this->load->config('user_meta');

		$meta_fields = config_item('user_meta_fields');



		Template::set('meta_fields', $meta_fields);



		if (isset($_POST['save']))

		{



			$user_id = $this->current_user->id;

			if ($this->save_user('update',$user_id, $meta_fields, $user->role_name))

			{



				$meta_data = array();

				foreach ($meta_fields as $field)

				{

					if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE

						|| (isset($field['admin_only']) && $field['admin_only'] === TRUE

							&& isset($this->current_user) && $this->current_user->role_id == 1))

						&& (!isset($field['frontend']) || $field['frontend'] === TRUE)

						&& $this->input->post($field['name']) !== FALSE)

					{

						$meta_data[$field['name']] = $this->input->post($field['name']);

					}

				}



				// now add the meta is there is meta data

				$this->user_model->save_meta_for($user_id, $meta_data);



				// Log the Activity



				$user = $this->user_model->find($user_id);

				$log_name = (isset($user->display_name) && !empty($user->display_name)) ? $user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $user->username : $user->email);

				log_activity($this->current_user->id, lang('us_log_edit_profile') . ': ' . $log_name, 'users');



				Template::set_message(lang('us_profile_updated_success'), 'success');



				// redirect to make sure any language changes are picked up

				Template::redirect('/users/profile');

			}

			else

			{

				Template::set_message(lang('us_profile_updated_error'), 'error');

			}//end if

		}//end if



		// get the current user information

		$user = $this->user_model->find_user_and_meta($this->current_user->id);



        $settings = $this->settings_lib->find_all();

        if ($settings['auth.password_show_labels'] == 1) {

            Assets::add_module_js('users','password_strength.js');

            Assets::add_module_js('users','jquery.strength.js');

            Assets::add_js($this->load->view('users_js', array('settings'=>$settings), true), 'inline');

        }

        // Generate password hint messages.

		$this->user_model->password_hints();
		
		// associated social acc status in profile
		$this->load->model('users_social_login/users_social_login_model', null, true);
		$social_login_data = $this->users_social_login_model->find_all_by('user_id',$this->auth->user_id());
		$social_login = array();
		if($social_login_data)
			foreach($social_login_data as $sld)
			{
				$social_login[$sld->social_provider] = $sld->social_uid;
			}
		Template::set('social_login', $social_login);

		Template::set('user', $user);

		Template::set('languages', unserialize($this->settings_lib->item('site.languages')));



		Template::set_view('users/users/profile');
    Template::set('toolbar_title', 'View Profile');
	
	
		Template::render();



	}//end profile()


public function edit()

	{
		Template::set_block('sub_nav', 'users/users/_sub_nav');
		 $this->load->library('ui/contexts');

Template::set_theme('notebook');
		// Make sure we're logged in.

		$this->auth->restrict();

		$this->set_current_user();



		$this->load->helper('date');



		$this->load->config('address');

		$this->load->helper('address');



		$this->load->config('user_meta');

		$meta_fields = config_item('user_meta_fields');



		Template::set('meta_fields', $meta_fields);



		if (isset($_POST['save']))

		{
			
			


$this->db->query("UPDATE intg_companies SET address='".$this->input->post('company_add')."',phone_no='".$this->input->post('company_phone_number')."',fax='".$this->input->post('company_fax_number')."',web_address='".$this->input->post('company_web_address')."' WHERE id = ".$this->auth->company_id()."");

			$user_id = $this->current_user->id;

			if ($this->save_user('update',$user_id, $meta_fields, $user->role_name))

			{



				$meta_data = array();

				foreach ($meta_fields as $field)

				{

					if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE

						|| (isset($field['admin_only']) && $field['admin_only'] === TRUE

							&& isset($this->current_user) && $this->current_user->role_id == 1))

						&& (!isset($field['frontend']) || $field['frontend'] === TRUE)

						&& $this->input->post($field['name']) !== FALSE)

					{

						$meta_data[$field['name']] = $this->input->post($field['name']);

					}

				}



				// now add the meta is there is meta data

				$this->user_model->save_meta_for($user_id, $meta_data);



				// Log the Activity



				$user = $this->user_model->find($user_id);

				$log_name = (isset($user->display_name) && !empty($user->display_name)) ? $user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $user->username : $user->email);

				log_activity($this->current_user->id, lang('us_log_edit_profile') . ': ' . $log_name, 'users');



				Template::set_message(lang('us_profile_updated_success'), 'success');



				// redirect to make sure any language changes are picked up

				//Template::redirect('/users/profile');

			}

			else

			{

				Template::set_message(lang('us_profile_updated_error'), 'error');

			}//end if

		}//end if



		// get the current user information

		$user = $this->user_model->find_user_and_meta($this->current_user->id);



        $settings = $this->settings_lib->find_all();

        if ($settings['auth.password_show_labels'] == 1) {

            Assets::add_module_js('users','password_strength.js');

            Assets::add_module_js('users','jquery.strength.js');

            Assets::add_js($this->load->view('users_js', array('settings'=>$settings), true), 'inline');

        }

        // Generate password hint messages.

		$this->user_model->password_hints();
		
		// associated social acc status in profile
		$this->load->model('users_social_login/users_social_login_model', null, true);
		$social_login_data = $this->users_social_login_model->find_all_by('user_id',$this->auth->user_id());
		$social_login = array();
		if($social_login_data)
			foreach($social_login_data as $sld)
			{
				$social_login[$sld->social_provider] = $sld->social_uid;
			}
		Template::set('social_login', $social_login);

		Template::set('user', $user);

		Template::set('languages', unserialize($this->settings_lib->item('site.languages')));



		Template::set_view('users/users/edit');
    Template::set('toolbar_title', 'Manage Profile');
	Assets::add_css(Template::theme_url('js/datepicker2/bootstrap-datepicker.css'));
		Assets::add_js(Template::theme_url("js/datepicker2/bootstrap-datepicker.js"));
		Template::render();



	}//end profile()

	//--------------------------------------------------------------------



	/**

	 * Allows the user to create a new password for their account. At the moment,

	 * the only way to get here is to go through the forgot_password() process,

	 * which creates a unique code that is only valid for 24 hours.

	 *

	 * Since 0.7 this method is also gotten to here by the force_password_reset

	 * security features.

	 *

	 * @access public

	 *

	 * @param string $email The email address to check against.

	 * @param string $code  A randomly generated alphanumeric code. (Generated by forgot_password() ).

	 *

	 * @return void

	 */

	public function reset_password($email='', $code='')

	{

		// if the user is not logged in continue to show the login page

		if ($this->auth->is_logged_in() === FALSE)

		{

			// If we're set here via Bonfire and not an email link

			// then we might have the email and code in the session.

			if (empty($code) && $this->session->userdata('pass_check'))

			{

				$code = $this->session->userdata('pass_check');

			}



			if (empty($email) && $this->session->userdata('email'))

			{

				$email = $this->session->userdata('email');

			}



			// If there is no code, then it's not a valid request.

			if (empty($code) || empty($email))

			{

				Template::set_message(lang('us_reset_invalid_email'), 'error');

				Template::redirect(LOGIN_URL);

			}



			// Handle the form

			if (isset($_POST['set_password']))

			{

				$this->form_validation->set_rules('password', 'lang:bf_password', 'required|max_length[120]|valid_password');

				$this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', 'required|matches[password]');



				if ($this->form_validation->run() !== FALSE)

				{

					// The user model will create the password hash for us.

					$data = array('password' => $this->input->post('password'),

					              'reset_by'	=> 0,

					              'reset_hash'	=> '',

					              'force_password_reset' => 0);



					if ($this->user_model->update($this->input->post('user_id'), $data))

					{

						// Log the Activity

						log_activity($this->input->post('user_id'), lang('us_log_reset') , 'users');



						Template::set_message(lang('us_reset_password_success'), 'success');

						Template::redirect(LOGIN_URL);

					}

					else

					{

						Template::set_message(sprintf(lang('us_reset_password_error'), $this->user_model->error), 'error');



					}

				}

			}//end if



			// Check the code against the database

			$email = str_replace(':', '@', $email);

			$user = $this->user_model->find_by(array(

                                        'email' => $email,

										'reset_hash' => $code,

										'reset_by >=' => time()

                                   ));



			// It will be an Object if a single result was returned.

			if (!is_object($user))

			{

				Template::set_message( lang('us_reset_invalid_email'), 'error');

				Template::redirect(LOGIN_URL);

			}



            $settings = $this->settings_lib->find_all();

            if ($settings['auth.password_show_labels'] == 1) {

                Assets::add_module_js('users','password_strength.js');

                Assets::add_module_js('users','jquery.strength.js');

                Assets::add_js($this->load->view('users_js', array('settings'=>$settings), true), 'inline');

            }

            // If we're here, then it is a valid request....

			Template::set('user', $user);



			Template::set_view('users/users/reset_password');

			Template::render();

		}

		else

		{



			Template::redirect('/');

		}//end if



	}//end reset_password()



	//--------------------------------------------------------------------



	/**

	 * Display the registration form for the user and manage the registration process

	 *

	 * @access public

	 *

	 * @return void

	 */

	public function register($package_id = 1)

	{
		if (!$this->settings_lib->item('auth.allow_register'))

		{
			Template::set_message(lang('us_register_disabled'), 'error');

			Template::redirect('/');
		}


		$this->load->model('roles/role_model');

		$this->load->helper('date');



		$this->load->config('address');

		$this->load->helper('address');



		$this->load->config('user_meta');

		$meta_fields = config_item('user_meta_fields');

		Template::set('meta_fields', $meta_fields);

		if (isset($_POST['register']))
		{
			// Validate input

			$this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email|max_length[120]|unique[users.email]');
			$this->form_validation->set_rules('company_name', 'lang:us_company_name', 'required|max_length[255]|unique[companies.name]');


			$username_required = '';

			if ($this->settings_lib->item('auth.login_type') == 'username' ||

			    $this->settings_lib->item('auth.use_usernames'))

			{

				$username_required = 'required|';

			}

		//	$this->form_validation->set_rules('username', 'lang:bf_username', $username_required . 'trim|max_length[30]|unique[users.username]');

		//	$this->form_validation->set_rules('password', 'lang:bf_password', 'required|max_length[120]|valid_password');

		//	$this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', 'required|matches[password]');

		//	$this->form_validation->set_rules('language', 'lang:bf_language', 'required|trim');

		//	$this->form_validation->set_rules('timezones', 'lang:bf_timezone', 'required|trim|max_length[4]');

		//	$this->form_validation->set_rules('display_name', 'lang:bf_display_name', 'trim|max_length[255]');

			$this->session->set_userdata(array(
						'checkout_package_period' => $this->input->post('package_period'),
						'checkout_package_id' => $this->input->post('package_id'),
						'checkout_company_name' => $this->input->post('company_name'),
						'checkout_email' => $this->input->post('email'),
						'checkout_country' => $this->input->post('country'),
			));

			if ($this->form_validation->run() !== FALSE)
			{
					
				if($this->input->post('package_id') == 1) {

					$this->confirm_register();

					$this->session->unset_userdata(array(
						'checkout_package_period' => '',
						'checkout_package_id' => '',
						'checkout_company_name' => '',
						'checkout_country' => '',
					));

					Template::set_message('Your account have been created. Please check your email.', 'success');
					Template::redirect(site_url('login'));
				} else {
				//if($this->input->post('register') == 'Subscribe')
					Template::redirect(site_url('users/paypal/subscribe'));
				//Template::redirect(site_url('users/paypal'));
				}
			}//end if

			else if($this->auth->is_logged_in()) {

				$this->load->model('companies/companies_model',null,true);
				$company = $this->companies_model->find($this->auth->company_id());
				
				$user = $this->user_model->find_by(array('company_id' => $company->id, 'users.role_id' => 7));

				$this->session->set_userdata(array(
						'checkout_company_name' => $company->name,
						'checkout_email' => $user->email,
				));

				Template::redirect(site_url('users/paypal/subscribe'));

			}

		}//end if



        $settings = $this->settings_lib->find_all();

        if ($settings['auth.password_show_labels'] == 1) {

            Assets::add_module_js('users','password_strength.js');

            Assets::add_module_js('users','jquery.strength.js');

            Assets::add_js($this->load->view('users_js', array('settings'=>$settings), true), 'inline');

        }

        $this->load->library('geoplugin');
        $this->geoplugin->locate();
        $geoplugin = $this->geoplugin;
		Template::set('geoplugin',$this->geoplugin);

		$this->load->model('currency/currency_model',null,true);
		$currency_model = $this->currency_model;
		Template::set('get_currency', function($currency_id = 1) use($currency_model, $geoplugin){
				$home_currency = 'RM';
				$currency = $currency_model->find($currency_id);
				if($currency)
					$home_currency = $currency->home_currency;
				if(isset($geoplugin->countryCode) && $geoplugin->countryCode != 'MY')
					$home_currency = 'USD';
				return $home_currency;
			});

		Template::set('discount', $this->discount);

        // Generate password hint messages.

		$this->user_model->password_hints();

		if($this->auth->is_logged_in() !== false && $package_id == 1)
			$package_id = 2;

		Template::set('languages', unserialize($this->settings_lib->item('site.languages')));

		$this->load->model('packages/packages_model',null,true);
		Template::set('packages',$this->packages_model->find_all());

		Template::set_view('users/users/register');
		Template::set('selected_package_id', $package_id);

		Template::set('page_title', 'Register');

		Template::render();



	}//end register()

	/*
		Proceed register after payment validated
		
		@return Array message, type
	*/
	private function confirm_register($checkout_data = array())
	{
		
		$package_id = isset($checkout_data['package_id']) ? $checkout_data['package_id'] : $this->session->userdata('checkout_package_id');
		
		$company_name = isset($checkout_data['company_name']) ? $checkout_data['company_name'] : $this->session->userdata('checkout_company_name');
		
		$email = isset($checkout_data['email']) ? $checkout_data['email'] : $this->session->userdata('checkout_email');
		
		$this->load->model('packages/packages_model', null, true);

		$this->load->model('companies/companies_model', null, true);
		
        $package = $this->packages_model->find($package_id);
				
		// Create company
		$company_id = $this->companies_model->insert(array(
			'name'	=> $company_name,
			'no_users'	=> $package->no_of_users,
			'package_id'	=> $package_id,
		));
				
		//if($company_id) {
		
			// create license
			$this->load->model('license/license_model',null,true);
			$license_id = $this->license_model->skip_validation(true)->insert(array(
							'company_id' => $company_id,
							'package_id' => $package_id,
							'package_name' => $package->package_name,
							'no_of_users' => $package->no_of_users,
							'modules' => $package->modules,
							'price' => $package->price,
							'start_date' => date('Y-m-d'),
							'end_date' => date('Y-m-d', strtotime('+'.$package->license_duration.' '.$package->duration_metric)),
							'payment_date' => date('Y-m-d'),
							'amount_paid' => 0.00,
							'payment_mode' => 'null',
							'transaction_id' => 'null',
							'transaction_details' => 'null',
							'active' => 'no',
							'deleted' => 0,
						));

	

			
			// create Default socialize category
			$this->load->model('idea_categories/idea_categories_model',null,true);
			$this->idea_categories_model->skip_validation(true)->insert(array(

				'category_id'	=> "Default",
				'company_id'	=> $company_id,
			));
				  
			// Assign modules
            $modules = str_replace(',','|',$package->modules);
			
			//$modules .= '|'.'Users'.'|'.'Roles'.'|'.'Site.Settings.View'.'|'.'Bonfire.Permissions.Manage'.'|'.'Permissions.Manager.Manage'.'|'.'Permissions.HOD.Manage'.'|'.'Permissions.Employee.Manage'.'|'.'Permissions.CEO.Manage'.'|'.'Dashboard'.'|'.'Work_Flow'.'|'.'Fileupload.Settings.View|Notifications'; 
			$modules .= '|'.'Users'.'|'.'Roles'.'|'.'Site.Settings.View'.'|'.'Bonfire.Permissions.Manage'.'|'.'Permissions.Manager.Manage'.'|'.'Permissions.HOD.Manage'.'|'.'Permissions.Employee.Manage'.'|'.'Permissions.CEO.Manage'.'|'.'Dashboard'.'|'.'Fileupload.Settings.View|Notifications|companies|cost_centre|non_chargeable_project|programs|sbu|task_pool|user_level|client';
			
			// start Kishore code
			$permission_query = $this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT 7, permission_id, $company_id FROM `intg_permissions` WHERE name RLIKE '$modules'");
			
			$records = $this->db->query("SELECT role_id FROM `intg_roles` WHERE role_name != 'cadmin' and role_name != 'administrator'")->result();
			// sql query in loop is bad
			/* foreach($records as $record)
			{
				$role = $record->role_id;
				$this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT $role, permission_id, $company_id FROM `intg_permissions` WHERE name RLIKE '$modules'");
			}  */// end Kishore code
			 $mod = explode(',',$package->modules);	
			 //print_r($mod);
			// exit;
			foreach($records as $record)
					{
					//$test = $test.'|'.'Dashboard';
					$test = "Dashboard|Notifications|Tnc";
					$role = $record->role_id;
					
					if(in_array("Quotes", $mod))
					{
					
				$test = $test."|Site.Quotes.View|Quote.Quotes.Delete|Quote.Quotes.Edit|Quote.Quotes.Create|Quote.Quotes.Deleted|Quote.Quotes.View|Fileupload.Settings.View";
					
					//echo "In quotes";
				//	echo $test;
				//	exit;
					}
					if(in_array("Invoices", $mod))
					{
					
				$test = $test."|Site.Invoices.View|Invoice.Invoices.Delete|Invoice.Invoices.Edit|Invoice.Invoices.Create|Invoice.Invoices.Deleted|Invoice.Invoices.View|Invoice_for_you.Invoices.Delete|Invoice_for_you.Invoices.Deleted|Invoice_for_you.Invoices.View|Recurring_Invoice.Invoices.Delete|Recurring_Invoice.Invoices.Edit|Recurring_Invoice.Invoices.Restore_purge|Recurring_Invoice.Invoices.Create|Recurring_Invoice.Invoices.Deleted|Recurring_Invoice.Invoices.View";
				
				
					}
					if(in_array("Client", $mod))
					{
					
					$test = $test."|Site.Client.View|Clients.Client.View|Clients.Client.Deleted|Clients.Client.Create|Clients.Client.Edit|Clients.Client.Delete";
					
				
					}
					if(in_array("Content", $mod))
					{
					
					$test = $test."|Site.Content.View|Inventory_Type.Content.View|Inventory_Type.Content.Deleted|Inventory_Type.Content.Create|Inventory_Type.Content.Edit|Inventory_Type.Content.Delete|Inventory.Content.View|Inventory.Content.Deleted|Inventory.Content.Create|Inventory.Content.Edit|Inventory.Content.Delete";
					
				
					}
					if(in_array("Projectmgmt", $mod))
					{
					
				$test = $test."|Site.Projectmgmt.View|Projects.Projectmgmt.View|Projects.Projectmgmt.Deleted|Projects.Projectmgmt.Create|Projects.Projectmgmt.Edit|Projects.Projectmgmt.Delete|Tasks.Projectmgmt.View|Tasks.Projectmgmt.Deleted|Tasks.Projectmgmt.Create|Tasks.Projectmgmt.Edit|Tasks.Projectmgmt.Delete";
				
					}
					
					if(in_array("Rostermanagment", $mod))
					{
					
				$test = $test."|Site.RosterManagment.View|RM_Task_Category.Rostermanagment.Create|RM_Task_Category.Rostermanagment.Deleted|RM_Task_Category.Rostermanagment.View|RM__Project.Rostermanagment.Edit|RM__Project.Rostermanagment.Create|RM__Project.Rostermanagment.Deleted|RM__Project.Rostermanagment.View|RM__Project.Rostermanagment.Delete|RM_Tasks.Rostermanagment.View|RM_Tasks.Rostermanagment.Deleted|RM_Tasks.Rostermanagment.Create|RM_Tasks.Rostermanagment.Edit|RM_Tasks.Rostermanagment.Delete|RM_Task_Category.Rostermanagment.Delete|RM_Task_Category.Rostermanagment.Edit";
				
					}
					if(in_array("Ideabank", $mod))
					{
					
				$test = $test."|Site.Ideabank.View|Ideas.Ideabank.View|Ideas.Ideabank.Create|Idea_Categories.Ideabank.View|Idea_Categories.Ideabank.Deleted|Idea_Categories.Ideabank.Create|Idea_Categories.Ideabank.Edit";
				
					}
					if(in_array("Docmgnt", $mod))
					{
					
				$test = $test."|Site.Docmgnt.View|Files.Docmgnt.View|Files.Docmgnt.Create|Files.Docmgnt.Delete|File_Group_Permission.Docmgnt.Delete|File_Group_Permission.Docmgnt.Create|File_Group_Permission.Docmgnt.View"; 
				
					}
					
				//	$test = $test.'|'.'Dashboard';
					$this->db->query("INSERT INTO intg_role_permissions_client (role_id, permission_id, company_id) SELECT $role, permission_id, $company_id FROM `intg_permissions` WHERE name RLIKE '$test'");
					}
		//}
				
		// User activation method
		$activation_method = $this->settings_lib->item('auth.user_activation_method');

		// No activation method

		if ($activation_method == 0)
		{
			// Activate the user automatically

			$data['active'] = 1;
		}

		$user_data = array(
				'email'	=> $email,
				'password'	=> substr(md5($email . time()), 0, 8),
				'company_id'   => $company_id,
				'display_name'	=> $email,
				'username'	=> $email,
				'active' => 1,
		);

		if ($user_id = $this->user_model->insert($user_data))
		{
			// now add the meta is there is meta data
			$this->load->config('user_meta');

			$meta_fields = config_item('user_meta_fields');
			$meta_data = array();

			foreach ($meta_fields as $field)

			{

				if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE

					|| (isset($field['admin_only']) && $field['admin_only'] === TRUE

						&& isset($this->current_user) && $this->current_user->role_id == 1))

					&& (!isset($field['frontend']) || $field['frontend'] === TRUE))

				{

					$this->form_validation->set_rules($field['name'], $field['label'], $field['rules']);

					$meta_data[$field['name']] = $this->input->post($field['name']);

				}

			}
			$this->user_model->save_meta_for($user_id, $meta_data);

			/*

				* USER ACTIVATIONS ENHANCEMENT

			 */

			// Prepare user messaging vars

			$subject = '';

			$email_mess = '';

			$message = lang('us_email_thank_you');

			$type = 'success';

			$site_title = $this->settings_lib->item('site.title');

			$error = false;

			switch ($activation_method)
			{

						case 0:

							// No activation required. Activate the user and send confirmation email

							$subject 		=  str_replace('[SITE_TITLE]',$this->settings_lib->item('site.title'),lang('us_account_reg_complete'));

							$email_mess 	= $this->load->view('_emails/activated', array('title'=>$site_title,'link' => site_url(), 'username' => $user_data['username'], 'password' => $user_data['password']), true);

							$message 		.= lang('us_account_active_login');

							break;

						case 1:

							// Email Activiation.

							// Create the link to activate membership

							// Run the account deactivate to assure everything is set correctly

							$activation_code = $this->user_model->deactivate($user_id);

							$activate_link   = site_url('activate/'. $user_id);

							$subject         =  lang('us_email_subj_activate');



							$email_message_data = array(

								'title' => $site_title,

								'code'  => $activation_code,

								'link'  => $activate_link

							);

							$email_mess = $this->load->view('_emails/activate', $email_message_data, true);

							$message   .= lang('us_check_activate_email');

							break;

						case 2:

							// Admin Activation

							// Clear hash but leave user inactive

							$subject    =  lang('us_email_subj_pending');

							$email_mess = $this->load->view('_emails/pending', array('title'=>$site_title), true);

							$message   .= lang('us_admin_approval_pending');

							break;

			}//end switch

			// Now send the email
				/*	$this->load->library('emailer/emailer');

					$this->emailer->addAddress($email);
					$this->emailer->Subject = $subject;
					$this->emailer->Body = $email_mess;
					$this->emailer->mail(); */
					
										 $url = base_url().'index.php/login'; 
 //echo $url;
			// Now send the email
					$this->load->library('emailer/emailer');


$this->emailer->AddAddress("".$email."",$company_name);
$this->emailer->Subject = "Thanks for registering at SPP";
$this->emailer->Body  = '<div marginwidth="0" marginheight="0">



<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
<td width="100%" valign="top" bgcolor="#f8f8f8">

<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1;margin-top:50px">
  <tbody><tr>
    <td width="460">

      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td height="30">
          </td>
        </tr>
      </tbody></table>

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="40"></td>
          <td width="512" style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">
            
          </td>
          <td width="40"></td>
        </tr>
        <tr>
          <td width="40"></td>
           <td width="460" style="font-size:16px;color:#b8b9c1;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">
            <span style="text-decoration:none;color:#2f2f36;font-weight:bold;font-size:20px;line-height:32px">Hi '.$company_name.'<br>Thanks for registering at SPP!</span><br>
          </td>
          <td width="40"></td>
        </tr>
        </tbody></table>

    </td>
  </tr>
</tbody></table>



<table width="460" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody><tr>
    <td width="209">

      <table width="209" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="209" height="5" bgcolor="#ffffff"></td>
        </tr>
        <tr>
          <td width="209" height="1" bgcolor="#e1e1e1"></td>
        </tr>
        <tr>
          <td width="209" height="10" bgcolor="#ffffff"></td>
        </tr>
      </tbody></table>

    </td>
    <td width="42">

      <table width="42" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td style="text-align:center">
            <img src="https://ci5.googleusercontent.com/proxy/KveaD9HdYnB8s3-Zc6C809y_abyUkEw-32xLF1nlhYyRGb1NPYZZqpFAxjIo-ZmMIZKHkmySA1YaOAQxqkefobd2oPnN4M_-HC-mAvCOO2GRBed-FXB5MaqwWTyKl6sLOREe=s0-d-e1-ft#https://wave-vero-assets.s3.amazonaws.com/images/checkmark-reminder-email.jpg" alt="" border="0">
          </td>
        </tr>
      </tbody></table>

    </td>
    <td width="209">

      <table width="209" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="209" height="5" bgcolor="#ffffff"></td>
        </tr>
        <tr>
          <td width="209" height="1" bgcolor="#e1e1e1"></td>
        </tr>
        <tr>
          <td width="209" height="10" bgcolor="#ffffff"></td>
        </tr>
      </tbody></table>

    </td>
  </tr>
</tbody></table>



<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody>
    <tr>
      <td width="460">

        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
               <td style="font-size:16px;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top" height="20"> Below are your login details:<br><br>
            Login ID : '.$email.'<br>
			Password : '.$user_data['password'].'<br>
              </td>
            </tr>
          </tbody>
        </table>

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
              <td width="40"></td>
              <td width="510" style="font-size:14px;color:#444;font-weight:normal;text-align:left;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top"><br><br>
                
               
                 <p style="font-size:18px;border-top:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;padding:10px 0;background:#fefefe;text-align:center;margin:5px 0">
                  
                   Company Name :
                  
                  <span style="white-space:nowrap;font-weight:bold;font-size:18px">'.$company_name.'</span>
                </p>
                <br>
              </td>
              <td width="40"></td>
            </tr>
          </tbody>
        </table>

      </td>
    </tr>
  </tbody>
</table>


<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody>
    <tr>
      <td width="460">

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody>
            <tr>
              <td width="40"></td>
              <td width="510" style="font-size:14px;color:#a0a0a5;font-weight:normal;text-align:center;font-family:Helvetica,Arial,sans-serif;line-height:24px;vertical-align:top">

                <div>
                  
                    
                    <a href="'.$url.'" style="background-color:rgb(76, 192, 193); border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px" target="_blank">
                     Click here to Login
                    </a>
                  

                  

                </div>
                
              </td>
              <td width="40"></td>
            </tr>
            <tr>
              <td width="40"></td>
              <td width="512" height="10"></td>
              <td width="40"></td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>






<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
  <tbody><tr>
    <td width="460">

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td width="460" height="10">
          </td>
        </tr>
      </tbody></table>
      

    </td>
  </tr>
</tbody></table>


      



<table width="462" bgcolor="#f0f0f0" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-top:1px solid #e1e1e1">
  <tbody><tr>
    <td width="462" height="10">
    </td>
  </tr>
</tbody></table>



  
  <table width="460" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1">
    <tbody><tr>
      <td width="460" bgcolor="#f0f0f0">

        
        <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
          <tbody><tr>
            <td width="460">

              
              <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
                <tbody><tr>
                  <td width="30"></td>
                  <td width="530">

                    
                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                      <tbody><tr>
                        <td height="0" style="font-size:14px;font-weight:normal;font-family:Helvetica,Arial,sans-serif;line-height:24px">
                         </td>
                      </tr>
                    </tbody></table>

                    
                    <table width="140" border="0" cellpadding="0" cellspacing="0" align="left">
                      <tbody><tr>
                        <td width="140">
                        </td>
                      </tr>
                    </tbody></table>
                    

                  </td>
                  <td width="30"></td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>
  </tbody></table>
  



<table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
  <tbody><tr>
    <td width="460" style="border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;border-bottom:1px solid #e1e1e1;border-radius:0 0 10px 10px;background:#f0f0f0">

      
      <table width="460" border="0" cellpadding="0" cellspacing="0" align="center">
        <tbody><tr>
          <td height="14">
          </td>
        </tr>
      </tbody></table>
      

    </td>
  </tr>
</tbody></table>




  
  

<img src="https://ci5.googleusercontent.com/proxy/OhkjNm72TKOZl_1_s8UJboFGRdGO8wiwZ4EdX-fLvCRup6LyZOCNW4T4ueT0IU9m3hWVc7OR470OOa2lsB63x9ZsAmcKKODXux0-OFAQWwMVQUKkFufk8V6i9oqmfpaND6PBm0CbejMglkBBKw=s0-d-e1-ft#http://click.waveapps.com/track/open.php?u=11278363&amp;id=78d6045fd07248fca3cb9ac9f4b1e746" height="1" width="1"></div>';

$this->emailer->mail();
					// Log the Activity
					
					log_activity($user_id, lang('us_log_register'), 'users');
					
					return array(
						'message' => $message,
						'type' => $type,
						'license_id' => $license_id,
					);

		}

		return array('message' => lang('us_registration_fail'), 'type' => 'error');
		
	}


	/**

	 * Save the user

	 *

	 * @access private

	 *

	 * @param int   $id          The id of the user in the case of an edit operation

	 * @param array $meta_fields Array of meta fields fur the user

	 *

	 * @return bool

	 */

	private function save_user($id=0, $meta_fields=array())

	{


		if ( $id == 0 )

		{

			$id = $this->current_user->id; /* ( $this->input->post('id') > 0 ) ? $this->input->post('id') :  */

		}



		$_POST['id'] = $id;



		// Simple check to make the posted id is equal to the current user's id, minor security check

		if ( $_POST['id'] != $this->current_user->id )

		{

			$this->form_validation->set_message('email', 'lang:us_invalid_userid');

			return FALSE;

		}



		// Setting the payload for Events system.

		$payload = array ( 'user_id' => $id, 'data' => $this->input->post() );





		$this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email|max_length[120]|unique[users.email,users.id]');

		$this->form_validation->set_rules('password', 'lang:bf_password', 'max_length[120]|valid_password');



		// check if a value has been entered for the password - if so then the pass_confirm is required

		// if you don't set it as "required" the pass_confirm field could be left blank and the form validation would still pass

		//$extra_rules = !empty($_POST['password']) ? 'required|' : '';

		//$this->form_validation->set_rules('pass_confirm', 'lang:bf_password_confirm', ''.$extra_rules.'matches[password]');



		/* $username_required = '';

		if ($this->settings_lib->item('auth.login_type') == 'username' ||

		    $this->settings_lib->item('auth.use_usernames'))

		{

			$username_required = 'required|';

		}

		$this->form_validation->set_rules('username', 'lang:bf_username', $username_required . 'trim|max_length[225]|unique[users.username,users.id]');
 */


		$this->form_validation->set_rules('language', 'lang:bf_language', 'required|trim');

		$this->form_validation->set_rules('timezones', 'lang:bf_timezone', 'required|trim|max_length[4]');

		$this->form_validation->set_rules('display_name', 'lang:bf_display_name', 'trim|max_length[255]');



		// Added Event "before_user_validation" to run before the form validation

		Events::trigger('before_user_validation', $payload );





		foreach ($meta_fields as $field)

		{

			if ((!isset($field['admin_only']) || $field['admin_only'] === FALSE

				|| (isset($field['admin_only']) && $field['admin_only'] === TRUE

					&& isset($this->current_user) && $this->current_user->role_id == 1))

				&& (!isset($field['frontend']) || $field['frontend'] === TRUE))

			{

				$this->form_validation->set_rules($field['name'], $field['label'], $field['rules']);

			}

		}





		if ($this->form_validation->run() === FALSE)

		{

			return FALSE;

		}



		// Compile our core user elements to save.

		$data = array(

			'email'		=> $this->input->post('email'),
			'language'	=> $this->input->post('language'),
			'timezone'	=> $this->input->post('timezones'),
			'dob' => format_date($this->input->post('dob'),"Y-m-d"),
			'designation_id' => $this->input->post('designation_id'),
			'subsidiary_id' => $this->input->post('subsidiary_id'),
			'highest_qualification' => $this->input->post('highest_qualification'),
			'joining_date' => format_date($this->input->post('join_date'),"Y-m-d"),
					
			'gender' => $this->input->post('Gender'),
			'race' => $this->input->post('race'),
			'bumi' => $this->input->post('Bumi'),
			'sbu_id' => $this->input->post('sbu'),
			'cost_centre_id' => $this->input->post('cost_centre'),
			'user_level_id' => $this->input->post('user_level'),
			'origin' => $this->input->post('origin')
			
  
 

		);
		
		if (!empty($this->input->post('leave_date')))
		{   
		$data['leaving_date'] 	=   format_date($this->input->post('leave_date'),"Y-m-d");
		 }
		// If empty, the password will be left unchanged.

		if ($this->input->post('password') !== '')

		{

			$data['password'] = $this->input->post('password');

		}



		if ($this->input->post('display_name') !== '')

		{

			$data['display_name'] = $this->input->post('display_name');

		}



		if (isset($_POST['username']))

		{

			$data['username'] = $this->input->post('username');

		}
if (isset($_POST['company_id']))
		{
			$data['company_id'] = $this->input->post('company_id');
		}



		// Any modules needing to save data?

		// Event to run after saving a user

		Events::trigger('save_user', $payload );


		return $this->user_model->update($id, $data);
		
		//echo $this->db->last_query(); exit;



	}//end save_user()



	//--------------------------------------------------------------------



		//--------------------------------------------------------------------

		// ACTIVATION METHODS

		//--------------------------------------------------------------------

		/*

			Activate user.



			Checks a passed activation code and if verified, enables the user

			account. If the code fails, an error is generated and returned.



		*/
		
		public function packages()
		{		
			$this->load->model('packages/packages_model',null,true);
			Template::set('packages', $this->packages_model->find_all());
			
			$this->load->library('geoplugin');
			$this->geoplugin->locate();
			$geoplugin = $this->geoplugin;
			Template::set('geoplugin',$this->geoplugin);
			
			$this->load->model('currency/currency_model',null,true);
			$currency_model = $this->currency_model;
			Template::set('get_currency', function($currency_id = 1) use($currency_model, $geoplugin){
				$home_currency = 'RM';
				$currency = $currency_model->find($currency_id);
				if($currency)
					$home_currency = $currency->home_currency;
				if(isset($geoplugin->countryCode) && $geoplugin->countryCode != 'MY')
					$home_currency = 'USD';
				return $home_currency;
			});

			Template::set('languages', unserialize($this->settings_lib->item('site.languages')));
			Template::set_view('users/users/packages');
			Template::set('page_title', 'Packages');
			Template::render();
		}
	


public function activate($user_id = NULL)

		{

			if (isset($_POST['activate']))

			{

				$this->form_validation->set_rules('code', 'Verification Code', 'required|trim');

				if ($this->form_validation->run() == TRUE)

				{

					$code = $this->input->post('code');



					$activated = $this->user_model->activate($user_id, $code);

					if ($activated)

					{

						$user_id = $activated;



						// Now send the email

						$this->load->library('emailer/emailer');



						$site_title = $this->settings_lib->item('site.title');



						$email_message_data = array(

							'title' => $site_title,

							'link'  => site_url(LOGIN_URL)

						);

						$data = array

						(

							'to'		=> $this->user_model->find($user_id)->email,

							'subject'	=> lang('us_account_active'),

							'message'	=> $this->load->view('_emails/activated', $email_message_data, TRUE)

						);



						if ($this->emailer->send($data))

						{

							Template::set_message(lang('us_account_active'), 'success');

						}

						else

						{

							Template::set_message(lang('us_err_no_email'). $this->emailer->error, 'error');

						}

						Template::redirect('/');

					}

					else

					{

						Template::set_message($this->user_model->error.'. '. lang('us_err_activate_code'), 'error');

					}

				}

			}

			Template::set_view('users/users/activate');

			Template::set('page_title', 'Account Activation');

			Template::render();

		}



		//--------------------------------------------------------------------



		/*

			   Method: resend_activation



			   Allows a user to request that their activation code be resent to their

			   account's email address. If a matching email is found, the code is resent.

		   */

		public function resend_activation()

		{

			if (isset($_POST['send']))

			{

				$this->form_validation->set_rules('email', 'lang:bf_email', 'required|trim|valid_email');



				if ($this->form_validation->run())

				{

					// We validated. Does the user actually exist?

					$user = $this->user_model->find_by('email', $_POST['email']);



					if ($user !== FALSE)

					{

						// User exists, so create a temp password.

						$this->load->helpers(array('string', 'security'));



						$pass_code = random_string('alnum', 40);



						$activation_code = do_hash($pass_code . $user->password_hash . $_POST['email']);



						$site_title = $this->settings_lib->item('site.title');



						// Save the hash to the db so we can confirm it later.

						$this->user_model->update_where('email', $_POST['email'], array('activate_hash' => $activation_code ));



						// Create the link to reset the password

						$activate_link = site_url('activate/'. str_replace('@', ':', $_POST['email']) .'/'. $activation_code);



						// Now send the email

						$this->load->library('emailer/emailer');



						$email_message_data = array(

							'title' => $site_title,

							'code'  => $activation_code,

							'link'  => $activate_link

						);



						$data = array

						(

							'to'		=> $_POST['email'],

							'subject'	=> 'Activation Code',

							'message'	=> $this->load->view('_emails/activate', $email_message_data, TRUE)

						);

						$this->emailer->enable_debug(true);

						if ($this->emailer->send($data))

						{

							Template::set_message(lang('us_check_activate_email'), 'success');

						}

						else

						{

							Template::set_message(lang('us_err_no_email').$this->emailer->error.", ".$this->emailer->debug_message, 'error');

						}

					}

					else

					{

						Template::set_message('Cannot find that email in our records.', 'error');

					}

				}

			}

			Template::set_view('users/users/resend_activation');

			Template::set('page_title', 'Activate Account');

			Template::render();

		}

		// A function for Paypal page

		public function paypal($subscription_type = '')
		{
			$this->load->library('paypal/paypal');
			$this->load->model('packages/packages_model',null,true);
			
			$package = $this->packages_model->find((int) $this->session->userdata('checkout_package_id'));
			/*
			$this->load->model('currency/currency_model',null,true);
			$currency = $this->currency_model->find($package->currency_id);

			$this->load->library('geoplugin');
        	$this->geoplugin->locate();
			
        	if(isset($this->geoplugin->countryCode) && $this->geoplugin->countryCode != 'MY')
        		$currency->converted_currency = 'USD';
			*/
			//$purchase = $this->paypal->create_purchase(array('name' => $package->package_name, 'description' => $package->description, 'amount' => $package->price));
			//if($subscription_type && $subscription_type == 'subscribe')
			$currency = $this->session->userdata('checkout_country') == 'MY' ? 'MYR' : 'USD';
			$price_per_user = number_format(($currency == 'MYR' ? $package->price_in_myr : $package->price) * $this->session->userdata('checkout_package_period') * $this->discount[$this->session->userdata('checkout_package_period')], 2);
			$total_price = number_format($price_per_user * $package->no_of_users, 2);
				$purchase = $this->paypal->create_subscription(array(
					'currency' => $currency, 
					'description' => $package->package_name .': '.$package->description.', '.$currency.' '.$price_per_user .' / '. ($this->session->userdata('checkout_package_period') == 12 ? 'year' : 'month') .' X '.$package->no_of_users .' users = '.$currency.' '.$total_price, 
					'amount' => $total_price, 
					'initial_amount' => $total_price, 
					'start_date' => date( 'Y-m-d\TH:i:s', $this->session->userdata('checkout_package_period') == 12 ? strtotime('+1 year') : strtotime('next month')), 
					'frequency' => $this->session->userdata('checkout_package_period'), 
					'max_failed_payments' => 5
				));
				
			// log data
			$this->load->model('log_payment/log_payment_model',null,true);
			$this->session->set_userdata('checkout_log_payment_id', $this->log_payment_model->insert(array(
				'company_name' => $this->session->userdata('checkout_company_name'),
				'email' => $this->session->userdata('checkout_email'),
				'package_id' => $this->session->userdata('checkout_package_id'),
				'package_period' => $this->session->userdata('checkout_package_period'),
				'provider' => 'paypal',
				'status' => 'Pending',
				'raw_data' => json_encode(array('token' => $purchase->token())),
			)));
			log_activity(1, 'payment attempt by '.$this->session->userdata('checkout_email').' ('.$this->session->userdata('checkout_company_name').') for '.$package->package_name.' package from ' . $this->input->ip_address(), 'paypal');	
			
			Template::set_view('users/users/paypal');
			Template::set('checkout_url', $purchase->get_checkout_url());
			Template::set('page_title', 'Paypal Payment Gateway');
			Template::render();
		}
		
		public function paypal_return()
		{
			$this->load->library('paypal/paypal');
			$response = array();
			
			$this->load->model('packages/packages_model',null,true);
			$package = $this->packages_model->find((int) $this->session->userdata('checkout_package_id'));
			/*
			$this->load->model('currency/currency_model',null,true);
			$currency = $this->currency_model->find($package->currency_id);

			if(isset($this->geoplugin->countryCode) && $this->geoplugin->countryCode != 'MY')
        		$currency->converted_currency = 'USD';
			*/
			//if($this->input->get('PayerID')) {
			//	$purchase = $this->paypal->create_purchase(array('name' => $package->package_name, 'description' => $package->description, 'amount' => $package->price));
			//	$response = $purchase->process_payment();
			//} else {
			$currency = $this->session->userdata('checkout_country') == 'MY' ? 'MYR' : 'USD';
			$price_per_user = number_format(($currency == 'MYR' ? $package->price_in_myr : $package->price) * $this->session->userdata('checkout_package_period') * $this->discount[$this->session->userdata('checkout_package_period')], 2);
			$total_price = number_format($price_per_user * $package->no_of_users, 2);
				$subscription = $this->paypal->create_subscription(array(
					'currency' => $currency, 
					'description' => $package->package_name .': '.$package->description.', '.$currency.' '.$price_per_user .' / '. ($this->session->userdata('checkout_package_period') == 12 ? 'year' : 'month') .' X '.$package->no_of_users .' users = '.$currency.' '.$total_price, 
					'amount' => $total_price, 
					'initial_amount' => $total_price, 
					'start_date' => date( 'Y-m-d\TH:i:s', $this->session->userdata('checkout_package_period') == 12 ? strtotime('+1 year') : strtotime('next month')), 
					'frequency' => $this->session->userdata('checkout_package_period'), 
					'max_failed_payments' => 5
				));
				$response = $subscription->start_subscription();
			//}
			
			$response = array_merge($_GET, $response);
				
			// log payment data
			$this->load->model('log_payment/log_payment_model',null,true);
			$this->log_payment_model->update($this->session->userdata('checkout_log_payment_id'), array(
				'status' => $response['ACK'],
				'raw_data' => json_encode($response),
			));
			log_activity(1, 'payment returned by '.$this->session->userdata('checkout_email').' ('.$this->session->userdata('checkout_company_name').') for package id :'.$this->session->userdata('checkout_package_id').' from ' . $this->input->ip_address(), 'paypal');
			// end log
			
			$this->load->model('packages/packages_model',null,true);
			$package = $this->packages_model->find((int) $this->session->userdata('checkout_package_id'));
			
			if($response['ACK'] == 'Success') {
				
				$this->session->unset_userdata(array(
					'checkout_package_period' => '',
					'checkout_package_id' => '',
					'checkout_company_name' => '',
				));
			}
			
			$this->session->unset_userdata('checkout_log_payment_id');
			
			Template::set('package', $package);
			Template::set('response', $response);
			Template::set_view('users/users/paypal_return');
			Template::set('page_title', 'Paypal Return Page');
			Template::render();
			
		}

		//Paypal return function after cancel

		public function paypal_cancel()
		{
			// update log payment data
			if($this->session->userdata('checkout_log_payment_id')) {
			
				$this->load->model('log_payment/log_payment_model',null,true);
				$this->log_payment_model->update($this->session->userdata('checkout_log_payment_id'), array('status' => 'Cancel'));
				
				// notify admin
				$log_payment = $this->log_payment_model->find((int) $this->session->userdata('checkout_log_payment_id'));
				
				$this->load->library('paypal/paypal');
				$this->load->library('emailer/emailer');
				$this->emailer->addAddress($this->paypal->email);
				$this->emailer->Subject = 'Someone Cancelled Payment';
				$this->emailer->Body = 'email: '.$log_payment->email . ', company: '.$log_payment->company_name;
				$this->emailer->mail();
				
				$this->session->unset_userdata('checkout_log_payment_id');
			}
			
			Template::set_view('users/users/paypal_cancel');
			Template::set('page_title', 'Payment Cancelled');
			Template::render();
		}
		
		// Paypal IPN url
		public function paypal_notify()
		{
			$data = (array) array_merge($_POST, $_GET);
			
			$this->load->model('log_payment/log_payment_model',null,true);
			$log_payment_id = $this->log_payment_model->insert(array(
				'company_name' => 'Unknown',
				'email' => 'Unknown',
				'package_id' => 1,
				'provider' => 'paypal',
				'status' => isset($data['txn_type']) ? $data['txn_type'] : 'Unknown',
				'raw_data' => json_encode($data),
			));
			
			if(isset($data['txn_type'])) {
			
				$this->load->library('emailer/emailer');
				$this->load->library('paypal/paypal');
				$this->load->model('license/license_model',null,true);
				$this->load->model('companies/companies_model',null,true);
				
				$license = $this->license_model->find_by(array('transaction_id' => $data['recurring_payment_id']));
				
				$user = $this->user_model->find_by(array('company_id' => $license->company_id, 'users.role_id' => 7));

				switch($data['txn_type']){
					case 'recurring_payment_profile_created':
					
						shell_exec('curl '.site_url('users/validate_subscription_payment/'.$log_payment_id).' > /dev/null 2>/dev/null &');
						
						break;
					case 'recurring_payment_profile_cancel':
			
						$this->license_model->skip_validation(true)->update($license->id, array('is_cancelled' => 1));
				
						$company = $this->companies_model->find($license->company_id);
				
						$this->emailer->addAddress($this->paypal->email);
						$this->emailer->addAddress($user->email);
						$this->emailer->Subject = 'A subscription have been cancelled';
						$this->emailer->Body = 'Company of '.$company->name .' have cancelled its subscription.';
						$this->emailer->mail();
						break;
					case 'recurring_payment':
			
						$end_date = date('Y-m-d', strtotime($data['payment_cycle'] == 'Monthly' ? '+1 month' : '+1 year', strtotime($license->end_date)));
						$this->license_model->skip_validation(true)->update($license->id, array('end_date' => $end_date));
						
						$this->emailer->addAddress($user->email);

						$this->emailer->Subject = 'We have received your payment';
						$this->emailer->Body = 'We have received your payment for SPP subscription. Your next billing is on '.$end_date;
						$this->emailer->mail();
						break;
					case 'recurring_payment_failed':
					
						$this->emailer->addAddress($user->email);
						$this->emailer->Subject = 'A recurring payment for SPP subscription is failed';
						$this->emailer->Body = 'A recurring payment for SPP subscription is failed. Please update your credit card info in paypal as soon as possible to avoid subscription termination.';
						$this->emailer->mail();
						break;
					case 'recurring_payment_suspended_due_to_max_failed_payment':
			
						$this->license_model->skip_validation(true)->update($license->id, array('is_cancelled' => 1));
				
						$company = $this->companies_model->find($license->company_id);
				
						$this->emailer->addAddress($this->paypal->email);
						$this->emailer->addAddress($user->email);
						$this->emailer->Subject = 'A subscription have been cancelled';
						$this->emailer->Body = 'Company of '.$company->name .' have cancelled its subscription because max failed payment is reached.';
						$this->emailer->mail();
						break;
				}
			}
		}
		
		public function validate_subscription_payment($log_payment_id)
		{
			$this->load->library('paypal/paypal');
			$this->load->library('emailer/emailer');
		
			$this->load->model('log_payment/log_payment_model',null,true);
			$foo = $this->log_payment_model->find($log_payment_id);
			if(!$foo)
				return;
			$data = (array) json_decode($foo->raw_data);
			
			$initial_payment_max_attempt = 25;
			$last_seconds_gap_attmept = 0;
			$seconds_gap_attmept = 1;
			
			if(strtolower($data['initial_payment_status']) != 'completed')
				while($initial_payment_max_attempt > 0){
							
					sleep($seconds_gap_attmept);
								
					$foo = $last_seconds_gap_attmept;
					$last_seconds_gap_attmept = $seconds_gap_attmept;
					$seconds_gap_attmept += $foo;
								
					$initial_payment = $this->paypal->get_transaction_details($data['initial_payment_txn_id']);
					if(!$initial_payment)
						continue;
					if(strtolower($initial_payment['PAYMENTSTATUS']) == 'completed')
						break;
						
					$initial_payment_max_attempt--;
				}
						
			$log_payment = $this->log_payment_model->like('raw_data','"PROFILEID":"'.$data['recurring_payment_id'].'"')->find_by('status','Success');
						
			if($initial_payment_max_attempt > 0 && $log_payment) {
				
				$this->load->model('license/license_model',null,true);
				$this->load->model('packages/packages_model',null,true);
				$package = $this->packages_model->find($log_payment->package_id);
				// notify user
				$this->emailer->addAddress($log_payment->email);
				$this->emailer->Subject = 'We have received your payment';
				$this->emailer->Body = $this->load->view('_emails/successful_payment', array('package' => $package), TRUE);
				$this->emailer->mail();

				$this->load->model('companies/companies_model',null,true);

				$company = $this->companies_model->find_by(array('name' => $log_payment->company_name));

				if($company) { // if exist it means from demo acc

					// update license
					$this->license_model->skip_validation(true)->update_where('company_id', $company->id, array(
						'package_id' => $package->id,
						'package_name' => $package->package_name,
						'no_of_users' => $package->no_of_users,
						'modules' => $package->modules,
						'price' => $data['amount'],
						'end_date' => date('Y-m-d', $log_payment->package_period == 12 ? strtotime('+1 year') : strtotime('next month'))));
				
				} else { // create new acc

				// validate company name and email again
				if(!$this->companies_model->is_unique('name', $log_payment->company_name) || !$this->user_model->is_unique('email',$log_payment->email)) {
				
					$this->emailer->addAddress($log_payment->email);
					$this->emailer->addAddress($this->paypal->email);
					
					$this->emailer->Subject = 'Registration failed in SPP';
					
					$email_msg = 'Registration failed because of the following reason(s),<ul>';
					
					if(!$this->companies_model->is_unique('name', $log_payment->company_name))
						$email_msg .= '<li>Company name of "'.$log_payment->company_name.'" already in use</li>';
						
					if(!$this->user_model->is_unique('email', $log_payment->email))
						$email_msg .= '<li>Email of "'.$log_payment->email.'" already in use</li>';
						
					$this->emailer->Body = $email_msg.'</ul>';
					$this->emailer->mail();
					
					return;
				}

				$registered_data = $this->confirm_register(array(
						'package_id' => $log_payment->package_id, 
						'company_name' => $log_payment->company_name, 
						'email' => $log_payment->email));
			
				// send email to admin
				$this->emailer->addAddress($this->paypal->email);
				$this->emailer->Subject = 'A new account is created';
				$this->emailer->Body = 'A new subcsription for '.$package->package_name.' plan have been created by '.$log_payment->email;
				$this->emailer->mail();
				
				// update license
				$this->license_model->skip_validation(true)->update($registered_data['license_id'], array(
						'end_date' => date('Y-m-d', $log_payment->package_period == 12 ? strtotime('+1 year') : strtotime('next month')),
						'payment_date' => date('Y-m-d'),
						'amount_paid' => $data['amount'],
						'payment_mode' => 'paypal',
						'transaction_id' => $data['recurring_payment_id'],
						'transaction_details' => json_encode($data),
					));
			} // end create new acc

			} else if($initial_payment_max_attempt < 1) {

				$this->emailer->addAddress($this->paypal->email);

				$this->emailer->addAddress($log_payment->email);

				$this->emailer->Subject = 'First month payment failed';

				$this->emailer->Body = 'Subscription failed because first month payment failed.';

				$this->emailer->mail();

			} else {

				$this->emailer->addAddress($this->paypal->email);

				$this->emailer->Subject = 'Log payment data not found';

				$this->emailer->Body = 'Log payment data not found. Probably because they pay from other site or you forgot to change Paypal IPN url to current working site.';

				$this->emailer->mail();
			}
		}
	//--------------------------------------------------------------------


		//--------------------------------------------------------------------

		/* Method: OAuth */

		public function oauth()
		{
			if($this->uri->segment(3) != '') {
			
				$this->load->config('opauth');
				$this->load->library('Opauth/Opauth', $this->config->item('opauth_config'));
				$this->opauth->run();
			}
		}

		public function oauth_response()
		{
			$isValid = false;
		
			if($this->input->get('opauth')) {
			
				$response = unserialize(base64_decode( $this->input->get('opauth') ));
				// validate
				$this->load->config('opauth');
				$this->load->library('Opauth/Opauth', $this->config->item('opauth_config'));
				
				$isValid = $this->opauth->validate(sha1(print_r($response['auth'], true)), $response['timestamp'], $response['signature'], $error = '');
				$provider = $response['auth']['provider'];
				$uid = $response['auth']['uid'];
				
			} else if($this->input->get('fb_login')) {
			
				$response = json_decode(file_get_contents('https://graph.facebook.com/me?access_token='.$this->input->get('accessToken')), true);
				$isValid = !isset($response['error']);
				$error = isset($response['error']) ? $response['error']['message'] : '';
				$provider = 'Facebook';
				$uid = $this->input->get('userID');
			}
			
			// process login
			if($isValid) {
			
				$this->load->model('users_social_login/users_social_login_model', null, true);
				
				if($this->auth->is_logged_in() === false) {
				
					$social_login = $this->users_social_login_model->find_by(array(
						'social_provider' => $provider,
						'social_uid' => $uid
					));
					
					if($social_login) {
					
						$user = $this->user_model->find($social_login->user_id);
				
						$this->auth->login_using_user_obj($user);

						Template::redirect(!empty($user->login_destination) ? $user->login_destination : '/');
					}
					
					Template::set_message('No associated accounts found.', 'error');
					Template::redirect('login');
				
				} else {
					
					$this->users_social_login_model->delete_where(array('social_uid' => $uid, 'social_provider' => $provider));
					$this->users_social_login_model->delete_where(array('user_id' => $this->auth->user_id(), 'social_provider' => $provider));
					$this->users_social_login_model->insert(array('user_id' => $this->auth->user_id(), 'social_provider' => $provider, 'social_uid' => $uid));
					Template::redirect('users/profile');
				}
			}
		}


}//end Users



/* Front-end Users Controller */

/* End of file users.php */

