<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$view =<<<END
<style>
#reset {
	-webkit-animation: flash 6s infinite linear;
	animation: flash 6s infinite linear;
}
</style>
<div class="row">
<section class="panel panel-default">
 <div class="panel-body">
	
	<?php 

echo form_open(\$this->uri->uri_string(),'class="form-inline"'); ?>
<div class="row">
  <div class="col-md-6">
 <h4>{$module_name}</h4></div>
 <div class="col-md-3">
	
<select name="select_field" id="select_field" class="form-control m-b" onchange="setfname()">

<? if ( \$this->session->userdata('{$module_name_lower}field') !== NULL ) { ?>
<option  selected="selected" value="<?=\$this->session->userdata('{$module_name_lower}field')?>"><?=\$this->session->userdata('{$module_name_lower}field')?></option>
<? } else { ?><option selected="selected" disabled="disabled">Please Select</option><? }  ?>
<option>All Fields</option>		
<?
\$fquery = \$this->db->query('SHOW COLUMNS FROM intg_$module_name_lower')->result();
foreach  ( \$fquery as \$frow  ) {
?>
<option value="<? echo \$frow->Field; ?>"><? echo \$frow->Field; ?></option>
<? } ?>
</select>
</div><div class="col-md-3"><div class="input-group">
<input type="text" name="field_value" class="form-control" placeholder="Enter" value="<?=\$this->session->userdata('{$module_name_lower}fvalue')?>">
<input type="hidden" name="field_name" id="field_name" value="<?=\$this->session->userdata('{$module_name_lower}fname')?>"/>


<span class="input-group-btn">
<button type="submit" name="submit" value="Search" title="Search" class="btn btn-info"><span class="fa  fa-search"></span></button>
<button type="submit" name="submit"   class="btn btn-primary btn-icon" <?=\$this->session->userdata('{$module_name_lower}field') ? 'id="reset"' : ''?> value="Reset" title="Reset"><span class="fa  fa-refresh"></span></button></span>
</div></div></div>

<?php echo form_close(); ?>
     <div class="table-responsive">   
  


	<?php  echo form_open(\$this->uri->uri_string()); ?>
		<table class="table table-striped datagrid m-b-sm">
			<thead>
				<tr>
					<?php if (\$this->auth->has_permission('{delete_permission}') && isset(\$records) && is_array(\$records) && count(\$records)) : ?>
					<th class="column-check"><input class="check-all" type="checkbox" /></th>
					<?php endif;?>
					{table_header}
				</tr>
			</thead>
			<?php if (isset(\$records) && is_array(\$records) && count(\$records)) : ?>
			<tfoot>
				<?php if (\$this->auth->has_permission('{delete_permission}')) : ?>
				<tr>
					<td colspan="{cols_total}">
						<?php echo lang('bf_with_selected') ?>
                         <input type="submit" name="restore" class="btn" value="<?php echo lang('bf_action_restore') ?>">
                       
				<!--	<input type="submit" name="purge" class="btn btn-danger" value="<?php //echo lang('bf_action_purge') ?>" onclick="return confirm('<?php //echo lang('us_purge_del_confirm'); ?>')">-->
						
					</td>
				</tr>
				<?php endif;?>
			</tfoot>
			<?php endif; ?>
			<tbody>
			<?php if (isset(\$records) && is_array(\$records) && count(\$records)) :  \$no = \$this->input->get('per_page')+1;?>
			<?php foreach (\$records as \$record) : ?>
				<tr>
					<?php if (\$this->auth->has_permission('{delete_permission}')) : ?>
					<td><input type="checkbox" name="checked[]" value="<?php echo \$record->{$primary_key_field} ?>" /></td><td><?=\$no;?></td>
					<?php endif;?>
					{table_records}
				</tr>
			<?php \$no++; endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="{cols_total}">No records found that match your selection.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
         
	<?php echo form_close(); ?>
  <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 hidden-xs">
                      
                                
                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">Showing <?=\$offset+1?> - <? echo \$rowcount+\$offset?> Of  <?  echo \$total; ?></small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                    
                    <?php  echo \$this->pagination->create_links(); ?>
                    </div>
                  </div>
                </footer>                
                 </div>
              
                              
                     </div>
                     </section>
                     </div>
END;
$headers = '<th>#</th>';

for($counter=1; $field_total >= $counter; $counter++)
{
	// only build on fields that have data entered.

	//Due to the required if rule if the first field is set the the others must be

	if (set_value("view_field_label$counter") == NULL)
	{
		continue; 	// move onto next iteration of the loop
	}
    
    $fieldnames = set_value("view_field_name$counter");
	$headers .= "
					<th <?php if (\$this->input->get('sort_by') == '$module_name_lower'.'_$fieldnames') { echo 'class=\'sort_'.\$this->input->get('sort_order').'\''; }  ?>>
                   <a href='<?php echo base_url() .'index.php/admin/".$controller_name."/".$module_name_lower."?sort_by=".$module_name_lower."_".$fieldnames."&sort_order='.((\$this->input->get('sort_order') == 'asc' && \$this->input->get('sort_by') == '$module_name_lower'.'_$fieldnames') ? 'desc' : 'asc'); ?>'>
                    ". set_value("view_field_label$counter")."</a></th>";
                    
                    
}
/*if ($use_soft_deletes == 'true')
{
	$headers .= '
					<th>Deleted</th>';
}*/
if ($use_created == 'true')
{
	$headers .= '
					<th>Created</th>';
}
if ($use_modified == 'true')
{
	$headers .= '
					<th>Modified</th>';
}

$table_records = '';
$pencil_icon   = "'<i class=\"icon-pencil\">&nbsp;</i>' . ";
for($counter=1; $field_total >= $counter; $counter++)
{
	// only build on fields that have data entered.

	//Due to the requiredif rule if the first field is set the the others must be

	if (set_value("view_field_name$counter") == NULL || set_value("view_field_name$counter") == $primary_key_field)
	{
		continue; 	// move onto next iteration of the loop
	}

	if($db_required == 'new' && $table_as_field_prefix === TRUE)
	{
		$field_name = $module_name_lower . '_' . set_value("view_field_name$counter");
	}
	elseif($db_required == 'new' && $table_as_field_prefix === FALSE)
	{
		$field_name = set_value("view_field_name$counter");
	}
	else
	{
		$field_name = set_value("view_field_name$counter");
	}

	if ($counter == 1) {
		$table_records .= "
				<?php if (\$this->auth->has_permission('{edit_permission}')) : ?>
				<td><?php echo anchor(SITE_AREA .'/".$controller_name."/".$module_name_lower."/restore_purge/'. \$record->".$primary_key_field.", {$pencil_icon} \$record->".$field_name.") ?></td>
				<?php else: ?>
				<td><?php echo \$record->".$field_name." ?></td>
				<?php endif; ?>
			";
	}
	else {
    
    if ( set_value("db_field_type$counter")=="DATE"  ) {
				
				$table_records .= '
					<td><?php echo $record->'.$field_name.'=="0000-00-00" ? "" : date("d/m/Y",strtotime($record->'.$field_name.')); ?></td>';
				
			} 
			
			else if (  set_value("db_field_type$counter")=="DATETIME" ) {
				
				$table_records .= '
					<td><?php echo $record->'.$field_name.'=="0000-00-00 00:00:00" ? "" : date("d/m/Y h:i:s",strtotime($record->'.$field_name.')); ?></td>';
				
			}
			else {
		$table_records .= '
				<td><?php echo $record->'.$field_name.'?></td>';
                }
	}
}
/*if ($use_soft_deletes == 'true')
{
	$table_records .= '
				<td><?php echo $record->deleted > 0 ? lang(\''.$module_name_lower.'_true\') : lang(\''.$module_name_lower.'_false\')?></td>';
	$field_total++;
}*/
if ($use_created == 'true')
{
	$table_records .= '
				<td><?php echo date("d/m/Y h:i:s",strtotime($record->'.set_value("created_field").')); ?></td>';
	$field_total++;
}
if ($use_modified == 'true')
{
	$table_records .= '
			<td><?php echo $record->'.set_value("modified_field").'=="0000-00-00 00:00:00" ? "" :	date("d/m/Y h:i:s",strtotime($record->'.set_value("modified_field").')); ?></td>';
	$field_total++;
}



$view = str_replace('{cols_total}', $field_total + 1 , $view);
$view = str_replace('{table_header}', $headers, $view);
$view = str_replace('{table_records}', $table_records, $view);
$view = str_replace('{delete_permission}', preg_replace("/[ -]/", "_", ucfirst($module_name)).'.'.ucfirst($controller_name).'.Delete', $view);
$view = str_replace('{edit_permission}', preg_replace("/[ -]/", "_", ucfirst($module_name)).'.'.ucfirst($controller_name).'.Edit', $view);

echo $view;

unset($view, $headers);
