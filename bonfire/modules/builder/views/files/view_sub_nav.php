<?php

$controller_name_lower = strtolower($controller_name);



$view = <<<END

<ul class="nav nav-pills">

	<li <?php echo \$this->uri->segment(4) == '' ? 'class="active"' : '' ?>>

		<a href="<?php echo site_url(SITE_AREA .'/{$controller_name_lower}/{$module_name_lower}') ?>"style="border-radius:0"  id="list"><?php echo lang('{$module_name_lower}_list'); ?></a>

	</li>

	<?php if (\$this->auth->has_permission('{create_permission}')) : ?>

	<li <?php echo \$this->uri->segment(4) == 'create' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/{$controller_name_lower}/{$module_name_lower}/create') ?>"style="border-radius:0"  id="create_new"><?php echo lang('{$module_name_lower}_new'); ?></a>

	</li>

	<?php endif; ?>

     <?php if (\$this->auth->has_permission('{deleted_permission}')) : ?>

    	<li <?php echo \$this->uri->segment(4) == 'deleted' ? 'class="active"' : '' ?> >

		<a href="<?php echo site_url(SITE_AREA .'/{$controller_name_lower}/{$module_name_lower}/deleted') ?>" style="border-radius:0"  id="deleted">Deleted</a>

	</li>

    <?php endif; ?>

</ul>

END;



$view = str_replace('{create_permission}', preg_replace("/[ -]/", "_", ucfirst($module_name)).'.'.ucfirst($controller_name).'.Create', $view);

$view = str_replace('{deleted_permission}', preg_replace("/[ -]/", "_", ucfirst($module_name)).'.'.ucfirst($controller_name).'.Deleted', $view);



echo $view;

