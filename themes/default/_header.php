<?php
    Assets::add_js(	array('bootstrap.js','app.js','app.plugin.js', 'appear/jquery.appear.js','slimscroll/jquery.slimscroll.min.js',  'scroll/smoothscroll.js',  'landing.js') );
	

    Assets::add_css( array('bootstrap.css', 'animate.css','font-awesome.min.css', 'font.css',  'app.css',  'landing.css','js/select2/select2.css','js/select2/theme.css'));
 

    $inline  = '$(".dropdown-toggle").dropdown();';
    $inline .= '$(".tooltips").tooltip();';

    Assets::add_js( $inline, 'inline' );
?>
<!doctype html>
<html lang="en" class="">

    <meta charset="utf-8">

    <title><?php echo isset($page_title) ? $page_title .' : ' : ''; ?> <?php if (class_exists('Settings_lib')) e(settings_item('site.title')); else echo 'Bonfire'; ?></title>

    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <meta name="description" content="">
    <meta name="author" content="">
    

    <?php echo Assets::css(); ?>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
	<script src="<?php echo Template::theme_url('js/jquery.min.js'); ?>"></script>
</head>
<body>

