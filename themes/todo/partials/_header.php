<?php
	
	
	  Assets::add_css( array('bootstrap.css', 'animate.css','font-awesome.min.css', 'font.css', 'plugin.css',  'app.css','select2.css'));	

	
	

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?> <?php e($this->settings_lib->item('site.title')) ?></title>

	 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<meta name="robots" content="noindex" />
	<?php echo Assets::css(null, true); ?>
	
</head>
<body>
 <section class="hbox stretch">
<!--[if lt IE 7]>
		<p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or
		<a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p>
<![endif]-->


				<? /*
				<!-- izereb title-->
					<?php echo anchor( '/', html_escape($this->settings_lib->item('site.title')), 'class="brand"' ); ?>


					

					<div class="nav-collapse">
						<!-- User Menu -->
						<div class="nav pull-right" id="user-menu">
							<div class="btn-group">
								<a href="<?php echo site_url(SITE_AREA .'/settings/users/edit') ?>" id="tb_email" class="btn dark" title="<?php echo lang('bf_user_settings') ?>">
									<?php echo (isset($current_user->display_name) && !empty($current_user->display_name)) ? $current_user->display_name : ($this->settings_lib->item('auth.use_usernames') ? $current_user->username : $current_user->email); ?>
								</a>
								<!-- Change **light** to **dark** to match colors -->
								<a class="btn dropdown-toggle light" data-toggle="dropdown" href="#"><span class="caret"></span></a>
								<ul class="dropdown-menu pull-right toolbar-profile">
									<li>
										<div class="inner">
											<div class="toolbar-profile-img">
												<?php echo gravatar_link($current_user->email, 96, null, $current_user->display_name) ?>
											</div>

											<div class="toolbar-profile-info">
												<p><b><?php echo $current_user->display_name ?></b><br/>
													<?php e($current_user->email) ?>
													<br/>
												</p>
												<a href="<?php echo site_url(SITE_AREA .'/settings/users/edit') ?>"><?php echo lang('bf_user_settings')?></a>
												<a href="<?php echo site_url('logout'); ?>"><?php echo lang('bf_action_logout')?></a>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>

						<?php echo Contexts::render_menu('text', 'normal'); ?>
					</div> <!-- END OF nav-collapse -->

		
			<div style="clearfix"></div>
	

	

 <div class="subnav" >
	<div class="container-fluid">

		<?php if (isset($toolbar_title)) : ?>
			<h1><?php echo $toolbar_title ?></h1>
		<?php endif; ?>

		<div class="pull-right" id="sub-menu">
			<?php Template::block('sub_nav', ''); ?>
		</div>
	</div>
</div>
 */ ?>

<aside class="bg-primary aside-sm" id="nav">
      <section class="vbox">
        <header class="dker nav-bar nav-bar-fixed-top">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
            <i class="fa fa-bars"></i>
          </a>
          <a href="#" class="nav-brand" data-toggle="fullscreen">i-Aset</a>
          <a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-user">
            <i class="fa fa-comment-o"></i>
          </a>
        </header>
        <section>
          <!-- user -->
          <div class="bg-success nav-user hidden-xs pos-rlt">
            <div class="nav-avatar pos-rlt">
              <a href="#" class="thumb-sm avatar animated rollIn" data-toggle="dropdown">
                <img src="<?php echo Template::theme_url('images/avatar.jpg') ?>" alt="" class="">
                <span class="caret caret-white"></span>
              </a>
              <ul class="dropdown-menu m-t-sm animated fadeInLeft">
              	<span class="arrow top"></span>
                <li>
                  <a href="#">Settings</a>
                </li>
                <li>
                  <a href="profile.html">Profile</a>
                </li>
                <li>
                  <a href="#">
                    <span class="badge bg-danger pull-right">3</span>
                    Notifications
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="docs.html">Help</a>
                </li>
                <li>
                  <a href="signin.html">Logout</a>
                </li>
              </ul>
              <div class="visible-xs m-t m-b">
                <a href="#" class="h3">John.Smith</a>
                <p><i class="fa fa-map-marker"></i> London, UK</p>
              </div>
            </div>
            <div class="nav-msg">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <b class="badge badge-white count-n">2</b>
              </a>
              <section class="dropdown-menu m-l-sm pull-left animated fadeInRight">
                <div class="arrow left"></div>
                <section class="panel bg-white">
                  <header class="panel-heading">
                    <strong>You have <span class="count-n">2</span> notifications</strong>
                  </header>
                  <div class="list-group">
                    <a href="#" class="media list-group-item">
                      <span class="pull-left thumb-sm">
                        <img src="images/avatar.jpg" alt="John said" class="img-circle">
                      </span>
                      <span class="media-body block m-b-none">
                        Use awesome animate.css<br>
                        <small class="text-muted">28 Aug 13</small>
                      </span>
                    </a>
                    <a href="#" class="media list-group-item">
                      <span class="media-body block m-b-none">
                        1.0 initial released<br>
                        <small class="text-muted">27 Aug 13</small>
                      </span>
                    </a>
                  </div>
                  <footer class="panel-footer text-sm">
                    <a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
                    <a href="#">See all the notifications</a>
                  </footer>
                </section>
              </section>
            </div>
          </div>
          <!-- / user -->
          <!-- nav -->
            <nav class="nav-primary hidden-xs">
            <?php 
			 Contexts::set_themevar('todo');
		Contexts::set_parent_class('dropdown-submenu');//		  
		 echo Contexts::render_menu('text', 'normal'); 
		 
		 
		 ?></nav>
         
        
           
          
         </nav>
          <!-- note -->
          <div class="bg-danger wrapper hidden-vertical animated fadeInUp text-sm">            
              <a href="#" data-dismiss="alert" class="pull-right m-r-n-sm m-t-n-sm"><i class="fa fa-times"></i></a>
              Hi, welcome to todo,  you can start here.
          </div>
          <!-- / note -->
        </section>
        <footer class="footer bg-gradient hidden-xs">
          <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-sm btn-link m-r-n-xs pull-right">
            <i class="fa fa-power-off"></i>
          </a>
          <a href="#nav" data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm">
            <i class="fa fa-bars"></i>
          </a>
        </footer>
      </section>
    </aside>
    
    
			<div style="clearfix"></div>
	


	

	   <header class="header bg-light bg-gradient">
              
	
		<?php //if (isset($toolbar_title)) : ?>
			<?php //echo $toolbar_title ?>
		<?php //endif; ?>

	<div class="pull-left" id="sub-menu">
			<?php Template::block('sub_nav', ''); ?>
		</div>

            </header>
