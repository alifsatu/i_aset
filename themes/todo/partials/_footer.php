	</section>
    <footer class="container-fluid footer">
		<p class="pull-right">
			Executed in {elapsed_time} seconds, using {memory_usage}.
			<br/>
		</p>
	</footer>

	<div id="debug"></div>
   

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="<?php echo Template::theme_url('js/jquery.min.js'); ?>"></script>
  <!-- Bootstrap -->
  <script src="<?php echo Template::theme_url('js/bootstrap.js'); ?>"></script>
  <!-- Sparkline Chart -->
  <script src="<?php echo Template::theme_url('js/charts/sparkline/jquery.sparkline.min.js'); ?>"></script>
  <!-- App -->
  <script src="<?php echo Template::theme_url('js/app.js'); ?>"></script>
  <script src="<?php echo Template::theme_url('js/app.plugin.js'); ?>"></script>
  <script src="<?php echo Template::theme_url('js/app.data.js'); ?>"></script>
   <script src="<?php echo Template::theme_url('js/select2.js'); ?>"></script>
	
    
    <script>
	//$(document).ready(function(){	
		$('.selecta').select2({ placeholder: "Please Select",
    allowClear: true,dropdownAutoWidth:true});
	
			</script>
</body>
</html>
