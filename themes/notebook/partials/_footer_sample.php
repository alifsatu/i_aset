	
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
       
       
      </section>
    </section>
  </section>
   
<!--<script src="<?php echo Template::theme_url('js/jquery.min.js'); ?>"></script>
-->  <?  if ( basename($_SERVER['PHP_SELF']) != "fullcalendar" ) {?>
 <script src="<?php echo Template::theme_url('js/bootstrap.js'); ?>"></script> 
  <script src="<?php echo Template::theme_url('js/app.js'); ?>"></script><? } ?>
   <script src="<?php echo Template::theme_url('js/app.plugin.js'); ?>"></script>
 
  <script src="<?php echo Template::theme_url('js/fuelux/fuelux.js'); ?>"></script>
 <!-- <script src="<?php //echo Template::theme_url('js/app.data.js'); ?>"></script>-->
  <script src="<?php echo Template::theme_url('js/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
  <script src="<?php echo Template::theme_url('js/charts/easypiechart/jquery.easy-pie-chart.js'); ?>"  cache="false"></script> 
  <script src="<?php echo Template::theme_url('js/charts/sparkline/jquery.sparkline.min.js'); ?>"></script>
 
  <script src="<?php //echo Template::theme_url('js/sortable/jquery.sortable.js'); ?>"></script>
    <script src="<?php echo Template::theme_url('js/select2.js'); ?>"></script>
    <!-- <script src="<?php //echo Template::theme_url('js/jquery.ui.widget.js'); ?>"></script>
    <script src="<?php //echo Template::theme_url('js/jquery.fileupload.js'); ?>"></script>-->
  <script src="<?php echo Template::theme_url('js/timeago/jquery.timeago.js')?>" type="text/javascript"></script>
<!--  <script src="<?php echo Template::theme_url('js/sweetalert/sweet-alert.min.js'); ?>"></script>
-->   


    
    <script>
	//$(document).ready(function(){	
		$('.selecta').select2(
		
		{ placeholder: "Please Select",    allowClear: false,  dropdownAutoWidth:true}
		
		
		);

	</script>
    <script>
				$(document).ready(function() {
$("abbr.timeago").timeago();
	
	

	});




</script>

<script type="text/javascript" src="<?php echo Template::theme_url('js/datetimepicker/moment.min.js');?>"></script>
<script src="http://js.pusher.com/2.2/pusher.min.js"></script>
<script type="text/javascript" src="<?php echo Template::theme_url('js/pnotify/pnotify.custom.min.js');?>"></script>
<link href="<?php echo Template::theme_url('js/pnotify/pnotify.custom.min.css');?>" media="all" rel="stylesheet" type="text/css" />
<script>
	<?php $this->load->config('pusher');?>
	var pusher = new Pusher('<?php echo $this->config->item('app_key');?>'),
		channel = pusher.subscribe('<?php echo $this->auth->user_id();?>_channel'), 
		stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25},
		addMsg = function($msg){
			var $el = $('.nav-user'), $n = $('.count:first', $el), $v = parseInt($n.text());
			$('.count', $el).fadeOut().fadeIn().text($v+1);
			$el.find('.list-group a.no-notification').hide();
			$($msg).hide().prependTo($el.find('.list-group')).slideDown().css('display','block');
			$("abbr.timeago").timeago();
		};
	PNotify.prototype.options.styling = "bootstrap3";
	channel.bind('notify', function(data){
		var pnotify_opts = {
				title: data.title,
				text: data.message,
				icon: 'fa fa-2x '+data.class,
				type: data.type,
				buttons: {
					closer: false,
					sticker: false
				},
				addclass: "stack-bottomright",
				stack: stack_bottomright
			},
			notice = new PNotify(pnotify_opts),
			$msg = '<a href="'+ (data.link ? data.link : 'javascript:false') +'" class="media list-group-item">'+
					'<span class="pull-left thumb-sm text-center">'+
                    '<i class="fa '+data.class+' fa-2x text-info"></i>'+
                  '</span>'+
                  '<span class="media-body block m-b-none">'+
                    pnotify_opts.title+'<br>'+
                    '<small class="text-muted"><abbr class="timeago" title="'+moment(data.created_on).utc().format()+ '">'+moment(data.created_on).format('DD-MM-YYYY HH:mm:ss')+'</abbr></small>'+
                  '</span>'+
					'</a>';
			addMsg($msg);
			notice.get().click(function() {
				if(typeof data.link == 'undefined' || data.link == '')
					notice.remove();
				else
					location.href = data.link;
			});
		});
		
/*
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * @author Eugene Maslovich <ehpc@em42.ru>
 */

var waitingDialog = (function ($) {

    // Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal bounceIn animated fade in" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
		'<div class="modal-body" align="center"><i class="fa fa-spinner fa-spin fa-3x" style="color:#fff"></i><span style="color:#fff" id="message"></span></div></div>'
		/*
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;height:20px"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>'
		*/
		);

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			var settings = $.extend({
				dialogSize: 'm',
				progressType: ''
			}, options);
			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			if (typeof options === 'undefined') {
				options = {};
			}
			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			
			$dialog.find('#message').text(message); // foo
			
			// Opening dialog
			$dialog.modal();
		},
		update: function (message) {
			$dialog.find('h3').text(message);
			
			$dialog.find('#message').text(message); // foo
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	}

})(jQuery);
</script>    
				
	<?php echo Assets::js(); ?>
    
    

  

</body>
</html>
