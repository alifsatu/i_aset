<?php

	   Assets::add_css( array('bootstrap.css','js/fuelux/fuelux.css', 'animate.css','font-awesome.min.css', 'font.css', 'app.css','js/select2/select2.css','js/select2/theme.css','js/sweetalert/sweet-alert.css'));

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">



<meta charset="utf-8">
<title><?php echo isset($toolbar_title) ? $toolbar_title .' : ' : ''; ?>
<?php e($this->settings_lib->item('site.title')) ?>
</title>

<?php if(isset($_REQUEST['_downloadPDF'])) { 
     Assets::add_css( array('app_2.css'));
     ?>
<meta name="viewport" content="width=1028">
<?php }
else { 
     Assets::add_css( array('app.css'));?>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<?php } ?>
 <link rel="shortcut icon" href="<?php echo base_url(); ?>favicony.ico">
<?php echo Assets::css(null, true); ?>
   <script src="<?php echo Template::theme_url('js/jquery.min.js'); ?>"></script>
    
</head>

<body>
<section class="vbox">
    <?php if(!isset($_REQUEST['_downloadPDF'])) {?>
<header class="bg-white dk header navbar navbar-fixed-top-xs pdf-ignore">
  <div class="navbar-header aside-md"> 
      <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> 
          <i class="fa fa-bars"></i> 
      </a>
      <!-- Logo and system name -->
      <a href="#" class="navbar-brand " data-toggle="fullscreen">
          
          <img src="<?php echo base_url('images/acgt_gat.png') ?>" class=""> &nbspTimesheet
      </a>
      
      <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> 
          <i class="fa fa-cog"></i> 
      </a>
  </div>
   <ul class="nav navbar-nav">
        <li>
          <div class="m-t m-l ">Toggle Error&nbsp;&nbsp;<input type="radio" name="errtog" id="errtog1" value="production" onClick="togerr('production')">Production&nbsp;&nbsp;<input type="radio" name="errtog" id="errtog2" value="development" onClick="togerr('development')">Development</div>
        </li>
  </ul>
  <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
  
	<!-- START Notification -->
	<?php 
		$notification_id = $this->input->get('notification_id');
	
		$this->load->model('notifications/notifications_model',null,true);
		
		if($notification_id)
			$this->notifications_model->delete($notification_id);
		
		$unread_notifications = $this->notifications_model->find_all_by(array('user_id' => $this->auth->user_id(), 'deleted' => 0));
		
		$last_unread_notifications = $this->notifications_model->order_by('created_on', 'desc')->limit(3)->find_all_by(array('user_id' => $this->auth->user_id(), 'deleted' => 0));
	
	?>
    <li class="hidden-xs"> 
		<a href="#" class="dropdown-toggle dk" data-toggle="dropdown"> <i class="fa fa-bell"></i> <span class="badge badge-sm up bg-danger m-l-n-sm count"><?php echo $unread_notifications ? count($unread_notifications) : '0';?></span> </a>
		<section class="dropdown-menu aside-xl">
			<section class="panel bg-white">
				<header class="panel-heading b-light bg-light"> <strong>You have <span class="count"><?php echo $unread_notifications ? count($unread_notifications) : '0';?></span> notifications</strong> </header>
				<div class="list-group list-group-alt animated fadeInRight"> 
					<?php if($last_unread_notifications):?>
					<?php foreach($last_unread_notifications as $notification):?>
					<a href="<?php echo $notification->link ? (strpos($notification->link, '?') === false ? $notification->link .'?' : '&') . 'notification_id='.$notification->id : 'javascript:false';?>" class="media list-group-item"> 
						<span class="pull-left thumb-sm text-center"> <i class="fa <?php echo $notification->class;?> fa-2x text-info"></i> </span> 
						<span class="media-body block m-b-none"> <?php echo $notification->title;?><br>
							<small class="text-muted"><abbr class="timeago" title="<?php echo date('c', strtotime($notification->created_on));?>"><?php echo date('d/m/Y H:i:s', strtotime($notification->created_on));?></abbr></small> 
						</span> 
					</a> 
					<?php endforeach;?>
					<?php else:?>
					<a href="javascript:false" class="media list-group-item no-notification"> 
						<span class="media-body block m-b-none"> No new notification<br>
						</span> 
					</a> 
					<?php endif;?>
				</div>
				<footer class="panel-footer text-sm"> 
					<a href="<?php echo site_url('admin/settings/notifications/desktop');?>" class="pull-right" title="Get notifications in your desktop"><i class="fa fa-desktop"></i></a>
					<a href="<?php echo site_url('admin/settings/notifications');?>">See all the notifications</a> 
				</footer>
			</section>
		</section>
    </li>
	<!-- END Notification -->
    <!--<li class="dropdown hidden-xs"> <a href="#" class="dropdown-toggle dker" data-toggle="dropdown"><i class="fa fa-fw fa-search"></i></a>
      <section class="dropdown-menu aside-xl animated fadeInUp">
        <section class="panel bg-white">
          <form role="search">
            <div class="form-group wrapper m-b-none">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" >
                <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-icon"><i class="fa fa-search"></i></button>
                </span> </div>
            </div>
          </form>
        </section>
      </section>
    </li>-->
    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm avatar pull-left">
    <?
	
	
	if ( @getimagesize(base_url()."themes/notebook/images/userimg/".$current_user->profileimage)) { ?>
    <img id="avatarsm" src="<?=base_url()?>themes/notebook/images/userimg/<?=$current_user->profileimage?>">
    <? } else { ?> <img id="avatarsm" src="<?php echo Template::theme_url('images/userimg/default.jpg') ?>"><? } ?>
    
    
    </span> <?php echo $current_user->display_name ?> <b class="caret"></b> </a>
      <ul class="dropdown-menu animated fadeInRight">
        <span class="arrow top"></span>      
        <li> <a href="<?php echo site_url('/users/profile'); ?>">Profile</a> </li>    
		<li class="visible-xs"> <a href="<?php echo site_url('admin/settings/notifications');?>"> Notifications <span class="badge bg-danger count pull-right"><?php echo $unread_notifications ? count($unread_notifications) : '0';?></span></a> </li>
      
      
        <li class="divider"></li>
        <li> <a href="<?php echo site_url('logout'); ?>"><?php echo lang('bf_action_logout')?></a></li>
      </ul>
    </li>
  </ul>
</header>
    <?php } ?>
<section>
<section class="hbox stretch">
    
    <?php if(!isset($_REQUEST['_downloadPDF'])) {?>
<!-- .aside -->
<aside class="bg-dark lter aside-md hidden-print" id="nav">
  <section class="vbox">
   <!-- <header class="header bg-primary lter text-center clearfix">
      <div class="btn-group">
        <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i class="fa fa-plus"></i></button>
        <div class="btn-group hidden-nav-xs">
          <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown"> Switch Project <span class="caret"></span> </button>
          <ul class="dropdown-menu text-left">
            <li><a href="#">Project</a></li>
            <li><a href="#">Another Project</a></li>
            <li><a href="#">More Projects</a></li>
          </ul>
        </div>
      </div>
    </header>-->
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333"> 
        
        <!-- nav -->
        <nav class="nav-primary hidden-xs">
        <? 
			
			if ( $this->auth->has_permission('Site.Dashboard.View') ) {
			
			?>
          
          <ul class="nav">
       
                  

  <li  class="<? if ( basename($_SERVER['PHP_SELF'])=="dashboards") { echo "active"; }?>">
                      <a href="<? echo base_url();?>index.php/admin/dashboard/dashboards" id="tb_dashboard">
                        <i class="fa fa-dashboard icon">
                          <b class="bg-danger"></b>
                        </i>
                        <span><?=lang('bf_context_dashboard')?></span>
                      </a>
                    </li>
        </ul>  
          <? } ?>
        
          
          
          
          <?php 
		
		  Contexts::set_outer_class('nav');
		  Contexts::set_child_class('nav lt');
		  
                  Contexts::set_icon_tag('<i class="fa fa-dashboard icon"> '
                          . '<b class="bg-danger"></b> </i>'
                          . '<span class="pull-right">'
                          . '<i class="fa fa-angle-down text"></i> '
                          . '<i class="fa fa-angle-up text-active"></i> '
                          . '</span> ');
		  
                  Contexts::set_angle_right('<i class="fa fa-angle-right"></i>');		  
                  Contexts::set_angle_down('<i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i>');
		  Contexts::set_themevar('notebook');
		 // Contexts::set_parent_li("<i class='fa \".{$iconcontext[$no]}.\" icon'> <b class='\".$iconcolor[$no].\"'></b> </i><span class='pull-right'> <i class='fa fa-angle-down text'></i> <i class='fa fa-angle-up text-active'></i> </span>");
		  
		  echo Contexts::render_menu('text', 'normal'); ?> </nav>
        <!-- / nav --> 
        
      </div>
    </section>
    <footer class="footer lt hidden-xs b-t b-dark">
      <div id="chat" class="dropup">
        <section class="dropdown-menu on aside-md m-l-n">
          <section class="panel bg-white">
            <header class="panel-heading b-b b-light">Active chats</header>
            <div class="panel-body animated fadeInRight">
              <p class="text-sm">No active chats.</p>
              <p><a href="#" class="btn btn-sm btn-default">Start a chat</a></p>
            </div>
          </section>
        </section>
      </div>
      <div id="invite" class="dropup">
        <section class="dropdown-menu on aside-md m-l-n">
          <section class="panel bg-white">
            <header class="panel-heading b-b b-light"> John <i class="fa fa-circle text-success"></i> </header>
            <div class="panel-body animated fadeInRight">
              <p class="text-sm">No contacts in your lists.</p>
              <p><a href="#" class="btn btn-sm btn-facebook"><i class="fa fa-fw fa-facebook"></i> Invite from Facebook</a></p>
            </div>
          </section>
        </section>
      </div>
      <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
      <div class="btn-group hidden-nav-xs">
          
                <font style="font-size:12px;">Powered by</font>
                <img src="<?=base_url('images/logo.png')?>" width="12" align="baseline" >  
                <strong style="font-size:10px;color:#999;">
                    <a href="https://apps.satuwater.com.my/i_aset/">
                        <strong style="font-size:13px;color:#999;">i-Aset</strong>
                    </a>       
                </strong>
          
<!--        <button type="button" title="Chats" class="btn btn-icon btn-sm btn-dark" data-toggle="dropdown" data-target="#chat"><i class="fa fa-comment-o"></i></button>
        <button type="button" title="Contacts" class="btn btn-icon btn-sm btn-dark" data-toggle="dropdown" data-target="#invite"><i class="fa fa-facebook"></i></button>-->
      </div>
    </footer>
  </section>
</aside>
<!-- /.aside -->
    <?php } ?>
<section id="content" style="width:100%">
<section class="vbox">
<section class="scrollable padder">

			  <section class="vbox">
	
<?php if(!isset($_REQUEST['_downloadPDF'])) {?>
	   <header class="header">
              
	

	<div class="pull-left" id="sub-menu">
			<?php Template::block('sub_nav', ''); ?>
		</div>
        
        
	
            </header>

<?php } ?>
<script>

function togerr(val)
{

$.post("<?php echo  base_url().'errtog.php' ?>", {togtype:val}, function(data){ } )
	
}
</script>



