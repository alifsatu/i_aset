+function ($) {
  $(function(){
    
    var intro = introJs();

    intro.setOptions({
      steps: [
	   {
          
          element: '#intro',
          intro: '<p class="h4 text-uc"><strong>Hi! Let\'s get started</p><p class="text-uc"><strong>Use these easy steps and get going</p>',
          position: 'center'
        },
      {
          element: '#tb_settings',
          intro: '<p class="h4 text-uc"><strong>Add Users to your Team here</p>Need a workflow?<br> Create here',
          position: 'top'
        },
        {
          element: '#intro',
          intro: '<p class="h4 text-uc"><strong>Were you expecting more steps.<br> Fortunately no. Start using!</p>',
          position: 'center'
        }
               
        
      ],
      showBullets: true,
    });

    intro.start();

  });
}(jQuery);